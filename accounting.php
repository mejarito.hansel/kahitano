<?php
include("header.php");
?>
<div class="container">
	<div class="row">
    	<div class="col-lg-12" style="background-image: url('assets/images/banner copy2.png');
		height:210px;">
        </div>
    </div>
	<br>
    <div class="row">
        <h4 style='color:#2c3e50;'>
            <center>
                <span  style='color:#2c3e50; margin-right:5px;' class="fa fa-bank">  
                </span>
                <b>Accounting</b>
            </center>
        </h4><hr>
        <!--Contents-->
        <div class="col-xs-12">
            <form action="accounting.php?action=edit&amp;id=1" enctype="multipart/form-data" method="POST" class="form-horizontal well" style="overflow-y:auto;" name="form1">
                <fieldset>
                    <h4><b><center>WAYNE, BRUCE<br>DK</center></b></h4>
                    <center>
                        <div class="bs-component">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tALL" data-toggle="tab" aria-expanded="false">ALL
                                    </a>
                                </li>
                               <li class="dropdown">
                                   <a class="dropdown-toggle" data-toggle="dropdown" href="#">Year & Tri/Semester
                                   <span class="caret" style="color:black;"></span></a>
                                    <ul class="dropdown-menu">
                                      <li><a href="#t3" data-toggle="tab" aria-expanded="false">2014-15 3rd Tri/Semester                
                                        </a></li>
                                      <li><a href="#t2" data-toggle="tab" aria-expanded="false">2014-15 2nd Tri/Semester                
                                        </a></li>
                                    </ul>
                                </li>
                               <li>
                                    <a href="#t" data-toggle="tab" aria-expanded="false">OTHERS                
                                    </a>
                                </li>
                            </ul>
                            <br>
                            <div id="myTabContent" class="tab-content">
                                <div class="tab-pane fade" id="t3">
                                <p>2014-15 3rd Tri/Semester</p>
                                </div>
                            <div class="tab-pane fade" id="t2">
                                <p>2014-15 2nd Tri/Semester</p>
                             <!-- START --> 
                             <!-- START -->
                                <div  style="overflow:auto;">
                                <table class="table table-bordered" style="" cellpadding="0px" cellspacing="0px">
                                    <tbody>
                                        <tr><th width="10%">Date</th>
                                            <th width="">Particular</th>
                                            <th width="10%" align="center">OR</th>
                                            <th width="25%"><center>Account Receivables</center></th>
                                            <th width="25%"><center>Payment</center></th>
                                        </tr>
                                        <tr><td valign="top"><b>02/25/15</b></td>
                                            <td colspan="4" width="90%" style="padding:0px;">
                                                <table class="table" style="padding:0px; width:100%; cellpadding:0px; cellspacing: 0px; border-top: transparent; ">                                                                                                  
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <span>
                                                                <b style="color:#00CC00;">Tuition Fees</b>
                                                                </span>
                                                            </td>
                                                            <td width="11.1%" align="center">
                                                                <a title="Click to Edit" href="?action=edit_payment&amp;id=1&amp;pay_ID=2055" onclick="return confirm('Click OK to proceed')"> 154</a> 
                                                            </td>
                                                            <td width="27.8%" align="center">
                                                            </td>
                                                            <td width="27.8%" align="center">
                                                                <a title="Click to Edit" href="?action=edit_payment&amp;id=1&amp;pay_ID=2055" onclick="return confirm('Click OK to proceed')"> 
                                                                P 3080.00</a>
                                                                <a href="?action=hide_payment&amp;pay_ID=2055&amp;uid=1" title="Click to Delete" onclick="return confirm('Are you sure to delete?')">[x]</a>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><b>Total:</b></td>
                                            <td align="center"><b>P 0.00</b></td>
                                            <td align="center"><b>P 3,080.00</b></td>
                                       </tr>     
                                    </tbody>
                                </table>  
                                </div>           
                                <div align="right" style="width:100%;">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Excess:
                                                </td>
                                                <td>
                                                    <b><font color="orange">P 3,080.00</font></b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Balance:
                                                </td>
                                                <td><b>0</b>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div align="center" style="width:100%;">
                                    <h4>Promissory</h4>
                                    <div style="overflow:auto;">
                                        <table class="table table-bordered table-responsive table-striped text-md" width="80%">
                                            <tbody>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Date Added</th>
                                                    <th>Date Promised</th>
                                                    <th>Particular</th>
                                                    <th>Amount</th>
                                                    <th>Interest</th>
                                                    <th>Status</th>
                                                    <th>Penalty</th>
                                                    <th>Option</th>
                                                </tr>
                                                <tr>
                                                    <td colspan="9" align="center">No Record</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>       
                                    <!-- END -->
                                 <!-- END -->
                                </div>
                                <div class="tab-pane fade" id="t">
                                    OTHERS
                                </div>
                                <div class="tab-pane fade in active" id="tALL">
                                    ALL PAYMENTS
                                </div>
                            </div>
                        </div>   
                    </center>
                </fieldset>
            </form>
        </div>
    </div>
<?php
include("footer.php");
?>
</div>