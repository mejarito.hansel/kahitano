<div class="container">
		<div class="row col-md-8 col-md-offset-2">
				<h4 style='color:#2c3e50;'><center>
				<span  style='color:#2c3e50;' class="fa fa-navicon">  
				</span>
					<b>Navigation</b>
				</center></h4>
				<hr>
				
			
			<?php
			$level_id = $_SESSION['USER']['access_ID'];
			
			if($level_id=='1'||$level_id=='3'){
				//SUPERADMIN & SCHOOL
				NAV::SIS(); 
				NAV::ACCTG(); 
				NAV::SUBJ_MAN(); 
				NAV::ROOM_MAN(); 
				#NAV::ASSESSMENTLIST(); 
				NAV::SEM_MAN(); 
				NAV::INS_MAN(); 
				NAV::COURSE_MAN(); 
				NAV::CURR_MAN(); 
				NAV::GRADE_MAN(); 
				NAV::ENROLMENT(); 
				NAV::CHANGE_PASS(); 
				NAV::LOGOUT();
				
			}else if($level_id=='2'){
				//REGISTRAR
				NAV::SIS(); 
				NAV::GRADE_MAN();
				NAV::ENROLMENT(); 
				NAV::CHANGE_PASS();
				NAV::LOGOUT();		
				
			}else if($level_id=='4'){
				//ACCOUNTING
				NAV::ACCTG_TRANSACT();
				NAV::ACCTG_PROMISSORY();
				NAV::ACCTG_PARTICULARS();
				NAV::ACCTG_EXPENSE();
				NAV::ACCTG_DEPOSIT();
				NAV::ACCTG_TRANSACTION_LOGS();
				NAV::ACCTG_TRANSACTION_REPORTS();
				NAV::CHANGE_PASS();
				NAV::LOGOUT();
			}else if($level_id=='8'){
				//ACCOUNTING
				
				NAV::ENROLMENT_REPORT();
				NAV::ENROLMENT_COMPARATIVE_REPORT();
				NAV::ACCTG_REPORT();
				NAV::ACCTG_AUDIT();
				NAV::AUDIT_REPORT();
				NAV::CHANGE_PASS();
				NAV::LOGOUT();
				
			}else if($level_id=='5'){ 
			
				
				$instructor_ID = ui::getInstructorID($user['account_ID']);  
				$instructor_ID = $instructor_ID['instructor_ID'];
			
				#SESSION::DisplayCustomMsg('warning','Maintenance On-going'); ?>
				
			<?php 
				#NAV::OLD_PORTAL();
				NAV::STUDENT_LIST();
				NAV::ENCODE_GRADES();
				#NAV::MY_SUBJECTS();
				NAV::CHANGE_PASS();
				NAV::LOGOUT();
			} ?>
		</div>	
	</div>

<div class="clearfix"></div>