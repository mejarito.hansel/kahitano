<!DOCTYPE html>
<html class="easy-sidebar-active">
<head>
	<title>SCHOOL MANAGEMENT SYSTEM</title>
	<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400' rel='stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= HOME; ?>assets/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?= HOME; ?>assets/css/easy-sidebar.css">
	<link rel="stylesheet" href="<?= HOME; ?>assets/css/styles.css">
	<link rel="stylesheet" href="<?= HOME; ?>assets/font-awesome-4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= HOME; ?>assets/css/simplemenu.css">
	<link rel="stylesheet" href="<?= HOME; ?>assets/css/morris.css">
	<link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
  	<script src="<?= HOME; ?>assets/jquery/1.11.3/jquery.min.js"></script>
  	<!-- <script src="<?= HOME; ?>assets/jquery/1.11.3/jquery.min.js"></script> -->
  	<script src="<?= HOME; ?>assets/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  	<!--script src="<?= HOME; ?>/detect_swipe/jquery.detect_swipe.js"></script-->
  	<script async src="<?= HOME; ?>assets/js/simplemenu.js"> </script> 
  	<script src="<?= HOME; ?>assets/jquery.cookie.min.js"></script>
  	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  	<style type="text/css">
  		html {
  			overflow: hidden !important;
  		}
  	</style>
</head>
<body style="background: linear-gradient(to top left, #3333ff 0%, #66ccff 100%); background-size: 100%;">

	<div class="navbar navbar-default navbar-fixed-top hidden">
		<div class="container">
			<div class="navbar-header ">
			    <a href="<?= HOME; ?>index.php" class="navbar-brand">School Management System</a>

<?php
	if(basename($_SERVER['PHP_SELF'])=='index.php'||basename($_SERVER['PHP_SELF'])=='home.php'){
	}else{
?>			
<div class="col-sm-8 col-sm-offset-2 col-md-8 col-md-offset-2"  style="float:right;">
        		<div class="mobilenav"> 
				  	<?php include(LAYOUTS_N . "navigation.php"); ?>
				</div> 
	      	</div>
	
	      	<a href="javascript:void(0)" class="icon"> 
		           	<div class="hamburger"> 
	            		<div class="menui top-menu"></div> 
             			<div class="menui mid-menu"></div> 
		             	<div class="menui bottom-menu"></div> 
		           	</div> 
		    </a>
<?php 
	}
?>
			</div>
			
		</div>
	</div>
			
			
			
