<?php
include("header.php");
?>
<div class="container">
	<div class="row">
    	<div class="col-lg-12" style="background-image: url('assets/images/banner copy2.png');
		height:210px;">
        </div>
    </div>
	<br>
    <div class="row">
        <h4 style='color:#2c3e50;'>
            <center>
                <span  style='color:#2c3e50; margin-right:5px;' class="fa fa-calendar">  
                </span>
                <b>Schedule</b>
            </center>
        </h4><hr>
        <!--Contents-->
        <div class="col-xs-12">
            <table class="table table-hover table-responsive table-striped text-md">
                <tr>
                    <td>
                        <b><i class="fa fa-book"></i>&nbsp;&nbsp;Course:
                    </td>
                    <td>
                        DK
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><i class="fa fa-calendar"></i>&nbsp;&nbsp;School Year/Semester:
                    </td>
                    <td>
                        <select style="border:1px solid rgba(100,100,100,0);">
                            <option>2015-16 1st Trimester</option>
                            <option>2015-16 2nd Trimester</option>
                            <option>2015-16 3rd Trimester</option>
                        </select>
                    </td>
                </tr>
            </table>
            <hr>
            <center><h4>2015-16 1st Trimester</h4></center>
            <div style="overflow:auto;">
            <table class="table table-hover table-responsive table-striped text-md">
                <tbody>
                    <tr>             
                        <th>Code</th>
                        <th>Description</th>
                        <th>Section</th>
                        <th>Room</th>
                        <th>Day</th>
                        <th style="">Time</th>   
                    </tr>    
                    <tr>
                        <td>ENG 2</td>
                        <td>Communication Arts &amp; Skills 2</td>
                        <td>ATM   - 1Y2-1</td>
                        <td>RM 201</td>
                        <td>Mon</td>
                        <td>7:00 AM-10:00 AM</td>
                    </tr>    
                    <tr>
                        <td>ACCTG 1</td>
                        <td>Basic Accounting </td>
                        <td>AHRM - 1Y2-1</td>
                        <td>RM 201</td>
                        <td>Mon</td>
                        <td>10:00 AM-1:00 PM</td>
                    </tr>  
                    <tr>
                        <td>CS 417</td>
                        <td>Multimedia Systems</td>
                        <td>ACT - 1Y3-1</td>
                        <td>CL 2</td>
                        <td>Mon</td>
                        <td>1:00 PM-4:00 PM</td>
                    </tr>
                    <tr>
                        <td>ACT 4</td>
                        <td>Digital Electronics</td>
                        <td>ACT - 1Y3-1</td>
                        <td>RM 201</td>
                        <td>Tue</td>
                        <td>1:00 PM-4:00 PM</td>
                    </tr>   
                    <tr>
                        <td>ACT 5</td>
                        <td>Technical Drafting with AUTOCAD</td>
                        <td>ACT - 1Y3-1</td>
                        <td>RM 201/<br>CL 1</td>
                        <td>Wed/<br>Tue</td>
                        <td>1:00 PM-4:00 PM/<br>4:00 PM-7:00 PM</td>
                    </tr>  
                    <tr>
                        <td>CS 120</td>
                        <td>Software Utilities</td>
                        <td>ACT - 1Y3-1</td>
                        <td>RM 201</td>
                        <td>Wed</td>
                        <td>4:00 PM-7:00 PM</td>
                    </tr>     
                    <tr>
                        <td>NSTP 1</td>
                        <td>National Service Training Program 1</td>
                        <td>ATM - 1Y2-1</td>
                        <td>RM 302</td>                                                                                          </td>
                        <td>Fri</td>
                        <td>10:00 AM-1:00 PM</td>
                    </tr>    
                    <tr>
                        <td>PE 1</td>
                        <td>Physical Education 1</td>
                        <td>AHRM - 1Y2-1</td>
                        <td>PE                                                                                             </td>
                        <td>Fri</td>
                        <td>1:30 PM-3:30 PM</td>
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
<?php
include("footer.php");
?>
</div>