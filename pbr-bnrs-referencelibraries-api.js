/* 
 * This contains API that can be used for testing if response from web server
 * is okay and live.
 */
$ = (typeof $ !== 'undefined') ? $ : {};
$.dti = (typeof $.dti !== 'undefined') ? $.dti : {};
$.dti.pbr = (typeof $.dti.pbr !== 'undefined') ? $.dti.pbr : {};

$.dti.pbr.ref = (function () {
    var ___reflib_path = '/reflibraries';
    var ___psgclib_path = '/psgclib/';
    var ___psiclib_busact_path = '/psic/busact/';
    var ___psiclib_prdsrv_path = '/psic/prdsrv/';
    var ___psiclib_actual_path = '/psic/actual/';

    var __postReference = function (idv, cbackfunc) {
        // Validation
        var errmsg = null;

        if (typeof idv === 'undefined') {
            errmsg = 'Invalid ID. Should not be empty and must contain numbers only.';
        }

        if (errmsg !== null) {
            if (typeof cbackfunc !== 'undefined') {
                cbackfunc({
                    status: 'ERROR',
                    message: errmsg
                });
            } else {
                console.log('--post-echo-result--');
                console.log(errmsg);
                console.log('--post-echo-result--');
            }
        } else {
            var echod = {
                valueCode: idv
            };

            var ctx = $.dti.pbr.getContext();

            $.dti.pbr.executePost(ctx + ___reflib_path, echod).done(function (result) {
                if (typeof cbackfunc !== 'undefined') {
                    cbackfunc(result);
                } else {
                    console.log('--post-echo-result--');
                    console.log(result);
                    console.log('--post-echo-result--');
                }
            });
        }
    };
    
    var ___getPsgcList = function(argcode, cbackfunc) {
        var finarg = (typeof argcode === 'undefined' || argcode === null) ? '' : argcode.trim();
        
        if(finarg === '' || finarg.length !== 10) {
            var retval = {
                status: 'ERROR',
                message: 'Invalid argument for PSGC List Query'
            };
            
            if (typeof cbackfunc !== 'undefined') {
                cbackfunc(retval);
            } else {
                console.log(retval);
            }
        } else {
            var ctx = $.dti.pbr.getContext();
            
            $.dti.pbr.executeGet(ctx + ___psgclib_path + argcode).done(function (result) {
                if (typeof cbackfunc !== 'undefined') {
                    cbackfunc(result);
                } else {
                    console.log(result);
                }
            });
        }
    };
    
    var __getProvinceForRegion = function(regionCode, cbackfunc) {
        ___getPsgcList('P' + regionCode, cbackfunc);
    };
    
    var __getMunicipalityForProvince = function(provinceCode, cbackfunc) {
        ___getPsgcList('M' + provinceCode, cbackfunc);
    };
    
    var __getBarangayForMunicipality = function(muncityCode, cbackfunc) {
        ___getPsgcList('B' + muncityCode, cbackfunc);
    };
    
    var __getZipForBarangay = function(barangayCode, cbackfunc) {
        ___getPsgcList('Z' + barangayCode, cbackfunc);
    };
    
    var __getBusinessActivity = function (filterdesc, cbackfunc) {
        var fnfilter = (typeof filterdesc !== 'undefined' && filterdesc !== null) ? filterdesc.trim() : '*';

        var ctx = $.dti.pbr.getContext();

        $.dti.pbr.executeGet(ctx + ___psiclib_busact_path + fnfilter).done(function (result) {
            if (typeof cbackfunc !== 'undefined') {
                cbackfunc(result);
            } else {
                console.log(result);
            }
        });
    };
    
    var __getProductsAndServices = function (grpcode, filterdesc, cbackfunc) {
        if(typeof grpcode !== 'undefined' && grpcode !== null && grpcode.trim().length > 0) {
            var fnfilter = (typeof filterdesc !== 'undefined' && filterdesc !== null) ? filterdesc.trim() : '*';

            var ctx = $.dti.pbr.getContext();

            $.dti.pbr.executeGet(ctx + ___psiclib_prdsrv_path + grpcode + '/' + fnfilter).done(function (result) {
                if (typeof cbackfunc !== 'undefined') {
                    cbackfunc(result);
                } else {
                    console.log(result);
                }
            });
        } else {
            var retval = {
                status: 'ERROR',
                message: 'Missing Business Activity Code for PSIC Products And Services'
            };
            
            if (typeof cbackfunc !== 'undefined') {
                cbackfunc(retval);
            } else {
                console.log(retval);
            }
        }
    };
    
    var __getActualPSICList = function (classcode, filterdesc, cbackfunc) {
        if(typeof classcode !== 'undefined' && classcode !== null && classcode.trim().length > 0) {
            var fnfilter = (typeof filterdesc !== 'undefined' && filterdesc !== null) ? filterdesc.trim() : '*';

            var ctx = $.dti.pbr.getContext();

            $.dti.pbr.executeGet(ctx + ___psiclib_actual_path + classcode + '/' + fnfilter).done(function (result) {
                if (typeof cbackfunc !== 'undefined') {
                    cbackfunc(result);
                } else {
                    console.log(result);
                }
            });
        } else {
            var retval = {
                status: 'ERROR',
                message: 'Missing Products and Service Code for PSIC List'
            };
            
            if (typeof cbackfunc !== 'undefined') {
                cbackfunc(retval);
            } else {
                console.log(retval);
            }
        }
    };

    return {
        postReference: __postReference,
        getProvinceForRegion : __getProvinceForRegion,
        getMunicipalityForProvince : __getMunicipalityForProvince,
        getBarangayForMunicipality : __getBarangayForMunicipality,
        getBusinessActivity : __getBusinessActivity,
        getProductsAndServices : __getProductsAndServices,
        getActualPSICList : __getActualPSICList,
        getZipForBarangay : __getZipForBarangay
    };
}());