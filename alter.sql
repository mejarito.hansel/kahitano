ALTER TABLE `student_additional_info` ADD `sai_religion` TEXT NULL AFTER `sai_graduating`, ADD `sai_civil` TEXT NULL AFTER `sai_religion`, ADD `sai_height` TEXT NULL AFTER `sai_civil`, ADD `sai_weight` TEXT NULL AFTER `sai_height`, ADD `sai_special_skills` TEXT NULL AFTER `sai_weight`, ADD `sai_present_addr_st` TEXT NULL AFTER `sai_special_skills`, ADD `sai_present_brgy` TEXT NULL AFTER `sai_present_addr_st`, ADD `sai_present_city` TEXT NULL AFTER `sai_present_brgy`, ADD `sai_present_district` TEXT NULL AFTER `sai_present_city`, ADD `sai_present_prov` TEXT NULL AFTER `sai_present_district`;



ALTER TABLE `student_additional_info` ADD `sai_present_contact` TEXT NULL AFTER `sai_present_prov`;

ALTER TABLE `student_information` ADD `si_BIRTHPLACE` TEXT NULL AFTER `si_BIRTHDATE`;
