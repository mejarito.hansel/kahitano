<?php
include("header.php");
?>
<style>
    .orb-lg{
        display: inline-block;
        -moz-border-radius: 100px;
        -webkit-border-radius: 100px;
        border-radius: 100px;
        -moz-box-shadow: 0px 0px 2px #888;
        -webkit-box-shadow: 0px 0px 2px #888;
        box-shadow: 0px 0px 2px #888;
        color:white;
    }
    .orb-md{
        font-size:30px;
        padding: 18px 20px ;
        display: inline-block;
        -moz-border-radius: 100px;
        -webkit-border-radius: 100px;
        border-radius: 100px;
        -moz-box-shadow: 0px 0px 2px #888;
        -webkit-box-shadow: 0px 0px 2px #888;
        box-shadow: 0px 0px 2px #888;
        color:white;
    }
</style>
<div class="container">
	<div class="row">
    	<div class="col-lg-12" style="background-image: url('assets/images/banner copy2.png');
		height:210px;">
        </div>
    </div>
	<br>
    <div class="row">
        <h4 style='color:#2c3e50;'>
            <center>
                <span  style='color:#2c3e50; margin-right:5px;' class="fa fa-file-text">  
                </span>
                <b>Student Information</b>
            </center>
        </h4><hr>
        <!--Contents-->
        <div class="col-xs-12">
            <center>
                <table border=0>
                    <tr>
                        <td  style="text-align:right;">
                            <img class="orb-lg" src="assets/images/id.png" border="0" width="90px" height="90px">
                        <td style="padding-left:20px;"><label for="email">Student No.</label>
                            <span id="email" style="color:black;">00-1996-69</span>                    
                        <br><label for="email">Name:</label>
                            <span id="email" style="color:black;">Wayne, Bruce</span>
                    <tr>
                        <td style="text-align:right;"><label for="email">Date of Birth</label><br>
                            <span id="email" style="color:black;">July 3, 1996</span>
                        <td  style="text-align:left; padding-left:20px;">
                            <i class="orb-md fa fa-birthday-cake" style="background:#e74c3c;"></i>
                    <tr>
                        <td style="text-align:right;">
                            <i class="orb-md fa fa-male" style="padding: 13px 20px ; background:#2980b9;"></i>
                        <td style="padding-left:20px;"><label for="email">Gender</label>
                            <span id="email" style="color:black;"><br>Male</span>
                    <tr>
                        <td style="text-align:right;"><label for="email">Address</label><br>
                            <span id="email" style="color:black;">Wayne Enterprises</span>
                        <td  style="text-align:left; padding-left:20px;">
                            <i class="orb-md fa fa-street-view" style="background:#27ae60;"></i>
                    <tr>
                        <td style="text-align:right;">
                            <i class="orb-md fa fa-envelope" style="background:#bdc3c7;"></i>
                        <td style="padding-left:20px;"><label for="email">E-mail Address</label><br>
                            <span id="email" style="color:black;">johndoe@bat.cave</span>
                    <tr>
                        <td style="text-align:right;"><label for="email">Contact</label><br>
                            <span id="email" style="color:black;">09696969669</span>
                        <td  style="text-align:left; padding-left:20px;">
                            <i class="orb-md fa fa-phone" style="background:#1abc9c;"></i>
                </table>
             </center>
        </div>
    </div>

<?php
include("footer.php");
?>
</div>