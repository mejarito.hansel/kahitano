<?php
include("header.php");
?>
<div class="container">
	<div class="row">
    	<div class="col-lg-12" style="background-image: url('assets/images/banner copy2.png');
		height:210px;">
        </div>
    </div>
	<br>
    <div class="row">
        <h4 style='color:#2c3e50;'>
            <center>
                <span  style='color:#2c3e50; margin-right:5px;' class="fa fa-mortar-board">  
                </span>
                <b>Curriculum</b>
            </center>
        </h4><hr>
        <!--Contents-->
        <div class="col-xs-12">
        <table class="table table-hover table-responsive table-striped text-md">
                <tbody>
                <tr>
                
                </tr>
                <tr>
                    <td><b>Course: 
                  </td>
                  <td colspan="4">DK               </td></tr>
                <tr>
                    <td><b>Curriculum:
                  </td>
                  <td colspan="4">2014  Curriculum              </td></tr>
                </tbody></table><div style="overflow:auto;">
            <table id="sem13" border="1" class="table table-responsive text-md">
                                    
                                                            <tbody><tr><th class="bg-primary">1st Year - 1st Trimester                                                                </th></tr><tr>
                                                                    <td>
                                                                        <table border="0" class="table table-responsive text-md">
                                                                            <!-- <th width="10%">S-ID -->
                                                                            <tbody><tr><th width="10%">S-Code
                                                                            </th><th width="30%">Subject Description
                                                                            </th><th width="10%" style="text-align:center">Units
                                                                            </th><th width="20%" style="text-align:center">PreRequisite
                                                                            </th><th width="" style="text-align:center">Option
                                                                     
                                                                               
                                                                                                                            </th></tr><tr>
                                                                    <!-- <td colspan='6'> -->
                                                                        <!--<table border='1'>-->
                                                                                                                                                        </tr><tr>
                                                                                   <!-- <td>144 -->
                                                                                    <td>IT 111                                                                                    </td><td>Computer Programming 1                                                                                    </td><td align="center">3                                                                                    </td><td align="center">None                                                                                  </td><td align="center">
                                                                                                                                                                     
                                                                                                                                                                      <form>
                                                                                  <select name="grade144" class="form-control">
                                                                                    <option value="NG">NO GRADE</option>
                                                                                    <option value="1.00">1.00</option>
                                                                                    <option value="1.25">1.25</option>
                                                                                    <option value="1.50">1.50</option>
                                                                                    <option value="1.75">1.75</option>
                                                                                    <option value="2.00">2.00</option>
                                                                                    <option value="2.25">2.25</option>
                                                                                    <option value="2.50">2.50</option>
                                                                                    <option value="2.75">2.75</option>
                                                                                    <option value="3.00">3.00</option>
                                                                                    <option value="5.00">5.00</option>
                                                                                    <option value="INC">INCOMPLETE</option>
                                                                                    <option value="DRP">DROPPED</option>
                                                                                     <option value="C">CREDIT</option>
                                                                                </select>   
                                                                                
                                                                                </form>
                                                                                
                                                                               
                                                                                                                                                                    
                                                                                 <span id="result144"></span>
                                                                                                                                                            
                                                                        <!--</table>-->
                                                                        <!--?// = $val['subject_ID']; ?-->
                                                                    </td>
                                                                                                                            </tr><tr>
                                                                    <!-- <td colspan='6'> -->
                                                                        <!--<table border='1'>-->
                                                                                                                                                        </tr><tr>
                                                                                   <!-- <td>146 -->
                                                                                    <td>IT 115                                                                                    </td><td>Desktop Publishing                                                                                    </td><td align="center">3                                                                                    </td><td align="center">None                                                                                  </td><td align="center">
                                                                                  1.00                                                                                  
                                                                                 <span id="result146"></span>
                                                                                                                                                            
                                                                        <!--</table>-->
                                                                        <!--?// = $val['subject_ID']; ?-->
                                                                    </td>
                                                                                                                            </tr><tr>
                                                                    <!-- <td colspan='6'> -->
                                                                        <!--<table border='1'>-->
                                                                                                                                                        </tr><tr>
                                                                                   <!-- <td>145 -->
                                                                                    <td>IT 113                                                                                    </td><td>IT  Fundamentals                                                                                    </td><td align="center">3                                                                                    </td><td align="center">None                                                                                  </td><td align="center">
                                                                                                                                                                     
                                                                                                                                                                      <form>
                                                                                  <select name="grade145" class="form-control">
                                                                                    <option value="NG">NO GRADE</option>
                                                                                    <option value="1.00">1.00</option>
                                                                                    <option value="1.25">1.25</option>
                                                                                    <option value="1.50">1.50</option>
                                                                                    <option value="1.75">1.75</option>
                                                                                    <option value="2.00">2.00</option>
                                                                                    <option value="2.25">2.25</option>
                                                                                    <option value="2.50">2.50</option>
                                                                                    <option value="2.75">2.75</option>
                                                                                    <option value="3.00">3.00</option>
                                                                                    <option value="5.00">5.00</option>
                                                                                    <option value="INC">INCOMPLETE</option>
                                                                                    <option value="DRP">DROPPED</option>
                                                                                     <option value="C">CREDIT</option>
                                                                                </select>   
                                                                                
                                                                                </form>
                                                                                
                                                                               
                                                                                                                                                                    
                                                                                 <span id="result145"></span>
                                                                                                                                                            
                                                                        <!--</table>-->
                                                                        <!--?// = $val['subject_ID']; ?-->
                                                                    </td>
                                                                                                                            </tr><tr>
                                                                    <!-- <td colspan='6'> -->
                                                                        <!--<table border='1'>-->
                                                                                                                                                        </tr><tr>
                                                                                   <!-- <td>97 -->
                                                                                    <td>ENG 1                                                                                    </td><td>Communication Arts &amp; Skills 1                                                                                    </td><td align="center">3                                                                                    </td><td align="center">None                                                                                  </td><td align="center">
                                                                                  1.75                                                                                  
                                                                                 <span id="result97"></span>
                                                                                                                                                            
                                                                        <!--</table>-->
                                                                        <!--?// = $val['subject_ID']; ?-->
                                                                    </td>
                                                                                                                            </tr><tr>
                                                                    <!-- <td colspan='6'> -->
                                                                        <!--<table border='1'>-->
                                                                                                                                                        </tr><tr>
                                                                                   <!-- <td>170 -->
                                                                                    <td>MAT 1                                                                                    </td><td>College Algebra                                                                                     </td><td align="center">3                                                                                    </td><td align="center">None                                                                                  </td><td align="center">
                                                                                  1.25                                                                                  
                                                                                 <span id="result170"></span>
                                                                                                                                                            
                                                                        <!--</table>-->
                                                                        <!--?// = $val['subject_ID']; ?-->
                                                                    </td>
                                                                                                                            </tr><tr>
                                                                    <!-- <td colspan='6'> -->
                                                                        <!--<table border='1'>-->
                                                                                                                                                        </tr><tr>
                                                                                   <!-- <td>147 -->
                                                                                    <td>IT 123                                                                                    </td><td>PC Trouble Shooting                                                                                    </td><td align="center">3                                                                                    </td><td align="center">None                                                                                  </td><td align="center">
                                                                                  1.50                                                                                  
                                                                                 <span id="result147"></span>
                                                                                                                                                            
                                                                        <!--</table>-->
                                                                        <!--?// = $val['subject_ID']; ?-->
                                                                    </td>
                                                                    
                                                                    
                                                            <script>
                                $(document).ready(function(){
                                
                                
                                                                              $('[name="grade144"]').each(function() {
                                                $(this).change(function() {
                                                    var mydata = $('[name="grade144"]').val();//$(this).val();
                                                    var inputdata = 144;
                                                    var studentID = 578;
                                                //alert(mydata)
                                                //alert(inputdata)
                                                //alert(studentID)
                                            
                                                    $.ajax({   
                                                       type: 'GET',   
                                                       url: 'update_ajax.php',   
                                                       data: {action:'grade_encoding_admin.php',subject_ID:inputdata, grade:mydata, student_ID:studentID},
                                                            success: function(data){
                                                            
                                                              $('#result144').html(data);
                                                            }                                                   
                                                        });
                                                
                                                
                                                    });
                                                    
                                            });
                                                                              $('[name="grade146"]').each(function() {
                                                $(this).change(function() {
                                                    var mydata = $('[name="grade146"]').val();//$(this).val();
                                                    var inputdata = 146;
                                                    var studentID = 578;
                                                //alert(mydata)
                                                //alert(inputdata)
                                                //alert(studentID)
                                            
                                                    $.ajax({   
                                                       type: 'GET',   
                                                       url: 'update_ajax.php',   
                                                       data: {action:'grade_encoding_admin.php',subject_ID:inputdata, grade:mydata, student_ID:studentID},
                                                            success: function(data){
                                                            
                                                              $('#result146').html(data);
                                                            }                                                   
                                                        });
                                                
                                                
                                                    });
                                                    
                                            });
                                                                              $('[name="grade145"]').each(function() {
                                                $(this).change(function() {
                                                    var mydata = $('[name="grade145"]').val();//$(this).val();
                                                    var inputdata = 145;
                                                    var studentID = 578;
                                                //alert(mydata)
                                                //alert(inputdata)
                                                //alert(studentID)
                                            
                                                    $.ajax({   
                                                       type: 'GET',   
                                                       url: 'update_ajax.php',   
                                                       data: {action:'grade_encoding_admin.php',subject_ID:inputdata, grade:mydata, student_ID:studentID},
                                                            success: function(data){
                                                            
                                                              $('#result145').html(data);
                                                            }                                                   
                                                        });
                                                
                                                
                                                    });
                                                    
                                            });
                                                                              $('[name="grade97"]').each(function() {
                                                $(this).change(function() {
                                                    var mydata = $('[name="grade97"]').val();//$(this).val();
                                                    var inputdata = 97;
                                                    var studentID = 578;
                                                //alert(mydata)
                                                //alert(inputdata)
                                                //alert(studentID)
                                            
                                                    $.ajax({   
                                                       type: 'GET',   
                                                       url: 'update_ajax.php',   
                                                       data: {action:'grade_encoding_admin.php',subject_ID:inputdata, grade:mydata, student_ID:studentID},
                                                            success: function(data){
                                                            
                                                              $('#result97').html(data);
                                                            }                                                   
                                                        });
                                                
                                                
                                                    });
                                                    
                                            });
                                                                              $('[name="grade170"]').each(function() {
                                                $(this).change(function() {
                                                    var mydata = $('[name="grade170"]').val();//$(this).val();
                                                    var inputdata = 170;
                                                    var studentID = 578;
                                                //alert(mydata)
                                                //alert(inputdata)
                                                //alert(studentID)
                                            
                                                    $.ajax({   
                                                       type: 'GET',   
                                                       url: 'update_ajax.php',   
                                                       data: {action:'grade_encoding_admin.php',subject_ID:inputdata, grade:mydata, student_ID:studentID},
                                                            success: function(data){
                                                            
                                                              $('#result170').html(data);
                                                            }                                                   
                                                        });
                                                
                                                
                                                    });
                                                    
                                            });
                                                                              $('[name="grade147"]').each(function() {
                                                $(this).change(function() {
                                                    var mydata = $('[name="grade147"]').val();//$(this).val();
                                                    var inputdata = 147;
                                                    var studentID = 578;
                                                //alert(mydata)
                                                //alert(inputdata)
                                                //alert(studentID)
                                            
                                                    $.ajax({   
                                                       type: 'GET',   
                                                       url: 'update_ajax.php',   
                                                       data: {action:'grade_encoding_admin.php',subject_ID:inputdata, grade:mydata, student_ID:studentID},
                                                            success: function(data){
                                                            
                                                              $('#result147').html(data);
                                                            }                                                   
                                                        });
                                                
                                                
                                                    });
                                                    
                                            });
                                            
                                });
                                </script>       
                                                                    
                                                                                                                          
                                                                     
                                                                        </tr></tbody></table>
                                    </td></tr></tbody></table></div>
        </div>
    </div>
<?php
include("footer.php");
?>
</div>