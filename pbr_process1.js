$ = (typeof $) !== 'undefined' ? $ : {};
$.dti = (typeof $.dti) !== 'undefined' ? $.dti : {};
$.dti.pbr = (typeof $.dti.pbr) !== 'undefined' ? $.dti.pbr : {};
$.dti.pbr.registration = (typeof $.dti.pbr.registration) !== 'undefined' ? $.dti.pbr.registration : {};

$.dti.pbr.registration.pageone = (function () {

    var __attachEventsToGeneralInfoPage = function () {

        $('#GoToNextPage').on('click', function () {
            // ___validateGeneralInfoPage();
            window.location.assign("pbr_process2.html");
        });

        var hiddenBox = $( "#AgreeModal" );
        	$( "#DeclineBtn" ).on( "click", function( event ) {
        	$( ".AgreeModalForm" ).addClass( "in" );
        	hiddenBox.toggle();
      	});

      	$( ".TermsAgree" ).on( "click", function( event ) {
        	$( ".AgreeModalForm" ).removeClass( "in" );
        	// hiddenBox.toggle();
        	window.location.assign("../index.html");
      	});

    };

    return{
        attachEventsToGeneralInfoPage: __attachEventsToGeneralInfoPage
    };

}());