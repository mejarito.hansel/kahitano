<?php 
//INITIALIZE INCLUDES
    include('init.php');
#SESSION::CheckLogin();

    //QUERIES
    if(Request::post()){
        switch(Request::get("action")) {
            case "add": 
			
			#var_dump(Request::post());
			
            USERS::addstudent(Request::post());
            
			
			#var_dump(Request::post());
            
            break;
            case "edit": USERS::update_student($_GET['id'],Request::post()); break;
            #case "update_blog":  BLOG::update(Request::post("blog_info")); break;
            
            #case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
            
            default: Fail::not_found();
        }
    }
    
    if(Request::get()){
        switch(Request::get("action")) {
            case "delete": ANNOUNCEMENT::delete($_GET['id']); break;
        
        }
    }       
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
      <title>SCHOOL MANAGEMENT SYSTEM</title>
    </head>

    <body>

        <!-- top nav -->
        <?php include(LAYOUTS . 'top_nav.php'); ?>
        <!-- end nav -->

        <div class="container">
            
            <!-- banner -->
            <?php include(LAYOUTS . "banner.php"); ?>
            <!-- end banner -->
            
          
            
            
            <!-- alert messages -->
            <?php SESSION::DisplayMsg(); ?>
            <!-- end of alert messages -->
            
            <!-- start row container -->
            <!-- 
            <div class="row">
                
                <div class="col-lg-12">

                
                    <ul class="breadcrumb">
                        <li class="active">Home</li>
                    </ul>
                

                </div>
                
            </div>
            -->
            <!-- end row container -->
            
   
             
            <!-- row container -->
            <div class="row">
			<!-- left nav -->

      	
                <div class="col-md-3">
					<?php include(PAGES."navigation.php");?>
                </div>
				
				<!-- end left nav -->
                
                <!-- BODY -->
                
                
                <div class="col-md-9">
                <?php if (SESSION::isLoggedIn()) { ?>
                    <?php
                    switch($_GET['action']){
                         case "view":
                    ?>
              </div>
          </div>
                  <?php  break;
						
						
						case "reports":
						?>
 
							<div class="panel panel-default">
								<div class="panel-heading">
									
								<?php
                                    if(isset($_GET['start']) && isset($_GET['end']))
                                                                        {
										$start = date("Y-m-d H:i:s",strtotime($_GET['start']));
										#$end = date("Y-m-d H:i:s",strtotime($_GET['end']));
										$end = $_GET['end'];
										$sem = $_GET['sem_ID'];
										$viewStudents =SUBJENROL::getenrollmentpersemperdate(array("date_enrolled"=>"ASC"),$sem,$start,$end);
										#var_dump($viewStudents);
										 
										 if($_GET['start'] == NULL)
										 {
										 $sem = $_GET['sem_ID'];
										 $viewStudents =SUBJENROL::getenrollmentpersem($sem);
										 }
										 
										}else{
										 #$viewStudents = NULL;
										 $sem = $_GET['sem_ID'];
										 $viewStudents =SUBJENROL::getenrollmentpersem($sem);
										}
										
										?>
                                    <form action="" method="GET">
                                    <table border="0" width="100%">
                                		<tr>
                                			<td width="" style="width:40%">
												<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-list"></i> Enrolment List</h3>											</td>

                                                <script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
												<script>
                                                webshims.setOptions('forms-ext', {types: 'date'});
                                                webshims.polyfill('forms forms-ext');
                                                </script>
                                              
                                                
                                                
                                          <td colspan="2">School Year/Semester
                                            <select name="sem_ID" id="sem_ID" onchange=" this.form.submit() " >
                                              <?php $e = SEM::getallsemsenrollment(array("sem_NAME"=>"DESC")); ?>
                                              <option value=""></option>
                                              <?php foreach($e as $key => $val){ ?>
                                              <option <?php if(isset($_GET['sem_ID'])) { if($_GET['sem_ID']==$val['sem_ID']){ echo "SELECTED=SELECTED "; }  } ?> value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
                                              <?php } ?>
                                            </select></td>  
                                          <td>&nbsp;</td>
                                             <input type="hidden" name="action" value="reports" />   
                                             
                                             
                                             
                                          <script>
											 
											 $(document).ready(function() {                                       
												$("#start").on("input", function() {
												  $("#end").val($(this).val());
												  alert($(this).val());
												})
											 }); ​
                                             </script>
                               			</tr>
                                		<tr>
                                		  <td style="width:40%">
                                          <?php
										   if($viewStudents) {
										   ?>
										   Displaying	<?= count($viewStudents); ?> Records
										   
										   <?php
										   }
										   ?>
                                          
                                          </td>
                                		  <td>Start:
                                		    <input id="start" type="date" value="<?php if(isset($_GET['start'])){ echo $_GET['start']; } ?>" name="start" /></td>
                                		  <td>End:
                                		    <input id="end" type="date" value="<?php if(isset($_GET['end'])){ echo $_GET['end']; } ?>" name="end" /></td>
                                		  <td><input type="submit" value="Go" /></td>
                               		  </tr>
                                	</table>
                                    
                                  </form>
							  </div>



								<div class="panel-body">
									<div id="display_result"></div>
                                	<div id="display_hide">
									<table class="table table-hover table-responsive table-striped text-md">
									  <tr>
                                         <th  style="width:3%; text-align:center">#</th>
                                          <th  style="width:10%;">Student No.</th>
                                         <th  style="width:25%;">Name</th>
                                        <th style="width:5%;">Course</th>
                                        <th style="width:15%;">Date</th>
                                        <?php
									
										
										 if($viewStudents) {
											$x=1;
											$total_amnt = 0;
											
									
									?>
                                   
									
									<?php
											
											foreach($viewStudents as $key => $value){
											
											$user = USERS::getSingle(array("si_ID"=>"ASC"),$value['si_ID']);
												?>
											<tr>
                                            	<td  align="center"><?= $x++; ?></td>
                                                <td><?php echo strtoupper($user['student_ID']); ?></td>
												<td>
													<a target="_blank" href="fin_enrolment2.php?sem_ID=<?= $_GET['sem_ID']; ?>&action=view&id=<?= $value['si_ID']; ?>" title="View Account"><?php echo $user['si_LNAME']; ?>, <?php echo $user['si_FNAME']; ?></a>
												<a href="student_information.php?action=edit&id=<?=$value['si_ID'];?>" target="_blank">[ADM]</a><a href="accounting.php?action=view&id=<?= $value['si_ID']; ?>" target="_blank">[ACC]</a><td><?php echo USERS::getCourse($value['si_ID']); ?>
											  <td><?php echo date("M-j-Y", strtotime($value['date_enrolled'])); ?></td>
                                              </tr>
											<?php
											 } 
										
										?> 
                                        
                                        
                                          <?php }else{ ?>
                                            <tr>
                                            	<td colspan="5" align="center"><b>No Record</b></td>
                                            <?php } ?>
                                        
                                       <tr>
											  <td align="center" style="border-top: #000000 solid thin"><b><?= --$x; ?></b></td>
											  <td>                                            
											  <td>                                            
											  <td>&nbsp;</td>
									  		</tr>
									</table> 

								</div>
							</div>
								<center>
									
								</center>
							</div>
                        </div>
                    </div>

					<?php
					
					break;
					default: href("?action=reports");
					
					
					}}
				  ?>
         
            
            <!-- end row container -->
            
            

            <!-- footer-->      
        
            <?php include (LAYOUTS . "footer.php"); ?>
            <!-- end footer -->
		</div>
        <!-- end container -->

    </body>
</html>
