<?php
	include('init.php');

SESSION::CheckLogin();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>schedule</title>
<style type="text/css">
<!--
.style9 {font-family: Arial, Helvetica, sans-serif}
.style10 {font-size: 12px}
.style11 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; }

-->
</style>
<script language="javascript" type="text/javascript">
<!--


// -->
</script>
</head>

<body>
<table width="644" border="0">
  <tr>
    <td width="178" valign="top">School Year - Semester</td>
    <td width="425" valign="">
	<?php
	$sem = SEM::getallsems(array("sem_ID"=>"DESC"));
	#print_r($sem);
	?>
      <form action="<?php echo $_SERVER['REQUEST_URI']; ?>#choose" method="get">
          <input type="hidden" name="action" value="choose" />
          <select name="sem_ID" id="sem_ID" onchange="this.form.submit()" style="width:100%">
          
           	<option></option>
            <?php foreach($sem as $key => $val){ ?>
            <option <?php if(isset($_GET['sem_ID'])) { if($_GET['sem_ID']==$val['sem_ID']){ echo "SELECTED=SELECTED "; }  } ?> value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?>
            
            <?php if($val['sem_STATUS'] == 1){ echo "(Active)"; }else{ echo "(Inactive)"; } ?>
            
            </option>
            <?php } ?>
            </optgroup>
          </select>	
      </form></td>
  
  </tr>
  <tr>
    <td valign="bottom">&nbsp;</td>
    <td colspan="1" valign="bottom">&nbsp;</td>
  </tr>
  <tr>
    <td valign="bottom">Course - Section:</td>
    <td valign="bottom">
	
	
	
	
	
        <form action="#choose" method="get">
       <?php if(isset($_GET['sem_ID'])){ ?>
          <input type="hidden" name="sem_ID" value="<?php echo clean($_GET['sem_ID']); ?>" />
       <?php } ?>
          <select name="semester_section_ID" id="semester_section_ID"  onchange="this.form.submit()" style="width:100%">
            <option></option>
        <?php if(isset($_GET['sem_ID'])){ ?>
        
        <?php
			$sem = SEMSECTION::getAllbyID(array("a.course_ID"=>"DESC","semester_section_NAME"=>"ASC"),$_GET['sem_ID']);
			#print_r($sem);
			?>
        
        
            <?php foreach($sem as $key => $val){ ?>
            <option <?php if(isset($_GET['semester_section_ID'])){ if($_GET['semester_section_ID'] == $val['semester_section_ID']){ echo " SELECTED=SELECTED " ; } } ?>  value="<?php echo $val['semester_section_ID']; ?>">
            <?php $sem = SEMSECTION::getID($val['semester_section_ID']); ?>
            <?php $d = SEMSECTION::getSEM($sem['sem_ID']);
							echo $d['sem_NAME']; ?>
              -
              <?php $c = COURSE::getbyID($val['course_ID']);
							echo $c['course_INIT']; ?>
              -<?php echo $val['semester_section_NAME']; ?></option>
            <?php } ?>
            
            
      <?php } ?>      
          </select>
          
          
          
      </form>
      
      </td>
   
  </tr>
</table>





<p>Subjects:</p>

<?php if(isset($_GET['semester_section_ID']))
{
$subjects = SUBJSCHED::getSubjectsBySection(clean($_GET['semester_section_ID']));

?>




<table width="615" border="1" cellpadding="3" cellspacing="0" class="table table-hover table-responsive table-striped text-md" >
				 
               
<tr>
           			  <th><div align="center" class="style6 style8 style9 style10">Code</div></th>
                                        <th ><div align="center" class="style11">Description</div></th>
                                       	<th><div align="center" class="style11">Room</div></th>
                                        <th><div align="center" class="style11">Day</div></th>
                                        <th><div align="center" class="style11">Time</div></th>
                                        <th><div align="center" class="style11">Rem</div></th>
                                        <th ><div align="center" class="style11">Taken</div></th>
                                        <th><div align="center" class="style11">Instructor</div></th>
                                        <th ><div align="center" class="style11">Options<br />
                      </div></th>
                   </tr>
            
               
            
                  <?php foreach($subjects as $key => $val) { ?>
                  <tr>
                  	<td style="width:80px"><div align="center" class="style11">
                  	  <?php 
                   			 $ss = SUBJSCHED::getSched($val['subject_sched_ID']);
							 $s = SUBJECTS::getID($ss['subject_ID']);
							 
							# print_r($s);
                             echo $s['subject_CODE'];
							 
							 							 
                   		 ?>                    
               	    </div></td>
                    <td s><div align="center" class="style11">
                      <?= $s['subject_DESCRIPTION']; ?> 
                    (<?php echo $s['LEC_UNIT']; ?>/<?php echo $s['LAB_UNIT']; ?>)</div></td>
                    <td ><div align="center" class="style11">
                      <?php  $room = ROOM::getID($val['room_ID']); echo $room['room_NAME']; ?>
                    </div></td>
                    <td ><div align="center" class="style11"><?php echo substr($val['day'],0,3); ?></div></td>
                    <td ><div align="center" class="style11"><?php echo $val['start']."-".$val['end']; ?></div></td>
                    <td><div align="center" class="style11"><?php echo ($val['max_student']-SUBJSCHED::checkRemaining($val['subject_sched_ID'])); ?>                    </div></td>
                    <td ><div align="center" class="style11">
                      <?= SUBJSCHED::checkRemaining($val['subject_sched_ID']); ?>
                    </div></td>
                    <td> <div align="center" class="style11"><?php
                    $ins = INSTRUCTORS::getSingle1($val['instructor_ID']);
					
					echo $ins['instructor_NAME'];
					?></div></td>
                    <td> <span class="style11"><a href="subject_sched.php?action=edit&id=<?php echo $val['subject_sched_ID']; ?>" target="_blank">Edit</a>
                    
                    
                        <a href="classlist.php?action=view&id=<?php echo $val['subject_sched_ID']; ?>" target="_blank">Class List</a> <a href="classlist2.php?action=view&amp;id=<?php echo $val['subject_sched_ID']; ?>" target="_blank"> List2</a> <a href="classlist3.php?action=view&amp;id=<?php echo $val['subject_sched_ID']; ?>" target="_blank"> List3</a>
						
						<a onclick="return popitup('gs.php?id=<?php echo $val['subject_sched_ID']; ?>')" href="#" >Grading Sheet</a>
						</span>
						</td>                    
  </tr>
                <?php } ?>
              </table>


<?php } ?>

<p>&nbsp;</p>
</body>
</html>
