<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Student Affairs / Dorm, DESCRIPTION: User visited Dorm";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}

	



if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
    

    
  
    
	if ($user) 
	{
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();
	
			  
	
			
				SESSION::login($user);
                msgbox("Login Success");
               
               
				AUDIT::insert($user['account_ID'],"LOGGED IN");
				
				
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}



	//QUERIES




	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  	<!-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->
      <title>HOME | School Management System v2.0</title>
	</head>

	<body>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
            
			<!-- alert messages -->
			<?php 
			
			SESSION::DisplayMsg(); 
			
			
			
			?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
				
				//audit
					?>
					
				<div class="col-md-3">
            		<?php include(PAGES."navigation.php");?>
				</div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 


								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>
								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

									</div>
								</div>
                                
                                	<div class="form-group row">
									<div class="col-lg-offset-9">
										 <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                                
                                </fieldset>
                    </form>
                    </div>	
                </div>
                
                <?php   }else{
				
				AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
				
				?>

<div class="col-md-3">

<?php include(PAGES."navigation.php");
  $CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR']; ?>
</div>
              
                
<div class="col-md-9">
	<div class="panel panel-default">

		<div class="panel-heading">
			<table>
        		<tr>
        			<td width="50%">
						<h4 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"> Dorm Management<i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
					</td>
				 	<td>
            			<div class="input-group col-md-12 hide">
              				
              				<input id="searchItem" class="searchItem form-control"  type="text" autofocus="autofocus"  style="text-transform: uppercase;"/>
                            
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            			</div>
        			</td>

					<td width="5%">
	    				<div class="col-md-1" style="padding-right: 3px">   
	    					<button id='btnDorm' class='btn btn-success P4C2_ADDNEWDORM btn-ms'>Add New Dorm</button>
	    				</div>

        			<td width="5%">
	    				<div class="col-md">   
	    					<button class="btn btn-success P4C2_EXPORTSASCSV btn-ms btnCSV" style="padding-left: 10px;" >Exports as CSV</button>
	    				</div>
	    			</td>
	    		</tr>
	    	</table>
		</div>
		<div class="panel-body" id="getaccountidforDorm" data-acct="<?php echo $user['account_ID']; ?>" data-acctip ="<?php echo $CLIENT_IP?>">
<!-- 			<div class="row">
            	<div class="form-group">
	                <div class="col-sm-5">
	                    <input name='' id="tblDorm" placeholder="Search by Room Number" class='form-control' type='text' required autofocus style="font-style: italic;"/>
	                </div>
            	</div>
           </div> -->
            <br><table border="0" cellspacing="5" cellpadding="5" style="width:40%;">
                <tbody>
                    <tr>
                        <td>
	                    <input name='' id="myInput" placeholder="Search by Room No., Total Capacity, Available Slot or Status " class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
                        </td>
                    </tr>
                </tbody>
            </table><br>
        	<table id="tblDorm1" class="table table-hover table-responsive table-striped text-md" style="width:100%;">
		        <thead>
		            <tr>
					  <th>Room Number</th>
		              <th>Total Capacity</th>
		              <th>Status</th>
		              <th>Actions</th>
		            </tr>
		        </thead>
		    </table>

			<div class="modal fade" id="modalAdd" role="dialog">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="" style="padding:40px 40px 0px 40px; text-align: center;">
					  <button type="button" class="close" data-dismiss="modal">&times;</button>
					  <h4 style="">Add Student Tool</h4>
					  <p>Select Student in designated dorm areas</p>
					</div>
		        <div class="modal-body" style="padding:40px 90px;">
						
						  <div class="success alert-success hidden" role="alert" style="text-align: center;"></div>
						<div class="alert alert-danger hidden" role="alert" style="text-align: center;">				
						  </div>

						<div class="form-group">

						<select class="form-control" id="SemofStudent" required autofocus style="font-style: italic; margin-bottom: 10px; text-align: center">
                        <option></option>

                    </select>
	<!-- 					<input name='' placeholder="STUDENT NAME" class='form-control' type='text' required autofocus style="font-style: italic; margin-bottom: 10px; text-align: center"/> -->
					</div>
						<button type="submit" class="btn btn-success btn-block" id="saveStudentBtn"> Save</button>
				    </div>
						  </div> 
					 </div>
			</div>


		  <div class="modal fade" id="modalDorm" role="dialog">
		    <div class="modal-dialog">
		      <div class="modal-content">
		        <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 style="">Add New Dorm Tool</h4>
				  <p>Create new dorm by allocating new spaces</p>
		        </div>
		        <div class="modal-body" style="padding:40px 90px;">
		        	<div class="success alert-success hidden" role="alert" style="text-align: center;"></div>
		        	<div class="alert alert-danger hidden" role="alert" style="text-align: center;"></div>
		            <div class="form-group">
					<input name='' id="dormNumber" placeholder="Dorm Number" class='form-control' type='number' min="0" required autofocus style="font-style: italic; margin-bottom: 10px; text-align: center"/>
					<input name='' id="dormCap" placeholder="Capacity" class='form-control' type='number' min="0" required autofocus style="font-style: italic; text-align: center"/>
					</div>

					<button type="submit" class="btn btn-success btn-block" id="saveDormBtn"> Save</button>
	  			</div>
		  			
		    
		        </div>
		      </div>
			</div>
	

		<div class="modal fade" id="modalUpdate" role="dialog">
		    <div class="modal-dialog">
		      <div class="modal-content">
		        <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 style="">Remove Student</h4>
		        </div>
		        <div class="modal-body" style="padding:40px 90px;">
		    		<div class="success alert-success hidden" role="alert" style="text-align: center;"></div>
		    		<div class="alert alert-danger hidden" role="alert" style="text-align: center;"></div>

		    	<div class="wrapper">
<!-- 		    		<div class="form-group">


				<div class="container">
 					<div class="col-lg-13">
  					   	 <div class="iin_ input-group">
						<input name='' id="dormNumber" placeholder="KEVIN, SAN JOSE" class='form-control inputstl' type='label' required autofocus style="font-style: italic; text-align: center"/>
						<label class= "test_"></label>
						<span class="input-group-addon frspan">
       						 <input type="checkbox" aria-label="123" class="chboxvalue_" >
      					</span>
						</div>


					</div>
				</div>
				</div> -->

				</div>	
		          

<!-- 
				<div class="container">
 					<div class="col-lg-13">
  					   	 <div class="input-group">

						<input name='' id="dormNumber" placeholder="Cadet Name" class='form-control inputstl' type='text' required autofocus style="font-style: italic; text-align: center"/>
						<span class="input-group-addon frspan">
       						 <input type="checkbox" aria-label="123">
      					</span>
						</div>
					</div>
        </div> -->
        <div>
						<button type="submit" class="btn btn-success btn-block" style="margin-top: 15px" id="saveBtn"> Remove</button>
             </div>
          </div>	
			</div>
		</div>
    </div>
    
    	</div>
		</div>
</div>
  </div>
<?php } ?>
  	
			
            <?php 
			?>
			<?php include (LAYOUTS . "footer.php"); ?>
			<script src="<?php echo HOME; ?>old/assets/js/custom/dorm.js"></script>
		
  <script type="text/javascript">
  	$( window ).ready(function(){
        setTimeout(function() {

      		$.main.dorm.dormManagement();
      		$.main.dorm.AddDorm();
      		$.main.dorm.getStudentsWithoutDorm();
      		$.main.dorm.saveStudenttoDorm();
      		$.main.dorm.removeStudent();
          
          
        },100);
  	})
  	function myFunction() {
      	var input, filter, table, tr, td, i;
          input = document.getElementById("myInput");
          console.log(input);
          filter = input.value.toUpperCase();
          table = document.getElementById("tblDorm1");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td2 = tr[i].getElementsByTagName("td")[1];
            td1 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            if (td) {
              if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }   
      	}
    }

  </script>
<script type="text/javascript">
      		
    	 $(".btnAdd").on('click',function(){
            $("#modalAdd").modal('show')
        })
        
        $(".btnUpdate").on('click',function(){
            $("#modalDorm").modal('show')
		})
		

</script>
	</body>
</html>
<style type="text/css">
	.dataTables_filter, .dataTables_info { display: none; }
</style>