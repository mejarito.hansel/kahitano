<?php
	include('init.php');

if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}

if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
    
    
	if ($user) 
	{
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();
	
			
				SESSION::login($user);
                msgbox("Login Success");
               
               
				AUDIT::insert($user['account_ID'],"LOGGED IN");
				
				
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
      <title>LOGIN | <?php echo $site_options['site_NAME']; ?></title>
	</head>

	<body>

		<!-- Navbar -->	
		<?php include(LAYOUTS . "top_nav.php"); ?>
		<!--END Navbar -->

		<div class="container">

		<?php
		include(LAYOUTS . "banner.php");
		?>

			<!-- start body -->

			<!-- start row -->
			<div class="row">
				<div class="col-lg-12">
					<!-- breadcrumbs -->
					<ul class="breadcrumb">
						<li class=""><a href="<?php echo HOME; ?>index.php">Home</a></li>
						<li class="active">Login</li>
					</ul>
					<!-- end breadcrumbs -->  
				</div>
			</div>
			<!-- end row -->

			<!-- start row -->
			<div class="row">
				<div class="col-md-2"></div>

				<div class="col-md-8">


					<div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 


								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>
								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

									</div>
								</div>
                                
                                	<div class="form-group row">
									<div class="col-lg-offset-9">
										 <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                                
                                </fieldset>
                    </form>
                    </div>
					
				</div>
				
			</div>
			<!-- end row -->

			<!-- end body -->

			<div class="spacer-short"></div>

			<!-- footer-->
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- container end -->

	</body>
</html>