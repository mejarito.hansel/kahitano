<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<?php
#$site_options = navCat::site_options();
?>

<link rel="stylesheet" type="text/css" href="<?php echo HOME; ?>old/assets/css/clndr.css" />
<link rel="stylesheet" href="<?php echo HOME; ?>old/assets/css/bootstrap.css" media="screen">
<link rel="stylesheet" href="<?php echo HOME; ?>old/assets/font-awesome/css/font-awesome.css">
<link rel="stylesheet" href="<?php echo HOME; ?>old/assets/css/bootswatch.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo HOME; ?>old/assets/css/jquery.datetimepicker.css" />
<link rel="stylesheet" href="<?php echo HOME; ?>old/assets/css/misc.css">


<link rel="shortcut icon" href="<?php echo IMAGES; ?>/site_icon/logo.png">
<!--[if lt IE 9]>
<script src="old/assets/js/html5shiv.js"></script>
<script src="old/assets/js/respond.HOME.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<?php echo HOME; ?>old/assets/css/ui-lightness/jquery-ui-1.10.3.custom.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo HOME; ?>old/assets/css/jquery-ui.css" />
<link rel="stylesheet" type="text/css" href="<?php echo HOME; ?>old/assets/css/jquery.dataTables.min.css" />

<link rel="stylesheet" type="text/css" href="<?php echo HOME; ?>old/assets/css/spectrum.css" />
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />

<style>
	.navbar-default .navbar-collapse,
	.navbar-default .navbar-form {
		border-color:none;
	}

	.navbar{
		background-image: none;
		border-bottom: none;
	}

	.navbar-default
	{

	}

	.sidenav {
		height: 100%;
		width: 100%;
		z-index: 1;
		overflow-x: hidden;
	}

	.sidenav a, .dropdown-btn {
		padding: 10px 8px 10px 23px;
		text-decoration: none;
		font-size: 15px;
		color: #0355C3;
		display: block;
		border: none;
		background: none;
		width: 100%;
		text-align: left;
		cursor: pointer;
		outline: none;
	}

	.sidenav a:hover, .dropdown-btn:hover {
		color: #0355C3;
		/*background: #fafafa;*/
	}

	.main {
		margin-left: 200px;
		font-size: 20px;
		padding: 0px 10px;
	}

	.active {
		background: #fafafa;
		color: #0355C3;
	}

	.dropdown-container {
		display: none;
		/*padding-left: 8px;*/
	}

	.fa-chevron-right {
		font-size:10px !important;
		color: #0355C3;
		float: right;
		margin-top: 4px;
		padding-right: 8px;
	}
	.fa-chevron-down {
		font-size:10px !important;
		color: #0355C3;
		float: right;
		margin-top: 4px;
		padding-right: 8px;
	}
</style>
