<!-- Analytics -->
<!-- Navbar -->	

<div class="navbar navbar-fixed-top" style="background-color: #ffffff; box-shadow: 0px 1px 5px #adadad!important"> <!-- navbar-default -->
	<div class="container">
        <div class="navbar-header">
           <a href="<?php echo HOME; ?>old/index.php"><img name="logo" src="<?php echo HOME; ?>assets/images/logo.png" border="0" id="logo" alt=""/></a>
			<!-- <a href="<?php echo HOME; ?>index.php" class="navbar-brand">School Management System v2.0</a> -->
            	<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar-main">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main" style="height: 1px;">
			<ul class="nav navbar-nav">

				<?php
				if (SESSION::isLoggedIn()) {
					?>
			<!--
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">My Courses <span class="caret"></span></a>
						<ul class="dropdown-menu" >
						</ul>
					</li>
                    
                -->   

				<?php }else{
				
				} ?>
					<!--
                    <li>
						<form class="navbar-form navbar-left">
							<input class="form-control col-lg-8" type="text" placeholder="Search"/>
						</form>
					</li>
                    -->
			</ul>

			<ul class="nav navbar-nav navbar-right">

				

					<li></li>
					<!--<li><a href="<?php echo HOME; ?>login.php" target="" >You are not logged in. (Login)</a></li>-->
                    <li id="loginB" class="dropdown" style="margin-top: 10px">
                    
                      <?php
				if (SESSION::isLoggedIn()) {
					?>

                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" >
							<?= $user['account_FNAME']." ".$user['account_MNAME']." ".$user['account_LNAME']." (".$user['access_NAME'].") ";   ?>  <span class="caret"></span>
                             </a>
                            
                            <ul class="dropdown-menu">
							
                            <li><a href="profile.php?action=change_password" ><i class="fa fa-key"></i> Change Password</a></li>
                            
                            	<li class="divider"></li>
                            <li><a onclick="return confirm('Do you really want to logout?');" href="logout.php" ><i class="fa fa-power-off"></i> Logout</a></li>
                            <!-- <li><a id="btnLogout" href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-power-off"></i> Logout</a></li> -->
							</ul>
                            
                       
                        
                    <?php }else { ?>
                        
                        
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" >You are not logged in. (Login) <span class="caret"></span></a>
                    	
						<ul class="dropdown-menu">
                        <form class="form-horizontal" method="POST" action="<?php echo HOME; ?>login.php">
                        	<li class="li-login">Username:<br />
                            		<input type="text" class="form-control" data-stopPropagation="true"  id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
                            </li>
                            <li class="li-login">Password:<br />
                            <input type="password" class="form-control"  data-stopPropagation="true" id="inputPassword" name="password" placeholder="Password" required></li>
                             <li class="li-login" >
                            	<div align="right">
                                  <button type="button" onclick="parent.location='contact.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                
                                <button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button></div>
                            </li>
                             </form>
                        </ul>
                       
                        <?php } ?>
                    </li>
                   
                   
					<li><img src="<?php echo IMAGES; ?>f1.png" height="70" title="Guest"/></li>
                      
			

			</ul>


        </div>  
	</div>
</div>
<script>


$( document ).ready(function() {
	$(function() {
	    $("ul.dropdown-menu").on("click", "[data-stopPropagation]", function(e) {
	        e.stopPropagation();
	    });
	});	
});


</script>
<!--END Navbar -->
<?php
#$cur_layout = navCat::cur_layout();
#print_r($cur_layout);


#$security_option = navCat::security_option();
#print_r($security_option);
?>
<br />