<?php
  if (!SESSION::isLoggedIn()) {
?>

<script type="text/javascript">
  window.location = '../index.php';
</script>
                    
<?php }else{ 
  $acid = $_SESSION['USER']['access_ID'];
?>
  
<div class="panel panel-default">
  <div class="panel-heading"> 
    <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px;">
      Navigation
    </h4>
  </div>
  <div class="panel-body" style="padding:0px;">
    <div class="sidenav">
        <a class="moduleLink" href="index.php" style="padding: 10px 8px 10px 23px !important;">Home</a>
    </div>
  </div>
</div>

<script type="text/javascript">
$(function(){
    // $.cookie("access_ID","<?= $_SESSION['USER']['access_ID']; ?>");
    var payload = { "access_ID": <?= $_SESSION['USER']['access_ID']; ?> }
    var host = window.location.protocol+"//"+window.location.hostname ;

    $.ajax({
      method: "POST",
       url: host+"/api/index.php/Ams_controller/fetAllUserAccountLevelControlByAccessID",
      // url: "http://sms.massiveits.com/api/index.php/Ams_controller/fetAllUserAccountLevelControlByAccessID",
      // url: "http://localhost/student-portal-backend/index.php/Ams_controller/fetAllUserAccountLevelControlByAccessID",
      dataType: "json",
      data: JSON.stringify(payload)
    }).done(function (result, textStatus, jqXHR) {
      for(i=0;i<result.payload.length;i++){
        if(result.payload[i].status === "1"){
          $(".sidenav").append("\
          <span>\
          <button class='dropdown-btn'>"+result.payload[i].USER_TYPE_DESCRIPTION+"\
          <i class='fa fa-chevron-right'></i>\
          </button>\
          <div id='module_"+result.payload[i].USER_TYPE_ID+"' class='dropdown-container'>\
          </div>\
          </span>\
          ");
          // $(".sidenav").append("<a href='#"+result.payload[i].USER_TYPE_DESCRIPTION+"'>"+result.payload[i].USER_TYPE_DESCRIPTION+"</a>")
        }
        var parent_ID = $.cookie("parent_ID",i+1)
      }
      $(".sidenav span:nth-of-type(7)").append("<span><a href='audit_student_portal.php'>Student's Portal</a></span>");

      var payload = {
        "access_ID": <?= $_SESSION['USER']['access_ID']; ?>
      }
      $.ajax({
      method: "POST",

      url: host+"/api/index.php/Ams_controller/fetchAllUserModuleByUserTypeID",
      // url: "http://sms.massiveits.com/api/index.php/Ams_controller/fetchAllUserModuleByUserTypeID",
      // url: "http://localhost/student-portal-backend/index.php/Ams_controller/fetchAllUserModuleByUserTypeID",

      dataType: "json",
      data: JSON.stringify(payload)
    }).done(function (result, textStatus, jqXHR) {
      for(z=0;z<result.payload.length;z++){
        if(result.payload[z].status === "1"){
          $("#module_"+result.payload[z].USER_TYPE_ID+"").append("<a class='moduleLink' href='"+result.payload[z].USER_ACCOUNT_MODULE_HREF+"' data-href='"+result.payload[z].USER_ACCOUNT_MODULE_HREF.split("?action").splice(0, 1).join("").replace(/http:\/\/ams.local\/old\//g,'').replace(/\?/g,'')+"'>"+result.payload[z].USER_ACCOUNT_MODULE+"</a>")
        }
      }
      var dropdown = document.getElementsByClassName("dropdown-btn");
      var i;

      for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function() {
          this.classList.toggle("active");
          var dropdownContent = this.nextElementSibling;
          if (dropdownContent.style.display === "block") {
          dropdownContent.style.display = "none";
          $(this).find(".fa").removeClass("fa-chevron-down").addClass("fa-chevron-right");
          } else {;
          // dropdownContent.style.display = "block";
          $(dropdownContent).slideToggle("fast");
          $(this).find(".fa").removeClass("fa-chevron-right").addClass("fa-chevron-down");
          }
        });
      }

      $("#module_2").find('a.moduleLink').eq(2).attr("data-href","sectioning.php")
      $("#module_2").find('a.moduleLink').eq(11).attr("data-href","faculty_loading.php")
      $("#module_2").find('a.moduleLink').eq(17).attr("data-href","fin_enrolment2.php")
      // $("#module_3").find('a.moduleLink').eq(0).attr("data-href","fin_enrolment2.php")
      $("#module_3").find('a.moduleLink').eq(2).attr("data-href","faculty_loading.php")
      $("#module_3").find('a.moduleLink').eq(3).attr("data-href","sectioning.php")
      $("#module_3").find('a.moduleLink').eq(4).attr("data-href","subject_sched.php")
      $("#module_3").find('a.moduleLink').eq(5).attr("data-href","section_status_listing.php")
      $("#module_4").find('a.moduleLink').eq(4).attr("data-href","curricullum.php")
      $("#module_4").find('a.moduleLink').eq(9).attr("data-href","sectioning.php")
      $("#module_4").find('a.moduleLink').eq(11).attr("data-href","faculty_loading.php")
      $("#module_4").find('a.moduleLink').eq(12).attr("data-href","clearance.php")
      $("#module_5").find('a.moduleLink').eq(0).attr("data-href","accounting.php")
      $("#module_5").find('a.moduleLink').eq(2).attr("data-href","promi_list.php")
      $("#module_5").find('a.moduleLink').eq(4).attr("data-href","particulars.php")
      $("#module_5").find('a.moduleLink').eq(5).attr("data-href","acct_transaction_logs1.php")
      $("#module_7").find('a.moduleLink').eq(1).attr("data-href","clearance.php")
      setTimeout(function(){ 
        
        for(z=0;z<$(".moduleLink").length;z++){
        if($(".moduleLink").eq(z).attr("data-href") === window.location.href.split("?action").splice(0, 1).join("").replace(/http:\/\/ams.local\/old\//g,'')){
          $(".moduleLink").eq(z).addClass("active");
          $(".moduleLink").eq(z).closest("div").css("display","block")
          $(".moduleLink").eq(z).closest("div span").find("button").addClass("active")
        }

        if($(".moduleLink").eq(z).attr("data-href") === window.location.href.split("?action").splice(0, 1).join("").replace(/http:\/\/sms.massiveits.com\/old\//g,'')){
          $(".moduleLink").eq(z).addClass("active");
          $(".moduleLink").eq(z).closest("div").css("display","block")
          $(".moduleLink").eq(z).closest("div span").find("button").addClass("active")
        }
        // console.log(window.location.href.split("?action").splice(0, 1).join("").replace(/http:\/\/ams.local\/old\//g,'').replace(/\?/g,''))
        // console.log(window.location.href.split("?action").splice(0, 1).join("").replace(/http:\/\/sms.massiveits.com\/old\//g,'').replace(/\?/g,''))
        $(".dropdown-btn.active").find("i").removeClass("fa-chevron-right").addClass("fa fa-chevron-down");
      }
      }, 300);
  

      
    }).fail(function (jqXHR, textStatus, errorThrown,request) {
        // console.log(JSON.stringify(jqXHR))
        // console.log(JSON.stringify(textStatus))
        // console.log(JSON.stringify(errorThrown))
        // console.log(JSON.stringify(request))
    });

    }).fail(function (jqXHR, textStatus, errorThrown,request) {
        // console.log(JSON.stringify(jqXHR))
        // console.log(JSON.stringify(textStatus))
        // console.log(JSON.stringify(errorThrown))
        // console.log(JSON.stringify(request))
    });
});
</script>

<!-- End -->
<?php   } ?>    