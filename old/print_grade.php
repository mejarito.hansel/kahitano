<?php
	include('init.php');
$id = clean($_GET['si_ID']);
				$info =  USERS::viewSingleStudent($_GET['si_ID']);
				#var_dump($info);
				$sem_ID = clean($_GET['sem_ID']);
				$mygrade = SUBJENROL::gradebyid($id,$sem_ID);
				$sem = SEM::getSingleSem($sem_ID);
				
				#print_r($mygrade);
        $user_id = $user['account_ID'];
        $action_event = "Print";
        $event_desc = "MODULE: Registrar / Grade Slip, DESCRIPTION: User Printed the Grade of ".$info['si_LNAME'].", ".$info['si_FNAME']." ".$info['si_MNAME'];
        $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<title>Grade Slip</title>
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
      <title>Student Information | School Management System v2.0</title>
      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
<link rel="stylesheet" href="<?php echo HOME; ?>old/assets/css/bootstrap.css">

<style>

body{
	width: 8.5in;
	height: 5.5in;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
    }

.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style3 {
	font-size: 18px;
	font-weight: bold;
}
.style4 {
	font-size: 14px;
	font-weight: bold;
}
.style5 {font-size: 10px}
.style7 {font-size: 16px; font-weight: bold; }
</style>


</head>

<body onload="window.print();">
<!-- <body> -->


<table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" >
      <tr>
       <div align="center"><img src="<?php echo HOME; ?>assets/images/logo.png" alt="" class="img-responsive"/></div></td>
       <div align="center"><span class="style1">
          <span class="style5">Tel: (046) 454-2187 | Website: www.mmma.com.ph</span></span></div></td>
      </tr>
    </table>


    <center><h3>Grade Slip</h3></center>

<div class="container text-capitalize" style="margin-bottom: 15px; margin-top: 15px">
    <table class="table table-responsive">
      <tr>
        <td>Student No:</td>
        <td><?php echo strtoupper(strtoupper($info['student_ID'])); ?></td>
        <td>Academic Year & Sem:</td>
        <td><?= $sem['sem_NAME']; ?></td>
      </tr>
      <tr>
        <td>Student Name:</td>
        <td><?php echo strtoupper($info['si_LNAME']).", ".strtoupper($info['si_FNAME'])." ".strtoupper($info['si_MNAME']);  ?></td>
        <td>Program:</td>
        <td><?php echo USERS::getCourse($info['si_ID']) ?></td>
      </tr>
    </table>


    
    <div class="row">
      <div class="col-lg-12">
        <table class="table table-bordered">
            <thead class="">

              <tr>
                <th>Course</th>
                <th>Description</th>
                <th>Units</th>
                <th>Grade</th>
                <th>Remarks</th>
                <th>Teacher</th>
              </tr>
            </thead>
            <tbody>

               <?php foreach($mygrade as $key => $val) { ?>
               
               <?php if($val['status'] == 1){ ?>    
               <tr>
                  <td>
                    <?php
                    $ss = SUBJSCHED::getSched($val['subject_sched_ID']);
                    $s = SUBJECTS::getID($ss['subject_ID']);
                   echo $s['subject_CODE'];
                   $units = $s['TOTAL_UNIT'];
                    ?>
                  </td>
                  <td><?php echo $s['subject_DESCRIPTION']; ?></td>
                  <td><?php printf("%.2f",$units); ?></td>
                  <td>
                    
                    <?php
                    if($ss['is_check'] == 1){
                     if($val['grade'] == NULL){ echo "NG"; }else{ 
                      if(is_numeric($val['grade'])){
                       echo number_format((float)$val['grade'], 2, '.', '');
                      }else{
                        echo $val['grade'];
                      }
                      


                     } 
                     }else{
                      echo "NG";
                     }?>


                  </td>
                  <td class=""><div align=""><?php 
             if($ss['is_check'] == 1){
                        switch($val['grade'])
                        {
                          case "1.00":
                          case "1.25":
                          case "1.50":
                          case "1.75":
                          case "2.00":
                          case "2.25":
                          case "2.50":
                          case "2.75":
                          case "3.00": echo "PASSED";
                          break;
                          case "5.00": echo "FAILED";
                          break;
                          case "INC": echo "INCOMPLETE";
                          break;
                          case "DRP": echo "DROPPED";
                          break;
                            default:
                              if($val['grade'] >= 100 || 60 <= $val['grade']){
                                echo "PASSED";
                              }elseif($val['grade'] >= 59 || 1 <= $val['grade']){
                                echo "FAILED";
                              }else{
                                echo "NO GRADE";
                              }
                        }
            }else{
              echo "NO GRADE";
            }           
                       ?></div></td>
              <td><?php  $ins = INSTRUCTORS::getSingle1($ss['instructor_ID']); echo $ins['instructor_NAME']." ".$ins['instructor_LNAME'];  ?></td>
                  <?php #print_r($ss); ?>
               </tr>

               <?php } ?>
               <?php } ?>


             <!--  <tr>
                <td>NGEC1</td>
                <td>Understanding Self </td>
                <td>3</td>
                <td>1</td>
                <td>1st Semester 2017-2018</td>
              </tr>
              <tr>
                <td>NGEC6</td>
                <td>Art Appreciation</td>
                <td>3</td>
                <td>1</td>
                <td>1st Semester 2017-2018</td>
              </tr>
              <tr>
                <td>GEM1</td>
                <td>Filipino 1</td>
                <td>3</td>
                <td>1.5</td>
                <td>1st Semester 2017-2018</td>
              </tr>
              <tr>
                <td>GEM3</td>
                <td>The Life and Works of Dr. Jose Rizal</td>
                <td>3</td>
                <td>1</td>
                <td>1st Semester 2017-2018</td>
              </tr> -->
            </tbody>
          </table>
      </div>
    </div>
  </div>
<div class="" style="margin-left: 20px">
Released By:            <br />
             <br />
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $user['account_FNAME']." ".substr($user['account_MNAME'],0,1).". ".$user['account_LNAME']; ?></b>
          <br />
          &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Admin Staff<br/>
<br/>            Released Date: <?php echo date("M j, Y"); ?>

</div>

<table width="100%" border="0" cellspacing="0" cellpadding="5" style="display:none;">
  <tr>
    <td height="85" style=" border-right-color:#000000; border-right-width:thin; border-right-style:dashed"><table width="90%" border="0" align="center" cellpadding="3" cellspacing="3" >
      <tr>
        <td  ><div align="center"><!-- <img src="<?php echo HOME ?>assets/images/site_logo.png" alt="" width="75" height="75" /> --></div></td>
       <td  ><div align="center"><span class="style1"><span class="style7">Massive Integrated Tech Solutions Inc.</span><br />
              <span class="style4">School Systems</span><br />
        </span><span class="style1"><span class="style5">Tel: 02-448-6511 Website: www.massiveits.com</span></span></div></td>
      </tr>

    </table></td>
    <td style=" border-left-color:#000000; border-left-width:thin; border-left-style:dashed"><table width="90%" border="0" align="center" cellpadding="3" cellspacing="3" >
      <tr>
        <td  ><div align="center"><!-- <img src="<?php echo HOME ?>assets/images/site_logo.png" alt="" width="75" height="75" /> --></div></td>
        <td  ><div align="center"><span class="style1"><span class="style7">Massive Integrated Tech Solutions Inc.</span><br />
              <span class="style4">School Systems</span><br />
        </span><span class="style1"><span class="style5">Tel: 02-448-6511 Website: www.massiveits.com</span></span></div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="50%"  style=" border-right-color:#000000; border-right-width:thin; border-right-style:dashed"><center><b>Grade Slip</b></center><br />
    <table width="100%" border="0" cellspacing="0" cellpadding="1">
        <tr>
          <td width="32%" class="style1">Name:</td>
          <td colspan="3" class="style1"><?php echo strtoupper($info['si_LNAME']).", ".strtoupper($info['si_FNAME'])." ".strtoupper($info['si_MNAME']);  ?></td>
        </tr>
        <tr>
          <td class="style1">Student No:</td>
          <td colspan="3" class="style1"><?php echo strtoupper(strtoupper($info['student_ID'])); ?></td>
        </tr>
        <tr>
          <td class="style1">Course:</td>
          <td colspan="3" class="style1"><?php echo USERS::getCourse($info['si_ID']) ?></td>
        </tr>
        <tr>
          <td class="style1">School Yr/Sem:</td>
          <td colspan="3" class="style1"><?= $sem['sem_NAME']; ?></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td width="18%">&nbsp;</td>
          <td width="19%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr>
          <td class="style4"><div align="center">Subject</div></td>
          <td class="style4"><div align="center">Units</div></td>
          <td class="style4"><div align="center">Grade</div></td>
          <td class="style4"><div align="center">Remarks</div></td>
        </tr>
          <?php foreach($mygrade as $key => $val) { ?>
               
               <?php if($val['status'] == 1){ ?>       
        
        
                    <tr >
                      <td class="style1"><div align="center"><?php
                            $ss = SUBJSCHED::getSched($val['subject_sched_ID']);
							                $s = SUBJECTS::getID($ss['subject_ID']);
                             echo $s['subject_CODE'];
							               $units = $s['TOTAL_UNIT'];
                            ?>                      
                            
                      </div></td></div></td>
                      <td class="style1"><div align="center"><?php printf("%.2f",$units); ?></div></td>
                      <td class="style1"><div align="center"><?php
					   if($ss['is_check'] == 1){
					   if($val['grade'] == NULL){ echo "NG"; }else{ 
							if(is_numeric($val['grade'])){
							 echo number_format((float)$val['grade'], 2, '.', '');
							}else{
								echo $val['grade'];
							}
							


					   } 
					   }else{
					   	echo "NG";
					   }?></div></td>
                      <td class="style1"><div align="center"><?php 
					   if($ss['is_check'] == 1){
											 	switch($val['grade'])
												{
													case "1.00":
													case "1.25":
													case "1.50":
													case "1.75":
													case "2.00":
													case "2.25":
													case "2.50":
													case "2.75":
													case "3.00": echo "PASSED";
													break;
													case "5.00": echo "FAILED";
													break;
													case "INC": echo "INCOMPLETE";
													break;
													case "DRP": echo "DROPPED";
													break;
													default: echo "NO GRADE";
												}
						}else{
							echo "NO GRADE";
						}						
											 ?></div></td>
                    </tr>
        
      		  <?php } ?>
        
        <?php } ?>
        
        <tr>
        	<td colspan="4">&nbsp;         </td>
        <tr>
       	   <td colspan="3" class="style1">Released By:            <br />
       	     <br />
   	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $user['account_FNAME']." ".substr($user['account_MNAME'],0,1).". ".$user['account_LNAME']; ?></b>
          <br />
          &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Admin Staff</td>   
            <td valign="top" class="style1">Released Date: <br />
              <br />
               &nbsp;&nbsp;&nbsp;<?php echo date("M j, Y"); ?></td>
        <tr>
          <td colspan="2" class="style1"><blockquote>&nbsp;</blockquote></td>
          <td colspan="2" class="style1"><div align="right"><em>Registrar's Office Copy</em></div></td>
    </table>    </td>
    <td  style=" border-left-color:#000000; border-left-width:thin; border-left-style:dashed"><center>
      <b>Grade Slip</b>
    </center>
      <br />
      <table width="100%" border="0" cellspacing="0" cellpadding="1">
        <tr>
          <td width="32%" class="style1">Name:</td>
          <td colspan="3" class="style1"><?php echo strtoupper($info['si_LNAME']).", ".strtoupper($info['si_FNAME'])." ".strtoupper($info['si_MNAME']);  ?></td>
        </tr>
        <tr>
          <td class="style1">Student No:</td>
          <td colspan="3" class="style1"><?php echo strtoupper(strtoupper($info['student_ID'])); ?></td>
        </tr>
        <tr>
          <td class="style1">Course:</td>
          <td colspan="3" class="style1"><?php echo USERS::getCourse($info['si_ID']) ?></td>
        </tr>
        <tr>
          <td class="style1">School Yr/Sem:</td><h1></h1>
          <td colspan="3" class="style1"><?= $sem['sem_NAME']; ?></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td width="18%">&nbsp;</td>
          <td width="19%">&nbsp;</td>
          <td width="31%">&nbsp;</td>
        </tr>
        <tr>
          <td class="style4"><div align="center">Subject</div></td>
          <td class="style4"><div align="center">Units</div></td>
          <td class="style4"><div align="center">Grade</div></td>
          <td class="style4"><div align="center">Remarks</div></td>
        </tr>
        <?php foreach($mygrade as $key => $val) { ?>
        <?php if($val['status'] == 1){ ?>
        <tr >
          <td class="style1"><div align="center">
            <?php
                            $ss = SUBJSCHED::getSched($val['subject_sched_ID']);
							 $s = SUBJECTS::getID($ss['subject_ID']);
                             echo $s['subject_CODE'];
							 $units = $s['TOTAL_UNIT'];
                            ?>
          </div></td>
          <td class="style1"><div align="center"><?php printf("%.2f",$units); ?></div></td>
            <td class="style1"><div align="center"><?php  if($ss['is_check'] == 1){ if($val['grade'] == NULL){ echo "NG"; }else{ 
			
			
					if(is_numeric($val['grade'])){
							 echo number_format((float)$val['grade'], 2, '.', '');
							}else{
								echo $val['grade'];
							}
							



			} }else{ echo "NG"; } ?></div></td>
                     <td class="style1"><div align="center">
            <?php  if($ss['is_check'] == 1){
											 	switch($val['grade'])
												{
													case "1.00":
													case "1.25":
													case "1.50":
													case "1.75":
													case "2.00":
													case "2.25":
													case "2.50":
													case "2.75":
													case "3.00": echo "PASSED";
													break;
													case "5.00": echo "FAILED";
													break;
													case "INC": echo "INCOMPLETE";
													break;
													case "DRP": echo "DROPPED";
													break;
													default: echo "NO GRADE";
												}
						}else{
						echo "NO GRADE";
						}					 ?>
          </div></td>
        </tr>
        <?php } ?>
        <?php } ?>
        <tr>
          <td colspan="4">&nbsp;</td>
        </tr>
        <tr>
       	   <td colspan="3" class="style1">Released By:            <br />
       	     <br />
   	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $user['account_FNAME']." ".substr($user['account_MNAME'],0,1).". ".$user['account_LNAME']; ?></b>
          <br />
          &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Admin Staff</td>   
            <td valign="top" class="style1">Released Date: <br />
              <br />
               &nbsp;&nbsp;&nbsp;<?php echo date("M j, Y"); ?></td>
        <tr>
          <td colspan="2" class="style1"><blockquote>&nbsp;</blockquote></td>
          <td colspan="2" class="style1"><div align="right"><em>Student's Copy</em></div></td>
        </tr>
    </table></td>
  </tr>
</table>

</body>
  <script src="pages\htmlfolder\assets/js/bootstrap.min.js"></script>
</html>
