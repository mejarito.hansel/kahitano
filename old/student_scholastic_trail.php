<?php
include('init.php');
$siiid = clean($_GET['id']);
$getsingle = USERS::getSingle(array("si_ID"=>"DESC"),$siiid  );
$getsingle1 = USERS::getSingleSAI(array("si_ID"=>"DESC"), $siiid );

$user_id = $user['account_ID'];
$action_event = "Print";
$event_desc = "Module: 2 Registrar / 2.2 Find Student, DESCRIPTION: User clicked Print Scholastic Trail for ".$getsingle['si_FNAME'].' '.$getsingle['si_MNAME'].' '.$getsingle['si_LNAME'];
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/jquery.dataTables.min.css">
    <!-- <link href="ams\old\pages\htmlfolder\assets/css/style.css" rel="stylesheet"> -->
    <link href="pages\htmlfolder\assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="pages\htmlfolder\assets/css/style.css" rel="stylesheet">
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>AMS</title>

</head>
<body onload="window.print();">
  <div class="container text-capitalize" style="margin-bottom: 15px; margin-top: 15px">
      <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12">
          <img src="pages\htmlfolder\assets/img/logo.png">
        </div>
      </div>
      <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12">
        <h4 class="user_info">SCHOLASTIC TRAIL</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-2 col-sm-2 col-md-2">
        <span>Student Number:</span>
      </div>
      <div class="col-lg-10 col-sm-10 col-md-10">
       <span><?= $getsingle['student_ID']; ?></span>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-2 col-sm-2 col-md-2">
        <span>Student Name:</span>
      </div>
      <div class="col-lg-10 col-sm-10 col-md-10">
        <span><?= $getsingle['si_LNAME']; ?>, <?= $getsingle['si_FNAME']; ?> <?= $getsingle['si_MNAME']; ?></span>
      </div>
    </div>
    <!-- <div class="row">
      <div class="col-lg-2 col-sm-2 col-md-2">
        <span>Year:</span>
      </div>
      <div class="col-lg-10 col-sm-10 col-md-10">
        <span>1</span>
      </div>
    </div> -->
    <div class="row">
      <div class="col-lg-2 col-sm-2 col-md-2">
        <span>program:</span>
      </div>
      <div class="col-lg-10 col-sm-10 col-md-10">
        <span><?= USERS::getFullCourse($getsingle['si_ID']); ?></span>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-2 col-sm-2 col-md-2">
        <span>Contact No:</span>
      </div>
      <div class="col-lg-10 col-sm-10 col-md-10">
        <span><?= $getsingle['si_CONTACT']; ?></span>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-2 col-sm-2 col-md-2">
        <span>Home Address:</span>
      </div>
      <div class="col-lg-10 col-sm-10 col-md-10">
        <span><?= $getsingle['si_STREET']; ?>, <?= $getsingle['si_BRGY']; ?>, <?= $getsingle['si_CITY']; ?>, <?= $getsingle['si_DISTRICT']; ?>, <?= $getsingle['si_PROVINCE']; ?></span>
      </div>
    </div>
    
          <?php
                    $sem = SEM::getallsemsofstudent($getsingle['si_ID']);
                    #print_r($sem);
          ?>
    <br>
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12">
            <h5 class="b">Enrollment History</h5>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-sm-4 col-md-4">
            <span><b>Academic Year / Semester</b></span>
          </div>
          
        </div>
        <?php foreach($sem as $key1 => $val1){ ?>
        <div class="row">
          <div class="col-lg-4 col-sm-4 col-md-4">
            <span><?php echo $val1['sem_NAME']; ?></span>
          </div>
        </div>
        <?php } ?>
        <br>
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12">
            <h5>Scholastic Performance</h5>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-sm-4 col-md-4">
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-md-12">
                <span><b>Academic Year / Semester</b></span>
              </div>
            </div>
          </div>
          <div class="col-lg-2 col-sm-2 col-md-2"></div>
          <div class="col-lg-6 col-sm-6 col-md-6">
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>Passed</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>Failed</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>INC</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>AW</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>UW</span>
              </div>
              <!-- <div class="col-lg-2 col-sm-2 col-md-2">
                <span>Average</span>
              </div> -->
            </div>
          </div>
        </div>
        
<?php $overallpassed=0;
      $overalltaken=0;
      $overallaw=0;
      $overalluw=0;
 ?>
        <?php foreach($sem as $key1 => $val1){ ?>
        <div class="row">
          <div class="col-lg-4 col-sm-4 col-md-4">
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-md-12">
                <span><?php echo $val1['sem_NAME']; ?></span>
              </div>
            </div>
          </div>
          <div class="col-lg-2 col-sm-2 col-md-2">
            

            <?php
                $mygrade = SUBJENROL::gradebyid($siiid,$val1['sem_ID']);

                $unitspassed = 0;
                $inc= 0;
                $aw= 0;
                $uw= 0;
                 foreach($mygrade as $key2 => $val2) { ?>
               
                    <?php if($val2['status'] == 1){ ?>   
                    <?php
                    $ss = SUBJSCHED::getSched($val2['subject_sched_ID']);
                    $s = SUBJECTS::getID($ss['subject_ID']);
                   
                   $units = $s['LEC_UNIT'] + $s['LAB_UNIT'];

                   ?>
                 
                   
                      <?php
                    if($ss['is_check'] == 1){
                      if($val2['grade'] == NULL){ 
                        $inc+= $units;
                        #"NG";
                      }else{ 
                          #echo $val2['grade'];
                          if(is_numeric($val2['grade'])){
                           #echo number_format((float)$val2['grade'], 2, '.', '');
                           $unitspassed+= $units ;
                          }else if($val2['grade'] == 'AW'){
                            $aw+= $units;
                          }else if($val2['grade'] == 'UW'){
                            $uw+= $units;
                          }else{
                            #echo $val2['grade'];
                            $unitspassed+= $units ;
                          }
                        
                     } 
                     }else{
                      $inc+= $units;
                      #echo "NG";
                     }



                     ?>

                   

                <?php } ?><?php } 
                $overallpassed += $unitspassed;
                $overalltaken += ($unitspassed+$inc);
                $overalluw += $uw;
                $overallaw += $aw;
                 #$unitspassed;
                  ?>




          </div>
          <div class="col-lg-6 col-sm-6 col-md-6">
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span><?=  number_format((float)$unitspassed, 2, '.', ''); ?></span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>0.00</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span><?=  number_format((float)$inc, 2, '.', ''); ?></span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span><?=  number_format((float)$aw, 2, '.', ''); ?></span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span><?=  number_format((float)$uw, 2, '.', ''); ?></span>
              </div>
              <!-- <div class="col-lg-2 col-sm-2 col-md-2">
                <span>1</span>
              </div> -->
            </div>
          </div>
        </div>
        <?php } ?>


        <br>
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="row">
              <div class="col-lg-6 col-sm-6 col-md-6"></div>
              <div class="col-lg-6 col-sm-6 col-md-6">
                <div class="row">
                  <div class="col-lg-8 col-sm-8 col-md-8"></div>
                  <div class="col-lg-2 col-sm-2 col-md-2">
                    <b><span class="hidden">GWA</span></b>
                  </div>
                  <!-- <div class="col-lg-2 col-sm-2 col-md-2">
                    <span>1</span>
                  </div> -->
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <b><span>Unit Passed</span></b>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span><?=  $overallpassed; ?></span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <b><span>Unit Failed</span></b>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>0.00</span>
              </div>
            </div>
          </div>
        </div>

        <br>
        <div class="row">
          <div class="col-lg-4 col-sm-4 col-md-4">
            <span>Grade Range (Per Course)</span>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>100% - 97%</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>1.00</span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>96% - 93%</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>1.25</span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>92% - 89% </span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>1.50</span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>88% - 85%</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>1.75</span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>84% - 80%</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>2.00</span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>79% - 75%</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>2.25</span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>74% - 70%</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>2.50</span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>69% - 65%</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>2.75</span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>64% - 60%</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>3.00</span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>59% & below</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>5.00</span>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span>INC Grade</span>
              </div>
              <div class="col-lg-2 col-sm-2 col-md-2">
                <span></span>
              </div>
            </div>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-lg-4 col-sm-4 col-md-4">
            <span>Unauthorized Withdrawal (UW)</span>
          </div>
          <div class="col-lg-2 col-sm-2 col-md-2">
            <span><?= money($overalluw); ?></span>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 col-sm-4 col-md-4">
            <span>Authorized Withdrawal (AW)</span>
          </div>
          <div class="col-lg-2 col-sm-2 col-md-2">
            <span><?= money($overallaw); ?></span>
          </div>
        </div>
        <br>
        <div class="row">
          <div class="col-lg-2 col-sm-2 col-md-2">
            <span>Total Units Taken</span>
          </div>
          <div class="col-lg-2 col-sm-2 col-md-2">
            <span><?=  $overalltaken ?></span>
          </div>
        </div>
        




      </div>
    </div>
  </div>

    <script src="pages\htmlfolder\assets/js/jquery.min.js"></script>
    <script src="pages\htmlfolder\assets/js/jquery.dataTables.min.js"></script>
    <script src="pages\htmlfolder\assets/js/bootstrap.min.js"></script>

    

</body>
</html>
