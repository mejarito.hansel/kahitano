<?php 
//INITIALIZE INCLUDES
  include('init.php');
#SESSION::CheckLogin();


if (SESSION::isLoggedIn()) {
  #HTML::redirect();
}

  



if (isset($_POST['username']) and isset($_POST['password'])) {
  $user = USERS::getID($_POST['username'], $_POST['password']);
    

    
  
    
  if ($user) 
  {
    #$ip =get_client_ip();
    # $security_option = navCat::security_option();
  
        
  
      
        SESSION::login($user);
                msgbox("Login Success");
               
               
        AUDIT::insert($user['account_ID'],"LOGGED IN");
        
        
        if(isset($_SESSION['previous_page']))
        {
          header("location:$_SESSION[previous_page]");
        }else{
          HTML::redirect();
        }
    

  } else {
       
    SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
  }
} else {
  $_SESSION['errors'] = NULL;
}


  //QUERIES

  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php include(LAYOUTS . "styles.php"); ?>
    <?php include(LAYOUTS . "scripts.php"); ?>
      <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>

      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
      <title>HOME | School Management System v2.0</title>
  </head>

  <body>

    <!-- top nav -->
    <?php include(LAYOUTS . 'top_nav.php'); ?>
    <!-- end nav -->

    <div class="container">
      
      <!-- banner -->
      <?php include(LAYOUTS . "banner.php"); ?>
      <!-- end banner -->
      
          
            
            
      <!-- alert messages -->
      <?php 
      
      SESSION::DisplayMsg(); 
      
      
      
      ?>
      <!-- end of alert messages -->
      
      <!-- start row container -->
      <!-- 
            <div class="row">
        
        <div class="col-lg-12">

        
          <ul class="breadcrumb">
            <li class="active">Home</li>
          </ul>
        

        </div>
        
      </div>
            -->
      <!-- end row container -->
      
   
             
            <!-- row container -->
      <div class="row">

        <!-- left nav -->
                
            <?php
        if (!SESSION::isLoggedIn()) {
        
        //audit
        
        
        
          ?>
        <div class="col-md-3">
                <?php include(PAGES."navigation.php");?>
        </div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
                <legend>Login</legend>

                <!-- if errors --> 
                <?php SESSION::DisplayMsg(); ?>
                <!-- end errors --> 


                <div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
                  <label for="inputUsername" class="col-lg-2 control-label">Username</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
                  </div>
                </div>
                <div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
                  <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                  <div class="col-lg-10">
                    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

                  </div>
                </div>
                                
                <div class="form-group row">
                  <div class="col-lg-offset-9">
                    <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                       
                    <button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
                  </div>
                </div>
                                
                                </fieldset>
                    </form>
                    </div>  
                </div>
                
                <?php   }else{
        
        AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
        
        ?>

<div class="col-md-3">

<?php include(PAGES."navigation.php");?>
</div>
              
                
<div class="col-md-9">
  <div class="panel panel-default">

    <div class="panel-heading">
      <table>
            <tr>
              <td width="50%">
                <h4 class="nav-pills" style="margin-top: 9px; margin-bottom: 10px"> Permission User Account <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
              </td>
              <!-- <td>
                <div class="input-group col-md-12">
                </div>
              </td> -->
              <td width="5%">
              <div class="col-md-1">   
                    <button class="btn btn-success" id="addUser">Add User</button>
              </div>
            </td>
          </tr>
        </table>
    </div>
    <div class="panel-body"><br>
  
      <div class="alert alert-success hide" id="successxd">
        <h4 class="alert-heading">System Message</h4> 
        <strong>Success!</strong> Successfully updated!
      </div>
      <!-- <div class="row">
              <div class="form-group">
                  <div class="col-sm-5">
                      <input name='' id="myInput" placeholder="Search by Name or Applicant Number" class='form-control' onkeyup="myFunction()" type='text' required autofocus style="font-style: italic;"/>
                  </div>
              </div>
            </div><br> -->
        <div class="col-sm-6">
          <!-- <table border="0" cellspacing="5" cellpadding="5" style="width:40%;">
            <tbody>
              <tr>
                <td><input name='' id="myInput" placeholder="Search by Name or Applicant Number" class='form-control' onkeyup="myFunction()" type='text' required autofocus style="font-style: italic;"/></td>
              </tr>
            </tbody>
          </table><br> -->
          <table id="myTable" class="table table-hover table-responsive table-striped text-md" style="width:100%;">
            <thead>
                <tr>
                  <th>User Account</th>
                  <!-- <th>Status</th> -->
                  <th>Action</th>
                </tr>
            </thead>
            <tbody id="tbody">
            </tbody>
          </table>
        </div>
        <div class="col-sm-6">
          <div id="page-wrap" class="userTypes">
  
          <h4><span id="user_type_title"></span></h4>
            <div class="scrollbox">
              <div class="scrollbox-content">
              <ul class="treeview">
              </ul>
              </div>
            </div>
              <div class="col-sm-12">
                <a class="btn btn-success btn-block hide" id="btnModule"> Save </a>
              </div>
          </div>

        </div>
        <div class="modal fade" id="myModal" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 style="">Edit User Type Name</h4>
              </div>
              <div class="modal-body" style="padding:40px 90px;">
                <div class="failed hide" id="icpID"><strong>Error!</strong> Internet connection problem</div>
                <div class="success hide" id="success"><strong>Success!</strong> Successfully updated!</div>
                <div class="failed hide" id="requiredfield"><strong>Error!</strong> Required all fields</div>
                <div class="failed hide" id="exst2"><strong>Error!</strong> User Account Already Exist.</div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="userTypeName" placeholder="Enter user account">
                    <span class="errorRequired hide" id="reqErrorEdit">Required *</span>
                  </div>
                  <button type="submit" class="btn btn-success btn-block" id="edibtn"> Save </button>
              </div>
            </div>
          </div>
        </div>
        <div class="modal fade" id="addModal" role="dialog">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 style="">Add User Account</h4>
              </div>
              <div class="modal-body" style="padding:40px 90px;">
                <div class="failed hide" id="icpID"><strong>Error!</strong> Internet connection problem</div>
                <div class="success hide" id="success2"><strong>Success!</strong> Successfully Added!</div>
                <div class="failed hide" id="requiredfield"><strong>Error!</strong> Required all fields</div>
                <div class="failed hide" id="exst"><strong>Error!</strong> User Account Already Exist.</div>
                  <div class="form-group">
                    <input type="text" class="form-control" id="userType" placeholder="Enter user account">
                    <span class="errorRequired hide" id="reqErrorAdd">Required *</span>
                  </div>
                  <button type="submit" class="btn btn-success btn-block" id="saveUser"> Save </button>
              </div>
            </div>
          </div>
        </div> 
    </div>
  </div>
</div>
<style type="text/css">
.scrollbox {
  width: auto;
  height: 455px;
  overflow: auto;
  visibility: hidden;
  margin-bottom: 10px;
}
.scrollbox-content,
.scrollbox:hover,
.scrollbox:focus {
  visibility: visible;
}
* { margin: 0; padding: 0; }

#page-wrap {
  margin: auto 0;
}

.treeview {
  margin: 10px 0 0 20px;
}

ul { 
  list-style: none;
}

.treeview li {
  /*background: url(http://jquery.bassistance.de/treeview/images/treeview-default-line.gif) 0 0 no-repeat;*/
  padding: 2px 0 2px 16px;
}

.treeview > li:first-child > label {
  /* style for the root element - IE8 supports :first-child
  but not :last-child ..... */
  
}

.treeview li.last {
  background-position: 0 -1766px;
}

.treeview li > input {
  height: 16px;
  width: 16px;
  /* hide the inputs but keep them in the layout with events (use opacity) */
  opacity: 0;
  filter: alpha(opacity=0); /* internet explorer */ 
  -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(opacity=0)"; /*IE8*/
}

.treeview li > label {
  background: url(assets/img/inputs.png) 0 -1px no-repeat;
  /* move left to cover the original checkbox area */
  margin-left: -20px;
  /* pad the text to make room for image */
  padding-left: 20px;
}

/* Unchecked styles */

.treeview .custom-unchecked {
  background-position: 0 -1px;
}
.treeview .custom-unchecked:hover {
  background-position: 0 -21px;
}

/* Checked styles */

.treeview .custom-checked { 
  background-position: 0 -81px;
}
.treeview .custom-checked:hover { 
  background-position: 0 -101px; 
}

/* Indeterminate styles */

.treeview .custom-indeterminate { 
  background-position: 0 -141px; 
}
.treeview .custom-indeterminate:hover { 
  background-position: 0 -121px; 
}
</style>
<?php } ?>
    
      </div>
            <?php 
      ?>
      <?php include (LAYOUTS . "footer.php"); ?>
      <script src="<?php echo HOME; ?>old/assets/js/custom/access-control.js"></script>
      

    </div>
  <script type="text/javascript">
    $( window ).ready(function(){
        setTimeout(function() {

          $.main.accesscontrol.accountControl();
          $.main.accesscontrol.add();
          
        },100);
    })
  </script>
  </body>
</html>
