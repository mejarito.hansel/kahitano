<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Library / Book Inventory, DESCRIPTION: User visited Book Inventory";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}

if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
    
	if ($user) 
	{
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();
	
				SESSION::login($user);
                msgbox("Login Success");
               
               
				AUDIT::insert($user['account_ID'],"LOGGED IN");
				
				
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}



	//QUERIES




	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
	  	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>

      <title>HOME | School Management System v2.0</title>
	</head>

	<body>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
            
			<!-- alert messages -->
			<?php 
			
			SESSION::DisplayMsg(); 
			
			
			
			?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
				
				//audit
				
				
				
					?>
				<div class="col-md-3">
            		<?php include(PAGES."navigation.php");?>
				</div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 


								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>
								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

									</div>
								</div>
                                
                                	<div class="form-group row">
									<div class="col-lg-offset-9">
										 <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                                
                                </fieldset>
                    </form>
                    </div>	
                </div>
                
                <?php   }else{
				
				AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
				
				?>

<div class="col-md-3">

<?php include(PAGES."navigation.php");?>
</div>
              
                
<div class="col-md-9">
	<div class="panel panel-default">

		<div class="panel-heading">
			<table>
        		<tr>
        			<td width="50%">
						<h4 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"> Book Inventory <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
					</td>
				 	<td>
            			<div class="input-group col-md-12 hide">
              				
              				<input id="searchItem" class="searchItem form-control"  type="text" autofocus="autofocus"  style="text-transform: uppercase;"/>
                            
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            			</div>
        			</td>
        			<td width="5%">
	    				<div class="col-md-1">   
	    					<a id="addbook" class='btn btn-success btn-ms addbook P7C1_ADDABOOK'>Add a Book</a>
	    				</div>
	    			</td>
        			<td width="5%">
	    				<div class="col-md-1">   
	    					<a id="" class='btn btn-success btn-ms btnCSV P7C1_EXPORTSASCSV' >Export as CSV</a>
	    				</div>
	    			</td>
	    		</tr>
	    	</table>
		</div>
		<div class="panel-body"><br>
			<div class="row">
            	<div class="form-group">
	                <div class="col-sm-6">
	                    <input name='' id="myInput" placeholder="Search by book title, author name, book type, index" class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
	                </div>
            	</div>
           	</div><br>
           	<div id="dvData">
	        	<table id="myTable" class="table table-hover table-responsive table-striped text-md" style="width:100%;">
			        <thead>
			            <tr>
			              <th>Date Accesion No.</th>
			              <th>Accesion No.</th>
			              <th>Call No.</th>
			              <th>Title</th>
			              <th>Book Type</th>
			              <th>Author</th>
			              <th>Copyright</th>
			              <th>Source</th>
			              <th>Date Received</th>
			              <th>Status</th>
			              <th>Index Term</th>
			              <th>Action</th>
			            </tr>
			        </thead>
			        <tbody id="tbody">
			        </tbody>
			    </table>
			</div>
		  	<div class="modal fade" id="myModaladd" role="dialog">
			    <div class="modal-dialog">
			      	<div class="modal-content">
				        <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <h4 style="">Add Book Tool</h4>
				          <p>Enter the details of the book below to update the inventory of library management module</p>
				        </div>
				        <div class="modal-body" style="padding:40px 90px;">
		                    <div class="success hide" id="success"><strong>Success!</strong> Successfully Book Added!</div>
					        <div class='form-group'>
					        	<label>Date Accession No.</label>
					            <input type='text' class="form-control" id='datetimepicker' placeholder="Date Accession No." />
					            <span style=" margin-top: -30px; float: right; margin-right: 7px;"><i class="fa fa-calendar"></i></span>
					        </div>
					        <div class='form-group'>
					        	<label>Accession No.</label>
					            <input id="accessionID" type='text' class="form-control" placeholder=" Accession No."/>
					        </div>
					        <div class='form-group'>
					        	<label>Call No.</label>
					            <input id="callID" type='text' class="form-control" placeholder=" Call No."/>
					        </div>
					        <div class='form-group'>
					        	<label>Title</label>
					            <input id="titleID" type='text' class="form-control" placeholder=" Title"/>
					        </div>
					        <div class='form-group'>
					        	<label>Book Type</label>
					            <input id="booktypeID" type='text' class="form-control" placeholder=" Book Type"/>
					        </div>
					        <div class='form-group'>
					        	<label>Author</label>
					            <input id="authorID" type='text' class="form-control" placeholder=" Author"/>
					        </div>
					        <div class='form-group'>
					        	<label>Copyright</label>
					            <input id="copyrightID" type='text' class="form-control" placeholder=" Copyright"/>
					        </div>
					        <div class='form-group'>
					        	<label>Source</label>
					            <input id="sourceID" type='text' class="form-control" placeholder=" Source"/>
					        </div>
					        <div class='form-group'>
					        	<label>Date Received</label>
					            <input type='text' class="form-control" id='datetimepicker4' placeholder="Date Received" />
					            <span style=" margin-top: -30px; float: right; margin-right: 7px;"><i class="fa fa-calendar"></i></span>
					        </div>
					        <!-- <div class='form-group'>
					        	<label>Returned Date</label>
					            <input type='text' class="form-control" id='Udatetimepicker4' placeholder="Returned Date" />
					            <span style=" margin-top: -30px; float: right; margin-right: 7px;"><i class="fa fa-calendar"></i></span>
					        </div> -->
					        <div class='form-group'>
					        	<label>Book status</label>
					            <select id="bookStatus" class="form-control">
					            	<option value="0">- Select Status -</option>
					            	<option value="1">Lost</option>
					            	<option value="2">Available</option>
					            </select>
					        </div>
					        <div class='form-group'>
					        	<label>Index Term</label>
					        	<input id="bookIndex" type='text' class="form-control" placeholder=" Index Term"/>
					        </div>
				          	<button type="submit" class="btn btn-success btn-block" id="saveBtn"> Save</button>
				        </div>
			      	</div>
			    </div>
		  	</div> 
		  	<div class="modal fade" id="myModalupdate" role="dialog">
			    <div class="modal-dialog">
			      	<div class="modal-content">
				        <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <h4 style="">Update Book Tool</h4>
				          <p>Enter the details of the book below to update the inventory of library management module</p>
				        </div>
				        <div class="modal-body" style="padding:40px 90px;">
		                    <div class="success hide" id="success"><strong>Success!</strong> Successfully Book Updated!</div>
					        <div class='form-group'>
					        	<label>Date Accession No.</label>
					            <input type='text' class="form-control" id='Udatetimepicker' placeholder="Date Accession No." />
					            <span style=" margin-top: -30px; float: right; margin-right: 7px;"><i class="fa fa-calendar"></i></span>
					        </div>
					        <div class='form-group'>
					        	<label>Accession No.</label>
					            <input id="UaccessionID" type='text' class="form-control" placeholder=" Accession No."/>
					        </div>
					        <div class='form-group'>
					        	<label>Call No.</label>
					            <input id="UcallID" type='text' class="form-control" placeholder=" Call No."/>
					        </div>
					        <div class='form-group'>
					        	<label>Title</label>
					            <input id="UtitleID" type='text' class="form-control" placeholder=" Title"/>
					        </div>
					        <div class='form-group'>
					        	<label>Book Type</label>
					            <input id="UbooktypeID" type='text' class="form-control" placeholder=" Book Type"/>
					        </div>
					        <div class='form-group'>
					        	<label>Author</label>
					            <input id="UauthorID" type='text' class="form-control" placeholder=" Author"/>
					        </div>
					        <div class='form-group'>
					        	<label>Copyright</label>
					            <input id="UcopyrightID" type='text' class="form-control" placeholder=" Copyright"/>
					        </div>
					        <div class='form-group'>
					        	<label>Source</label>
					            <input id="UsourceID" type='text' class="form-control" placeholder=" Source"/>
					        </div>
					        <div class='form-group'>
					        	<label>Date Received</label>
					            <input type='text' class="form-control" id='Udatetimepicker41' placeholder="Date Received" />
					            <span style=" margin-top: -30px; float: right; margin-right: 7px;"><i class="fa fa-calendar"></i></span>
					        </div>
					        <div class='form-group'>
					        	<label>Book Status</label>
					            <select id="UbookStatus" class="form-control">
					            	<option value="0">- Select Status -</option>
					            	<option value="1">Lost</option>
					            	<option value="2">Available</option>
					            </select>
					        </div>
					        <div class='form-group'>
					        	<label>Index Term</label>
					        	<input id="UbookIndex" type='text' class="form-control" placeholder=" Index Term"/>
					        </div>
				          	<button type="submit" class="btn btn-success btn-block" id="updatebtn"> Save</button>
				        </div>
			      	</div>
			    </div>
		  	</div> 
	 	</div>
	</div>
</div>
<?php } ?>
  	
			</div>
            <?php 
			?>
			<?php include (LAYOUTS . "footer.php"); ?>
 			<script src="<?php echo HOME; ?>old/assets/js/custom/lb_material.js"></script>

		</div>
  <script type="text/javascript">
  	$( window ).ready(function(){
        setTimeout(function() {

      		$.main.library.fetchallBook();
 
        },100);
  	})
    function myFunction() {
      var input, filter, table, tr, td, i;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[3];
        td1 = tr[i].getElementsByTagName("td")[4];
        td2 = tr[i].getElementsByTagName("td")[5];
        td3 = tr[i].getElementsByTagName("td")[10];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 ||  td3.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
    $(function () {
        $('#datetimepicker').datetimepicker();
        $('#datetimepicker4').datetimepicker();
        $('#Rdatetimepicker').datetimepicker();
        $('#Rdatetimepicker4').datetimepicker();
        $('#Udatetimepicker').datetimepicker();
        $('#Udatetimepicker4').datetimepicker();
        $('#Udatetimepicker41').datetimepicker();
    });
  </script>
	</body>
</html>
