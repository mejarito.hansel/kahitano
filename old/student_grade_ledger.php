<?php
include('init.php');
$siiid = clean($_GET['id']);
$getsingle = USERS::getSingle(array("si_ID"=>"DESC"),$siiid  );
$getsingle1 = USERS::getSingleSAI(array("si_ID"=>"DESC"), $siiid );
$user_id = $user['account_ID'];
$action_event = "Print";
$event_desc = "Module: 2 Registrar / 2.2 Find Student, DESCRIPTION: User clicked Print Grade Ledger for ".$getsingle['si_FNAME'].' '.$getsingle['si_MNAME'].' '.$getsingle['si_LNAME'];
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="pages\htmlfolder\assets/css/jquery.dataTables.min.css">
  	<!-- <link href="ams\old\pages\htmlfolder\assets/css/style.css" rel="stylesheet"> -->
  	<link href="pages\htmlfolder\assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  	<link href="pages\htmlfolder\assets/css/style.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  	<title>AMS</title>

</head>
<body onload="window.print();">
	<div class="container text-capitalize" style="margin-bottom: 15px; margin-top: 15px">
	    <div class="row">
	      <div class="col-lg-12 col-sm-12 col-md-12">
	        <img src="pages\htmlfolder\assets/img/logo.png">
	      </div>
	    </div>
	    <div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<h4 class="user_info">GRADE LEDGER</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-8 col-sm-8 col-md-8">
				<div class="row">
					<div class="col-lg-3 col-sm-3 col-md-3">
						<span>Student no:</span>
					</div>
					<div class="col-lg-9 col-sm-9 col-md-9">
						<span><?= $getsingle['student_ID']; ?></span>
					</div>
				</div>
			</div>
			<!-- <div class="col-lg-4 col-sm-4 col-md-4">
				<div class="row">
					<div class="col-lg-6 col-sm-6 col-md-6">
						<span>Academic Year:</span>
					</div>
					<div class="col-lg-6 col-sm-6 col-md-6">
						<span>2017-2018</span>
					</div>
				</div>
			</div> -->
		</div>
		<div class="row">
			<div class="col-lg-2 col-sm-2 col-md-2">
				<span>Student Name:</span>
			</div>
			<div class="col-lg-4 col-sm-4 col-md-4">
				<span><?= $getsingle['si_LNAME']; ?>, <?= $getsingle['si_FNAME']; ?> <?= $getsingle['si_MNAME']; ?></span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-sm-2 col-md-2">
				<span>Program:</span>
			</div>
			<div class="col-lg-4 col-sm-4 col-md-4">
				<span><?= USERS::getCourse($getsingle['si_ID']); ?></span>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<table class="table table-bordered compact nowrap table-condensed">
				    <thead class="">
				      <tr>
				        <th>course</th>
				        <th>description</th>
				        <th>units</th>
				        <th>grade</th>
				        <th>remarks</th>
				        <th>semester</th>
				      </tr>
				    </thead>

				    <?php
                    $sem = SEM::getallsemsofstudent($getsingle['si_ID']);
                    #print_r($sem);
					?>


				    <tbody>
				    	<?php foreach($sem as $key1 => $val1){ ?>
				    	<?php
				    		$mygrade = SUBJENROL::gradebyid($siiid,$val1['sem_ID']);
				    	 ?>
				    	 <?php foreach($mygrade as $key2 => $val2) { ?>
               
               				<?php if($val2['status'] == 1){ ?>   
               				<?php
                    $ss = SUBJSCHED::getSched($val2['subject_sched_ID']);
                    $s = SUBJECTS::getID($ss['subject_ID']);
                   
                   $units = $s['LEC_UNIT'] + $s['LAB_UNIT'];

                   ?>
						      <tr>
						        <td><?php echo $s['subject_CODE']; ?></td>
						        <td><?php echo $s['subject_DESCRIPTION']; ?></td>
						        <td><?php printf("%.2f",$units); ?></td>
						        <td>
						        	<?php
                    if($ss['is_check'] == 1){
                     if($val2['grade'] == NULL){ echo "NG"; }else{ 
                      if(is_numeric($val2['grade'])){
                       echo number_format((float)$val2['grade'], 2, '.', '');
                      }else{
                        echo $val2['grade'];
                      }
                      


                     } 
                     }else{
                      echo "NG";
                     }?>

						        </td>

                  <td class=""><div align=""><?php 
             if($ss['is_check'] == 1){
                        switch($val['grade'])
                        {
                          case "1.00":
                          case "1.25":
                          case "1.50":
                          case "1.75":
                          case "2.00":
                          case "2.25":
                          case "2.50":
                          case "2.75":
                          case "3.00": echo "PASSED";
                          break;
                          case "5.00": echo "FAILED";
                          break;
                          case "INC": echo "INCOMPLETE";
                          break;
                          case "DRP": echo "DROPPED";
                          break;
                            default:
                              if($val['grade'] >= 100 || 60 <= $val['grade']){
                                echo "PASSED";
                              }elseif($val['grade'] >= 59 || 1 <= $val['grade']){
                                echo "FAILED";
                              }else{
                                echo "NO GRADE";
                              }
                        }
            }else{
              echo "NO GRADE";
            }           
                       ?></div></td>
						        <td><?= $val1['sem_NAME']; ?></td>
						      </tr>

				      	<?php } ?><?php } ?><?php } ?>
				     <!--  <tr>
				        <td>NGEC6</td>
				        <td>Art Appreciation</td>
				        <td>3</td>
				        <td>1</td>
				        <td>1st Semester 2017-2018</td>
				      </tr>
				      <tr>
				        <td>GEM1</td>
				        <td>Filipino 1</td>
				        <td>3</td>
				        <td>1.5</td>
				        <td>1st Semester 2017-2018</td>
				      </tr>
				      <tr>
				        <td>GEM3</td>
				        <td>The Life and Works of Dr. Jose Rizal</td>
				        <td>3</td>
				        <td>1</td>
				        <td>1st Semester 2017-2018</td>
				      </tr> -->
				    </tbody>
				  </table>
			</div>
		</div>
	</div>

    <script src="pages\htmlfolder\assets/js/jquery.min.js"></script>
    <script src="pages\htmlfolder\assets/js/jquery.dataTables.min.js"></script>
  	<script src="pages\htmlfolder\assets/js/bootstrap.min.js"></script>

    

</body>
</html>
