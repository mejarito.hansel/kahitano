<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
            SUBJECTS::addSubject(Request::post());
			#var_dump(Request::post());
			
			break;
			case "edit": SUBJECTS::update_Subject($_GET['id'],Request::post()); break;
			#case "update_blog":  BLOG::update(Request::post("blog_info")); break;
			
			#case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": SUBJECTS::delete($_GET['id']); break;
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
      <title>SCHOOL MANAGEMENT SYSTEM</title>
	</head>

	<body>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
            
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                

                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
               
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                      		<div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Course </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                             <input type="hidden" name =AccountID value="<?= $user['account_ID'];?>">
                        	<table border='0' class="table table-responsive text-md" style="width:80%">
                        			<!--
                        			<tr>
                        				<td>
                        					&nbsp;
                        				</td>

                        				<td>
                        					<label for='subject_ID' class='col-md-3'>ID</label>
                        				</td>

                        				<td>
                        					<input name='id' class='form-control' type='text'/>
                            			</td>
                            		</tr>
                            		-->

                            		<tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='subject_CODE' class='col-md-12'>COURSE CODE</label>                        				</td>

                        				<td>
                        					<input name='subject_CODE' autofocus required class='form-control' type='text'/>                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='subject_DESCRIPTION' class='col-md-12'>COURSE TITLE</label>                        				</td>

                        				<td>
                        					<input name='subject_DESCRIPTION'  required class='form-control' type='text'/>                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='UNIT' class='col-md-12'>LEC HRS</label>                        				</td>

                        				<td>
                        					<input name='LEC_UNIT' value="0.0"  required class='form-control' type='text'/>                            			</td>
                            		</tr>

                            		<tr>
                                        <td>&nbsp;                                      </td>

                                        <td>
                                            <label for='UNIT' class='col-md-12'>LAB HRS</label>                                     </td>

                                        <td>
                                            <input name='LAB_UNIT' value="0.0"  required class='form-control' type='text'/>                                     </td>
                                    </tr>

                                    <tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='UNIT' class='col-md-12'>TOTAL UNIT</label>                        				</td>

                        				<td>
                        					<input name='TOTAL_UNIT' value="0.0"  required class='form-control' type='text'/>                            			</td>
                            		</tr>
                            		<tr>
                            		  <td colspan='2'>                                    
                            		  
                       		          <td align='right'><button  class='btn btn-success btn-ms' type='submit'><i class='fa fa-edit'></i> Add Subject</button></td>
                       		  </tr>
                            </table>
                   	  </form>
                    </div>
                	</div>
                    <?php break; 
                        case "edit":
                        $subject_ID  =$_GET['id'];
                        $getsingle = SUBJECTS::getSingle(array("subject_ID"=>"DESC"),$subject_ID  );
                        //print_r($getsingle);
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                            <i class="fa fa-folder-o"></i> Update Subject </h4> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                        	<table border='0' class="table table-responsive text-md" style="width:75%">
                        			<tr>
                        				<td>&nbsp;
                        					
                        				</td>

                        				<td>
                        					<label for='subject_ID' class='col-md-12'>ID</label>
                        				</td>

                        				<td>
                        					<input name='id' class='form-control' type='hidden' value="<?= $getsingle['subject_ID'];?>"/>
                        					<input name='subject_ID' class='form-control' type='text' value="<?= $getsingle['subject_ID'];?>"/>
                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;
                        					
                        				</td>

                        				<td>
                        					<label for='subject_CODE' class='col-md-12'>COURSE CODE</label>
                        				</td>

                        				<td>
                        					<input name='subject_CODE' class='form-control' type='text' value="<?= $getsingle['subject_CODE'];?>"/>
                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;
                        					
                        				</td>

                        				<td>
                        					<label for='subject_DESCRIPTION' class='col-md-12'>COURSE TITLE</label>
                        				</td>

                        				<td>
                        					<input name='subject_DESCRIPTION' class='form-control' type='text' value="<?= $getsingle['subject_DESCRIPTION'];?>"/>
                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='UNIT' class='col-md-12'>LEC HRS</label>                        				</td>

                        				<td>
                        					<input name='LEC_UNIT' required class='form-control' type='text' value="<?= $getsingle['LEC_UNIT'];?>"/>                            			</td>
                            		</tr>

                            		<tr>
                                        <td>&nbsp;                                      </td>

                                        <td>
                                            <label for='UNIT' class='col-md-12'>LAB HRS</label>                                     </td>

                                        <td>
                                            <input name='LAB_UNIT' value="<?= $getsingle['LAB_UNIT'];?>"  required class='form-control' type='text'/>                                       </td>
                                    </tr>

                                    <tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='UNIT' class='col-md-12'>TOTAL UNIT</label>                        				</td>

                        				<td>
                        					<input name='TOTAL_UNIT' value="<?= $getsingle['TOTAL_UNIT'];?>"  required class='form-control' type='text'/>                            			</td>
                            		</tr>

                            		<tr>
                            			<td colspan='3' align='right'>
                                        <input type="hidden" name =AccountID value="<?= $user['account_ID'];?>">
                                        <input type="hidden" name =beforesubject_CODE value="<?= $getsingle['subject_CODE'];?>">
                                        <input type="hidden" name =beforesubject_DESCRIPTION value="<?= $getsingle['subject_DESCRIPTION'];?>">
                                        <input type="hidden" name =beforeLEC_UNIT value="<?= $getsingle['LEC_UNIT'];?>">
                                        <input type="hidden" name =beforeLAB_UNIT value="<?=$getsingle['LAB_UNIT'];?>">
                                        <input type="hidden" name =beforeTOTAL_UNIT value="<?= $getsingle['TOTAL_UNIT'];?>">
                                    		<button  class='btn btn-success btn-ms' type='submit'><i class='fa fa-edit'></i> Update Subject</button>
                            			</td>
                            		</tr>
                            </table>
                    	</form>
                    </div>
                	</div>
                </div>
                </div>
            
                    <?php break; 
                        case "view":
                        $user_id = $user['account_ID'];
                        $action_event = "View";
                        $event_desc = "MODULE: Academic Affairs / Course Management, DESCRIPTION: User visited Course Management";
                        $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
					?>
						<?php
						if(!isset($_GET['id']))
						{
						?>
					
						<div class="panel panel-default">
							<div class="panel-heading">
								<table>
									<tr>
											<td width="50%">
												<h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px"><i class="fa fa-folder-o"></i> Course List </h4>
											</td>
											
											<td>
												<!--<input type="text" placeholder="Search" class="form-control">-->
												 <div class="input-group col-md-12">
                                                    <!-- <form id="form1" runat="server">
                                                         <input id="searchItem" class="searchItem form-control"  name="q" type="text" onkeyup="term(q.value)" autofocus="autofocus" onchange="term(q.value)"/>
                                                    </form> -->
                                                  <!-- <span class="input-group-addon"><i class="fa fa-search"></i></span> -->
                                               
                                                    <input name='' id="myInput" placeholder="Search by Id, Code, Title, Lec/Lab Hours or Units" class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
                                                    
                                                </div>
                                              <!--   <script>
                                    			$(document).ready(function() {
												$('#searchItem').keyup(function(){
												var s_Item = $('#searchItem').val();
													
													if(s_Item.length >= 3)
													{
														 $.ajax({
															type: "POST",
															url: "search.php?action=subjects.php",
															data: 'search_term=' + s_Item,
															success: function(msg){
																/* $('#resultip').html(msg); */
																	$("#display_result").show();
																	$("#display_result").html(msg);
																	$("#display_hide").hide();
																
															}
											
														}); // Ajax Call
													
														//alert(s_Item);
														}else{
															$("#display_hide").show();
															$("#display_result").hide();   
														}
													});
												});			
                                    			</script> -->
											</td>

											
                                            <td width="5%">
                                                <div class="col-md-1">   
                                                	<button style="margin-top: 1px; margin-bottom: 1px" class='btn btn-success P5C2_CREATENEWCOURSE btn-ms' type='submit' onclick="javascript:window.open('subjects.php?action=add','_self');">Create New Course</button>
                                                </div>
                                            </td>
                    
									</tr>
								</table>
							</div>
							
							

							<div id="display_result"></div>

							<div class="panel-body" id="display_hide">
								<table id="tblDisc" class="table table-hover table-responsive table-striped text-md">
									</tr>

									<th style="width:15%;">ID</th>
									<th style="width:20%;">CODE</th>
									<th style="width:30%;">TITLE</th>
									<th align="center" style="width:20%;">LEC/LAB HOURS</th>
                                    <th align="center" style="width:20%;">UNITS</th>
									<th style="width:40%;">OPTIONS</th>	
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    $viewSubjects = SUBJECTS:: getAllSubjects(array("subject_ID"=>"ASC"));
                                     if(count($viewSubjects)>=10) 
                                     {
                                         $pagination2 = new Pagination($viewSubjects, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($viewSubjects, 10, NULL);
                                     }

                                     $viewSubjects = $pagination2->get_array();

                                     if($viewSubjects) {

                                        foreach($viewSubjects as $key => $value){
                                    ?>
                                        <tr>
                                            <td>
                                                <?php echo $value['subject_ID']; ?>
                                            <td>
                                                <?php echo $value['subject_CODE']; ?>
                                            <td>
                                                <?php echo $value['subject_DESCRIPTION']; ?>
                                            <td align="center">
                                                <?php echo $value['LEC_UNIT']; ?> / <?php echo $value['LAB_UNIT']; ?>
                                            <td align="">
                                               <?php echo $value['TOTAL_UNIT']; ?>
                                            <td>
                                                <a href="subjects.php?action=edit&id=<?php echo $value['subject_ID']; ?>" class='btn btn-warning btn-xs P5C2_EDIT'>
                                                    Edit
                                                </a>
                                               <!--
                                               <a href="subjects.php?action=delete&id=<?php echo $value['subject_ID']; ?>" onClick="return confirm('You\'re about to delete the subject.')"; class='btn btn-danger btn-xs'>
													Delete
												</a>
                                                -->
                                            </td>
                                        </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
								<center>
                                <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                            </center>
								</div>
							</div>
						</div>
					</div>
                </div>
                  <?php } break; ?>
				  
				  
         <div id="myTabContent" class="tab-content">         
                  
              <!-- all --><div class="tab-pane fade " id="all">

                    <?php 
                        case "delete":?>
                        view
                <?php break;
                    }}else{?>
                    
                <?php } ?>
                	
				    
                 </div>

				    
				</div>
                
				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
            

			<!-- footer-->      
            
            <?php 
			#echo MESSAGES::check_message("gago");
			?>
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
	
</html>
<script type="text/javascript">
    function myFunction() {
        var input, filter, table, tr, td, i;
          input = document.getElementById("myInput");
          console.log(input);
          filter = input.value.toUpperCase();
          table = document.getElementById("tblDisc");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td2 = tr[i].getElementsByTagName("td")[1];
            td1 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            td4 = tr[i].getElementsByTagName("td")[4];
            if (td) {
              if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1 || td4.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }   
        }
    }
</script>
