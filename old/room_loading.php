<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::post("action")) {
			case "add": 
			
			break;
			case "change_lock":
				SUBJSCHED::change_lock(Request::post());
			break;
			
			case "change_check":
				SUBJSCHED::change_check(Request::post());
			break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ROOM::delete($_GET['id']); break;

		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>ROOM SCHEDULE | School Management System v2.0</title>
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end of alert messages -->
			<!-- start row container -->
			<!-- 
            <div class="row">
				<div class="col-lg-12">
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
            </div>	
			</div>
            -->
			<!-- end row container -->
            <!-- row container -->
			<div class="row">
				<!-- left nav -->   
          
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
               
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Semester Section</h3> 
                    </div>
                    <div class="panel-body">
                    
                    <?php
					$sem = SEM::getallsems(array("sem_ID"=>"DESC"));
					#print_r($sem);
					
					$course = COURSE::getallcourse(array("course_NAME"=>"ASC"));
					?>
                    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Semester Section Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                              <div class="col-sm-6">
                                  <label for='si_FNAME' class=''>Semester: </label>
                                  <select class='form-control' name="sem_ID">
                                  	<?php foreach($sem as $key => $val){ ?>
                                    <option value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
                                    <?php } ?>
                                  </select>
                              </div>
                            </div>
                            
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Course:</label>
                                    <select class='form-control' name="course_ID">
										<?php foreach($course as $key => $val){ ?>
                                        <option value="<?php echo $val['course_ID']; ?>"><?php echo $val['course_NAME']; ?></option>
                                        <?php } ?>    
                                    </select>
                               </div>
                             </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Section Name:</label>
                                    <input name='semester_section_NAME' placeholder="Section Name" class='form-control' type='text' required="required"/>
                               </div>
                            </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''></label>
                               </div>
                            </div>                            
                            
                            
                             <div class="form-group"></div>                            
                            
                            
                            
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Add Semester Section</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":

          			$geThis = SEMSECTION::geThis($_GET['id']);
					#print_r($geThis);
					?>
                    <?php
					$sem = SEM::getallsems(array("sem_ID"=>"DESC"));
					#print_r($sem);
					
					$course = COURSE::getallcourse(array("course_NAME"=>"ASC"));
					?>
                                        
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Update Semester Section</h3> 
                    </div>
                    <div class="panel-body">
                       <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Semester Section Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                              <div class="col-sm-6">
                                  <label for='si_FNAME' class=''>Semester: </label>
                                  <select class='form-control' name="sem_ID">
                                  	<?php foreach($sem as $key => $val){ ?>
                                    <option value="<?php echo $val['sem_ID']; ?>"
                                    
                                    <?php
									  if($val['sem_ID'] == $geThis['sem_ID']){
										echo "selected=selected";
									  }
									?>
                                    
                                    ><?php echo $val['sem_NAME']; ?></option>
                                    <?php } ?>
                                  </select>
                              </div>
                            </div>
                            
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Course:</label>
                                    <select class='form-control' name="course_ID">
										<?php foreach($course as $key => $val){ ?>
                                        <option value="<?php echo $val['course_ID']; ?>" 
                                        
                                          <?php
											  if($geThis['course_ID'] == $val['course_ID']){
												echo "selected=selected";
											  }
											?>
                                        
                                        ><?php echo $val['course_NAME']; ?></option>
                                        <?php } ?>    
                                    </select>
                               </div>
                             </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Section Name: </label>
                                    <input name='semester_section_NAME' value="<?php echo $geThis['semester_section_NAME']; ?>" placeholder="Section Name" class='form-control' type='text' required="required"/>
                               </div>
                            </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''></label>
                               </div>
                            </div>                            
                            
                            
                             <div class="form-group"></div>                            
                            
                            
                            
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Update Semester Section</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
						include "searching.php";						
						?>
							
                    <?php break; 
                        case "view":
                        $user_id = $user['account_ID'];
                        $action_event = "View";
                        $event_desc = "MODULE: Academic Affairs / Room Schedule, DESCRIPTION: User visited Room Schedule";
                        $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

                        ?>
                       
                       
                       
                       <ul class="breadcrumb">
                                        <li class="active"><a href="room_loading.php?action=view">ROOM SCHEDULE</a></li>
                                        
                                        <?php if(isset($_GET['sem_ID']))
										{	$sem = SEM::getSingleSem($_GET['sem_ID']) ?>
                                        
                                        <li class="active"><a href="?action=view&sem_ID=<?= $_GET['sem_ID'] ?>"><?php echo $sem['sem_NAME']; ?></a></li>
                                    	<?php } ?>
                                        
                                        
                                         <?php if(isset($_GET['fac_id']))
										{	$f = INSTRUCTORS::getSingle1($_GET['fac_id']) ?>
                                        
                                        <li class="active"><a href="?action=view&sem_ID=<?= $_GET['sem_ID'] ?>&fac_id=<?= $_GET['fac_id']; ?>"><?php  echo $f['instructor_NAME']; ?></a></li>
                                    	<?php } ?>
                                        
                                        <?php if(isset($_GET['subject_sched_id'])){ 
										$su = SUBJSCHED::getSched($_GET['subject_sched_id']) ;
										#print_r($su);
										?>
                                        <li class="active"><a href="?action=view&sem_ID=<?= $_GET['sem_ID'] ?>&fac_id=<?= $_GET['fac_id']; ?>&subject_sched_id=<?= $_GET['subject_sched_id']; ?>">
                                        
                                         <?php $c=COURSE::getbyID($su['course_ID']); 
												echo $c['course_INIT']."-";
                                              echo $su['semester_section_NAME']; ?> -
                                           <?php $s = SUBJECTS::getID($su['subject_ID']);
												echo $s['subject_CODE'];
												 ?> 
                                        
                                        </a></li>
                                    	
                                        <?php } ?>
                                        
                                        
                                       
                                        
                                        </ul>
                       
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table width="100%">
                                <tr>
                                    <td colspan="6">
                                    
                                    
                                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                            <i class="fa fa-folder-o"></i> ROOM SCHEDULE</h3>                    

                                        <?php 
                                        if(isset($_GET['fac_id'])){

                                        
                                        ?>
                                        <span class="pull-right">

                                          <a target="_new" href="subject_sched.php?action=add&fac_id=<?= $_GET['fac_id']; ?>" class="btn btn-success"><i class="fa fa-plus"></i> Add Subject</a>


                                          <a target="_new" href="faculty_loading_print.php?action=view&sem_ID=<?= $_GET['sem_ID'] ?>&fac_id=<?= $_GET['fac_id']; ?>" class="btn btn-success"><i class="fa fa-print"></i> Print</a>


                                        </span>

                                        <?php  } ?>                              
                                      <!--
                                        <div class="col-md-1">   
                                            <button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('?action=add','_self');"><i class='fa fa-plus'></i></button>
                                        </div>
                                    -->                                        </td>
                                  </tr>
                                <tr>
                                  <td width="10%">Semester:</td>
                                  <td width="28%" >
                                  <?php
								  $sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
								  ?>
                                  <form action="<?= $_SERVER['REQUEST_URI']; ?>" method="GET">
                                  <input type="hidden" value="view" name="action" />
                                  
                                  <select class='form-control' name="sem_ID" onchange="this.form.submit()">
								  <option value="">Please Choose</option>
                                		<?php foreach($sem as $key => $val){ ?>
                                        <option <?php if(isset($_GET['sem_ID'])){ if($_GET['sem_ID'] == $val['sem_ID']){ echo " SELECTED=SELECTED "; } } ?> value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
                                        <?php } ?>
                                  </select>
                                  </form></td>
                                  <td width="3%" >&nbsp;</td>
                                  
                                  <?php if(isset($_GET['sem_ID'])){ ?>
                                  <?php $faculty = SEM::allfacultypersem($_GET['sem_ID']);       
								  #print_r($faculty);
								  ?>
                                  <td width="5%">&nbsp;</td>
                                  <td width="31%">&nbsp;</td>
                                  
                                  <?php } ?>
                                  <td width="23%">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>Room:</td>
                                  <td><form action="<?= $_SERVER['REQUEST_URI']; ?>" method="get">
                                      <input type="hidden" value="view" name="action" />
                                      <input type="hidden" value="<?= $_GET['sem_ID']; ?>" name="sem_ID" />

                                      <?php $rooms = ROOM:: getallrooms(array("room_ID"=>"ASC")); ?>

                                      <select class='form-control' name="room_ID" onchange="this.form.submit()">
                                        <option   value="">Please Choose</option>
                                        <?php foreach($rooms as $key => $val){ ?>
                                        <option <?php if(isset($_GET['room_ID'])){ if($_GET['room_ID'] == $val['room_ID']){ echo " SELECTED=SELECTED "; } } ?> value="<?php echo $val['room_ID']; ?>">
                                        <?=  $val['room_NAME']; ?>
                                        </option>
                                        <?php } ?>
                                      </select>




                                  </form></td>
                                  <td >&nbsp;</td>
                                  
                                  <?php if(isset($_GET['fac_id'])){ ?>
                                 
                                  <?php } ?>
                                  <td>&nbsp;</td>
                                </tr>
                                
                                </table>
                          </div>
							
							<div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
                                
                              
                                <?php 
								 if(isset($_GET['room_ID']))
								{
								?>
                                
                <table class="table table-hover table-responsive table-striped text-md">
                    <th style="">#</th>
                    <th style=";">Course</th>
                    <th style="">Section</th>
                    <th style="">Days</th>
                    <th style="">Time</th>
                    <th style="">Faculty</th>
  									<th style="">Enrolled</th>
                                    
                      <?php
									
									$getmysubj = SUBJSCHED::getByRoom($_GET['sem_ID'], $_GET['room_ID']);
									// print_r($getmysubj);
                                    $totalcount = count($getmysubj);
                                    // echo '<script>alert('.$totalcount.')</script>';
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                   // $viewSems = SEMSECTION::getAll(array("semester_section_ID" => "DESC"));
									
									
                                     if(count($getmysubj)>=50) 
                                     {
                                         $pagination2 = new Pagination($getmysubj, 50, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($getmysubj, 50, NULL);
                                     }

                                     $getmysubj = $pagination2->get_array();
                                    # var_dump($getmysubj);
									                   $x = 1;

                                     if($getmysubj) {
                                        foreach($getmysubj as $key => $value){
                                    ?>
                                        <tr>
                                            <td>
											<?php 
												if(isset($_GET['p']))
												{
												$p = $_GET['p'];
												
												$num = 	$x+($p*10)-10;
												echo $num;
												$x++;
												
												}else{
													echo $x++;
												}
											 ?>
                          <td><?php $s = SUBJECTS::getID($value['subject_ID']);
                                echo $s['subject_CODE']."/".$s['subject_DESCRIPTION'];
                               ?> 

                          <td><?php $c=COURSE::getbyID($value['course_ID']); 
											          echo $c['course_INIT']."-";
                                echo $value['semester_section_NAME']; 
                              ?>
                                        
												 </td>
                        <td><?php echo $value['day']; ?> <?php if($value['day2'] != NULL){ ?>/<br /><?php echo $value['day2']; ?><?php } ?></td>
                        <td><?php echo $value['start']; ?>-<?php echo $value['end']; ?>
                            <?php if($value['start2'] != NULL){ ?>/<br /><?php echo $value['start2']; ?>-<?php echo $value['end2']; ?><?php } ?>
                        </td>
                        <td>
                              <?php
                    $ins = INSTRUCTORS::getSingle1($value['instructor_ID']);
          
          echo $ins['instructor_NAME']; echo " "; echo $ins['instructor_LNAME'];
          ?>

                        </td>
                          <td align="center"> <?= SUBJSCHED::checkRemaining($value['subject_sched_ID']); ?>
											
												
                                            </td>
                                  </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
                                Total Records: <?=  $totalcount; ?>
                                        <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view&sem_ID='.$_GET['sem_ID'].'&fac_id='.$_GET['fac_id'].'') ?>
                                        </center>
                                        
                                  <?php }else if(isset($_GET['sem_ID']) && $_GET['sem_ID']!= null){ 

                                    #No Selected Room
                                    ?>  
                                  
                                  <table class="table table-hover table-responsive table-striped text-md">
									
                                    <th style="width:5%;">#</th>
                                    <th style="width:25%;">Room</th>
                  									<th style="width:20%;"><div align="center">No. of Subjects</div></th>
                  									<!-- <th style="width:20%;"><div align="center">Checked Subjects</div></th> -->
                                    <th style=""><div align="center">Operation</div></th>
                                     <?php     
                                  	$rooms =  ROOM:: getallrooms(array("room_ID"=>"ASC"));      
                  // print_r($faculty);
									// print_r($rooms);
                                    ?>    
                                    
                                <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
									 
                                     if(count($rooms)>20) 
                                     {
                                         $pagination2 = new Pagination($rooms, 20, Request::get("p"));
						
                                     }else{
                                         $pagination2 = new Pagination($rooms, 20, NULL);
							
                                     }
			
                                     $rooms = $pagination2->get_array();
									                   $x = 1;
									

                                     if($rooms) {
                                        foreach($rooms as $key => $value){
                                    ?>
                                        <tr>
                                            <td><?php echo $x++; ?>.
                                            <td><?php
                                                #$f = INSTRUCTORS::getSingle1($value['instructor_ID']); 
                                                echo $value['room_NAME']; 
                                                ?>
                                            <td>
                                              <div align="center">
                                  <?php
                                  $noSubjectperRoom = SEM::numofsubjpersemperroom($_GET['sem_ID'],$value['room_ID']);
                                    echo $noSubjectperRoom;
                                    // echo $value['room_ID'];

                                  ?>
                                            </div>
                                            <!-- <td><div align="center"><?php #echo SEM::numofchksubjpersemperfac($_GET['sem_ID'],$value['instructor_ID']); ?> -->
                                            </div>
                                            <td><div align="center"><a href="?action=view&sem_ID=<?= $_GET['sem_ID']; ?>&room_ID=<?= $value['room_ID']; ?>" class='btn btn-warning btn-xs P5C11_VIEWSCHEDULE'>View Schedule</a></div></td>
                                  </tr>
                                    
                                        <?php
                                         } 
                                        }else{
                                    ?>
                                        <tr>
                                          <td colspan="5" align="center">No Faculty</td>
                                        </tr>
                                        <?php } ?>
								</table> 
          <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view&sem_ID='.$_GET['sem_ID']) ?>
                                        </center>
                                
                                        
                                  <?php } ?>      
                           
                </div>
                  <?php  break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
					default: href("?action=view");
				
				
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                	
				    
                 </div>

				    
				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
            

			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
</html>
