<?php 
//INITIALIZE INCLUDES
include('init.php');
#SESSION::CheckLogin();

    //QUERIES
    if(Request::post()){
        switch(Request::get("action")) {
            case "add": 
            #var_dump(Request::post());
            USERS::addstudent(Request::post());
            #var_dump(Request::post());
            break;
            case "edit": USERS::update_student($_GET['id'],Request::post()); break;
            #case "update_blog":  BLOG::update(Request::post("blog_info")); break;
            
            #case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
            
            default: Fail::not_found();
        }
    }
    
    if(Request::get()){
        switch(Request::get("action")) {
            case "delete": ANNOUNCEMENT::delete($_GET['id']); break;
        
        }
    }       
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php include(LAYOUTS . "styles.php"); ?>
    <?php include(LAYOUTS . "scripts.php"); ?>
    <title>Student Information | School Management System v2.0</title>
    <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
</head>

<body>
    <div class="modal fade" id="modal_updateapplicantstatus" tabindex="-1" role="dialog" aria-labelledby="modal_updateapplicantstatus">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h3>Confirmation Box<br><br><small>Do you want to save your changes?</small></h3>
                </div>
                <div class="modal-footer" style="border-top:none; text-align:center !important; padding: 0px 20px 20px !important;">
                    <button type="button" class="btn btn-xs btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-xs btn-primary btnUpdateApplicant">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- top nav -->
    <?php include(LAYOUTS . 'top_nav.php'); ?>
    <!-- end nav -->

    <div class="container">

        <!-- banner -->
        <?php include(LAYOUTS . "banner.php"); ?>
        <!-- end banner -->
        <!-- alert messages -->
        <?php SESSION::DisplayMsg(); ?>

        <div class="row">
            <!-- left nav -->


            <div class="col-md-4 col-lg-3">
                <?php include(PAGES."navigation.php");?>
            </div>

            <!-- end left nav -->

            <!-- BODY -->


            <div class="col-lg-9 ">

            <?php
                switch($_GET['action']){
                case "view":
                $user_id = $user['account_ID'];
                $action_event = "View";
                $event_desc = "MODULE: Registrar / List of Enrolees, DESCRIPTION: User visited List of Enrolees";
                $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
                
                $current_date =  date('Y-m-d H:i:s');
                $CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
                ?>
                <div class="alert-applicant-stauts alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Updated Successfully!</strong>
                    </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <table>
                            <tr>
                                <td width="50%">
                                    <h4 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-user-circle"></i> List of Enrolees <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
                                </td>

                                <td>
                                 <!--    <div class="input-group col-md-12 hide">

                                        <input id="searchItem" class="searchItem form-control" type="text" autofocus="autofocus" style="text-transform: uppercase;" />

                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div> -->

                                </td>
                                <script>
                                    $(".alert-applicant-stauts").slideUp(0);
                                    $(document).ready(function() {


                                                    function postData(){
                                                        var s_Item = $('#searchItem').val();
                                                        if(s_Item.length >= 3)
                                                        {
                                                            console.log("Searching")
                                                            $.ajax({
                                                                    type: "POST",
                                                                    url: "search.php?action=si.php",
                                                                    data: 'search_term=' + s_Item,
                                                                    beforeSend: function ( xhr ) {
                                                                        $("#spinner").show();
                                                                       //Add your image loader here
                                                                    },
                                                                    success: function(msg){
                                                                        /* $('#resultip').html(msg); */
                                                                        $("#spinner").hide();
                                                                        $('#resultip').html(msg); 
                                                                        $("#display_result").show();
                                                                        $("#display_result").html(msg);
                                                                        $("#display_hide").hide();
                                                                        
                                                                    }
                                                    
                                                                })
                                                        }else{
                                                            $("#display_hide").show();
                                                            $("#display_result").hide();
                                                        }
                                                        return false;
                                                    }

                                                    $(function() {
                                                        var timer;
                                                        $("#searchItem").bind('keyup input',function() {
                                                            timer && clearTimeout(timer);
                                                            timer = setTimeout(postData, 300);
                                                        });
                                                    });

                                                });
                                </script>
                            </tr>
                        </table>
                    </div>

                    <div class="panel-body">
                        
                        <div id="display_result"></div>
                        <div id="display_hide">
                            <br><table border="0" cellspacing="5" cellpadding="5" style="width:40%;">
                                <tbody>
                                    <tr>
                                        <td>
                                        <input name='' id="myInput" placeholder="Search by Applicant No., First Name, Last Name or Email" class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
                                        </td>
                                    </tr>
                                </tbody>
                            </table><br>
                            <table id="cadetView" class="display nowrap table table-hover table-responsive table-striped text-md" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Applicant No</th>
                                        <th>First Name</th>
                                        <th>Middle Name</th>
                                        <th>Last Name</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="tbody">

                                </tbody>
                                <!-- <tfoot>
                                            <tr>
                                                <th>Applicant No</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot> -->
                            </table>
                            <script type="text/javascript">
                                 var host = window.location.protocol+"//"+window.location.hostname ;
                                $.ajax({
                                        method: "GET",
                                        // url: "http://20.20.25.41/student-portal-backend/index.php/Magsaysay_controller/fetchallApplicantInfo",
                                        // url: "http://sms.massiveits.com/api/index.php/Magsaysay_controller/fetchallApplicantInfo",
                                        url: host+"/api/index.php/Magsaysay_controller/fetchallApplicantInfo",
                                        dataType: "json",
                                        }).done(function (result, textStatus, jqXHR) {
                                            if(result.status === "SUCCESS"){
                                                
                                                    var data = [];
                                                    var items = [];
                                                    
                                                    
                                                    for (i = 0; i < result.payload.length; i++) {
                                                        if(result.payload[i].applicant_status === "-5" || result.payload[i].applicant_status === "-6" || result.payload[i].applicant_status === "-8" ){
                                                          items.push(result.payload[i]);
                                                          console.log(result.payload[i])
                                                        }
                                                    }



                                                    for (i = 0; i < items.length; i++) {


                                                        if(items[i].applicant_status === "-5"){
                                                            var status1 = "Pending"
                                                        }else if(items[i].applicant_status === "-6"){
                                                            var status1 = "Not Recommended";
                                                        }else{
                                                            var status1 = "Waiting"
                                                        }


                                                        var trc = $('<tr>');
                                                        trc.append($('<td class="row1">'    )
                                                        .append(items[i].applicant_number));
                                                        trc.append($('<td class="row2">')
                                                        .append(items[i].applicant_fname));
                                                        trc.append($('<td class="row3">')
                                                        .append(items[i].applicant_mname));
                                                        trc.append($('<td class="row4">')
                                                        .append(items[i].applicant_lname));
                                                        trc.append($('<td class="row5">')
                                                        .append($('<span>'+status1+'</span>')));
                                                        trc.append($('<td class="row6">')
                                                        .append($('<select class="applicant-status" data-id="'+items[i].applicant_id+'">\
                                                            <option value="0">- Select Status -</option>\
                                                            <option value="-6">Not Recommended</option>\
                                                            <option value="-7">For Enrolment</option>\
                                                            <option value="-8">On Waiting list</option>\
                                                            <select>')))
                    
                                                        $('#tbody').append(trc);
                                                    }
                                                    var dataRow = $("#cadetView").DataTable({
                                                        'destroy': true,
                                                        "scrollY": true,
                                                        "bFilter": true,
                                                        "lengthChange": false,
                                                        "bLengthChange": true,
                                                        fnDrawCallback: function() { 
                                                        }
                                                    });
                                                }
                                                
                                           $("#cadetView").on("change",".applicant-status", function(){
                                            var applicant_id = $(this).data("id");
                                                    var applicant_status = $(this).val();
                                            if(status === "0"){
                                                console.log()
                                            }else{
                                                $("#modal_updateapplicantstatus").modal("show");
                                                $(".btnUpdateApplicant").unbind().bind("click", function(){
                                                    //////////////////////////// Audit Trail Start (Jeelbert) ////////////////////
                                                    var payload = {
                                                        'applicant_ID':applicant_id
                                                    }
                                                     var host = window.location.protocol+"//"+window.location.hostname ;
                                                    // $.main.executeExternalPost('http://20.20.25.41/student-portal-backend/index.php/Magsaysay_controller/fetchApplicantInfoByApplicantID', JSON.stringify(payload)).done(function(result){
                                                    // $.main.executeExternalPost('http://sms.massiveits.com/api/index.php/Magsaysay_controller/fetchApplicantInfoByApplicantID', JSON.stringify(payload)).done(function(result){
                                                    $.main.executeExternalPost(host+'/api/index.php/Magsaysay_controller/fetchApplicantInfoByApplicantID', JSON.stringify(payload)).done(function(result){
                                                        if(result.status ==='SUCCESS'){  
                                                            console.log(result);
                                                            if(applicant_status === "-6"){
                                                                var action = "Not Recommended";
                                                            }else if(applicant_status === "-8"){
                                                                var action = "On Waiting List";
                                                            }else{
                                                                var action = "For Enrolment";
                                                            }
                                                            var wew = $("#myInput").data("acct");
                                                            var ip_add = $("#myInput").data("ip");
                                                            var event_action = "Update";
                                                            var eventdesc = "MODULE: Registrar / List of Enrolees, DESCRIPTION: User set an Action to "+result.payload.applicant_fname+" "+result.payload.applicant_mname+" "+result.payload.applicant_fname+" he/she is now at "+action;

                                                            var api = host+'/api2/wsv1/api/Audit/';
                                                            var payload = {
                                                                'method':'PostAudit',
                                                                'acct_id':wew,
                                                                'client_ip':ip_add,
                                                                'event_action':event_action,
                                                                'eventdesc':eventdesc
                                                            }
                                                            console.log(wew);
                                                            $.main.executeExternalPost(api,JSON.stringify(payload)).done(function(result){
                                                                if(result.status === 'SUCCESS'){
                                                                    console.log(result);
                                                                }else{
                                                                    console.log(result.message);
                                                                }


                                                            });
                                                        }else{
                                                            console.log(result.message);
                                                        }
                                                    });
                                                    //////////////////////////// Audit Trail End (Jeelbert) ///////////////////////


                                                    $(this).off('click');   

                                                    var payload = {
                                                        "applicant_id": applicant_id,
                                                        "applicant_status": applicant_status
                                                    }
                                                    var host = window.location.protocol+"//"+window.location.hostname ;
                                                    
                                                    if(applicant_status === "-7"){
                                                        $(this).off('click');
                                                        $.ajax({
                                                        method: "POST",
                                                        url: host+"/api/index.php/Magsaysay_controller/updateApplicantStatus1",
                                                        // url: "http://sms.massiveits.com/api/index.php/Magsaysay_controller/updateApplicantStatus1",
                                                        // url: "http://20.20.25.41/student-portal-backend/index.php/Magsaysay_controller/updateApplicantStatus1",
                                                        dataType: "json",
                                                        data: JSON.stringify(payload)
                                                        }).done(function (result, textStatus, jqXHR) {
                                                        var payload = {
                                                            "applicant_id": applicant_id,
                                                            "aai_ID": applicant_id
                                                        }
                                                        // console.log(payload)
                                                            $.ajax({
                                                            method: "POST",
                                                            url: host+"/api/index.php/Ams_controller/insertStudentInfo",
                                                            // url: "http://sms.massiveits.com/api/index.php/Ams_controller/insertStudentInfo",
                                                            // url: "http://20.20.25.41/student-portal-backend/index.php/Ams_controller/insertStudentInfo",
                                                            dataType: "json",
                                                            data: JSON.stringify(payload)
                                                            }).done(function (result, textStatus, jqXHR) {
                                                                $("#modal_updateapplicantstatus").fadeOut(1000).modal("hide");
                                                                 $(".alert-applicant-stauts").slideDown(500).delay(2000).slideUp(500)
                                                                setTimeout(function(){
                                                                   window.location.href = host+"/old/enrolment_list_pending.php?action=view"
                                                               },2000);
                                                            }).fail(function (jqXHR, textStatus, errorThrown,request) {
                                                            });
                                                        }).fail(function (jqXHR, textStatus, errorThrown,request) {
                                                        });
                                                    }else{
                                                        $(this).off('click');
                                                        $.ajax({
                                                        method: "POST",
                                                        url: host+"/api/index.php/Magsaysay_controller/updateApplicantStatus1",
                                                        // url: "http://sms.massiveits.com/api/index.php/Magsaysay_controller/updateApplicantStatus1",
                                                        // url: "http://20.20.25.41/student-portal-backend/index.php/Magsaysay_controller/updateApplicantStatus1",
                                                        dataType: "json",
                                                        data: JSON.stringify(payload)
                                                        }).done(function (result, textStatus, jqXHR) {
                                                            $("#modal_updateapplicantstatus").fadeOut(1000).modal("hide");
                                                             $(".alert-applicant-stauts").slideDown(500).delay(2000).slideUp(500)
                                                            setTimeout(function(){
                                                               window.location.href = host+"/old/enrolment_list_pending.php?action=view"
                                                           },2000);
                                                        }).fail(function (jqXHR, textStatus, errorThrown,request) {
                                                        });
                                                    }

                                                    // console.log(payload)
                                                    
                                                });
                                            }
                                            
                                            });
                                            
                                        }).fail(function (jqXHR, textStatus, errorThrown,request) {
                                        });
                                        

                            </script>
                        </div>

                    </div>
                </div>

            <?php
                break;
                case "edit":

            ?>
            <h1>test</h1>
            <?php 
                break;
                default: href("?action=view");

            }

            ?>
                
                <!-- end row container -->
                <!-- footer-->

                <?php include (LAYOUTS . "footer.php"); ?>


                <!-- end footer -->
            </div>
            <!-- end container -->

</body>
<script type="text/javascript">
    function myFunction() {
        var input, filter, table, tr, td, i;
          input = document.getElementById("myInput");
          console.log(input);
          filter = input.value.toUpperCase();
          table = document.getElementById("cadetView");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td2 = tr[i].getElementsByTagName("td")[1];
            td1 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            td4 = tr[i].getElementsByTagName("td")[4];
            if (td) {
              if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1 || td4.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }   
        }
    }
</script>
<style type="text/css">
    .dataTables_filter, .dataTables_info { display: none; }

</style>
</html>

