<?php 
//INITIALIZE INCLUDES
    include('init.php');
SESSION::CheckLogin();

  
   
    
    if(Request::get()){
        switch(Request::get("action")) {
            #case "delete": ANNOUNCEMENT::delete($_GET['id']); break;
        
        }
    }       
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      
      <title>EXPENSE REPORT | School Management System v2.0</title>
      <style>
	  body
	  {
	  font-family:Verdana, Arial, Helvetica, sans-serif;
	  font-size:12px;
	  }
	  </style>
    </head>

    <body>

     
        <div class="container">
            
         
            <!-- row container -->
            <div class="row">

                <!-- left nav -->
         
                
                <!-- BODY -->
                
                
                <div class="col-md-9">
                    <div class="panel panel-default">
                    
                    
                    <?php
                 switch($_GET['action']){
                        case "daily":?>   
                    
                    
                    		<?php
								
							$active = ACCT::getAllActiveExpenseByDateCustom(Request::post());
							#var_dump($active);
							
							?>
                    
                            <div class="panel-heading">
                                <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> DAILY EXPENSE REPORT </h3>
                                <b>Date: <?= date("F j, Y", strtotime($_POST['date'])); ?></b><br />&nbsp;
                            </div>
                            
                            <div class="panel-body">
                                <table style="font-family:verdana;  font-size:12px" width="100%" border="2" cellspacing="0" cellpadding="5" class="">
                              <tr style="font-weight:bold;">
                                    <td style="width:5%;">#</th>
                                    <td style="width:20%;">Particulars</th>
                                    <td style="width:20%;">Department                                    
                                    <td style="width:15%;" align="center">Amount</th>
                                    <td style="width:20%;" align="center">Request By</th>  
                                    <td style="width:20%;" align="center">Received By</th>  
                                    <td style="width:20%;" align="center">Note</th>  
                                <?php $i=1;
										$amt = 0;
								
								  foreach($active as $key => $val){ ?>    
                                <tr>
                                	<td><?= $i++; ?>
                                	<td><?= $val['ae_PARTICULARS']; ?>                                   
                                	<td><?php
										  $dept = DEPT::getdeptbyID($val['department_ID']); 
										  echo $dept['department_NAME'];
										  
										  ?>                                    
                                	<td  align="right">
									
									<?= 
									money($val['ae_AMOUNT']);
										#sprintf("%.2f",$val['ae_AMOUNT']);
									
									$amt += $val['ae_AMOUNT'];
									
									 ?>                                    
                                	<td align="right"><?= $val['ae_REQUEST']; ?>
                                	<td align="right"><?= $val['ae_RECEIVED']; ?>
                                	<td align="right"><?= $val['NOTE']; ?>                                                                                                        
                                <?php } ?>    
                                <tr>
                                	<td>
                                    <td colspan="2" align="right"><b>TOTAL:</b>
                                    <td align="right"><b>
									
								
                                    
                                    <?php echo money($amt);
									?>		
                                    
                                    </b>
                                    <td>
                                    <td>
                                    <td>
                                </table> 
							<br /><b>Prepared By:</b><br /><br />
                            <?= $user['account_FNAME']; ?>  <?= $user['account_LNAME']; ?>
                            <br />
                             <?= $user['access_NAME']; ?>
                            <br /> <br />
                            <i>Printed: <?= date("F j, Y"); ?></i>                            </div>	
                        </div>
						
                        </div>
                       <?php
					   break;
					   case 'monthly': 
					   
				
					   
							# print_r(Request::post());
							   $date = $_POST['date'];
							  # echo $date;
							   #echo date("m/1/Y",strtotime($date));
							   
							   $by = $_POST['by'];
							   
							   $number = cal_days_in_month(CAL_GREGORIAN, date("m",strtotime($date)), date("Y",strtotime($date))); // 31
							?>
							
							
                            
                            
                            
                            
                                <?php 
										$month = date("Y-m",strtotime($date));
										
										#CUSTOM
										if($by == 'custom')
										{
											$show = $_POST['show'];
											$show = explode(",",$show);
											
											
											if(count($show) == 0)
											{
												$record = 0;
												$pages = 1;
											}else{
												$record = count($show);
												$pages = ceil(count($show)/10);
											}
									?>
                                    	  
										 
									<?php
									    }
										#CUSTOM
										else 
										{ 
											$part = ACCT::getAllAEParticularsActiveByMonth($month);
											if( count($part) == 0)
												{	
												$record = 0;
												$pages = 1;
												}
											else{
												$record = count($part);
												$pages = ceil(count($part)/10);
												}	
											?>
                                   	 <!-- ONLY -->
                           		 <?php } ?>
							
              <?php
			  $start = 0;
			  ?>              
                     
                     
                     
                                                       
              <?php  
			  
			  $row = array();
			   $supertotal=0;
			  for($start = 1; $start <= $pages; $start++)
			  {
			  ?>             
              <!-- START -->              
                            <div class="panel-heading">
                                <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> MONTHLY EXPENSE REPORT </h3>
                                <b>Date: <?= date("F j-$number, Y", strtotime($_POST['date'])); ?></b>
                                
                                
              				</div>
                            
							<table border="1" cellspacing="0" cellpadding="1" style=" font-family: verdana;font-size:12px; ">
								<tr>
									<td  align="center">
                                    	<b><?= date("M",strtotime($date)); ?><br /><?= date("Y",strtotime($date)); ?></b>
                                    </td>
									
                                    <?php 
										$month = date("Y-m",strtotime($date));
										
										#CUSTOM
										if($by == 'custom')
										{
											$show = $_POST['show'];
											$show = explode(",",$show);
										#echo ceil(count($show)/3);	
									?>
                                    	  <!-- LOOP -->
                                           <?php
                                           if($start == $pages)
                                           {
										   		$end = count($show)-1;
										   }else{
										   		$end = $start*10-1;
										   }
                                           ?>
                                           
										   <?php for($i=($start*10-10);$i<= $end; $i++) { ?>
                                            <td align="center" width="75px"><b><?= $show[$i]; ?></b></td>
                                               <!-- CUSTOM -->
                                           <?php } ?>
                                           <!-- LOOP -->
									<?php
									    }
										#CUSTOM
										else 
										{ 
										
										$rec = array();
										
											$part = ACCT::getAllAEParticularsActiveByMonth($month);
											foreach($part as $key => $val)
											{
											?>
												<!-- <td align="center" width="75px"><b><?=  $val['ae_PARTICULARS']; ?></b></td> -->
											<?php
												$rec[] = $val['ae_PARTICULARS'];
											}
											
											#print_r($rec);
											?>
                                            
                                            
                                           <?php
                                           if($start == $pages)
                                           {
										   		$end = count($rec)-1;
										   }else{
										   		$end = $start*10-1;
										   }
                                           ?>
                                            
                                            
                                             <?php for($i=($start*10-10);$i<= $end; $i++) { ?>
                                            <td align="center" width="75px"><b><?= $rec[$i]; ?></b></td>
                                               <!-- CUSTOM -->
                                           <?php } ?>
                                           <!-- LOOP -->
                                            
                                   	 <!-- ONLY -->
                           		 <?php } ?>
                                 
                                 
                                 <?php if($start == $pages) { ?>
                                 		 <!-- TOTAL -->
								  		<td width="75px" align="center"><b>TOTAL<br />EXPENSES</b></td>
                                         <!-- TOTAL -->
                                 <?php } ?>        
								</tr>
                                
                              <!--DATA -->  
                             <?php for($x=1;$x<=$number;$x++) { ?>
                                <tr>
                                	<?php 	$total = 0;		?>
                                	<td align="center" style=""><?= $x; ?></td>
									
										<?php 
                                        if($by == 'custom')
										{
											$show = $_POST['show'];
											$show = explode(",",$show);
                                        ?>
                                        <!-- CUSTOM -->
                                        
                                        
                                         <!-- LOOP -->
                                           <?php
                                           if($start == $pages)
                                           {
										   		$end = count($show)-1;
										   }else{
										   		$end = $start*10-1;
										   }
                                           ?>
                                           
										 
                                           <!-- LOOP -->
                                        
                                        
										  <?php for($i=($start*10-10);$i<= $end; $i++) { ?>
                                    <td align="right">
                                                <?php
                                                 $date = $month."-".$x;
												 $day = ACCT::getAllActiveExpenseByParticularDate($show[$i],$date);
												
                                                if(!is_null($day['SUM']))
												{
													$row[$x] += $day['SUM'];
													echo  money($day['SUM']);
												}	
												
												?>
                                                

                                           	<?php } ?>
                                                
                                      <?php }else{ ?>
                                           
                                      <?php     
											
											
											$rec = array();
										
											$part = ACCT::getAllAEParticularsActiveByMonth($month);
											
											?>
                                            
                                            
                                           
                                            
                                            
                                           
                                           <?php 
								
                                            
                                            
                                            foreach($part as $key => $val)
											{
											?>
												<!-- <td align="center" width="75px"><b><?=  $val['ae_PARTICULARS']; ?></b></td> -->
											<?php
												$rec[] = $val['ae_PARTICULARS'];
											}
											
											#print_r($rec);
											?>
                                            <?php
                                           if($start == $pages)
                                           {
										   		$end = count($rec)-1;
										   }else{
										   		$end = $start*10-1;
										   }
                                           ?>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            <?php for($i=($start*10-10);$i<= $end; $i++) { ?>
									 			 <td align="right">
														
														<?php
														 $date = $month."-".$x;
														 $day = ACCT::getAllActiveExpenseByParticularDate($rec[$i],$date);
														
															if(!is_null($day['SUM']))
															{
																$row[$x] += $day['SUM'];
																echo money($day['SUM']);
															}	
													
												} 		
														?>
														
											
                                          
                                               <!-- CUSTOM -->
										   
										   
										   
                                           
                                           
                                   <?php } //ENDIF ?>
                                 
                                 
										 <?php if($start == $pages) { ?>
                                         <!-- TOTAL -->
                                          <td align="right">
                                                <?= money($row[$x]); ?>
                                         <!-- TOTAL -->  
                                    	 <?php } ?>
									
								  <?php } ?>  
							    
                                
                                
                                
                                
                                <!-- LAST ROW -->
                                <tr>

							      <td align="center"> Total    
                                  
                                                             
							      <?php 
									$month = date("Y-m",strtotime($date));
									if($by == 'custom')
									{
										$show = $_POST['show'];
										$show = explode(",",$show);
									?>
                                    <?php
                                           if($start == $pages)
                                           {
										   		$end = count($show)-1;
										   }else{
										   		$end = $start*10-1;
										   }
                                           ?>
                                    
                                    
                                   <?php for($i=($start*10-10);$i<= $end; $i++) { ?>
                                  <td align="right" width="75px"><b>
                                        <!-- CUSTOM -->
                                        <?php
										$t = ACCT::getAllAEParticularsActiveByMonthByParticulars($month,$show[$i]);
										 $show[$i]; 
										 #print_r($t);
										 $supertotal += $t['SUM'];
										 echo money($t['SUM']);
										?></b>
                                   <?php } ?>
                                  		
									<?php }else{ 
									
									
									
									
									$rec = array();
										
									$part = ACCT::getAllAEParticularsActiveByMonth($month);
									foreach($part as $key => $val)
											{
											?>
												<!-- <td align="center" width="75px"><b><?=  $val['ae_PARTICULARS']; ?></b></td> -->
											<?php
												$rec[] = $val['ae_PARTICULARS'];
											}
											
											#print_r($rec);
											?>
                                            
                                            
                                           <?php
                                           if($start == $pages)
                                           {
										   		$end = count($rec)-1;
										   }else{
										   		$end = $start*10-1;
										   }
                                           ?>
                                            
                                            
                                             <?php for($i=($start*10-10);$i<= $end; $i++) { ?>
                                          
                                               <!-- CUSTOM -->
                                               <?php
                                              $t = ACCT::getAllAEParticularsActiveByMonthByParticulars($month,$rec[$i]);
										 $show[$i]; 
										 #print_r($t);
										 $supertotal += $t['SUM'];
										 ;
										?>
                               	  <td align="right" width="75px"><b><?= money($t['SUM']); ?></b>
                                        <?php 
                                               
                                               
                                               
                                            } ?>
									
								
									
									
									
                                    
                                    <?php } ?>
                                    
                                    					<!-- TOTAL -->
                                                       <?php if($start == $pages) { ?>
                                                      <td width="75px" align="right"><b><?= money($supertotal); ?></b></td>        
                                                      <!-- TOTAL -->                     
                                                      <?php } ?>
                                  </table>                
			 
             <?php if($start != $pages) { ?>
             <br />
             <?php if(date("m",strtotime($_POST['date'])) == '02'){ echo "<br><br>"; } ?>
             <?php } ?>
             
             <!-- END -->
             <?php } ?>
             
					
			 
              
              			   
      <?php    
					   break;
					   
					 }  
					   ?> 
                        
                       
                    
                </div>
                </div>
            <!-- end row container -->
            
            

            <!-- footer-->      
            
          

        </div>
        <!-- end container -->

    </body>
</html>
