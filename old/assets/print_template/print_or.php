<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<style type="text/css">
		body{
			margin: 0px;
			padding: 0px;
			font-size: 12px;
		}
		img{
			position: absolute; 
			top: -5px; 
			left: -5px; 
			z-index: 9998; 
			width:768px;
			height:480px;
			opacity: 0.3;
		}
        .paymentType:nth-of-type(1){
            position: absolute;
            top:310px;
            left:165px;
        }
        .paymentType:nth-of-type(2){
            position: absolute;
            top:325px;
            left:165px;
        }
        .paymentType:nth-of-type(3){
            position: absolute;
            top:340px;
            left:165px;
        }
        
		@media screen{
            .checkSymbol:nth-of-type(1){
                position: absolute;
                top: 300px;
                left: 170px;
            }
            .checkSymbol:nth-of-type(2){
                position: absolute;
                top:315px;
                left:170px;
            }
            .checkSymbol:nth-of-type(3){
                position: absolute;
                top:330px;
                left:170px;
            }
			#cancel{
				position: absolute;
				top: 80%;
				left: 24%;
				cursor: pointer;
			}
			#print{
				position: absolute;
				top: 80%;
				left: 27%;
				cursor: pointer;
			}
			#or_paper{
				position: absolute;
				width: 768px;
				height: 480px;
				z-index: 9999;
				border: 1px solid black;
			}
            #txtStudNumber{
                position: absolute;
                top: 152px;
                left: 244px;
                font-size: 15px;
            }
            #txtStudName{
                position: absolute;
                top: 180px;
                left: 350px;
                font-size: 15px;
            }
            #txtMonth{
                position: absolute;
                top: 162px;
                left: 538px;
                font-size: 15px;
            }
            #txtYear{
                position: absolute;
                top: 162px;
                left: 705px;
                font-size: 15px;
            }
            #txtAmount{
                position: absolute;
                top: 280px;
                left: 605px;
                font-size: 15px;
            }
            #txtPOname{
                position: absolute;
                top: 307px;
                left: 390px;
                font-size: 12px;
            }
            #txtAmountToWords{
                position: absolute;
                top: 255px;
                left: 310px;
                font-size: 14px;
                text-transform: uppercase;
            }
		}
		@media print{
            .checkSymbol:nth-of-type(1){
                position: absolute;
                top: 300px;
                left: 170px;
            }
            .checkSymbol:nth-of-type(2){
                position: absolute;
                top:315px;
                left:170px;
            }
            .checkSymbol:nth-of-type(3){
                position: absolute;
                top:330px;
                left:170px;
            }
			#or_picture, #cancel, #print{
				display: none;
			}
			#or_paper{
				position: absolute;
				width: 768px;
				height: 480px;
				z-index: 9999;
				border: 1px solid black;
			}
            #txtStudNumber{
                position: absolute;
                top: 152px;
                left: 244px;
                font-size: 15px;
            }
            #txtStudName{
                position: absolute;
                top: 180px;
                left: 350px;
                font-size: 15px;
            }
            #txtMonth{
                position: absolute;
                top: 162px;
                left: 538px;
                font-size: 15px;
            }
            #txtYear{
                position: absolute;
                top: 162px;
                left: 705px;
                font-size: 15px;
            }
            #txtAmount{
                position: absolute;
                top: 280px;
                left: 605px;
                font-size: 15px;
            }
            #txtPOname{
                position: absolute;
                top: 307px;
                left: 390px;
                font-size: 12px;
            }
            #txtAmountToWords{
                position: absolute;
                top: 255px;
                left: 310px;
                font-size: 14px;
                text-transform: uppercase;
            }
		}
		@page {
			size: auto !important;  
			margin: 0mm !important;
		}
	</style>
</head>
<body>
<div id="or_paper">
    <p class="checkSymbol">&#x2714;</p>
    <p class="checkSymbol">&#x2714;</p>
    <p class="checkSymbol">&#x2714;</p>
    <p id="txtStudNumber"> <?= $_GET['stud_number']; ?></p>
    <p id="txtStudName"> <?= $_GET['stud_name']; ?></p>
    <p id="txtAmount"> <?= number_format($_GET['amount']); ?>.00</p>
    <p id="txtMonth"> <?= date("F d"); ?></p>
    <p id="txtYear"> <?= date("y"); ?></p>
    <input class="paymentType" type="radio" name='paymentType' value="cash"> 
    <input class="paymentType" type="radio" name='paymentType' value="check">
    <input class="paymentType" type="radio" name='paymentType' value="bank">
    <p id="txtPOname"> <?= $_GET['poNAME']; ?></p>
    <p id="txtAmountToWords"> <?= $_GET['amounttowords']; ?> ONLY</p>
</div>
<img id="or_picture" src="or_template1.png"/>
<a href="#" id="cancel" onclick="cancel()">[cancel]</a>
<a href="" id="print" onclick="myFunction()">[print reciept]</a>
<script type="text/javascript">
    $(".checkSymbol").eq(0).hide()
    $(".checkSymbol").eq(1).hide()
    $(".checkSymbol").eq(2).hide()
	function myFunction(){
        $(".paymentType").hide()
        if($(".paymentType:checked").val() === "cash"){
            $(".checkSymbol").eq(0).show()
        }else if($(".paymentType:checked").val() === "check"){
            $(".checkSymbol").eq(1).show()
        }else{
            $(".checkSymbol").eq(2).show()
        }
		window.print();
	}
	function cancel(){
		var r = confirm("Are you sure you want to cancel?");
		if (r == true) {
			window.location = window.origin+"/old/"+"accounting.php?action=view&id=<?= $_GET['id']; ?>"
		} else {
			
		}
	}
</script>
</body>
</html>