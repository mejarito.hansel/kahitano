<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		body{
			padding:0px;
			font-family: Arial;
			font-size:12px;
		}
		@media screen{
			#print{
				position:absolute;
				right:10px;
			}
			.a4-container {
				position: absolute;
				top: 0px;
				left: 0px;
				width: 595px;
				height: 842px;
				border: 3px solid black;
				/*background: red;*/
			}
			.underline:nth-of-type(1){
				position:absolute;
				top: 12.5rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.underline:nth-of-type(2){
				position:absolute;
				top: 13.7rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.underline:nth-of-type(3){
				position:absolute;
				top: 15rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.hr{
				position: absolute;
				top: 570px;
				left: -3px;
				width: 595px;
				height: 80px;
				border: 3px solid black;
			}
			table{
				position:absolute;
				top: 5.5rem;
				left: 12rem;
				border: 2px solid black;
				padding: 0px;
				border-spacing: 0px;
			}
			table td{
				padding:.1rem;
				width:230px;
				border: 1.4px solid black;
			}
			table tr td p{
				margin: 0px !important;
				padding: 0px !important;
			}
		}
		@media print{
			#print{
				display:none !important;
			}
			.a4-container {
				position: absolute;
				top: 0px;
				left: 0px;
				width: 595px;
				height: 842px;
				border: 3px solid black;
			}
			.underline:nth-of-type(1){
				position:absolute;
				top: 12.5rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.underline:nth-of-type(2){
				position:absolute;
				top: 13.7rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.underline:nth-of-type(3){
				position:absolute;
				top: 15rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.hr:nth-of-type(1){
				position: absolute;
				top: 570px;
				left: -3px;
				width: 595px;
				height: 80px;
				border: 3px solid black;
			}
		}
		@page {
			margin-top: 1in;
			margin-left: 1.10in;
		}
	</style>
</head>
<body>
	<div class="a4-container">
			<div style="margin-top:1rem;"></div>
			<p style="margin-left:1.5rem; line-height:1.25rem;">MOL Magsaysay Maritime Academy Inc.<br>Statement of Accounts<br>FTM August 2018</p>
			
			<p style="margin-left:1.5rem; line-height:1.25rem;">Student #<br>Student Name:<br>SNPL Approval:</p>
			<br>
			<p style="margin-left:1.5rem; line-height:1.25rem;">Total Assessed Tuition and Other Fees<br>Amount extended to SNPL<br>Collectible from Student -</p>
			<table>
				<tr>
					<td>
						<p style="text-align:center;">{}</p>		
					</td>
				</tr>
				<tr>
					<td>
						<p>{}</p>		
					</td>
				</tr>
				<tr>
					<td>
						<p>{}</p>		
					</td>
				</tr>
			</table>
			<p style="position:absolute; top:10.8rem; left:28.5rem;">{}</p><span class="underline"></span>
			<p style="position:absolute; top:12rem; left:28.5rem;">{}</p><span class="underline"></span>
			<p style="position:absolute; top:13.3rem; left:28.5rem;">{}</p><span class="underline"></span>
			<br>
			<p style="margin-left:1.5rem; line-height:1.25rem;">Down Payment/ Payments Made</p>
			<p style="margin-top:-10px; margin-left:7.5rem; line-height:1.25rem; float:left;">Date</p>
			<p style="margin-top:-10px; margin-left:10.5rem; line-height:1.25rem; float:left;">OR #</p>
			<p style="margin-top:-10px; margin-left:6.5rem; line-height:1.25rem; float:left;">Amount</p>
			
			<p style="margin-top: -10px; margin-left:7rem; line-height:1.25rem; float:left;">{}</p>
			<p style="margin-top: -10px; margin-left:10.5rem; line-height:1.25rem; float:left;">{}</p>
			<p style="margin-top: -10px; margin-left:8.5rem; line-height:1.25rem; float:left;">{}</p>
			<br>
			<br>
			<br>
			<br>
			<p style="margin-left:1.5rem; line-height:1.25rem; float:left;">Balance</p>
			<p style="margin-left:24.5rem; line-height:1.25rem; float:left;">{}</p>

			<br>
			<br>
			<br>
			<br>
			<p style="margin-left:1.5rem; line-height:1.25rem; float:left;">Due Dates</p>
			<p style="margin-left:1rem; line-height:1.25rem; float:left; font-weight: bold;">ON or BEFORE</p>

			<br>
			<br>
			<br>
			<br>
			<p style="margin-top:-10px; margin-left:6.5rem; line-height:1.25rem; float:left;">{}</p>
			<p style="margin-top:-10px; margin-left:21.5rem; line-height:1.25rem; float:left;">{}</p>
			<br>
			<br>
			<p style="margin-top:-14px; margin-left:6.5rem; line-height:1.25rem; float:left;">{}</p>
			<p style="margin-top:-14px; margin-left:21.5rem; line-height:1.25rem; float:left;">{}</p>
			<br>
			<br>
			<p style="margin-top:-18px; margin-left:6.5rem; line-height:1.25rem; float:left;">{}</p>
			<p style="margin-top:-18px; margin-left:21.5rem; line-height:1.25rem; float:left;">{}</p>

			<div class="hr">
				<p style="margin-top:1rem; margin-left:2rem; float:left;">You may send your payment thru MMMA BPI Account S/A#</p>
				<p style="margin-top:1rem; margin-left:1rem; float:left;">{}</p>
				<br>
				<br>
				<p style="margin-top:1rem; margin-left:1rem;">Please send us copy of Deposit Slip for proper posting and updating of your account.</p>
			</div>
			<p style="margin-top:8rem; margin-left:1rem; font-weight:bold;">REMINDERS:</p>
			<p style="margin-top:1rem; margin-left:1rem;">* NO DP/Reservation Received</p>
			<p style="margin-top:1rem; margin-left:1rem; font-weight:bold;">*Pending SNP: Requirements: PLS SEND to MMMA SNPL Agreement with signature of CO-MAKER</p>
			<p style="margin-top:1rem; margin-left:1.5rem;">*None submission of Requirements will result to full assessment of tuition fee due and demandable</p>
			<p style="margin-top:1rem; margin-left:12.5rem;">next month/cut off.</p>
			<p style="margin-top:1rem; margin-left:1rem;">*Late payment is subject to penalty.</p>
	</div>
	<button id="print" onclick="myFunction()">print</button>
	<script type="text/javascript">
		function myFunction() {
    window.print();
}
	</script>
</body>
</html>