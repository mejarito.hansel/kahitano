
$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.lock = (typeof $.main.lock !=='undefined')? $.main.lock : {};

$.main.lock =(function() {
	var host = window.location.protocol+"//"+window.location.hostname ;
	var path = host+"/api/index.php/Ams_controller/";
    // var path = "http://20.20.25.41/student-portal-backend/index.php/Ams_controller/"
 
	var __trimesterTbl = function() {
		console.log("attached events")

	  	$.main.executeExternalPost(path+"fetchAllSem").done(function(result) {
	    console.log(result);
	    var items = result.payload;
	    																																																																																																	
	    if ( $.fn.DataTable.isDataTable('#myTable') ) {
            $('#myTable').DataTable().destroy();
            $('#myTable tbody').empty();
        }
	    for(i=0; i<items.length; i++){

         	var trc = $('<tr class='+result.payload[i].sem_ID+'>');

			var btnEdit = $('<button class="btn btn-warning btn-xs P2C9_MARKSCEDULE btnEdit id="btnEdit" data-sid="'+ result.payload[i].sem_ID +'" >Edit</button>')
			// var btnLock = $('<button class="btn  btn-success btn-xs P2C9_MARKSCEDULE btnLock id="btnLock" data-sid="'+ result.payload[i].sem_ID +'" >Lock</button>')
			var btnCheck_Lock = $('<button class="btn btn-success btn-xs P2C9_MARKSCEDULE btnCheck_Lock id="btnCheck_Lock" data-name="'+result.payload[i].sem_NAME+'" data-sid="'+ result.payload[i].sem_ID +'" >Lock&Check</button>')
			var btnUnCheck_UnLock = $('<button class="btn btn-success btn-xs P2C9_MARKSCEDULE btnUnCheck_UnLock id="btnUnCheck_UnLock" data-name="'+result.payload[i].sem_NAME+'" data-sid="'+ result.payload[i].sem_ID +'" >Unlock&Uncheck</button>')
			var btnCUL = $('<button class="btn btn-success btn-xs P2C9_MARKSCEDULE btnCUL id="btnCUL" data-name="'+result.payload[i].sem_NAME+'" data-sid="'+ result.payload[i].sem_ID +'" >Check</button>')
			// var btnCheck = $('<button class="btn btn-success btn-xs P2C9_MARKSCEDULE btnCheck id="btnCheck" data-sid="'+ result.payload[i].sem_ID +'" >Check</button>')
			var btnLUC = $('<button class="btn btn-success btn-xs P2C9_MARKSCEDULE btnLUC id="btnLUC" data-name="'+result.payload[i].sem_NAME+'" data-sid="'+ result.payload[i].sem_ID +'" >Lock</button>')

	       	trc.append($('<td class="row1">')
	          .append(result.payload[i].sem_NAME));

	       	var status = result.payload[i].sem_lock_unlocked_check_unchecked
	       	if (status == 0) {
	       		status = "Unlocked&Unchecked"
	       	}
	       	else if (status == 1) {
	       		status = "Locked&Checked"
	       	}
	       	else if (status == 2) {
	       		status = "Checked"
	       	}
	       	else if (status == 3) {
	       		status = "Locked"
	       	}else{
	       		status = ""
	       	}

	       	trc.append($('<td class="row2">')
	          .append(status));

	       	trc.append($('<td class="row3">')
	          .append(btnEdit)
	          .append(" ")
	          .append(btnCheck_Lock)
	          .append(" ")
	          .append(btnUnCheck_UnLock)
	          .append(" ")
	          .append(btnCUL)
	          .append(" ")
	          .append(btnLUC)
	         );

	       	$('#tbody').append(trc);
      	}

	      	var dataRow = $("#myTable").DataTable(
	      	{
		        'destroy': true,
		        "scrollY": true,
		        "bFilter": false, 
		        "bLengthChange": false,
		        'scrollX' : true,		       
		        fnDrawCallback : function(){

            		$("#myTable tbody").on("click", ".btnCheck_Lock", function(){
                		sid = $(this).data("sid")
                		name = $(this).data("name")
                		$("#modalLockCheck").modal()
        				$("#Bsuccess").addClass("hide")
                		$(".txt").html("Are you sure you want to lock and check " + name + "?")
                		$("#btnConfirm").removeClass("hide")
                		$("#btnConfirm1").addClass("hide")
                		$("#btnConfirm2").addClass("hide")
                		$("#btnConfirm3").addClass("hide")

	            		$("#btnConfirm").on("click", function(){
	          				var payload = {
	          					"sem_ID" : sid,
								"lock": "1",
								"check": "1"
	          				}
	            			// console.log(payload)
		  					$.main.executeExternalPost(path+"update", JSON.stringify(payload)).done(function(result) {
	            				console.log(result)
                				$("#Bsuccess").removeClass("hide")
	            				__trimesterTbl()
            				 	setTimeout(function() {
	            					location.reload()
                				},600);
	        				})

	            			var payload2 = {
								"sem_ID": sid,
								"sem_lock_unlocked_check_unchecked": "1"
	            			}
		  					$.main.executeExternalPost(path+"updateTrisemStatus", JSON.stringify(payload2)).done(function(result) {
	            			// console.log(result)
	            			// __trimesterTbl()
	            			})
            			})
            		})
            		$("#myTable tbody").on("click", ".btnUnCheck_UnLock", function(){
                		sid = $(this).data("sid")
                		name = $(this).data("name")
                		$("#modalLockCheck").modal()
        				$("#Bsuccess").addClass("hide")
                		$(".txt").html("Are you sure you want to unlock and uncheck " + name + "?")
                		$("#btnConfirm").addClass("hide")
                		$("#btnConfirm1").removeClass("hide")
                		$("#btnConfirm2").addClass("hide")
                		$("#btnConfirm3").addClass("hide")
	            		$("#btnConfirm1").on("click", function(){

	          				var payload = {
	          					"sem_ID" : sid,
								"lock": "0",
								"check": "0"
	          				}
	            			// console.log(payload)
		  					$.main.executeExternalPost(path+"update", JSON.stringify(payload)).done(function(result) {
	            				console.log(result)
                				$("#Bsuccess").removeClass("hide")
	            				__trimesterTbl()
            				 	setTimeout(function() {
	            					location.reload()
                				},600);
	        				})

	            			var payload2 = {
								"sem_ID": sid,
								"sem_lock_unlocked_check_unchecked": "0"
	            			}
		  					$.main.executeExternalPost(path+"updateTrisemStatus", JSON.stringify(payload2)).done(function(result) {
	            			// console.log(result)
	            			// __trimesterTbl()
	            			})
	            		})
            		})
            		$("#myTable tbody").on("click", ".btnCUL", function(){
                		sid = $(this).data("sid")
                		name = $(this).data("name")
        				$("#Bsuccess").addClass("hide")
                		$("#modalLockCheck").modal()
                		$(".txt").html("Are you sure you want to check " + name + "?")
                		$("#btnConfirm").addClass("hide")
                		$("#btnConfirm1").addClass("hide")
                		$("#btnConfirm2").removeClass("hide")
                		$("#btnConfirm3").addClass("hide")
	            		$("#btnConfirm2").on("click", function(){
	          				var payload = {
	          					"sem_ID" : sid,
								"lock": "0",
								"check": "1"
	          				}
	            			// console.log(payload)
		  					$.main.executeExternalPost(path+"update", JSON.stringify(payload)).done(function(result) {
	            				console.log(result)
                				$("#Bsuccess").removeClass("hide")
	            				__trimesterTbl()
            				 	setTimeout(function() {
	            					location.reload()
                				},600);
	        				})

	            			var payload2 = {
								"sem_ID": sid,
								"sem_lock_unlocked_check_unchecked": "2"
	            			}
		  					$.main.executeExternalPost(path+"updateTrisemStatus", JSON.stringify(payload2)).done(function(result) {
	            			// console.log(result)
	            			// __trimesterTbl()
	            			})
            			})
            		})
            		$("#myTable tbody").on("click", ".btnLUC", function(){
                		sid = $(this).data("sid")
                		$("#modalLockCheck").modal()
                		name = $(this).data("name")
        				$("#Bsuccess").addClass("hide")
                		$(".txt").html("Are you sure you want to lock " + name + "?")
                		$("#btnConfirm").addClass("hide")
                		$("#btnConfirm1").addClass("hide")
                		$("#btnConfirm2").addClass("hide")
                		$("#btnConfirm3").removeClass("hide")
	            		$("#btnConfirm3").on("click", function(){
	          				var payload = {
	          					"sem_ID" : sid,
								"lock": "1",
								"check": "0"
	          				}
	            			// console.log(payload)
		  					$.main.executeExternalPost(path+"update", JSON.stringify(payload)).done(function(result) {
	            				console.log(result)
                				$("#Bsuccess").removeClass("hide")
	            				__trimesterTbl()
            				 	setTimeout(function() {
	            					location.reload()
                				},600);
	        				})

	            			var payload2 = {
								"sem_ID": sid,
								"sem_lock_unlocked_check_unchecked": "3"
	            			}
		  					$.main.executeExternalPost(path+"updateTrisemStatus", JSON.stringify(payload2)).done(function(result) {
	            			// console.log(result)
	            			// __trimesterTbl()
	            			})
            			})
            		})
            		$("#myTable tbody").on("click", ".btnEdit", function(){
						$("#Usuccess").addClass("hide")
						$("#requiredfield").addClass("hide")
						$("#exstU").addClass("hide")
		          		console.log("clicked")
                		sid = $(this).data("sid")
		          		console.log(sid)
          				$("#modalUpdateTri").modal();
          				var payload = {
          					"sem_ID" : sid
          				}
	  					$.main.executeExternalPost(path+"fetchAllSemByID", JSON.stringify(payload)).done(function(result) {
	  						// console.log(result)
	  						var item = result.payload
		  						$("#UtriName").val(item.sem_NAME)
		  						$("#UtriStats").val(item.sem_STATUS)
								$("#UtriEnrol").val(item.sem_ENROLMENT)
								$("#UencodeGrades").val(item.sem_ENCODING)
								$("#UtriLoading").val(item.sem_LOADING)

	    					$("#btnUpdate").unbind("click").on("click", function(){
								$("#exstU").addClass("hide")
									
								var payload = {
          							"sem_ID" : sid,
									"sem_NAME" : $("#UtriName").val(),
									"sem_STATUS" : $("#UtriStats").val(),
									"sem_ENROLMENT" : $("#UtriEnrol").val(),
									"sem_ENCODING" : $("#UencodeGrades").val(),
									"sem_LOADING" : $("#UtriLoading").val()
								}
	          					if(payload.sem_NAME.length == "" || payload.sem_NAME.length == undefined) {
									$("#requiredfield").removeClass("hide")
									$("#exstU").addClass("hide")
	          					}else{
		  							$.main.executeExternalPost(path+"updateTrimester", JSON.stringify(payload)).done(function(result) {
		  							console.log(result)

			  							if (result.status == "ERROR") {
											$("#exstU").removeClass("hide")
											$("#requiredfield").addClass("hide")
			  							}else{
											$("#requiredfield").addClass("hide")
				  							$("#Usuccess").removeClass("hide")
			          						__trimesterTbl()
			            				 	setTimeout(function() {
				            					location.reload()
			                				},600);
			  							}

									})
	          					}
							})
	  					})
		          	})
		      	}
	  		})

    		$(".btnAddTri").on("click", function(){
				$("#Arequiredfield").addClass("hide")
				$("#Asuccess").addClass("hide")
				$("#btnSaveM").addClass("hide")
				$("#triName").val("")
				$("#btnSave").removeClass("hide")
				$("#exst").addClass("hide")

				$('#triName').prop('disabled', false);
				$('#triStats').prop('disabled', false);
				$('#triEnrol').prop('disabled', false);
				$('#encodeGrades').prop('disabled', false);
				$('#triLoading').prop('disabled', false);

          		$("#modalNewTri").modal()

	    		$("#btnSave").unbind("click").on("click", function(){
				$("#btnSave").addClass("hide")

	          		var payload = {
						"sem_NAME" : $("#triName").val(),
						"sem_STATUS" : $("#triStats").val(),
						"sem_ENROLMENT" : $("#triEnrol").val(),
						"sem_ENCODING" : $("#encodeGrades").val(),
						"sem_LOADING" : $("#triLoading").val()
	          		}
	          		if(payload.sem_NAME.length == "" || payload.sem_NAME.length == undefined) {
						$("#Arequiredfield").removeClass("hide")
						$("#exst").addClass("hide")
						$("#btnSave").removeClass("hide")
	          		}else{

		  				$.main.executeExternalPost(path+"insertTrimester", JSON.stringify(payload)).done(function(result) {
	          			console.log(result);

	          			if (result.status == "FAILED") {
							$("#exst").removeClass("hide")
							$("#Arequiredfield").addClass("hide")
							$("#btnSave").removeClass("hide")
							
	          			}else{

							$("#exst").addClass("hide")
							$("#Asuccess").removeClass("hide")
							$("#Asuccess").removeClass("hide")
							$("#btnSaveM").removeClass("hide")
							$("#Arequiredfield").addClass("hide")

		    				$('#triName').prop('disabled', true);
		    				$('#triStats').prop('disabled', true);
		    				$('#triEnrol').prop('disabled', true);
		    				$('#encodeGrades').prop('disabled', true);
		    				$('#triLoading').prop('disabled', true);
		          			__trimesterTbl()
	          			}

		  				})
	          		}
	          	})
	    		$("#btnSaveM").unbind("click").on("click", function(){
					$("#btnSaveM").addClass("hide")

					$("#exst").addClass("hide")
					$("#Asuccess").addClass("hide")
					$("#triName").val("")
    				$('#triName').prop('disabled', false);
    				$('#triStats').prop('disabled', false);
    				$('#triEnrol').prop('disabled', false);
    				$('#encodeGrades').prop('disabled', false);
    				$('#triLoading').prop('disabled', false);
					$("#btnSave").removeClass("hide")

	          	})
          	})

		})
	}
    return {
        trimesterTbl: __trimesterTbl
    };

}());
