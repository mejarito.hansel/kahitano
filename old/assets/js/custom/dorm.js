$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.dorm = (typeof $.main.dorm !=='undefined')? $.main.dorm : {};

$.main.dorm = (function() {
    var host = window.location.protocol+"//"+window.location.hostname ;
    // var API = "http://sms.massiveits.com/api2/wsv1/api/dorm/"
    var API = host+"/api2/wsv1/api/dorm/";
    var name = $(".navbar.navbar-fixed-top").find("#loginB a.dropdown-toggle").text().replace(/\n\t\t\t\t\t\t\t/g, '').replace(/   \n                             /g, '');
    var dorm_id;
    var dorm_name; 
   // var aud_path = "http://sms.massiveits.com/api/index.php/Magsaysay_controller/"
    var aud_path = host+"/api2/wsv1/api/Audit/";
    var __AddDorm = function() {

        $("#btnDorm").on('click', function() {
            $("#modalDorm").modal('show')
            $(".alert").addClass("hidden");
            console.log("test1")
        });

        $("#saveDormBtn").on('click', function() {
        
        if((($("#dormNumber").val() <= "0")||($("#dormNumber").val() === ""))&&(($("#dormCap").val() <= "0")||($("#dormCap").val() === ""))){
          console.log("0")
          // $("#dormNumber").addClass("has-error");
          // $("#dormCap").addClass("has-error");
          $(".alert").removeClass("hidden");
          $(".alert").html('PLEASE FILL UP THE REQUIRED FIELD')


        }else if(($("#dormNumber").val() <= "0")||($("#dormNumber").val() === "")){
          console.log("1")
          $(".alert").removeClass("hidden");
          $(".alert").html('');
          $(".alert").html('PLEASE FILL UP ROOM NUMBER')

        $("#dormNumber").addClass("is-invalid");

        }else if(($("#dormCap").val() <= "0")||($("#dormCap").val() === "")){
           console.log("2")
          $(".alert").removeClass("hidden");
          $(".alert").html('');
          $(".alert").html('PLEASE FILL UP ROOM CAPACITY')
    
        }else{


            let payload = {
                "method": "checkRoomIfexist",
                "roomname": $("#dormNumber").val(),
                "capacity": $("#dormCap").val(),
                "created_by": name
            }
            //console.log(payload)
            $.main.executeExternalPost(API, JSON.stringify(payload)).done(function(result) {
                //console.log(result.status)
                if (result.status === "AVAILABLE") {
                    let payload = {
                        "method": "addnewroom",
                        "roomname": $("#dormNumber").val(),
                        "capacity": $("#dormCap").val(),
                        "created_by": name
                    }
                    // console.log(payload)
                    $.main.executeExternalPost(API, JSON.stringify(payload)).done(function(result) {
                        console.log(result)
                        $(".success").removeClass("hidden");
                        $(".success").html('');
                        $(".success").html('SUCCESS ADDING DORM NUMBER')

                var acct_id = $("#getaccountidforDorm").data('acct');
                var acct_ip = $("#getaccountidforDorm").data('acctip');
                            var payload1 = {
                                    "method" : "PostAudit",
                                    "acct_id": acct_id, 
                                    "client_ip": acct_ip,
                                    "event_action":"ADDED",
                                    "eventdesc": "MODULE: Dorm Management, <br> DESCRIPTION: User Added New Dorm Room No : " +   
                                    $("#dormNumber").val() + "<br> Capacity: "+ $("#dormCap").val()

                            };
                           //console.log(payload1)
                            $.main.executeExternalPost(aud_path, JSON.stringify(payload1)).done(function(result){
                                  if(result.status == "SUCCESS"){
                                                 setTimeout(function(){
                                                  $(".success").html('');
                                                  $(".success").addClass("hidden");
                                                  $("#modalDorm").modal('hide')
                                                  
                                                   location.reload();
                                                }, 600);

                                          }

                                });


                       

                    })


                } else {
                   $(".alert").removeClass("hidden");
                    $(".alert").html('');
                    $(".alert").html('ROOM NUMBER ALREADY EXIST')

                }


            })

          }


        });

    }

    var __getStudentsWithoutDorm = function(){

              let payload = {
            "method": "getallenrolledandnodormassigned"
        }
          $.main.executeExternalPost(API, JSON.stringify(payload)).done(function(result) {
            if(result.status == "SUCCESS"){
                $("#SemofStudent").empty().append('<option>CHOOSE Student</option>');
                for(i=0;i<result.payload.length;i++){

                    $("#SemofStudent").append($('<option>',{
                        value : result.payload[i].si_ID,
                        text : result.payload[i].NAME
                    },'</option'));

                }

                
            }else{

            }



          })

    }

    var __dormManagement = function() {
        //console.log("WORKING");

        let payload = {
            "method": "checkifavailable"
        }
        $.main.executeExternalPost(API, JSON.stringify(payload)).done(function(result) {
            console.log(result)

            if (result.status === "NORECORDS") {

                $('#tblDorm1 tbody').append('<tr><td colspan = "5" align ="center" >NO RECORDS FOUND</td></tr>');

            } else {

               var arr = [];

                for (i = 0; i < result.payload.length; i++) {

                    var obj = {};

                    var item = result.payload[i];
                    obj['RoomNumber'] = item.room_name;
                    obj['TotalCapacity'] = item.capacity;
                    obj['Status'] = item.ROOMSTATUS;

                var btnAdd = "<div class='btn-group'> <button style='display: block;' class='btnAdd P4C2_ADDSTUDENT btn btn-xs btn-primary' title='Add Student' data-dormid= "+item.dorm_id+" data-dormname = "+ item.room_name +" >Add Student</button>";

                var btnUpdate = "<button style='display: block;' class='btnUpdate btn btn-xs P4C2_UPDATESTUDENT btn-danger' title='Update Dorm' data-dormid= "+item.dorm_id+" data-dormname = "+ item.room_name +">Update Student</button></div";



                    obj["ACTIONS"] = btnAdd + " " + btnUpdate;
                    //console.log(obj)        
                    arr.push(obj);


                }


        var dataRow = $("#tblDorm1").DataTable(
          {
            'dom' : 'Brftip',
            'data':arr,
            'destroy': true,
            "scrollY": true,
            "bFilter": false, 
            "bLengthChange": false,
            'scrollX' : true,
            'columns': [{"data": "RoomNumber"}, {"data": "TotalCapacity"},{"data": "Status"},{"data": "ACTIONS"} ],
             'buttons': [
                   {  
                    extend: 'excelHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
                  
                    extend:  'pdfHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3 ] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3] }
                  },
                 ]
          });

          $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
          $(".dt-buttons").addClass("hidden")
                                

                // arr.forEach(function(item) {

                //     $('#tblDorm tbody').append('<tr> <td> ' + item.RoomNumber + ' </td> <td> ' + item.TotalCapacity + '</td> <td> ' + item.AvailableSlot + '</td > <td>' + item.Status + '</td > <td>' + item.ACTIONS + '</td> </tr>');

                // });

            }
               $(function() {
                $("#tblDorm1 tbody").on("click", ".btnAdd", function() {
                $("#modalAdd").modal('show')

                 var flag = $(this).closest("td").closest("tr").find("td:nth-of-type(3)").text()
                  dormid = $(this).data("dormid")
                  dormname = $(this).data("dormname")

                 if($(this).closest("td").closest("tr").find("td:nth-of-type(3)").text() <= 0){
                  $(".alert").removeClass("hidden");
                    $(".alert").html('');
                    $(".alert").html('DORM IS FULL!')
                  $("#SemofStudent").prop("disabled",true);
                   $("#saveStudentBtn").addClass("hidden");
                   $(".test").addClass("hidden");
                
                 }else{
                  //$(".test").addClass("hidden");
                  $(".alert").addClass("hidden");
                   $("#SemofStudent").prop("disabled",false);
                   $("#saveStudentBtn").removeClass("hidden");
                     console.log(dormid)

                 }
            })

            $(".btnUpdate").on('click', function() {
                dormid = $(this).data("dormid")
                 dormname = $(this).data("dormname")
                $(".alert").addClass("hidden");
                $(".wrapper").html("")
                    let payload = {
                            "method" : "getStudentDorm",
                            "dormid" : dormid
                    }
            $.main.executeExternalPost(API, JSON.stringify(payload)).done(function(result) {
              console.log(result.status)
            if((result.status === "SUCCESS")&&(result.payload.length != 0)){
            for(i=0;i<result.payload.length;i++){
              var j = result.payload[i];
              console.log(j)
              var NAME = j.NAME;
              var si_ID = j.si_ID;
              console.log(si_ID)
              console.log(NAME)
               $(".wrapper").append('<div class="form-group"><div class="container"><div class="col-lg-13"><div class="iin_ input-group"><input name="" id="dormNumber" placeholder="'+NAME+'"" class="form-control inputstl" type="text" disabled style="font-style: italic; text-align: center"/><label class= "test_"></label><span class="input-group-addon frspan"><input type="checkbox" aria-label="123" value = '+si_ID+'></span></div></div></div></div>');
                }
                    }else{

                $(".alert").removeClass("hidden");
                $(".alert").html('');
                $(".alert").html('NO RECORDS FOUND');

                    }


                 })
                $("#modalUpdate").modal('show')

            })
        })

        });

     
    };

        var __removeStudent = function(){

            $("#saveBtn").on('click', function()
            {

              var chkd = $('input:checkbox:checked');
              var vals = chkd.map(function() {
                  return this.value;}).get().join(',');

              let payload = {
                "method":"removestudentfromdorm",
                 "si_ID" : vals,
                 "dormid" : dormid
              }
                   //  console.log(payload);
            $.main.executeExternalPost(API, JSON.stringify(payload)).done(function(result) { 
               console.log(result.status);
               if(result.status === "SUCCESS"){

                $(".success").removeClass("hidden");
                $(".success").html('');
                $(".success").html('SUCCESS REMOVING Student');
                   var acct_id = $("#getaccountidforDorm").data('acct');
                  var acct_ip = $("#getaccountidforDorm").data('acctip');
                   var payload1 = {
                                    "method" : "PostAudit",
                                    "acct_id": acct_id, 
                                    "client_ip": acct_ip,
                                    "event_action":"UPDATE",
                                    "eventdesc": "MODULE: Dorm Management, <br> DESCRIPTION: User Remove a Student to Room No : " +   
                                    dormname 

                            };
                         //  console.log(payload1)
                            $.main.executeExternalPost(aud_path, JSON.stringify(payload1)).done(function(result){
                                if(result.status === "SUCCESS"){

                                setTimeout(function(){
                                $(".success").html('');
                                $(".success").addClass("hidden");
                                $("#modalUpdate").modal('hide');
                                location.reload();
                                      }, 600);


                                }

                            })

              }

            })


        })

        }


    var __saveStudenttoDorm = function(){

        $("#saveStudentBtn").on('click', function() {
            if($("#SemofStudent").val() === "0"){
                
                $(".test").removeClass("hidden");

            }else if($("#SemofStudent").val() === ""){

                
                  $(".test").removeClass("hidden");

            }else if ($("#SemofStudent").val() === "CHOOSE Student") {
                
                 $(".alert").removeClass("hidden");
                 $(".alert").html('PLEASE CHOOSE A Student')

            }else{
            
                $(".alert").addClass("hidden");

            let payload = {

                    "method" : "UpdateRoomAvailability",
                    "dormid" :dormid,
                    "student_out":0

            }
            console.log($("#SemofStudent").val())

              $.main.executeExternalPost(API, JSON.stringify(payload)).done(function(result) {
                //console.log(result)
                if(result.status === "SUCCESS"){
                        

                 let payload = {

                    "method" : "addstudenttodorm",
                    "dormid" :dormid,
                    "si_ID" : $("#SemofStudent").val(),
                    "created_by":name

                     }

                 $.main.executeExternalPost(API, JSON.stringify(payload)).done(function(result) {
                    console.log(result)
                     if(result.status === "SUCCESS"){

                $(".success").removeClass("hidden");
                $(".success").html('');
                $(".success").html('SUCCESS ADDING Student');
                var acct_id = $("#getaccountidforDorm").data('acct');
                var acct_ip = $("#getaccountidforDorm").data('acctip');
                   var payload1 = {
                                    "method" : "PostAudit",
                                    "acct_id": acct_id, 
                                    "client_ip": acct_ip,
                                    "event_action":"ADDED",
                                    "eventdesc": "MODULE: Dorm Management, <br> DESCRIPTION: User Added New Student to Room No : " +   
                                    dormname 

                            };
                         //  console.log(payload1)
                            $.main.executeExternalPost(aud_path, JSON.stringify(payload1)).done(function(result){
                                  if(result.status == "SUCCESS"){
                                  
                                  setTimeout(function(){
                                  $(".success").html('');
                                  $(".success").addClass("hidden");
                                  $("#modalAdd").modal('hide')
                                  location.reload();
                                  }, 600);

                                  }

                                   });

                     }


                     })


                    }
                })
              }

        })

     }
     var __getStudentDorm = function(){

 





     }


    $(document).ready(function() {
        $("#dormCap").keydown(function(e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }

            var charValue = String.fromCharCode(e.keyCode),
                valid = /^[0-9]+$/.test(charValue);

            if (!valid) {
                e.preventDefault();
            }
        });
    });



    return {
        dormManagement: __dormManagement,
        AddDorm: __AddDorm,
        getStudentsWithoutDorm :__getStudentsWithoutDorm,
        saveStudenttoDorm:__saveStudenttoDorm,
        removeStudent:__removeStudent
    };
}());