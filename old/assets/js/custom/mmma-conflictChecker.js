//////////////////////////////////////////////////////////////////
//////////////////////  Conflict Checker  ////////////////////////
//////////////////////////////////////////////////////////////////
ConflictChecker = {
    Day1Validation: function(day1,room1,time1a,time1b){
        // COMPARE TO DAY 2
        function validation(){
            if($(".validate-day-1 option:selected").val() === $(".validate-day-2 option:selected").val() && $(".validate-room-1 option:selected").val() === $(".validate-room-2 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-1 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-1 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-2 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-2 option:selected").attr("data-mt"));

                if($(".validate-end-2 option:selected").attr("data-mt") === "NONE" || $(".validate-end-2 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(2)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }else{
                        $(".alert-day1 span:nth-of-type(2)").text("");
                    }
                }else if($(".validate-start-2 option:selected").attr("data-mt") === "NONE" || $(".validate-start-2 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(2)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }else{
                        $(".alert-day1 span:nth-of-type(2)").text("");
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(2)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day1 span:nth-of-type(2)").text("");
                    }else{
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(2)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }
                }
            }
        }
        // COMPARE TO DAY 3
        function validation1(){
            if($(".validate-day-1 option:selected").val() === $(".validate-day-3 option:selected").val() && $(".validate-room-1 option:selected").val() === $(".validate-room-3 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-1 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-1 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-3 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-3 option:selected").attr("data-mt"));

                if($(".validate-end-3 option:selected").attr("data-mt") === "NONE" || $(".validate-end-3 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(3)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }else{
                        $(".alert-day1 span:nth-of-type(3)").text("");
                    }
                }else if($(".validate-start-3 option:selected").attr("data-mt") === "NONE" || $(".validate-start-3 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(3)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }else{
                        $(".alert-day1 span:nth-of-type(3)").text("");
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(3)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day1 span:nth-of-type(3)").text("");
                    }else{
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(3)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }
                }
            }
        }
        // COMPARE TO DAY 4
        function validation2(){
            if($(".validate-day-1 option:selected").val() === $(".validate-day-4 option:selected").val() && $(".validate-room-1 option:selected").val() === $(".validate-room-4 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-1 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-1 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-4 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-4 option:selected").attr("data-mt"));

                if($(".validate-end-4 option:selected").attr("data-mt") === "NONE" || $(".validate-end-4 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }else{
                        $(".alert-day1 span:nth-of-type(4)").text("");
                    }
                }else if($(".validate-start-4 option:selected").attr("data-mt") === "NONE" || $(".validate-start-4 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }else{
                        $(".alert-day1 span:nth-of-type(4)").text("");
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day1 span:nth-of-type(4)").text("");
                    }else{
                        $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                        $(".alert-day1 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(0).find("small").text("validate");
                        $(".alert-day1").slideDown();
                    }
                }
            }
        }

        if(day1 === "None" || room1 === "None" || time1a === "NONE" && time1b === "NONE" || time1a > 0 && time1b === "NONE" || time1a === "NONE" && time1b > 0  || time1a === "TBA" && time1b === "NONE" || time1a === "NONE" && time1b === "TBA"){
            $(".alert-day1").slideDown();
            $(".alert-day1 span:nth-of-type(2)").text("Please complete all fields: ( Day | Room | Start | End )");
            $(".error-hint").eq(2).removeClass("has-error");
            $(".error-hint").eq(2).removeClass("has-success");
            $(".error-hint").eq(3).removeClass("has-error");
            $(".error-hint").eq(3).removeClass("has-success");
        }else{
            if($("#select2-start-container").text() === "TBA" || $("#select2-end-container").text() === "TBA"){
                    $(".validate-start-1 option[value='TBA']").attr("selected","selected");
                    $(".validate-end-1 option[value='TBA']").attr("selected","selected");
                    $("#select2-start-container").text("TBA");
                    $("#select2-start-container").attr("title","TBA");
                    $("#select2-end-container").text("TBA");
                    $("#select2-end-container").attr("title","TBA");
                    $(".error-hint").eq(2).removeClass("has-error").addClass("has-success");
                    $(".error-hint").eq(3).removeClass("has-error").addClass("has-success");
                    $(".alert-day1").slideUp();
            }else if(time1a < time1b){
                $(".error-hint").eq(2).removeClass("has-error").addClass("has-success");
                $(".error-hint").eq(3).removeClass("has-error").addClass("has-success");
                $(".alert-day1").slideUp();
                $(".btn-addmoreday1").fadeIn();
                $(".btn-verify").eq(0).attr("data-status","true");
                $(".btn-verify").eq(0).removeClass("btn-validate").addClass("btn-verified");
                $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-verified");
                $(".btn-verify").eq(0).find("small").text("valid");
                validation();
                validation1();
                validation2();
            }else if(time1a === time1b){
                $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                $(".alert-day1 span:nth-of-type(2)").text("Start time is same with End time.");
                $(".alert-day1 span:nth-of-type(3)").text("");
                $(".alert-day1 span:nth-of-type(4)").text("");
                $(".alert-day1").slideDown();
                $(".btn-addmoreday1").fadeOut();
            }else{
                $(".error-hint").eq(2).removeClass("has-success").addClass("has-error");
                $(".error-hint").eq(3).removeClass("has-success").addClass("has-error");
                $(".alert-day1 span:nth-of-type(2)").text("Start time is earlier than End time.");
                $(".alert-day1 span:nth-of-type(3)").text("");
                $(".alert-day1 span:nth-of-type(4)").text("");
                $(".alert-day1").slideDown();
                $(".btn-addmoreday1").fadeOut();
            }
        }
    },
    Day2Validation: function(day2,room2,time1a,time1b){
        // COMPARE TO DAY 1
        function validation(){
            if($(".validate-day-2 option:selected").val() === $(".validate-day-1 option:selected").val() && $(".validate-room-2 option:selected").val() === $(".validate-room-1 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-2 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-2 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-1 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-1 option:selected").attr("data-mt"));

                if($(".validate-end-1 option:selected").attr("data-mt") === "NONE" || $(".validate-end-1 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }else{
                        $(".alert-day2 span:nth-of-type(2)").text("");
                    }
                }else if($(".validate-start-1 option:selected").attr("data-mt") === "NONE" || $(".validate-start-1 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }else{
                        $(".alert-day2 span:nth-of-type(2)").text("");
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day2 span:nth-of-type(2)").text("");
                    }else{
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }
                }
            }
        }
        // COMPARE TO DAY 3
        function validation1(){
            if($(".validate-day-2 option:selected").val() === $(".validate-day-3 option:selected").val() && $(".validate-room-2 option:selected").val() === $(".validate-room-3 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-2 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-2 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-3 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-3 option:selected").attr("data-mt"));

                if($(".validate-end-3 option:selected").attr("data-mt") === "NONE" || $(".validate-end-3 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(3)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }else{
                        $(".alert-day2 span:nth-of-type(2)").text("");
                    }
                }else if($(".validate-start-3 option:selected").attr("data-mt") === "NONE" || $(".validate-start-3 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(3)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }else{
                        $(".alert-day2 span:nth-of-type(3)").text("");
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(3)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day2 span:nth-of-type(3)").text("");
                    }else{
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(3)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }
                }
            }
        }
        // COMPARE TO DAY 4
        function validation2(){
            if($(".validate-day-2 option:selected").val() === $(".validate-day-4 option:selected").val() && $(".validate-room-2 option:selected").val() === $(".validate-room-4 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-2 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-2 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-4 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-4 option:selected").attr("data-mt"));

                if($(".validate-end-4 option:selected").attr("data-mt") === "NONE" || $(".validate-end-4 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }else{
                        $(".alert-day2 span:nth-of-type(4)").text("");
                    }
                }else if($(".validate-start-4 option:selected").attr("data-mt") === "NONE" || $(".validate-start-4 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }else{
                        $(".alert-day2 span:nth-of-type(4)").text("");
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day2 span:nth-of-type(4)").text("");
                    }else{
                        $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                        $(".alert-day2 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(1).find("small").text("validate");
                        $(".alert-day2").slideDown();
                    }
                }
            }
        }

        if(day2 === "None" || room2 === "None" || time1a === "NONE" && time1b === "NONE" || time1a > 0 && time1b === "NONE" || time1a === "NONE" && time1b > 0  || time1a === "TBA" && time1b === "NONE" || time1a === "NONE" && time1b === "TBA"){
            $(".alert-day2").slideDown();
            $(".alert-day2 span:nth-of-type(2)").text("Please complete all fields: ( Day | Room | Start | End )");
            $(".error-hint").eq(6).removeClass("has-error");
            $(".error-hint").eq(6).removeClass("has-success");
            $(".error-hint").eq(7).removeClass("has-error");
            $(".error-hint").eq(7).removeClass("has-success");
        }else{
            if($("#select2-start2-container").text() === "TBA" || $("#select2-end2-container").text() === "TBA"){
                $(".validate-start-2 option[value='TBA']").attr("selected","selected");
                $(".validate-end-2 option[value='TBA']").attr("selected","selected");
                $("#select2-start2-container").text("TBA");
                $("#select2-start2-container").attr("title","TBA");
                $("#select2-end2-container").text("TBA");
                $("#select2-end2-container").attr("title","TBA");
                $(".error-hint").eq(6).removeClass("has-error").addClass("has-success");
                $(".error-hint").eq(7).removeClass("has-error").addClass("has-success");
                $(".alert-day2").slideUp();
            }else if(time1a < time1b){
                $(".error-hint").eq(6).removeClass("has-error").addClass("has-success");
                $(".error-hint").eq(7).removeClass("has-error").addClass("has-success");
                $(".alert-day2").slideUp();
                $(".btn-addmoreday2").fadeIn();
                $(".btn-verify").eq(1).attr("data-status","true");
                $(".btn-verify").eq(1).removeClass("btn-validate").addClass("btn-verified");
                $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-verified");
                $(".btn-verify").eq(1).find("small").text("valid");
                validation();
                validation1();
                validation2();
            }else if(time1a === time1b){
                $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                $(".alert-day2 span:nth-of-type(2)").text("Start time is same with End time.");
                $(".alert-day2 span:nth-of-type(3)").text("");
                $(".alert-day2 span:nth-of-type(4)").text("");
                $(".alert-day2").slideDown();
                $(".btn-addmoreday2").fadeOut();
            }else{
                $(".error-hint").eq(6).removeClass("has-success").addClass("has-error");
                $(".error-hint").eq(7).removeClass("has-success").addClass("has-error");
                $(".alert-day2 span:nth-of-type(2)").text("Start time is earlier than End time.");
                $(".alert-day2 span:nth-of-type(3)").text("");
                $(".alert-day2 span:nth-of-type(4)").text("");
                $(".alert-day2").slideDown();
                $(".btn-addmoreday2").fadeOut();
            }
        }
    },
    Day3Validation: function(day3,room3,time1a,time1b){
        // COMPARE TO DAY 1
        function validation(){
            if($(".validate-day-3 option:selected").val() === $(".validate-day-1 option:selected").val() && $(".validate-room-3 option:selected").val() === $(".validate-room-1 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-3 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-3 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-1 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-1 option:selected").attr("data-mt"));

                if($(".validate-end-1 option:selected").attr("data-mt") === "NONE" || $(".validate-end-1 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }else{
                        $(".alert-day3 span:nth-of-type(2)").text("");
                        return true;
                    }
                }else if($(".validate-start-1 option:selected").attr("data-mt") === "NONE" || $(".validate-start-1 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }else{
                        $(".alert-day3 span:nth-of-type(2)").text("");
                        return true;
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day3 span:nth-of-type(2)").text("");
                        return true;
                    }else{
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }
                }
            }
        }
        // COMPARE TO DAY 2
        function validation1(){
            if($(".validate-day-3 option:selected").val() === $(".validate-day-2 option:selected").val() && $(".validate-room-3 option:selected").val() === $(".validate-room-2 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-3 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-3 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-2 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-2 option:selected").attr("data-mt"));

                if($(".validate-end-2 option:selected").attr("data-mt") === "NONE" || $(".validate-end-2 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(3)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }else{
                        $(".alert-day3 span:nth-of-type(3)").text("");
                        return true;
                    }
                }else if($(".validate-start-2 option:selected").attr("data-mt") === "NONE" || $(".validate-start-2 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(3)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }else{
                        $(".alert-day3 span:nth-of-type(3)").text("");
                        return true;
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(3)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day3 span:nth-of-type(3)").text("");
                        return true;
                    }else{
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(3)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }
                }
            }
        }
        // COMPARE TO DAY 4
        function validation2(){
            if($(".validate-day-3 option:selected").val() === $(".validate-day-4 option:selected").val() && $(".validate-room-3 option:selected").val() === $(".validate-room-4 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-3 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-3 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-4 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-4 option:selected").attr("data-mt"));

                if($(".validate-end-4 option:selected").attr("data-mt") === "NONE" || $(".validate-end-4 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }else{
                        $(".alert-day3 span:nth-of-type(4)").text("");
                    }
                }else if($(".validate-start-4 option:selected").attr("data-mt") === "NONE" || $(".validate-start-4 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }else{
                        $(".alert-day3 span:nth-of-type(4)").text("");
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day3 span:nth-of-type(4)").text("");
                    }else{
                        $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                        $(".alert-day3 span:nth-of-type(4)").text("You have conflict time on Day 4. ");
                        $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(2).find("small").text("validate");
                        $(".alert-day3").slideDown();
                    }
                }
            }
        }

        if(day3 === "None" || room3 === "None" || time1a === "NONE" && time1b === "NONE" || time1a > 0 && time1b === "NONE" || time1a === "NONE" && time1b > 0  || time1a === "TBA" && time1b === "NONE" || time1a === "NONE" && time1b === "TBA"){
            $(".alert-day3").slideDown();
            $(".alert-day3 span:nth-of-type(2)").text("Please complete all fields: ( Day | Room | Start | End )");
            $(".error-hint").eq(10).removeClass("has-error");
            $(".error-hint").eq(10).removeClass("has-success");
            $(".error-hint").eq(11).removeClass("has-error");
            $(".error-hint").eq(11).removeClass("has-success");
        }else{
            if($("#select2-start3-container").text() === "TBA" || $("#select2-end3-container").text() === "TBA"){
                $(".validate-start-3 option[value='TBA']").attr("selected","selected");
                $(".validate-end-3 option[value='TBA']").attr("selected","selected");
                $("#select2-start3-container").text("TBA");
                $("#select2-start3-container").attr("title","TBA");
                $("#select2-end3-container").text("TBA");
                $("#select2-end3-container").attr("title","TBA");
                $(".error-hint").eq(10).removeClass("has-error").addClass("has-success");
                $(".error-hint").eq(11).removeClass("has-error").addClass("has-success");
                $(".alert-day3").slideUp();
            }else if(time1a < time1b){
                $(".error-hint").eq(10).removeClass("has-error").addClass("has-success");
                $(".error-hint").eq(11).removeClass("has-error").addClass("has-success");
                $(".alert-day3").slideUp();
                $(".btn-addmoreday3").fadeIn();
                $(".btn-verify").eq(2).attr("data-status","true");
                $(".btn-verify").eq(2).removeClass("btn-validate").addClass("btn-verified");
                $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-verified");
                $(".btn-verify").eq(2).find("small").text("valid");
                validation();
                validation1();
                validation2();

            }else if(time1a === time1b){
                $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                $(".alert-day3 span:nth-of-type(2)").text("Start time is same with End time.");
                $(".alert-day3 span:nth-of-type(3)").text("");
                $(".alert-day3").slideDown();
                $(".btn-addmoreday3").fadeOut();
            }else{
                $(".error-hint").eq(10).removeClass("has-success").addClass("has-error");
                $(".error-hint").eq(11).removeClass("has-success").addClass("has-error");
                $(".alert-day3 span:nth-of-type(2)").text("Start time is earlier than End time.");
                $(".alert-day3 span:nth-of-type(3)").text("");
                $(".alert-day3").slideDown();
                $(".btn-addmoreday3").fadeOut();
            }
        }
    },
    Day4Validation: function(day4,room4,time1a,time1b){
        // COMPARE TO DAY 1
        function validation(){
            if($(".validate-day-4 option:selected").val() === $(".validate-day-1 option:selected").val() && $(".validate-room-4 option:selected").val() === $(".validate-room-1 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-4 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-4 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-1 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-1 option:selected").attr("data-mt"));

                if($(".validate-end-1 option:selected").attr("data-mt") === "NONE" || $(".validate-end-1 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }else{
                        $(".alert-day4 span:nth-of-type(2)").text("");
                        return true;
                    }
                }else if($(".validate-start-1 option:selected").attr("data-mt") === "NONE" || $(".validate-start-1 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }else{
                        $(".alert-day4 span:nth-of-type(2)").text("");
                        return true;
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day4 span:nth-of-type(2)").text("");
                        return true;
                    }else{
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(2)").text("You have conflict time on Day 1. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }
                }
            }
        }
        // COMPARE TO DAY 2
        function validation1(){
            if($(".validate-day-4 option:selected").val() === $(".validate-day-2 option:selected").val() && $(".validate-room-4 option:selected").val() === $(".validate-room-2 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-4 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-4 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-2 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-2 option:selected").attr("data-mt"));

                if($(".validate-end-2 option:selected").attr("data-mt") === "NONE" || $(".validate-end-2 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(3)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }else{
                        $(".alert-day4 span:nth-of-type(3)").text("");
                        return true;
                    }
                }else if($(".validate-start-2 option:selected").attr("data-mt") === "NONE" || $(".validate-start-2 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(3)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }else{
                        $(".alert-day4 span:nth-of-type(3)").text("");
                        return true;
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(3)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day4 span:nth-of-type(3)").text("");
                        return true;
                    }else{
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(3)").text("You have conflict time on Day 2. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }
                }
            }
        }
        // COMPARE TO DAY 3
        function validation2(){
            if($(".validate-day-4 option:selected").val() === $(".validate-day-3 option:selected").val() && $(".validate-room-4 option:selected").val() === $(".validate-room-3 option:selected").val()){
                var vtime1a = parseInt($(".validate-start-4 option:selected").attr("data-mt"));
                var vtime1b = parseInt($(".validate-end-4 option:selected").attr("data-mt"));
                var vtime2a = parseInt($(".validate-start-3 option:selected").attr("data-mt"));
                var vtime2b = parseInt($(".validate-end-3 option:selected").attr("data-mt"));

                if($(".validate-end-3 option:selected").attr("data-mt") === "NONE" || $(".validate-end-3 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a <= vtime2a && vtime1b > vtime2a){
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(4)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }else{
                        $(".alert-day4 span:nth-of-type(4)").text("");
                        return true;
                    }
                }else if($(".validate-start-3 option:selected").attr("data-mt") === "NONE" || $(".validate-start-3 option:selected").attr("data-mt") === "TBA"){
                    if(vtime1a < vtime2b && vtime1b >= vtime2b){
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(4)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }else{
                        $(".alert-day4 span:nth-of-type(4)").text("");
                        return true;
                    }
                }else{
                    if(vtime1a > vtime2a && vtime1a < vtime2b || vtime1b > vtime2a && vtime1b < vtime2b){
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(4)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }else if(vtime1a < vtime2a && vtime1a < vtime2b && vtime1b <= vtime2a && vtime1b < vtime2b || vtime1a > vtime2a && vtime1a >= vtime2b && vtime1b > vtime2a && vtime1b > vtime2b){
                        $(".alert-day4 span:nth-of-type(4)").text("");
                        return true;
                    }else{
                        $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                        $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                        $(".alert-day4 span:nth-of-type(4)").text("You have conflict time on Day 3. ");
                        $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
                        $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
                        $(".btn-verify").eq(3).find("small").text("validate");
                        $(".alert-day4").slideDown();
                    }
                }
            }
        }

        if(day4 === "None" || room4 === "None" || time1a === "NONE" && time1b === "NONE" || time1a > 0 && time1b === "NONE" || time1a === "NONE" && time1b > 0  || time1a === "TBA" && time1b === "NONE" || time1a === "NONE" && time1b === "TBA"){
            $(".alert-day4").slideDown();
            $(".alert-day4 span:nth-of-type(2)").text("Please complete all fields: ( Day | Room | Start | End )");
            $(".error-hint").eq(14).removeClass("has-error");
            $(".error-hint").eq(14).removeClass("has-success");
            $(".error-hint").eq(15).removeClass("has-error");
            $(".error-hint").eq(15).removeClass("has-success");
        }else{
            if($("#select2-start4-container").text() === "TBA" || $("#select2-end4-container").text() === "TBA"){
                $(".validate-start-4 option[value='TBA']").attr("selected","selected");
                $(".validate-end-4 option[value='TBA']").attr("selected","selected");
                $("#select2-start4-container").text("TBA");
                $("#select2-start4-container").attr("title","TBA");
                $("#select2-end4-container").text("TBA");
                $("#select2-end4-container").attr("title","TBA");
                $(".error-hint").eq(14).removeClass("has-error").addClass("has-success");
                $(".error-hint").eq(15).removeClass("has-error").addClass("has-success");
                $(".alert-day4").slideUp();
            }else if(time1a < time1b){
                $(".error-hint").eq(14).removeClass("has-error").addClass("has-success");
                $(".error-hint").eq(15).removeClass("has-error").addClass("has-success");
                $(".alert-day4").slideUp();
                $(".btn-verify").eq(3).attr("data-status","true");
                $(".btn-verify").eq(3).removeClass("btn-validate").addClass("btn-verified");
                $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-verified");
                $(".btn-verify").eq(3).find("small").text("valid");
                validation();
                validation1();
                validation2();
                // if(validation() === true && validation1() === true && validation2() === true){
                //     $(".btnAdd-subjectsection").removeClass("hidden")
                // }else{
                //     $(".btnAdd-subjectsection").addClass("hidden")
                // }
            }else if(time1a === time1b){
                $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                $(".alert-day4 span:nth-of-type(2)").text("Start time is same with End time.");
                $(".alert-day4 span:nth-of-type(3)").text("");
                $(".alert-day4").slideDown();
            }else{
                $(".error-hint").eq(14).removeClass("has-success").addClass("has-error");
                $(".error-hint").eq(15).removeClass("has-success").addClass("has-error");
                $(".alert-day4 span:nth-of-type(2)").text("Start time is earlier than End time.");
                $(".alert-day4 span:nth-of-type(3)").text("");
                $(".alert-day4").slideDown();
            }
        }
    },
    Notification: function(){
        $(".alert-day1").hide().slideUp(); 
        $(".alert-day2").hide().slideUp(); 
        $(".alert-day3").hide().slideUp(); 
        $(".alert-day4").hide().slideUp();

        $(".section-validation.row").eq(1).slideUp();
        $(".section-validation.row").eq(2).slideUp();
        $(".section-validation.row").eq(3).slideUp();

        $(".alert-close1").unbind().bind("click", function(){ $(".alert-day1").slideUp(); });
        $(".alert-close2").unbind().bind("click", function(){ $(".alert-day2").slideUp(); });
        $(".alert-close3").unbind().bind("click", function(){ $(".alert-day3").slideUp(); });
        $(".alert-close4").unbind().bind("click", function(){ $(".alert-day4").slideUp(); });
        $(".alert-closesecsub").unbind().bind("click", function(){ $(".alert-secsub").slideUp(); });

    },
    Controllers: function(){
        ConflictChecker.Notification();

        $(".validate-day-1").on("change", function(){
            $(".btn-addmoreday1").fadeOut();
            $(".btn-verify").eq(0).attr("data-status","false");
            $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(0).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
            
        }),$(".validate-room-1").on("change", function(){
            $(".btn-addmoreday1").fadeOut();
            $(".btn-verify").eq(0).attr("data-status","false");
            $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(0).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }),$(".validate-start-1").on("change", function(){
            $(".btn-addmoreday1").fadeOut();
            $(".btn-verify").eq(0).attr("data-status","false");
            $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(0).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }),$(".validate-end-1").on("change", function(){
            $(".btn-addmoreday1").fadeOut();
            $(".btn-verify").eq(0).attr("data-status","false");
            $(".btn-verify").eq(0).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(0).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(0).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        });

        $(".validate-day-2").on("change", function(){
            $(".btn-addmoreday2").fadeOut();
            $(".btn-verify").eq(1).attr("data-status","false");
            $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(1).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }),$(".validate-room-2").on("change", function(){
            $(".btn-addmoreday2").fadeOut();
            $(".btn-verify").eq(1).attr("data-status","false");
            $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(1).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }),$(".validate-start-2").on("change", function(){
            $(".btn-addmoreday2").fadeOut();
            $(".btn-verify").eq(1).attr("data-status","false");
            $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(1).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }),$(".validate-end-2").on("change", function(){
            $(".btn-addmoreday2").fadeOut();
            $(".btn-verify").eq(1).attr("data-status","false");
            $(".btn-verify").eq(1).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(1).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(1).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        });

        $(".validate-day-3").on("change", function(){
            $(".btn-addmoreday3").fadeOut();
            $(".btn-verify").eq(2).attr("data-status","false");
            $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(2).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }),$(".validate-room-3").on("change", function(){
            $(".btn-addmoreday3").fadeOut();
            $(".btn-verify").eq(2).attr("data-status","false");
            $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(2).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }),$(".validate-start-3").on("change", function(){
            $(".btn-addmoreday3").fadeOut();
            $(".btn-verify").eq(2).attr("data-status","false");
            $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(2).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }),$(".validate-end-3").on("change", function(){
            $(".btn-addmoreday3").fadeOut();
            $(".btn-verify").eq(2).attr("data-status","false");
            $(".btn-verify").eq(2).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(2).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(2).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        });

        $(".validate-day-4").on("change", function(){
            $(".btn-addmoreday4").fadeOut();
            $(".btn-verify").eq(3).attr("data-status","false");
            $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(3).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }),$(".validate-room-4").on("change", function(){
            $(".btn-addmoreday4").fadeOut();
            $(".btn-verify").eq(3).attr("data-status","false");
            $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(3).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }),$(".validate-start-4").on("change", function(){
            $(".btn-addmoreday4").fadeOut();
            $(".btn-verify").eq(3).attr("data-status","false");
            $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(3).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }),$(".validate-end-4").on("change", function(){
            $(".btn-addmoreday4").fadeOut();
            $(".btn-verify").eq(3).attr("data-status","false");
            $(".btn-verify").eq(3).removeClass("btn-verified").addClass("btn-validate");
            $(".btn-verify").eq(3).removeClass("btn-unverified").addClass("btn-validate");
            $(".btn-verify").eq(3).find("small").text("validate");
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        });

        $(".section-validation.row").eq(1).attr("data-visible","false");
        $(".section-validation.row").eq(2).attr("data-visible","false");
        $(".section-validation.row").eq(3).attr("data-visible","false");

        $(".btn-addmoreday1").unbind().bind("click", function(){
            $(".btn-addmoreday1").remove();
            $(".section-validation.row").eq(1).slideDown();
            $(".section-validation.row").eq(1).attr("data-visible","true")
        }),$(".btn-addmoreday2").unbind().bind("click", function(){
            $(".btn-addmoreday2").remove();
            $(".section-validation.row").eq(2).slideDown();
            $(".section-validation.row").eq(2).attr("data-visible","true")
        }),$(".btn-addmoreday3").unbind().bind("click", function(){
            $(".btn-addmoreday3").remove();
            $(".section-validation.row").eq(3).slideDown();
            $(".section-validation.row").eq(3).attr("data-visible","true")
        });

        $(".btn-verify").eq(0).unbind().bind("click", function(){
            var day1 = $(".validate-day-1 option:selected").text()
            var room1 = $(".validate-room-1 option:selected").text()
            
            if($(".validate-start-1 option:selected").attr("data-mt") === "NONE"){
                var time1a = "NONE"
            }else if($(".validate-start-1 option:selected").attr("data-mt") === "TBA"){
                var time1a = "TBA"
            }else{
                var time1a = parseInt($(".validate-start-1 option:selected").attr("data-mt"))
            }

            if($(".validate-end-1 option:selected").attr("data-mt") === "NONE"){
                var time1b = "NONE"
            }else if($(".validate-end-1 option:selected").attr("data-mt") === "TBA"){
                var time1b = "TBA"
            }else{
                var time1b = parseInt($(".validate-end-1 option:selected").attr("data-mt"))
            }
            
            ConflictChecker.Day1Validation(day1,room1,time1a,time1b)
        }),$(".btn-verify").eq(1).unbind().bind("click", function(){
            var day2 = $(".validate-day-2 option:selected").text()
            var room2 = $(".validate-room-2 option:selected").text()
            
            if($(".validate-start-2 option:selected").attr("data-mt") === "NONE"){
                var time1a = "NONE"
            }else if($(".validate-start-2 option:selected").attr("data-mt") === "TBA"){
                var time1a = "TBA"
            }else{
                var time1a = parseInt($(".validate-start-2 option:selected").attr("data-mt"))
            }

            if($(".validate-end-2 option:selected").attr("data-mt") === "NONE"){
                var time1b = "NONE"
            }else if($(".validate-end-2 option:selected").attr("data-mt") === "TBA"){
                var time1b = "TBA"
            }else{
                var time1b = parseInt($(".validate-end-2 option:selected").attr("data-mt"))
            }
            
            ConflictChecker.Day2Validation(day2,room2,time1a,time1b)
        }),$(".btn-verify").eq(2).unbind().bind("click", function(){
            var day2 = $(".validate-day-3 option:selected").text()
            var room2 = $(".validate-room-3 option:selected").text()
            
            if($(".validate-start-3 option:selected").attr("data-mt") === "NONE"){
                var time1a = "NONE"
            }else if($(".validate-start-3 option:selected").attr("data-mt") === "TBA"){
                var time1a = "TBA"
            }else{
                var time1a = parseInt($(".validate-start-3 option:selected").attr("data-mt"))
            }

            if($(".validate-end-3 option:selected").attr("data-mt") === "NONE"){
                var time1b = "NONE"
            }else if($(".validate-end-3 option:selected").attr("data-mt") === "TBA"){
                var time1b = "TBA"
            }else{
                var time1b = parseInt($(".validate-end-3 option:selected").attr("data-mt"))
            }
            
            ConflictChecker.Day3Validation(day2,room2,time1a,time1b)
        }),$(".btn-verify").eq(3).unbind().bind("click", function(){
            var day4 = $(".validate-day-4 option:selected").text()
            var room4 = $(".validate-room-4 option:selected").text()
            
            if($(".validate-start-4 option:selected").attr("data-mt") === "NONE"){
                var time1a = "NONE"
            }else if($(".validate-start-4 option:selected").attr("data-mt") === "TBA"){
                var time1a = "TBA"
            }else{
                var time1a = parseInt($(".validate-start-4 option:selected").attr("data-mt"))
            }

            if($(".validate-end-4 option:selected").attr("data-mt") === "NONE"){
                var time1b = "NONE"
            }else if($(".validate-end-4 option:selected").attr("data-mt") === "TBA"){
                var time1b = "TBA"
            }else{
                var time1b = parseInt($(".validate-end-4 option:selected").attr("data-mt"))
            }
            
            ConflictChecker.Day4Validation(day4,room4,time1a,time1b)
        });

        $(".btn-addmoreday1").hide().fadeOut();
        $(".btn-addmoreday2").hide().fadeOut();
        $(".btn-addmoreday3").hide().fadeOut();

        $(".alert-secsub").hide().slideUp();

        $(".btn-verify").eq(0).attr("data-status","false");
        $(".btn-verify").eq(1).attr("data-status","false");
        $(".btn-verify").eq(2).attr("data-status","false");
        $(".btn-verify").eq(3).attr("data-status","false");

        $(".btnAdd-subjectsection").slideUp();

        

    }
}
ConflictChecker.Controllers();
$(".conflict-notification").slideUp();
$(".btn-conflictChecker").on("click", function(){
    var verify_btn1 = $(".btn-verify").eq(0).attr("data-status");
    var verify_btn2 = $(".btn-verify").eq(1).attr("data-status");
    var verify_btn3 = $(".btn-verify").eq(2).attr("data-status");
    var verify_btn4 = $(".btn-verify").eq(3).attr("data-status");

    if(verify_btn1 === "false" && verify_btn2 === "false" && verify_btn3 === "false"  && verify_btn4 === "false"){
        $(".alert-secsub").slideDown();
        $(".error-message").text("Pleast encode at least 1 valid time schedule.");
    }else if($("#semester_section_ID option:selected").val() === "NONE" || $("#subject_ID option:selected").val() === "NONE"){
        $(".alert-secsub").slideDown();
        $(".error-message").text("Do not leave section or subject field empty.");
    }else{
        $(".alert-secsub").slideUp();
        $(".conflict-notification").slideDown();
    $(".list-of-conflict").html("");
    var encoded_day1 = $(".validate-day-1 option:selected").val();
    var encoded_room1 = $(".validate-room-1 option:selected").val();
    var encoded_start1 = $(".validate-start-1 option:selected").attr("data-mt");
    var encoded_end1 = $(".validate-end-1 option:selected").attr("data-mt");

    var encoded_day2 = $(".validate-day-2 option:selected").val();
    var encoded_room2 = $(".validate-room-2 option:selected").val();
    var encoded_start2 = $(".validate-start-2 option:selected").attr("data-mt");
    var encoded_end2 = $(".validate-end-2 option:selected").attr("data-mt");

    var encoded_day3 = $(".validate-day-3 option:selected").val();
    var encoded_room3 = $(".validate-room-3 option:selected").val();
    var encoded_start3 = $(".validate-start-3 option:selected").attr("data-mt");
    var encoded_end3 = $(".validate-end-3 option:selected").attr("data-mt");

    var encoded_day4 = $(".validate-day-4 option:selected").val();
    var encoded_room4 = $(".validate-room-4 option:selected").val();
    var encoded_start4 = $(".validate-start-4 option:selected").attr("data-mt");
    var encoded_end4 = $(".validate-end-4 option:selected").attr("data-mt");
    $(".noc").text("0");
        for(i=0;i<$(".no-reference").length;i++){
            var compare_day1 = $(".no-reference li:nth-of-type(4)").eq(i).text()
            var compare_room1 = $(".no-reference li:nth-of-type(5)").eq(i).text()
            var compare_roomname1 = $(".no-reference li:nth-of-type(5)").eq(i).attr("data-roomname1")
            var compare_start1 = $(".no-reference li:nth-of-type(6)").eq(i).text()
            var compare_12hourstart1 = $(".no-reference li:nth-of-type(6)").eq(i).attr("data-starttime")
            var compare_end1 = $(".no-reference li:nth-of-type(7)").eq(i).text()
            var compare_12hourend1 = $(".no-reference li:nth-of-type(7)").eq(i).attr("data-endtime")

            var compare_day2 = $(".no-reference li:nth-of-type(8)").eq(i).text()
            var compare_room2 = $(".no-reference li:nth-of-type(9)").eq(i).text()
            var compare_roomname2 = $(".no-reference li:nth-of-type(9)").eq(i).attr("data-roomname2")
            var compare_start2 = $(".no-reference li:nth-of-type(10)").eq(i).text()
            var compare_12hourstart2 = $(".no-reference li:nth-of-type(10)").eq(i).attr("data-starttime2")
            var compare_end2 = $(".no-reference li:nth-of-type(11)").eq(i).text()
            var compare_12hourend2 = $(".no-reference li:nth-of-type(11)").eq(i).attr("data-endtime2")

            var compare_day3 = $(".no-reference li:nth-of-type(12)").eq(i).text()
            var compare_room3 = $(".no-reference li:nth-of-type(13)").eq(i).text()
            var compare_roomname3 = $(".no-reference li:nth-of-type(13)").eq(i).attr("data-roomname3")
            var compare_start3 = $(".no-reference li:nth-of-type(14)").eq(i).text()
            var compare_12hourstart3 = $(".no-reference li:nth-of-type(14)").eq(i).attr("data-starttime3")
            var compare_end3 = $(".no-reference li:nth-of-type(15)").eq(i).text()
            var compare_12hourend3 = $(".no-reference li:nth-of-type(15)").eq(i).attr("data-endtime3")

            var compare_day4 = $(".no-reference li:nth-of-type(16)").eq(i).text()
            var compare_room4 = $(".no-reference li:nth-of-type(17)").eq(i).text()
            var compare_roomname4 = $(".no-reference li:nth-of-type(17)").eq(i).attr("data-roomname4")
            var compare_start4 = $(".no-reference li:nth-of-type(18)").eq(i).text()
            var compare_12hourstart4 = $(".no-reference li:nth-of-type(18)").eq(i).attr("data-starttime4")
            var compare_end4 = $(".no-reference li:nth-of-type(19)").eq(i).text()
            var compare_12hourend4 = $(".no-reference li:nth-of-type(19)").eq(i).attr("data-endtime4")


            function ed1cd1(){
                if(encoded_day1 === compare_day1 && encoded_room1 === compare_room1){
                    if(compare_end1 === "NONE" || compare_end1 === "TBA"){
                        if (parseInt(encoded_start1) <= parseInt(compare_start1) && parseInt(encoded_end1) > parseInt(compare_start1)) {
                            console.log("conflict 1")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start1 === "NONE" || compare_start1 === "TBA"){
                        if(parseInt(encoded_start1) < parseInt(compare_end1) && parseInt(encoded_end1) >= parseInt(compare_end1)){
                            console.log("conflict 2")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start1) > parseInt(compare_start1) && parseInt(encoded_start1) < parseInt(compare_end1) || parseInt(encoded_end1) > parseInt(compare_start1) && parseInt(encoded_end1) < parseInt(compare_end1)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 3")
                            
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day1+' - '+compare_roomname1+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart1+' - '+compare_12hourend1+'</p>\
                                                            </a>')
                                                            return false;
        
                        }else if(parseInt(encoded_start1) < parseInt(compare_start1) && parseInt(encoded_start1) < parseInt(compare_end1) && parseInt(encoded_end1) <= parseInt(compare_start1) && parseInt(encoded_end1) < parseInt(compare_end1) || parseInt(encoded_start1) > parseInt(compare_start1) && parseInt(encoded_start1) >= parseInt(compare_end1) && parseInt(encoded_end1) > parseInt(compare_start1) && parseInt(encoded_end1) > parseInt(compare_end1)){
                            console.log("valid")
                        }else{
                            console.log("conflict 4")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day1+' - '+compare_roomname1+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart1+' - '+compare_12hourend1+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed1cd2(){
                if(encoded_day1 === compare_day2 && encoded_room1 === compare_room2){
                    if(compare_end1 === "NONE" || compare_end1 === "TBA"){
                        if (parseInt(encoded_start1) <= parseInt(compare_start2) && parseInt(encoded_end1) > parseInt(compare_start2)) {
                            console.log("conflict 1")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start2 === "NONE" || compare_start2 === "TBA"){
                        if(parseInt(encoded_start1) < parseInt(compare_end2) && parseInt(encoded_end1) >= parseInt(compare_end2)){
                            console.log("conflict 2")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start1) > parseInt(compare_start2) && parseInt(encoded_start1) < parseInt(compare_end2) || parseInt(encoded_end1) > parseInt(compare_start2) && parseInt(encoded_end1) < parseInt(compare_end2)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 3")
                            
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day2+' - '+compare_roomname2+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart2+' - '+compare_12hourend2+'</p>\
                                                            </a>')
                                                            return false;
        
                        }else if(parseInt(encoded_start1) < parseInt(compare_start2) && parseInt(encoded_start1) < parseInt(compare_end2) && parseInt(encoded_end1) <= parseInt(compare_start2) && parseInt(encoded_end1) < parseInt(compare_end2) || parseInt(encoded_start1) > parseInt(compare_start2) && parseInt(encoded_start1) >= parseInt(compare_end2) && parseInt(encoded_end1) > parseInt(compare_start2) && parseInt(encoded_end1) > parseInt(compare_end2)){
                            console.log("valid")
                        }else{
                            console.log("conflict 4")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day2+' - '+compare_roomname2+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart2+' - '+compare_12hourend2+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed1cd3(){
                if(encoded_day1 === compare_day3 && encoded_room1 === compare_room3){
                    if(compare_end3 === "NONE" || compare_end3 === "TBA"){
                        if (parseInt(encoded_start1) <= parseInt(compare_start3) && parseInt(encoded_end1) > parseInt(compare_start3)) {
                            console.log("conflict 1")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start3 === "NONE" || compare_start3 === "TBA"){
                        if(parseInt(encoded_start1) < parseInt(compare_end3) && parseInt(encoded_end1) >= parseInt(compare_end3)){
                            console.log("conflict 2")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start1) > parseInt(compare_start3) && parseInt(encoded_start1) < parseInt(compare_end3) || parseInt(encoded_end1) > parseInt(compare_start3) && parseInt(encoded_end1) < parseInt(compare_end3)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 3")
                            
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day3+' - '+compare_roomname3+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart3+' - '+compare_12hourend3+'</p>\
                                                            </a>')
                                                            return false;
        
                        }else if(parseInt(encoded_start1) < parseInt(compare_start3) && parseInt(encoded_start1) < parseInt(compare_end3) && parseInt(encoded_end1) <= parseInt(compare_start3) && parseInt(encoded_end1) < parseInt(compare_end3) || parseInt(encoded_start1) > parseInt(compare_start3) && parseInt(encoded_start1) >= parseInt(compare_end3) && parseInt(encoded_end1) > parseInt(compare_start3) && parseInt(encoded_end1) > parseInt(compare_end3)){
                            console.log("valid")
                        }else{
                            console.log("conflict 4")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day3+' - '+compare_roomname3+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart3+' - '+compare_12hourend3+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed1cd4(){
                if(encoded_day1 === compare_day4 && encoded_room1 === compare_room4){
                    if(compare_end4 === "NONE" || compare_end4 === "TBA"){
                        if (parseInt(encoded_start1) <= parseInt(compare_start4) && parseInt(encoded_end1) > parseInt(compare_start4)) {
                            console.log("conflict 1")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start4 === "NONE" || compare_start4 === "TBA"){
                        if(parseInt(encoded_start1) < parseInt(compare_end4) && parseInt(encoded_end1) >= parseInt(compare_end4)){
                            console.log("conflict 2")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start1) > parseInt(compare_start4) && parseInt(encoded_start1) < parseInt(compare_end4) || parseInt(encoded_end1) > parseInt(compare_start4) && parseInt(encoded_end1) < parseInt(compare_end4)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 3")
                            
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day4+' - '+compare_roomname4+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart4+' - '+compare_12hourend4+'</p>\
                                                            </a>')
                                                            return false;
        
                        }else if(parseInt(encoded_start1) < parseInt(compare_start4) && parseInt(encoded_start1) < parseInt(compare_end4) && parseInt(encoded_end1) <= parseInt(compare_start4) && parseInt(encoded_end1) < parseInt(compare_end4) || parseInt(encoded_start1) > parseInt(compare_start4) && parseInt(encoded_start1) >= parseInt(compare_end4) && parseInt(encoded_end1) > parseInt(compare_start4) && parseInt(encoded_end1) > parseInt(compare_end4)){
                            console.log("valid")
                        }else{
                            console.log("conflict 4")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day4+' - '+compare_roomname4+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart4+' - '+compare_12hourend4+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }

            function ed2cd1(){
                if(encoded_day2 === compare_day1 && encoded_room2 === compare_room1){
                    if(compare_end1 === "NONE" || compare_end1 === "TBA"){
                        if (parseInt(encoded_start2) <= parseInt(compare_start1) && parseInt(encoded_end2) > parseInt(compare_start1)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start1 === "NONE" || compare_start1 === "TBA"){
                        if(parseInt(encoded_start2) < parseInt(compare_end1) && parseInt(encoded_end2) >= parseInt(compare_end1)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start2) > parseInt(compare_start1) && parseInt(encoded_start2) < parseInt(compare_end1) || parseInt(encoded_end2) > parseInt(compare_start1) && parseInt(encoded_end2) < parseInt(compare_end1)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day1+' - '+compare_roomname1+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart1+' - '+compare_12hourend1+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start2) < parseInt(compare_start1) && parseInt(encoded_start2) < parseInt(compare_end1) && parseInt(encoded_end2) <= parseInt(compare_start1) && parseInt(encoded_end2) < parseInt(compare_end1) || parseInt(encoded_start2) > parseInt(compare_start1) && parseInt(encoded_start2) >= parseInt(compare_end1) && parseInt(encoded_end2) > parseInt(compare_start1) && parseInt(encoded_end2) > parseInt(compare_end1)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day1+' - '+compare_roomname1+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart1+' - '+compare_12hourend1+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed2cd2(){
                if(encoded_day2 === compare_day2 && encoded_room2 === compare_room2){
                    if(compare_end1 === "NONE" || compare_end1 === "TBA"){
                        if (parseInt(encoded_start2) <= parseInt(compare_start2) && parseInt(encoded_end2) > parseInt(compare_start2)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start2 === "NONE" || compare_start2 === "TBA"){
                        if(parseInt(encoded_start2) < parseInt(compare_end2) && parseInt(encoded_end2) >= parseInt(compare_end2)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start2) > parseInt(compare_start2) && parseInt(encoded_start2) < parseInt(compare_end2) || parseInt(encoded_end2) > parseInt(compare_start2) && parseInt(encoded_end2) < parseInt(compare_end2)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day2+' - '+compare_roomname2+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart2+' - '+compare_12hourend2+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start2) < parseInt(compare_start2) && parseInt(encoded_start2) < parseInt(compare_end2) && parseInt(encoded_end2) <= parseInt(compare_start2) && parseInt(encoded_end2) < parseInt(compare_end2) || parseInt(encoded_start2) > parseInt(compare_start2) && parseInt(encoded_start2) >= parseInt(compare_end2) && parseInt(encoded_end2) > parseInt(compare_start2) && parseInt(encoded_end2) > parseInt(compare_end2)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day2+' - '+compare_roomname2+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart2+' - '+compare_12hourend2+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed2cd3(){
                if(encoded_day2 === compare_day3 && encoded_room2 === compare_room3){
                    if(compare_end3 === "NONE" || compare_end3 === "TBA"){
                        if (parseInt(encoded_start2) <= parseInt(compare_start3) && parseInt(encoded_end2) > parseInt(compare_start3)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start3 === "NONE" || compare_start3 === "TBA"){
                        if(parseInt(encoded_start2) < parseInt(compare_end3) && parseInt(encoded_end2) >= parseInt(compare_end3)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start2) > parseInt(compare_start3) && parseInt(encoded_start2) < parseInt(compare_end3) || parseInt(encoded_end2) > parseInt(compare_start3) && parseInt(encoded_end2) < parseInt(compare_end3)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day3+' - '+compare_roomname3+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart3+' - '+compare_12hourend3+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start2) < parseInt(compare_start3) && parseInt(encoded_start2) < parseInt(compare_end3) && parseInt(encoded_end2) <= parseInt(compare_start3) && parseInt(encoded_end2) < parseInt(compare_end3) || parseInt(encoded_start2) > parseInt(compare_start3) && parseInt(encoded_start2) >= parseInt(compare_end3) && parseInt(encoded_end2) > parseInt(compare_start3) && parseInt(encoded_end2) > parseInt(compare_end3)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day3+' - '+compare_roomname3+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart3+' - '+compare_12hourend3+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed2cd4(){
                if(encoded_day2 === compare_day4 && encoded_room2 === compare_room4){
                    if(compare_end4 === "NONE" || compare_end4 === "TBA"){
                        if (parseInt(encoded_start2) <= parseInt(compare_start4) && parseInt(encoded_end2) > parseInt(compare_start4)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start4 === "NONE" || compare_start4 === "TBA"){
                        if(parseInt(encoded_start2) < parseInt(compare_end4) && parseInt(encoded_end2) >= parseInt(compare_end4)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start2) > parseInt(compare_start4) && parseInt(encoded_start2) < parseInt(compare_end4) || parseInt(encoded_end2) > parseInt(compare_start4) && parseInt(encoded_end2) < parseInt(compare_end4)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day4+' - '+compare_roomname4+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart4+' - '+compare_12hourend4+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start2) < parseInt(compare_start4) && parseInt(encoded_start2) < parseInt(compare_end4) && parseInt(encoded_end2) <= parseInt(compare_start4) && parseInt(encoded_end2) < parseInt(compare_end4) || parseInt(encoded_start2) > parseInt(compare_start4) && parseInt(encoded_start2) >= parseInt(compare_end4) && parseInt(encoded_end2) > parseInt(compare_start4) && parseInt(encoded_end2) > parseInt(compare_end4)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day4+' - '+compare_roomname4+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart4+' - '+compare_12hourend4+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            
            function ed3cd1(){
                if(encoded_day3 === compare_day1 && encoded_room3 === compare_room1){
                    if(compare_end1 === "NONE" || compare_end1 === "TBA"){
                        if (parseInt(encoded_start3) <= parseInt(compare_start1) && parseInt(encoded_end3) > parseInt(compare_start1)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start1 === "NONE" || compare_start1 === "TBA"){
                        if(parseInt(encoded_start3) < parseInt(compare_end1) && parseInt(encoded_end3) >= parseInt(compare_end1)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start3) > parseInt(compare_start1) && parseInt(encoded_start3) < parseInt(compare_end1) || parseInt(encoded_end3) > parseInt(compare_start1) && parseInt(encoded_end3) < parseInt(compare_end1)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day1+' - '+compare_roomname1+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart1+' - '+compare_12hourend1+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start3) < parseInt(compare_start1) && parseInt(encoded_start3) < parseInt(compare_end1) && parseInt(encoded_end3) <= parseInt(compare_start1) && parseInt(encoded_end3) < parseInt(compare_end1) || parseInt(encoded_start3) > parseInt(compare_start1) && parseInt(encoded_start3) >= parseInt(compare_end2) && parseInt(encoded_end3) > parseInt(compare_start1) && parseInt(encoded_end3) > parseInt(compare_end1)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day1+' - '+compare_roomname1+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart1+' - '+compare_12hourend1+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed3cd2(){
                if(encoded_day3 === compare_day2 && encoded_room3 === compare_room2){
                    if(compare_end2 === "NONE" || compare_end2 === "TBA"){
                        if (parseInt(encoded_start3) <= parseInt(compare_start2) && parseInt(encoded_end3) > parseInt(compare_start2)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start2 === "NONE" || compare_start2 === "TBA"){
                        if(parseInt(encoded_start3) < parseInt(compare_end2) && parseInt(encoded_end3) >= parseInt(compare_end2)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start3) > parseInt(compare_start2) && parseInt(encoded_start3) < parseInt(compare_end2) || parseInt(encoded_end3) > parseInt(compare_start2) && parseInt(encoded_end3) < parseInt(compare_end2)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day2+' - '+compare_roomname2+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart2+' - '+compare_12hourend2+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start3) < parseInt(compare_start2) && parseInt(encoded_start3) < parseInt(compare_end2) && parseInt(encoded_end3) <= parseInt(compare_start2) && parseInt(encoded_end3) < parseInt(compare_end2) || parseInt(encoded_start3) > parseInt(compare_start2) && parseInt(encoded_start3) >= parseInt(compare_end2) && parseInt(encoded_end3) > parseInt(compare_start2) && parseInt(encoded_end3) > parseInt(compare_end2)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day2+' - '+compare_roomname2+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart2+' - '+compare_12hourend2+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed3cd3(){
                if(encoded_day3 === compare_day3 && encoded_room3 === compare_room3){
                    if(compare_end3 === "NONE" || compare_end3 === "TBA"){
                        if (parseInt(encoded_start3) <= parseInt(compare_start3) && parseInt(encoded_end3) > parseInt(compare_start3)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start3 === "NONE" || compare_start3 === "TBA"){
                        if(parseInt(encoded_start3) < parseInt(compare_end3) && parseInt(encoded_end3) >= parseInt(compare_end3)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start3) > parseInt(compare_start3) && parseInt(encoded_start3) < parseInt(compare_end3) || parseInt(encoded_end3) > parseInt(compare_start3) && parseInt(encoded_end3) < parseInt(compare_end3)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day3+' - '+compare_roomname3+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart3+' - '+compare_12hourend3+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start3) < parseInt(compare_start3) && parseInt(encoded_start3) < parseInt(compare_end3) && parseInt(encoded_end3) <= parseInt(compare_start3) && parseInt(encoded_end3) < parseInt(compare_end3) || parseInt(encoded_start3) > parseInt(compare_start3) && parseInt(encoded_start3) >= parseInt(compare_end3) && parseInt(encoded_end3) > parseInt(compare_start3) && parseInt(encoded_end3) > parseInt(compare_end3)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day3+' - '+compare_roomname3+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart3+' - '+compare_12hourend3+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed3cd4(){
                if(encoded_day3 === compare_day4 && encoded_room3 === compare_room4){
                    if(compare_end4 === "NONE" || compare_end4 === "TBA"){
                        if (parseInt(encoded_start3) <= parseInt(compare_start4) && parseInt(encoded_end3) > parseInt(compare_start4)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start4 === "NONE" || compare_start4 === "TBA"){
                        if(parseInt(encoded_start3) < parseInt(compare_end4) && parseInt(encoded_end3) >= parseInt(compare_end4)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start3) > parseInt(compare_start4) && parseInt(encoded_start3) < parseInt(compare_end4) || parseInt(encoded_end3) > parseInt(compare_start4) && parseInt(encoded_end3) < parseInt(compare_end4)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day4+' - '+compare_roomname4+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart4+' - '+compare_12hourend4+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start3) < parseInt(compare_start4) && parseInt(encoded_start3) < parseInt(compare_end4) && parseInt(encoded_end3) <= parseInt(compare_start4) && parseInt(encoded_end3) < parseInt(compare_end4) || parseInt(encoded_start3) > parseInt(compare_start4) && parseInt(encoded_start3) >= parseInt(compare_end4) && parseInt(encoded_end3) > parseInt(compare_start4) && parseInt(encoded_end3) > parseInt(compare_end4)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day4+' - '+compare_roomname4+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart4+' - '+compare_12hourend4+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }

            function ed4cd1(){
                if(encoded_day4 === compare_day1 && encoded_room4 === compare_room1){
                    if(compare_end1 === "NONE" || compare_end1 === "TBA"){
                        if (parseInt(encoded_start4) <= parseInt(compare_start1) && parseInt(encoded_end4) > parseInt(compare_start1)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start1 === "NONE" || compare_start1 === "TBA"){
                        if(parseInt(encoded_start4) < parseInt(compare_end1) && parseInt(encoded_end4) >= parseInt(compare_end1)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start4) > parseInt(compare_start1) && parseInt(encoded_start4) < parseInt(compare_end1) || parseInt(encoded_end4) > parseInt(compare_start1) && parseInt(encoded_end4) < parseInt(compare_end1)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day1+' - '+compare_roomname1+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart1+' - '+compare_12hourend1+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start4) < parseInt(compare_start1) && parseInt(encoded_start4) < parseInt(compare_end1) && parseInt(encoded_end4) <= parseInt(compare_start1) && parseInt(encoded_end4) < parseInt(compare_end1) || parseInt(encoded_start4) > parseInt(compare_start1) && parseInt(encoded_start4) >= parseInt(compare_end1) && parseInt(encoded_end4) > parseInt(compare_start1) && parseInt(encoded_end4) > parseInt(compare_end1)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day1+' - '+compare_roomname1+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart1+' - '+compare_12hourend1+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed4cd2(){
                if(encoded_day4 === compare_day2 && encoded_room4 === compare_room2){
                    if(compare_end2 === "NONE" || compare_end2 === "TBA"){
                        if (parseInt(encoded_start4) <= parseInt(compare_start2) && parseInt(encoded_end4) > parseInt(compare_start2)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start2 === "NONE" || compare_start2 === "TBA"){
                        if(parseInt(encoded_start4) < parseInt(compare_end2) && parseInt(encoded_end4) >= parseInt(compare_end2)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start4) > parseInt(compare_start2) && parseInt(encoded_start4) < parseInt(compare_end2) || parseInt(encoded_end4) > parseInt(compare_start2) && parseInt(encoded_end4) < parseInt(compare_end2)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day2+' - '+compare_roomname2+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart2+' - '+compare_12hourend2+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start4) < parseInt(compare_start2) && parseInt(encoded_start4) < parseInt(compare_end2) && parseInt(encoded_end4) <= parseInt(compare_start2) && parseInt(encoded_end4) < parseInt(compare_end2) || parseInt(encoded_start4) > parseInt(compare_start2) && parseInt(encoded_start4) >= parseInt(compare_end2) && parseInt(encoded_end4) > parseInt(compare_start2) && parseInt(encoded_end4) > parseInt(compare_end2)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day2+' - '+compare_roomname2+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart2+' - '+compare_12hourend2+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed4cd3(){
                if(encoded_day4 === compare_day3 && encoded_room4 === compare_room3){
                    if(compare_end3 === "NONE" || compare_end3 === "TBA"){
                        if (parseInt(encoded_start4) <= parseInt(compare_start3) && parseInt(encoded_end4) > parseInt(compare_start3)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start3 === "NONE" || compare_start3 === "TBA"){
                        if(parseInt(encoded_start4) < parseInt(compare_end3) && parseInt(encoded_end4) >= parseInt(compare_end3)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start4) > parseInt(compare_start3) && parseInt(encoded_start4) < parseInt(compare_end3) || parseInt(encoded_end4) > parseInt(compare_start3) && parseInt(encoded_end4) < parseInt(compare_end3)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day3+' - '+compare_roomname3+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart3+' - '+compare_12hourend3+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start4) < parseInt(compare_start3) && parseInt(encoded_start4) < parseInt(compare_end3) && parseInt(encoded_end4) <= parseInt(compare_start3) && parseInt(encoded_end4) < parseInt(compare_end3) || parseInt(encoded_start4) > parseInt(compare_start3) && parseInt(encoded_start4) >= parseInt(compare_end3) && parseInt(encoded_end4) > parseInt(compare_start3) && parseInt(encoded_end4) > parseInt(compare_end3)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day3+' - '+compare_roomname3+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart3+' - '+compare_12hourend3+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            function ed4cd4(){
                if(encoded_day4 === compare_day4 && encoded_room4 === compare_room4){
                    if(compare_end4 === "NONE" || compare_end4 === "TBA"){
                        if (parseInt(encoded_start4) <= parseInt(compare_start4) && parseInt(encoded_end4) > parseInt(compare_start4)) {
                            console.log("conflict 5")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else if(compare_start4 === "NONE" || compare_start4 === "TBA"){
                        if(parseInt(encoded_start4) < parseInt(compare_end4) && parseInt(encoded_end4) >= parseInt(compare_end4)){
                            console.log("conflict 6")
                            return false;
                        }else{
                            console.log("valid")
                        }
                    }else{
                        if(parseInt(encoded_start4) > parseInt(compare_start4) && parseInt(encoded_start4) < parseInt(compare_end4) || parseInt(encoded_end4) > parseInt(compare_start4) && parseInt(encoded_end4) < parseInt(compare_end4)){
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            console.log("conflict 7")
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day4+' - '+compare_roomname4+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart4+' - '+compare_12hourend4+'</p>\
                                                            </a>')
                                                            return false;
                        }else if(parseInt(encoded_start4) < parseInt(compare_start4) && parseInt(encoded_start4) < parseInt(compare_end4) && parseInt(encoded_end4) <= parseInt(compare_start4) && parseInt(encoded_end4) < parseInt(compare_end4) || parseInt(encoded_start4) > parseInt(compare_start4) && parseInt(encoded_start4) >= parseInt(compare_end4) && parseInt(encoded_end4) > parseInt(compare_start4) && parseInt(encoded_end4) > parseInt(compare_end4)){
                            console.log("valid")
                        }else{
                            console.log("conflict 8")
                            var num = parseInt($(".noc").text()) + 1;
                            $(".noc").text(num);
                            $(".list-of-conflict").append('<a href="javascript:;" class="list-group-item" style="border:0px;">\
                                                            <h5 class="list-group-item-heading"><b>REFERENCE #'+$(".no-reference li:nth-of-type(1)").eq(i).text()+'</b></h5>'+compare_day4+' - '+compare_roomname4+'\
                                                            <p class="list-group-item-text">Time: '+compare_12hourstart4+' - '+compare_12hourend4+'</p>\
                                                            </a>')
                                                            return false;
                        }
                    }
                }else{
                }
            }
            
            ed1cd1();
            ed1cd2();
            ed1cd3();
            ed1cd4();

            ed2cd1();
            ed2cd2();
            ed2cd3();
            ed2cd4();

            ed3cd1();
            ed3cd2();
            ed3cd3();
            ed3cd4();

            ed4cd1();
            ed4cd2();
            ed4cd3();
            ed4cd4()
        }
        var noConflict = parseInt($(".noc").text())
        if(noConflict === 0){
        
            $(".btn-conflictChecker").slideUp();
            $(".btnAdd-subjectsection").slideDown();
        }else{
            $(".btn-conflictChecker").slideDown();
            $(".btnAdd-subjectsection").slideUp();
        }
    }
});