$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.payment = (typeof $.main.payment !=='undefined')? $.main.payment : {};

$.main.payment =(function() {
        var host = window.location.protocol+"//"+window.location.hostname ;
    var __getAllPaymentOption = function() {
        //Part of Viewing
        $.main.executeExternalGet(host+"/api/index.php/Ams_controller/fetchAllPayment").done(function(result){
            if ( $.fn.DataTable.isDataTable('#myTable1') ) {
                    $('#myTable1').DataTable().destroy();
                    $('#myTable1 tbody').empty();
            }
            console.log(result);
            var arr = [];
            for(i=0; i<result.payload.length; i++){
                //console.log(result);
                var obj = {};
                var item = result.payload[i];
                var btnUpdate ="<a class='btnUpdateTest btn btn-success P6C10_UPDATE' title='Update Info' data-clearance='"+item.po_ID+"' data-toggle='modal' data-target='#updateSamp'>Update</a>";
                obj['sem_NAME'] = item.sem_NAME;
                obj['po_NAME'] = item.po_NAME;
                obj['po_book_fees'] = item.po_book_fees;
                obj['po_lab_fees'] = item.po_lab_fees;
                obj['po_regother_fees'] = item.po_regother_fees;
                obj['po_LAB'] = item.po_LAB;
                obj['po_LEC'] = item.po_LEC;
                obj['po_MISC'] = item.po_MISC;
                obj['po_discount_tf'] = item.po_discount_tf;
                obj['po_discount_misc'] = item.po_discount_misc;
                obj['po_installment_fee'] = item.po_installment_fee;
                obj['po_additional_tf'] = item.po_additional_tf;
                obj['po_additional_tfee'] = item.po_additional_tfee;
                obj['po_additional_misc'] = item.po_additional_misc;
                obj['po_DATE1'] = item.po_DATE1;
                obj['po_DATE2'] = item.po_DATE2;
                obj['po_DATE3'] = item.po_DATE3;
                obj['po_DATE4'] = item.po_DATE4;
                obj['po_DATE5'] = item.po_DATE5;
                obj['po_SNPL'] = item.po_SNPL;
                obj['ACTION'] = btnUpdate
                arr.push(obj);
            }
            $(document).ready( function() {
                   $('#myTable1').DataTable({
                       'data' : arr,
                       "lengthChange": false,
                       'scrollX': true,
                       'columns' : [{"data": "sem_NAME"},{"data": "po_NAME"},{"data": "po_book_fees"},{"data": "po_lab_fees"},{"data": "po_regother_fees"},{"data": "po_LAB"},{"data": "po_LEC"},{"data": "po_MISC"},{"data": "po_discount_tf"},{"data": "po_discount_misc"},{"data": "po_installment_fee"},{"data": "po_additional_tf"},{"data": "po_additional_tfee"},{"data": "po_additional_misc"},{"data": "po_DATE1"},{"data": "po_DATE2"},{"data": "po_DATE3"},{"data": "po_DATE4"},{"data": "po_DATE5"},{"data": "po_SNPL"},{"data": "ACTION"}]

                   });
            });
           
            //Part of Updating Payment Options
            function validateUpSem(){
                if($("#UpSem").val() == ""){
                    $("#UpSem").closest("div").addClass("has-error");
                    return false;
                }else{
                    $("#UpSem").closest("div").removeClass("has-error").addClass("has-success");
                    return true;
                }
            }
            function validateUpPoName(){
                if($("#UpName").val() == ""){
                    $("#UpName").closest("div").addClass("has-error");
                    return false;
                }else{
                    $("#UpName").closest("div").removeClass("has-error").addClass("has-success");
                    return true;
                }
            }
            function validateUpPoDATE1(){
                if($("#up_po_DATE1").val() == ""){
                    $("#up_po_DATE1").closest("div").addClass("has-error");
                    return false;
                }else{
                    $("#up_po_DATE1").closest("div").removeClass("has-error").addClass("has-success");
                    return true;
                }
            }
            $("#myTable1 tbody").on("click", ".btnUpdateTest", function(){
                $("#myModal").modal();
                var id = $(this).data("clearance");
                var payload = {
                    "po_ID": id
                }
                $.main.executeExternalPost(host+"/api/index.php/Ams_controller/fetchPaymentById", JSON.stringify(payload)).done(function(result){
                    if(result.status === 'SUCCESS'){
                        console.log(result);
                        $("#UpName").val(result.payload[0].po_NAME);
                        $("#UpBookFees").val(result.payload[0].po_book_fees);
                        $("#UpRegOtherFees").val(result.payload[0].po_regother_fees);
                        $("#UpLabFees").val(result.payload[0].po_lab_fees);
                        $("#UpLab").val(result.payload[0].po_LAB);
                        $("#UpLec").val(result.payload[0].po_LEC);
                        $("#UpMisc").val(result.payload[0].po_MISC);
                        $("#up_po_installment_fee").val(result.payload[0].po_installment_fee);
                        $("#up_po_additional_tfee").val(result.payload[0].po_additional_tfee);
                        $("#up_po_DATE1").val(result.payload[0].po_DATE1);
                        $("#up_po_DATE2").val(result.payload[0].po_DATE2);
                        $("#up_po_DATE3").val(result.payload[0].po_DATE3);
                        $("#up_po_DATE4").val(result.payload[0].po_DATE4);
                        $("#up_po_DATE5").val(result.payload[0].po_DATE5);
                        var selectedSNPL = result.payload[0].po_SNPL;
                        var selectedDiscountTf = result.payload[0].po_discount_tf;
                        var selectedDiscountMisc = result.payload[0].po_discount_misc;
                        var selectedAdditionalTf = result.payload[0].po_additional_tf;
                        var selectedAdditionalMisc = result.payload[0].po_additional_misc;

                        //snpl start
                        $("#up_po_SNPL").html("");
                        for(i=1; i<=9; i++){
                            $("#up_po_SNPL").append("<option value=.0"+i+">.0"+i+"%</option>");
                        }
                        for(i=10; i<=99; i++){
                            $("#up_po_SNPL").append("<option value=."+i+">."+i+"%</option>");
                        }
                        $("#up_po_SNPL").append("<option value='1'>100%</option>");
                        $("#up_po_SNPL").val(selectedSNPL);
                        //snpl end

                        //discount tuition fee start
                        $("#up_po_discount_tf").html("");
                        for(i=1; i<=9; i++){
                            $("#up_po_discount_tf").append("<option value=.0"+i+">.0"+i+"%</option>");
                        }
                        for(i=10; i<=99; i++){
                            $("#up_po_discount_tf").append("<option value=."+i+">."+i+"%</option>");
                        }
                        $("#up_po_discount_tf").append("<option value='1'>100%</option>");
                        $("#up_po_discount_tf").val(selectedDiscountTf);
                        //discount tuition fee end

                        //discount tuition misc start
                        $("#up_po_discount_misc").html("");
                        for(i=1; i<=9; i++){
                            $("#up_po_discount_misc").append("<option value=.0"+i+">.0"+i+"%</option>");
                        }
                        for(i=10; i<=99; i++){
                            $("#up_po_discount_misc").append("<option value=."+i+">."+i+"%</option>");
                        }
                        $("#up_po_discount_misc").append("<option value='1'>100%</option>");
                        $("#up_po_discount_misc").val(selectedDiscountMisc);
                        //discount tuition misc end


                        //additional tuition fee start
                        $("#up_po_additional_tf").html("");
                        for(i=1; i<=9; i++){
                            $("#up_po_additional_tf").append("<option value=.0"+i+">.0"+i+"%</option>");
                        }
                        for(i=10; i<=99; i++){
                            $("#up_po_additional_tf").append("<option value=."+i+">."+i+"%</option>");
                        }
                        $("#up_po_additional_tf").append("<option value='1'>100%</option>");
                        $("#up_po_additional_tf").val(selectedAdditionalTf);
                        //additional tuition fee end

                        //additional tuition misc start
                        $("#up_po_additional_misc").html("");
                        for(i=1; i<=9; i++){
                            $("#up_po_additional_misc").append("<option value=.0"+i+">.0"+i+"%</option>");
                        }
                        for(i=10; i<=99; i++){
                            $("#up_po_additional_misc").append("<option value=."+i+">."+i+"%</option>");
                        }
                        $("#up_po_additional_misc").append("<option value='1'>100%</option>");
                        $("#up_po_additional_misc").val(selectedAdditionalMisc);
                        //additional tuition misc end

                        var selectedSemName = result.payload[0].sem_ID
                        $.main.executeExternalGet(host+"/api/index.php/Ams_controller/fetchAllSem").done(function(result){
                            if(result.status === 'SUCCESS'){
                                $("#UpSem").html("");
                                for(i=0; i<result.payload.length; i++){
                                    $("#UpSem").append("<option value="+result.payload[i].sem_ID+">"+result.payload[i].sem_NAME+"</option>");
                                }
                                $("#UpSem").val(selectedSemName);
                            }
                        });
                        var peyload2 = {
                            'sem_ID':  selectedSemName 
                        }
                        $.main.executeExternalPost(host+"/api/index.php/Ams_controller/fetchAllSemByID",JSON.stringify(peyload2)).done(function(result){
                            if(result.status ==='SUCCESS'){
                                var sem1 = result.payload.sem_NAME; 
                                localStorage.setItem("sem_NAME1", sem1);
                                
                            }else{
                                console.log(result.message);
                            }
                        });
                        
                       


                        $("#updatePaymentOption").unbind().bind("click",function(){
                            var peyload3 = {
                                 'sem_ID':$("#UpSem option:selected").val() 
                            }
                            $.main.executeExternalPost(host+"/api/index.php/Ams_controller/fetchAllSemByID",JSON.stringify(peyload3)).done(function(result){
                                if(result.status ==='SUCCESS'){
                                    var sem2 = result.payload.sem_NAME; 
                                    localStorage.setItem("sem_NAME2", sem2);
                                }else{
                                    console.log(result.message);
                                }
                            });
                            var sample99 = localStorage.getItem("sem_NAME1");
                            var sample69 = localStorage.getItem("sem_NAME2");
                            console.log(sample69);
                            if(validateUpSem() && validateUpPoName() && validateUpPoDATE1() === true){
                                if($('#up_po_DATE1').val() == ""){
                                    var UpPoDate1 = null;
                                }else{
                                    var UpPoDate1 = $('#up_po_DATE1').val();
                                }
                                if($('#up_po_DATE2').val() == ""){
                                    var UpPoDate2 = null;
                                }else{
                                    var UpPoDate2 = $('#up_po_DATE2').val();
                                }
                                if($('#up_po_DATE3').val() == ""){
                                    var UpPoDate3 = null;
                                }else{
                                    var UpPoDate3 = $('#up_po_DATE3').val();
                                }
                                if($('#up_po_DATE4').val() == ""){
                                    var UpPoDate4 = null;
                                }else{
                                    var UpPoDate4 = $('#up_po_DATE4').val();
                                }
                                if($('#up_po_DATE5').val() == ""){
                                    var UpPoDate5 = null;
                                }else{
                                    var UpPoDate5 = $('#up_po_DATE5').val();
                                }
                                var payload = {
                                    'po_ID':id,
                                    'sem_ID':$("#UpSem option:selected").val(),
                                    'po_NAME':$("#UpName").val(),
                                    'po_book_fees':$("#UpBookFees").val(),
                                    'po_lab_fees':$("#UpLabFees").val(),
                                    'po_regother_fees':$("#UpRegOtherFees").val(),
                                    'po_LAB':$("#UpLab").val(),
                                    'po_LEC':$("#UpLec").val(),
                                    'po_MISC':$("#UpMisc").val(),
                                    'po_DATE1':UpPoDate1,
                                    'po_DATE2':UpPoDate2,
                                    'po_DATE3':UpPoDate3,
                                    'po_DATE4':UpPoDate4,
                                    'po_DATE5':UpPoDate5,
                                    'po_discount_tf':$("#up_po_discount_tf").val(),
                                    'po_discount_misc':$("#up_po_discount_misc").val(),
                                    'po_installment_fee':$("#up_po_installment_fee").val(),
                                    'po_additional_tf':$("#up_po_additional_tf").val(),
                                    'po_additional_misc':$("#up_po_additional_misc").val(),
                                    'po_additional_tfee':$("#up_po_additional_tfee").val(),
                                    'po_SNPL':$("#up_po_SNPL").val()
                                }
                                var before99 = {
                                    'Semester Name':sample99,
                                    'Payment Option Name':result.payload[0].po_NAME,
                                    'Book Fees':result.payload[0].po_book_fees,
                                    'Registration/Other Fees':result.payload[0].po_regother_fees,
                                    'Laboratory Fees':result.payload[0].po_lab_fees,
                                    'Laboratory':result.payload[0].po_LAB,
                                    'Lecture':result.payload[0].po_LEC,
                                    'Miscellaneous':result.payload[0].po_MISC,
                                    'Discount Tuition Fee(%)':result.payload[0].po_discount_tf,
                                    'Discount Miscellaneous(%)':result.payload[0].po_discount_misc,
                                    'Installment Fee(fixed)':result.payload[0].po_installment_fee,
                                    'Additional Tuition Fee(%)':result.payload[0].po_additional_tf,
                                    'Additional Tuition Fee(fixed)':result.payload[0].po_additional_tfee,
                                    'Additional Miscellaneous(%)':result.payload[0].po_additional_misc,
                                    'Date 1':result.payload[0].po_DATE1,
                                    'Date 2':result.payload[0].po_DATE2,
                                    'Date 3':result.payload[0].po_DATE3,
                                    'Date 4':result.payload[0].po_DATE4,
                                    'Date 5':result.payload[0].po_DATE5,
                                    'SNPL':result.payload[0].po_SNPL
                                } 
                                var after99 = {
                                    'Semester Name':sample69,
                                    'Payment Option Name':$("#UpName").val(),
                                    'Book Fees':$("#UpBookFees").val(),
                                    'Registration/Other Fees':$("#UpRegOtherFees").val(),
                                    'Laboratory Fees':$("#UpLabFees").val(),
                                    'Laboratory':$("#UpLab").val(),
                                    'Lecture':$("#UpLec").val(),
                                    'Miscellaneous':$("#UpMisc").val(),
                                    'Discount Tuition Fee(%)':$("#up_po_discount_tf").val(),
                                    'Discount Miscellaneous(%)':$("#up_po_discount_misc").val(),
                                    'Installment Fee(fixed)':$("#up_po_installment_fee").val(),
                                    'Additional Tuition Fee(%)':$("#up_po_additional_tf").val(),
                                    'Additional Tuition Fee(fixed)':$("#up_po_additional_tfee").val(),
                                    'Additional Miscellaneous(%)':$("#up_po_additional_misc").val(),
                                    'Date 1':UpPoDate1,
                                    'Date 2':UpPoDate2,
                                    'Date 3':UpPoDate3,
                                    'Date 4':UpPoDate4,
                                    'Date 5':UpPoDate5,
                                    'SNPL':$("#up_po_SNPL").val()
                                }
                                var asdasd1 = {};
                                $.each(before99, function (key, value) {
                                    if (!after99.hasOwnProperty(key) || after99[key] !== before99[key]) {
                                        asdasd1[key] = value;
                                    }
                                });
                                var asdasd2 = {};
                                $.each(after99, function (key, value) {
                                    if (!before99.hasOwnProperty(key) || before99[key] !== after99[key]) {
                                        asdasd2[key] = value;
                                    }
                                });
                                
                                var b4 = JSON.stringify(asdasd1);
                                var aft = JSON.stringify(asdasd2);
                                var des2 = b4.replace(/[&\/\\#+()$~%'"*?<>{}]/g,' ');
                                var des3 = aft.replace(/[&\/\\#+()$~%'"*?<>{}]/g,' ');

                                
                                $.main.executeExternalPost(host+"/api/index.php/Ams_controller/updatePaymentOption", JSON.stringify(payload)).done(function(result){
                                    if(result.status === 'SUCCESS'){
                                        var acctid1 = $("#myTable").data('acct');
                                        var ip1 = $("#myTable").data('ip');
                                        
                                        
                                        if(des3 === null || des3 === ""){
                                            var event_desc1 = "MODULE: Accounting / Payment Option. DESCRIPTION: Nothing to Change";
                                        }else{
                                            var event_desc1 = "MODULE: Accounting / Payment Option. DESCRIPTION: User Updated Payment Option with an ID of "+id+" Details: BEFORE:"+des2+" AFTER: "+des3;
                                        }
                                        var asdasdasdasd1 = {
                                            "method" : "PostAudit",
                                            "acct_id":acctid1, 
                                            "client_ip":ip1,
                                            "event_action":"Update",
                                            "eventdesc": event_desc1
                                                
                                        }
                                        $.main.executeExternalPost(host+"/api2/wsv1/api/Audit/",JSON.stringify(asdasdasdasd1)).done(function(result){
                                            if(result.status === 'SUCCESS'){
                                                console.log(result);
                                            }else{
                                                console.log(result.message);
                                            }
                                        });
                                        $("#update").removeClass("hidden");
                                        location.reload();
                                        console.log(result);
                                    }else{
                                        console.log(result.message);
                                    }
                                });
                            }else{  
                                console.log("has-error");
                            }
                        });
                    }else{
                        console.log(result.message);
                    }
                });
            });
        });

        //Part of adding Payment Options
        function validateSem(){
            if($("#day").val() == ""){
                $("#day").closest("div").addClass("has-error");
                return false;
            }else{
                $("#day").closest("div").removeClass("has-error").addClass("has-success");
                return true;
            }
        }
        function validateFee(){
            if($("#fee").val() == ""){
                $("#fee").closest("div").addClass("has-error");
                return false;
            }else{
                $("#fee").closest("div").removeClass("has-error").addClass("has-success");
                return true;
            }
        }
        function validatePoName(){
            if($("#po_NAME").val() == ""){
                $("#po_NAME").closest("div").addClass("has-error");
                return false;
            }else{
                $("#po_NAME").closest("div").removeClass("has-error").addClass("has-success");
                return true;
            }
        }
        function validatePoDATE1(){
            if($("#po_DATE1").val() == ""){
                $("#po_DATE1").closest("div").addClass("has-error");
                return false;
            }else{
                $("#po_DATE1").closest("div").removeClass("has-error").addClass("has-success");
                return true;
            }
        }
        $.main.executeExternalGet(host+"/api/index.php/Ams_controller/fetchAllSem").done(function(result){
            if(result.status === 'SUCCESS'){
                console.log(result);
                for(i=0; i<result.payload.length; i++){
                    $("#day").append("<option value='"+result.payload[i].sem_ID+"'>"+result.payload[i].sem_NAME+"</option>");
                }

            }else{
                console.log(result.message);
            }
        });
        $.main.executeExternalGet(host+"/api/index.php/MagsaysayAccountingController/getAllFeeTemplate").done(function(result){
            if(result.status === 'SUCCESS'){
                console.log(result);
                for(i=0; i<result.payload.length; i++){
                    $("#fee").append("<option value='"+result.payload[i].fee_template_id+"'>"+result.payload[i].fee_template_name+"</option>");
                }
                 
                $("#fee").change(function(){
                    var payload = {
                        'fee_template_id':$("#fee option:selected").val()
                    }
                    $.main.executeExternalPost(host+"/api/index.php/MagsaysayAccountingController/getAllFeeConfigByTemplateID",JSON.stringify(payload)).done(function(result){
                      
                        if(result.result === 'SUCCESS'){
                            var misc = 0;
                            var lab = 0;
                            var other = 0;
                            var book = 0;
                            var lec = 0;
                            var lab1 = 0;
                            var template_name = result.payload[0].fee_template_name;
                            localStorage.setItem("template_NAME", template_name);
                            
                            for(i=0; i<result.payload.length; i++){
                                $("#myTable").find(".form-control.4").val(result.payload[i].fee_value)
                               
                                if(result.payload[i].fee_type_id == 1){
                                    lec += parseFloat(result.payload[i].fee_value);
                                    lab1 += parseFloat(result.payload[i].fee_value);
                                }else if(result.payload[i].fee_type_id == 2){
                                    other += parseFloat(result.payload[i].fee_value);
                                }else if(result.payload[i].fee_type_id == 3){
                                    lab += parseFloat(result.payload[i].fee_value);
                                }else if(result.payload[i].fee_type_id == 4){
                                    misc += parseFloat(result.payload[i].fee_value);
                                }else if(result.payload[i].fee_type_id == 5){
                                    book += parseFloat(result.payload[i].fee_value);
                                }else{
                                }
                            }
                                $("#po_MISC").val(misc);
                                $("#po_lab_fees").val(lab);
                                $("#po_regother_fees").val(other);
                                $("#po_book_fees").val(book);
                                $("#po_LEC").val(lec);
                                $("#po_LAB").val(lab1);
                        }else{
                            console.log(result.message);
                        }
                    });
                });
            }else{
                console.log(result.message);
            }
        });
        $("#button").unbind().bind("click", function(){
            if(validateSem() && validateFee() && validatePoName() && validatePoDATE1()=== true){
                if($('#po_DATE1').val() == ""){
                var poDate1 = null;
                }else{
                    var poDate1 = $('#po_DATE1').val();
                }
                if($('#po_DATE2').val() == ""){
                    var poDate2 = null;
                }else{
                    var poDate2 = $('#po_DATE2').val();
                }
                if($('#po_DATE3').val() == ""){
                    var poDate3 = null;
                }else{
                    var poDate3 = $('#po_DATE3').val();
                }
                if($('#po_DATE4').val() == ""){
                    var poDate4 = null;
                }else{
                    var poDate4 = $('#po_DATE4').val();
                }
                if($('#po_DATE5').val() == ""){
                    var poDate5 = null;
                }else{
                    var poDate5 = $('#po_DATE5').val();
                }
                var payload = {
                    'sem_ID': $("#day option:selected").val(),
                    'po_NAME': $('#po_NAME').val(),
                    'po_book_fees':$("#po_book_fees").val(),
                    'po_lab_fees':$("#po_lab_fees").val(),
                    'po_regother_fees':$("#po_regother_fees").val(),
                    'po_LAB': $("#po_LAB").val(),
                    'po_LEC': $('#po_LEC').val(),
                    'po_MISC': $("#po_MISC").val(),
                    'po_DATE1': poDate1,
                    'po_DATE2': poDate2,
                    'po_DATE3': poDate3,
                    'po_DATE4': poDate4,
                    'po_DATE5': poDate5,
                    'po_discount_tf': $('#po_discount_tf').val(),
                    'po_discount_misc': $('#po_discount_misc').val(),
                    'po_installment_fee': $('#po_installment_fee').val(),
                    'po_additional_tf': $('#po_additional_tf').val(),
                    'po_additional_misc': $('#po_additional_misc').val(),
                    'po_additional_tfee': $('#po_additional_tfee').val(),
                    'po_SNPL': $("#po_SNPL").val()
                }
                ///////////////////////////////////// Audit Trail Start (Jeelbert) ////////////////////////////////////////
                var peyload = {
                    'sem_ID':$("#day option:selected").val()
                }
                $.main.executeExternalPost(host+"/api/index.php/Ams_controller/fetchAllSemByID",JSON.stringify(peyload)).done(function(result){
                    if(result.status ==='SUCCESS'){
                        var sem = result.payload.sem_NAME; 
                        localStorage.setItem("sem_NAME", sem);
                        
                    }else{
                        console.log(result.message);
                    }
                });
                $.main.executeExternalPost(host+"/api/index.php/Ams_controller/insertNewPaymentOption",JSON.stringify(payload)).done(function(result){

                    if(result.status === 'SUCCESS'){
                        $("#success").removeClass("hidden");
                        var sample1 = localStorage.getItem("template_NAME");
                        var sample = localStorage.getItem("sem_NAME");
                        var payload = {
                            "Semester Name":sample,
                            "Fee Template Name":sample1,
                            "Payment Option Name":$("#po_NAME").val(),
                            "Book Fees":$("#po_book_fees").val(),
                            "Registration/Other Fees":$("#po_regother_fees").val(),
                            "Laboratory Fees":$("#po_lab_fees").val(),
                            "Miscellaneous":$("#po_MISC").val(),
                            "Lacture":$('#po_LEC').val(),
                            "Laboratory":$("#po_LAB").val(),
                            "Discount Tuition Fee(%)":$('#po_discount_tf').val(),
                            "Discount Miscellaneous(%)":$('#po_discount_misc').val(),
                            "Installment Fee(fixed)":$('#po_installment_fee').val(),
                            "Additional Tuition Fee(%)":$('#po_additional_tf').val(),
                            "Additional Tuition Fee(fixed)":$('#po_additional_tfee').val(),
                            "Additional Miscellaneous(%)":$('#po_additional_misc').val(),
                            "Date 1":poDate1,
                            "Date 2":poDate2,
                            "Date 3":poDate3,
                            "Date 4":poDate4,
                            "Date 5":poDate5,
                            "Payment Option SNPL":$("#po_SNPL").val()
                        }
                        var before = JSON.stringify(payload);

                        var acctid = $("#myTable").data('acct');
                        var ip = $("#myTable").data('ip');
                        var desc = before.replace(/[&\/\\#+()$~%'"*?<>{}]/g,' ');
                        var event_desc = "MODULE: Accounting / Payment Option. DESCRIPTION: User Added/Insert New Payment Option. Details: "+desc;
                        var asdasdasdasd = {
                            "method" : "PostAudit",
                            "acct_id":acctid, 
                            "client_ip":ip,
                            "event_action":"Add",
                            "eventdesc": event_desc
                                
                        }
                        $.main.executeExternalPost(host+"/api2/wsv1/api/Audit/",JSON.stringify(asdasdasdasd)).done(function(result){
                            if(result.status === 'SUCCESS'){
                                console.log(result);
                            }else{
                                console.log(result.message);
                            }
                        });
                        location.reload();
                        console.log(result);
                    }else{
                        console.log(result.message);
                    }
                });
                


                
            }else{
                console.log("Error");
            }
            
        });
}
return {
    getAllPaymentOption : __getAllPaymentOption

};
}());