$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.audit = (typeof $.main.audit !=='undefined')? $.main.audit : {};

$.main.audit =(function() {
    var __getAllAudit = function() {
      var host = window.location.protocol+"//"+window.location.hostname ;
       var api = host+"/api2/wsv1/api/Audit/";
    	// var api = "http://localhost/csjp_api/wsv1/api/Audit/";
      var api1 = "http://localhost/csjp_api/wsv1/api/Audit/";
    	console.log('connected');
    	var payload = {
    		'method' : 'fetchAudit'
    	}
      // var dataRow = $("#table_audit").DataTable(
      //     {
      //       'dom' : 'Brftip',
      //       'data':arr,
      //       'destroy': true,
      //       "scrollY": true,
      //       "bFilter": false, 
      //       "bLengthChange": false,
      //       'scrollX' : true,
      //       'columns': [{"data": "Audit ID"},{"data": "User Name"},{"data": "Access Level"},{"data": "Client IP Address"},{"data": "Action Event"},{"data": "Event Description"},{"data": "Event Date Time"} ],
      //        'buttons': [
      //              {  
      //               extend: 'excelHtml5',
      //               title: "Exam Result",download: 'open',
      //               exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
      //              // title: "Transaction Report " + $("#start").val(),download: 'open'
      //                 exportOptions: {
      //                     columns: [ 0, 1 ]
      //                 }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'
      //             },{
                  
      //               extend:  'pdfHtml5',
      //               title: "Exam Result",download: 'open',
      //               exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
      //             },
      //             {   
      //               extend: 'csvHtml5',
      //               title: "Exam Result",download: 'open',
      //               exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
      //             },
      //            ]
      //     });

      //     $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
      //     $(".dt-buttons").addClass("hidden")
    	$.main.executeExternalPost(api,JSON.stringify(payload)).done(function(result){
    		if(result.status === 'SUCCESS'){
    			console.log(result)

    			if ( $.fn.DataTable.isDataTable('#table_audit') ) {
			        $('#table_audit').DataTable().destroy();
			        $('#table_audit tbody').empty();
				}

	      		var arr = [];
	      		for (i=0;i<result.payload.length;i++ ){
                   var obj = {};
                   var item = result.payload[i];
                   obj['AUDIT_ID'] = item.AUDIT_ID;
                   obj['EVENT_DATETIME'] = item.EVENT_DATETIME;
                   obj['FULLNAME'] = item.FULLNAME;
                   // obj['USERNAME'] = item.USERNAME;
                   obj['ACCESS_LEVEL'] = item.ACCESS_LEVEL;
                   obj['CLIENT_IP'] = item.CLIENT_IP;
                   obj['ACTION_EVENT'] = item.ACTION_EVENT;
                   obj['EVENT_DESC'] = item.EVENT_DESCRIPTION;
                   arr.push(obj);
                }
                //"lengthChange": false,
                $(document).ready( function() {
                   $("table.dataTable thead .sorting").eq(0).hide()
                   $("#table_audit tbody tr td:nth-of-type(1)").hide()
                   $('#table_audit').DataTable({
                       'scrollX': true,
                       'order': [[ 0, "desc" ]],
                       'data' : arr,
                       'columns' : [{"data": "AUDIT_ID"},{"data": "EVENT_DATETIME"},{"data": "FULLNAME"},{"data": "ACCESS_LEVEL"},{"data": "CLIENT_IP"},{"data": "ACTION_EVENT"},{"data": "EVENT_DESC"}],
                       'columnDefs' : [{"visible": false, "targets": 0}],
                        dom: 'Bfrtip',
                        buttons: [
                            'csv'
                        ]
                       // 'columns' : [{"data": "AUDIT_ID"},{"data": "EVENT_DATETIME"},{"data": "FULLNAME"},{"data": "USERNAME"},{"data": "ACCESS_LEVEL"},{"data": "CLIENT_IP"},{"data": "ACTION_EVENT"},{"data": "EVENT_DESC"}]

                   });
                   $(".dt-button.buttons-csv.buttons-html5").addClass('btn btn-success btn-ms');
                   $(".dt-button.buttons-csv.buttons-html5").prependTo("#csvDiv")
                });
    		}else{
    			console.log(result.message);
    		}
    	});
    }
    return {
        getAllAudit : __getAllAudit
    };
}());
