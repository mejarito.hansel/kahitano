
$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.manual = (typeof $.main.manual !=='undefined')? $.main.manual : {};

$.main.manual =(function() {
    var host = window.location.protocol+"//"+window.location.hostname ;
    var path = host+"/api/index.php/Magsaysay_controller/"

    var __manual_encode = function() {
        $('#searchBtn').unbind('click').on('click',function(){

    		$("#iqScore").closest("div").removeClass("has-error");
    		$("#iqError").addClass("hide");
    		$("#eqA").closest("div").removeClass("has-error");
    		$("#eqAerror").addClass("hide");
    		$("#eqB").closest("div").removeClass("has-error");
    		$("#eqBerror").addClass("hide");
    		$("#eqC").closest("div").removeClass("has-error");
    		$("#eqCerror").addClass("hide");
    		$("#eqD").closest("div").removeClass("has-error");
    		$("#eqDerror").addClass("hide");
    		$("#eqE").closest("div").removeClass("has-error");
    		$("#eqEerror").addClass("hide");

			var applicantID = ("application#" + $("#applicantID").val())
	    	payload = {
	    		"application_number" : applicantID
	    	}
			// console.log(payload)
			/////////////////////////////////// Audit Trail Start (Jeelbert) ///////////////////////////////
			var acct_id = $("#searchBtn").data('acct');
			var ip_addr = $("#searchBtn").data('ip');
			var payload23 = {
                'method':"PostAudit",
                'acct_id':acct_id,
                'client_ip':ip_addr,
                'event_action':"Search",
                'eventdesc': "Module: Admission / Manual Encoding of Test Result. Description: User Searches Applicant No# "+$("#applicantID").val()
              }
              $.main.executeExternalPost(host+"/api2/wsv1/api/Audit/",JSON.stringify(payload23)).done(function(result){
                if(result.status === 'SUCCESS'){
                  console.log(result);
                }else{
                  console.log(result.message);
                }
              });			
			/////////////////////////////////// Audit Trail End (Jeelbert) /////////////////////////////////

	      	$.main.executeExternalPost(path + "getApplicantByNumber", JSON.stringify(payload)).done(function(result) {
	        	console.log(result);
		        if (result.status == "SUCCESS") {
		        	console.log('dito na tayo <3');
		        	var payload = result.payload[0];
			        $("#studName").val(payload.fname)
			        $("#iqScore").val(payload.IQ)
			        $("#eqA").val(payload.A)
			        $("#eqB").val(payload.B)
			        $("#eqC").val(payload.C)
			        $("#eqD").val(payload.D)
			        $("#eqE").val(payload.E)
		        	$(".formYieh").removeClass("hide");
		        	$("#applicantID").closest("div").removeClass("has-error").addClass("has-success")
		        	if (result.message == "SUCCESS") {
		        		$("#savebutton").addClass("hide")
		        		$("#updatebutton").removeClass("hide")
		        	}else{
		        		$("#savebutton").removeClass("hide")
		        		$("#updatebutton").addClass("hide")
		        	}
		        }else{
		        	$("#applicantID").closest("div").addClass("has-error");
		        	$(".formYieh").addClass("hide");
		        }

        		$('#saveBtn').unbind('click').on('click',function(){

			      	payload = {
		      		    "application_id": payload.application_id,
					    "iq_score" : $("#iqScore").val(),
					    "eqA" : $("#eqA").val(),
					    "eqB" : $("#eqB").val(),
					    "eqC" : $("#eqC").val(),
					    "eqD" : $("#eqD").val(),
					    "eqE" : $("#eqE").val()
			      	}
			      	console.log(payload)
	                if(__validatedata(payload)){
		      			$("#saveBtn").addClass("hide")
				      	$.main.executeExternalPost(path + "submitExamResult", JSON.stringify(payload)).done(function(result) {
				      		console.log(result)
				      		
				      		if (result.status == "SUCCESS") {
				      			$("#saveSuccess").removeClass("hide")
				                setTimeout(function() {
				                  window.location.href = "exam_results.php"
				                },200);
				      		}
				      	})
			      	}
	      		})

        		$('#updateBtn').unbind('click').on('click',function(){

			      	payload = {    
			      		"iq_id" : payload.iq_id,
						"eq_id" : payload.eq_id,
		      		    "application_id": payload.application_id,
					    "iq_score" : $("#iqScore").val(),
					    "eqA" : $("#eqA").val(),
					    "eqB" : $("#eqB").val(),
					    "eqC" : $("#eqC").val(),
					    "eqD" : $("#eqD").val(),
					    "eqE" : $("#eqE").val()
			      	}
			      	console.log(payload)
	                if(__validatedata(payload)){
		      			$("#updateBtn").addClass("hide")
				      	$.main.executeExternalPost(path + "updateExamResult", JSON.stringify(payload)).done(function(result) {
				      		console.log(result)
				      		if (result.status == "SUCCESS") {
				      			$("#updateSuccess").removeClass("hide")
					      		setTimeout(function() {
				                  window.location.href = "exam_results.php"
				                },200);
				                ///////////////////////////// Audit Trail Start (jeelbert) /////////////////////////////
								var payload24 = {
					                'method':"PostAudit",
					                'acct_id':acct_id,
					                'client_ip':ip_addr,
					                'event_action':"Update",
					                'eventdesc': "Module: Admission / Manual Encoding of Test Result. Description: User Updated Applicant Name :"+$("#studName").val()+"\
					                			  Applicant Exam Result Set IQ-Score to: "+$("#iqScore").val()+" EQ-A to: "+$("#eqA").val()+" EQ-B to: "+$("#eqB").val()+"\
					                			  EQ-C to: "+$("#eqC").val()+" EQ-D to :"+$("#eqD").val()+" EQ-E to: "+$("#eqE").val()
					            }
					              console.log(payload24);
					              $.main.executeExternalPost(host+"/api2/wsv1/api/Audit/",JSON.stringify(payload24)).done(function(result){
					                if(result.status === 'SUCCESS'){
					                  console.log(result);
					                }else{
					                  console.log(result.message);
					                }
					              });		
				                ///////////////////////////// Audit Trail End (jeelbert) ///////////////////////////////
				      		}
				      	})
			      	}
	      		})
	      	})
      	})
    }


    var __validatedata = function(payload){
        var submit = true;
        
        if(payload.iq_score == null || payload.iq_score == ""){
          	submit = false;
    		$("#iqScore").closest("div").addClass("has-error");
    		$("#iqError").removeClass("hide");
        }else{
    		$("#iqScore").closest("div").removeClass("has-error");
    		$("#iqError").addClass("hide");
        }
        
        if(payload.eqA == null || payload.eqA == ""){
          	submit = false;
    		$("#eqA").closest("div").addClass("has-error");
    		$("#eqAerror").removeClass("hide");
        }else{
    		$("#eqA").closest("div").removeClass("has-error");
    		$("#eqAerror").addClass("hide");
        }
        
        if(payload.eqB == null || payload.eqB == ""){
          	submit = false;
    		$("#eqB").closest("div").addClass("has-error");
    		$("#eqBerror").removeClass("hide");
        }else{
    		$("#eqB").closest("div").removeClass("has-error");
    		$("#eqBerror").addClass("hide");
        }
        
        if(payload.eqC == null || payload.eqC == ""){
          	submit = false;
    		$("#eqC").closest("div").addClass("has-error");
    		$("#eqCerror").removeClass("hide");
        }else{
    		$("#eqC").closest("div").removeClass("has-error");
    		$("#eqCerror").addClass("hide");
        }
        
        if(payload.eqD == null || payload.eqD == ""){
          	submit = false;
    		$("#eqD").closest("div").addClass("has-error");
    		$("#eqDerror").removeClass("hide");
        }else{
    		$("#eqD").closest("div").removeClass("has-error");
    		$("#eqDerror").addClass("hide");
        }
        
        if(payload.eqE == null || payload.eqE == ""){
          	submit = false;
    		$("#eqE").closest("div").addClass("has-error");
    		$("#eqEerror").removeClass("hide");
        }else{
    		$("#eqE").closest("div").removeClass("has-error");
    		$("#eqEerror").addClass("hide");
        }

        if(submit){
          return true;
        }
        return false;

    };

    return {
        manual_encode : __manual_encode,
    };
}());
