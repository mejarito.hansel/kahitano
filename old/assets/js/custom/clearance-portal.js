$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.clearance = (typeof $.main.clearance !=='undefined')? $.main.clearance : {};

$.main.clearance =(function() {
	var host = window.location.protocol+"//"+window.location.hostname ;
    var __getAllClearance = function() {
      // console.log("test");
      var acct_id = $("#getaccountidforclearance").data('acct');
      $.main.executeExternalPost(host+'/api/index.php/Ams_controller/fetchAllClearance').done(function(result) {
      	if(result.status === "SUCCESS"){
      		console.log(result)
      		
      		if ( $.fn.DataTable.isDataTable('#myTable1') ) {
			        $('#myTable1').DataTable().destroy();
			        $('#myTable1 tbody').empty();
			}
      		var arr = [];

               for (i=0;i<result.payload.length;i++){
                   var obj = {};
                   var item = result.payload[i];
                   var btnView = "<a class='btnView btn btn-success' title='Update Info' data-clearance1='"+item.clearance_ID+"' data-mma='"+item.student_ID+"'>View Details</a>";
                   var btnUpdate = "<a class='btnUpdateTest btn btn-danger' title='Update Info' data-clearance='"+item.clearance_ID+"' data-mma1='"+item.student_ID+"' data-toggle='modal' data-target='#updateSamp'>unblock</a>";
                   obj['Cadet_No'] = item.student_ID;
                   obj['Cadet_Name'] = item.si_LNAME+", "+item.si_FNAME+" "+item.si_MNAME;
                   obj['Department'] = item.department_NAME;
                   obj['ACTION'] = btnView+" "+btnUpdate;
                   arr.push(obj);
               }

               $(document).ready( function() {
                   $('#myTable1').DataTable({
                   	   'scrollX': true,
                   	   "lengthChange": false,
                       'data' : arr,
                       'columns' : [{"data": "Cadet_No"},{"data": "Cadet_Name"},{"data": "Department"},{"data": "ACTION"}]

                   });
               });
               //unblock part
             $("#myTable1").on("click",".btnUpdateTest",function(){

		      	 $("#myModal1").modal();
		    		var clearance_id1 = $(this).data("clearance");
		    		var student_id1 = $(this).data("mma1");
		    		var payload = {
		    			"clearance_ID": clearance_id1
		    		}
		    		console.log(payload);
		    		$.main.executeExternalPost(host+'/api/index.php/Ams_controller/fetchAllClearanceByID', JSON.stringify(payload)).done(function(result){
		    			if(result.status === 'SUCCESS'){
		    				console.log(result);
		    				$("#myModal1").show();
		    				$("#cadet_id").val(student_id1);
		    				$("#cadet_name").val(result.payload[0].si_LNAME+", "+result.payload[0].si_FNAME+" "+result.payload[0].si_MNAME);
		    				$("#cadet_dept").val(result.payload[0].department_NAME);
		    				$("#btnUnblock").on("click", function(){
		    					if($("#cadet_reason").val() == ""){
		    						$("#rsn").removeClass("hidden");
		    					}else{
		    						$("#rsn").addClass("hidden");
		    						$("#nsr").removeClass("hidden");
									var payload = {
				       	 				"account_ID":acct_id,
				       	 				"clearance_ID":clearance_id1,
				       	 				"unblocked_remarks":$("#myModal1").find("#cadet_reason").val()
				       	 			}
				       	 			console.log(payload);
				       	 			$.main.executeExternalPost(host+'/api/index.php/Ams_controller/updateClearance', JSON.stringify(payload)).done(function(result) {
						            	if(result.status === "SUCCESS"){
						            		console.log(result);
						            		location.reload();
						            	}else{
						            		console.log("Error unblocking");
						            	}
						            });
		    					}
		    					
		    				});
		    			}else{
		    				console.log("Sorry");
		    			}
		    		});


		       });

             // view full details part
        $("#myTable1").on("click",".btnView",function(){	
        	var clearance_id = $(this).data("clearance1");
        	var student_id = $(this).data("mma");
        	var payload = {
        		"clearance_ID": clearance_id
        	}
        	
        
        	console.log(clearance_id);
        	console.log(student_id);
        	console.log(payload);
        	$.main.executeExternalPost(host+'/api/index.php/Ams_controller/fetchAllClearanceByID', JSON.stringify(payload)).done(function(result){
        		if(result.status === 'SUCCESS'){
        			console.log(result);
        			$("#myModal2").modal();
		        	$("#myModal2").find(".modal-body").html("<div class='row'>\
		        		<p>Student No: "+student_id+"</p><p>Student Name: "+result.payload[0].si_LNAME+", "+result.payload[0].si_FNAME+" "+result.payload[0].si_MNAME+"</p>\
		        		<p>Semester Blocked "+result.payload[0].sem_NAME+"</p>\
		        		<p>Department: "+result.payload[0].department_NAME+"</p>\
		        		<p>Blocked By: "+result.payload[0].account_LNAME+", "+result.payload[0].account_FNAME+" "+result.payload[0].account_MNAME+"</p>\
		        		<p>Date Blocked: "+result.payload[0].blocked_date+"</p>\
		        	</div>")
        		}else{
        			console.log(result.message);
        		}
        	});
        	
        	
        
     	});
     	//add block
     	$("#asd").click(function(){
     		$("#myModal").modal();
     		
     		function isValidSemYear(){
     			if($("#txtYrNSem").val() == ""){
     				$("#txtYrNSem").closest("div").addClass("has-error");
					return false;
				}else{
					$("#txtYrNSem").closest("div").removeClass("has-error").addClass("has-success");
					return true;
				}
     		};
     		function isValidDepartment(){
				if($("#txtDepartment").val() == ""){
     				$("#txtDepartment").closest("div").addClass("has-error");
					return false;
				}else{
					$("#txtDepartment").closest("div").removeClass("has-error").addClass("has-success");
					return true;
				}
     		};
     		function isValidReason(){
     			if($("#txtReason").val() == ""){
     				$("#txtReason").closest("div").addClass("has-error");
					return false;
				}else{
					$("#txtReason").closest("div").removeClass("has-error").addClass("has-success");
					return true;
				}
     		};
     		function isValidStudent(){
     			if($("#cadetName").val() == ""){
     				$(".select2-selection").css({"border-color":'#b94a48'});
					return false;
				}else{
					$(".select2-selection").css({"border-color":'#468847'});
					return true;
				}
     		};
     		$.main.executeExternalGet(host+'/api/index.php/Ams_controller/fetchAllSem').done(function(result) {
     			$("#myModal").find("#txtYrNSem").html("");
		        if(result.status === "SUCCESS"){
		        	$("#txtYrNSem").append("<option value=''selected>-- Select Year & Semester --</option>");
		            for(i=0;i<result.payload.length;i++){
		            	$("#myModal").find("#txtYrNSem").append("<option value='"+result.payload[i].sem_ID+"'>"+result.payload[i].sem_NAME+"</option>")
		            }
		        }
		    });
		    $.main.executeExternalGet(host+'/api/index.php/Ams_controller/fetchAllDepartmentList').done(function(result) {
		       $("#myModal").find("#txtDepartment").html("");
		        if(result.status === "SUCCESS"){
		        	$("#txtDepartment").append("<option value='' selected>-- Select Department --</option>");
		            for(i=0;i<result.payload.length;i++){
		            	$("#myModal").find("#txtDepartment").append("<option value='"+result.payload[i].department_ID+"'>"+result.payload[i].department_NAME+"</option>")
		            }
		        }
		    });
     		$(document).ready(function(){
			   $('#searchCadetID').on("cut copy paste",function(e) {
			      e.preventDefault();
			   });
			});
			 // $(".select2-select").select2({
		  //       maximumSelectionLength: 1
		  //   });	
		   $("#cadetName").select2({
            placeholder: "Please select a Student"
        	});	
			var API = host+"/api2/wsv1/api/dorm/";
			let payload = {
	            "method": "getallenrolledandnodormassigned"
	      	}
			$.main.executeExternalPost(API, JSON.stringify(payload)).done(function(result) {
		      
		        if(result.status === "SUCCESS"){
		            for(i=0;i<result.payload.length;i++){
		            	$("#cadetName").append("<option value='' selected>-- Select Department --</option>");
		            	$("#myModal").find("#cadetName").append("<option value='"+result.payload[i].si_ID+"'>"+result.payload[i].Student_No+" "+result.payload[i].NAME+"</option>")
		            }
		       
				        console.log(result);
				    }else{
				    	console.log(result.message);
				    }
		    });

     		// $("#myModal").on("keypress keyup", "#searchCadetID", function(){
     		// 	var id = $("#searchCadetID").val().length;
     		// 	$("#spin").removeClass("hidden");
     		// 	if(id > 4){
     		// 		$("#123").text("");
	     	// 		var searchValue = $("#searchCadetID").val()
	     	// 		var payload = {
	     	// 			"student_ID" : searchValue.replace(/\s/g, '')

	     	// 		}
	     	// 		console.log(payload)
	     	// 		$.main.executeExternalPost('http://sms.massiveits.com/api/index.php/Ams_controller/fetchStudentInfoByID', JSON.stringify(payload)).done(function(result){
	     	// 			if(result.status === 'SUCCESS'){

	     	// 				console.log(result);
	     	// 				$("#spin").addClass("hidden");
	     	// 				$("#123").text(""+result.payload.si_LNAME+", "+result.payload.si_FNAME+" "+result.payload.si_MNAME+"").css("color","green");
	     	// 				$("#123").show();
	     	// 				$("#btnBlockCadet").unbind().bind("click", function(){

     		// 					if(isValidSemYear() && isValidDepartment() && isValidReason()  === true){
     		// 						var payload = {
				   //          			"account_ID": acct_id,
							// 			"si_ID": result.payload.si_ID,
							// 			"department_ID" :$("#txtDepartment option:selected").val(),
							// 			"sem_ID": $("#txtYrNSem option:selected").val(),
							// 			"blocked_remakrs": $("#txtReason").val() 
				   //          		}
				   //          		console.log(payload);
					  //           		$.main.executeExternalPost('http://sms.massiveits.com/api/index.php/Ams_controller/createClearance', JSON.stringify(payload)).done(function(result) {
					  //           			if(result.status === "SUCCESS"){
					  //           				console.log(result);
					  //           				$("#banned").removeClass("hidden");
					  //           				location.reload();
					  //           			}else{
					  //           				console.log("Error Blocking");
					  //           			}
					  //           		});
     		// 					}else{
     		// 						console.log("ERROR");
     		// 					}
     							
				            	
	     	// 				});
	     	// 			}else{
	     	// 				$("#spin").addClass("hidden");
	     	// 				$("#123").text("User does not exist").css("color","red");
	     	// 				$("#123").show();
	     	// 				payload = ""

	     	// 			}
	     	// 		});
     		// 	}else if(id < 1){
     		// 		$("#spin").addClass("hidden");
     		// 		$("#123").text("");
     		// 	}else{
     		// 	}
     			
     		// });
     		
     		// $("#btnBlockCadet").on("click",function(){
     		// 	$("#fillId").removeClass("hidden");
     		// });
     		// $("#searchCadetID").on("click",function(){
     		// 	$("#fillId").addClass("hidden");
     		// });
     		$("#btnBlockCadet").on("click",function(){
     			isValidSemYear();
				isValidDepartment();
				isValidReason();
				isValidStudent();

				if(isValidStudent() === true && isValidSemYear() === true && isValidDepartment() === true && isValidReason() === true ){
					var payload = {
            			"account_ID": acct_id,
						"si_ID": $("#cadetName").val(),
						"department_ID" :$("#txtDepartment option:selected").val(),
						"sem_ID": $("#txtYrNSem option:selected").val(),
						"blocked_remakrs": $("#txtReason").val() 
            		}

            		console.log(payload);
            		$.main.executeExternalPost(host+'/api/index.php/Ams_controller/createClearance', JSON.stringify(payload)).done(function(result) {
            			if(result.status === "SUCCESS"){
            				console.log(result);
            				$("#banned").removeClass("hidden");
            				location.reload();
            			}else{
            				console.log("Error Blocking");
            			}
            		});
				}else{
					console.log('tangina ');
					$("#fillId").removeClass('hidden');
				}
     		});
     	});
		       
       }else{
           console.log("ERROR");
      }
      });

    }
    return {
        getAllClearance : __getAllClearance
    };
}());