$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.Updateinfo = (typeof $.main.Updateinfo !=='undefined')? $.main.Updateinfo : {};

$.main.Updateinfo = (function(){
	var host = window.location.protocol+"//"+window.location.hostname ;
	var path = host+"/api/index.php/Ams_controller/";
	// var path = "http://20.20.25.41/student-portal-backend/index.php/Ams_controller/"
	$(".alert-usermanagement").hide();

	var __Updateinfo = function() {
        // console.log("attached events in update info");

        $("#saveBtn").unbind("click").on("click", function(){
            // console.log("clicked");
            var pageURL = $(location). attr("href");
            var id = pageURL.substring(pageURL.lastIndexOf('/') + 44);
            console.log(id);
            var payload = {
                "si_ID" : id,
                "RAAI_ID" : id
            }
            
			$.main.executeExternalPost(path+"upsertRequestApproval", JSON.stringify(payload)).done(function(result){
                console.log(result)
                if(result.status == "SUCCESS"){
                    $("#success").fadeIn("slow")
                    setTimeout(function(){
                        window.location.href = "student_information.php?action=view&id="+id;
                    },1000);
                }

            })
        })

  	}
	return {
		Updateinfo: 	__Updateinfo
	};

}());
