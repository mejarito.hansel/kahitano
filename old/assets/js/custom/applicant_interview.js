

$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.interview = (typeof $.main.interview !=='undefined')? $.main.interview : {};

$.main.interview =(function() {
	

    // var path = "http://20.20.25.41/student-portal-backend/index.php/Magsaysay_controller/"
  //  var path = "http://sms.massiveits.com/api/index.php/Magsaysay_controller/"

	 var host = window.location.protocol+"//"+window.location.hostname ;
     var path = host+"/api/index.php/Magsaysay_controller/"

    var getApplicantList = path + "getApplicantList"

	var __applicantInterview = function() {
		console.log("attached events")
	  $.main.executeExternalPost(getApplicantList).done(function(result) {
	    // console.log(result);
	    var items = result.payload;
	    console.log(items)
	    var data = [];

	       for(i=0; i<items.length; i++){

	         var trc = $('<tr class='+result.payload[i].application_status+'>');

	       trc.append($('<td class="row1">')
	          .append(result.payload[i].applicant_fname));
	       trc.append($('<td class="row2">')
	          .append(result.payload[i].applicant_lname));
	       trc.append($('<td class="row3">')
	          .append(result.payload[i].applicant_email));
	       trc.append($('<td class="row4">')
	          .append(result.payload[i].applicant_contact));
	       trc.append($('<td class="row5">')
	          .append(result.payload[i].applicant_number));
	       trc.append($('<td class="row6">')
	          .append(result.payload[i].application_interview_sched));

	       var status = result.payload[i].application_status;

	         var ifFetchBtn;
	            if(status === "7"){
	              ifFetchBtn = "hide";
	            }
	            else{
	              ifFetchBtn = "";
	            }
	       trc.append($('<td class="row7">')
	          .append($('<button class="btn btn-danger btn-xs P2C7_MARKSCEDULE updateBtn '+ifFetchBtn+'" id="updateBtn" '
	              + ' data-aid="'+ result.payload[i].application_id +'" >Mark Schedule</button>')));


	       $('#tbody').append(trc);
	       $('#tbody').find('tr.7').remove();
	      }

	      var dataRow = $("#myTable").DataTable(
	      {
	        // 'dom' : 'Brftip',
	        'destroy': true,
	        fnDrawCallback : function()
	        {
	          $('.updateBtn').unbind('click').on('click',function(){
	  
	              $("#icpID").addClass("hide");
	              $("#success").addClass("hide");
	              $("#requiredfield").addClass("hide");
	              $("#passwordmatch").addClass("hide");
	              $("#newPass").val("");
	              $("#retypePass").val("");

	            rowID = $(this).parent().parent()
	            // console.log(rowID)
	            
	            $("#myModal").modal();
	            aid = $(this).data('aid')
	            console.log('ID ' + '= ' + aid)
	            // console.log('ID ' + '= ' + aid)
	            // $.cookie("applicant_id", aid);

	          
	            $('#saveBtn').unbind('click').on('click',function(){
	              console.log("clicked")
	              var payload = {
	                  "application_id" : aid,
	                  "application_interview_sched" : $("#datetimepicker4").val(),
	                  // "application_date_released" : $("#timeSched").val(),
	              }
	              console.log(payload);
	                $.main.executeExternalPost(path+"updateInterviewSchedule", JSON.stringify(payload)).done(function(result) {
	                  console.log(result);
	                  var status = result.status;

	                  if(status == "SUCCESS") {
	                      // console.log("complete");
	                      $("#success").removeClass("hide");
	                      setTimeout(function() {
	                      window.location.href = "initial_interview_schedule.php"
	                      },200);

	                  }else {
	                      // console.log("Failed");
	                      $("#submitCC").removeClass("hide");
	                      $("#preloader").addClass("hide");
	                      $("#icpID").removeClass("hide");
	                      $("#submitAdminPass").removeClass("hide");
	                  }
	                })
	            })
	          })
	        },
	        "scrollY": true,
	        "bFilter": false, 
	        "bLengthChange": true,
	        'scrollX' : true,
	         'buttons': [
	               {  
	                extend: 'excelHtml5',
	                title: "Initial Interview Schedule",download: 'open',
	                exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
	               // title: "Transaction Report " + $("#start").val(),download: 'open'
	                 /* exportOptions: {
	                      columns: [ 0, 1 ]
	                  }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
	              },{
	              
	                extend:  'pdfHtml5',
	                title: "Initial Interview Schedule",download: 'open',
	                exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
	              },
	              {   
	                extend: 'csvHtml5',
	                title: "Initial Interview Schedule",download: 'open',
	                exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
	              },
	             ]
	      });

	      $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
	      $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
	      $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
	      $(".dt-buttons").addClass("hidden")
	    
	  })
	}

	var __applicantInterviewResult = function () {
		console.log("test")

	  $.main.executeExternalPost(getApplicantList).done(function(result) {
	    console.log(result);
	    
	    var items = result.payload;
	    console.log(items)
	    var data = [];

	       	for(i=0; i<items.length; i++){

	         var trc = $('<tr class='+result.payload[i].application_status+'>');

	       	trc.append($('<td class="row1">')
	          .append(result.payload[i].applicant_fname + " " + result.payload[i].applicant_lname));
	       	trc.append($('<td class="row2">')
	          .append(result.payload[i].applicant_number));
            
            var status = result.payload[i].application_interview_result
            
            var step = ""
              if(status ==='1'){
                // step = "Registered"
                step = "Passed"
              }else if(status === '2'){
                step = "Not Qualified"
              }
						var status = result.payload[i].application_status;

	         	var ifFetchBtn;
	            if(status === "7"){
	              ifFetchBtn = "7";
	            }
	            else{
	              ifFetchBtn = "";
	            }
           trc.append($('<td class="row3">')
             .append(step));
           trc.append($('<td class="row4">')
             .append($('<button class="btn btn-danger btn-xs P2C8_CHANGE updateBtn '+ifFetchBtn+'" id="updateBtn" '
                  + ' data-aid="'+ result.payload[i].application_id +'" >Change</button>')));
                  // + ' data-uid="'+ result.payload[i].applicant_id +'" >Reset Exam</button>')))

	       $('#tbody').append(trc);
	       $('#tbody').find('tr.7').remove();
	      }

	      var dataRow = $("#myTable").DataTable(
	      {
	        // 'dom' : 'Brftip',
	        'destroy': true,
            fnDrawCallback : function()
            {
              $('.updateBtn').unbind('click').on('click',function(){
                    
                  $("#icpID").addClass("hide");
                  $("#success").addClass("hide");
                  $("#requiredfield").addClass("hide");
                  $("#passwordmatch").addClass("hide");
                  $("#newPass").val("");
                  $("#retypePass").val("");

                rowID = $(this).parent().parent()
                // console.log(rowID)
                
                $("#myModal").modal();
                aid = $(this).data('aid')
                console.log('ID ' + '= ' + aid)
                // console.log('ID ' + '= ' + aid)
                // $.cookie("applicant_id", aid);

                $('#saveBtn').unbind('click').on('click',function(){
                  console.log("clicked")
                  var payload = {
                      "application_id" : aid,
                      "application_interview_result" : $("#statusID").val(),
                  }
                  console.log(payload);
                    $.main.executeExternalPost(path+"updateInterviewResult", JSON.stringify(payload)).done(function(result) {
                      console.log(result);
                      var status = result.status;
                      // console.log(result.workgroup_id);

                      if(status == "SUCCESS") {
                          // console.log("complete");
                          $("#success").removeClass("hide");
                          setTimeout(function() {
                          window.location.href = "initial_interview_results.php"
                          },200);

                      }else {
                          // console.log("Failed");
                          $("#submitCC").removeClass("hide");
                          $("#preloader").addClass("hide");
                          $("#icpID").removeClass("hide");
                          $("#submitAdminPass").removeClass("hide");
                      }
                    })
                })
              })
            },
	        "scrollY": true,
	        "bFilter": false, 
	        "bLengthChange": true,
	        'scrollX' : true,
	         'buttons': [
	               {  
	                extend: 'excelHtml5',
	                title: "Initial Interview Results",download: 'open',
	                exportOptions: {columns: [ 0, 1,2] }
					//	                exportOptions: {columns: [ 0, 1,2,3,4,5 ] }

		               // title: "Transaction Report " + $("#start").val(),download: 'open'
	                 /* exportOptions: {
	                      columns: [ 0, 1 ]
	                  }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
	              },{
	              
	                extend:  'pdfHtml5',
	                title: "Initial Interview Results",download: 'open',
	                exportOptions: {columns: [ 0, 1,2 ] }
	                //	                exportOptions: {columns: [ 0, 1,2,3,4,5 ] }

	              },
	              {   
	                extend: 'csvHtml5',
	                title: "Initial Interview Results",download: 'open',
	                exportOptions: {columns: [ 0, 1,2 ] }
	                //	                exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
	              },
	             ]
	      });

	      $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
	      $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
	      $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
	      $(".dt-buttons").addClass("hidden")
	    
	  })
	}	

    return {
        applicantInterview : __applicantInterview,
        applicantInterviewResult : __applicantInterviewResult
    };

}());
