/* 
 * This the main API js of main 
 *  Portal web services.
 */



$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !== 'undefined') ? $.main : {};

$.main = (function() {

    var ___ctx = '';
   
    var __setContext = function(newctx) {
        ___ctx = newctx;
    };
    
    var __getContext = function() {
        return ___ctx;
    };

    var __getImgPath = function(){
        //return "/main-portal/";
        return "/";
    }
	var __executeGet = function (path) {
        var dfd = $.Deferred();
        $.get(path, function(data) {})
        .done(function(data){
            dfd.resolve(data);
        })
        .fail(function(qXHR, textStatus, errorThrown){
            dfd.resolve({
                status : 'ERROR',
                message : errorThrown
            });
        })
        .always(function(data){

        });
        return dfd.promise();
    };
    var __executePost = function(path, jsonObj, customLoader) {
        path = $.main.getContextPath() + path;
        var d = $.Deferred();
        if(customLoader != ""){
            $("#"+customLoader).show();
        }
        $.ajax({
            method: "POST",
            url: path,
            dataType: "json",
            data: jsonObj
        }).done(function (data, textStatus, jqXHR) {
            if(customLoader != ""){
                $("#"+customLoader).hide();
            }
            d.resolve(data)
        }).fail(function (jqXHR, textStatus, errorThrown,request) {
            console.log('---FAILED---');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            console.log('---FAILED---');
            
            d.resolve({
                status : 'ERROR',
                message : request
            });
            
            if(customLoader != ""){
                $("#"+customLoader).hide();
            }
        });
        
        return d.promise();
    };

    var __executeExternalPost = function(path, jsonObj, customLoader) {

        // path = $.main.getContextPath() + path;
        // console.log(path);
        var d = $.Deferred();
        if(customLoader != ""){
            $("#"+customLoader).show();
            $("#"+customLoader).removeClass("hide");
        } 

        $.ajax({
            method: "POST",
            url: path,
            dataType: "json",
            data: jsonObj
        }).done(function (data, textStatus, jqXHR) {
            if(customLoader != ""){
                $("#"+customLoader).hide();
                $("#"+customLoader).addClass("hide");
            }
        // console.log(path);

            d.resolve(data)
        }).fail(function (jqXHR, textStatus, errorThrown,request) {
            console.log('---FAILED---');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            console.log('---FAILED---');
            
            d.resolve({
                status : 'ERROR',
                message : request
            });
            
            if(customLoader != ""){
                $("#"+customLoader).hide();
                $("#"+customLoader).addClass("hide");
            }
        });
        
        return d.promise();
    };

    var __executeExternalGet = function(path, customLoader) {
        // path = $.main.getContextPath() + path;
        var d = $.Deferred();
        if(customLoader != ""){
            $("#"+customLoader).show();
            $("#"+customLoader).removeClass("hide");
        }
        $.ajax({
            method: "GET",
            url: path,
            dataType: "json",
        }).done(function (data, textStatus, jqXHR) {
            if(customLoader != ""){
                $("#"+customLoader).hide();
                $("#"+customLoader).addClass("hide");
            }
            d.resolve(data)
        }).fail(function (jqXHR, textStatus, errorThrown,request) {
            console.log('---FAILED---');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            console.log('---FAILED---');
            
            d.resolve({
                status : 'ERROR',
                message : request
            });
            
            if(customLoader != ""){
                $("#"+customLoader).hide();
                $("#"+customLoader).addClass("hide");
            }
        });
        
        return d.promise();
    };



    var __executeFile = function(path, jsonObj) {
        var d = $.Deferred();
            $(".overlay-back").show();
            $(".loadDiv").show();
        $.ajax({
            method: "POST",
            url: path,
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            /*data: JSON.stringify(jsonObj)*/
            data: jsonObj
        }).done(function (data, textStatus, jqXHR) {
            d.resolve(data);
            $(".loadDiv").hide();
            $(".overlay-back").hide();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log('---FAILED---');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            console.log('---FAILED---');
            
            d.resolve({
                status : 'ERROR',
                message : errorThrown
            });
            $(".overlay-back").hide();
            $(".loadDiv").hide();
        });
        return d.promise();
    };
    
    var __CheckCookie = function() {
        if (typeof  $.cookie('userID') != undefined){
            //Redirect to logout page
        }
    }

    var __showStatus = function(status){
        var retval = "";
        switch(status){
            case '-1' : retval = 'Pending'; break;
            case '1' : retval = 'Ongoing'; break;
            case '0' : retval = 'Inactive'; break;
            case '2' : retval = 'Completed'; break;
            case '3' : retval = 'Pending'; break;
        }
        return retval;
    }
    
    return {
        setContext:__setContext,
        getContext : __getContext,
        getImgPath : __getImgPath,
        executePost : __executePost,
        executeExternalPost : __executeExternalPost,
        executeGet : __executeGet,
        executeFile : __executeFile,
        executeExternalGet : __executeExternalGet,
        CheckCookie :__CheckCookie,
        showStatus : __showStatus
    };
}());
