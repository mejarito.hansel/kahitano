

$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.library = (typeof $.main.library !=='undefined')? $.main.library : {};

$.main.library =(function() {
    var host = window.location.protocol+"//"+window.location.hostname ;
    var path = host+"/api2/wsv1/ApiLibrary/"
    // var path = "http://locationcalhost/magsaysay_library_ws/index.php/wsv1/api/"
    var __fetchallBook = function() {
      // console.log("attached events to library");

        $('#addbook').unbind('click').on('click',function(){
          console.log("click")
            $("#myModaladd").modal();
        });

        $('#saveBtn').unbind('click').on('click',function(){
          console.log("clicked")
          if(validateAll() === true){
            var payload = {
              "ACCESSION_NO" : $("#accessionID").val(),
              "DATE_ACCESSION" : $("#datetimepicker").val(),
              "MATERIAL_TITLE" : $("#titleID").val(),
              "AUTHOR_NAME" : $("#authorID").val(),
              "MATERIAL_COPYRIGHT" : $("#copyrightID").val(),
              "MATERIAL_SOURCE" : $("#sourceID").val(),
              "MATERIAL_DATE_RECEIVED" : $("#datetimepicker4").val(),
              "BOOK_TYPE" : $("#booktypeID").val(),
              "CALL_NO" : $("#callID").val(),
              "BOOK_STATUS": $("#bookStatus option:selected").val(),
              "BOOK_INDEX": $("#bookIndex").val()
            }
            $.main.executeExternalPost(path + "insertMaterials", JSON.stringify(payload)).done(function(result) {
              var status = result.status;
              if(status == "SUCCESS") {
                $("#success").removeClass("hide");
                setTimeout(function() {
                  window.location.href = "library.php"
                },200);
              }else {
                $("#submitCC").removeClass("hide");
                $("#preloader").addClass("hide");
                $("#icpID").removeClass("hide");
                $("#submitAdminPass").removeClass("hide");
              }
            });
          }else{
            alert("Please fill out this form completely");
          }
        });
        function datetimepicker() {
          var datetimepicker = $("#datetimepicker").val();
          if(datetimepicker == ""){
            return false;
          }else{
            return true;
          }
        };

        function accessionID() {
          var accessionID = $("#accessionID").val();
          if(accessionID == ""){
            return false;
          }else{
            return true;
          }
        };

        function callID() {
          var callID = $("#callID").val();
          if(callID == ""){
            return false;
          }else{
            return true;
          }
        };

        function titleID() {
          var titleID = $("#titleID").val();
          if(titleID == ""){
            return false;
          }else{
            return true;
          }
        };

        function booktypeID() {
          var booktypeID = $("#booktypeID").val();
          if(booktypeID == ""){
            return false;
          }else{
            return true;
          }
        };

        function authorID() {
          var authorID = $("#authorID").val();
          if(authorID == ""){
            return false;
          }else{
            return true;
          }
        };

        function copyrightID() {
          var copyrightID = $("#copyrightID").val();
          if(copyrightID == ""){
            return false;
          }else{
            return true;
          }
        };

        function sourceID() {
          var sourceID = $("#sourceID").val();
          if(sourceID == ""){
            return false;
          }else{
            return true;
          }
        };

        function datetimepicker4() {
          var datetimepicker4 = $("#datetimepicker4").val();
          if(datetimepicker4 == ""){
            return false;
          }else{
            return true;
          }
        };

        function validateAll() {
          if(datetimepicker() && accessionID() && callID() && titleID() && booktypeID() && authorID() && copyrightID() && sourceID() && datetimepicker4()){
            return true;
          }else{
            return false;
          }
        }

      $.main.executeExternalPost(path +'fetchAllMaterials').done(function (result) {
        console.log(result);

        var items = result.payload;
        console.log(items)
        var data = [];

           for(i=0; i<items.length; i++){ 
             var trc = $('<tr>');

           trc.append($('<td class="row1">')
             .append(result.payload[i].DATE_ACCESSION));
           trc.append($('<td class="row2">')
             .append(result.payload[i].ACCESSION_NO));
           trc.append($('<td class="row3">')
             .append(result.payload[i].CALL_NO));
           trc.append($('<td class="row4">')
             .append(result.payload[i].MATERIAL_TITLE));
           trc.append($('<td class="row5">')
             .append(result.payload[i].BOOK_TYPE));
           trc.append($('<td class="row6">')
             .append(result.payload[i].AUTHOR_NAME));  
           trc.append($('<td class="row7">')
             .append(result.payload[i].MATERIAL_COPYRIGHT));
           trc.append($('<td class="row8">')
             .append(result.payload[i].MATERIAL_SOURCE));
           // var status = result.payload[i].BOOK_STATUS_ID
           // var stats = ''
           // if (status == 1) {
           //  stats = 'Borrowed'
           // }else if (status == 2) {
           //  stats = 'Returned'
           // }
           if(result.payload[i].BOOK_STATUS === "1"){
              var BookStatus = "Lost"
           }else if(result.payload[i].BOOK_STATUS === "2"){
              var BookStatus = "Available"
           }else{
              var BookStatus = ""
           }

           trc.append($('<td class="row9">')
             .append(result.payload[i].MATERIAL_DATE_RECEIVED));
           trc.append($('<td class="row10">')
             .append(BookStatus));
           trc.append($('<td class="row11">')
             .append(result.payload[i].BOOK_INDEX));
           trc.append($('<td class="row12">')
             .append($('<button class="btn btn-danger btn-xs update P7C1_UPDATEBOOK" '
                  + ' data-uid="'+ result.payload[i].MATERIAL_ID +' " '
                  + ' data-ac="'+ result.payload[i].DATE_ACCESSION +' "'
                  + ' data-an="'+ result.payload[i].ACCESSION_NO +' "'
                  + ' data-cn="'+ result.payload[i].CALL_NO +' "'
                  + ' data-mt="'+ result.payload[i].MATERIAL_TITLE +' "'
                  + ' data-atn="'+ result.payload[i].AUTHOR_NAME +'"'
                  + ' data-mc="'+ result.payload[i].MATERIAL_COPYRIGHT +' "'
                  + ' data-ms="'+ result.payload[i].MATERIAL_SOURCE +' "'
                  + ' data-mdr="'+ result.payload[i].MATERIAL_DATE_RECEIVED +'"'
                  + ' data-bs="'+ result.payload[i].BOOK_STATUS +'"'
                  + ' data-bi="'+ result.payload[i].BOOK_INDEX +'"'
                  + ' data-bt="'+ result.payload[i].BOOK_TYPE +' " >Update Book</button>')))

           $('#tbody').append(trc);

          }

          var dataRow = $("#myTable").DataTable(
          {
            'dom' : 'Brftip',
            'destroy': true,
            "scrollY": true,
            "bFilter": false, 
            "bLengthChange": false,
            'scrollX' : true,
             'buttons': [
                   {  
                    extend: 'excelHtml5',
                    title: "Materials",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6,7,8 ] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
                  
                    extend:  'pdfHtml5',
                    title: "Materials",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6,7,8 ] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Materials",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6,7,8 ] }
                  },
                 ]
          });

          $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
          $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
          $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
          $(".dt-buttons").addClass("hidden")


        // $('#addbook').unbind('click').on('click',function(){
        //     // rowID = $(this).parent().parent()
        //     // console.log(rowID)
            
        //     $("#myModal").modal();
        //     materialid = $(this).data('uid')
        //     console.log('material_id ' + '= ' + materialid)
        // });

        


        $('.update').unbind('click').on('click',function(){
            $("#myModalupdate").modal();
            MATERIAL_ID = $(this).data('uid')
            console.log('MATERIAL_ID ' + '= ' + MATERIAL_ID)
            DATE_ACCESSION = $(this).data('ac')
            console.log('DATE_ACCESSION ' + '= ' + DATE_ACCESSION)
            ACCESSION_NO = $(this).data('an')
            console.log('ACCESSION_NO ' + '= ' + ACCESSION_NO)
            CALL_NO = $(this).data('cn')
            console.log('CALL_NO ' + '= ' + CALL_NO)
            MATERIAL_TITLE = $(this).data('mt')
            console.log('MATERIAL_TITLE ' + '= ' + MATERIAL_TITLE)
            AUTHOR_NAME = $(this).data('atn')
            console.log('AUTHOR_NAME ' + '= ' + AUTHOR_NAME)
            MATERIAL_COPYRIGHT = $(this).data('mc')
            console.log('MATERIAL_COPYRIGHT ' + '= ' + MATERIAL_COPYRIGHT)
            MATERIAL_SOURCE = $(this).data('ms')
            console.log('MATERIAL_SOURCE ' + '= ' + MATERIAL_SOURCE)
            MATERIAL_DATE_RECEIVED = $(this).data('mdr')
            console.log('MATERIAL_DATE_RECEIVED ' + '= ' + MATERIAL_DATE_RECEIVED)
            BOOK_TYPE = $(this).data('bt')
            console.log('BOOK_TYPE ' + '= ' + BOOK_TYPE)
            BOOK_STATUS = $(this).data('bs')
            console.log('BOOK_STATUS ' + '= ' + BOOK_STATUS)
            BOOK_INDEX = $(this).data('bi')
            console.log('BOOK_INDEX ' + '= ' + BOOK_INDEX)

            $("#Udatetimepicker").val(DATE_ACCESSION);
            $("#UcallID").val(CALL_NO);
            $("#UaccessionID").val(ACCESSION_NO);
            $("#UtitleID").val(MATERIAL_TITLE);
            $("#UauthorID").val(AUTHOR_NAME);
            $("#UcopyrightID").val(MATERIAL_COPYRIGHT);
            $("#UsourceID").val(MATERIAL_SOURCE);
            $("#Udatetimepicker41").val(MATERIAL_DATE_RECEIVED);
            $("#UbooktypeID").val(BOOK_TYPE);
            $("#UbookStatus").val(BOOK_STATUS);
            $("#UbookIndex").val(BOOK_INDEX);

        });

        $('#updatebtn').unbind('click').on('click',function(){

          var payload = {
            "MATERIAL_ID" : MATERIAL_ID,
            "ACCESSION_NO" : $("#UaccessionID").val(),
            "DATE_ACCESSION" : $("#Udatetimepicker").val(),
            "MATERIAL_TITLE" : $("#UtitleID").val(),
            "AUTHOR_NAME" : $("#UauthorID").val(),
            "MATERIAL_COPYRIGHT" : $("#UcopyrightID").val(),
            "MATERIAL_SOURCE" : $("#UsourceID").val(),
            "MATERIAL_DATE_RECEIVED" : $("#Udatetimepicker41").val(),
            "BOOK_TYPE" : $("#UbooktypeID").val(),
            "CALL_NO" : $("#UcallID").val(),
            "BOOK_STATUS": $("#UbookStatus option:selected").val(),
            "BOOK_INDEX": $("#UbookIndex").val()
          }

          $.main.executeExternalPost(path + "updateMaterials", JSON.stringify(payload)).done(function(result) {
            console.log(result)
            var status = result.status;
            
            if(status == "SUCCESS") {
                // console.log("complete");
                $("#success").removeClass("hide");
                setTimeout(function() {
                window.location.href = "library.php"
                },200);

            }else {
                // console.log("Failed");
                $("#submitCC").removeClass("hide");
                $("#preloader").addClass("hide");
                $("#icpID").removeClass("hide");
                $("#submitAdminPass").removeClass("hide");
            }

          })
        })
      })
    }
    var __fetchallborrow = function() {
      // console.log("test");
      var payload = {
        "BOOK_STATUS_ID" : "1"
      }
      $.main.executeExternalPost(path + "getallBookByBookStatus",JSON.stringify(payload)).done(function(result) {
        console.log(result);

        var items = result.payload;
        console.log(items)
        var data = [];

           for(i=0; i<items.length; i++){

             var trc = $('<tr>');

           trc.append($('<td class="row1">')
             .append(result.payload[i].RL_ID));
           trc.append($('<td class="row2">')
             .append(result.payload[i].BOOK_STATUS));
           trc.append($('<td class="row3">')
             .append(result.payload[i].MATERIAL_TITLE));
           trc.append($('<td class="row4">')
             .append(result.payload[i].ACCESSION_NO));
           trc.append($('<td class="row5">')
             .append(result.payload[i].B_DATE_BORROWED));
           trc.append($('<td class="row6">')
             .append(result.payload[i].B_DUE_DATE));
           trc.append($('<td class="row7">')
             .append(result.payload[i].B_FINES));
           trc.append($('<td class="row8">')
             .append($('<button class="btn btn-danger btn-xs updateBtn" id="updateBtn" '
                  + ' data-aid="'+ result.payload[i].B_ID +'" '
                  + ' data-bs="'+ result.payload[i].BOOK_STATUS +'"'
                  + ' data-bdb="'+ result.payload[i].B_DATE_BORROWED +'"'
                  + ' data-bdr="'+ result.payload[i].B_DUE_DATE +'"'
                  + ' data-bsid="'+ result.payload[i].BOOK_STATUS_ID +'"'
                  + ' data-bf="'+ result.payload[i].B_FINES +'"'
                  + ' data-rl_id="'+ result.payload[i].RL_ID +'" >Update Request</button>')));


           $('#tbody').append(trc);

          }

          var dataRow = $("#myTable").DataTable(
          {
            'dom' : 'Brftip',
            'destroy': true,
            "scrollY": true,
            "bFilter": false, 
            "bLengthChange": false,
            'scrollX' : true,
             'buttons': [
                   {  
                    extend: 'excelHtml5',
                    title: "Request",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
                  
                    extend:  'pdfHtml5',
                    title: "Request",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Request",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6] }
                  },
                 ]
          });

          $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
          $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
          $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
          $(".dt-buttons").addClass("hidden")

        $('.updateBtn').unbind('click').on('click',function(){
          $("#myModal2").modal();
          // rowID = $(this).parent().parent()
          // console.log(rowID)

          $("#icpID").addClass("hide");
          $("#success").addClass("hide");
          $("#requiredfield").addClass("hide");
          $("#passwordmatch").addClass("hide");
          $("#newPass").val("");
          $("#retypePass").val("");
          
          aid = $(this).data('aid')
          bsid = $(this).data('bs')
          bdb = $(this).data('bdb')
          bdr = $(this).data('bdr')
          bf = $(this).data('bf')
          bsid = $(this).data('bsid')
          rl_id = $(this).data('rl_id')
          bs = $(this).data('bs')
          // console.log('aid ' + '= ' + aid)
          // console.log('bs ' + '= ' + bs)
          // console.log('bdb ' + '= ' + bdb)
          // console.log('bdr ' + '= ' + bdr)
          // console.log('bf ' + '= ' + bf)
          // console.log('bsid ' + '= ' + bsid)
          // console.log('rl_id ' + '= ' + rl_id)

          $("#RborrowerID").val(rl_id);
          $("#Rdatetimepicker").val(bdb);
          $("#Rdatetimepicker4").val(bdr);
          $("#statusID").val(bsid);
        
          $('#RsaveBtn').unbind('click').on('click',function(){
              console.log("test")

            var payload = {
              "B_ID" : aid,
              "B_DATE_BORROWED" : $("#Rdatetimepicker").val(),
              "B_DUE_DATE" : $("#Rdatetimepicker4").val(),
              "B_FINES" : bf,
              "BOOK_STATUS_ID"  : $("#statusID").val()
            }
              console.log(payload)

            $.main.executeExternalPost(path + "updateBorrow_returned", JSON.stringify(payload)).done(function(result) {
              console.log(result)
              var status = result.status;
              
              if(status == "SUCCESS") {
                  $("#success").removeClass("hide");
                  setTimeout(function() {
                  window.location.href = "borrow.php"
                  },200);

              }else {
                  $("#submitCC").removeClass("hide");
                  $("#preloader").addClass("hide");
                  $("#icpID").removeClass("hide");
                  $("#submitAdminPass").removeClass("hide");
              }

            })
          })
        })
      })
    }
    var __fetchallrequest = function() {
      // console.log("test");
      $.main.executeExternalPost(path + "fetchallrequest").done(function(result) {
        console.log(result);

        var items = result.payload;
        console.log(items)
        var data = [];

           for(i=0; i<items.length; i++){

             var trc = $('<tr>');

           trc.append($('<td class="row1">')
             .append(result.payload[i].RL_ID));
           trc.append($('<td class="row2">')
             .append(result.payload[i].BOOK_STATUS));
           trc.append($('<td class="row3">')
             .append(result.payload[i].MATERIAL_TITLE));
           trc.append($('<td class="row4">')
             .append(result.payload[i].ACCESSION_NO));
           trc.append($('<td class="row5">')
             .append(result.payload[i].B_DATE_BORROWED));
           trc.append($('<td class="row6">')
             .append(result.payload[i].B_DUE_DATE));
           trc.append($('<td class="row7">')
             .append(result.payload[i].B_FINES));
           trc.append($('<td class="row8">')
             .append($('<button class="btn btn-danger btn-xs updateBtn" id="updateBtn" '
                  + ' data-aid="'+ result.payload[i].B_ID +'" '
                  + ' data-bs="'+ result.payload[i].BOOK_STATUS +'"'
                  + ' data-bdb="'+ result.payload[i].B_DATE_BORROWED +'"'
                  + ' data-bdr="'+ result.payload[i].B_DUE_DATE +'"'
                  + ' data-bsid="'+ result.payload[i].BOOK_STATUS_ID +'"'
                  + ' data-bf="'+ result.payload[i].B_FINES +'"'
                  + ' data-rl_id="'+ result.payload[i].RL_ID +'" >Update Request</button>')));


           $('#tbody').append(trc);

          }

          var dataRow = $("#myTable").DataTable(
          {
            'dom' : 'Brftip',
            'destroy': true,
            fnDrawCallback : function()
            {
              $('.updateBtn').unbind('click').on('click',function(){
                $("#myModal2").modal();
                // rowID = $(this).parent().parent()
                // console.log(rowID)

                $("#icpID").addClass("hide");
                $("#success").addClass("hide");
                $("#requiredfield").addClass("hide");
                $("#passwordmatch").addClass("hide");
                $("#newPass").val("");
                $("#retypePass").val("");
                
                aid = $(this).data('aid')
                bsid = $(this).data('bs')
                bdb = $(this).data('bdb')
                bdr = $(this).data('bdr')
                bf = $(this).data('bf')
                bsid = $(this).data('bsid')
                rl_id = $(this).data('rl_id')
                bs = $(this).data('bs')
                // console.log('aid ' + '= ' + aid)
                // console.log('bs ' + '= ' + bs)
                // console.log('bdb ' + '= ' + bdb)
                // console.log('bdr ' + '= ' + bdr)
                // console.log('bf ' + '= ' + bf)
                // console.log('bsid ' + '= ' + bsid)
                // console.log('rl_id ' + '= ' + rl_id)

                $("#RborrowerID").val(rl_id);
                $("#Rdatetimepicker").val(bdb);
                $("#Rdatetimepicker4").val(bdr);
                $("#statusID").val(bsid);
              
                $('#RsaveBtn').unbind('click').on('click',function(){
                    console.log("test")

                  var payload = {
                    "B_ID" : aid,
                    "B_DATE_BORROWED" : $("#Rdatetimepicker").val(),
                    "B_DUE_DATE" : $("#Rdatetimepicker4").val(),
                    "B_FINES" : bf,
                    "BOOK_STATUS_ID"  : $("#statusID").val()
                  }
                    console.log(payload)

                  $.main.executeExternalPost(path + "updateBorrow_returned", JSON.stringify(payload)).done(function(result) {
                    console.log(result)
                    var status = result.status;
                    
                    if(status == "SUCCESS") {
                        $("#success").removeClass("hide");
                        setTimeout(function() {
                        window.location.href = "request.php"
                        },200);

                    }else {
                        $("#submitCC").removeClass("hide");
                        $("#preloader").addClass("hide");
                        $("#icpID").removeClass("hide");
                        $("#submitAdminPass").removeClass("hide");
                    }

                  })
                })
              })
            },
            "scrollY": true,
            "bFilter": false, 
            "bLengthChange": false,
            'scrollX' : true,
             'buttons': [
                   {  
                    extend: 'excelHtml5',
                    title: "Request",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
  
                    extend:  'pdfHtml5',
                    title: "Request",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Request",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6] }
                  },
                 ]
          });

          $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
          $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
          $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
          $(".dt-buttons").addClass("hidden")

      })
    }
    var __fetchallrenew = function() {
      // console.log("test");
      var payload = {
        "BOOK_STATUS_ID" : "3"
      }
      $.main.executeExternalPost(path + "getallBookByBookStatus",JSON.stringify(payload)).done(function(result) {
        console.log(result);

        var items = result.payload;
        console.log(items)
        var data = [];

           for(i=0; i<items.length; i++){

             var trc = $('<tr>');

           trc.append($('<td class="row1">')
             .append(result.payload[i].RL_ID));
           trc.append($('<td class="row2">')
             .append(result.payload[i].BOOK_STATUS));
           trc.append($('<td class="row3">')
             .append(result.payload[i].MATERIAL_TITLE));
           trc.append($('<td class="row4">')
             .append(result.payload[i].ACCESSION_NO));
           trc.append($('<td class="row5">')
             .append(result.payload[i].B_DATE_BORROWED));
           trc.append($('<td class="row6">')
             .append(result.payload[i].B_DUE_DATE));
           trc.append($('<td class="row7">')
             .append(result.payload[i].B_FINES));
           trc.append($('<td class="row8">')
             .append($('<button class="btn btn-danger btn-xs updateBtn" id="updateBtn" '
                  + ' data-aid="'+ result.payload[i].B_ID +'" '
                  + ' data-bs="'+ result.payload[i].BOOK_STATUS +'"'
                  + ' data-bdb="'+ result.payload[i].B_DATE_BORROWED +'"'
                  + ' data-bdr="'+ result.payload[i].B_DUE_DATE +'"'
                  + ' data-bsid="'+ result.payload[i].BOOK_STATUS_ID +'"'
                  + ' data-bf="'+ result.payload[i].B_FINES +'"'
                  + ' data-rl_id="'+ result.payload[i].RL_ID +'" >Update Request</button>')));


           $('#tbody').append(trc);

          }

          var dataRow = $("#myTable").DataTable(
          {
            'dom' : 'Brftip',
            'destroy': true,
            "scrollY": true,
            "bFilter": false, 
            "bLengthChange": false,
            'scrollX' : true,
             'buttons': [
                   {  
                    extend: 'excelHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
                  
                    extend:  'pdfHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6] }
                  },
                 ]
          });

          $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
          $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
          $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
          $(".dt-buttons").addClass("hidden")

        $('.updateBtn').unbind('click').on('click',function(){
          $("#myModal2").modal();
          // rowID = $(this).parent().parent()
          // console.log(rowID)

          $("#icpID").addClass("hide");
          $("#success").addClass("hide");
          $("#requiredfield").addClass("hide");
          $("#passwordmatch").addClass("hide");
          $("#newPass").val("");
          $("#retypePass").val("");
          
          aid = $(this).data('aid')
          bsid = $(this).data('bs')
          bdb = $(this).data('bdb')
          bdr = $(this).data('bdr')
          bf = $(this).data('bf')
          bsid = $(this).data('bsid')
          rl_id = $(this).data('rl_id')
          bs = $(this).data('bs')
          // console.log('aid ' + '= ' + aid)
          // console.log('bs ' + '= ' + bs)
          // console.log('bdb ' + '= ' + bdb)
          // console.log('bdr ' + '= ' + bdr)
          // console.log('bf ' + '= ' + bf)
          // console.log('bsid ' + '= ' + bsid)
          // console.log('rl_id ' + '= ' + rl_id)

          $("#RborrowerID").val(rl_id);
          $("#Rdatetimepicker").val(bdb);
          $("#Rdatetimepicker4").val(bdr);
          $("#statusID").val(bsid);
        
          $('#RsaveBtn').unbind('click').on('click',function(){
              console.log("test")

            var payload = {
              "B_ID" : aid,
              "B_DATE_BORROWED" : $("#Rdatetimepicker").val(),
              "B_DUE_DATE" : $("#Rdatetimepicker4").val(),
              "B_FINES" : bf,
              "BOOK_STATUS_ID"  : $("#statusID").val()
            }
              console.log(payload)

            $.main.executeExternalPost(path + "updateBorrow_returned", JSON.stringify(payload)).done(function(result) {
              console.log(result)
              var status = result.status;
              
              if(status == "SUCCESS") {
                  $("#success").removeClass("hide");
                  setTimeout(function() {
                  window.location.href = "borrow.php"
                  },200);

              }else {
                  $("#submitCC").removeClass("hide");
                  $("#preloader").addClass("hide");
                  $("#icpID").removeClass("hide");
                  $("#submitAdminPass").removeClass("hide");
              }

            })
          })
        })
      })
    }
    var __fetchallreturned = function() {
      // console.log("test");
      var payload = {
        "BOOK_STATUS_ID" : "4"
      }
      $.main.executeExternalPost(path + "getallBookByBookStatus",JSON.stringify(payload)).done(function(result) {
        console.log(result);

        var items = result.payload;
        console.log(items)
        var data = [];

           for(i=0; i<items.length; i++){

             var trc = $('<tr>');

           trc.append($('<td class="row1">')
             .append(result.payload[i].RL_ID));
           trc.append($('<td class="row2">')
             .append(result.payload[i].BOOK_STATUS));
           trc.append($('<td class="row3">')
             .append(result.payload[i].MATERIAL_TITLE));
           trc.append($('<td class="row4">')
             .append(result.payload[i].ACCESSION_NO));
           trc.append($('<td class="row5">')
             .append(result.payload[i].B_DATE_BORROWED));
           trc.append($('<td class="row6">')
             .append(result.payload[i].B_DUE_DATE));
           trc.append($('<td class="row7">')
             .append(result.payload[i].B_FINES));
           trc.append($('<td class="row8">')
             .append($('<button class="btn btn-danger btn-xs updateBtn" id="updateBtn" '
                  + ' data-aid="'+ result.payload[i].B_ID +'" '
                  + ' data-bs="'+ result.payload[i].BOOK_STATUS +'"'
                  + ' data-bdb="'+ result.payload[i].B_DATE_BORROWED +'"'
                  + ' data-bdr="'+ result.payload[i].B_DUE_DATE +'"'
                  + ' data-bsid="'+ result.payload[i].BOOK_STATUS_ID +'"'
                  + ' data-bf="'+ result.payload[i].B_FINES +'"'
                  + ' data-rl_id="'+ result.payload[i].RL_ID +'" >Update Request</button>')));


           $('#tbody').append(trc);

          }

          var dataRow = $("#myTable").DataTable(
          {
            'dom' : 'Brftip',
            'destroy': true,
            "scrollY": true,
            "bFilter": false, 
            "bLengthChange": false,
            'scrollX' : true,
             'buttons': [
                   {  
                    extend: 'excelHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
                  
                    extend:  'pdfHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6] }
                  },
                 ]
          });

          $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
          $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
          $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
          $(".dt-buttons").addClass("hidden")

        $('.updateBtn').unbind('click').on('click',function(){
          $("#myModal2").modal();
          // rowID = $(this).parent().parent()
          // console.log(rowID)
          $("#icpID").addClass("hide");
          $("#success").addClass("hide");
          $("#requiredfield").addClass("hide");
          $("#passwordmatch").addClass("hide");
          $("#newPass").val("");
          $("#retypePass").val("");
          
          aid = $(this).data('aid')
          bsid = $(this).data('bs')
          bdb = $(this).data('bdb')
          bdr = $(this).data('bdr')
          bf = $(this).data('bf')
          bsid = $(this).data('bsid')
          rl_id = $(this).data('rl_id')
          bs = $(this).data('bs')
          // console.log('aid ' + '= ' + aid)
          // console.log('bs ' + '= ' + bs)
          // console.log('bdb ' + '= ' + bdb)
          // console.log('bdr ' + '= ' + bdr)
          // console.log('bf ' + '= ' + bf)
          // console.log('bsid ' + '= ' + bsid)
          // console.log('rl_id ' + '= ' + rl_id)

          $("#RborrowerID").val(rl_id);
          $("#Rdatetimepicker").val(bdb);
          $("#Rdatetimepicker4").val(bdr);
          $("#statusID").val(bsid);
        
          $('#RsaveBtn').unbind('click').on('click',function(){
              console.log("test")

            var payload = {
              "B_ID" : aid,
              "B_DATE_BORROWED" : $("#Rdatetimepicker").val(),
              "B_DUE_DATE" : $("#Rdatetimepicker4").val(),
              "B_FINES" : bf,
              "BOOK_STATUS_ID"  : $("#statusID").val()
            }
              console.log(payload)

            $.main.executeExternalPost(path + "updateBorrow_returned", JSON.stringify(payload)).done(function(result) {
              console.log(result)
              var status = result.status;
              
              if(status == "SUCCESS") {
                  $("#success").removeClass("hide");
                  setTimeout(function() {
                  window.location.href = "request.php"
                  },200);

              }else {
                  $("#submitCC").removeClass("hide");
                  $("#preloader").addClass("hide");
                  $("#icpID").removeClass("hide");
                  $("#submitAdminPass").removeClass("hide");
              }

            })
          })
        })
      })
    }

    var API = host+'/api2/wsv1/api/student/';
    // var path = host+"/api2/wsv1/ApiLibrary/"
    var path = "http://localhost/magsaysay_library_ws/index.php/wsv1/api/"
    var __request = function() {
      console.log("attached events to library");

      $.main.executeExternalPost(path +'fetchAllMaterials2').done(function (result) {
      // $.main.executeExternalPost(path +'fetchallrequest').done(function (result) {
        console.log(result);
        
        var items = result.payload;
        console.log(items)
        var data = [];

           for(i=0; i<items.length; i++){

             var trc = $('<tr>');

           trc.append($('<td class="row1">')
             .append(result.payload[i].MATERIAL_TITLE));
           trc.append($('<td class="row2">')
             .append(result.payload[i].AUTHOR_NAME));
           trc.append($('<td class="row3">')
             .append(result.payload[i].BOOK_TYPE));
           var bookStatus = result.payload[i].BOOK_STATUS
             if (bookStatus == "" || bookStatus == null) {
              bookStatus = "Available"
             }else if((bookStatus === "Returned")
              || (bookStatus === "Expired")
              || (bookStatus === "Request to borrow")
              || (bookStatus === "Borrowed and Issued")) {
              
              bookStatus = "Available"
             }else{
              bookStatus = result.payload[i].BOOK_STATUS
             }
           trc.append($('<td class="row4">')
             .append(bookStatus));

            // var borrowed;
            //   if(bookStatus === "Request to borrow"){
            //     borrowed = "hide";
            //   }
            //   else{
            //     borrowed = "";
            //   }

           trc.append($('<td class="row5">')
             .append($('<button class="btn btn-danger btn-xs update P7C3_REQUESTTOBORROW" ' //  '+borrowed+'
                  + ' data-uid="'+ result.payload[i].MATERIAL_ID +' "  >Request to borrow</button>')))

           $('#tbody').append(trc);

          }

        var dataRow = $("#myTable").DataTable({

          'destroy': true,
          "scrollY": true,
          "bFilter": false,
          "bLengthChange": false

        });

        $('#addbook').unbind('click').on('click',function(){
            // rowID = $(this).parent().parent()
            // console.log(rowID)
            
            $("#myModal").modal();
            materialid = $(this).data('uid')
            console.log('material_id ' + '= ' + materialid)
        });

        $('.update').unbind('click').on('click',function(){
            $("#myModalupdate").modal();
          var d = new Date();

          var month = d.getMonth()+1;
          var day = d.getDate();

          var output = d.getFullYear() + '/' +
              ((''+month).length<2 ? '0' : '') + month + '/' +
              ((''+day).length<2 ? '0' : '') + day;
          $("#Rdatetimepicker").val(output)
          
            MATERIAL_ID = $(this).data('uid')
            console.log('MATERIAL_ID ' + '= ' + MATERIAL_ID)

        });

        $('#updatebtn').unbind('click').on('click',function(){

          var payload = {
            "RL_ID" : $("#RborrowerID").val(),
            "MATERIAL_ID" : MATERIAL_ID,
            "B_DATE_BORROWED" : $("#Rdatetimepicker").val(),
            "B_DUE_DATE" : $("#Rdatetimepicker4").val(),
            "B_DATE_RETURNED" : "",
            "B_FINES" : "",
            "BOOK_STATUS_ID" : "1"
          }

          $.main.executeExternalPost(path + "insert_borrow_returned", JSON.stringify(payload)).done(function(result) {
            console.log(result)
            var status = result.status;
            
            if(status == "SUCCESS") {
              // console.log("complete");
              $("#success").removeClass("hide");
              setTimeout(function() {
              window.location.href = "request2.php"
              },200);

            }else {
              // console.log("Failed");
              $("#submitCC").removeClass("hide");
              $("#preloader").addClass("hide");
              $("#icpID").removeClass("hide");
              $("#submitAdminPass").removeClass("hide");
            }
          })
        })
      })
    }


    return {
        fetchallBook : __fetchallBook,
        fetchallborrow : __fetchallborrow,
        fetchallrequest : __fetchallrequest,
        fetchallrenew : __fetchallrenew,
        fetchallreturned : __fetchallreturned,
        request : __request
    }


}());
