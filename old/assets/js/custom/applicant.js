
$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.applicant = (typeof $.main.applicant !=='undefined')? $.main.applicant : {};

$.main.applicant =(function() {

    // var path = "http://sms.massiveits.com/api/index.php/Magsaysay_controller/"
    // var path = "http://20.20.25.41/student-portal-backend/index.php/Magsaysay_controller/"

     var host = window.location.protocol+"//"+window.location.hostname ;
     var path = host+"/api/index.php/Magsaysay_controller/"
    
    var getApplicantList = path + "getApplicantList"
    var updatePassword = path + "updatePassword"

    var __applicantInfo = function() {

      $.main.executeExternalPost(getApplicantList).done(function(result) {
        console.log(result);

        var items = result.payload;
        console.log(items)
        var data = [];

           for(i=0; i<items.length; i++){

             var trc = $('<tr class='+result.payload[i].application_status+'>');

           trc.append($('<td class="row1">')
             .append(result.payload[i].applicant_fname));
           trc.append($('<td class="row2">')
             .append(result.payload[i].applicant_lname));
           trc.append($('<td class="row3">')
             .append(result.payload[i].applicant_email));
           trc.append($('<td class="row4">')
             .append($('<button class="btn btn-success btn-xs resetpass P2C2_RESET" id="resetpass" '
                  + ' data-aid="'+ result.payload[i].applicant_id +'">Reset</button>')));
           trc.append($('<td class="row5">')
             .append(result.payload[i].applicant_address));
           trc.append($('<td class="row6">')
             .append(result.payload[i].applicant_contact));

           trc.append($('<td class="row7">')
             .append(result.payload[i].applicant_number));
           // trc.append($('<td class="row8">')
           //   .append($('<button class="btn btn-success btn-xs view" id="view" '
           //        + ' data-aid="'+ result.payload[i].applicant_id +'" >View all info</button>')))
                  // + ' data-uid="'+ result.payload[i].applicant_id +'" >Reset Exam</button>')))


           $('#tbody').append(trc);
           $('#tbody').find('tr.7').remove();

          }
          
          var dataRow = $("#myTable").DataTable(
          {
            // 'dom' : 'Brftip',
            'destroy': true,
            fnDrawCallback : function()
            {
              $('.resetpass').unbind('click').on('click',function(){
                $("#myModal").modal();

                $("#icpID").addClass("hide");
                $("#success").addClass("hide");
                $("#requiredfield").addClass("hide");
                $("#passwordmatch").addClass("hide");
                $("#newPass").val("");
                $("#retypePass").val("");

                rowID = $(this).parent().parent()
                // console.log(rowID)
                
                aid = $(this).data('aid')
                console.log('ID ' + '= ' + aid)
                // console.log('ID ' + '= ' + aid)
                // $.cookie("applicant_id", aid);
                $('#resetBtn').unbind('click').on('click',function(){
                  var password = $("#newPass").val()
                  var verifyPassword = $("#retypePass").val()
                  if (password.length > 0 && verifyPassword.length > 0) {
                      $("#requiredfield").addClass("hide");
                      $("#passwordmatch").addClass("hide");

                      if (password == verifyPassword) {
                          // console.log("success");
                          $("#preloader").removeClass("hide");
                          $("#passwordmatch").addClass("hide");
                          $("#submitAdminPass").addClass("hide");

                          var payload = {
                              "applicant_id" : aid,
                              "applicant_password" : verifyPassword,
                          }
                          console.log(payload);
                            $.main.executeExternalPost(updatePassword, JSON.stringify(payload)).done(function(result) {
                              console.log(result);
                              var status = result.status;
                              // console.log(result.workgroup_id);

                              if(status == "SUCCESS") {
                                  // console.log("complete");
                                  $("#success").removeClass("hide");
                                  setTimeout(function() {
                                  window.location.href = "registered_applicants.php"
                                  },200);

                              }else {
                                  // console.log("Failed");
                                  $("#submitCC").removeClass("hide");
                                  $("#preloader").addClass("hide");
                                  $("#icpID").removeClass("hide");
                                  $("#submitAdminPass").removeClass("hide");
                              }
                            })
                          
                      }else{
                          // console.log("failed");
                          $("#passwordmatch").removeClass("hide");
                      }

                  }else {
                      // console.log("required");
                      $("#requiredfield").removeClass("hide");
                      $("#passwordmatch").addClass("hide");
                  }
                })
              })
            },
            "scrollY": true,
            "bFilter": false,
            "bLengthChange": true,
            'scrollX' : true,
             'buttons': [
                   {
                    extend: 'excelHtml5',
                    title: "Registered Applicants",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,4,5,6 ] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
                  
                    extend:  'pdfHtml5',
                    title: "Registered Applicants",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,4,5,6] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Registered Applicants",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,4,5,6 ] }
                  },
                 ]
          });

          $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
          $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
          $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
          $(".dt-buttons").addClass("hidden")
      })
    }
    var __applicantPayments = function() {
      // console.log("test");
      $.main.executeExternalPost(getApplicantList).done(function(result) {
        console.log(result);
        var items = result.payload;
        console.log(items)
        var data = [];

           for(i=0; i<items.length; i++){

             var trc = $('<tr>');

           trc.append($('<td class="row1">')
             .append(result.payload[i].applicant_fname));
           trc.append($('<td class="row2">')
             .append(result.payload[i].applicant_lname));
           trc.append($('<td class="row3">')
             .append(result.payload[i].applicant_email));
           trc.append($('<td class="row4">')
             .append(result.payload[i].applicant_contact));
           trc.append($('<td class="row5">')
             .append(result.payload[i].applicant_number));

           var status = result.payload[i].application_status;

            var step = ""
              if(status ==='7'){
                // step = "Registered"
                step = "Unpaid"
              }else {
                step = "Paid"
              }
              // else if(status === '5'){
              //   step = "for schedule"
              // }else if(status === '4'){
              //   step = "for examination"
              // }else if(status === '3'){
              //   step = "for assessment"
              // }
           trc.append($('<td class="row6">')
             .append(step));

            var ifFetchBtn;
              if(step === "Paid" || step === "for schedule" || step === "for examination" || step === "for assessment"){
                ifFetchBtn = "hide";
              }
              else{
                ifFetchBtn = "";
              }

           trc.append($('<td class="row7">')
             .append($('<button class="btn btn-danger btn-xs P2C3_MAKEPAYMENT updateBtn  '+ifFetchBtn+'" id="updateBtn" '
                  + ' data-aid="'+ result.payload[i].application_id +'" >Make Payment</button>')));


           $('#tbody').append(trc);

          }

          var dataRow = $("#myTable").DataTable(
          {
            // 'dom' : 'Brftip',
            'destroy': true,
            fnDrawCallback : function()
            {
              $('.updateBtn').unbind('click').on('click',function(){
            
                  $("#icpID").addClass("hide");
                  $("#success").addClass("hide");
                  $("#requiredfield").addClass("hide");
                  $("#passwordmatch").addClass("hide");
                  $("#newPass").val("");
                  $("#retypePass").val("");

                rowID = $(this).parent().parent()
                // console.log(rowID)
                
                $("#myModal").modal();
                aid = $(this).data('aid')
               
                console.log('ID ' + '= ' + aid)
                // console.log('ID ' + '= ' + aid)
                // $.cookie("applicant_id", aid);
                $('#saveBtn').unbind('click').on('click',function(){
                  console.log("clicked")
                  var payload = {
                      "application_id" : aid,
                      "application_status" : 6,
                  }
                  console.log(payload);
                    $.main.executeExternalPost(path+"updateApplicationStatus", JSON.stringify(payload)).done(function(result) {
                      console.log(result);
                      var status = result.status;
                      // console.log(result.workgroup_id);

                      if(status == "SUCCESS") {
                          // console.log("complete");
                          $("#success").removeClass("hide");
                          setTimeout(function() {
                          window.location.href = "applicant_payments.php"
                          },200);
                          ////////////////////////////// Audit Trail Start (Jeelbert) ///////////////////////////////
                          var payload23 = {
                            "applicant_ID":aid
                          }
                          var ip_address = $("#myTable").data('ip');
                          var acct= $("#myTable").data('acct');
                          $.main.executeExternalPost(path+"fetchApplicantInfoByApplicantID",JSON.stringify(payload23)).done(function(result){
                            if(result.status === 'SUCCESS'){
                              var payload = {
                                'method':"PostAudit",
                                'acct_id':acct,
                                'client_ip':ip_address,
                                'event_action':"Update",
                                'eventdesc': "Module: Admission / Applicant Payments. Description: User Mark as Paid The Applicant "+result.payload.applicant_fname+" "+result.payload.applicant_mname+" "+result.payload.applicant_lname
                              }
                              $.main.executeExternalPost(host+"/api2/wsv1/api/Audit/",JSON.stringify(payload)).done(function(result){
                                if(result.status === 'SUCCESS'){
                                  console.log(result);
                                }else{
                                  console.log(result.message);
                                }
                              });
                              console.log(payload);
                            }else{
                              console.log(result.message);
                            }
                          });
                          ////////////////////////////// Audit Trail End (Jeelbert) //////////////////////////////////
                      }else {
                          // console.log("Failed");
                          $("#submitCC").removeClass("hide");
                          $("#preloader").addClass("hide");
                          $("#icpID").removeClass("hide");
                          $("#submitAdminPass").removeClass("hide");
                      }
                    })
                })
              })
            },
            "scrollY": true,
            "bFilter": false, 
            "bLengthChange": true,
            'scrollX' : true,
             'buttons': [
                   {  
                    extend: 'excelHtml5',
                    title: "Applicant Payments",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
                  
                    extend:  'pdfHtml5',
                    title: "Applicant Payments",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Applicant Payments",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
                  },
                 ]
          });

          $(".btnCSV").unbind('click').on("click",function(){ 
            $(".buttons-csv").click();
            ////////////////////////////// Audit Trail Start (Jeelbert) ///////////////////////////////
        
            var ip_address = $("#myTable").data('ip');
            var acct= $("#myTable").data('acct');
            
              var payload = {
                'method':"PostAudit",
                'acct_id':acct,
                'client_ip':ip_address,
                'event_action':"Update",
                'eventdesc': "Module: Admission / Applicant Payments. Description: Export the Table of Aplicant Payment as CSV File "
              }
              $.main.executeExternalPost(host+"/api2/wsv1/api/Audit/",JSON.stringify(payload)).done(function(result){
                if(result.status === 'SUCCESS'){
                  console.log(result);
                }else{
                  console.log(result.message);
                }
              });
              // console.log(payload);
            
            ////////////////////////////// Audit Trail End (Jeelbert) //////////////////////////////////

           });
          $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
          $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
          $(".dt-buttons").addClass("hidden")
      })
    }
    var __applicantSchedule = function() {
      // console.log("test");
      // var payload = {
      //     "application_status" : 6,
      // }
      // $.main.executeExternalPost(path+"getApplicationByStatus", JSON.stringify(payload)).done(function(result) {
      $.main.executeExternalPost(getApplicantList).done(function(result) {
        console.log(result);
        
        var items = result.payload;
        console.log(items)
        var data = [];

           for(i=0; i<items.length; i++){

             var trc = $('<tr class='+result.payload[i].application_status+'>');

           trc.append($('<td class="row1">')
              .append(result.payload[i].applicant_fname));
           trc.append($('<td class="row2">')
              .append(result.payload[i].applicant_lname));
           trc.append($('<td class="row3">')
              .append(result.payload[i].applicant_email));
           trc.append($('<td class="row4">')
              .append(result.payload[i].applicant_contact));
           trc.append($('<td class="row5">')
              .append(result.payload[i].applicant_number));
           trc.append($('<td class="row6">')
              .append(result.payload[i].application_exam_details));

           var status = result.payload[i].application_status;

             var ifFetchBtn;
                if(status === "7"){
                  ifFetchBtn = "hide";
                }
                else{
                  ifFetchBtn = "";
                }
           trc.append($('<td class="row7">')
              .append($('<button class="btn btn-danger btn-xs P2C4_MARKSCHEDULE updateBtn '+ifFetchBtn+'" id="updateBtn" '
                  + ' data-aid="'+ result.payload[i].application_id +'" >Mark Schedule</button>')));


           $('#tbody').append(trc);
           $('#tbody').find('tr.7').remove();
          }

          var dataRow = $("#myTable").DataTable(
          {
            // 'dom' : 'Brftip',
            'destroy': true,
            fnDrawCallback : function()
            {
              $('.updateBtn').unbind('click').on('click',function(){
      
                  $("#icpID").addClass("hide");
                  $("#success").addClass("hide");
                  $("#requiredfield").addClass("hide");
                  $("#passwordmatch").addClass("hide");
                  $("#newPass").val("");
                  $("#retypePass").val("");

                rowID = $(this).parent().parent()
                // console.log(rowID)
                
                $("#myModal").modal();
                aid = $(this).data('aid')
                console.log('ID ' + '= ' + aid)
                // console.log('ID ' + '= ' + aid)
                // $.cookie("applicant_id", aid);

              
                $('#saveBtn').unbind('click').on('click',function(){
                  console.log("clicked")
                  var payload = {
                      "application_id" : aid,
                      "application_exam_details" : $("#datetimepicker4").val(),
                      // "application_date_released" : $("#timeSched").val(),
                  }
                  console.log(payload);
                    $.main.executeExternalPost(path+"updateExamSchedule", JSON.stringify(payload)).done(function(result) {
                      console.log(result);
                      var status = result.status;

                      if(status == "SUCCESS") {
                          // console.log("complete");
                          $("#success").removeClass("hide");
                          setTimeout(function() {
                          window.location.href = "exam_schedule.php"
                          },200);

                      }else {
                          // console.log("Failed");
                          $("#submitCC").removeClass("hide");
                          $("#preloader").addClass("hide");
                          $("#icpID").removeClass("hide");
                          $("#submitAdminPass").removeClass("hide");
                      }
                    })
                })
              })
            },
            "scrollY": true,
            "bFilter": false, 
            "bLengthChange": true,
            'scrollX' : true,
             'buttons': [
                   {  
                    extend: 'excelHtml5',
                    title: "Exam Schedules",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
                  
                    extend:  'pdfHtml5',
                    title: "Exam Schedules",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Exam Schedules",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
                  },
                 ]
          });

          $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
          $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
          $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
          $(".dt-buttons").addClass("hidden")
        
      })
    }
    var __applicantExamResults = function() {
      // console.log("test");

      // $.main.executeExternalPost(getApplicantList).done(function(result) {
      $.main.executeExternalPost(path +"getExamResult").done(function(result) {
        console.log(result);
        
        var items = result.payload;
        console.log(items)
        var data = [];

           for(i=0; i<items.length; i++){

             var trc = $('<tr>');

           trc.append($('<td class="row1">')
             .append(result.payload[i].fname));
           trc.append($('<td class="row2">')
             .append(result.payload[i].application_number));
           trc.append($('<td class="row3">')
             .append(result.payload[i].IQ));
           trc.append($('<td class="row4">')
             .append(result.payload[i].A));
           trc.append($('<td class="row5">')
             .append(result.payload[i].B));
           trc.append($('<td class="row6">')
             .append(result.payload[i].C));
           trc.append($('<td class="row7">')
             .append(result.payload[i].D));
           trc.append($('<td class="row8">')
             .append(result.payload[i].E));
           trc.append($('<td class="row9">')
             .append(result.payload[i].SUM));
           trc.append($('<td class="row10">')
             .append(result.payload[i].QUO));

            var status = result.payload[i].application_status
            
            var step = ""
              if(status ==='7'){
                // step = "Registered"
                step = "Unpaid"
              }else if(status === '6'){
                step = "Paid"
              }else if(status === '5'){
                step = "for schedule"
              }else if(status === '4'){
                step = "for examination"
              }else if(status === '2'){
                step = "failed"
              }else if(status === '1'){
                step = "Passed"
              }

           trc.append($('<td class="row11">')
             .append(step));
           trc.append($('<td class="row12">')
             .append($('<button class="btn btn-danger btn-xs updateBtn P2C5_CHANGE" id="updateBtn" '
                  + ' data-aid="'+ result.payload[i].application_id +'" >Change</button>')));
                  // + ' data-uid="'+ result.payload[i].applicant_id +'" >Reset Exam</button>')))


           $('#tbody').append(trc);

          }

          var dataRow = $("#myTable").DataTable(
          {
            // 'dom' : 'Brftip',
            'destroy': true,
            fnDrawCallback : function()
            {
              $('.updateBtn').unbind('click').on('click',function(){
                    
                  $("#icpID").addClass("hide");
                  $("#success").addClass("hide");
                  $("#requiredfield").addClass("hide");
                  $("#passwordmatch").addClass("hide");
                  $("#newPass").val("");
                  $("#retypePass").val("");

                rowID = $(this).parent().parent()
                // console.log(rowID)
                
                $("#myModal").modal();
                aid = $(this).data('aid')
                console.log('ID ' + '= ' + aid)
                // console.log('ID ' + '= ' + aid)
                // $.cookie("applicant_id", aid);

                $('#saveBtn').unbind('click').on('click',function(){
                  console.log("clicked")
                  var payload = {
                      "application_id" : aid,
                      "application_status" : $("#statusID").val(),
                  }
                  console.log(payload);
                    $.main.executeExternalPost(path+"updateApplicationStatus", JSON.stringify(payload)).done(function(result) {
                      console.log(result);
                      var status = result.status;
                      // console.log(result.workgroup_id);

                      if(status == "SUCCESS") {
                          // console.log("complete");
                          $("#success").removeClass("hide");
                          setTimeout(function() {
                          window.location.href = "exam_results.php"
                          },200);

                      }else {
                          // console.log("Failed");
                          $("#submitCC").removeClass("hide");
                          $("#preloader").addClass("hide");
                          $("#icpID").removeClass("hide");
                          $("#submitAdminPass").removeClass("hide");
                      }
                    })
                })
              })
            },
            "scrollY": true,
            "bFilter": false, 
            "bLengthChange": true,
            'scrollX' : true,
             'buttons': [
                   {  
                    extend: 'excelHtml5',
                    title: "Exam Results",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6,7,8,9,10,11 ] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
                  
                    extend:  'pdfHtml5',
                    title: "Exam Results",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6,7,8,9,10,11 ] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Exam Results",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6,7,8,9,10,11 ] }
                  },
                 ]
          });

          $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
          $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
          $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
          $(".dt-buttons").addClass("hidden")


        
      })
    }


    return {
        applicantInfo : __applicantInfo,
        applicantPayments : __applicantPayments,
        applicantSchedule : __applicantSchedule,
        applicantExamResults : __applicantExamResults
    };

}());
