

$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.tuit = (typeof $.main.tuit !=='undefined')? $.main.tuit : {};

$.main.tuit =(function() {


  // var path = "http://20.20.25.41/student-portal-backend/index.php/MagsaysayAccountingController/"
   // var path = "http://sms.massiveits.com/api/index.php/MagsaysayAccountingController/";
   // var aud_path = "http://sms.massiveits.com/api2/wsv1/api/Audit/";

     var host = window.location.protocol+"//"+window.location.hostname ;
     var path = host+"/api/index.php/MagsaysayAccountingController/";
     var aud_path = host+"/api2/wsv1/api/Audit/";

    var __tuitInteg = function() {
      $.main.executeGet(path+"getAllFeeTemplate").done(function(result){ 
        result.payload.forEach(function(item){
          var btnView = "<button class='btn btn-success btnVw P6C2_VIEW' data-id="+item.fee_template_id+"> VIEW </button>";
          $("#tblTuit tbody").append("<tr><td>"+item.fee_template_name+"</td><td>"+item.fee_value+"</td><td>"+btnView+"</td></tr>");
        });
        $(".btnVw").unbind().on('click',function(){
          $('.wrapper').html(''); $("#vwModal").modal('show'); var id = $(this).data('id'); var payload = { "fee_template_id" : id};
           $.main.executeExternalPost(path+"getAllFeeConfigByTemplateID",JSON.stringify(payload)).done(function(result){

             $(".tempName").text(result.payload[0].fee_template_name); $("#inpTempName").val(result.payload[0].fee_template_name);
             for(i=0;i<result.payload.length;i++){
               var j = result.payload[i];
               var x = j.fee_label; var y = x.replace(/_/g,' ');
               $(".wrapper").append('<div class="form-group"><label for='+y+' style="display:block">'+y+' FEE</label><input data-name='+j.fee_label+' type="text" class="form-control frmUpd" disabled value='+j.fee_value+'> <button class="btn btnUpdate P6C2_UPDATE btn-inline btn-warning" data-dd='+j.fee_template_id+' data-count='+i+' data-name='+j.fee_label+' data-beforevalue = '+j.fee_value+'> Update</button><br></div>');
             } 
             $('.btnUpdate').unbind().on('click',function(){
                var input = $(this).data("count"); var label = $(this).data("name"); var dd = $(this).data("dd");
                $("#vwModal").find("input.frmUpd").eq(input).removeAttr("disabled");
                $("#vwModal").find("button.btnUpdate").eq(input).removeClass("btn-warning").addClass("btn-success btnD").text("Save");  
                  $(".btnD").unbind().on('click',function(){
                   var val =  $("#vwModal").find("input.frmUpd").eq(input).val(); var lab =  label;
                     var pd = { "fee_template_id" : dd, "fee_label" : lab, "fee_value" : val }

                    //========================================================================
                            //FOR AUDIT TRAIL ELIN START
                        var event_descs = '';
                        var template_name = result.payload[0].fee_template_name;
                        var beforevalue = $(this).data("beforevalue");
                        var acct_id = $("#getaccountidforTuit").data('acct');
                        var acct_ip = $("#getaccountidforTuit").data('acctip');
                        console.log(template_name)
                        console.log(beforevalue)
                         //FOR AUDIT TRAIL ELIN END
                    //========================================================================


                      $.main.executeExternalPost(path+'updateFeeConfigRate',JSON.stringify(pd)).done(function(result){
                          //========================================================================
                            //FOR AUDIT TRAIL ELIN START
                        if(result.status == "SUCCESS"){
                          if (beforevalue == val) {
                            event_descs = "MODULE: UPDATE TUTION CONFIG FOR "+template_name+"<br>DESCRIPTION: NO CHANGES";
                          }else{
                            event_descs = "MODULE: UPDATE TUTION CONFIG FOR "+template_name+"<br>DESCRIPTION: USER UPDATES DATA <br>DETAILS <br>BEFORE "+lab+": "+beforevalue+
                            "<br>AFTER "+lab+": "+val;
                          }
                          var payload1 = {
                                    "method" : "PostAudit",
                                    "acct_id": acct_id, 
                                    "client_ip": acct_ip,
                                    "event_action":"UPDATED",
                                    "eventdesc": event_descs

                            };
                                $.main.executeExternalPost(aud_path, JSON.stringify(payload1)).done(function(result){
                                  if(result.status == "SUCCESS")
                                  {
                          //FOR AUDIT TRAIL ELIN END
                    //========================================================================
                                          __tuitInteg();
                                          $("#tblTuit tbody").html("");
                                          $("#vwModal").modal("hide");

                                  }
                                    });                        
                     
                        } 
                      });
                    });
              });
              
           });
        });
      });
      
      $("#vwModal").on('hidden.bs.modal',function(){
        $('.wrapper').html('');
      });
     
    };

    var __tuitSum = function() {
      arr = [];
      obj1 = {}; obj2 = {}; obj3 = {} ; obj4 = {}; obj5 = {}; obj6 = {}; obj7 = {}; obj8 = {}; obj9 = {} ; obj10 = {}; obj11 = {}; obj12 = {};
      obj13 = {}; obj14 = {}; obj15 = {} ; obj16 = {}; obj17 = {}; obj18 = {}; obj19 = {}; obj20 = {}; obj21 = {} ; obj22 = {}; obj23 = {}; obj24 = {};
      obj25 = {}; obj26 = {}; obj27 = {} ; obj28 = {}; obj29 = {}; obj30 = {}; obj31 = {}; obj32 = {}; obj33 = {} ; obj34 = {}; obj35 = {}; obj36 = {};
      obj37 = {}; obj38 = {}; obj39 = {} ;

       $(".sum1").on('blur',function(){  
        obj1["fee_label"] = "TUITION"; obj1["fee_type_id"] = "1"; obj1["fee_template_name"] = $(".txtTemp").val(); obj1["fee_value"] = parseFloat($('.sum1').val());
        if (obj1.fee_value == 0){ obj1["fee_value"] = 0; }  else {arr.push(obj1); } 
        
      });

      $(".sum2").on('blur',function(){  
        obj2["fee_label"] = "ATHLETICS"; obj2["fee_type_id"] = "2"; obj2["fee_template_name"] = $(".txtTemp").val(); obj2["fee_value"] = parseFloat($('.sum2').val());    
        if (obj2.fee_value == 0){ obj2["fee_value"] = 0; }  else {arr.push(obj2); } 
        
      });

      $(".sum3").on('focusout',function(){ 
        obj3["fee_label"] = "AVR"; obj3["fee_type_id"] = "2"; obj3["fee_template_name"] = $(".txtTemp").val(); obj3["fee_value"] = parseFloat($('.sum3').val());    
        if (obj3.fee_value == 0){ obj3["fee_value"] = 0; }else {arr.push(obj3); }

      });

      $(".sum4").on('focusout',function(){  

        obj4["fee_label"] = "COMMUNITY_EXTENSION"; obj4["fee_type_id"] = "2"; obj4["fee_template_name"] = $(".txtTemp").val(); obj4["fee_value"] = parseFloat($('.sum4').val());    
        if (obj4.fee_value == 0){ obj4["fee_value"] = 0; }else {arr.push(obj4); }


      });

      $(".sum5").on('focusout',function(){  

        obj5["fee_label"] = "DEVELOPMENTAL"; obj5["fee_type_id"] = "2"; obj5["fee_template_name"] = $(".txtTemp").val(); obj5["fee_value"] = parseFloat($('.sum5').val());    
        if (obj5.fee_value == 0){ obj5["fee_value"] = 0; }else {arr.push(obj5); }

      });

      $(".sum6").on('focusout',function(){  

        obj6["fee_label"] = "IT_SERVICE"; obj6["fee_type_id"] = "2"; obj6["fee_template_name"] = $(".txtTemp").val(); obj6["fee_value"] = parseFloat($('.sum6').val());    
        if (obj6.fee_value == 0){ obj6["fee_value"] = 0; }else {arr.push(obj6); }
      });

      $(".sum7").on('focusout',function(){  

        obj7["fee_label"] = "INTERNET"; obj7["fee_type_id"] = "2"; obj7["fee_template_name"] = $(".txtTemp").val(); obj7["fee_value"] = parseFloat($('.sum7').val());  
        if (obj7.fee_value == 0){ obj7["fee_value"] = 0; }else {arr.push(obj7); }

      });

      $(".sum8").on('focusout',function(){  

        obj8["fee_label"] = "MEDICAL_DENTAL"; obj8["fee_type_id"] = "2"; obj8["fee_template_name"] = $(".txtTemp").val(); obj8["fee_value"] = parseFloat($('.sum8').val());  
        if (obj8.fee_value == 0){ obj8["fee_value"] = 0; }else {arr.push(obj8); }  

      });

      $(".sum9").on('focusout',function(){  

        obj9["fee_label"] = "MEDICAL_EXAM_YEARLY"; obj9["fee_type_id"] = "2"; obj9["fee_template_name"] = $(".txtTemp").val(); obj9["fee_value"] = parseFloat($('.sum9').val());    
        if (obj9.fee_value == 0){ obj9["fee_value"] = 0; }else {arr.push(obj9); }

      });

      $(".sum10").on('focusout',function(){  

        obj10["fee_label"] = "PUBLICATION"; obj10["fee_type_id"] = "2"; obj10["fee_template_name"] = $(".txtTemp").val(); obj10["fee_value"] = parseFloat($('.sum10').val());    
        if (obj10.fee_value == 0){ obj10["fee_value"] = 0; }else {arr.push(obj10); }

      });

      $(".sum11").on('focusout',function(){  

        obj11["fee_label"] = "ID"; obj11["fee_type_id"] = "2"; obj11["fee_template_name"] = $(".txtTemp").val(); obj11["fee_value"] = parseFloat($('.sum11').val());   
        if (obj11.fee_value == 0){ obj11["fee_value"] = 0; }else {arr.push(obj11); } 

      });

      $(".sum12").on('focusout',function(){  

        obj12["fee_label"] = "GUIDENOTES"; obj12["fee_type_id"] = "2"; obj12["fee_template_name"] = $(".txtTemp").val(); obj12["fee_value"] = parseFloat($('.sum12').val());  
        if (obj12.fee_value == 0){ obj12["fee_value"] = 0; }else {arr.push(obj12); }  

      });

      $(".sum13").on('focusout',function(){  

        obj13["fee_label"] = "SEAGULL"; obj13["fee_type_id"] = "3"; obj13["fee_template_name"] = $(".txtTemp").val(); obj13["fee_value"] = parseFloat($('.sum13').val()); 
        if (obj13.fee_value == 0){ obj13["fee_value"] = 0; }else {arr.push(obj13); }   

      });

      $(".sum14").on('focusout',function(){  

        obj14["fee_label"] = "GEN_ED_LAB"; obj14["fee_type_id"] = "3"; obj14["fee_template_name"] = $(".txtTemp").val(); obj14["fee_value"] = parseFloat($('.sum14').val());   
        if (obj14.fee_value == 0){ obj14["fee_value"] = 0; }else {arr.push(obj14); } 

      });

      $(".sum15").on('focusout',function(){  

        obj15["fee_label"] = "TECH_LAB"; obj15["fee_type_id"] = "3"; obj15["fee_template_name"] = $(".txtTemp").val(); obj15["fee_value"] = parseFloat($('.sum15').val());    
        if (obj15.fee_value == 0){ obj15["fee_value"] = 0; }else {arr.push(obj15); } 

      });

      $(".sum16").on('focusout',function(){  

        obj16["fee_label"] = "COMPUTER_LAB"; obj16["fee_type_id"] = "3"; obj16["fee_template_name"] = $(".txtTemp").val(); obj16["fee_value"] = parseFloat($('.sum16').val());    
        if (obj16.fee_value == 0){ obj16["fee_value"] = 0; }else {arr.push(obj16); } 

      });
      
      $(".sum17").on('focusout',function(){  

        obj17["fee_label"] = "SHIPBOARD_FAMILIARIZATION"; obj17["fee_type_id"] = "3"; obj17["fee_template_name"] = $(".txtTemp").val(); obj17["fee_value"] = parseFloat($('.sum17').val()); 
        if (obj17.fee_value == 0){ obj17["fee_value"] = 0; }else {arr.push(obj17); }    

      });

      $(".sum18").on('focusout',function(){  

        obj18["fee_label"] = "KUMON_MATH"; obj18["fee_type_id"] = "3"; obj18["fee_template_name"] = $(".txtTemp").val(); obj18["fee_value"] = parseFloat($('.sum18').val());    
        if (obj18.fee_value == 0){ obj18["fee_value"] = 0; }else {arr.push(obj18); } 

      });

      $(".sum19").on('focusout',function(){  

        obj19["fee_label"] = "MARITIME_TEXT"; obj19["fee_type_id"] = "5"; obj19["fee_template_name"] = $(".txtTemp").val(); obj19["fee_value"] = parseFloat($('.sum19').val());    
        if (obj19.fee_value == 0){ obj19["fee_value"] = 0; }else {arr.push(obj19); } 

      });

      $(".sum20").on('focusout',function(){  

        obj20["fee_label"] = "GEN_ED_BOOKS"; obj20["fee_type_id"] = "5"; obj20["fee_template_name"] = $(".txtTemp").val(); obj20["fee_value"] = parseFloat($('.sum20').val());    
        if (obj20.fee_value == 0){ obj20["fee_value"] = 0; }else {arr.push(obj20); } 

      });

      $(".sum21").on('focusout',function(){  

        obj21["fee_label"] = "ACCOMMODATION"; obj21["fee_type_id"] = "4"; obj21["fee_template_name"] = $(".txtTemp").val(); obj21["fee_value"] = parseFloat($('.sum21').val());    
        if (obj21.fee_value == 0){ obj21["fee_value"] = 0; }else {arr.push(obj21); } 

      });

      $(".sum22").on('focusout',function(){  

        obj22["fee_label"] = "MESS_SERVICE"; obj22["fee_type_id"] = "4"; obj22["fee_template_name"] = $(".txtTemp").val(); obj22["fee_value"] = parseFloat($('.sum22').val());    
        if (obj22.fee_value == 0){ obj22["fee_value"] = 0; }else {arr.push(obj22); } 

      });

      $(".sum23").on('focusout',function(){  

        obj23["fee_label"] = "TRANSPORTATION"; obj23["fee_type_id"] = "4"; obj23["fee_template_name"] = $(".txtTemp").val(); obj23["fee_value"] = parseFloat($('.sum23').val()); 
        if (obj23.fee_value == 0){ obj23["fee_value"] = 0; }else {arr.push(obj23); }    

      });

      $(".sum24").on('focusout',function(){  

        obj24["fee_label"] = "UNIFORMS"; obj24["fee_type_id"] = "4"; obj24["fee_template_name"] = $(".txtTemp").val(); obj24["fee_value"] = parseFloat($('.sum24').val());    
        if (obj24.fee_value == 0){ obj24["fee_value"] = 0; }else {arr.push(obj24); } 

      });

      $(".sum25").on('focusout',function(){  

        obj25["fee_label"] = "GRADUATION"; obj25["fee_type_id"] = "4"; obj25["fee_template_name"] = $(".txtTemp").val(); obj25["fee_value"] = parseFloat($('.sum25').val());  
        if (obj25.fee_value == 0){ obj25["fee_value"] = 0; }else {arr.push(obj25); }   

      });

      $(".sum26").on('focusout',function(){  

        obj26["fee_label"] = "DIPLOMA_TOR"; obj26["fee_type_id"] = "4"; obj26["fee_template_name"] = $(".txtTemp").val(); obj26["fee_value"] = parseFloat($('.sum26').val()); 
        if (obj26.fee_value == 0){ obj26["fee_value"] = 0; }else {arr.push(obj26); }    

      });

      $(".sum27").on('focusout',function(){  

        obj27["fee_label"] = "EXAM_MATERIALS"; obj27["fee_type_id"] = "4"; obj27["fee_template_name"] = $(".txtTemp").val(); obj27["fee_value"] = parseFloat($('.sum27').val());   
        if (obj27.fee_value == 0){ obj27["fee_value"] = 0; }else {arr.push(obj27); }  

      });

      $(".sum28").on('focusout',function(){  

        obj28["fee_label"] = "RESEARCH_FEE"; obj28["fee_type_id"] = "4"; obj28["fee_template_name"] = $(".txtTemp").val(); obj28["fee_value"] = parseFloat($('.sum28').val());  
        if (obj28.fee_value == 0){ obj28["fee_value"] = 0; }else {arr.push(obj28); }   

      });

      $(".sum29").on('focusout',function(){  

        obj29["fee_label"] = "NSTP_FEE"; obj29["fee_type_id"] = "4"; obj29["fee_template_name"] = $(".txtTemp").val(); obj29["fee_value"] = parseFloat($('.sum29').val());    
        if (obj29.fee_value == 0){ obj29["fee_value"] = 0; }else {arr.push(obj29); } 

      });

      $(".sum30").on('focusout',function(){  

        obj30["fee_label"] = "PRISAA_FEE"; obj30["fee_type_id"] = "4"; obj30["fee_template_name"] = $(".txtTemp").val(); obj30["fee_value"] = parseFloat($('.sum30').val());    
        if (obj30.fee_value == 0){ obj30["fee_value"] = 0; }else {arr.push(obj30); } 

      });

      $(".sum31").on('focusout',function(){  

        obj31["fee_label"] = "RECREATIONAL_FEE"; obj31["fee_type_id"] = "4"; obj31["fee_template_name"] = $(".txtTemp").val(); obj31["fee_value"] = parseFloat($('.sum31').val());    
        if (obj31.fee_value == 0){ obj31["fee_value"] = 0; }else {arr.push(obj31); } 

      });

      $(".sum32").on('focusout',function(){  

        obj32["fee_label"] = "SPORTS_TOURNAMENT_FEE"; obj32["fee_type_id"] = "4"; obj32["fee_template_name"] = $(".txtTemp").val(); obj32["fee_value"] = parseFloat($('.sum32').val());   
        if (obj32.fee_value == 0){ obj32["fee_value"] = 0; }else {arr.push(obj32); }  

      });

      $(".sum33").on('focusout',function(){  

        obj33["fee_label"] = "STUDENT_ORGANIZATION_FEE"; obj33["fee_type_id"] = "4"; obj33["fee_template_name"] = $(".txtTemp").val(); obj33["fee_value"] = parseFloat($('.sum33').val());  
        if (obj33.fee_value == 0){ obj33["fee_value"] = 0; }else {arr.push(obj33); }   

      });

      $(".sum34").on('focusout',function(){  

        obj34["fee_label"] = "YEAR_BOOK_FEE"; obj34["fee_type_id"] = "4"; obj34["fee_template_name"] = $(".txtTemp").val(); obj34["fee_value"] = parseFloat($('.sum34').val());  
        if (obj34.fee_value == 0){ obj34["fee_value"] = 0; }else {arr.push(obj34); }   

      });

      $(".sum35").on('focusout',function(){  

        obj35["fee_label"] = "INSURANCE"; obj35["fee_type_id"] = "4"; obj35["fee_template_name"] = $(".txtTemp").val(); obj35["fee_value"] = parseFloat($('.sum35').val());  
        if (obj35.fee_value == 0){ obj35["fee_value"] = 0; }else {arr.push(obj35); }   

      });

      $(".sum36").on('focusout',function(){  

        obj36["fee_label"] = "REGISTRATION"; obj36["fee_type_id"] = "2"; obj36["fee_template_name"] = $(".txtTemp").val(); obj36["fee_value"] = parseFloat($('.sum36').val());  
        if (obj36.fee_value == 0){ obj36["fee_value"] = 0; }else {arr.push(obj36); }   

      });

      $(".sum37").on('focusout',function(){  

        obj37["fee_label"] = "LIBRARY"; obj37["fee_type_id"] = "2"; obj37["fee_template_name"] = $(".txtTemp").val(); obj37["fee_value"] = parseFloat($('.sum37').val());  
        if (obj37.fee_value == 0){ obj37["fee_value"] = 0; }else {arr.push(obj37); }   

      });

      $(".sum38").on('focusout',function(){  

        obj38["fee_label"] = "GUIDANCE_COUNSELING"; obj38["fee_type_id"] = "2"; obj38["fee_template_name"] = $(".txtTemp").val(); obj38["fee_value"] = parseFloat($('.sum38').val());  
        if (obj38.fee_value == 0){ obj38["fee_value"] = 0; }else {arr.push(obj38); }   

      }); 


      var arrayUnique = function (arr) {
        return arr.filter(function(item, index){
          return arr.indexOf(item) >= index;
        });
      };
      
    var click1 = 2;
    // $(".registrationFee").hide(); $(".labFee").hide(); $(".bookFee").hide(); $(".miscFee").hide();

		$(".regiMin").hover(function(){
      $(".regiMin").css('cursor','pointer');
    }).click(function () {
      click1 % 2 == 0 ?	$(".registrationFee").hide() : $(".registrationFee").show();
			click1++;
		});
      
    var click2 = 2;
		$(".labMin").hover(function(){
      $(".labMin").css('cursor','pointer');
    }).click(function () {
      click2 % 2 == 0 ?	$(".labFee").hide() : $(".labFee").show();
			click2++;
    });

    var click3 = 2;
		$(".bookMin").hover(function(){
      $(".bookMin").css('cursor','pointer');
    }).click(function () {
      click3 % 2 == 0 ?	$(".bookFee").hide() : $(".bookFee").show();
			click3++;
    });
    
    var click4 = 2;
		$(".miscMin").hover(function(){
      $(".miscMin").css('cursor','pointer');
    }).click(function () { 
      click4 % 2 == 0 ?	$(".miscFee").hide() : $(".miscFee").show();
			click4++;
    });
    
      
      $("#btnSum").on('click',function(){ 
        var jobsUnique = arrayUnique(arr); var sum = 0;
        jobsUnique.forEach(function(item){
          sum +=  item.fee_value;
        });
        var sumAll = (sum).toFixed(2); // console.log(jobsUnique) 
        $("#total_FEE").val("P" + sumAll)
        var h = $(".txtTemp").val().length;
        if(h == 0){
          $(".txtTemp").css({"border":"1px solid red"});
                $(".err").removeClass('hide');
                $(".errorMsg").text("Your template name must have a name!");
                $("#btnSum").prop('disabled',true);
                $(".txtTemp").on('click',function(){
                  $(".txtTemp").css({"border":"1px solid #cccccc"});
                  $(".err").addClass('hide');
                  $("#btnSum").prop('disabled',false);
                });
        }
        else {
                var acct_id = $("#getaccountidforTuit").data('acct');
                var acct_ip = $("#getaccountidforTuit").data('acctip');
          $.main.executeExternalPost(path+'insertFeeConfig', JSON.stringify(jobsUnique)).done(function(result){
            if(result.status == "SUCCESS"){
              //console.log(acct_ip)
              //console.log(jobsUnique)
              var payload1 = {
                                    "method" : "PostAudit",
                                    "acct_id": acct_id, 
                                    "client_ip": acct_ip,
                                    "event_action":"ADDED",
                                    "eventdesc": "MODULE: Add Tuition Template, <br> DESCRIPTION: User Added Tuition Template <br>DETAILS: <br>" +   
                                  JSON.stringify(jobsUnique)

                            };
                $.main.executeExternalPost(aud_path, JSON.stringify(payload1)).done(function(result){
                                  if(result.status == "SUCCESS")
                                  {
                                    console.log(payload1)
                                    console.log(result)
                                  window.location.href = 'tuit_conf.php'
                                  }
                                                    
                                                    });
           // window.location.href = 'tuit_conf.php'
           }else if (result.status == "FAILED"){
             $(".txtTemp").css({"border":"1px solid red"});
             $(".err").removeClass('hide');
             $(".errorMsg").text("Your template name already exists!");
             $("#btnSum").prop('disabled',true);
             $(".txtTemp").on('click',function(){
               $(".txtTemp").css({"border":"1px solid #cccccc"});
               $(".err").addClass('hide');
               $("#btnSum").prop('disabled',false);
             });
           }
          })
      
        }
              
      });
    };
    return {
        tuitInteg : __tuitInteg,
        tuitSum : __tuitSum
    };

}());
