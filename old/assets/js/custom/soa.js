$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.soa = (typeof $.main.soa !=='undefined')? $.main.soa : {};

$.main.soa = (function(){
    var host = window.location.protocol+"//"+window.location.hostname ;
  var __individual = function() {
    console.log("WORKING");
    var arr = [];
    var obj = {}
    obj["COURSE"] = "Marine Technology";
    obj["BATCH"] = "MT 1y1-2";
    obj["CUTOFF"] = "August 15, 2018";
    obj["TUIT"] = "PHP 355,000.00";
    arr.push(obj)
        $("#tblIndividual").DataTable({

          'data' : arr,
          'columns': [{"data": "COURSE"}, {"data": "BATCH"}, {"data": "CUTOFF"}, {"data": "TUIT"}]
        });
  };

  var __batch = function() {
   

    $("#tblBatch").DataTable({
                'dom': "Bfrtip",
                'scrollX' : true,
                "searching": true,
                "info": true, 
                "order": [[ 6, "asc" ]],
                'lengthChange': false,
                'buttons': [
                   {  
                    extend: 'excelHtml5',
                    title: "Statement of Account",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
                  
                    extend:  'pdfHtml5',
                    title: "Statement of Account",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Statement of Account",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5,6 ] }
                  },
                 ]
                /*

                'data' : arr,*/
                //'columns': [{"data": "STUDENT"}, {"data": "STUDNUM"}, {"data": "COURSE"}, {"data": "BATCH"}, {"data": "FEE"}]
              });
              //$(".dataTables_filter").addClass("hide")
              $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
              $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
              $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
              $(".dt-buttons").addClass("hidden")

              $("#tblBatch").unbind("click").on("click",'.btnPrint',function(){

                stud_name = $(this).data("name")
                stud_no = $(this).data("sid")
                course = $(this).data("course")
                section = $(this).data("section")
                paid = $(this).data("paid")
                payable = $(this).data("payable")
                balances = $(this).data("bal")
                id = $(this).data("id")
                semid = $(this).data("semid")

                console.log(id);
                console.log(semid);



                $(".modal").modal()
                $(".ind_stud_name").html(stud_name)
                $(".ind_stud_no").html(stud_no)
                $(".ind_stud_course").html(course)
                $(".ind_stud_section").html(section)
                $(".ind_stud_balances").html(balances)
                $(".ind_stud_paid").html(paid)
                $(".ind_stud_payable").html(payable)

                $(".btnsoa").unbind("click").on("click",function(){
                setTimeout(function() {

                  window.open("print_soa.php?id="+id+"&sem_ID="+semid+"",'_blank');
          
                },100);

                })
                
              })


  };

  return {
    individual : __individual,
    batch : __batch
  };
}());