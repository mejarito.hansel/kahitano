$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.discAct = (typeof $.main.discAct !=='undefined')? $.main.discAct : {};

$.main.discAct =(function() {
var host = window.location.protocol+"//"+window.location.hostname ;
var aud_path = host+"/api2/wsv1/api/Audit/";
    var __viewDisc = function(){


        // var path = 'http://localhost/csjp_api/wsv1/';
        var path = host+"/api2/wsv1/Disc/";

        $.main.executeGet(path+'viewAllDisc').done(function(result){
            // if($.fn.DataTable.isDataTable('#tblDisc')) {
            //     $('#tblDisc').DataTable().destroy();
            //     $('#tblDisc tbody').empty();
            // }

            var arr = [];
                $("#tblDisc thead").append("<tr><th>Student Name</th><th>Disciplinary Action</th><th>Date</th><th>Action</th></tr>")

                for (i=0;i<result.payload.length;i++){
                var obj = {};
                var item = result.payload[i];
                var btnDelete = "<button class='btn btn-danger btn-xs btnDelete' data-id="+item.DISC_ID+">Delete</button>";
                obj["FULLNAME"] = item.FULLNAME;
                obj["DISC_ACTION"] = item.DISC_ACTION;
                obj["DATA"] = item.DATA;
                obj["ACTION"] = btnDelete;
                arr.push(obj);

                    
            }
            $("#tblDisc").DataTable({
                'data' : arr,
                "lengthChange": false,
                'columns': [{"data": "FULLNAME"}, {"data": "DISC_ACTION"}, {"data": "DATA"}, {"data": "ACTION"}],
                'columnDefs' : [
                    {'orderable': false, 'targets': [3]}],

            });
           
            $("#tblDisc").unbind().on('click','.btnDelete',function(){
                
                var btnDelete = $(this).data('id');

                var payload = {
                    "DISC_ID" : btnDelete
                };
                console.log(payload)
                var acct_id = $("#getaccountidforDA").data('acct');
                var acct_ip = $("#getaccountidforDA").data('acctip');
                    $.main.executeExternalPost(path+ '/dltDiscStudent',JSON.stringify(payload)).done(function(result){
                        if (result.status == "SUCCESS"){

                            var payload1 = {
                                    "method" : "PostAudit",
                                    "acct_id": acct_id, 
                                    "client_ip": acct_ip,
                                    "event_action":"REMOVE",
                                    "eventdesc": "MODULE: Disciplinary Action, <br> DESCRIPTION: User REMOVE a Student From Disciplinary Action <br> ID: "
                                    +  btnDelete 

                            };
                          //  console.log(payload1)
                         $.main.executeExternalPost(aud_path, JSON.stringify(payload1)).done(function(result){
                                  if(result.status == "SUCCESS"){

                            // __viewDisc();
                            location.reload();
                        }
                          });

                            // $("#updateTest").modal('hide');
                            // $("#updateTest").on('hidden.bs.modal',function(){
                            // $(".btnDelete").modal('hide');    
                            // });
                            
                        }else{
                            console.log("ERROR")
                        }
                    });
            })    
            
        });
   
    };

    var __addDiscAction = function() {
        // console.log("WORKING")
        $("#cadetName").select2({
            placeholder: "Please select a Student"
        });
        // var path = 'http://localhost/csjp_api/wsv1/';
        var path = host+"/api2/wsv1/Disc/";
     //   var acct_id = $("#getaccountidforDA").data('acct');
        $.main.executeGet(path+'viewStudents').done(function(result){
            if(result.status == "SUCCESS"){
                for(i=0;i<result.payload.length;i++){
                    var item = result.payload[i];
                    $("#cadetName").append($('<option>', {
                        value : item.SI_ID,
                        text : item.FULLNAME
                    }));
                }
            }
               
        });
        $("#btnSave").on('click',function(){
            var  payload = {
                "si_ID": $("#cadetName option:selected").val(),
                "DISC_ACTION" : $("#disc_action").val(),
                "STATUS" : "A",
                "DATE" : $("#date").val()                   
              };
                var acct_id = $("#getaccountidforDA").data('acct');
                var acct_ip = $("#getaccountidforDA").data('acctip');
              //  console.log(payload)
                    $.main.executeExternalPost(path+'addDiscStudent', JSON.stringify(payload)).done(function(result){
                        if(result.status == "SUCCESS"){
                            var test = $('#select2-cadetName-container').attr('title');
                            var payload1 = {
                                    "method" : "PostAudit",
                                    "acct_id": acct_id, 
                                    "client_ip": acct_ip,
                                    "event_action":"ADDED",
                                    "eventdesc": "MODULE: Disciplinary Action, <br> DESCRIPTION: User Added Disciplinary Action Student Name: "
                                    +  test + "<br> DISCIPLINARY_ACTION: "+ $("#disc_action").val() + "<br> DATE: "+ $("#date").val() 

                            };
                           // console.log(payload1)
                            $.main.executeExternalPost(aud_path, JSON.stringify(payload1)).done(function(result){
                                  if(result.status == "SUCCESS"){
                                     window.location.href= 'disciplinary_action.php';
                                        }


                            });
                        }
                   });
        });


    }
               
  
        return {
            viewDisc : __viewDisc,
            addDiscAction : __addDiscAction
        };
    }());
