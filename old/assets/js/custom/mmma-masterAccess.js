$( document ).ready(function(){
	setTimeout(function(){
		//TOTAL OF 23
		var admission = {
			P2C1_VIEWFULLINFORMATION: function(status){
				if(status === 1){
					$("a.P2C1_VIEWFULLINFORMATION").show()
					console.log("NAKA UNHIDE")
				}else{
					$("a.P2C1_VIEWFULLINFORMATION").hide()
					console.log("NAKA HIDE")
				}
			},
			P2C1_UPDATEINFORMATION: function(status){
				if(status === 1){
					$("a#P2C1_UPDATEINFORMATION").show()
				}else{
					$("a#P2C1_UPDATEINFORMATION").hide()
				}
			},
			P2C2_EXPORTSASCSV: function(status){
				if(status === 1){
					$("a#P2C2_EXPORTSASCSV").show()
				}else{
					$("a#P2C2_EXPORTSASCSV").hide()
				}
			},
			P2C2_RESET: function(status){
				if(status === 1){
					$("#myTable #tbody").find("button.P2C2_RESET").show()
				}else{
					$("#myTable #tbody").find("button.P2C2_RESET").hide()
				}
			},
			P2C3_EXPORTSASCSV: function(status){
				if(status === 1){
					$("a#P2C3_EXPORTSASCSV").show()
				}else{
					$("a#P2C3_EXPORTSASCSV").hide()
				}
			},
			P2C3_MAKEPAYMENT: function(status){
				if(status === 1){
					$("button.P2C3_MAKEPAYMENT").show()
				}else{
					$("button.P2C3_MAKEPAYMENT").hide()
				}
			},
			P2C4_EXPORTSASCSV: function(status){
				if(status === 1){
					$("a#P2C4_EXPORTSASCSV").show()
				}else{
					$("a#P2C4_EXPORTSASCSV").hide()
				}
			},
			P2C4_MARKSCHEDULE: function(status){
				if(status === 1){
					$("button.P2C4_MARKSCHEDULE").show()
				}else{
					$("button.P2C4_MARKSCHEDULE").hide()
				}
			},
			P2C5_EXPORTSASCSV: function(status){
				if(status === 1){
					$("button#P2C5_EXPORTSASCSV").show()
				}else{
					$("button#P2C5_EXPORTSASCSV").hide()
				}
			},
			P2C5_CHANGE: function(status){
				if(status === 1){
					$("button.P2C5_CHANGE").show()
				}else{
					$("button.P2C5_CHANGE").hide()
				}
			},
			P2C6_UPDATE: function(status){
				if(status === 1){
					$("button.P2C6_UPDATE").show()
				}else{
					$("button.P2C6_UPDATE").hide()
				}
			},
			P2C7_EXPORTSASCSV: function(status){
				if(status === 1){
					$("a#P2C7_EXPORTSASCSV").show()
				}else{
					$("a#P2C7_EXPORTSASCSV").hide()
				}
			},
			P2C7_MARKSCEDULE: function(status){
				if(status === 1){
					$("button.P2C7_MARKSCEDULE").show()
				}else{
					$("button.P2C7_MARKSCEDULE").hide()
				}
			},
			P2C8_EXPORTSASCSV: function(status){
				if(status === 1){
					$("button#P2C8_EXPORTSASCSV").show()
				}else{
					$("button#P2C8_EXPORTSASCSV").hide()
				}
			},
			P2C8_CHANGE: function(status){
				if(status === 1){
					$("button.P2C8_CHANGE").show()
				}else{
					$("button.P2C8_CHANGE").hide()
				}
			},
			P2C9_EXPORTSASCSV: function(status){
				if(status === 1){
					$("a#P2C9_EXPORTSASCSV").show()
				}else{
					$("a#P2C9_EXPORTSASCSV").hide()
				}
			},
			P2C9_MARKSCEDULE: function(status){
				if(status === 1){
					$("button.P2C9_MARKSCEDULE").show()
				}else{
					$("button.P2C9_MARKSCEDULE").hide()
				}
			},
			P2C10_EXPORTSASCSV: function(status){
				if(status === 1){
					$("button#P2C10_EXPORTSASCSV").show()
				}else{
					$("button#P2C10_EXPORTSASCSV").hide()
				}
			},
			P2C10_CHANGE: function(status){
				if(status === 1){
					$("button.P2C10_CHANGE").show()
				}else{
					$("button.P2C10_CHANGE").hide()
				}
			},
			P2C11_EXPORTSASCSV: function(status){
				if(status === 1){
					$("a#P2C11_EXPORTSASCSV").show()
				}else{
					$("a#P2C11_EXPORTSASCSV").hide()
				}
			},
			P2C11_MARKSCEDULE: function(status){
				if(status === 1){
					$("button.P2C11_MARKSCEDULE").show()
				}else{
					$("button.P2C11_MARKSCEDULE").hide()
				}
			},
			P2C12_EXPORTSASCSV: function(status){
				if(status === 1){
					$("button#P2C12_EXPORTSASCSV").show()
				}else{
					$("button#P2C12_EXPORTSASCSV").hide()
				}
			},
			P2C12_CHANGE: function(status){
				if(status === 1){
					$("button.P2C12_CHANGE").show()
				}else{
					$("button.P2C12_CHANGE").hide()
				}
			}
		}
		//TOTAL OF 29
		var registrar = {
			P3C1_VIEWFULLINFORMATION: function(status){
				if(status === 1){
					$("a#P3C1_VIEWFULLINFORMATION").show()
				}else{
					$("a#P3C1_VIEWFULLINFORMATION").hide()
				}
			},
			P3C1_UPDATEINFORMATION: function(status){
				if(status === 1){
					$("a#P3C1_UPDATEINFORMATION").show()
				}else{
					$("a#P3C1_UPDATEINFORMATION").hide()
				}
			},
			P3C2_ADDSTUDENT: function(status){
				if(status === 1){
					$("button#P3C2_ADDSTUDENT").show()
				}else{
					$("button#P3C2_ADDSTUDENT").hide()
				}
			},
			P3C2_VIEWFULLINFO: function(status){
				if(status === 1){
					$("a.P3C2_VIEWFULLINFO").hide()
				}else{
					$("a.P3C2_VIEWFULLINFO").hide()
				}
			},
			P3C2_EDIT: function(status){
				if(status === 1){
					$("a.P3C2_EDIT").show()
				}else{
					$("a.P3C2_EDIT").hide()
				}
			},
			P3C2_UPDATESTUDENT: function(status){
				if(status === 1){
					$("button.P3C2_UPDATESTUDENT").show()
				}else{
					$("button.P3C2_UPDATESTUDENT").hide()
				}
			},
			P3C2_PRINTSTUDENTINFO: function(status){
				if(status === 1){
					$("a.P3C2_PRINTSTUDENTINFO").show()
				}else{
					$("a.P3C2_PRINTSTUDENTINFO").hide()
				}
			},
			P3C2_PRINTSTUDENTCLEARANCE: function(status){
				if(status === 1){
					$("a.P3C2_PRINTSTUDENTCLEARANCE").show()
				}else{
					$("a.P3C2_PRINTSTUDENTCLEARANCE").hide()
				}
			},
			P3C2_PRINTGOODMORAL: function(status){
				if(status === 1){
					$("a.P3C2_PRINTGOODMORAL").show()
				}else{
					$("a.P3C2_PRINTGOODMORAL").hide()
				}
			},
			P3C2_PRINTHONORABLEDISMISSAL: function(status){
				if(status === 1){
					$("a.P3C2_PRINTHONORABLEDISMISSAL").show()
				}else{
					$("a.P3C2_PRINTHONORABLEDISMISSAL").hide()
				}
			},
			P3C2_PRINTGRADELEDGER: function(status){
				if(status === 1){
					$("a.P3C2_PRINTGRADELEDGER").show()
				}else{
					$("a.P3C2_PRINTGRADELEDGER").hide()
				}
			},
			P3C2_PRINTDIPLOMA: function(status){
				if(status === 1){
					$("a.P3C2_PRINTDIPLOMA").show()
				}else{
					$("a.P3C2_PRINTDIPLOMA").hide()
				}
			},
			P3C2_PRINTSCHOLASTICTRAIL: function(status){
				if(status === 1){
					$("a.P3C2_PRINTSCHOLASTICTRAIL").show()
				}else{
					$("a.P3C2_PRINTSCHOLASTICTRAIL").hide()
				}
			},
			P3C3_CHOOSESUBJECTS: function(status){
				if(status === 1){
					$("a.P3C3_CHOOSESUBJECTS").show()
				}else{
					$("a.P3C3_CHOOSESUBJECTS").hide()
				}
			},
			P3C4_PRINTCLEARANCE: function(status){
				if(status === 1){
					$("a.P3C4_PRINTCLEARANCE").show()
				}else{
					$("a.P3C4_PRINTCLEARANCE").hide()
				}
			},
			P3C5_VIEW: function(status){
				if(status === 1){
					$("a.P3C5_VIEW").show()
				}else{
					$("a.P3C5_VIEW").hide()
				}
			},
			P3C6_VIEW: function(status){
				if(status === 1){
					$("a.P3C6_VIEW").show()
				}else{
					$("a.P3C6_VIEW").hide()
				}
			},
			P3C6_ADDSUBJECT: function(status){
				if(status === 1){
					$("a.P3C6_ADDSUBJECT").show()
				}else{
					$("a.P3C6_ADDSUBJECT").hide()
				}
			},
			P3C6_PRINT: function(status){
				if(status === 1){
					$("input.P3C6_PRINT").show()
				}else{
					$("input.P3C6_PRINT").hide()
				}
			},
			P3C7_VIEWGRADE: function(status){
				if(status === 1){
					$("input.P3C7_VIEWGRADE").show()
				}else{
					$("input.P3C7_VIEWGRADE").hide()
				}
			},
			P3C7_PRINTGRADESLIP: function(status){
				if(status === 1){
					$("a.P3C7_PRINTGRADESLIP").show()
				}else{
					$("a.P3C7_PRINTGRADESLIP").hide()
				}
			},
			P3C7_SEARCHAGAIN: function(status){
				if(status === 1){
					$("a.P3C7_SEARCHAGAIN").show()
				}else{
					$("a.P3C7_SEARCHAGAIN").hide()
				}
			},
			P3C8_PRINTSCHOLASTICTRAIL: function(status){
				if(status === 1){
					$("a.P3C8_PRINTSCHOLASTICTRAIL").show()
				}else{
					$("a.P3C8_PRINTSCHOLASTICTRAIL").hide()
				}
			},
			P3C9_PRINTGRADELEDGER: function(status){
				if(status === 1){
					$("a.P3C9_PRINTGRADELEDGER").show()
				}else{
					$("a.P3C9_PRINTGRADELEDGER").hide()
				}
			},
			// P3C10_: function(status){
			// 	if(status === 1){
			// 		$("").show()
			// 	}else{
			// 		$("").hide()
			// 	}
			// },
			P3C11_CHOOSESUBJECTS: function(status){
				if(status === 1){
					$("a.P3C11_CHOOSESUBJECTS").show()
				}else{
					$("a.P3C11_CHOOSESUBJECTS").hide()
				}
			},
			P3C11_CHANGEOFENROLMENT: function(status){
				if(status === 1){
					$("a.P3C11_CHANGEOFENROLMENT").show()
				}else{
					$("a.P3C11_CHANGEOFENROLMENT").hide()
				}
			},
			P3C11_STUDENTSCEDULECARD: function(status){
				if(status === 1){
					$("a.P3C11_STUDENTSCEDULECARD").show()
				}else{
					$("a.P3C11_STUDENTSCEDULECARD").hide()
				}
			},
			// P3C12_: function(status){
			// 	if(status === 1){
			// 		$("").show()
			// 	}else{
			// 		$("").hide()
			// 	}
			// },
			P3C13_PRINTGOODMORAL: function(status){
				if(status === 1){
					$("a.P3C13_PRINTGOODMORAL").show()
				}else{
					$("a.P3C13_PRINTGOODMORAL").hide()
				}
			},
			P3C14_PRINTHONORABLEDISMISSAL: function(status){
				if(status === 1){
					$("a.P3C14_PRINTHONORABLEDISMISSAL").show()
				}else{
					$("a.P3C14_PRINTHONORABLEDISMISSAL").hide()
				}
			},
		}
		//TOTAL OF 10
		var studentAffairs = {
			P4C1_VIEW: function(status){
				if(status === 1){
					$("input.P4C1_VIEW").show()
				}else{
					$("input.P4C1_VIEW").hide()
				}
			},
			P4C1_STUDENTSCHEDULECARD: function(status){
				if(status === 1){
					$("a.P4C1_STUDENTSCHEDULECARD").show()
				}else{
					$("a.P4C1_STUDENTSCHEDULECARD").hide()
				}
			},
			P4C2_ADDNEWDORM: function(status){
				if(status === 1){
					$("button.P4C2_ADDNEWDORM").show()
				}else{
					$("button.P4C2_ADDNEWDORM").hide()
				}
			},
			P4C2_EXPORTSASCSV: function(status){
				if(status === 1){
					$("button.P4C2_EXPORTSASCSV").show()
				}else{
					$("button.P4C2_EXPORTSASCSV").hide()
				}
			},
			P4C2_ADDSTUDENT: function(status){
				if(status === 1){
					$("button.P4C2_ADDSTUDENT").show()
				}else{
					$("button.P4C2_ADDSTUDENT").hide()
				}
			},
			P4C2_UPDATESTUDENT: function(status){
				if(status === 1){
					$("button.P4C2_UPDATESTUDENT").show()
				}else{
					$("button.P4C2_UPDATESTUDENT").hide()
				}
			},
			P4C3_CREATEDISCIPLINARYACTION: function(status){
				if(status === 1){
					$("button.P4C3_CREATEDISCIPLINARYACTION").show()
				}else{
					$("button.P4C3_CREATEDISCIPLINARYACTION").hide()
				}
			},
			// P4C4_: function(status){
			// 	if(status === 1){
			// 		$("").show()
			// 	}else{
			// 		$("").hide()
			// 	}
			// },
			P4C5_CHOOSESUBJECTS: function(status){
				if(status === 1){
					$("a.P4C5_CHOOSESUBJECTS").show()
				}else{
					$("a.P4C5_CHOOSESUBJECTS").hide()
				}
			},
			// P4C6_CHOOSESUBJECTS: function(status){
			// 	if(status === 1){
			// 		$("a.P4C5_CHOOSESUBJECTS").show()
			// 	}else{
			// 		$("a.P4C5_CHOOSESUBJECTS").hide()
			// 	}
			// },
			P4C7_ADDSUBJECTSECTION: function(status){
				if(status === 1){
					$("button.P4C7_ADDSUBJECTSECTION").show()
				}else{
					$("button.P4C7_ADDSUBJECTSECTION").hide()
				}
			},
			P4C8_PRINT: function(status){
				if(status === 1){
					$("a.P4C8_PRINT").show()
				}else{
					$("a.P4C8_PRINT").hide()
				}
			},
		}
		//TOTAL OF 23
		var academicAffairs = {
			P5C1_CREATENEWPROGRAM: function(status){
				if(status === 1){
					$("button.P5C1_CREATENEWPROGRAM").show()
				}else{
					$("button.P5C1_CREATENEWPROGRAM").hide()
				}
			},
			P5C1_EDIT: function(status){
				if(status === 1){
					$("a.P5C1_EDIT").show()
				}else{
					$("a.P5C1_EDIT").hide()
				}
			},
			P5C2_CREATENEWCOURSE: function(status){
				if(status === 1){
					$("button.P5C2_CREATENEWCOURSE").show()
				}else{
					$("button.P5C2_CREATENEWCOURSE").hide()
				}
			},
			P5C2_EDIT: function(status){
				if(status === 1){
					$("a.P5C2_EDIT").show()
				}else{
					$("a.P5C2_EDIT").hide()
				}
			},
			P5C3_CREATENEWSEMSTER: function(status){
				if(status === 1){
					$("button.P5C3_CREATENEWSEMSTER").show()
				}else{
					$("button.P5C3_CREATENEWSEMSTER").hide()
				}
			},
			P5C3_EDIT: function(status){
				if(status === 1){
					$("a.P5C3_EDIT").show()
				}else{
					$("a.P5C3_EDIT").hide()
				}
			},
			P5C4_ADDSEMESTERSECTION: function(status){
				if(status === 1){
					$("button.P5C4_ADDSEMESTERSECTION").show()
				}else{
					$("button.P5C4_ADDSEMESTERSECTION").hide()
				}
			},
			P5C4_EDIT: function(status){
				if(status === 1){
					$("a.P5C4_EDIT").show()
				}else{
					$("a.P5C4_EDIT").hide()
				}
			},
			P5C5_ADDCURRICULUM: function(status){
				if(status === 1){
					$("button.P5C5_ADDCURRICULUM").show()
				}else{
					$("button.P5C5_ADDCURRICULUM").hide()
				}
			},
			P5C5_VIEW: function(status){
				if(status === 1){
					$("a.P5C5_VIEW").show()
				}else{
					$("a.P5C5_VIEW").hide()
				}
			},
			P5C5_EDIT: function(status){
				if(status === 1){
					$("a.P5C5_EDIT").show()
				}else{
					$("a.P5C5_EDIT").hide()
				}
			},
			P5C5_DELETE: function(status){
				if(status === 1){
					$("a.P5C5_DELETE").show()
				}else{
					$("a.P5C5_DELETE").hide()
				}
			},
			P5C6_VIEW: function(status){
				if(status === 1){
					$("a.P5C6_VIEW").show()
				}else{
					$("a.P5C6_VIEW").hide()
				}
			},
			P5C6_ADDSUBJECT: function(status){
				if(status === 1){
					$("a.P5C6_ADDSUBJECT").show()
				}else{
					$("a.P5C6_ADDSUBJECT").hide()
				}
			},
			P5C6_PRINT: function(status){
				if(status === 1){
					$("input.P5C6_PRINT").show()
				}else{
					$("input.P5C6_PRINT").hide()
				}
			},
			P5C7_ADDROOM: function(status){
				if(status === 1){
					$("button.P5C7_ADDROOM").show()
				}else{
					$("button.P5C7_ADDROOM").hide()
				}
			},
			P5C7_EDIT: function(status){
				if(status === 1){
					$("a.P5C7_EDIT").show()
				}else{
					$("a.P5C7_EDIT").hide()
				}
			},
			P5C7_DELETE: function(status){
				if(status === 1){
					$("a.P5C7_DELETE").show()
				}else{
					$("a.P5C7_DELETE").hide()
				}
			},
			P5C8_ADDSUBJECTSECTION: function(status){
				if(status === 1){
					$("button.P5C8_ADDSUBJECTSECTION").show()
				}else{
					$("button.P5C8_ADDSUBJECTSECTION").hide()
				}
			},
			P5C9_PRINT: function(status){
				if(status === 1){
					$("a.P5C9_PRINT").show()
				}else{
					$("a.P5C9_PRINT").hide()
				}
			},
			P5C10_ADDSUBJECTSECTION: function(status){
				if(status === 1){
					$("button.P5C10_ADDSUBJECTSECTION").show()
				}else{
					$("button.P5C10_ADDSUBJECTSECTION").hide()
				}
			},
			P5C11_VIEWSCHEDULE: function(status){
				if(status === 1){
					$("a.P5C11_VIEWSCHEDULE").show()
				}else{
					$("a.P5C11_VIEWSCHEDULE").hide()
				}
			},
			// P5C12_VIEWSCHEDULE: function(status){
			// 	if(status === 1){
			// 		$("a.P5C11_VIEWSCHEDULE").show()
			// 	}else{
			// 		$("a.P5C11_VIEWSCHEDULE").hide()
			// 	}
			// },
			P5C13_ADDBLOCK: function(status){
				if(status === 1){
					$("a.P5C13_ADDBLOCK").show()
				}else{
					$("a.P5C13_ADDBLOCK").hide()
				}
			},
		}
		var accounting = {
			// P6C1_CREATENEWPROGRAM: function(status){
			// 	if(status === 1){
			// 		$("button.P5C1_CREATENEWPROGRAM").show()
			// 	}else{
			// 		$("button.P5C1_CREATENEWPROGRAM").hide()
			// 	}
			// },
			P6C2_CREATETEMPLATE: function(status){
				if(status === 1){
					$("a.P6C2_CREATETEMPLATE").show()
				}else{
					$("a.P6C2_CREATETEMPLATE").hide()
				}
			},
			P6C2_VIEW: function(status){
				if(status === 1){
					$("button.P6C2_VIEW").show()
				}else{
					$("button.P6C2_VIEW").hide()
				}
			},
			P6C4_CREATEPARTICULARS: function(status){
				if(status === 1){
					$("button.P6C4_CREATEPARTICULARS").show()
				}else{
					$("button.P6C4_CREATEPARTICULARS").hide()
				}
			},
			P6C4_EDIT: function(status){
				if(status === 1){
					$("a.P6C4_EDIT").show()
				}else{
					$("a.P6C4_EDIT").hide()
				}
			},
			P6C8_CSV: function(status){
				if(status === 1){
					$("button.P6C8_CSV").show()
				}else{
					$("button.P6C8_CSV").hide()
				}
			},
			P6C8_XLS: function(status){
				if(status === 1){
					$("button.P6C8_XLS").show()
				}else{
					$("button.P6C8_XLS").hide()
				}
			},
			P6C8_PDF: function(status){
				if(status === 1){
					$("button.P6C8_PDF").show()
				}else{
					$("button.P6C8_PDF").hide()
				}
			},
			P6C9_SAVE: function(status){
				if(status === 1){
					$("button.P6C9_SAVE").show()
				}else{
					$("button.P6C9_SAVE").hide()
				}
			},
			P6C10_UPDATE: function(status){
				if(status === 1){
					$("a.P6C10_UPDATE").show()
				}else{
					$("a.P6C10_UPDATE").hide()
				}
			},
		}
		//TOTAL OF 5
		var library = {
			P7C1_ADDABOOK: function(status){
				if(status === 1){
					$("a.P7C1_ADDABOOK").show()
				}else{
					$("a.P7C1_ADDABOOK").hide()
				}
			},
			P7C1_EXPORTSASCSV: function(status){
				if(status === 1){
					$("a.P7C1_EXPORTSASCSV").show()
				}else{
					$("a.P7C1_EXPORTSASCSV").hide()
				}
			},
			P7C1_UPDATEBOOK: function(status){
				if(status === 1){
					$("button.P7C1_UPDATEBOOK").show()
				}else{
					$("button.P7C1_UPDATEBOOK").hide()
				}
			},
			P7C2_EXPORTSASCSV: function(status){
				if(status === 1){
					$("a.P7C2_EXPORTSASCSV").show()
				}else{
					$("a.P7C2_EXPORTSASCSV").hide()
				}
			},
			P7C3_REQUESTTOBORROW: function(status){
				if(status === 1){
					$("button.P7C3_REQUESTTOBORROW").show()
				}else{
					$("button.P7C3_REQUESTTOBORROW").hide()
				}
			},
		}
		//TOTAL OF 5
		var fileMaintenance = {
			P8C1_UPDATE: function(status){
				if(status === 1){
					$("a.P8C1_UPDATE").show()
				}else{
					$("a.P8C1_UPDATE").hide()
				}
			},
			P8C2_ADDACCOUNT: function(status){
				if(status === 1){
					$("a.P8C2_ADDACCOUNT").show()
				}else{
					$("a.P8C2_ADDACCOUNT").hide()
				}
			},
			P8C2_UPDATE: function(status){
				if(status === 1){
					$("a.P8C2_UPDATE").show()
				}else{
					$("a.P8C2_UPDATE").hide()
				}
			},
			P8C5_ADDBLOCK: function(status){
				if(status === 1){
					$("a.P8C5_ADDBLOCK").show()
				}else{
					$("a.P8C5_ADDBLOCK").hide()
				}
			},
			P8C7_EDIT: function(status){
				if(status === 1){
					$("button.P8C7_EDIT").show()
				}else{
					$("button.P8C7_EDIT").hide()
				}
			},
		}

		// var access_ID = $.cookie("access_ID")
		// if(access_ID == "1"){
				
		// }else if (access_ID == "5") {
				

		// }
		



	}, 300);

	
});






