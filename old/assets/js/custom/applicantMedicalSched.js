

$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.medical = (typeof $.main.medical !=='undefined')? $.main.medical : {};

$.main.medical =(function() {
	

	// var path = "http://20.20.25.41/student-portal-backend/index.php/Magsaysay_controller/"
    // var path = "http://sms.massiveits.com/api/index.php/Magsaysay_controller/"

	 var host = window.location.protocol+"//"+window.location.hostname ;
     var path = host+"/api/index.php/Magsaysay_controller/"

    var getApplicantList = path + "getApplicantList"

	var __medical_sched = function() {
		console.log("attached events")
	  $.main.executeExternalPost(getApplicantList).done(function(result) {
	    console.log(result);
	    
	    var items = result.payload;
	    console.log(items)
	    var data = [];

	       for(i=0; i<items.length; i++){

	         var trc = $('<tr class='+result.payload[i].application_status+'>');

	       trc.append($('<td class="row1">')
	          .append(result.payload[i].applicant_fname));
	       trc.append($('<td class="row2">')
	          .append(result.payload[i].applicant_lname));
	       trc.append($('<td class="row3">')
	          .append(result.payload[i].applicant_email));
	       trc.append($('<td class="row4">')
	          .append(result.payload[i].applicant_contact));
	       trc.append($('<td class="row5">')
	          .append(result.payload[i].applicant_number));
	       trc.append($('<td class="row6">')
	          .append(result.payload[i].application_medical_sched));

	       var status = result.payload[i].application_status;

	         var ifFetchBtn;
	            if(status === "7"){
	              ifFetchBtn = "hide";
	            }
	            else{
	              ifFetchBtn = "";
	            }
	       trc.append($('<td class="row7">')
	          .append($('<button class="btn btn-danger btn-xs P2C11_MARKSCEDULE updateBtn '+ifFetchBtn+'" id="updateBtn" '
	              + ' data-aid="'+ result.payload[i].application_id +'" >Mark Schedule</button>')));


	       $('#tbody').append(trc);
	       $('#tbody').find('tr.7').remove();
	      }

	      var dataRow = $("#myTable").DataTable(
	      {
	        // 'dom' : 'Brftip',
	        'destroy': true,
	        fnDrawCallback : function()
	        {
	          $('.updateBtn').unbind('click').on('click',function(){
	  
	              $("#icpID").addClass("hide");
	              $("#success").addClass("hide");
	              $("#requiredfield").addClass("hide");
	              $("#passwordmatch").addClass("hide");
	              $("#newPass").val("");
	              $("#retypePass").val("");

	            rowID = $(this).parent().parent()
	            // console.log(rowID)
	            
	            $("#myModal").modal();
	            aid = $(this).data('aid')
	            console.log('ID ' + '= ' + aid)
	            // console.log('ID ' + '= ' + aid)
	            // $.cookie("applicant_id", aid);

	          
	            $('#saveBtn').unbind('click').on('click',function(){
	              console.log("clicked")
	              var payload = {
	                  "application_id" : aid,
	                  "application_medical_sched" : $("#datetimepicker4").val(),
	              }
	              console.log(payload);
	                $.main.executeExternalPost(path+"updateMedicalSchedule", JSON.stringify(payload)).done(function(result) {
	                  console.log(result);
	                  var status = result.status;

	                  if(status == "SUCCESS") {
	                      // console.log("complete");
	                      $("#success").removeClass("hide");
	                      setTimeout(function() {
	                      window.location.href = "medical_schedule.php"
	                      },200);

	                  }else {
	                      // console.log("Failed");
	                      $("#submitCC").removeClass("hide");
	                      $("#preloader").addClass("hide");
	                      $("#icpID").removeClass("hide");
	                      $("#submitAdminPass").removeClass("hide");
	                  }
	                })
	            })
	          })
	        },
	        "scrollY": true,
	        "bFilter": false, 
	        "bLengthChange": true,
	        'scrollX' : true,
	         'buttons': [
	               {  
	                extend: 'excelHtml5',
	                title: "Medical Schedule",download: 'open',
	                exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
	               // title: "Transaction Report " + $("#start").val(),download: 'open'
	                 /* exportOptions: {
	                      columns: [ 0, 1 ]
	                  }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
	              },{
	              
	                extend:  'pdfHtml5',
	                title: "Medical Schedule",download: 'open',
	                exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
	              },
	              {   
	                extend: 'csvHtml5',
	                title: "Medical Schedule",download: 'open',
	                exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
	              },
	             ]
	      });

	      $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
	      $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
	      $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
	      $(".dt-buttons").addClass("hidden")
	    
	  })
	}

	var __medical_result = function () {
		console.log("test")

	  $.main.executeExternalPost(getApplicantList).done(function(result) {
	    console.log(result);
	    
	    var items = result.payload;
	    console.log(items)
	    var data = [];

	       	for(i=0; i<items.length; i++){

	         	var trc = $('<tr class='+result.payload[i].application_status+'>');

	       	trc.append($('<td class="row1">')
	          .append(result.payload[i].applicant_fname + " " + result.payload[i].applicant_lname));
	       	trc.append($('<td class="row2">')
	          .append(result.payload[i].applicant_number));
            
            var status = result.payload[i].application_medical_result
            
            var step = ""
              if(status ==='1'){
                step = "Recommended"
              }else if(status === '2'){
                step = "Not Recommended"
              }

           trc.append($('<td class="row3">')
             .append(step));
           trc.append($('<td class="row4">')
             .append($('<button class="btn btn-danger btn-xs P2C12_CHANGE updateBtn" id="updateBtn" '
                  + ' data-aid="'+ result.payload[i].application_id +'" >Change</button>')));
                  // + ' data-uid="'+ result.payload[i].applicant_id +'" >Reset Exam</button>')))

	       $('#tbody').append(trc);
	       $('#tbody').find('tr.7').remove();
	      }

	      var dataRow = $("#myTable").DataTable(
	      {
	        // 'dom' : 'Brftip',
	        'destroy': true,
            fnDrawCallback : function()
            {
              $('.updateBtn').unbind('click').on('click',function(){
                    
                  $("#icpID").addClass("hide");
                  $("#success").addClass("hide");
                  $("#requiredfield").addClass("hide");
                  $("#passwordmatch").addClass("hide");
                  $("#newPass").val("");
                  $("#retypePass").val("");

                rowID = $(this).parent().parent()
                // console.log(rowID)
                
                $("#myModal").modal();
                aid = $(this).data('aid')
                console.log('ID ' + '= ' + aid)
                // console.log('ID ' + '= ' + aid)
                // $.cookie("applicant_id", aid);

				$('#saveBtn').unbind('click').on('click',function(){
				console.log("clicked")

                  var payload = {
                      "application_id" : aid,
                      "application_medical_result" : $("#statusID").val(),
                  }
                  console.log(payload);
                    $.main.executeExternalPost(path+"updateMedicalResult", JSON.stringify(payload)).done(function(result) {
                      console.log(result);
                      var status = result.status;
                      // console.log(result.workgroup_id);

                      if(status == "SUCCESS") {

	                  	var payload = {
						   "applicant_id":aid,
						   "applicant_status":"-5"
						}
                  		console.log(payload);
                      	$.main.executeExternalPost(path+"updateApplicantStatus1", JSON.stringify(payload)).done(function(result) {
	                      console.log(result);
	                      var status = result.status;
	                      if(status == "SUCCESS") {
	                          // console.log("complete");
	                          $("#success").removeClass("hide");
	                          setTimeout(function() {
	                          window.location.href = "medical_result.php"
	                          },200);

	                      }
                      else {
                          // console.log("Failed");
                          $("#submitCC").removeClass("hide");
                          $("#preloader").addClass("hide");
                          $("#icpID").removeClass("hide");
                          $("#submitAdminPass").removeClass("hide");
                      }
                		})
                      }
                    })
                })
              })
            },
	        "scrollY": true,
	        "bFilter": false, 
	        "bLengthChange": true,
	        'scrollX' : true,
	         'buttons': [
	               {  
	                extend: 'excelHtml5',
	                title: "Medical Result",download: 'open',
	                exportOptions: {columns: [ 0, 1,2] }
	               // title: "Transaction Report " + $("#start").val(),download: 'open'
	                 /* exportOptions: {
	                      columns: [ 0, 1 ]
	                  }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
	              },{
	              
	                extend:  'pdfHtml5',
	                title: "Medical Result",download: 'open',
	                exportOptions: {columns: [ 0, 1,2] }
	              },
	              {   
	                extend: 'csvHtml5',
	                title: "Medical Result",download: 'open',
	                exportOptions: {columns: [ 0, 1,2] }
	              },
	             ]
	      });

	      $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
	      $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
	      $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
	      $(".dt-buttons").addClass("hidden")
	    
	  })
	}	

    return {
        medical_sched : __medical_sched,
        medical_result : __medical_result
    };

}());
