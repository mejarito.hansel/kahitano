$ = (typeof $ !== 'undefined') ? $ : {};
$.main = (typeof $.main !=='undefined')? $.main: {};
$.main.accesscontrol = (typeof $.main.accesscontrol !=='undefined')? $.main.accesscontrol : {};

$.main.accesscontrol = (function(){
	var host = window.location.protocol+"//"+window.location.hostname ;
	var path = host+"/api/index.php/Ams_controller/";
	// var path = "http://20.20.25.41/student-portal-backend/index.php/Ams_controller/"
	$(".alert-usermanagement").hide();

	var __displayAccountLevel = function() {

	    $("#tableUserManagement").on("click", ".btnEditUser", function(){

			$("#modalUserManagement").modal("show");
			var payload = {
				// "account_ID": $(this).closest("tr").find("td:first").text()
				"account_ID": $(this).attr("data-id")
			}
			$.main.executeExternalPost(path+"fetchAccountByAccountID", JSON.stringify(payload)).done(function(result){
				if(result.status === "SUCCESS"){

					$("#modalUserManagement").find(".modal-body").html("<div class='alert alert-danger hidden' role='alert'><span class='form-text hidden'></span></div><div class='media'>\
					  <a class='pull-left' href='#' style='margin-right: 3rem;'>\
					    <img class='img img-thumbnail' src='assets/images/avatar.png' alt='avatar' style='width: 15rem;'>\
					  </a>\
					  <div class='media-body' style='margin-top: .9rem; line-height: 2.5rem'>\
					  	<table><tbody>\
					  	<tr hidden><td class='text-sm'>Account ID: </td><td><input id='account_ID' class='form-control input-sm' type='text' value='"+result.payload.account_ID+"' disabled></td></tr>\
					  	<tr><td class='text-sm'>Employee ID: </td><td><input id='employee_NO' class='form-control input-sm' type='text' value='"+result.payload.employee_NO+"'></td></tr>\
					  	<tr><td class='text-sm'>First name: <span class='text-danger small'>*</span></td><td><input id='account_FNAME' class='form-control input-sm' type='text' value='"+result.payload.account_FNAME+"'></td></tr>\
					  	<tr><td class='text-sm'>Middle name: <small>(optional)</small></td><td><input id='account_MNAME' class='form-control input-sm' type='text' value='"+result.payload.account_MNAME+"'></td></tr>\
					  	<tr><td class='text-sm'>Last name: <span class='text-danger small'>*</span></td><td><input id='account_LNAME' class='form-control input-sm' type='text' value='"+result.payload.account_LNAME+"'></td></tr>\
					  	<tr><td class='text-sm'>Password: <small>(optional)</small></td><td><input id='account_PASS' class='form-control input-sm' type='password' placeholder='Enter Password'></td></tr>\
					  	<tr><td class='text-sm'>Username: <span class='text-danger small'>*</span></td><td><input id='account_UNAME' class='form-control input-sm' type='text' value='"+result.payload.account_UNAME+"'></td></tr>\
					  	<tr><td class='text-sm'>Department Name: <span class='text-danger small'>*</span></td><td><select id='department_ID' class='form-control input-sm'><option val='0' data-id='0'>-- Select Department Level --</option></select></td></tr>\
					  	<tr><td class='text-sm'>Access Level: <span class='text-danger small'>*</span></td><td><select id='access_ID' class='form-control input-sm'><option val='0' data-id='0'>-- Select User Access Level --</option></select></td></tr>\
					  	</tbody></table>\
					  </div>\
					</div>");
					  	// <tr><td class='text-sm'>Status: <span class='text-danger small'>*</span></td><td><select id='accessStatus' class='form-control input-sm'><option val='1' data-id='1'>Active</option><option val='2' data-id='2'>Inactive</option></select></td></tr>\


					// var selectedStatus = result.payload.access_status
					// var stats = $("#accessStatus option[data-id='"+selectedStatus+"']").text()
					// $("#accessStatus").val(stats)

					var selectedDepartmentLevel = result.payload.department_ID
					$.main.executeExternalGet(path+"fetchAllDepartmentList").done(function(result){
					if(result.status === "SUCCESS"){	
							for(i=0;i<result.payload.length;i++){
								$("#department_ID").append("<option val="+result.payload[i].department_ID+" data-id="+result.payload[i].department_ID+">"+result.payload[i].department_NAME+"</option>")
							}
							$("#department_ID option[data-id='"+selectedDepartmentLevel+"']").hide()
							var getDeptName = $("#department_ID option[data-id='"+selectedDepartmentLevel+"']").text()
							$("#department_ID").val(getDeptName);
						}
					});
					var selectedAccessLevel = result.payload.access_ID
					$.main.executeExternalGet(path+"fetchAllAccountLevel").done(function(result){
					if(result.status === "SUCCESS"){	
							for(i=0;i<result.payload.length;i++){
								$("#access_ID").append("<option val="+result.payload[i].access_ID+" data-id="+result.payload[i].access_ID+">"+result.payload[i].access_NAME+"</option>")
							}
							$("#access_ID option[data-id='"+selectedAccessLevel+"']").hide()
							var getAccessName = $("#access_ID option[data-id='"+selectedAccessLevel+"']").text()
							$("#access_ID").val(getAccessName);
						}
					});
					$("#modalUserManagement").on("click", "#btnSaveAccountInfo", function(){
						applicant.formValidation();
						// var payload = {
						// 	"account_ID": $("#account_ID").val(),
						// 	"account_FNAME": $("#account_FNAME").val(),
						// 	"account_MNAME": $("#account_MNAME").val(),
						// 	"account_LNAME": $("#account_LNAME").val(),
						// 	"account_UNAME": $("#account_UNAME").val(),
						// 	"account_PASS": $("#account_PASS").val(),
						// 	"department_ID": $("#department_ID option:selected").data("id"),
						// 	"access_ID": $("#access_ID option:selected").data("id")
						// }
						// $.main.executeExternalPost(path+"updateAccountInfo", JSON.stringify(payload)).done(function(result){
						// 	if(result.status === "SUCCESS"){
						// 		$("#modalUserManagement").modal("hide");
						// 		$(".alert-usermanagement").removeClass("hidden");
						// 		$(".alert-usermanagement").slideDown("1000").delay(2000).slideUp("1000");
						// 		$("#upt-fullname").text(result.payload.account_LNAME+", "+result.payload.account_FNAME+" "+result.payload.account_MNAME)
						// 		__reloadTable();
						// 		setTimeout(function(){
						// 		    location.reload();
						// 		},2000);
						// 	}
						// });	
					});
				}
			});
			//$("#modalUserManagement").find(".modal-body").html("<img class='img img-thumbnail text-center' src='assets/images/avatar.png' style='width:15rem;'/>");
			
	    });

		$.main.executeExternalGet(path+"fetchAllAccount").done(function(result){
			console.log(result)
			if(result.status === "SUCCESS"){
				if ( $.fn.DataTable.isDataTable('#tableUserManagement') ) {
			        $('#tableUserManagement').DataTable().destroy();
			        $('#tableUserManagement tbody').empty();
			      }
			      var data = [];
			      for ( i = 0; i < result.payload.length; i++) {
			        var subdata = {};
			        var btnEdit = "<a data-toggle='modal' class='pl-0 btn btn-xs btn-info btn-block P8C2_UPDATE btnEditUser' data-id='" + result.payload[i].account_ID + "'>Update</a>\t";
			        // var btnDelete = "<a data-toggle='modal' class='btnDelete' data-id='" + result.payload[i].user_id + "' data-target='#modalDelete'><i id='btnDelete' class='fas fa-trash'></i></a>";
			        subdata['account_ID'] = result.payload[i].employee_NO;
			        subdata['firstname'] = result.payload[i].account_FNAME;
			        subdata['lastname'] = result.payload[i].account_LNAME;
			        subdata['accusername'] = result.payload[i].account_UNAME;
			        subdata['access_name'] = result.payload[i].access_NAME;
			        // if(result.payload[i].access_status === "1"){
			        // 	var status = "Active";
			        // }else{
			        // 	var status = "Inactive"
			        // }

			        // subdata['status'] = status;
			        // if(result.payload[i].access_ID === "1"){
			        // 	var username = "ADMINISTRATOR";
			        // }else if(result.payload[i].access_ID === "2"){
			        // 	var username = "LIBRARIAN";
			        // }else if(result.payload[i].access_ID === "3"){
			        // 	var username = "ADMISSION STAFF";
			        // }else if(result.payload[i].access_ID === "4"){
			        // 	var username = "ACCOUNTING STAFF";
			        // }else if(result.payload[i].access_ID === "5"){
			        // 	var username = "ACADEMIC SECRETARY";
			        // }else if(result.payload[i].access_ID === "6"){
			        // 	var username = "STUDENT AFFAIRS OFFICER";
			        // }else if(result.payload[i].access_ID === "7"){
			        // 	var username = "REGISTRAR";
			        // }else if(result.payload[i].access_ID === "8"){
			        // 	var username = "INSTRUCTOR";
			        // }else{
			        // 	var username = "Unknown"
			        // }
			        // subdata['accessID'] = username;
			        subdata['action'] = btnEdit;
			        data.push(subdata);
			      }
			  
			      $("#tableUserManagement").DataTable({      
			      'data' : data,
			      "lengthChange": true,
			      'bFilter':false,
			      'columns' : [
			          {"data":"account_ID"},
			          {"data":"firstname"},
			          {"data":"lastname"},
			          {"data":"accusername"},
			          {"data":"access_name"},
			          // {"data":"status"},
			          // {"data":"accessID"},
			          {"data":"action"}
			      ]
			    });
			}else{
			}
		});

		// var payload = [{"access_name":"account_ID"}];
		// for(i=0;i<payload.length;i++){
		// 	$("#selectAccountLevel").append("<option value="+(i+1)+">"+payload[i].access_name+"</option>");
		// }

		$('#tableUserManagement').DataTable( {
	        columnDefs: [ {
	            targets: [ 0 ],
	            orderData: [ 0, 1 ]
	        }, {
	            targets: [ 1 ],
	            orderData: [ 1, 0 ]
	        }, {
	            targets: [ 3 ],
	            orderData: [ 4, 0 ]
	        } ]
	    } );
	};
	var applicant = {
        formValidation: function(){
        	
           	if(applicant.firstname($("#account_FNAME").val())&&applicant.lastname($("#account_LNAME").val())&&applicant.department($("#department_ID option:selected").data('id'))&&applicant.accesslevel($("#access_ID option:selected").data('id')) === true){
            
                // alert("Register Successfully");
                applicant.registerApplicant();
                return true;
            }else{
                // $("#loaderDiv").fadeOut("1000");
                $(".alert.alert-danger.hidden").removeClass('hidden');
                // alert($("#account_FNAME").val().match(/^\s+/));
                // alert("Invalid");
                return false;
            }
        },
        registerApplicant: function(){
            var payload = {
				"account_ID": $("#account_ID").val(),
				"employee_NO": $("#employee_NO").val(),
				"account_FNAME": $("#account_FNAME").val(),
				"account_MNAME": $("#account_MNAME").val(),
				"account_LNAME": $("#account_LNAME").val(),
				"account_UNAME": $("#account_UNAME").val(),
				"account_PASS": $("#account_PASS").val(),
				"department_ID": $("#department_ID option:selected").data("id"),
				"access_ID": $("#access_ID option:selected").data("id"),
				// "access_status": $("#accessStatus option:selected").data("id")
				"account_ACTIVE": 1
			};
			console.log(payload)
            $.main.executeExternalPost(path+"updateAccountInfo", JSON.stringify(payload)).done(function(result){
                if (result.status === "SUCCESS") {
                    $("#modalUserManagement").modal("hide");
					$(".alert-usermanagement").removeClass("hidden");
					$(".alert-usermanagement").slideDown("1000").delay(2000).slideUp("1000");
					$("#upt-fullname").text(result.payload.account_FNAME+" "+result.payload.account_MNAME+" "+result.payload.account_LNAME)
					__reloadTable();
					setTimeout(function(){
					    location.reload();
					},2000);
                }
            });
        },

        firstname: function(firstname){
            var pattern = /^[a-zA-Z\s]+$/;
            var pattern1 = /^\s+/;
            var firstname = firstname;
            if (pattern.test(firstname) && firstname !== "") {
            	$(".alert.alert-danger").addClass('hidden');
            	var test = $("#account_FNAME").val()
            	var a = $.trim(test);
				$("#account_FNAME").val(a)
                return true;
            }else if(firstname.match(/^\s+/) == true){
            	alert("may space")
            	$(".alert.alert-danger.hidden").removeClass('hidden');
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden');
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden').html('may space');
                return false;
            }else if(firstname === ""){
            	$(".alert.alert-danger.hidden").removeClass('hidden');
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden');
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden').html('* This field is required');
                return false;
            }else {
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden');
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden').html('Please input a valid name');
                return false;
            }
        },
        lastname: function(lastname){
            var pattern = /^[a-zA-Z\s]+$/;
            var pattern1 = /^\s+/;
            var lastname = lastname;
            if (pattern.test(lastname) && lastname !== "") {
            	$(".alert.alert-danger").addClass('hidden');
            	var test = $("#account_LNAME").val()
            	var a = $.trim(test);
				$("#account_LNAME").val(a)
                return true;
            }else if(lastname.match(/^\s+/) == true){
            	alert("may space")
            	$(".alert.alert-danger.hidden").removeClass('hidden');
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden');
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden').html('may space');
                return false;
            }else if(lastname === ""){
            	$(".alert.alert-danger.hidden").removeClass('hidden');
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden');
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden').html('* This field is required');
                return false;
            }else {
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden');
            	$(".alert.alert-danger").find('.form-text').removeClass('hidden').html('Please input a valid name');
                return false;
            }
        },
        department: function(department){
        	if(department === 0){
        		// $(".alert.alert-danger.hidden").append("<span class='alert-department'>* Please select an department</span>");
        		return false;
        	}else{
        		return true;
        	}
        },
        accesslevel: function(accesslevel){
        	if(accesslevel === 0){
        		// $(".alert.alert-danger.hidden").append("<p>* Please select an access level</p>");
        		return false;
        	}else{
        		return true;
        	}
        }
    }
	var __reloadTable = function() {
		
		$.main.executeExternalGet(path+"fetchAllAccount").done(function(result){
			if(result.status === "SUCCESS"){
				if ( $.fn.DataTable.isDataTable('#tableUserManagement') ) {
			        $('#tableUserManagement').DataTable().destroy();
			        $('#tableUserManagement tbody').empty();
			      }
			      var data = [];
			      for ( i = 0; i < result.payload.length; i++) {
			        var subdata = {};
			        var btnEdit = "<a data-toggle='modal' class='pl-0 btn btn-xs btn-info btn-block P8C2_UPDATE btnEditUser' data-id='" + result.payload[i].account_ID + "'>Update</a>\t";
			        // var btnDelete = "<a data-toggle='modal' class='btnDelete' data-id='" + result.payload[i].user_id + "' data-target='#modalDelete'><i id='btnDelete' class='fas fa-trash'></i></a>";
			        subdata['account_ID'] = result.payload[i].employee_NO;
			        subdata['firstname'] = result.payload[i].account_FNAME;
			        subdata['lastname'] = result.payload[i].account_LNAME;
			        subdata['accusername'] = result.payload[i].account_UNAME;
			        subdata['access_name'] = result.payload[i].access_NAME;
			        // if(result.payload[i].access_status === "1"){
			        // 	var status = "Active";
			        // }else{
			        // 	var status = "Inactive"
			        // }

			        // subdata['status'] = status;
			        // if(result.payload[i].access_ID === "1"){
			        // 	var username = "ADMINISTRATOR";
			        // }else if(result.payload[i].access_ID === "2"){
			        // 	var username = "LIBRARIAN";
			        // }else if(result.payload[i].access_ID === "3"){
			        // 	var username = "ADMISSION STAFF";
			        // }else if(result.payload[i].access_ID === "4"){
			        // 	var username = "ACCOUNTING STAFF";
			        // }else if(result.payload[i].access_ID === "5"){
			        // 	var username = "ACADEMIC SECRETARY";
			        // }else if(result.payload[i].access_ID === "6"){
			        // 	var username = "STUDENT AFFAIRS OFFICER";
			        // }else if(result.payload[i].access_ID === "7"){
			        // 	var username = "REGISTRAR";
			        // }else if(result.payload[i].access_ID === "8"){
			        // 	var username = "INSTRUCTOR";
			        // }else{
			        // 	var username = "Unknown"
			        // }
			        // subdata['accessID'] = username;
			        subdata['action'] = btnEdit;
			        data.push(subdata);
			      }
			  
			      $("#tableUserManagement").DataTable({      
			      'data' : data,
			      // "lengthChange": false,
			      'bFilter':false,
			      'columns' : [
			          {"data":"account_ID"},
			          {"data":"firstname"},
			          {"data":"lastname"},
			          {"data":"accusername"},
			          {"data":"access_name"},
			          // {"data":"status"},
			          {"data":"action"}
			      ]
			    });
			}else{
			}
		});
	};
	var __createAccountInfo = function() {
		$("div.col-md-9").on("click", ".btnAddAcc", function(){
			$.main.executeExternalGet(path+"fetchAllDepartmentList").done(function(result){
			console.log(result)
			if(result.status === "SUCCESS"){	
					for(i=0;i<result.payload.length;i++){
						$("#department_ID1").append("<option val="+result.payload[i].department_ID+" data-id="+result.payload[i].department_ID+">"+result.payload[i].department_NAME+"</option>")
					}
				}
			});
			$.main.executeExternalGet(path+"fetchAllAccountLevel").done(function(result){
			if(result.status === "SUCCESS"){	
					for(i=0;i<result.payload.length;i++){
						$("#access_ID1").append("<option val="+result.payload[i].access_ID+" data-id="+result.payload[i].access_ID+">"+result.payload[i].access_NAME+"</option>")
					}
				}
			});
			$("#modalCreateUserManagement").modal("show");
			$("#modalCreateUserManagement").find(".modal-body").html("<div class='media'>\
				  <a class='pull-left' href='#' style='margin-right: 3rem;'>\
				    <img class='img img-thumbnail' src='assets/images/avatar.png' alt='avatar' style='width: 15rem;'>\
				  </a>\
				  <div class='media-body' style='margin-top: .9rem; line-height: 2.5rem'>\
				  	<table><tbody>\
				  	<tr><td class='text-sm'>ID Number:</td><td><div class='input-group'><span class='input-group-addon input-sm' id='basic-addon3'>MMMA-</span><input id='account_IDNO' class='form-control input-sm' type='text' required='' placeholder='YYYY-0000'></div></td></tr>\
				  	<tr><td class='text-sm'>First name:</td><td><input id='account_FNAME' class='form-control input-sm' type='text' required></td></tr>\
				  	<tr><td class='text-sm'>Middle name:</td><td><input id='account_MNAME' class='form-control input-sm' type='text' required></td></tr>\
				  	<tr><td class='text-sm'>Last name:</td><td><input id='account_LNAME' class='form-control input-sm' type='text' required></td></tr>\
				  	<tr><td class='text-sm'>Password:</td><td><input id='account_PASS' class='form-control input-sm' type='password' required><span toggle='#account_PASS' class='fa fa-fw fa-eye field-icon toggle-password'></span></td></tr>\
				  	<tr><td class='text-sm'>Username:</td><td><input id='account_UNAME' class='form-control input-sm' type='text' required></td></tr>\
				  	<tr><td class='text-sm'>Department Name:</td><td><select id='department_ID1' class='form-control input-sm' data-id='0' required><option val='0'>-- Select Department Level --</option></select></td></tr>\
				  	<tr><td class='text-sm'>Access Level:</td><td><select id='access_ID1' class='form-control input-sm' data-id='0' required><option val='0'>-- Select User Access Level --</option></select></td></tr>\
				  	</tbody></table>\
				  </div>\
				</div>");
		});		
		$("#modalCreateUserManagement").on("click",".toggle-password",function() {
			// alert("working")
		  $(this).toggleClass("fa-eye fa-eye-slash");
		  var input = $($(this).attr("toggle"));
		  if (input.attr("type") == "password") {
		    input.attr("type", "text");
		  } else {
		    input.attr("type", "password");
		  }
		});
		function input1() {
			if($("#account_IDNO").val() == ""){
				return false;
			}else{
				return true;
			}
		}
		function input2() {
			if($("#account_FNAME").val() == ""){
				return false;
			}else{
				return true;
			}
		}
		function input4() {
			if($("#account_LNAME").val() == ""){
				return false;
			}else{
				return true;
			}
		}
		function input5() {
			if($("#account_PASS").val() == ""){
				return false;
			}else{
				return true;
			}
		}
		function input6() {
			if($("#account_UNAME").val() == ""){
				return false;
			}else{
				return true;
			}
		}
		function input7(){
			if($("select#department_ID1").val() == "-- Select Department Level --"){
				return false;
			}else{
				return true;
			}
		}
		function input8(){
			if($("select#access_ID1").val() == "-- Select User Access Level --"){
				return false;
			}else{
				return true;
			}
		}
		function validateAllInput(){
			if(input1() && input2() && input4() && input5() && input6() && input7() && input8()){
				return true;
			}else{
				return false;
			}
		}
		$("#modalCreateUserManagement").find(".alert-createusermanagement").slideUp("0")
		$("#btnCreateUser").on("click", function(){
			if(validateAllInput() === false){
				$("#modalCreateUserManagement").find(".alert-createusermanagement").slideDown("1000").delay(2000).slideUp("1000");
			}else{
				var payload = {
					"employee_NO": "MMMA-"+$("#account_IDNO").val(),
					"RFID":null,
					"account_FNAME": $("#account_FNAME").val(),
					"account_MNAME": $("#account_MNAME").val(),
					"account_LNAME": $("#account_LNAME").val(),
					"account_UNAME": $("#account_UNAME").val(),
					"account_PASS": $("#account_PASS").val(),
					"department_ID": $("#department_ID1 option:selected").data("id"),
					"access_ID": $("#access_ID1 option:selected").data("id")
				}
				$.main.executeExternalPost(path+"createAccountInfo", JSON.stringify(payload)).done(function(result){
					console.log(result)
					if(result.status === "SUCCESS"){
						$("#modalCreateUserManagement").modal("hide");
							$(".alert-usermanagement").removeClass("hidden");
							$(".alert-usermanagement").find("p").html("Record successfully created");
							$(".alert-usermanagement").slideDown("1000").delay(2000).slideUp("1000");
							__reloadTable();
							setTimeout(function(){
							    location.reload();
							},2000);
					}else{
						
					}
				});
			}
		});
	};
    var __accountControl = function() {

		$.main.executeExternalGet(path+"fetchAllAccountLevel").done(function(result){
    	var items = result.payload;
        // console.log(items)

		if ( $.fn.DataTable.isDataTable('#myTable') ) {
	        $('#myTable').DataTable().destroy();
	        $('#myTable tbody').empty();
	      }
        	var data = [];

           for(i=0; i<items.length; i++){

             var trc = $('<tr>');

           trc.append($('<td class="row1"'
           		+ ' data-aid="'+ result.payload[i].access_ID +'"'
           		+ ' data-an="'+ result.payload[i].access_NAME +'">')
             	.append(result.payload[i].access_NAME));
           // trc.append($('<td class="row2">')
             	// .append($('<button class="btn btn-success btn-xs status" id="status" '
          		// + ' data-aid="'+ result.payload[i].access_ID +'">On</button>')));
           trc.append($('<td class="row3">')
             	.append($('<button class="btn btn-success btn-xs edit P8C7_EDIT" id="edit" style=" padding: 3px 17px;" '
              	+ ' data-aid="'+ result.payload[i].access_ID +'"'
              	+ ' data-an="'+ result.payload[i].access_NAME +'">Edit</button>')));

           $('#tbody').append(trc);

          }

          var dataRow = $("#myTable").DataTable(
          {
            'dom' : 'Brftip',
            'destroy': true,
            fnDrawCallback : function()
            {
              	$('.edit').unbind('click').on('click',function(){
            
                  	$("#icpID").addClass("hide");
                  	$("#success").addClass("hide");
                  	$("#requiredfield").addClass("hide");
                  	$("#passwordmatch").addClass("hide");
                  	$("#newPass").val("");
                  	$("#retypePass").val("");

	                rowID = $(this).parent().parent()
	                // console.log(rowID)
	                
	                $("#myModal").modal();
	                aid = $(this).data('aid')
	                console.log('ID ' + '= ' + aid)
	                an = $(this).data('an')
	                console.log('an ' + '= ' + an)

	                $("#userTypeName").val(an)

              	})
              	$('#edibtn').unbind('click').on('click',function(){
              		// console.log("clicked")
	                
	                var payload = {
	                	"access_ID" : aid,
	                	"access_NAME" : $("#userTypeName").val()
	                }
	    			if (payload.access_NAME == "" || payload.access_NAME == null) {
						$("#reqErrorEdit").removeClass("hide")
	                	$("#success").addClass("hide")
	                	$("#exst2").addClass("hide")

	    			}else{
						$.main.executeExternalPost(path+"updatedAccountLevel",JSON.stringify(payload)).done(function(result){
		                console.log(result)
			                if (result.status == "SUCCESS") {
			                	$("#success").removeClass("hide")
			                	$("#exst2").addClass("hide")
								$("#reqErrorEdit").addClass("hide")
		                		__accountControl()

						        setTimeout(function() {
									window.location.reload();
						        },200);
			                }else{
								$("#reqErrorEdit").addClass("hide")
			                	$("#success").addClass("hide")
			                	$("#exst2").removeClass("hide")

			                }
		                })
	                }
              	})
              	// $('.status').unbind('click').on('click',function(){
              	// 	console.log("clicked")
              	// })
              	$(".row1").unbind('click').on('click',function(){
              		// console.log("clicked")
	                aid = $(this).data('aid')
	                an = $(this).data('an')
					$( ".treeview" ).empty();
					$("#btnModule").addClass("hide")
	                __loadModules(aid,an);

              	})
            },
            "scrollY": true,
            "bFilter": false, 
            "bLengthChange": false,
            'scrollX' : true,
             'buttons': [
                   {  
                    extend: 'excelHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
                   // title: "Transaction Report " + $("#start").val(),download: 'open'
                     /* exportOptions: {
                          columns: [ 0, 1 ]
                      }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
                  },{
                  
                    extend:  'pdfHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
                  },
                  {   
                    extend: 'csvHtml5',
                    title: "Exam Result",download: 'open',
                    exportOptions: {columns: [ 0, 1,2,3,4,5 ] }
                  },
                 ]
          });

          $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
          $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
          $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })
          $(".dt-buttons").addClass("hidden")


    	});



    };
    var __loadModules = function(aid,an) {
    	// console.log("load modules");

    	$(".userTypes").removeClass("hide")
    	$("#btnModule").removeClass("hide")
        access_name = an
        $("#user_type_title").html(access_name + " PERMISSIONS")
        access_ID = aid
        // console.log(access_ID)
        var payload = {
        	"access_ID" : access_ID
        }
		$.main.executeExternalPost(path+"fetAllUserAccountLevelControlByAccessID" , JSON.stringify(payload)).done(function(result){
    		// console.log(result)
    		item = result.payload
    		// console.log(item)

			for(i=0;i<item.length;i++){
				if (item[i].status == "1") {
					check_status = "checked"
					status = "custom-checked"
				}else{
					status = "custom-unchecked"
					check_status = ""
				}

	    	$(".scrollbox-content ul.treeview").append('<li class="module" data-uaid="'+item[i].user_account_ID+'" id="account-'+item[i].USER_TYPE_ID+'"><input type="checkbox" name="'+item[i].USER_TYPE_DESCRIPTION+'" id="'+item[i].USER_TYPE_DESCRIPTION+'" '+check_status+'><label for="'+item[i].USER_TYPE_DESCRIPTION+'" class="'+status+'">'+item[i].USER_TYPE_DESCRIPTION+'</label><ul></li>');

	    	}

	    	$("#btnModule").unbind('click').on('click',function(){
	    		$("#btnModule").addClass("hide");
	    		for(i=0;i<$(".module").length;i++){
	    			var parent_id = $(".module").eq(i).data("uaid")
	    			if($(".module").eq(i).find("input").is(":checked") === true){
	    				status = "1"
	    			}else{
	    				status = "2"
	    			}
	    			var payload = {
	    				"user_account_ID": parent_id,
	    				"status": status
	    			}
	    			// console.log(payload)
					$.main.executeExternalPost(path+"updateUserTypesStatus" , JSON.stringify(payload)).done(function(result){
	    				// console.log(result)
	    			})

	    			for(x=0;x<$(".module").eq(i).find("ul li").length;x++){
	    				var child_id = $(".module").eq(i).find("ul li input").eq(x).attr("id")
	    				if($(".module").eq(i).find("ul li input").eq(x).is(":checked") === true){
	    					var status = "1"
	    				}else{
	    					var status = "2"
	    				}
		    			var payload = {
		    				"UALCM_ID": child_id,
		    				"status": status
		    			}
		    				console.log(payload)
						$.main.executeExternalPost(path+"updateUserTypesModuleStatus" , JSON.stringify(payload)).done(function(result){
		    				console.log(result)
	    				$("#btnModule").removeClass("hide");
						$("#successxd").removeClass("hide");
						setTimeout(function(){
							window.location.reload();
						},2000);
		    			})
	    			}
	    		}
	    	
			})

	    	__fetchsubmodule()

		})
    }
    var __fetchsubmodule = function() {


        var payload = {
        	"access_ID" : access_ID
        }

		$.main.executeExternalPost(path+"fetchAllUserModuleByUserTypeID" , JSON.stringify(payload)).done(function(result){
    		items = result.payload
			for(i=0;i < items.length; i++){
				if (items[i].status == "1") {
					check_status = "checked"
					status = "custom-checked"
				}else{
					status = "custom-unchecked"
					check_status = ""
				}

				if (items[i].USER_TYPE_ID == 1) {
    				$(".scrollbox-content ul li ul").eq(0).append('<li class="submodule" data-uaid="'+items[i].UALCM_ID+'"><input type="checkbox" name="'+items[i].USER_ACCOUNT_MODULE+'" id="'+items[i].UALCM_ID+'"  '+check_status+'><label for="'+items[i].UALCM_ID+'" class="'+ status +'">'+items[i].USER_ACCOUNT_MODULE+'</label></li>');
				}if (items[i].USER_TYPE_ID == 2) {
    				$(".scrollbox-content ul li ul").eq(1).append('<li class="submodule" data-uaid="'+items[i].UALCM_ID+'"><input type="checkbox" name="'+items[i].USER_ACCOUNT_MODULE+'" id="'+items[i].UALCM_ID+'"  '+check_status+'><label for="'+items[i].UALCM_ID+'" class="'+ status +'">'+items[i].USER_ACCOUNT_MODULE+'</label></li>');
				}if (items[i].USER_TYPE_ID == 3) {
    				$(".scrollbox-content ul li ul").eq(2).append('<li class="submodule" data-uaid="'+items[i].UALCM_ID+'"><input type="checkbox" name="'+items[i].USER_ACCOUNT_MODULE+'" id="'+items[i].UALCM_ID+'"  '+check_status+'><label for="'+items[i].UALCM_ID+'" class="'+ status +'">'+items[i].USER_ACCOUNT_MODULE+'</label></li>');
				}if (items[i].USER_TYPE_ID == 4) {
    				$(".scrollbox-content ul li ul").eq(3).append('<li class="submodule" data-uaid="'+items[i].UALCM_ID+'"><input type="checkbox" name="'+items[i].USER_ACCOUNT_MODULE+'" id="'+items[i].UALCM_ID+'"  '+check_status+'><label for="'+items[i].UALCM_ID+'" class="'+ status +'">'+items[i].USER_ACCOUNT_MODULE+'</label></li>');
				}if (items[i].USER_TYPE_ID == 5) {
    				$(".scrollbox-content ul li ul").eq(4).append('<li class="submodule" data-uaid="'+items[i].UALCM_ID+'"><input type="checkbox" name="'+items[i].USER_ACCOUNT_MODULE+'" id="'+items[i].UALCM_ID+'"  '+check_status+'><label for="'+items[i].UALCM_ID+'" class="'+ status +'">'+items[i].USER_ACCOUNT_MODULE+'</label></li>');
				}if (items[i].USER_TYPE_ID == 6) {
    				$(".scrollbox-content ul li ul").eq(5).append('<li class="submodule" data-uaid="'+items[i].UALCM_ID+'"><input type="checkbox" name="'+items[i].USER_ACCOUNT_MODULE+'" id="'+items[i].UALCM_ID+'"  '+check_status+'><label for="'+items[i].UALCM_ID+'" class="'+ status +'">'+items[i].USER_ACCOUNT_MODULE+'</label></li>');
				}if (items[i].USER_TYPE_ID == 7) {
    				$(".scrollbox-content ul li ul").eq(6).append('<li class="submodule" data-uaid="'+items[i].UALCM_ID+'"><input type="checkbox" name="'+items[i].USER_ACCOUNT_MODULE+'" id="'+items[i].UALCM_ID+'"  '+check_status+'><label for="'+items[i].UALCM_ID+'" class="'+ status +'">'+items[i].USER_ACCOUNT_MODULE+'</label></li>');
				}if (items[i].USER_TYPE_ID == 8) {
    				$(".scrollbox-content ul li ul").eq(7).append('<li class="submodule" data-uaid="'+items[i].UALCM_ID+'"><input type="checkbox" name="'+items[i].USER_ACCOUNT_MODULE+'" id="'+items[i].UALCM_ID+'"  '+check_status+'><label for="'+items[i].UALCM_ID+'" class="'+ status +'">'+items[i].USER_ACCOUNT_MODULE+'</label></li>');
				}
			}

		  	$('input[type="checkbox"]').change(checkboxChanged);
    	})



    	function checkboxChanged() {
		    var $this = $(this),
		        checked = $this.prop("checked"),
		        container = $this.parent(),
		        siblings = container.siblings();
		        // console.log($this)
		        // console.log(checked)
		        // console.log(container)
		        // console.log(siblings)
		    // if (checked === true) {
		        // console.log(checked)
			    container.find('input[type="checkbox"]')
			    .prop({
			        indeterminate: false,
			        checked: checked
			    })
			    .siblings('label')
			    .removeClass('custom-checked custom-unchecked custom-indeterminate')
			    .addClass(checked ? 'custom-checked' : 'custom-unchecked');

		    checkSiblings(container, checked);

	  	}

	  	function checkSiblings($el, checked) {
		    var parent = $el.parent().parent(),
		        all = true,
		        indeterminate = false;
		    $el.siblings().each(function() {
		      return all = ($(this).children('input[type="checkbox"]').prop("checked") === checked);
		   		console.log("test")
		    });

		    if (all && checked) {
		      	parent.children('input[type="checkbox"]')
		      	.prop({
		          	indeterminate: false,
		          	checked: checked
		      	})
		      	.siblings('label')
		      	.removeClass('custom-checked custom-unchecked custom-indeterminate')
		      	.addClass(checked ? 'custom-checked' : 'custom-unchecked');

		      	checkSiblings(parent, checked);
		    }
		    else if (all && !checked) {
		      	indeterminate = parent.find('input[type="checkbox"]:checked').length > 0;
		      	console.log(indeterminate)
		      	parent.children('input[type="checkbox"]')
		      	.prop("checked", checked)
		      	.prop("indeterminate", indeterminate)
		      	.siblings('label')
		      	.removeClass('custom-checked custom-unchecked custom-indeterminate')
		      	.addClass(indeterminate ? 'custom-indeterminate' : (checked ? 'custom-checked' : 'custom-unchecked'));

		      	checkSiblings(parent, checked);
		    }
		    else {
		      	$el.parents("li").children('input[type="checkbox"]')
		      	.prop({
		          	indeterminate: true,
		          	checked: false
		      	})
		      	.siblings('label')
		      	.removeClass('custom-checked custom-unchecked custom-indeterminate')
		      	.addClass('custom-checked');
		    }
	  	}
    }



    var __add = function() {
    	$("#addUser").unbind('click').on('click',function(){
			$("#success2").addClass("hide")
			$("#exst").addClass("hide")
			$("#reqErrorAdd").addClass("hide")
    		// $("#userType").val("")
    		$("#addModal").modal()

	    	$("#saveUser").unbind('click').on('click',function(){
    			
    			var payload = {
    				"access_NAME" : $("#userType").val()
    			}
    			console.log(payload)
    			if (payload.access_NAME == "" || payload.access_NAME == null) {
					$("#reqErrorAdd").removeClass("hide")
					$("#success2").addClass("hide")
					$("#exst").addClass("hide")

    			}else{
					$.main.executeExternalPost(path+"UpsertUserType", JSON.stringify(payload)).done(function(result){
				  	console.log(result)
					  	if (result.status == "FAILED") {
		    				$("#exst").removeClass("hide")
		    				$("#success2").addClass("hide")
							$("#reqErrorAdd").addClass("hide")

					  	}else{
		    				$("#success2").removeClass("hide")
		    				window.location.reload();
		    				$("#exst").addClass("hide")
							$("#reqErrorAdd").addClass("hide")

					  	}
				  	});
    			}


		  	});
	  	});

  	}
	return {
		displayAccountLevel: 	__displayAccountLevel,
		createAccountInfo: 	__createAccountInfo,
		accountControl : __accountControl,
		add : __add
	};

}());
