<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Accounting / Statement of Accounts, DESCRIPTION: User visited Statement of Accounts";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}

	
if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
    

	if ($user) 
	{
		
	
			
				SESSION::login($user);
                msgbox("Login Success");
               
               
				AUDIT::insert($user['account_ID'],"LOGGED IN");
				
				
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}


	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

      <title>HOME | School Management System v2.0</title>
	</head>

	<body>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
			<?php 
			
			SESSION::DisplayMsg(); 
			
			
			
			?>
		
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
				
				//audit
				
				
				
					?>
				<div class="col-md-3">
            		<?php include(PAGES."navigation.php");?>
				</div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 


								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>
								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

									</div>
								</div>
                                
                                	<div class="form-group row">
									<div class="col-lg-offset-9">
										 <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                                
                                </fieldset>
                    </form>
                    </div>	
                </div>
                
                <?php   }else{
				
				AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
				
				?>

<div class="col-md-3">

<?php include(PAGES."navigation.php");?>
</div>
              
                
<div class="col-md-9">
	<div class="panel panel-default">

		<div class="panel-heading">
			<table>
        		<tr>
        			<td width="70%">
						<h4 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"> Batch Statement of Account <i id="tblbatchSoa" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
					</td>
				 	
        			<td width="%">
	    					
					 	Export: <button class="btn btn-success btn-ms btnCSV">CSV</button>
                               	<button class="btn btn-success btn-ms btnXLS">XLS</button>
                               	<button class="btn btn-success btn-ms btnPDF">PDF</button>
	    				
	    			</td>
	    		</tr>
	    	</table>
		</div>
		<div class="panel-body"><br>
			<div class="row">
            	<div class="form-group">

					<div class="col-md-3">
						 <form action="" method="GET">
            			School Year/Semester: <select class="form-control" name="sem_ID" id="sem_ID" onchange=" this.form.submit() " >
                          <?php $e = SEM::getallsemsenrollment(array("sem_NAME"=>"DESC")); ?>
                          <option value="">Please choose</option>
                          <?php foreach($e as $key => $val){ ?>
                          <option <?php if(isset($_GET['sem_ID'])) { if($_GET['sem_ID']==$val['sem_ID']){ echo "SELECTED=SELECTED "; }  } ?> value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
                          <?php } ?>
                        </select>
                        <input type="submit"  class="hidden" value="Go" />
                    </form>
                    </div>
	                <!-- <div class="col-sm-5">
	                    <input name='' id="" placeholder="Search by Name or Student Number" class='form-control' type='text' required autofocus style="font-style: italic;"/>
	                </div> -->
            	</div>
           </div><br>
        	<table id="tblBatch" class="display nowrap table table-hover table-responsive table-striped text-md" style="width:100%;">
		        <thead>
		        	 <div class="col-sm-5">
                           <input name='' id="myInput" placeholder="Search by Student No., Student Name, Program, Section, Tuition, Total or Balance" class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
                     </div>
		            <tr>
		              <th>Student Number</th>
		              <th>Student Name</th>
		              <th>Course</th>
		              <th>Section</th>
		              <th>Tuition and Other Fees</th>
		              <th>SNPL</th>
		              <th>Total Payment</th>
		              <th>Balances</th>
		              <th>Option</th>
		            </tr>
		        </thead>
		        <?php
		        if($_GET['start'] == NULL)
				 {
					 $sem = $_GET['sem_ID'];
					 $viewStudents =SUBJENROL::getenrollmentpersem($sem);
					 // var_dump($viewStudents);

				 }
				 ?>
		        <tbody id="tbody">
		        	<?php $t_balances=0; ?>
		        	<?php if($viewStudents){ ?>
		        	<?php foreach($viewStudents as $key => $value){ 
		        		$user = USERS::getSingle(array("si_ID"=>"ASC"),$value['si_ID']);
		        		?>
		        		<tr>
		        			<td><?php echo $student_ID =  strtoupper($user['student_ID']); ?></td>
		        			<td><?php echo $lname = strtoupper($user['si_LNAME']); ?>, <?php echo $fname = strtoupper($user['si_FNAME']); ?> <?php echo $mname = strtoupper($user['si_MNAME']); ?></td>
		        			<td><?php echo $course = USERS::getCourse($value['si_ID']); ?>
		        			<?php
		        			$mysubj = explode(",",$value['subject_sched_ID']);
		        			
		        			?>
		        			
		        			<td><?php
		        			$arr_subj = [];
		        			foreach($mysubj as $key2 => $value2){
		        				
		        				$ss = SUBJSCHED::getSched($value2);
		        				$arr_subj[] = (COURSE::getbyID($ss['course_ID'])['course_INIT']."-".$ss['semester_section_NAME']);
		        				#echo COURSE::getbyID($ss['course_ID'])['course_INIT']."-".$ss['semester_section_NAME'];
		        				#var_dump($ss);
		        			}
		        				
		        			$ar = array_count_values($arr_subj);
		        			#var_dump($ar);
		        			arsort($ar);
		        			$keys=array_keys($ar);
		        			echo $section=$keys[0];
							#$most = array_slice(array_keys($arr_subj), 0, 2, true);

		        			#var_dump($arr_subj);
		        			?></td>
		        			<td>
	        				<?php  
		        				$pybl = ACCT::getSPayablesPerSem($user['si_ID'],$_GET['sem_ID']);
		        				echo money($pybl);
	        			  	?>
		        			</td>
		        			<td>
								<?php
									$accinfo =  SUBJENROL::getenrollmentofstudentpersem($user['si_ID'],$_GET['sem_ID']);
								 	$po_info = SUBJSCHED::getPaymentOptionByID($accinfo['po_ID']);
    								$SNPL = $po_info['po_SNPL'];
								 	// echo $SNPL
								?>
								(<?= ($SNPL*100); ?>%)  P <?php echo $snpl = money($pybl*$SNPL) ?>
		        			</td>

		        			<td><?php 
		        				$paid = (ACCT::getSPaymentPerSem($user['si_ID'],$_GET['sem_ID'])); 
		        				echo money($paid);
		        			  ?></td>

		        			<td>

		        				<?php
		        				if ($SNPL > 0) {
									$bal = ($pybl - ($pybl*$SNPL)-$paid);
								}else{
									$bal = ($pybl)-$paid;
								}
		        				echo $bal = money($bal);
		        					$t_balances += ($pybl - ($pybl*$SNPL)-$paid);
		        			 ?>
		        			 	
		        			</td>
		        			<td><button class="btn btnPrint btn-success" data-payable="<?= money($pybl); ?>" data-section="<?= $section; ?>" data-bal="<?= $bal; ?>" data-paid="<?= money($paid); ?>"  data-name="<?= $lname ?>,<?= $fname ?> <?= $mname ?>" data-course="<?= $course ?>"  data-sID="<?= $student_ID ?>" data-id="<?= $user['si_ID'] ?>" data-semid="<?=$_GET['sem_ID'] ?>"><i class="fa fa-print"></i>Print SOA</button></td>
		        		</tr>
	        		<?php } ?>
	        		<tr>
	        			<td></td>
	        			<td></td>
	        			<td></td>
	        			<td></td>
	        			<td></td>
	        			<td></td>
	        			<td class="b">Total Balances</td>
	        			<td><?= money($t_balances); ?></td>
	        			<td></td>
	        		</tr>
	        		<?php } ?>
		        </tbody>


		        <script>
		        	 $(window).ready(function() {
			                    setTimeout(function () {

				      },500);
                    });
		        </script>
		    </table>
		    <script src="https://unpkg.com/jspdf@latest/dist/jspdf.min.js"></script>

		    <div class="modal fade" id="myModal" role="dialog">
		    <div class="modal-dialog">
		      <div class="modal-content">
		        <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
		          <button type="button" class="close" data-dismiss="modal">&times;</button>
		          <h4 style="">Statement of Account</h4>
		        </div>

		        <div class="modal-body" style="">
		        	<div class="row">
			        	<div class="col-md-4 b">Student No:</div>
			           	<div class="col-md-8"><span class="ind_stud_no"></span></div>
		           </div>
		           <div class="row">
		        		<div class="col-md-4 b">Student Name:</div>
		           		<div class="col-md-8"><span class="ind_stud_name"></span></div>
		           </div>
		           <div class="row">
		           		<div class="col-md-4 b">Course:</div>
		           		<div class="col-md-8"><span class="ind_stud_course"></span></div>
	           		</div>
	           		<div class="row">
		           		<div class="col-md-4 b">Section:</div>
		           		<div class="col-md-8"><span class="ind_stud_section"></span></div>
	           		</div>
	           		------------------------------------------------------------------------------------------------
	           		<div class="row">
		           		<div class="col-md-6 b">Total Tuition and Other Fees:</div>
		           		<div class="col-md-6"><span class="ind_stud_payable"></span></div>
	           		</div>
	           		<div class="row">
		           		<div class="col-md-6 b">Total Payment:</div>
		           		<div class="col-md-6"><span class="ind_stud_paid"></span></div>
	           		</div>
	           		<div class="row">
		           		<div class="col-md-6 b">Total Balances:</div>
		           		<div class="col-md-6"><span class="ind_stud_balances"></span></div>
	           		</div>
	           		<!-- <br> -->
	           		<!-- <div class="row">
		           		<div class="col-md-6 b">Filter Date</div>
	           		</div> -->
	           		<!-- <div class="row">
	           			<div class="col-md-6">
	           				<div class="form-group">
					            <input type="date" class="form-control b" id="" placeholder="choose date">
					        </div>
	           			</div>
	           			<div class="col-md-6">
	           				<div class="form-group">
					            <input type="date" class="form-control b" id="" placeholder="choose date">
					        </div>
	           			</div>
	           		</div> -->

		        </div>


		        <div class="modal-body" style="">
					<!-- <a href="print_soa.php?id=<?= $user['si_ID'] ?>&sem_ID=<?= $_GET['sem_ID']; ?>" target="_blank" class='btn btn-success btn-block'> Print</a> -->
		          	<button class="btn btn-success btn-block btnsoa"> Print</button>
		        </div>



		      </div>
		    </div>
		  </div>

	 	</div>
	</div>
</div>
<?php } ?>
  	
			</div>
            <?php 
			?>
			<?php include (LAYOUTS . "footer.php"); ?>
			<script src="<?php echo HOME; ?>old/assets/js/custom/soa.js"></script>

		</div>
  <script type="text/javascript">
  	$( window ).ready(function(){
        setTimeout(function() {

					// $.main.applicant.applicantInfo();
					$.main.soa.batch();
          
        },100);
  	})

  </script>
<script type="text/javascript">
	$(function () {
                $('#datetimepicker2').datetimepicker();
            });
</script>
	</body>
</html>
<script type="text/javascript">
    function myFunction() {
        var input, filter, table, tr, td, i;
          input = document.getElementById("myInput");
          console.log(input);
          filter = input.value.toUpperCase();
          table = document.getElementById("tblBatch");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td2 = tr[i].getElementsByTagName("td")[1];
            td1 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            td4 = tr[i].getElementsByTagName("td")[4];
            td5 = tr[i].getElementsByTagName("td")[5];
            td6 = tr[i].getElementsByTagName("td")[6];
            if (td) {
              if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1 || td4.innerHTML.toUpperCase().indexOf(filter) > -1 || td5.innerHTML.toUpperCase().indexOf(filter) > -1 || td6.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }   
        }
    }
</script>
<style type="text/css">
    .dataTables_filter, .dataTables_info { display: none; }

</style>
