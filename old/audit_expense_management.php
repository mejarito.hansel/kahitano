<?php
include('init.php');
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Accounting / Expense Management, DESCRIPTION: User visited Expense Management";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
header('location: accounting_expense.php?action=View');
?>