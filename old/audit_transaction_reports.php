<?php
include('init.php');
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Accounting / Transaction Reports, DESCRIPTION: User visited Transaction Reports";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
header('location: acct_transaction_logs.php?action=reports');
?>