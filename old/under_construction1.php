<?php 
//INITIALIZE INCLUDES
  include('init.php');
#SESSION::CheckLogin();
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Registrar / Transcript of Records, DESCRIPTION: User visited Transcript of Records";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

if (SESSION::isLoggedIn()) {
  #HTML::redirect();
}

  



if (isset($_POST['username']) and isset($_POST['password'])) {
  $user = USERS::getID($_POST['username'], $_POST['password']);
    

    
  
    
  if ($user) 
  {
    #$ip =get_client_ip();
    # $security_option = navCat::security_option();
  
        
  
      
        SESSION::login($user);
                msgbox("Login Success");
               
               
        AUDIT::insert($user['account_ID'],"LOGGED IN");
        
        
        if(isset($_SESSION['previous_page']))
        {
          header("location:$_SESSION[previous_page]");
        }else{
          HTML::redirect();
        }
    

  } else {
       
    SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
  }
} else {
  $_SESSION['errors'] = NULL;
}



  //QUERIES




  
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php include(LAYOUTS . "styles.php"); ?>
    <?php include(LAYOUTS . "scripts.php"); ?>
      <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>

      <title>HOME | School Management System v2.0</title>
  </head>

  <body>

    <!-- top nav -->
    <?php include(LAYOUTS . 'top_nav.php'); ?>
    <!-- end nav -->

    <div class="container">
      
      <!-- banner -->
      <?php include(LAYOUTS . "banner.php"); ?>
      <!-- end banner -->
      
          
            
            
      <!-- alert messages -->
      <?php 
      
      SESSION::DisplayMsg(); 
      
      
      
      ?>
      <!-- end of alert messages -->
      
      <!-- start row container -->
      <!-- 
            <div class="row">
        
        <div class="col-lg-12">

        
          <ul class="breadcrumb">
            <li class="active">Home</li>
          </ul>
        

        </div>
        
      </div>
            -->
      <!-- end row container -->
      
   
             
            <!-- row container -->
      <div class="row">

        <!-- left nav -->
                
            <?php
        if (!SESSION::isLoggedIn()) {
        
        //audit
        
        
        
          ?>
        <div class="col-md-3">
                <?php include(PAGES."navigation.php");?>
        </div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
                <legend>Login</legend>

                <!-- if errors --> 
                <?php SESSION::DisplayMsg(); ?>
                <!-- end errors --> 


                <div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
                  <label for="inputUsername" class="col-lg-2 control-label">Username</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
                  </div>
                </div>
                <div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
                  <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                  <div class="col-lg-10">
                    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

                  </div>
                </div>
                                
                                  <div class="form-group row">
                  <div class="col-lg-offset-9">
                     <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
                    <button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
                  </div>
                </div>
                                
                                </fieldset>
                    </form>
                    </div>  
                </div>
                
                <?php   }else{
        
        AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
        
        ?>

<div class="col-md-3">

<?php include(PAGES."navigation.php");?>
</div>
              
                
<div class="col-md-9">
  <div class="panel panel-default">

    <div class="panel-heading">
      <table>
            <tr>
              <td width="50%">
                <h4 class="nav-pills" style="margin-top: 9px; margin-bottom: 9px"> Underconstruction <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
              </td>
          </tr>
      </table>
    </div>
    <div class="panel-body"><br>
        <div class="col-sm-3">
          <!-- <img src="/old/assets/img/tools.png"> -->
          <i class="fa fa-gears" style="font-size: 125px!important; margin-left: 15px;"></i>
        </div>
        <div class="col-sm-6">
          <h1 style="color: #505050;">THIS PAGE IS UNDERCONSTRUCTION</h1>
          <span style="color: #8c8c8c;">More information will be available very soon.</span>
          
        </div>
    </div>
  </div>
</div>
<?php } ?>
    
      </div>
            <?php 
      ?>
      <?php include (LAYOUTS . "footer.php"); ?>
      

    </div>
  </body>
</html>
