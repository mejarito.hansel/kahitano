<?php
	include('init.php');

if (!SESSION::isLoggedIn()) {
	HTML::redirect();
}


?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
      <title>Grade Inquiry | <?php echo $site_options['site_NAME']; ?></title>
      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
	</head>

	<body>

		<!-- Navbar -->	
		<?php include(LAYOUTS . "top_nav.php"); ?>
		<!--END Navbar -->

		<div class="container">

		<?php
		include(LAYOUTS . "banner.php");
		?>

			<!-- start body -->

			<!-- start row -->
			<div class="row">
				<div class="col-lg-12">
					<!-- breadcrumbs -->
					<ul class="breadcrumb">
						<li class=""><a href="<?php echo HOME; ?>index.php">Home</a></li>
						<li class="active">True Copy of Grades</li>
					</ul>
					<!-- end breadcrumbs -->  
				</div>
			</div>
			<!-- end row -->

			<!-- start row -->
			<div class="row">
				
                <div class="col-md-3">
                	<?php include(PAGES."navigation.php");?>
                </div>

				<div class="col-md-9">
     			  <h2>True Copy of Grades</h2>
                
                
                <?php 
				switch($_GET['action'])
				{
				case "remove":
					$id = clean($_GET['id']);
					$si_ID = clean($_GET['si_ID']);
					SUBJENROL::hideGrade($id);
					HTML::redirect("grade_TrueCopyOfGrades.php?action=view&id=$si_ID");
				break;
				
				
				case "view":
				$id = clean($_GET['id']);
				$sem_ID = clean($_GET['sem_ID']);
				$info =  USERS::viewSingleStudent($_GET['id']);
				
				$s = SEM::getSingleSem($sem_ID);
				
				#var_dump($info);

				$mygrade = SUBJENROL::gradebyid($id,$sem_ID);
				#var_dump($mygrade);
				?>
                
                
                
                <table class="table table-hover table-responsive table-striped text-md">
				<tr>
               	  <td>Name:</td>
                	<td colspan="4"><?php echo $info['si_LNAME'].", ".$info['si_FNAME']." ".$info['si_MNAME'];  ?>                  </td>
                </tr>
                <tr>
                	<td>Program:</td>
                	<td colspan="4"><?php echo USERS::getCourse($info['si_ID']) ?>                    </td>
                </tr>
                 <tr>
                	<td>School Year/Semester:</td>
               	   <td colspan="4"> <?= $s['sem_NAME']; ?>  </td>
                </tr>
                <tr>
                	<td colspan="5">&nbsp;</td>
                </tr>    
                <tr>				
                                        <th style="width:15%">Course</th>
                                        <th style="width:15%">Units</th>
                                        <th style="width:15%">Grade</th>
                                       	<th style="width:15%">Remarks</th>
                                       	<th style="width:20%">Instructor</th>
                                        
                                  <!--     <?php if (SESSION::isLoggedIn()) { ?><th style="">Action</th>   <?php } ?> -->
                </tr>
               
               
               <?php foreach($mygrade as $key => $val) { ?>
               
                	<?php if($val['status'] == 1){ ?>       
                       
                        <tr>
                          <td>
                            
                            <?php
                            $ss = SUBJSCHED::getSched($val['subject_sched_ID']);
							#echo $val['subject_sched_ID'];
							#$sem_ID
							#$ss['instructor_ID']
							#print_r($ss);
							 $s = SUBJECTS::getID($ss['subject_ID']);
                             echo $s['subject_CODE'];
							 $units = $s['LEC_UNIT'] + $s['LAB_UNIT'];
                            ?>                          </td>
                            <td><?php printf("%.2f",$units); ?></td>
                            <td>
							<a target="_new" href="grade_checking.php?action=view&sem_ID=<?= $sem_ID; ?>&fac_id=<?= $ss['instructor_ID'] ?>&subject_sched_id=<?= $val['subject_sched_ID'] ?>">
							<?php  if($ss['is_check'] == 1){
							 echo $val['grade']; 
							 }else{
							 echo "NG";
							 }
							 ?>
                            </a>
                            
                            </td>
                            
                          <td><?php 
						  
						  if($ss['is_check'] == 1){
							  ?><?php
											 	switch($val['grade'])
												{
													case "1.00":
													case "1.25":
													case "1.50":
													case "1.75":
													case "2.00":
													case "2.25":
													case "2.50":
													case "2.75":
													case "3.00": echo "PASSED";
													break;
													case "5.00": echo "FAILED";
													break;
													case "UW": echo "UNAUTHORIZED WITHDRAWAL";
							                        break;
							                        case "AW": echo "AUTHORIZED WITHDRAWAL";
							                        break;
													case "INC": echo "INCOMPLETE";
													break;
													case "DRP": echo "DROPPED";
													break;
                            default:
                              if($val['grade'] >= 100 || 60 <= $val['grade']){
                                echo "PASSED";
                              }elseif($val['grade'] >= 59 || 1 <= $val['grade']){
                                echo "FAILED";
                              }else{
                                echo "NO GRADE";
                              }
												}
											 ?>
                              <?php }else{
							  echo "NO GRADE";
							  }
							  ?>               </td>
							<td><?php  $ins = INSTRUCTORS::getSingle1($ss['instructor_ID']); echo $ins['instructor_NAME']." ".$ins['instructor_LNAME'];  ?></td>
                            <!--<td>
							
							<a href="?action=remove&id=<?php echo $val['subject_enrolled_ID']; ?>&si_ID=<?php echo $val['si_ID']; ?>" onclick="return confirm('Are you sure this is an invalid entry?');">[invalid entry]</a>
							
							</td>-->
                        </tr> 
                    <?php } ?>                              
                <?php } ?> 
                </table>
				<center>
				<a target="_blank" class="P3C7_PRINTGRADESLIP" href="print_TrueCopyOfGrades.php?&sem_ID=<?= clean($_GET['sem_ID']); ?>&si_ID=<?php echo clean($_GET['id']); ?>">[Print Grade Slip]</a> -  <a class="P3C7_SEARCHAGAIN" href="?action=search"> [Search Again?]</a>
				</center>
				<?php
				break;
				?>
                 
                <?php case "search": 
                $user_id = $user['account_ID'];
                $action_event = "View";
                $event_desc = "MODULE: Registrar / Grade Slip, DESCRIPTION: User visited Grade Slip";
                $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);



                $current_date =  date('Y-m-d H:i:s');
				$CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
                ?>
                	
                      
                      
                      
                      
                      <div class="panel-heading">
									<table class=""  style="width:100%;">
                                		
                                        
                                        <tr>
                                			<td width="20%">
												<h3 class="nav-pills"  style="margin-top: 5px; margin-bottom: 5px"> Name <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h3>
											</td>

										 	<td width="80%">
                                    			<div class="input-group col-md-12">
                                   				  
                                                     <input id="searchItem" class="searchItem form-control" data-acct="<?php echo $user['account_ID']; ?>" type="text" autofocus="autofocus"  style="text-transform: uppercase;"/>
                                                     <input type="hidden" id="ip_add" data-ip="<?php echo $CLIENT_IP;?>">
                                      					<span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    			</div>
                                			</td>
                                            
                           				  	<!-- <script>
                                    				$(document).ready(function() {
												$('#searchItem').keyup(function(){
												var s_Item = $('#searchItem').val();
													
													if(s_Item.length >= 3)
													{
														 $.ajax({
															type: "POST",
															url: "search.php?action=grade_inquiry.php",
															data: 'search_term=' + s_Item,
															success: function(msg){
																/* $('#resultip').html(msg); */
																	$("#display_result").show();
																	$("#display_result").html(msg);
																
															}
											
														}); // Ajax Call
													
														//alert(s_Item);
													}else{
														$("#display_result").hide();   
													}
										  		});
											});			
                                    			</script> -->
                                    		 <script>
                                                $(document).ready(function() {

                                                  var host = window.location.protocol+"//"+window.location.hostname ;
                                                    function postData(){
                                                        var s_Item = $('#searchItem').val();
                                                        if(s_Item.length >= 3)
                                                        {
                                                            console.log("Searching")
                                                            $.ajax({
                                                                    type: "POST",
                                                                    url: "search.php?action=grade_TrueCopyOfGrades.php",
                                                                    data: 'search_term=' + s_Item,
                                                                    beforeSend: function ( xhr ) {
                                                                        $("#spinner").show();
                                                                       //Add your image loader here
                                                                    },
                                                                    success: function(msg){
                                                                        /* $('#resultip').html(msg); */
                                                                        $("#spinner").hide();
                                                                        $('#resultip').html(msg); 
                                                                        $("#display_result").show();
                                                                        $("#display_result").html(msg);
                                                                        $("#display_hide").hide();
                                                                        
                                                                    }
                                                    
                                                                })
                                                          	console.log(s_Item);

                                                          	var wew = $("#searchItem").data("acct");
                                                          	var ip_add = $("#ip_add").data("ip");
                                                          	var event_action = "Search";
                                                          	var eventdesc = "MODULE: Registrar / Grade Slip, DESCRIPTION: User Searched for "+$("#searchItem").val();
                                                          
                                                          	console.log(ip_add);
                                                          	var api = host+'/api2/wsv1/api/Audit/';
                                                          	var payload = {
                                                          		'method':'PostAudit',
                                                          		'acct_id':wew,
                                                          		'client_ip':ip_add,
                                                          		'event_action':event_action,
                                                          		'eventdesc':eventdesc
                                                          	}
                                                          	$.main.executeExternalPost(api,JSON.stringify(payload)).done(function(result){
                                                          		if(result.status === 'SUCCESS'){
                                                          			console.log(result);
                                                          		}else{
                                                          			console.log(result.message);
                                                          		}


                                                          	});
                                                        }else{
                                                            $("#display_hide").show();
                                                            $("#display_result").hide();
                                                        }
                                                        return false;
                                                    }

                                                    $(function() {
                                                        var timer;
                                                        $("#searchItem").bind('keyup input',function() {
                                                            timer && clearTimeout(timer);
                                                            timer = setTimeout(postData, 300);
                                                        });
                                                    });


                                                    /*$('#searchItem').keyup(function(){
                                                    var s_Item = $('#searchItem').val();
                                                    
                                                        if(s_Item.length >= 3)
                                                        {
                                                             $.ajax({
                                                                type: "POST",
                                                                url: "search.php?action=si.php",
                                                                data: 'search_term=' + s_Item,
                                                                success: function(msg){
                                                                     $('#resultip').html(msg); 
                                                                        $("#display_result").show();
                                                                        $("#display_result").html(msg);
                                                                        
                                                                        $("#display_hide").hide();
                                                                    
                                                                }
                                                
                                                            }); 
                                                        }else{
                                                            $("#display_hide").show();
                                                            $("#display_result").hide();   
                                                        }
                                                    });*/
                                                });         
                                                </script>
                                			
                                			
                                		</tr>
                                	</table>
				  </div>

								<div class="panel-body">
									<div id="display_result"></div>
                                	
								</div>
							
                      
                     <?php break; 
					 default:  HTML::redirect("old/grade_TrueCopyOfGrades.php?action=search");  ?>
                    <?php } ?>  
                      
                      
                      
			  </div>
				
			
			<!-- end row -->

			<!-- end body -->

			<div class="spacer-short"></div>

			  </div>
			<!-- footer-->
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- container end -->
</div>
	</body>
</html>
