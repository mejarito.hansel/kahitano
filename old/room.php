<?php 
include('init.php');
SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
            ROOM::addroom(Request::post());
			
			break;
			case "edit": ROOM::update_room($_GET['id'],Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ROOM::delete($_GET['id']); break;
		
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>SCHOOL MANAGEMENT SYSTEM</title>
	</head>
	<body>

    <div class="modal fade" id="modalRoom_delete" tabindex="-1" role="dialog" aria-labelledby="modalRoom_delete">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <h3>Confirmation Box<br><br><small>Do you want to delete this record?</small></h3>
                </div>
                <div class="modal-footer" style="border-top:none; text-align:center !important; padding: 0px 20px 20px !important;">
                    <button type="button" class="btn btn-xs btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-xs btn-primary btnRoomdelete">Save changes</button>
                </div>
            </div>
        </div>
    </div>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			
			<?php SESSION::DisplayMsg(); ?>
			
			<div class="row">
			
            <?php
				if (!SESSION::isLoggedIn()) {
					?>
				
                <?php   }else{?>
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
                <?php } ?>
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Room </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Room Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                                <div class="col-sm-6">
                                   <b>Room Name</b>
                                    <input autofocus name='room_NAME' pattern="([a-zA-Z0-9]| |/|\|@|#|$|%|&)+" placeholder="Room Name" class='form-control' type='text' required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <B>Class Size</B>
                                    <input name='room_SIZE' placeholder="Class Size"  class='form-control' type='number' required/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Add Room</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":
                        $siiid =$_GET['id'];
                        $getsingle = ROOM::getSingle(array("room_ID"=>"DESC"),$siiid  );
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Update Room </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Room Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <B>Room ID</B>
                                    <input disabled="disabled" class='form-control' type='text' required value="<?= $getsingle['room_ID'];?>"/>
                                <input type="hidden" name="room_ID" value="<?=  $getsingle['room_ID']; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <B>Room Name</b>
                                    <input name='room_NAME' pattern="([a-zA-Z0-9]| |/|\|@|#|$|%|&)+" placeholder="Room Name" class='form-control' type='text' required value="<?= $getsingle['room_NAME'];?>"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <B>Class Size</b>
                                    <input name='room_SIZE' placeholder="Room Size" class='form-control' type='text' required value="<?= $getsingle['room_SIZE'];?>"/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Update Room</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
						include "searching.php";						
						?>
							
                    <?php break; 
                        case "view":
                        $user_id = $user['account_ID'];
                        $action_event = "View";
                        $event_desc = "MODULE: Academic Affairs / Room Encoder, DESCRIPTION: User visited Room Encoder";
                        $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

                        ?>
                        <?php
								if(isset($_GET['id']))
								{
								$rID = $_GET['id'];
                                    //msgbox($studID);
                                    $view1room = ROOM::view1room($rID);
                                    if(count($view1room)>=1){
										foreach($view1room as $key => $value){
											$room_ID = $value['room_ID'];
											$room_NAME = $value['room_NAME'];
											
						?>
						<div class="panel panel-default">
							<div class="panel-heading"  style="margin-top: 5px; margin-bottom: 5px">
                                <div class="col-md-3">
								<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
									<i class="fa fa-user"></i>		
								</h3>
                                </div>
                                <div class="col-md-53">
								<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
								</h3>
                                </div>
                            </div>
							
							<div class="panel-body">
								<table border='0' class="table table-responsive text-md">
									<tr>
										<td colspan='4'><strong>View Rooms</strong>
									<tr>
										<td>&nbsp;
										<td align='center'><?php echo $room_ID;?>
									<tr>
										<td>&nbsp;
										<td align='center'><strong><i>Room ID</i></strong>
<tr>
										<td>&nbsp;
										<td align='center'><?php echo $room_Name;?>
									<tr>
										<td>&nbsp;
										<td align='center'><strong><i>Room Name</i></strong>

						<?php } ?>
								</table>
								<!--
								<tr>
										<td colspan='4' align='right'>
								-->
									
							</div>
							
						</div>
							<div align='right'>
								<input type='button' class='btn btn-warning' value='Edit'>
								<!--<input type='button' onClick="location.href='../../isjb/student_information.php?action=view'" class='btn btn-success' value='Back'>-->
							</div>
					<?php
									}else{ echo 'Error: 404 Not found.'; }
                                }else{
					?>
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table>
                                <tr>
                                <td width="50%">
                                    <h4 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                        <i class="fa fa-folder-o"></i> View Rooms </h4>
                                </td>
                               
                                <td >
                                   <!--  <div class="input-group col-md-12">
                                      	<form id="form1" runat="server">
                                       		 <input id="searchItem" class="searchItem form-control"  name="q" type="text" onkeyup="term(q.value)" autofocus="autofocus" onchange="term(q.value)"/>
                                        </form>
                                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div> -->
                                   
                                    <input name='' id="myInput" placeholder="Search by Room Id, Room Name or Siza" class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
                                
                                     </td>
                                	
                                   			    <script>
                                    			$(document).ready(function() {
												$('#searchItem').keyup(function(){
												var s_Item = $('#searchItem').val();
													
													if(s_Item.length >= 3)
													{
														 $.ajax({
															type: "POST",
															url: "search.php?action=room.php",
															data: 'search_term=' + s_Item,
															success: function(msg){
																/* $('#resultip').html(msg); */
																	$("#display_result").show();
																	$("#display_result").html(msg);
																	
																	$("#display_hide").hide();
																
															}
											
														}); // Ajax Call
													
														//alert(s_Item);
														}else{
															$("#display_hide").show();
															$("#display_result").hide();   
														}
													});
												});			
                                    			</script>
                                    
                                    
                                <td width="5%">
                                <div class="col-md-1">   
                                <button style="margin-top: 2px; margin-bottom: 2px" class='btn btn-success btn-ms P5C7_ADDROOM' type='submit' onclick="javascript:window.open('room.php?action=add','_self');">Add Room</button>
                                </div>
                                </td>
                                </tr>
                                </table>
                            </div>
							
							<div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
								<table class="table table-hover table-responsive table-striped text-md" id="cadetView">
									<tr><th style="width:15%;">Room ID</th>
									<th style="width:15%;">Room Name</th>
									<th style="width:15%;">Size</th>
									<th style="width:15%;">Operation</th>
                                    
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    $viewStudents = ROOM:: getallrooms(array("room_ID"=>"ASC"));
                                     if(count($viewStudents)>=10) 
                                     {
                                         $pagination2 = new Pagination($viewStudents, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($viewStudents, 10, NULL);
                                     }

                                     $viewStudents = $pagination2->get_array();

                                     if($viewStudents) {
                                        foreach($viewStudents as $key => $value){
                                    ?>
                                        <tr>
                                            <td><?php echo $value['room_ID']; ?>
                                            <td><?php echo $value['room_NAME']; ?>
                                            <td><?php echo $value['room_SIZE']; ?>                                            
                                            <td><a href="room.php?action=edit&id=<?php echo $value['room_ID']; ?>" class='btn btn-warning P5C7_EDIT btn-xs'>Edit</a>
                                            <!-- <a href="room.php?action=delete&id=<?php echo $value['room_ID']; ?>" class='btn btn-danger btn-xs'>Delete</a>                                            </td> -->
                                            <a href="#!" class='btnDeleteRoom btn btn-danger btn-xs P5C7_DELETE' data-id="<?php echo $value['room_ID']; ?>">Delete</a>                                            </td>
                                        </tr>
                                        <script type="text/javascript">
                                            $(".btnDeleteRoom").unbind().bind("click",function(){
                                                var delete_id = $(this).data("id");
                                                $("#modalRoom_delete").modal("show");
                                                $(".btnRoomdelete").on("click", function(){
                                                    window.location = "room.php?action=delete&id="+delete_id
                                                });
                                            });
                                        </script>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
          <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                                        </center>
							     </div>
                            </div>
						</div>
                            
					</div>
                </div>
                  <?php } break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                	
				    
			<!-- end row container -->
            
            

			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
</html>
<script type="text/javascript">
    function myFunction() {
        var input, filter, table, tr, td, i;
          input = document.getElementById("myInput");
          console.log(input);
          filter = input.value.toUpperCase();
          table = document.getElementById("cadetView");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td2 = tr[i].getElementsByTagName("td")[1];
            td1 = tr[i].getElementsByTagName("td")[2];
            if (td) {
              if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }   
        }
    }
</script>