<?php

$time_start = microtime(true); 
	error_reporting(E_ALL);
	include('init.php');
	error_reporting(E_ALL);
	SESSION::CheckLogin();
	$id = $_GET['id'];
	$subj = SUBJSCHED::getID($id);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Generating Grading Sheet</title>
</head>

<body>
	<?php
	$ss = SUBJSCHED::getSched($subj['subject_sched_ID']); 
	$s = SUBJECTS::getID($ss['subject_ID']);
	$sem = SEMSECTION::getID($subj['semester_section_ID']);
	$total_unit = ($s['LEC_UNIT']+$s['LAB_UNIT']);
    $course = COURSE::getbyID($sem['course_ID']); 
	$sem_ = SEM::getSingleSem($sem['sem_ID']);
	$sem_name = $sem_['sem_NAME'];
	$room = ROOM::getID($subj['room_ID']); 
    $ins = INSTRUCTORS::getSingle1($subj['instructor_ID']);
	$instructor = strtoupper($ins['instructor_NAME']);

	require_once 'phpexcel/Classes/PHPExcel/IOFactory.php';
	require_once 'phpexcel/Classes/PHPExcel.php';
	$stud =   SUBJSCHED::getBySubjectSchedID(clean($_GET['id']),$ins['instructor_ID']);
	$excel2 = PHPExcel_IOFactory::createReader('Excel2007');
	
	
	
	#$sheetnames = array('SUBJINFO','STUDINFO'); 
	#$excel2->setLoadSheetsOnly($sheetnames); 
	
	if( substr($course['course_INIT'],0,1) == 'A')
	{
		$excel2 = $excel2->load('template/TEMPLATE_ASSOCIATE.xlsx');
	}else{
		$excel2 = $excel2->load('template/TEMPLATE_BS.xlsx');
	}
	$excel2->setActiveSheetIndex(0);
	$sheet = $excel2->getActiveSheet();
	$sheet->setCellValue('E8', $sem_name);
	$sheet->setCellValue('E9', $s['subject_CODE']);
	$sheet->setCellValue('E10', $s['subject_DESCRIPTION']);
	$sheet->setCellValue('E11', $total_unit);
	$sheet->setCellValue('E12', $course['course_INIT']."-".$sem['semester_section_NAME']);
	$sheet->setCellValue('E13', $subj['day']);
	$sheet->setCellValue('E14',$subj['start']);
	$sheet->setCellValue('I14',$subj['end']);
	$sheet->setCellValue('E15',$room['room_NAME']);
	$sheet->setCellValue('E17',$instructor);
	$excel2->setActiveSheetIndex(1);
	$sheet = $excel2->getActiveSheet();
	$i=10;
	
	foreach($stud as $key => $val){
		$sheet->setCellValue('C'.$i, $val['si_LNAME'].", ".$val['si_FNAME']." ". substr($val['si_MNAME'],0,1).".");
		$sheet->setCellValue('D'.$i, $val['student_ID']);
		$sheet->setCellValue('E'.$i, USERS::getCourse($val['si_ID']));
		$sheet->setCellValue('F'.$i, $val['si_CONTACT']);	
	$i++;	
	}
	
	$objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('temp/'.$s['subject_CODE']."-".$course['course_INIT']."-".$sem['semester_section_NAME'].'.xlsx');
	#href("temp/".$s['subject_CODE']."-".$course['course_INIT']."-".$sem['semester_section_NAME'].'.xlsx');
	$link = "temp/".$s['subject_CODE']."-".$course['course_INIT']."-".$sem['semester_section_NAME'].'.xlsx';
?>

	<script>
		window.opener.location.href = '<?php echo $link; ?>';
		window.close();
	
	</script>


<?php echo (microtime(true) - $time_start);?>
</body>
</html>
