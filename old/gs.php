<?php

$time_start = microtime(true); 
	error_reporting(E_ALL);
	include('init.php');
	error_reporting(E_ALL);
	SESSION::CheckLogin();
	$id = $_GET['id'];
	$subj = SUBJSCHED::getID($id);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Generating Grading Sheet</title>
</head>

<body>
	<?php
	$ss = SUBJSCHED::getSched($subj['subject_sched_ID']); 
	$s = SUBJECTS::getID($ss['subject_ID']);
	$sem = SEMSECTION::getID($subj['semester_section_ID']);
	$total_unit = ($s['LEC_UNIT']+$s['LAB_UNIT']);
    $course = COURSE::getbyID($sem['course_ID']); 
	$sem_ = SEM::getSingleSem($sem['sem_ID']);
	$sem_name = $sem_['sem_NAME'];
	$room = ROOM::getID($subj['room_ID']); 
    $ins = INSTRUCTORS::getSingle1($subj['instructor_ID']);
	$instructor = strtoupper($ins['instructor_NAME']);
	echo "Loading From DB: ". (microtime(true) - $time_start)."<br>";
	require_once 'phpexcel/Classes/PHPExcel/IOFactory.php';
	require_once 'phpexcel/Classes/PHPExcel.php';
	$stud =   SUBJSCHED::getBySubjectSchedID(clean($_GET['id']),$ins['instructor_ID']);
	$excel2 = PHPExcel_IOFactory::createReader('Excel2007');
	echo "Creating Reader: ". (microtime(true) - $time_start)."<br>";
	
	
	#$sheetnames = array('SUBJINFO','STUDINFO'); 
	#$excel2->setLoadSheetsOnly($sheetnames); 
	
	if( substr($course['course_INIT'],0,1) == 'A')
	{
		$excel2 = $excel2->load('template/TEMPLATE_ASSOCIATE.xlsx');
	}else{
		$excel2 = $excel2->load('template/TEMPLATE_BS.xlsx');
	}
	echo "Loading Template Sheet: ". (microtime(true) - $time_start)."<br>";	
	$excel2->setActiveSheetIndex(0);
	echo "Setting Info Sheet: ". (microtime(true) - $time_start)."<br>";	
	$sheet = $excel2->getActiveSheet()->setCellValue('E8', $sem_name)
					->setCellValue('E9', $s['subject_CODE'])
					->setCellValue('E10', $s['subject_DESCRIPTION'])
					->setCellValue('E11', $total_unit)
					->setCellValue('E12', $course['course_INIT']."-".$sem['semester_section_NAME'])
					->setCellValue('E13', $subj['day'])
					->setCellValue('E14',$subj['start'])
					->setCellValue('I14',$subj['end'])
					->setCellValue('E15',$room['room_NAME'])
					->setCellValue('E17',$instructor);
	echo "Writing INFO Sheet: ". (microtime(true) - $time_start)."<br>";	
	
	$excel2->setActiveSheetIndex(1);
	echo "Setting Student Sheet: ". (microtime(true) - $time_start)."<br>";	
	$sheet = $excel2->getActiveSheet();
	$i=10;
	
	foreach($stud as $key => $val){
		$sheet->setCellValue('C'.$i, $val['si_LNAME'].", ".$val['si_FNAME']." ". substr($val['si_MNAME'],0,1).".")
				->setCellValue('D'.$i, $val['student_ID'])
				->setCellValue('E'.$i, USERS::getCourse($val['si_ID']))
				->setCellValue('F'.$i, $val['si_CONTACT']);	
	$i++;	
	}
	echo "Writing STUDENT Sheet: ". (microtime(true) - $time_start)."<br>";	
	$objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
	$objWriter->setPreCalculateFormulas(false);
	$objWriter->save('temp/'.$s['subject_CODE']."-".$course['course_INIT']."-".$sem['semester_section_NAME'].'.xlsx');
	echo "SAVING Sheet: ". (microtime(true) - $time_start)."<br>";	
	#href("temp/".$s['subject_CODE']."-".$course['course_INIT']."-".$sem['semester_section_NAME'].'.xlsx');
	$link = "temp/".$s['subject_CODE']."-".$course['course_INIT']."-".$sem['semester_section_NAME'].'.xlsx';
?>

	<script>
		window.opener.location.href = '<?php echo $link; ?>';
		window.close();
	
	</script>


<?php echo (microtime(true) - $time_start);?>
</body>
</html>
