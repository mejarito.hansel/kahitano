<?php
include('init.php');
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Academic Affairs / Faculty Utilities, DESCRIPTION: User visited Faculty Utilities";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

header("Location: faculty_loading.php?action=view");

?>