<?php 
//INITIALIZE INCLUDES
  include('init.php');
#SESSION::CheckLogin();


if (SESSION::isLoggedIn()) {
  #HTML::redirect();
}

  



if (isset($_POST['username']) and isset($_POST['password'])) {
  $user = USERS::getID($_POST['username'], $_POST['password']);
    

    
  
    
  if ($user) 
  {
    #$ip =get_client_ip();
    # $security_option = navCat::security_option();
  
        
  
      
        SESSION::login($user);
                msgbox("Login Success");
               
               
        AUDIT::insert($user['account_ID'],"LOGGED IN");
        
        
        if(isset($_SESSION['previous_page']))
        {
          header("location:$_SESSION[previous_page]");
        }else{
          HTML::redirect();
        }
    

  } else {
       
    SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
  }
} else {
  $_SESSION['errors'] = NULL;
}



  //QUERIES




  
  
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php include(LAYOUTS . "styles.php"); ?>
    <?php include(LAYOUTS . "scripts.php"); ?>
      <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>

      <title>HOME | School Management System v2.0</title>
      <script type="text/javascript">
            $(document).ready(function() {
              $('#example').DataTable({
                "scrollX": true,
                "order": [[ 0, "desc" ]]
              });
            } );
          </script>
  </head>

  <body>

    <!-- top nav -->
    <?php include(LAYOUTS . 'top_nav.php'); ?>
    <!-- end nav -->

    <div class="container">
      
      <!-- banner -->
      <?php include(LAYOUTS . "banner.php"); ?>
      <!-- end banner -->
      
          
            
            
      <!-- alert messages -->
      <?php 
      
      SESSION::DisplayMsg(); 
      
      
      
      ?>
      <!-- end of alert messages -->
      
      <!-- start row container -->
      <!-- 
            <div class="row">
        
        <div class="col-lg-12">

        
          <ul class="breadcrumb">
            <li class="active">Home</li>
          </ul>
        

        </div>
        
      </div>
            -->
      <!-- end row container -->
      
   
             
            <!-- row container -->
      <div class="row">

        <!-- left nav -->
                
            <?php
        if (!SESSION::isLoggedIn()) {
        
        //audit
        
        
        
          ?>
        <div class="col-md-3">
                <?php include(PAGES."navigation.php");?>
        </div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
                <legend>Login</legend>

                <!-- if errors --> 
                <?php SESSION::DisplayMsg(); ?>
                <!-- end errors --> 


                <div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
                  <label for="inputUsername" class="col-lg-2 control-label">Username</label>
                  <div class="col-lg-10">
                    <input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
                  </div>
                </div>
                <div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
                  <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                  <div class="col-lg-10">
                    <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

                  </div>
                </div>
                                
                                  <div class="form-group row">
                  <div class="col-lg-offset-9">
                     <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
                    <button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
                  </div>
                </div>
                                
                                </fieldset>
                    </form>
                    </div>  
                </div>
                
                <?php   }else{
        
        AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
        
        ?>

<div class="col-md-3">

<?php include(PAGES."navigation.php");?>
</div>
              
                        
<div class="col-md-9">
  <?php
          $record = SUBJSCHED::getAllperSEM2(array("subject_sched_ID" => "DESC"));
          #print_r($record);

          ?>
            <div class="panel panel-default">
              <div class="panel-heading">
                <table>
                      <tr>
                        <td width="50%">
                      <h4 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"> Subject Schedule <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
                    </td>
                    <td>
                            <div class="input-group col-md-12 hide">
                                
                                <input id="searchItem" class="searchItem form-control"  type="text" autofocus="autofocus"  style="text-transform: uppercase;"/>
                                      
                                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>
                        </td>
                        <td width="5%">
                        <div class="col-md-1">
                          <button style="margin-top: 3px; margin-bottom: 3px" class="btn btn-success btn-ms" type="submit" onclick="javascript:window.open('subject_sched.php','_self');">Back to View</button>   
                        </div>
                      </td>
                    </tr>
                  </table>
              </div>
              <div class="panel-body">
          <form action="<?php echo $_SERVER['REQUEST_URI']?>" method="post">
            <legend><i class="fa fa-user"></i>  Semester Section</legend>
              <div class="row">
                <div class="col-lg-6">
                  <label class="">Section</label>
                    <?php
                    #$sem = SEMSECTION::getAll(array("sem_ID"=>"DESC");
                    $sem = SEMSECTION::getAll3(array("sem_ID"=>"DESC","course_ID"=>"ASC","semester_section_NAME"=>"ASC"));
                    #print_r($sem);
                    ?>
                    <select name="semester_section_ID" id="semester_section_ID" class="form-control">
                        <?php foreach($sem as $key => $val){ ?>
                      <option value="<?php echo $val['semester_section_ID']; ?>"    <?php if(isset($_POST['semester_section_ID'])){ if($_POST['semester_section_ID'] == $val['semester_section_ID']){ echo "SELECTED=SELECTED"; } } ?>>
                        <?php $sem = SEMSECTION::getID($val['semester_section_ID']); ?>
                        <?php $d = SEMSECTION::getSEM($sem['sem_ID']);
                        echo $d['sem_NAME']; ?>
                      -
                        <?php $c = COURSE::getbyID($val['course_ID']);
                        echo $c['course_INIT']; ?>-<?php echo $val['semester_section_NAME']; ?>
                        </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-6">
                  <label for="Subject" class="">Subject</label>
                    <?php
                       $viewSubjects = SUBJECTS:: getAllSubjects(array("subject_CODE"=>"ASC"));
                    ?>
                    <select name="subject_ID" id="subject_ID" class="form-control">
                        <?php foreach($viewSubjects as $key => $val){ ?>
                        <option  value="<?php echo $val['subject_ID']; ?>"><?php echo $val['subject_CODE']; ?> - <?php echo $val['subject_DESCRIPTION']; ?> - <?php echo $val['LEC_UNIT']."/".$val['LAB_UNIT']; ?></option>
                        <?php } ?>    
                    </select>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-lg-3">
                  <label for="Day" class="">Day</label>
                  <select name="day" id="day" class="form-control">
                    <option value="TBD">TBD</option>
                    <option value="Monday">MON</option>
                    <option value="Tuesday">TUE</option>
                    <option value="Wednesday">WED</option>
                    <option value="Thursday">THU</option>
                    <option value="Friday">FRI</option>
                    <option value="Saturday">SAT</option>
                    <?php foreach($viewSubjects as $key => $val){ ?>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Room" class="">Room</label>
                    <?php
                     $viewStudents = ROOM:: getallrooms(array("room_NAME"=>"DESC"));
                    ?>
                    <select name="room_ID" id="room_ID" class="form-control">
                      <?php foreach($viewStudents as $key => $val){ ?>
                      <option value="<?php echo $val['room_ID']; ?>"><?php echo $val['room_NAME']; ?></option>
                      <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3">
                  <label for="Start" class="">Start</label>
                  <select name="start" id="start" class="form-control">
                    <option value="TBD">TBD</option>  
                    <option value="7:00 AM">7:00 AM</option>
                    <option value="7:30 AM">7:30 AM</option>
                    <option value="8:00 AM">8:00 AM</option>
                    <option value="8:30 AM">8:30 AM</option>
                    <option value="9:00 AM">9:00 AM</option>
                    <option value="9:30 AM">9:30 AM</option>
                    <option value="10:00 AM">10:00 AM</option>
                    <option value="10:30 AM">10:30 AM</option>
                    <option value="11:00 AM">11:00 AM</option>
                    <option value="11:30 AM">11:30 AM</option>
                    <option value="12:00 PM">12:00 PM</option>
                    <option value="12:30 PM">12:30 PM</option>
                    <option value="1:00 PM">1:00 PM</option>
                    <option value="1:30 PM">1:30 PM</option>
                    <option value="2:00 PM">2:00 PM</option>
                    <option value="2:30 PM">2:30 PM</option>
                    <option value="3:00 PM">3:00 PM</option>
                    <option value="3:30 PM">3:30 PM</option>
                    <option value="4:00 PM">4:00 PM</option>
                    <option value="4:30 PM">4:30 PM</option>
                    <option value="5:00 PM">5:00 PM</option>
                    <option value="5:30 PM">5:30 PM</option>
                    <option value="6:00 PM">6:00 PM</option>
                    <option value="6:30 PM">6:30 PM</option>
                    <option value="7:00 PM">7:00 PM</option>
                    <option value="7:30 PM">7:30 PM</option>
                    <option value="8:00 PM">8:00 PM</option>
                    <option value="8:30 PM">8:30 PM</option>
                    <option value="9:00 PM">9:00 PM</option>
                    <option value="9:30 PM">9:30 PM</option>
                    <option value="10:00 PM">10:00 PM</option>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Installment" class="">End</label>
                  <select name="end" id="end" class="form-control">
                    <option value="TBD">TBD</option>  
                    <option value="7:00 AM">7:00 AM</option>
                    <option value="7:30 AM">7:30 AM</option>
                    <option value="8:00 AM">8:00 AM</option>
                    <option value="8:30 AM">8:30 AM</option>
                    <option value="9:00 AM">9:00 AM</option>
                    <option value="9:30 AM">9:30 AM</option>
                    <option value="10:00 AM">10:00 AM</option>
                    <option value="10:30 AM">10:30 AM</option>
                    <option value="11:00 AM">11:00 AM</option>
                    <option value="11:30 AM">11:30 AM</option>
                    <option value="12:00 PM">12:00 PM</option>
                    <option value="12:30 PM">12:30 PM</option>
                    <option value="1:00 PM">1:00 PM</option>
                    <option value="1:30 PM">1:30 PM</option>
                    <option value="2:00 PM">2:00 PM</option>
                    <option value="2:30 PM">2:30 PM</option>
                    <option value="3:00 PM">3:00 PM</option>
                    <option value="3:30 PM">3:30 PM</option>
                    <option value="4:00 PM">4:00 PM</option>
                    <option value="4:30 PM">4:30 PM</option>
                    <option value="5:00 PM">5:00 PM</option>
                    <option value="5:30 PM">5:30 PM</option>
                    <option value="6:00 PM">6:00 PM</option>
                    <option value="6:30 PM">6:30 PM</option>
                    <option value="7:00 PM">7:00 PM</option>
                    <option value="7:30 PM">7:30 PM</option>
                    <option value="8:00 PM">8:00 PM</option>
                    <option value="8:30 PM">8:30 PM</option>
                    <option value="9:00 PM">9:00 PM</option>
                    <option value="9:30 PM">9:30 PM</option>
                    <option value="10:00 PM">10:00 PM</option>
                    <option value="10:30 PM">10:30 PM</option>
                    <option value="11:00 PM">11:00 PM</option>
                  </select>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-lg-3">
                  <label for="Day2" class="">Day2</label>
                  <select name="day2" id="day2" class="form-control">
                    <option value="">None</option>
                    <option value="TBD">TBD</option>
                    <option value="Monday">MON</option>
                    <option value="Tuesday">TUE</option>
                    <option value="Wednesday">WED</option>
                    <option value="Thursday">THU</option>
                    <option value="Friday">FRI</option>
                    <option value="Saturday">SAT</option>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Room2" class="">Room2</label>
                    <?php
                       $viewStudents = ROOM:: getallrooms(array("room_NAME"=>"ASC"));
                    ?>
                    <select name="room_ID2" id="room_ID2" class="form-control">
                      <option value="">None</option>
                      <?php foreach($viewStudents as $key => $val){ ?>
                      <option value="<?php echo $val['room_ID']; ?>"><?php echo $val['room_NAME']; ?></option>
                      <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3">
                  <label for="Start2" class="">Start2</label>
                  <select name="start2" id="start2" class="form-control">
                    <option value="">None</option>
                    <option value="TBD">TBD</option>  
                    <option value="7:00 AM">7:00 AM</option>
                    <option value="7:30 AM">7:30 AM</option>
                    <option value="8:00 AM">8:00 AM</option>
                    <option value="8:30 AM">8:30 AM</option>
                    <option value="9:00 AM">9:00 AM</option>
                    <option value="9:30 AM">9:30 AM</option>
                    <option value="10:00 AM">10:00 AM</option>
                    <option value="10:30 AM">10:30 AM</option>
                    <option value="11:00 AM">11:00 AM</option>
                    <option value="11:30 AM">11:30 AM</option>
                    <option value="12:00 PM">12:00 PM</option>
                    <option value="12:30 PM">12:30 PM</option>
                    <option value="1:00 PM">1:00 PM</option>
                    <option value="1:30 PM">1:30 PM</option>
                    <option value="2:00 PM">2:00 PM</option>
                    <option value="2:30 PM">2:30 PM</option>
                    <option value="3:00 PM">3:00 PM</option>
                    <option value="3:30 PM">3:30 PM</option>
                    <option value="4:00 PM">4:00 PM</option>
                    <option value="4:30 PM">4:30 PM</option>
                    <option value="5:00 PM">5:00 PM</option>
                    <option value="5:30 PM">5:30 PM</option>
                    <option value="6:00 PM">6:00 PM</option>
                    <option value="6:30 PM">6:30 PM</option>
                    <option value="7:00 PM">7:00 PM</option>
                    <option value="7:30 PM">7:30 PM</option>
                    <option value="8:00 PM">8:00 PM</option>
                    <option value="8:30 PM">8:30 PM</option>
                    <option value="9:00 PM">9:00 PM</option>
                    <option value="9:30 PM">9:30 PM</option>
                    <option value="10:00 PM">10:00 PM</option>
                    <option value="10:30 PM">10:30 PM</option>
                    <option value="11:00 PM">11:00 PM</option>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="End2" class="">End2</label>
                  <select name="end2" id="end2" class="form-control">
                    <option value="">None</option>
                    <option value="TBD">TBD</option>  
                    <option value="7:00 AM">7:00 AM</option>
                    <option value="7:30 AM">7:30 AM</option>
                    <option value="8:00 AM">8:00 AM</option>
                    <option value="8:30 AM">8:30 AM</option>
                    <option value="9:00 AM">9:00 AM</option>
                    <option value="9:30 AM">9:30 AM</option>
                    <option value="10:00 AM">10:00 AM</option>
                    <option value="10:30 AM">10:30 AM</option>
                    <option value="11:00 AM">11:00 AM</option>
                    <option value="11:30 AM">11:30 AM</option>
                    <option value="12:00 PM">12:00 PM</option>
                    <option value="12:30 PM">12:30 PM</option>
                    <option value="1:00 PM">1:00 PM</option>
                    <option value="1:30 PM">1:30 PM</option>
                    <option value="2:00 PM">2:00 PM</option>
                    <option value="2:30 PM">2:30 PM</option>
                    <option value="3:00 PM">3:00 PM</option>
                    <option value="3:30 PM">3:30 PM</option>
                    <option value="4:00 PM">4:00 PM</option>
                    <option value="4:30 PM">4:30 PM</option>
                    <option value="5:00 PM">5:00 PM</option>
                    <option value="5:30 PM">5:30 PM</option>
                    <option value="6:00 PM">6:00 PM</option>
                    <option value="6:30 PM">6:30 PM</option>
                    <option value="7:00 PM">7:00 PM</option>
                    <option value="7:30 PM">7:30 PM</option>
                    <option value="8:00 PM">8:00 PM</option>
                    <option value="8:30 PM">8:30 PM</option>
                    <option value="9:00 PM">9:00 PM</option>
                    <option value="9:30 PM">9:30 PM</option>
                    <option value="10:00 PM">10:00 PM</option>
                    <option value="10:30 PM">10:30 PM</option>
                    <option value="11:00 PM">11:00 PM</option>
                  </select>
                </div>
              </div>
              <br>  
              <div class="row">
                <div class="col-lg-3">
                  <label for="Instructor" class="">Instructor</label>
                    <?php
                      $viewIntructors = INSTRUCTORS:: getAllInstructors(array("instructor_ID"=>"ASC"));
                    ?>
                    <select name="instructor_ID" id="instructor_ID" class="form-control">
                      <?php foreach($viewIntructors as $key => $val){ ?>
                      <option <?php if($val['instructor_NAME'] == 'TBD'){ echo "SELECTED=SELECTED"; } ?> value="<?php echo $val['instructor_ID']; ?>"><?php echo $val['instructor_NAME']; ?></option>
                      <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3">
                  <label for="max_student" class="">Max Student</label>
                  <input class="form-control" name="max_student" type="number" id="max_student" value="30" />
                </div>
              </div> 
              <div class="row ">
                <div class="col-lg-12 ">
                  <div class="row">
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-3">
                      <button id="button" class="btn btn-success btn-block" type="submit"><i class="fa fa-edit"></i> Add
                      </button>
                    </div>
                    <div class="col-lg-1">
                    </div>
                  </div>
                </div>
              </div>
          </form>
        </div>
                 
              </div>
</div>
<?php } ?>
    
      </div>
            <?php 
      ?>
      <?php include (LAYOUTS . "footer.php"); ?>
      <script src="<?php echo HOME; ?>old/assets/js/custom/applicant.js"></script>

    </div>
  
  </body>
</html>
