<?php
	include('init.php');

SESSION::CheckLogin();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Semester per Subject Report</title>
<style type="text/css">
<!--
.style9 {font-family: Arial, Helvetica, sans-serif}
.style10 {font-size: 12px}
.style11 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; }
.style12 {font-family: Arial, Helvetica, sans-serif; font-size: 16px}

-->
</style>
</head>

<body onload="window.print()">
<!-- <table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" >
  <tr>
    <td width="20%"  ><div align="center"></div></td>
    <td width="50%"  ><div align="center"><img src="assets/img/new_csjp.gif" alt="" width="75" height="75" /><br />
        <span style="font-size:18px">College of Saint John Paul II Arts and Sciences</span> <br />
        <em style="font-size:14px"><strong>(Formerly SJB IAS Cainta)</strong></em><br />
    <span style="font-size:14px">Tel: (02)655-2171 | Website: www.csjpii.edu.ph</span></span></div></td>
    <td width="20%">&nbsp;</td>
  </tr>
</table> -->

<?php
switch($_GET['action']){

	case 'choose':
?>








<p>&nbsp;</p>
<table width="644" border="0" align="center">
  <tr>
    <td width="178" valign="top" class="style11">School Year - Semester</td>
    <td width="425" valign="">
	<?php $sem = SEM::getallsems(array("sem_NAME"=>"DESC")); ?>
      <form action="<?php echo $_SERVER['REQUEST_URI']; ?>#choose" method="get">
        <input type="hidden" name="action" value="view" />
          <select name="sem_ID" id="sem_ID" onchange="this.form.submit()" style="width:100%">
          	<option></option>
            <?php foreach($sem as $key => $val){ ?>
            <option <?php if(isset($_GET['sem_ID'])) { if($_GET['sem_ID']==$val['sem_ID']){ echo "SELECTED=SELECTED "; }  } ?> value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?>
           	
            </option>
            <?php } ?>
            </optgroup>
          </select>	
      </form></td>
  
  </tr>
</table>

<p>&nbsp;</p>



<?php 
break;
case 'view':
?>


<table width="644" border="0" align="center">
  <tr>
    <td valign="top" class="style12" align="center">
	<b>Section Status Listing <br><?php
     $d = SEMSECTION::getSEM($_GET['sem_ID']);
							echo $d['sem_NAME']; ?>	
      </b> <br />
   (School Year - Semester)</td>
  </tr>
</table>

<?php	


	$sem = SEMSECTION::getAllbyID(array("a.course_ID"=>"DESC","semester_section_NAME"=>"ASC"),$_GET['sem_ID']);
		foreach($sem as $key => $val)
		{ 
				 
				 
		$id = $val['semester_section_ID']; ?>
      
						

<table width="800" border="0" align="center" cellpadding="3" cellspacing="0">
  <tr>
	<td class="style11"><b><?php $sem = SEMSECTION::getID($val['semester_section_ID']); ?>
                       
                          <?php $c = COURSE::getbyID($val['course_ID']);
                                        echo $c['course_INIT']; ?>
                          -<?php echo $val['semester_section_NAME']; ?></b>
                          
                          <?php
                          $count = SUBJSCHED::CountSubjectsBySection($id);
						  #echo $count;
                          ?>
                          </td>
    </tr>
    <tr>
    	<td>
        <!-- START -->
        <?php 
$subjects = SUBJSCHED::getActiveSubjectsBySection($id);
$count = SUBJSCHED::CountSubjectsBySection($id);
#$count;
?>
<table width="100%" border="1" cellpadding="3" cellspacing="0" class="table table-hover table-responsive table-striped text-md" >              
<tr>
           			  <th  width="10%"><div align="center" class="style6 style8 style9 style10">Code</div></th>
                                        <th  width="30%"><div align="center" class="style11">Description</div></th>
                                       	<th  width="10%"><div align="center" class="style11">Room</div></th>
                                        <th  width="5%"><div align="center" class="style11">Day</div></th>
                                        <th  width="20%"><div align="center" class="style11">Time</div></th>
                                        <th  width="5%"><div align="center" class="style11">Rem</div></th>
                                        <th  width="5%" ><div align="center" class="style11">Taken</div></th>
                                        <th><div align="center" class="style11">Instructor</div></th>
                   </tr>
                  <?php 
				  $x = 0;
				  foreach($subjects as $key => $val) { ?>
                  <tr>
                  	<td style="width:80px"><div align="center" class="style11">
                  	  <?php 
                   			 $ss = SUBJSCHED::getSched($val['subject_sched_ID']);
							 $s = SUBJECTS::getID($ss['subject_ID']);
							 
							# print_r($s);
                             echo $s['subject_CODE'];
							 
							 							 
                   		 ?>                    
               	    </div></td>
                    <td s><div align="center" class="style11">
                      <?= $s['subject_DESCRIPTION']; ?> 
                    (<?php echo $s['LEC_UNIT']; ?>/<?php echo $s['LAB_UNIT']; ?>)</div></td>
                    <td ><div align="center" class="style11">
                      <?php  $room = ROOM::getID($val['room_ID']); echo $room['room_NAME']; ?>
                    </div></td>
                    <td ><div align="center" class="style11"><?php echo substr($val['day'],0,3); ?></div></td>
                    <td ><div align="center" class="style11"><?php echo $val['start']."-".$val['end']; ?></div></td>
                    <td><div align="center" class="style11"><?php echo ($val['max_student']-SUBJSCHED::checkRemaining($val['subject_sched_ID'])); ?>                    </div></td>
                    <td ><div align="center" class="style11">
                    <?php
					$x+= SUBJSCHED::checkRemaining($val['subject_sched_ID']);
					?>
                      <?=  SUBJSCHED::checkRemaining($val['subject_sched_ID']); ?>
                    </div></td>
                    <td> <div align="center" class="style11"><?php
                    $ins = INSTRUCTORS::getSingle1($val['instructor_ID']);
					echo $ins['instructor_NAME'];
					?></div></td>
  </tr>
                <?php } ?>
                <tr>
                	<td></td>
                    <td></td>
                	<td></td>
                    <td></td>
                    
                    <td colspan="2" align="right" class="style11">Average: </td>
                    <td align="center"  class="style11"><?php $x; ?>
                    <?= round($x/$count); ?>
                    </td>
                    <td></td>
                </tr>    
              </table>


        <!-- END -->
        </td>
    </tr>
    </table>
    
<br />
            <?php } ?>
            
       









<?php
break;
default: href("?action=choose");
}
?>

<p>&nbsp;</p>
</body>
</html>
