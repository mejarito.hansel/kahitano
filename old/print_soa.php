<?php
include('init.php');
				$id = clean($_GET['id']);
				$accinfo =  SUBJENROL::getenrollmentofstudentpersem($_GET['id'],$_GET['sem_ID']);
				$info =  USERS::viewSingleStudent($accinfo['si_ID']);
				// var_dump($info);

				#$mygrade = SUBJENROL::gradebyid($id);
				#var_dump($mygrade);

				$po_info = SUBJSCHED::getPaymentOptionByID($accinfo['po_ID']);
				// var_dump($po_info);
				$user_id = $user['account_ID'];

				?>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Statement Of Account</title>
	<style type="text/css">
		body{
			padding:0px;
			font-family: Arial;
			font-size:12px;
		}
		@media screen{
			#print{
				position:absolute;
				right:10px;
			}
			.a4-container {
				position: absolute;
				top: 0px;
				left: 0px;
				width: 595px;
				height: 842px;
				border: 3px solid black;
				/*background: red;*/
			}
			.underline:nth-of-type(1){
				position:absolute;
				top: 12.5rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.underline:nth-of-type(2){
				position:absolute;
				top: 13.7rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.underline:nth-of-type(3){
				position:absolute;
				top: 15rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.hr{
				position: absolute;
				top: 570px;
				left: -3px;
				width: 595px;
				height: 80px;
				border: 3px solid black;
			}
			table{
				position:absolute;
				top: 5.5rem;
				left: 12rem;
				border: 2px solid black;
				padding: 0px;
				border-spacing: 0px;
			}
			table td{
				padding:.1rem;
				width:230px;
				border: 1.4px solid black;
			}
			table tr td p{
				margin: 0px !important;
				padding: 0px !important;
			}
		}
		@media print{
			#print{
				position: absolute;
				display: none;
				right:10px;
			}
			.a4-container {
				position: absolute;
				top: 0px;
				left: 0px;
				width: 595px;
				height: 842px;
				border: 3px solid black;
				/*background: red;*/
			}
			.underline:nth-of-type(1){
				position:absolute;
				top: 12.5rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.underline:nth-of-type(2){
				position:absolute;
				top: 13.7rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.underline:nth-of-type(3){
				position:absolute;
				top: 15rem;
				left: 28.5rem;
				width:6rem;
				height:0rem;
				border-bottom: 1px solid black;
			}
			.hr{
				position: absolute;
				top: 570px;
				left: -3px;
				width: 595px;
				height: 80px;
				border: 3px solid black;
			}
			table{
				position:absolute;
				top: 5.5rem;
				left: 12rem;
				border: 2px solid black;
				padding: 0px;
				border-spacing: 0px;
			}
			table td{
				padding:.1rem;
				width:230px;
				border: 1.4px solid black;
			}
			table tr td p{
				margin: 0px !important;
				padding: 0px !important;
			}
		}
		@page {
			margin-top: 1in;
			margin-left: 1.10in;
		}
		.space{
			margin-top: 31px;
		}
		.space1{
			margin-top: 10px;
		}
	</style>
  	<script src="<?= HOME; ?>assets/jquery/1.11.3/jquery.min.js"></script>
</head>
<body onload="window.print();">
<!-- <body> -->
	<?php
				 #var_dump($po_info);
				  
				    $po_lab_rate = $po_info['po_LAB'];
					$po_lec_rate = $po_info['po_LEC'];	
					$po_misc = $po_info['po_MISC'];
					
					$accinfo['subject_sched_ID'] = explode(",",$accinfo['subject_sched_ID']);
					$length = sizeof($accinfo['subject_sched_ID']);
					$total_lec = 0;
					$total_lab = 0;
					$acc_ID = $accinfo['acc_ID'];
					
					for($i=0;$i<$length;$i++)
					{
						$subject_sched_ID = $accinfo['subject_sched_ID'][$i];
						$si_ID = $id;
						
							$subject_sched = SUBJSCHED::getID($subject_sched_ID);
							$subject_info = SUBJECTS::getID($subject_sched['subject_ID']);	
							if(isset($subject_info['TOTAL_UNIT'])){
                $total_lab += $subject_info['TOTAL_UNIT'];
              }else{
                $total_lec += $subject_info['LEC_UNIT'];
                $total_lab += $subject_info['LAB_UNIT'];
              }
							
						
					}

					$assess = explode(",",$accinfo['af_ID']);
					 $y = count($assess);
					 $afee = 0;
					 for($x=0;$x<$y;$x++)
					 {
						$af = SUBJSCHED::getFAbyID($assess[$x]);
						$afee += $af['af_PRICE'];
					 }	
				
				$grad_fee = 0;	
				
					
				  if($accinfo['gf_ID'] != '')
				  {
					$gf = SUBJSCHED::getGFbyID($accinfo['gf_ID']);
					$grad_fee = $gf['gf_AMOUNT'];
				  }		
				  
				  
				   $thesis_fee = 0;		
					  if($accinfo['tf_ID'] != '')
					  {
						$gf = SUBJSCHED::getTFbyID($accinfo['tf_ID']);
						$thesis_fee = $gf['tf_AMOUNT'];
					  }		
					
				        $lab_fee = $total_lab*$po_lab_rate;
						$lec_fee = $total_lec*$po_lec_rate;
						
    $SNPL = $po_info['po_SNPL'];
  	$install_fee = $po_info['po_installment_fee'];
 
		$discount_tf = ($lab_fee+$lec_fee)*$po_info['po_discount_tf'];
 
 		$discount_mf = ($po_misc)*$po_info['po_discount_misc'];
  
 		$additional_tf = ($lab_fee+lec_fee)*$po_info['po_additional_tf'];
  
     	$additional_mf = ($po_misc)*$po_info['po_additional_misc'];
 		 $total_tf = $thesis_fee + $grad_fee + $afee + $po_misc + $lab_fee + $lec_fee - ($discount_tf + $discount_mf) + ($additional_tf + $additional_mf) + $install_fee;
  
				 ?>
	<div class="a4-container">
			<div style="margin-top:1rem;"></div>
			<p style="margin-left:1.5rem; line-height:1.25rem;">MOL Magsaysay Maritime Academy Inc.<br>Statement of Accounts<br>FTM August 2018</p>
			
			<p style="margin-left:1.5rem; line-height:1.25rem;">Student #<br>Student Name:<br>
	          	<?php if ($SNPL > 0) { ?>
					SNPL Approval: 	 
					<p class="space1"></p>
	          	<?php } ?>

	          	<?php if ($SNPL == 0) { ?>
					<p class="space"></p>
	          	<?php } ?>
			</p>
			<br>
			<p style="margin-left:1.5rem; line-height:1.25rem;">
				Total Assessed Tuition and Other Fees
			<br>
          	<?php if ($SNPL > 0) { ?>
				Amount extended to SNPL
          	<?php } ?>  
			<br>
          	<?php if ($SNPL > 0) { ?>
				Collectible from Student -
          	<?php } ?>  
			</p>
			<table>
				<tr>
					<td>
						<p style="text-align:center;"><?php echo strtoupper($info['student_ID']); ?></p>		
					</td>
				</tr>
				<tr>
					<td>
						<p style="text-align:center;"> <?php echo strtoupper($info['si_LNAME']).", ".strtoupper($info['si_FNAME'])." ".strtoupper($info['si_MNAME']);  ?> </p>		
					</td>
				</tr>
          	<?php if ($SNPL > 0) { ?>
				<tr>
					<td>
						<p><?= $po_info['po_NAME']; ?></p>		
					</td>
				</tr>
          	<?php } ?>  
			</table>

			<p style="position:absolute; top:10.8rem; left:28.5rem;"><?= money($total_tf);
					
					
					$down = 0; 
					
					//$bal =$total_tf-$down;
          if ($SNPL > 0) {
            #$bal = ($total_tf*$SNPL)-$down;
             $bal = ($total_tf - ($total_tf*$SNPL) -$down);
          }else{
            $bal = ($total_tf)-$down;
          }
					
					?>
                    <?php
				if($po_info['po_DATE1'] == NULL)
				{
				$slice = 1;
				}else if($po_info['po_DATE2'] == NULL){
				$slice = 2;
				}else if($po_info['po_DATE3'] == NULL){
				$slice = 3;
				}else if($po_info['po_DATE4'] == NULL){
				$slice = 4;
				}else if($po_info['po_DATE5'] == NULL){
				$slice = 5;
				}
				
				$slice -= 1;
				#msgbox($slice);
				
				
				
					
					
					
				?></p>
				<span class="underline"></span>
          	<?php if ($SNPL > 0) { ?>
      		<!-- Amount extended to SNPL  -->
				<p style="position:absolute; top:12rem; left:28.5rem;">
					(<?= ($SNPL*100); ?>%)  P <?php echo $snpl = money($total_tf*$SNPL) ?>
				</p>
				<span class="underline"></span>

          	<?php } ?>  


			<!-- collectible from student -->
          	<?php if ($SNPL > 0) { ?>
			<p style="position:absolute; top:13.3rem; left:28.5rem;">
				<?php echo $snpl = money($total_tf-($total_tf*$SNPL)) ?>
			</p>
			<span class="underline"></span>

          	<?php } ?>  
			<br>

			<p style="margin-left:1.5rem; line-height:1.25rem;">Down Payment/ Payments Made</p>
			<p style="margin-top:-10px; margin-left:7.5rem; line-height:1.25rem; float:left;">Date</p>
			<p style="margin-top:-10px; margin-left:10.5rem; line-height:1.25rem; float:left;">OR #</p>
			<p style="margin-top:-10px; margin-left:6.5rem; line-height:1.25rem; float:left;">Amount</p>

            <?php if($accinfo['or_ID'] != ''){ ?>

            <?php $d = ACCT::checkORData($accinfo['or_ID'],$id,$sem_ID); 

            ?>
			
			<?php $paymentPerSemTF = ACCT::getAllPaymentPerSemTF($_GET['id'],$_GET['sem_ID']);
				// print_r($paymentPerSemTF);
				//echo $paymentPerSemTF['0']['payment_DATE'];
				//echo count($paymentPerSemTF);
				for($i = 0; $i < count($paymentPerSemTF) ; $i++)
				{
					?>
						<p style="margin-top: -10px; margin-left:7.5rem; line-height:1.25rem; float:left;">
							<?php echo date("j-M-y",strtotime($paymentPerSemTF[$i]['payment_DATE']));?>
						</p>
						<p style="margin-top: -10px; margin-left:8.5rem; line-height:1.25rem; float:left;">
							<?php echo $paymentPerSemTF[$i]['payment_OR'];?>
						</p>
						<p style="margin-top: -10px; margin-left:8rem; line-height:1.25rem; float:left;">
							<?php $down = $paymentPerSemTF[$i]['payment_AMOUNT'];

								if ($SNPL > 0) {
									$bal = ($total_tf - ($total_tf*$SNPL)-$down);
								}else{
									$bal = ($total_tf)-$down;
								}
								echo number_format($down,2, '.', ','); 
							?>
						</p>
					<?php
				}
			?>
			
        	<?php } ?>

			<br>
			<br>
			<br>
			<br>
			<p style="margin-left:1.5rem; line-height:1.25rem; float:left;">Balance</p>
			<p style="margin-left:24.5rem; line-height:1.25rem; float:left;"><?php echo number_format($bal,2, '.', ','); ?></p>

			<br>
			<br>
			<br>
			<br>
			<p style="margin-left:1.5rem; line-height:1.25rem; float:left;">Due Dates</p>
			<p style="margin-left:1rem; line-height:1.25rem; float:left; font-weight: bold;">ON or BEFORE</p>

			<br>
			<br>
			<br>
			<br>
			<?php
				if($slice == 1)
				{
				$p1 = $bal;
				}else if($slice == 2)
				{
				$p1 = floor($bal/2);
				$p2 = $bal-$p1;
				}else if($slice == 3)
				{
				
					$p1 = floor($bal/3);
					$p2 = floor(($bal-$p1) / 2);
					$p3 = $bal - ($p1+$p2);
				}else if($slice == 4)
				{
					$p1 = floor($bal/4);
					$p2 = floor(($bal-$p1) / 3);
					$p3 = $p2;
					$p4 = $bal-($p1+$p2+$p3);
				}	
				?>
        <?php if($accinfo['or_ID'] != ''){ ?>
            <?php if($slice >= 1){ ?>

			<p style="margin-top:-10px; margin-left:6.5rem; line-height:1.25rem; float:left;"><?php echo date("j-M-y",strtotime($po_info['po_DATE1'])); ?></p>
			<p style="margin-top:-10px; margin-left:18.5rem; line-height:1.25rem; float:left;">P<?php echo number_format($p1,2, '.', ','); ?></p>

            <?php } ?>

			<br>
			<br>
            <?php if($slice >= 2){ ?>

			<p style="margin-top:-14px; margin-left:6.5rem; line-height:1.25rem; float:left;"><?php echo date("j-M-y",strtotime($po_info['po_DATE2'])); ?></p>
			<p style="margin-top:-14px; margin-left:18.5rem; line-height:1.25rem; float:left;">P<?php echo number_format($p2,2, '.', ','); ?></p>

            <?php } ?>

			<br>
			<br>
            <?php if($slice >= 3){ ?>

			<p style="margin-top:-18px; margin-left:6.5rem; line-height:1.25rem; float:left;"><?php echo date("j-M-y",strtotime($po_info['po_DATE3'])); ?></p>
			<p style="margin-top:-18px; margin-left:18.5rem; line-height:1.25rem; float:left;">P<?php echo number_format($p3,2, '.', ','); ?></p>

            <?php } ?>

			<br>
			<br>
            <?php if($slice >= 4){ ?>

			<p style="margin-top:-24px; margin-left:6.5rem; line-height:1.25rem; float:left;"><?php echo date("j-M-y",strtotime($po_info['po_DATE4'])); ?></p>
			<p style="margin-top:-24px; margin-left:18.5rem; line-height:1.25rem; float:left;">P<?php echo number_format($p3,2, '.', ','); ?></p>

            <?php } ?>

			<br>
			<br>
            <?php if($slice >= 5){ ?>

			<p style="margin-top:-18px; margin-left:6.5rem; line-height:1.25rem; float:left;"><?php echo date("j-M-y",strtotime($po_info['po_DATE5'])); ?></p>
			<p style="margin-top:-18px; margin-left:18.5rem; line-height:1.25rem; float:left;">P<?php echo number_format($p3,2, '.', ','); ?></p>

            <?php } ?>

       <?php } ?>
			<div class="hr">
				<p style="margin-top:1rem; margin-left:2rem; float:left;">You may send your payment thru MMMA BPI Account S/A#</p>
				<p style="margin-top:1rem; margin-left:1rem; float:left;">{}</p>
				<br>
				<br>
				<p style="margin-top:1rem; margin-left:1rem;">Please send us copy of Deposit Slip for proper posting and updating of your account.</p>
			</div>
			<p style="margin-top:8rem; margin-left:1rem; font-weight:bold;">REMINDERS:</p>
			<p style="margin-top:1rem; margin-left:1rem;">* NO DP/Reservation Received</p>
			<p style="margin-top:1rem; margin-left:1rem; font-weight:bold;">*Pending SNP: Requirements: PLS SEND to MMMA SNPL Agreement with signature of CO-MAKER</p>
			<p style="margin-top:1rem; margin-left:1.5rem;">*None submission of Requirements will result to full assessment of tuition fee due and demandable</p>
			<p style="margin-top:1rem; margin-left:12.5rem;">next month/cut off.</p>
			<p style="margin-top:1rem; margin-left:1rem;">*Late payment is subject to penalty.</p>
	</div>
	<button id="print" onclick="myFunction()">print</button>
	<script type="text/javascript">
		function myFunction() {
		    window.print();
		}
	</script>
</body>
</html>