<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: File Maintenance / Update User Account, DESCRIPTION: User visited Update User Account";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}

if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
	if ($user) {
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();
		SESSION::login($user);
        msgbox("Login Success");

		AUDIT::insert($user['account_ID'],"LOGGED IN");

		if(isset($_SESSION['previous_page']))
		{
			header("location:$_SESSION[previous_page]");
		}else{
			HTML::redirect();
		}

	} else {
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}
	//QUERIES
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include(LAYOUTS . "styles.php"); ?>
<?php include(LAYOUTS . "scripts.php"); ?>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
<link rel="stylesheet" type="text/css" href="<?php echo HOME; ?>old/assets/css/custom-style.css">
<script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>
<title>User Management | School Management System</title>
<style>
	.modal-dialog {
		overflow-y: initial !important
	}
	.modal-body {
		height: auto;
		overflow-y: auto;
	}
	.wrapper {
		margin-top: 9%;
	}
	#myTable {
		margin-top: 1rem;
	}
	#myTable thead th {
		font-weight: normal;
		font-size:12px;
	}
</style>
</head>
<body>
<div class="modal fade" id="modalUserManagement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header" style='border-bottom:none;'>
      <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
      <h4 class="modal-title text-center">Preview Account</h4>
    </div>
    <div class="modal-body" style="height: auto;">
    </div>
    <div class="modal-footer" style='border-top:none;'>
      <button type="button" class="btn btn-xs btn-default btn-cancel" data-dismiss="modal">Close</button>
      <button type="button" id="btnSaveAccountInfo" class="btn btn-xs btn-primary">Save</button>
    </div>
  </div>
</div>
</div>
<div class="modal fade" id="modalCreateUserManagement" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header" style='border-bottom:none;'>
      <h4 class="modal-title text-center">Create New User</h4>
    </div>
    <div class="modal-header" style='border-bottom:none; margin:0px;'>
	<div class="alert-createusermanagement alert alert-danger" role="alert">
		<h4 class="alert-heading">System Message</h4> 
		<p>Please fill out this form completely</p>
		</div>
	</div>
    <div class="modal-body" style="height: auto;">
    	
    </div>
    <div class="modal-footer" style='border-top:none;'>
      <button type="button" class="btn btn-xs btn-default btn-cancel" data-dismiss="modal">Close</button>
      <button type="button" id="btnCreateUser" class="btn btn-xs btn-primary">Save</button>
    </div>
  </div>
</div>
</div>
<!-- top nav -->
<?php include(LAYOUTS . 'top_nav.php'); ?>
            <!-- end nav -->
<div class="container">
<!-- banner -->
<?php include(LAYOUTS . "banner.php"); ?>
<!-- end banner -->
<!-- alert messages -->
<?php 

SESSION::DisplayMsg(); 

?>
<div class="row">

<!-- left nav -->

<?php
if (!SESSION::isLoggedIn()) {
//audit
?>
<div class="col-md-3">
	<?php include(PAGES."navigation.php");?>
</div>
<div class="col-md-9">
    <div class="well">
        <form class="form-horizontal" method="POST">

            <fieldset>
                <legend>Login</legend>

                <!-- if errors -->
                <?php SESSION::DisplayMsg(); ?>
                    <!-- end errors -->

                    <div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
                        <label for="inputUsername" class="col-lg-2 control-label">Username</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
                        </div>
                    </div>
                    <div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
                        <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                        <div class="col-lg-10">
                            <input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-offset-9">
                            <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>

                            <button type="submit" class="btn btn-success" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
                        </div>
                    </div>

            </fieldset>
        </form>
    </div>
</div>

<?php   
	}else{
	AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
?>

<div class="col-md-3">
<?php include(PAGES."navigation.php");?>
</div>
<div class="col-md-9">
	<div class="alert-usermanagement alert alert-success" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		<span aria-hidden="true">&times;</span>
		</button> 
		<h4 class="alert-heading">System Message</h4> 
		<p>You've successfully updated <span id="upt-fullname"></span>.</p>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="container row">
				<h4 style="margin-top: 5px; margin-bottom: 5px" class='pull-left'> Update User Account</h4>
			<!-- <a class="btn btn-success btn-sm pull-right btnAddAcc">Add Account</a> -->
			</div>
		</div>
		<div class="panel-body">                                               
				<div class="container"> 
					<div class="row"> 
						<table id="tableUserManagement" class="display" style="width:100%">
					        <thead>
					            <tr>
					                <th>Account ID</th>
					                <th>First Name</th>
					                <th>Last Name</th>
					                <th>Account Username</th>
					                <th>User Level</th>
					                <th>Action</th>
					            </tr>
					        </thead>
					        <tbody>
					            
					        </tbody>
					    </table>
					</div> 
				</div>
		</div>
	</div>
</div>
<?php } ?>
</div>
<?php include (LAYOUTS . "footer.php"); ?>
<script src="<?php echo HOME; ?>old/assets/js/custom/access-control.js"></script>
</div>
<script type="text/javascript">
$(window).ready(function() {
    setTimeout(function() {
        // $.main.accesscontrol.updateAccountLevel();
        $.main.accesscontrol.createAccountInfo();
    }, 100);
})
</script>
</body>
</html>