<?php
include('init.php');
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Academic Affairs / Clearance Management, DESCRIPTION: User visited Clearance Management";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
header('location: clearance.php');
?>