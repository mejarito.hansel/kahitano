<?php
include('init.php');
$siiid = clean($_GET['id']);
$getsingle = USERS::getSingle(array("si_ID"=>"DESC"),$siiid  );
$getsingle1 = USERS::getSingleSAI(array("si_ID"=>"DESC"), $siiid );
// $user_id = $user['account_ID'];
// $action_event = "Print";
// $event_desc = "Module: 2 Registrar / 2.2 Find Student, DESCRIPTION: User clicked Print Good Moral for ".$getsingle['si_FNAME'].' '.$getsingle['si_MNAME'].' '.$getsingle['si_LNAME'];
// $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/jquery.dataTables.min.css">
    <!-- <link href="ams\old\pages\htmlfolder\assets/css/style.css" rel="stylesheet"> -->
    <link href="pages\htmlfolder\assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="pages\htmlfolder\assets/css/style.css" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>AMS</title>

</head>
 <?php if(!isset($_GET['action'])){ ?>
<body>
<?php }else{ ?>
<body onload="window.print();">
<?php } ?>
  <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>?action=print&id=<?php echo $_GET['id']; ?>">
  <div class="container text-capitalize" style="margin-bottom: 15px; margin-top: 15px">
      <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12">
          <img src="pages\htmlfolder\assets/img/logo.png">
        </div>
      </div>
      <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12">
        <h4 class="user_info">GOOD MORAL</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-2 col-sm-2 col-md-2">
        <span>Student no:</span>
      </div>
      <div class="col-lg-10 col-sm-10 col-md-10">
        <span><?= $getsingle['student_ID']; ?></span>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-2 col-sm-2 col-md-2">
        <span>Student Name:</span>
      </div>
      <div class="col-lg-10 col-sm-10 col-md-10">
        <span><?= $getsingle['si_LNAME']; ?>, <?= $getsingle['si_FNAME']; ?> <?= $getsingle['si_MNAME']; ?></span>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-lg-2 col-sm-2 col-md-2">
        <span>Remarks:</span>
      </div>
      <div class="col-lg-10 col-sm-10 col-md-10">
        <span>
          <?php if(!isset($_GET['action'])){ ?>
          <textarea name="remarks"></textarea>
          <button type="submit">Print</button>
          <?php }else{ ?>
            <?php echo $_POST['remarks']; 
            // $user_id = $user['account_ID'];
            // $action_event = "Print";
            // $event_desc = "Module: 2 Registrar / 2.2 Find Student, DESCRIPTION: User clicked Print of ".$getsingle['si_FNAME'].' '.$getsingle['si_MNAME'].' '.$getsingle['si_LNAME'].' with a remakrs of '.$_POST['remarks'];
            // $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);



            ?>
          <?php } ?>
        </span>
      </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12">
        <span>Signatories:</span>
        <div class="col-lg-4 col-sm-4 col-md-4 user_name">
          <br>
        <span><?= $user['account_FNAME']." ".substr($user['account_MNAME'],0,1).". ".$user['account_LNAME']; ?></span>
        </div>
        <div class="col-lg-4 col-sm-4 col-md-4 user_fullname font-italic">
          <p class="">Admin Staff</p>
        </div>
      </div>


       <br /><br />Released Date: <br />
               &nbsp;&nbsp;&nbsp;<?php echo date("M j, Y"); ?>
    </div>
  </div>

    <script src="pages\htmlfolder\assets/js/jquery.min.js"></script>
    <script src="pages\htmlfolder\assets/js/jquery.dataTables.min.js"></script>
    <script src="pages\htmlfolder\assets/js/bootstrap.min.js"></script>

    </form>

</body>
</html>
<?php 

$user_id = $user['account_ID'];
  $action_event = "Print";
  if(!isset($_POST['remarks']))
  {

  $event_desc = "Module: 2 Registrar / 2.2 Find Student, DESCRIPTION: User clicked Print Good Moral for ".$getsingle['si_FNAME'].' '.$getsingle['si_MNAME'].' '.$getsingle['si_LNAME'];

  }else{

  $event_desc = "Module: 2 Registrar / 2.2 Find Student, DESCRIPTION: User clicked Print Good Moral of ".$getsingle['si_FNAME'].' '.$getsingle['si_MNAME'].' '.$getsingle['si_LNAME'].' with a remakrs of '.$_POST['remarks'];

  }
 $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
?>