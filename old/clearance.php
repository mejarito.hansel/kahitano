<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();


if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}

	



if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
    

    
  
    
	if ($user) 
	{
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();
	
			  
	
			
				SESSION::login($user);
                msgbox("Login Success");
               
               
				AUDIT::insert($user['account_ID'],"LOGGED IN");
				
				
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}



	//QUERIES




	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
	  	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

      <title>HOME | CSJP-II-AS</title>
	</head>

	<body>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
            
			<!-- alert messages -->
			<?php 
			
			SESSION::DisplayMsg(); 
			
			
			
			?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
				
				//audit
				
				
				
					?>
				<div class="col-md-3">
            		<?php include( PAGES."navigation.php");?>
				</div>
                <div class="col-md-9">
                	<div class="well">
                    	<form class="form-horizontal" method="POST">
                    		<fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 

								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>

								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>
									</div>
								</div>
                                
                                <div class="form-group row">
									<div class="col-lg-offset-9">
										<button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                                
                            </fieldset>
                    	</form>
                    </div>	
                </div>
                
                <?php   }else{
				
				AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
				
				?>

				<div class="col-md-3">
				<?php include(PAGES."navigation.php");?>
				</div>
              
              	<div class="col-md-9">
	<div class="panel panel-default">

		<div class="panel-heading">
			<table>
        		<tr>
        			<td width="50%">
						<h4 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">Clearance <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
					</td>
				 	<td>
            			<div class="input-group col-md-12 hide">
              				
              				<input id="searchItem" class="searchItem form-control"  type="text" autofocus="autofocus"  style="text-transform: uppercase;"/>
                            
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            			</div>
        			</td>
        			<td width="5%">
	    				<div class="col-md-1">   
	    					<a id="asd" class='btn btn-success btn-ms P5C13_ADDBLOCK P8C5_ADDBLOCK' >Add Block</a>
	    				</div>
	    			</td>
	    		</tr>
	    	</table>
		</div>
		<div class="panel-body" id="getaccountidforclearance" data-acct="<?php echo $user['account_ID']; ?>"><br>
			<!-- <div class="row">
            	<div class="form-group">
	                <div class="col-sm-5">
	                    <input name='' id="myInput" placeholder="Search by Name or Applicant Number" class='form-control' onkeyup="myFunction()" type='text' required autofocus style="font-style: italic;"/>
	                </div>
            	</div>
           	</div><br> -->
           	<div id="dvData table table-responsive">
	        	<table id="myTable1" class="table table-hover table-striped text-md" style="width:100%;">
	        		<thead>
	        			<div class="col-sm-5">
                                    <input name='' id="myInput" placeholder="Search by Applicant No., First Name, Middle Name, Last Name or Status" class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
                                </div>
	        				<tr>
				                <th>Student No:</th>
					            <th>Student Name:</th>
					            <th>Department</th>
					           	<!-- <th>Blocked By:</th> -->
					            <!-- <th>Date Blocked:</th> -->
					            <!-- <th>Reason:</th> -->
					            <!-- <th>Unblocked By:</th> -->
					            <!-- <th>Date Unblocked:</th> -->
					            <!-- <th>Reason:</th> -->
					            <th>Action</th>
				            </tr>
	        		</thead>
	        		<tbody>
	        			
	        		</tbody>
			    </table>
		    </div>
		  	<div class="modal fade" id="myModal" class="classModal" role="dialog">
			    <div class="modal-dialog">
			      <div class="modal-content">
			        <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			          <h4 style="">Block Student</h4>
			          <center><h4 class="hidden" style="color:#b94a48;"id="fillId">Please Fill-Up This Form Completely</h4><center>
			          	<center><h4 class="hidden" style="color:green;"id="banned">Successfully Banned Student</h4><center>
			        </div>
			        <div class="modal-body" style="padding:40px 140px;">
			            <div class="form-group">
			            	
			            		
			             <!--  <input type="text" class="form-control" id="searchCadetID" placeholder="Search Student ID"> -->
			           
			            <select class='form-control'  id="cadetName" style="width: 100%" required>
						</select>
			         	<p id="123"></p>
			            </div>
							<center><i class="fa fa-circle-o-notch fa-spin hidden" id="spin" style="font-size:24px"></i></center><br>
			            <div class="form-group">
			            	<p class="hidden" id="semYear">Please select semester and year</p>
			               <select class="form-control" id="txtYrNSem">
			               		<option value="">-- Select Year & Semester --</option>
			               </select>
			            </div>
			            <div class="form-group">
			            	<p class="hidden" id="dept">Please select department</p>
			               <select class="form-control" id="txtDepartment">
			               		<option value="">-- Select Department --</option>
			               </select>
			            </div>
			            <div class="form-group">
			            	<p class="hidden" id="reas">Please input some reason</p>
			            	<textarea class="form-control" id="txtReason" placeholder="Reason for blocking"></textarea>
			            </div>
			          	<input type="submit" class="btn btn-success btn-block" value="Block Student" id="btnBlockCadet">
			        </div>
			 
			      </div>
			    </div>
		  	</div> 
		  	<div class="modal fade" id="myModal1" role="dialog">
			    <div class="modal-dialog">
			      <div class="modal-content">
			        <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			          <h4 style="">UnBlock Student</h4>
			        </div>
			        <div class="modal-body" style="padding:40px 140px;">
				      	<div class="form-group">
				      		<center><h4 style="color:green;"class="hidden" id="nsr">Successfully Unblocking</h4></center>
			            	<label>Student ID</label>
			               	<input class="form-control" id="cadet_id" type="text" disabled>
			            </div>
			            <div class="form-group">
			            	<label>Student Fullname</label>
			               	<input class="form-control" id="cadet_name" type="text" disabled>
			            </div>
			            <div class="form-group">
			            	<label>Department Banned</label>
			               	<input class="form-control" id="cadet_dept" type="text" disabled>
			            </div>
			            <div class="form-group">
			            	<center><h4 style="color:red;"class="hidden" id="rsn">Please input some reason for unblocking</h4></center>
			            	<label>Reason for Unblocking</label>
			               	<textarea class="form-control" id="cadet_reason"></textarea>
			            </div>
			            <input type="submit" class="btn btn-success btn-block" value="Block Cadet" id="btnUnblock">
			        </div>
			      </div>
			    </div>
		  	</div> 
		  	<div class="modal fade" id="myModal2" role="dialog">
			    <div class="modal-dialog">
			      <div class="modal-content">
			        <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			          <h4 style="">View Full Details</h4>
			        </div>
			        <div class="modal-body" style="padding:40px 140px;">
				      	
			        </div>
			      </div>
			    </div>
		  	</div> 
	 	</div>
	</div>
</div>
				<?php } ?>
				  	
							</div>
				            <?php 
							?>
							<?php include (LAYOUTS . "footer.php"); ?>
 				<script src="<?php echo HOME; ?>old/assets/js/custom/clearance-portal.js"></script>
				</div>
				<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
			  	<script type="text/javascript">
			  	$( window ).ready(function(){
			        setTimeout(function() {

			      		$.main.clearance.getAllClearance();
			 
			        },100);
			  	})
			    // function myFunction() {
			    //   var input, filter, table, tr, td, i;
			    //   input = document.getElementById("myInput");
			    //   filter = input.value.toUpperCase();
			    //   table = document.getElementById("myTable");
			    //   tr = table.getElementsByTagName("tr");
			    //   for (i = 0; i < tr.length; i++) {
			    //     td = tr[i].getElementsByTagName("td")[0];
			    //     td2 = tr[i].getElementsByTagName("td")[1];
			    //     td1 = tr[i].getElementsByTagName("td")[4];
			    //     if (td) {
			    //       if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
			    //         tr[i].style.display = "";
			    //       } else {
			    //         tr[i].style.display = "none";
			    //       }
			    //     }       
			    //   }
			    // }

  				function exportTableToCSV($table, filename) {

	    		var $rows = $table.find('tr:has(td)'),

		      	// Temporary delimiter characters unlikely to be typed by keyboard
		      	// This is to avoid accidentally splitting the actual contents
		      	tmpColDelim = String.fromCharCode(11), // vertical tab character
		      	tmpRowDelim = String.fromCharCode(0), // null character

	      		// actual delimiter characters for CSV format
	      		colDelim = '","',
	      		rowDelim = '"\r\n"',

	      		// Grab text from table into CSV formatted string
	      		csv = '"' + $rows.map(function(i, row) {
	        	var $row = $(row),
	          	$cols = $row.find('td');

	        	return $cols.map(function(j, col) {
	          	var $col = $(col),
	            	text = $col.text();

	          	return text.replace(/"/g, '""'); // escape double quotes

	        	}).get().join(tmpColDelim);

	      		}).get().join(tmpRowDelim)
	      		.split(tmpRowDelim).join(rowDelim)
	      		.split(tmpColDelim).join(colDelim) + '"';

	    		// Deliberate 'false', see comment below
	    		if (false && window.navigator.msSaveBlob) {

	      		var blob = new Blob([decodeURIComponent(csv)], {
	        		type: 'text/csv;charset=utf8'
	      			});

			      // Crashes in IE 10, IE 11 and Microsoft Edge
			      // See MS Edge Issue #10396033
			      // Hence, the deliberate 'false'
			      // This is here just for completeness
			      // Remove the 'false' at your own risk
			      window.navigator.msSaveBlob(blob, filename);

	    		} else if (window.Blob && window.URL) {
	      		// HTML5 Blob        
	      		var blob = new Blob([csv], {
	        		type: 'text/csv;charset=utf-8'
	      			});
	      		var csvUrl = URL.createObjectURL(blob);

	      		$(this)
	        		.attr({
	          			'download': filename,
	          			'href': csvUrl
	        		});
	    		} else {
	      		// Data URI
	      			var csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

				      $(this)
				        .attr({
				          'download': filename,
				          'href': csvData,
				          'target': '_blank'
				        });
				    }
			  	}
			  	$("#export").on('click', function(event) {
			     console.log("test")
				    var args = [$('#dvData>table'), 'export.csv'];

				    exportTableToCSV.apply(this, args);

				    // If CSV, don't do event.preventDefault() or return false
				    // We actually need this to be a typical hyperlink
			  	});
			    $(function () {
			        $('#datetimepicker4').datetimepicker({
			    	});
			    });
  				</script>
	</body>
</html>
<script type="text/javascript">
    function myFunction() {
        var input, filter, table, tr, td, i;
          input = document.getElementById("myInput");
          console.log(input);
          filter = input.value.toUpperCase();
          table = document.getElementById("myTable1");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td2 = tr[i].getElementsByTagName("td")[1];
            td1 = tr[i].getElementsByTagName("td")[2];
            if (td) {
              if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }   
        }
    }
</script>
<style type="text/css">
    .dataTables_filter, .dataTables_info { display: none; }

}

</style>