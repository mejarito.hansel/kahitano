<?php 
include('init.php');
SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
            ROOM::addroom(Request::post());
			
			break;
			case "edit": ROOM::update_room($_GET['id'],Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ROOM::delete($_GET['id']); break;
		
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>SCHOOL MANAGEMENT SYSTEM</title>
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			
			<?php SESSION::DisplayMsg(); ?>
			
			<div class="row">
			
            <?php
				if (!SESSION::isLoggedIn()) {
					?>
				
                <?php   }else{?>
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
                <?php } ?>
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
					case 'choose': 
                    ?>
                   	<div class="panel panel-default">
	                    <div class="panel-heading">
	                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
	                            <i class="fa fa-folder-o"></i> Section Status Listing </h3> </div>
	                    <div class="panel-body">
	                        <table width="644" border="0" align="center">
							  <tr>
							    <td width="178" valign="top" class="style11">School Year - Semester</td>
							    <td width="425" valign="">
								<?php $sem = SEM::getallsems(array("sem_NAME"=>"DESC")); ?>
							      <form action="<?php echo $_SERVER['REQUEST_URI']; ?>#choose" method="get">
							        <input type="hidden" name="action" value="view" />
							          <select name="sem_ID" id="sem_ID" onchange="this.form.submit()" style="width:100%">
							          	<option></option>
							            <?php foreach($sem as $key => $val){ ?>
							            <option <?php if(isset($_GET['sem_ID'])) { if($_GET['sem_ID']==$val['sem_ID']){ echo "SELECTED=SELECTED "; }  } ?> value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?>
							           		
							            </option>
							            <?php } ?>
							            </optgroup>
							          </select>	
							      </form></td>
							  
							  </tr>
							</table>
	                    </div>
	                </div>


	                <?php 
					break;
					case 'view':
					?>

					<div class="panel panel-default">
	                    <div class="panel-heading">
	                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
	                            <i class="fa fa-folder-o"></i> Section Status Listing 


	                             <span class="pull-right"><a target="_new" href="report_semester_per_subject.php?action=view&sem_ID=<?= $_GET['sem_ID']?>" class="btn btn-success P4C8_PRINT P5C9_PRINT"><i class=" fa fa-print"></i> Print</a></span>
	                        </h3> 

                        </div>
	                    <div class="panel-body">
	                        <table width="644" border="0" align="center">
							  <tr>
							    <td width="178" valign="top" class="style11">School Year - Semester</td>
							    <td width="425" valign="">
								<?php $sem = SEM::getallsems(array("sem_NAME"=>"DESC")); ?>
							      <form action="<?php echo $_SERVER['REQUEST_URI']; ?>#choose" method="get">
							        <input type="hidden" name="action" value="view" />
							          <select name="sem_ID" id="sem_ID" onchange="this.form.submit()" style="width:100%">
							          	<option></option>
							            <?php foreach($sem as $key => $val){ ?>
							            <option <?php if(isset($_GET['sem_ID'])) { if($_GET['sem_ID']==$val['sem_ID']){ echo "SELECTED=SELECTED "; }  } ?> value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?>
							           		
							            </option>
							            <?php } ?>
							            </optgroup>
							          </select>	
							      </form></td>
							  
							  </tr>
							</table>
							<br/>

							<table width="644" border="0" align="center">
							  <tr>
							    <td valign="top" class="style12" align="center">
								<b><?php
							     $d = SEMSECTION::getSEM($_GET['sem_ID']);
														echo $d['sem_NAME']; ?>	
							      </b> <br />
							   (School Year - Semester)</td>
							  </tr>
							</table>

							<?php	


								$sem = SEMSECTION::getAllbyID(array("a.course_ID"=>"DESC","semester_section_NAME"=>"ASC"),$_GET['sem_ID']);
									foreach($sem as $key => $val)
									{ 
											 
											 
									$id = $val['semester_section_ID']; ?>
							      
													

							<table width="800" border="0" align="center" cellpadding="3" cellspacing="0">
							  <tr>
								<td class="style11"><b><?php $sem = SEMSECTION::getID($val['semester_section_ID']); ?>
							                       
							                          <?php $c = COURSE::getbyID($val['course_ID']);
							                                        echo $c['course_INIT']; ?>
							                          -<?php echo $val['semester_section_NAME']; ?></b>
							                          
							                          <?php
							                          $count = SUBJSCHED::CountSubjectsBySection($id);
													  #echo $count;
							                          ?>
							                          </td>
							    </tr>
							    <tr>
							    	<td>
							        <!-- START -->
							        <?php 
							$subjects = SUBJSCHED::getActiveSubjectsBySection($id);
							$count = SUBJSCHED::CountSubjectsBySection($id);
							#$count;
							?>
							<table width="100%" border="1" cellpadding="3" cellspacing="0" class="table table-hover table-responsive table-striped text-md" >              
							<tr>
							           			  <th  width="10%"><div align="center" class="style6 style8 style9 style10">Code</div></th>
							                                        <th  width="30%"><div align="center" class="style11">Description</div></th>
							                                       	<th  width="10%"><div align="center" class="style11">Room</div></th>
							                                        <th  width="5%"><div align="center" class="style11">Day</div></th>
							                                        <th  width="20%"><div align="center" class="style11">Time</div></th>
							                                        <th  width="5%"><div align="center" class="style11">Rem</div></th>
							                                        <th  width="5%" ><div align="center" class="style11">Taken</div></th>
							                                        <th><div align="center" class="style11">Instructor</div></th>
							                   </tr>
							                  <?php 
											  $x = 0;
											  foreach($subjects as $key => $val) { ?>
							                  <tr>
							                  	<td style="width:80px"><div align="center" class="style11">
							                  	  <?php 
							                   			 $ss = SUBJSCHED::getSched($val['subject_sched_ID']);
														 $s = SUBJECTS::getID($ss['subject_ID']);
														 
														# print_r($s);
							                             echo $s['subject_CODE'];
														 
														 							 
							                   		 ?>                    
							               	    </div></td>
							                    <td s><div align="center" class="style11">
							                      <?= $s['subject_DESCRIPTION']; ?> 
							                    (<?php echo $s['LEC_UNIT']; ?>/<?php echo $s['LAB_UNIT']; ?>)</div></td>
							                    <td ><div align="center" class="style11">
							                      <?php  $room = ROOM::getID($val['room_ID']); echo $room['room_NAME']; ?>
							                    </div></td>
							                    <td ><div align="center" class="style11"><?php echo substr($val['day'],0,3); ?></div></td>
							                    <td ><div align="center" class="style11"><?php echo $val['start']."-".$val['end']; ?></div></td>
							                    <td><div align="center" class="style11"><?php echo ($val['max_student']-SUBJSCHED::checkRemaining($val['subject_sched_ID'])); ?>                    </div></td>
							                    <td ><div align="center" class="style11">
							                    <?php
												$x+= SUBJSCHED::checkRemaining($val['subject_sched_ID']);
												?>
							                      <?=  SUBJSCHED::checkRemaining($val['subject_sched_ID']); ?>
							                    </div></td>
							                    <td> <div align="center" class="style11"><?php
							                    $ins = INSTRUCTORS::getSingle1($val['instructor_ID']);
												echo $ins['instructor_NAME'];
												?></div></td>
							  </tr>
							                <?php } ?>
							                <tr>
							                	<td></td>
							                    <td></td>
							                	<td></td>
							                    <td></td>
							                    
							                    <td colspan="2" align="right" class="style11">Average: </td>
							                    <td align="center"  class="style11"><?php $x; ?>
							                    <?= round($x/$count); ?>
							                    </td>
							                    <td></td>
							                </tr>    
							              </table>


							        <!-- END -->
							        </td>
							    </tr>
							    </table>
							    
							<br />
							            <?php } ?>

	                    </div>
	                </div>
					

					<?php
					break;
					default : "";
                    }

                }else{?>
                        
                <?php redirect("/"); } ?>
                	
			    </div></div>
			<!-- end row container -->
            
            

			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>	
		<!-- end container -->

	</body>
</html>
<?php        
$user_info_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Section Status Listing <br> DESCRIPTION: User Viewed Section Status Listing 
<br>DETAILS: SEMESTER - ".$d['sem_NAME'];
$audit = Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
?>
