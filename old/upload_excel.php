<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<table>
		<form method="post" action="upload_excel.php" enctype='multipart/form-data'>
			<p><label>Please Select File(Only CSV Formate)</label>
            <input type="file" class="form-control" name="addstudent" /></p>
            <br />
            <input type="submit" name="upload" class="btn btn-info" value="Upload" />
		</form>
	</table>
</body>
</html>
<?php

	$mysqli = new mysqli('localhost','magsaysay','p@ssw0rd','itekniks_ams');
	

	if($_FILES['addstudent']['name']){
		$id = 1;
			$filename = explode(".", $_FILES['addstudent']['name']);
			if(end($filename) == 'csv'){
				$handle = fopen($_FILES['addstudent']['tmp_name'], "r");
				while($data = fgetcsv($handle)){
					$std_fname=mysqli_real_escape_string($mysqli, $data[0]);
					$std_mname=mysqli_real_escape_string($mysqli, $data[1]);
					$std_lname=mysqli_real_escape_string($mysqli, $data[2]);
					$std_year=mysqli_real_escape_string($mysqli, $data[3]);
					$std_month=mysqli_real_escape_string($mysqli, $data[4]);
					$std_day=mysqli_real_escape_string($mysqli, $data[5]);
					$std_address=mysqli_real_escape_string($mysqli, $data[6]);
					$std_place_of_birth=mysqli_real_escape_string($mysqli, $data[7]);
					$std_gender=mysqli_real_escape_string($mysqli, $data[8]);
					$std_id=mysqli_real_escape_string($mysqli, $data[9]);
					$std_street=mysqli_real_escape_string($mysqli, $data[10]);
					$std_brgy=mysqli_real_escape_string($mysqli, $data[11]);
					$std_city=mysqli_real_escape_string($mysqli, $data[12]);
					$std_district=mysqli_real_escape_string($mysqli, $data[13]);
					$std_province=mysqli_real_escape_string($mysqli, $data[14]);
					$std_email=mysqli_real_escape_string($mysqli, $data[15]);
					$std_contact=mysqli_real_escape_string($mysqli, $data[16]);
					$std_student_year_level=mysqli_real_escape_string($mysqli, $data[17]);
					$course_id=mysqli_real_escape_string($mysqli, $data[18]);

					$sql = "INSERT INTO student_information SET 
					si_FNAME = '$std_fname',
					si_MNAME = '$std_mname',
					si_LNAME = '$std_lname',
					si_BIRTHYEAR = '$std_year',
					si_BIRTHMONTH = '$std_month',
					si_BIRTHDAY = '$std_day',
					address = '$std_address',
					si_BIRTHPLACE = '$std_place_of_birth',
					si_GENDER = '$std_gender',
					student_ID = '$std_id',
					si_STREET = '$std_street',
					si_BRGY = '$std_brgy',
					si_CITY = '$std_city',
					si_DISTRICT = '$std_district',
					si_PROVINCE = '$std_province',
					si_EMAIL = '$std_email',
					si_CONTACT = '$std_contact',
					student_type_ID = 1,
					student_year_level_id = 0,
					access_ID = 6,
					course_ID = '$course_id'";

					$result = $mysqli->query($sql);
				
					$sai_guardian_name=mysqli_real_escape_string($mysqli, $data[19]);
					$sai_relationship=mysqli_real_escape_string($mysqli, $data[20]);
					$sai_guardian_occupation=mysqli_real_escape_string($mysqli, $data[21]);
					$sai_tell_number=mysqli_real_escape_string($mysqli, $data[22]);
					$sai_guardian_address=mysqli_real_escape_string($mysqli, $data[23]);
					
					$sai_guardian_elem_graduated_at=mysqli_real_escape_string($mysqli, $data[24]);
					$sai_guardian_elem_graduated_year=mysqli_real_escape_string($mysqli, $data[25]);
					$sai_guardian_hs_graduated_at=mysqli_real_escape_string($mysqli, $data[26]);
					$sai_guardian_hs_graduated_year=mysqli_real_escape_string($mysqli, $data[27]);
					$sai_guardian_college_last_attended_at=mysqli_real_escape_string($mysqli, $data[28]);
					
					$sai_guardian_degree=mysqli_real_escape_string($mysqli, $data[29]);
					$sai_grad_stat=mysqli_real_escape_string($mysqli, $data[30]);
					$sai_religion=mysqli_real_escape_string($mysqli, $data[31]);
					$sai_civil=mysqli_real_escape_string($mysqli, $data[32]);
					$sai_age=mysqli_real_escape_string($mysqli, $data[33]);
					
					$sai_height=mysqli_real_escape_string($mysqli, $data[34]);
					$sai_weight=mysqli_real_escape_string($mysqli, $data[35]);
					$sai_special_skills=mysqli_real_escape_string($mysqli, $data[36]);
					$sai_present_addr_st=mysqli_real_escape_string($mysqli, $data[37]);
					$sai_present_brgy=mysqli_real_escape_string($mysqli, $data[38]);
					
					$sai_present_city=mysqli_real_escape_string($mysqli, $data[39]);
					$sai_present_district=mysqli_real_escape_string($mysqli, $data[40]);
					$sai_present_prov=mysqli_real_escape_string($mysqli, $data[41]);
					$sai_present_contact=mysqli_real_escape_string($mysqli, $data[42]);
					$asdas = $id++;
					$std_add_info_query = "INSERT INTO student_additional_info SET
					si_ID='$asdas',
					sai_name_of_guardian = '$sai_guardian_name',
					sai_relationship = '$sai_relationship',
					sai_occupation = '$sai_guardian_occupation',
					sai_tel_cell_number = '$sai_tell_number',
					sai_address = '$sai_guardian_address',
					sai_elementary_graduated = '$sai_guardian_elem_graduated_at',
					sai_elementary_year_graduated = '$sai_guardian_elem_graduated_year',
					sai_highschool_graduated = '$sai_guardian_hs_graduated_at',
					sai_highschool_year_graduated = '$sai_guardian_hs_graduated_year',
					sai_college_last_attended = '$sai_guardian_college_last_attended_at',
					sai_degree_title = '$sai_guardian_degree',
					sai_status = 1,
					sai_graduating = 0,
					sai_religion = '$sai_religion',
					sai_civil = '$sai_civil',
					sai_age = '$sai_age',
					sai_height = '$sai_height',
					sai_weight = '$sai_weight',
					sai_special_skills = '$sai_special_skills',
					sai_present_addr_st = '$sai_present_addr_st',
					sai_present_brgy = '$sai_present_brgy',
					sai_present_city = '$sai_present_city',
					sai_present_district = '$sai_present_district',
					sai_present_prov = '$sai_present_prov',
					sai_present_contact = '$sai_present_contact'
					";
					$result1 = $mysqli->query($std_add_info_query);
				}
				fclose($handle);
				echo "success";
			}else{
				echo "select csv file";
			}
		}else{
			echo "select a file";
		}


?>