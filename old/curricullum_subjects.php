<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
            CSCONTROLLER::add(Request::post());
			#var_dump(Request::post());
			
			break;
			
			case "addfcur": 
            var_dump(Request::post());
            
			CSCONTROLLER::add2(Request::post());
			#msgbox($_SESSION['MSG']['txt']);
			href($_SERVER['HTTP_REFERER']);

			break;
			
			case "edit": CSCONTROLLER::update2($_GET['id'],Request::post()); break;
			#case "update_blog":  BLOG::update(Request::post("blog_info")); break;
			
			#case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": CSCONTROLLER::delete($_GET['id']); break;
		
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>SCHOOL MANAGEMENT SYSTEM</title>
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end of alert messages -->
			<!-- start row container -->
			<!-- 
            <div class="row">
				<div class="col-lg-12">
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
            </div>	
			</div>
            -->
			<!-- end row container -->
            <!-- row container -->
			<div class="row">
				<!-- left nav -->   
            <?php
				if (!SESSION::isLoggedIn()) {
					?>
				<div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> Course Categories </h3>
                    </div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                        <li >
                            <a style="cursor:pointer" id="1-link">
                                <span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=1','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CCS (2)
                                </span>
                            </a>
                        </li>
                        <li >
                            <a style="cursor:pointer" id="2-link">
                                <span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=2','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CBA</span>
                            </a>
                        </li>
                        <script>

                                  $( "#1-link" ).click(function() {
                             $( "#1-content" ).slideToggle( "fast" );
                         });
                                  $( "#2-link" ).click(function() {
                             $( "#2-content" ).slideToggle( "fast" );
                         });

                        </script>
                        <!--
                            <li class="active"><a href="#">Computer Studies (#)</a></li>
                            <li class=""><a href="#">Business Course</a></li>
                            <li class="dropdown"><a class="dropdown-toggle" id="link" data-toggle="dropdown" href="#">Cat3 <span class="caret"></span></a>
                                <ul class="nav nav-pills nav-stacked nav-sub"  >
                                    <li><a href="#">Cat3 Sub 1 <span class="caret"></span></a>

                                    <ul class="nav nav-pills nav-stacked  nav-sub"  style="">

                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                    </ul>

                                    </li>
                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                </ul>
                            </li>

                           --> 
                        </ul>
                    </div>
                </div>
				</div>
                <?php   }else{?>
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
                <?php } ?>
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                            <i class="fa fa-folder-o"></i> Curricullum Subjects </h4> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <!-- Name -->
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Curricullum</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=curricullum_ID class="form-control">
                                        <?php $getallCurricullum= CSCONTROLLER::getallCurricullum(array("curricullum_ID"=>"ASC"));   
                                            foreach($getallCurricullum as $key => $value){
                                                $cid = $value['course_ID'];
                                                $getsingleCourse = CSCONTROLLER::getsingleCourse(array("course_ID"=>"ASC"),$cid);
                                                foreach($getsingleCourse as $key2 => $val){
                                        ?>
                                        <option value=<?= $value['curricullum_ID']?>><?= $value['curricullum_NAME']." - (".$val['course_NAME'].")"?></option>
                                        <?php } } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Year-Sem</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=yr_sem_ID class="form-control">
                                        <?php $getallyrsem= CSCONTROLLER::getallyrsem(array("yr_sem_ID"=>"ASC"));   
                                            foreach($getallyrsem as $key => $value){
                                        ?>
                                        <option value=<?= $value['yr_sem_ID']?>><?= $value['yr_sem_NAME']?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Subjects</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=subject_ID class="form-control">
                                        <?php $getallsubject= CSCONTROLLER::getallsubjects(array("subject_CODE"=>"ASC"));   
                                            foreach($getallsubject as $key => $value){
                                        ?>
                                        <option value=<?= $value['subject_ID']?>><?= $value['subject_CODE']." - ".$value['subject_DESCRIPTION']." - ".$value['LEC_UNIT']."/".$value['LAB_UNIT']?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Pre-Requisites</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=pre_req class="form-control">
                                        <option value="">None</option>
                                        <?php $getallsubject= CSCONTROLLER::getallsubjects(array("subject_CODE"=>"ASC"));   
                                            foreach($getallsubject as $key => $value){
                                        ?>
                                        <option value=<?= $value['subject_ID']?>><?= $value['subject_CODE']." - ".$value['subject_DESCRIPTION']?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Add Curricullum Subjects</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":
                        $siiid =$_GET['id'];
                        $getsingle = CSCONTROLLER::getSingle(array("cs_ID"=>"ASC"),$siiid  );
						// echo $getsingle['curricullum_ID'];
                        // echo $getsingle['subject_ID'];
                         $presub = explode(",",$getsingle['prerequisite_subject_ID']);
                         // echo "sub".serialize($sub);
                         $copresub1 = explode(",",$getsingle['corequisite_subject_ID']);
                         // echo "sub1".serialize($sub1);
      //                   $getsingle['yr_sem_ID']
						// print_r($getsingle['curricullum_ID']);
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                            <i class="fa fa-folder-o"></i> Update Curricullum Subject</h4> 
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Curricullum Subjects</legend>
                            <!-- Name -->
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Curricullum</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=curricullum_ID class="form-control">
                                        <?php $getallCurricullum= CSCONTROLLER::getallCurricullum(array("curricullum_ID"=>"ASC"));   
                                            foreach($getallCurricullum as $key => $value){
                                                $cid = $value['course_ID'];
                                                $getsingleCourse = CSCONTROLLER::getsingleCourse(array("course_ID"=>"ASC"),$cid);
                                                foreach($getsingleCourse as $key2 => $val){
                                        ?>
                                        <option value=<?= $value['curricullum_ID']?>
                                                <?php
                                $getsingleCurricullum=CSCONTROLLER::getsingleCurricullum(array("curricullum_ID"=>"ASC"),$value['curricullum_ID']);
                                                foreach($getsingleCurricullum as $key1 => $value1){
                                                    if($value1['curricullum_ID']==$getsingle['curricullum_ID']){
                                                    echo"selected";
                                                    }

                                                }
                                                ?>
                                                
                                                ><?= $value['curricullum_NAME']." - (".$val['course_NAME'].")"?></option>
                                        <?php } } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Subjects</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=subject_ID class="form-control">
                                        <?php
                                            
                                            $getallsubjects= CSCONTROLLER::getallsubjects(array("subject_CODE"=>"ASC"));   
                                            foreach($getallsubjects as $key => $value){
                                        ?>
                                        <option value="<?= $value['subject_ID']?>" 
                                        
                                        
                                            
                                <?php
                                $getsinglesubject=CSCONTROLLER::getsinglesubjects(array("subject_CODE"=>"ASC"),$getsingle['subject_ID']);
                                                foreach($getsinglesubject as $key1 => $value1){
                                                    if($value1['subject_ID']==$value['subject_ID']){
                                                    echo"selected";
                                                    }

                                                }
                                                ?>  ><?= $value['subject_CODE']." - ".$value['subject_DESCRIPTION']." - ".$value['LEC_UNIT']."/".$value['LAB_UNIT']?> </option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Pre-Requisites</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=pre_req[] class="form-control select2" multiple="multiple">
                                        <option value="">None</option>
                                        <?php $getallsubject= CSCONTROLLER::getallsubjects(array("subject_CODE"=>"ASC"));   
                                            foreach($getallsubject as $key => $value){
                                        ?>
                                        <option value=<?= $value['subject_ID']?>  
                                                <?php
                                                $sub = explode(",",$getsingle['prerequisite_subject_ID']);
                                                foreach($sub as $k1 => $v1){
                                                    #$getsinglesubject=CSCONTROLLER::getsinglesubjects(array("subject_CODE"=>"ASC"),$getsingle['prerequisite_subject_ID']);
                                                    $getsinglesubject=CSCONTROLLER::getsinglesubjects(array("subject_CODE"=>"ASC"),$v1);
                                                    foreach($getsinglesubject as $key1 => $value1){
                                                        if($value1['subject_ID']==$value['subject_ID']){
                                                        echo"selected";
                                                        }

                                                    }
                                                }
                                                ?> ><?= $value['subject_CODE']." - ".$value['subject_DESCRIPTION']?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Co-Requisites</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=co_req[] class="form-control select2" multiple="multiple">
                                        <option value="">None</option>
                                        <?php $getallsubject= CSCONTROLLER::getallsubjects(array("subject_CODE"=>"ASC"));   
                                            foreach($getallsubject as $key => $value){
                                        ?>
                                        <option value=<?= $value['subject_ID']?>  
                                                <?php
                                                $sub = explode(",",$getsingle['corequisite_subject_ID']);
                                                foreach($sub as $k1 => $v1){
                                                    #$getsinglesubject=CSCONTROLLER::getsinglesubjects(array("subject_CODE"=>"ASC"),$getsingle['prerequisite_subject_ID']);
                                                    $getsinglesubject=CSCONTROLLER::getsinglesubjects(array("subject_CODE"=>"ASC"),$v1);
                                                    foreach($getsinglesubject as $key1 => $value1){
                                                        if($value1['subject_ID']==$value['subject_ID']){
                                                        echo"selected";
                                                        }

                                                    }
                                                }
                                                ?> ><?= $value['subject_CODE']." - ".$value['subject_DESCRIPTION']?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Year-Sem</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=yr_sem_ID class="form-control">
                                        <?php $getallyrsem= CSCONTROLLER::getallyrsem(array("yr_sem_ID"=>"ASC"));   
                                            foreach($getallyrsem as $key => $value){
                                        ?>
                                        <option value=<?= $value['yr_sem_ID']?>
                                                <?php
                                                $getsingleyrsem = CSCONTROLLER::getsingleyrsem(array("yr_sem_ID"=>"ASC"),$getsingle['yr_sem_ID']);
                                                foreach($getsingleyrsem as $key4 => $value4){
                                                if($value4['yr_sem_ID']==$value['yr_sem_ID']){
                                                    echo"selected";
                                                    }
                                                }
                                                ?>
                                                ><?= $value['yr_sem_NAME']?></option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Save</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
						include "searching.php";						
						?>
							
                    <?php break; 
                        case "view":?>
                        <?php
								if(isset($_GET['id']))
								{
								$rID = $_GET['id'];
                                    //msgbox($studID);
                                    $view1room = ROOM::view1room($rID);
                                    if(count($view1room)>=1){
										foreach($view1room as $key => $value){
											$room_ID = $value['room_ID'];
											$room_NAME = $value['room_NAME'];
											
						?>
						<div class="panel panel-default">
							<div class="panel-heading"  style="margin-top: 5px; margin-bottom: 5px">
                                <div class="col-md-3">
								<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
									<i class="fa fa-user"></i>		
								</h3>
                                </div>
                                <div class="col-md-53">
								<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
								</h3>
                                </div>
                            </div>
							
							<div class="panel-body">
								<table border='0' class="table table-responsive text-md">
									<tr>
										<td colspan='4'><strong>View Rooms</strong>
									<tr>
										<td>&nbsp;
										<td align='center'><?php echo $room_ID;?>
									<tr>
										<td>&nbsp;
										<td align='center'><strong><i>Room ID</i></strong>
<tr>
										<td>&nbsp;
										<td align='center'><?php echo $room_Name;?>
									<tr>
										<td>&nbsp;
										<td align='center'><strong><i>Room Name</i></strong>

						<?php } ?>
								</table>
								<!--
								<tr>
										<td colspan='4' align='right'>
								-->
									
							</div>
							
						</div>
							<div align='right'>
								<input type='button' class='btn btn-warning' value='Edit'>
								<!--<input type='button' onClick="location.href='../../isjb/student_information.php?action=view'" class='btn btn-success' value='Back'>-->
							</div>
					<?php
									}else{ echo 'Error: 404 Not found.'; }
                                }else{
					?>
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table>
                                <tr>
                                <td width="50%">
                                    <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                        <i class="fa fa-folder-o"></i> View Curricullum Subjects </h4>
                                </td>
                                <script src="jquery.js" type="text/javascript"></script>
                                    <script> 
                                    $(document).ready(function(){
                                      $("#flip").click(function(){
                                        $("#panel").slideToggle("fast");
                                      });
                                    });
                                    </script>
                                <td >
                                    <div class="input-group col-md-12">
                                        <form id="form1" runat="server">
                                       		 <input id="searchItem" class="searchItem form-control" name="q" type="text" onkeyup="term(q.value)" autofocus="autofocus" onchange="term(q.value)"/>
                                        </form>
                                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
                                </td>
                                  <script>
                                    			$(document).ready(function() {
												$('#searchItem').keyup(function(){
												var s_Item = $('#searchItem').val();
													
													if(s_Item.length >= 3)
													{
														 $.ajax({
															type: "POST",
															url: "search.php?action=cs.php",
															data: 'search_term=' + s_Item,
															success: function(msg){
																/* $('#resultip').html(msg); */
																	$("#display_result").show();
																	$("#display_result").html(msg);
																	
																	$("#display_hide").hide();
																
															}
											
														}); // Ajax Call
													
														//alert(s_Item);
														}else{
															$("#display_hide").show();
															$("#display_result").hide();   
														}
													});
												});			
                                    			</script>
                                <td width="5%">
                                <div class="col-md-1">   
                                <button style="margin-top: 3px; margin-bottom: 2px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('curricullum_subjects.php?action=add','_self');"><i class='fa fa-plus'></i></button>
                                </div>
                                </td>
                                </tr>
                                </table>
                            </div>
							<div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
								<table class="table table-hover table-responsive table-striped text-md">
									<th style="width:15%;">Curricullum ID</th>
									<th style="width:15%;">Subject ID</th>
									<th style="width:15%;">Pre Requisites</th>
									<th style="width:15%;">Year Sem</th>
									<th style="width:15%;">Operation</th>
                                    
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    $viewStudents = CSCONTROLLER:: getallcs(array("curricullum_ID"=>"ASC"));
                                     if(count($viewStudents)>=10) 
                                     {
                                         $pagination2 = new Pagination($viewStudents, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($viewStudents, 10, NULL);
                                     }

                                     $viewStudents = $pagination2->get_array();

                                     if($viewStudents) {
                                        foreach($viewStudents as $key => $value){
                                    ?>
                                        <tr>
                                            <td><?php $getsingleCurricullum = CSCONTROLLER::getsingleCurricullum(array("curricullum_ID"=>"ASC"),$value['curricullum_ID']);
                                                foreach($getsingleCurricullum as $key1 => $value1){
                                                    $getsingleCourse = CSCONTROLLER::getsingleCourse(array("course_ID"=>"ASC"),$value1['course_ID']);
                                                    foreach($getsingleCourse as $keys => $val){
                                                        echo $value1['curricullum_NAME']."(".$val['course_NAME'].")";
                                                    }
                                                }
                                                ?>
                                            <td><?php $getsinglesubjects = CSCONTROLLER::getsinglesubjects(array("subject_ID"=>"ASC"),$value['subject_ID']);
                                                foreach($getsinglesubjects as $key2 => $value2){
                                                echo $value2['subject_CODE']." - ".$value2['subject_DESCRIPTION'];
                                                }
                                                ?>
                                            <td><?php 
                                                if($value['prerequisite_subject_ID']==0){
                                                echo "NONE";
                                                }else{
                                                $getsinglesubjects = CSCONTROLLER::getsinglesubjects(array("subject_ID"=>"ASC"),$value['prerequisite_subject_ID']);
                                                foreach($getsinglesubjects as $key3 => $value3){
                                                echo $value3['subject_CODE']." - ".$value3['subject_DESCRIPTION'];
                                                } 
                                                }?>
                                            <td><?php
                                                $getsingleyrsem = CSCONTROLLER::getsingleyrsem(array("yr_sem_ID"=>"ASC"),$value['yr_sem_ID']);
                                                foreach($getsingleyrsem as $key4 => $value4){
                                                echo $value4['yr_sem_NAME'];
                                                }
                                                ?>
                                                
                                            <td><a href="curricullum_subjects.php?action=edit&id=<?= $value['cs_ID']; ?>" class='btn btn-warning btn-xs'>Edit</a>
                                            <a href="curricullum_subjects.php?action=delete&id=<?= $value['cs_ID'];?>" class='btn btn-danger btn-xs'>Delete</a>
                                            </td>
                                        </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
                                        <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                                        </center>
							     </div>
                            </div>
						</div>
                            
					</div>
                </div>
                  <?php } break; ?>
				  
                    <?php 
                        case "delete":
                      	  CSCONTROLLER::Remove($id);
						  msgbox("Subject Removed");
						?>  
						  
                <?php break;
				
				
						case "addfcur":
						
						break;
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                	
				    
                 </div>

				    
				</div>
                
				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
            
            <script>
            $(document).ready(function() {
                $('.select2').select2();
            });
            </script>
			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
</html>
