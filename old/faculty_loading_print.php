<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::post("action")) {
			case "add": 
			
			break;
			case "change_lock":
				SUBJSCHED::change_lock(Request::post());
			break;
			
			case "change_check":
				SUBJSCHED::change_check(Request::post());
			break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ROOM::delete($_GET['id']); break;

		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>FACULTY LOADING | School Management System v2.0</title>
	</head>
	<body onload="window.print();">
		<!-- top nav -->
		
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
		
			<!-- end banner -->
			<!-- alert messages -->
		
			<!-- end of alert messages -->
			<!-- start row container -->
			<!-- 
            <div class="row">
				<div class="col-lg-12">
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
            </div>	
			</div>
            -->
			<!-- end row container -->
            <!-- row container -->
			<div class="row">
				<!-- left nav -->   
          
               
               
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-12">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                    
                        
              case "view":?>
                       
                      
                       
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table width="100%">
                                <tr>
                                    <td colspan="6">
                                    
                                    
                                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                            <i class="fa fa-folder-o"></i> FACULTY SCHEDULE</h3>                                                                      
                                      <!--
                                        <div class="col-md-1">   
                                            <button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('?action=add','_self');"><i class='fa fa-plus'></i></button>
                                        </div>
                                    -->                                        </td>
                                  </tr>
                                <tr>
                                  <td width="10%">Semester:</td>
                                  <td width="28%" >
                                    <?php
                                      $sem = SEM::getSingleSem($_GET['sem_ID']);
                                      echo $sem['sem_NAME'];
                                      ?>
                                 </td>
                                  <td width="3%" >&nbsp;</td>
                                  
                                  <?php if(isset($_GET['sem_ID'])){ ?>
                                  <?php $faculty = SEM::allfacultypersem($_GET['sem_ID']);       
								  #print_r($faculty);
								  ?>
                                  <td width="5%">&nbsp;</td>
                                  <td width="31%">&nbsp;</td>
                                  
                                  <?php } ?>
                                  <td width="23%">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>Faculty:


                                    
                                  </td>
                                  <td><?php
                                   $f = INSTRUCTORS::getSingle1($_GET['fac_id']); 
                                  echo $f['instructor_NAME']; 
                                  ?></td>
                                  <td >&nbsp;</td>
                                  
                                  <?php if(isset($_GET['fac_id'])){ ?>
                                 
                                  <?php } ?>
                                  <td>&nbsp;</td>
                                </tr>
                                
                                </table>
                          </div>
							
							<div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
                                
                              
                                
                                
                                
                                
                                
                                
                                <?php 
								 if(isset($_GET['fac_id']))
								{
								?>
                                
                               
								
                                <table class="bordered" border="1" width="100%" cellspacing="0" cellpadding="5">
                                    <th style="">#</th>
                                    <th style=";">Course</th>
                                    <th style="">Section</th>
                                    <th style="">Days</th>
                                    <th style="">Time</th>
                                    <th style="">Room</th>
                  									<th style="">Enrolled</th>
                                    
                                    <?php
									
									$getmysubj = SUBJSCHED::getByfaculty($_GET['sem_ID'], $_GET['fac_id']);
									#print_r($getmysubj);
									$totalcount = count($getmysubj);
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                   // $viewSems = SEMSECTION::getAll(array("semester_section_ID" => "DESC"));
									
									
                                     if(count($getmysubj)>=50) 
                                     {
                                         $pagination2 = new Pagination($getmysubj, 50, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($getmysubj, 50, NULL);
                                     }

                                     $getmysubj = $pagination2->get_array();
                                    # var_dump($getmysubj);
									                   $x = 1;

                                     if($getmysubj) {
                                        foreach($getmysubj as $key => $value){
                                    ?>
                                        <tr>
                                            <td>
											<?php 
												if(isset($_GET['p']))
												{
												$p = $_GET['p'];
												
												$num = 	$x+($p*10)-10;
												echo $num;
												$x++;
												
												}else{
													echo $x++;
												}
											 ?>
                          <td><?php $s = SUBJECTS::getID($value['subject_ID']);
                                echo $s['subject_CODE']."/".$s['subject_DESCRIPTION'];
                               ?> 


                          <td><?php $c=COURSE::getbyID($value['course_ID']); 
											          echo $c['course_INIT']."-";
                                echo $value['semester_section_NAME']; 
                              ?>
                                            
                                          
                                      
												 </td>
                        <td><?php echo $value['day']; ?> 
                        <?php if($value['day2'] != NULL){ ?>/<br /><?php echo $value['day2']; ?><?php } 
                        ?>
                        <?php if($value['day3'] != NULL){ ?>/<br /><?php echo $value['day3']; ?><?php } 
                        ?>
                        <?php if($value['day4'] != NULL){ ?>/<br /><?php echo $value['day4']; ?><?php } 
                        ?>
                      </td>
                        <td><?php echo $value['start']; ?>-<?php echo $value['end']; ?>
                            <?php if($value['start2'] != NULL){ ?>/<br /><?php echo $value['start2']; ?>-<?php echo $value['end2']; ?><?php } 
                            ?>
                            <?php if($value['start3'] != NULL){ ?>/<br /><?php echo $value['start3']; ?>-<?php echo $value['end3']; ?><?php } 
                            ?>
                            <?php if($value['start4'] != NULL){ ?>/<br /><?php echo $value['start4']; ?>-<?php echo $value['end4']; ?><?php } 
                            ?>
                        </td>
                        <td><?php
                            $r = ROOM::getID($value['room_ID']);
                            echo $r['room_NAME'];
                             ?>
                               <?php if($value['room_ID2'] != NULL){ ?>/<br />
                               <?php
                                   $r = ROOM::getID($value['room_ID2']);
                                  echo $r['room_NAME'];
                             } ?>
                               <?php if($value['room_ID3'] != NULL){ ?>/<br />
                               <?php
                                   $r = ROOM::getID($value['room_ID3']);
                                  echo $r['room_NAME'];
                             } ?>
                               <?php if($value['room_ID4'] != NULL){ ?>/<br />
                               <?php
                                   $r = ROOM::getID($value['room_ID4']);
                                  echo $r['room_NAME'];
                             } ?>
                             <td align="center"> <?= SUBJSCHED::checkRemaining($value['subject_sched_ID']); ?></td>
                                  </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
                                Total Records: <?=  $totalcount; ?>
                                       
                                        
                                  <?php }else{ ?>  
                                  
                                        
                                        
                                        
                                        
                                  <table class="table table-hover table-responsive table-striped text-md">
									
                                    <th style="width:5%;">#</th>
                                    <th style="width:25%;">Faculty</th>
                  									<th style="width:20%;"><div align="center">No. of Subjects</div></th>
                  									<!-- <th style="width:20%;"><div align="center">Checked Subjects</div></th> -->
                                    <th style=""><div align="center">Operation</div></th>
                                     <?php     
                                  	$faculty = SEM::allfacultypersem($_GET['sem_ID']);      
									#print_r($faculty);
                                    ?>    
                                    
                                    
                                <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
									 
									 
									 
                                 
									
									
                                     if(count($faculty)>20) 
                                     {
                                         $pagination2 = new Pagination($faculty, 20, Request::get("p"));
						
                                     }else{
                                         $pagination2 = new Pagination($faculty, 20, NULL);
							
                                     }
			
                                     $faculty = $pagination2->get_array();
									 $x = 1;
									

                                     if($faculty) {
                                        foreach($faculty as $key => $value){
                                    ?>
                                        <tr>
                                            <td><?php echo $x++; ?>.
                                            <td><?php
											$f = INSTRUCTORS::getSingle1($value['instructor_ID']); 
											echo $f['instructor_NAME']; 
											?>
                                            <td><div align="center"><?php echo SEM::numofsubjpersemperfac($_GET['sem_ID'],$value['instructor_ID']); ?>
                                            </div>
                                            <!-- <td><div align="center"><?php echo SEM::numofchksubjpersemperfac($_GET['sem_ID'],$value['instructor_ID']); ?> -->
                                            </div>
                                                                            </tr>
                                    
                                        <?php
                                         } 
                                        }else{
                                    ?>
                                        <tr>
                                          <td colspan="5" align="center">No Faculty</td>
                                        </tr>
                                        <?php } ?>
								</table> 
        
                                        
                                  <?php } ?>      
                                        
                                        
						      </div>
                            </div>
						</div>
                            
					</div>
                </div>
                  <?php  break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
					default: href("?action=view");
				
				
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                	
				    
                 </div>

				    
				</div>
                
				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
            

			<!-- footer-->      
			
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
</html>
