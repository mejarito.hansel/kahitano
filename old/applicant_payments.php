<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();
	$user_id = $user['account_ID'];
    $action_event = "View";
    $event_desc = "MODULE: Admission / Applicant Payments, DESCRIPTION: User visited Applicant Payments";
    $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}
    
  
if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
    

	if ($user) 
	{
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();
	
			  
				SESSION::login($user);
                msgbox("Login Success");
               
               
				AUDIT::insert($user['account_ID'],"LOGGED IN");
				
				
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}

	//QUERIES


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
	  	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>

      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
      <title>HOME | School Management System v2.0</title>
	</head>

	<body>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
			<!-- alert messages -->
			<?php 
			
			SESSION::DisplayMsg(); 
			
			
			
			?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
				
				//audit
				
				
				
					?>
				<div class="col-md-3">
            		<?php include(PAGES."navigation.php");?>
				</div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 


								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>
								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

									</div>
								</div>
                                
                                	<div class="form-group row">
									<div class="col-lg-offset-9">
										 <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                                
                                </fieldset>
                    </form>
                    </div>	
                </div>
                
                <?php   }else{
				
				AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
				
				?>

<div class="col-md-3">

<?php include(PAGES."navigation.php");?>
</div>
              
                
<div class="col-md-9">
	<div class="panel panel-default">

		<div class="panel-heading">
			<table>
        		<tr>
        			<td width="50%">
						<h4 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"> Applicant Payments <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
					</td>
				 	<td>
            			<div class="input-group col-md-12 hide">
              				
              				<input id="searchItem" class="searchItem form-control"  type="text" autofocus="autofocus"  style="text-transform: uppercase;"/>
                            
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            			</div>
        			</td>
        			<td width="5%">
	    				<div class="col-md-1">   
	    					<a id="P2C3_EXPORTSASCSV" class='btn btn-success btn-ms btnCSV' >Exports as CSV</a>
	    				</div>
	    			</td>
	    		</tr>
	    	</table>
		</div>
		<div class="panel-body"><br>
			<div class="row">
            	<div class="form-group">
	                <div class="col-sm-5">
	                    <input name='' id="myInput" placeholder="Search by Name or Applicant Number" class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
	                </div>
            	</div>
           	</div><br>
           	<?php
           		$CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];


           	?>
	        	<table id="myTable" data-ip="<?php echo $CLIENT_IP;?>" data-acct="<?php echo $user['account_ID'];?>"class="table table-hover table-responsive table-striped text-md" style="width:100%;">
			        <thead>
			            <tr>
			              <th>First Name</th>
			              <th>Last Name</th>
			              <th>E-mail Address</th>
			              <th>Contact No.</th>
			              <th>Applicant No.</th>
			              <th>Status</th>
			              <th>Actions</th>
			            </tr>
			        </thead>
			        <tbody id="tbody">

			        </tbody>
			    </table>
		  	<div class="modal fade" id="myModal" role="dialog">
			    <div class="modal-dialog">
			      	<div class="modal-content">
				        <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <h4 style="">Applicant Payment Tool</h4>
				          <p>Once you mark as paid,the applicant will receive an update to the account registered in MMMA Portal informing that the payment has been received.</p>
				        </div>
				        <div class="modal-body" style="padding:40px 90px;">
					      	<div class="failed hide" id="icpID"><strong>Error!</strong> Internet connection problem</div>
					      	<div class="success " id="success"><strong>Success!</strong> Successfully created password!</div>
					      	<div class="failed hide" id="requiredfield"><strong>Error!</strong> Required all fields</div>
					      	<div class="failed hide" id="passwordmatch"><strong>Error!</strong> Password didn't match</div>
		                    <div class="form-group">
		                        <select id="" required class="form-control" name="payments">
		                            <option value="">Mark as Paid</option>
		                            <!-- <option value="auto">Unpaid</option> -->
		                        </select>    
		                     </div>
				          	<button type="submit" class="btn btn-success btn-block" id="saveBtn"> Save</button>
				        </div>
			      	</div>
			    </div>
		  	</div> 
	 	</div>
	</div>
</div>
<?php } ?>
  	
			</div>
            <?php 
			?>
			<?php include (LAYOUTS . "footer.php"); ?>
 			<script src="<?php echo HOME; ?>old/assets/js/custom/applicant.js"></script>

		</div>
  <script type="text/javascript">
  	$( window ).ready(function(){
        setTimeout(function() {

      		$.main.applicant.applicantPayments();
          
        },100);
  	})
    function myFunction() {
      var input, filter, table, tr, td, i;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        td2 = tr[i].getElementsByTagName("td")[1];
        td1 = tr[i].getElementsByTagName("td")[4];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }
  </script>
	</body>
</html>
