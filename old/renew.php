<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();


if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}

	



if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
    

    
  
    
	if ($user) 
	{
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();
	
			  
	
			
				SESSION::login($user);
                msgbox("Login Success");
               
               
				AUDIT::insert($user['account_ID'],"LOGGED IN");
				
				
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}



	//QUERIES




	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
	  	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>

      <title>HOME | School Mancagement System v2.0</title>
	</head>

	<body>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
            
			<!-- alert messages -->
			<?php 
			
			SESSION::DisplayMsg(); 
			
			
			
			?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
				
				//audit
				
				
				
					?>
				<div class="col-md-3">
            		<?php include(PAGES."navigation.php");?>
				</div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 


								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>
								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

									</div>
								</div>
                                
                                	<div class="form-group row">
									<div class="col-lg-offset-9">
										 <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                                
                                </fieldset>
                    </form>
                    </div>	
                </div>
                
                <?php   }else{
				
				AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
				
				?>

<div class="col-md-3">

<?php include(PAGES."navigation.php");?>
</div>
              
                
<div class="col-md-9">
	<div class="panel panel-default">

		<div class="panel-heading">
			<table>
        		<tr>
        			<td width="50%">
						<h4 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"> Request <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
					</td>
				 	<td>
            			<div class="input-group col-md-12 hide">
              				
              				<input id="searchItem" class="searchItem form-control"  type="text" autofocus="autofocus"  style="text-transform: uppercase;"/>
                            
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            			</div>
        			</td>
        			<td width="5%">
	    				<div class="col-md-1">   
	    					<a id="" class='btn btn-success btn-ms btnCSV' >Export as CSV</a>
	    				</div>
	    			</td>
	    		</tr>
	    	</table>
		</div>
		<div class="panel-body"><br>
			<div class="row">
            	<div class="form-group">
	                <div class="col-sm-6">
	                    <input name='' id="myInput" placeholder="Search by borrower, status or book title" class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
	                </div>
            	</div>
           	</div><br>
           	<div id="dvData">
	        	<table id="myTable" class="table table-hover table-responsive table-striped text-md" style="width:100%;">
			        <thead>
			            <tr>
			              <th>Borrower</th>
			              <th>Status</th>
			              <th>Book Title</th>
			              <th>Accession No</th>
			              <th>Borrowed Date</th>
			              <th>Return Date</th>
			              <th>Billing</th>
			              <th>Action</th>
			            </tr>
			        </thead>
			        <tbody id="tbody">
			        </tbody>
			    </table>
			</div>
		  	<div class="modal fade" id="myModal2" role="dialog">
			    <div class="modal-dialog">
			      	<div class="modal-content">
				        <div class="" style="padding:40px 40px 0px 40px; text-align: center;">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <h4 style="">Update Book Tool</h4>
				          <p>Enter the details of the book below to update the inventory of library management module</p>
				        </div>
				        <div class="modal-body" style="padding:40px 140px;">
		                    <div class="success hide" id="success"><strong>Success!</strong> Successfully created password!</div>
					        <div class='form-group'>
					            <input id="RborrowerID" type='text' class="form-control" placeholder=" Borrower" disabled="" />
					        </div>
					        <div class='form-group'>
					            <input type='text' class="form-control" id='Rdatetimepicker' placeholder=" Borrow Date" />
					            <span style=" margin-top: -30px; float: right; margin-right: 7px;"><i class="fa fa-calendar"></i></span>
					        </div>
					        <div class='form-group'>
					            <input type='text' class="form-control" id='Rdatetimepicker4' placeholder=" Returned Date" />
					            <span style=" margin-top: -30px; float: right; margin-right: 7px;"><i class="fa fa-calendar"></i></span>
					        </div>
		                    <div class="form-group">
		                        <select id="statusID" required class="form-control" name="payments">
		                            <option value="1">Borrowed and Issued</option>
		                            <option value="2">Request</option>
		                            <option value="3">Renew</option>
		                            <option value="4">Returned</option>
		                        </select> 
					        </div>
				          	<button type="submit" class="btn btn-success btn-block" id="RsaveBtn"> Save</button>
				        </div>
			      	</div>
			    </div>
		  	</div> 
	 	</div>
	</div>
</div>
<?php } ?>

			</div>
            <?php
			?>
			<?php include (LAYOUTS . "footer.php"); ?>
 			<script src="<?php echo HOME; ?>old/assets/js/custom/lb_material.js"></script>

		</div>
  <script type="text/javascript">
  	$( window ).ready(function(){
        setTimeout(function() {

      		$.main.library.fetchallrenew();
 
        },100);
  	})
    function myFunction() {
      var input, filter, table, tr, td, i;
      input = document.getElementById("myInput");
      filter = input.value.toUpperCase();
      table = document.getElementById("myTable");
      tr = table.getElementsByTagName("tr");
      for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        td2 = tr[i].getElementsByTagName("td")[1];
        td1 = tr[i].getElementsByTagName("td")[2];
        if (td) {
          if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
          } else {
            tr[i].style.display = "none";
          }
        }       
      }
    }

    $(function () {
        $('#datetimepicker').datetimepicker();
        $('#datetimepicker4').datetimepicker();
        $('#Rdatetimepicker').datetimepicker();
        $('#Rdatetimepicker4').datetimepicker();
        $('#Udatetimepicker').datetimepicker();
        $('#Udatetimepicker4').datetimepicker();
    });
  </script>
	</body>
</html>
