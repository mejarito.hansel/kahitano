<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();


if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}

	



if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
    

    
  
    
	if ($user) 
	{
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();
	
			  
	
			
				SESSION::login($user);
                msgbox("Login Success");
               
               
				AUDIT::insert($user['account_ID'],"LOGGED IN");
				
				
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}



	//QUERIES




	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
      <title>HOME | School Management System v2.0</title>
	</head>

	<body>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
            
			<!-- alert messages -->
			<?php 
			
			SESSION::DisplayMsg(); 
			
			
			
			?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
				
				//audit
				
				
				
					?>
				<div class="col-md-3">
            		<?php include(PAGES."navigation.php");?>
				</div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 


								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>
								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

									</div>
								</div>
                                
                                	<div class="form-group row">
									<div class="col-lg-offset-9">
										 <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                                
                                </fieldset>
                    </form>
                    </div>	
                </div>
                
                <?php   }else{
				
				AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
				
				?>

<div class="col-md-3">

<?php include(PAGES."navigation.php");?>
</div>
              
                
<div class="col-md-9">
	<div class="panel panel-default">

		<div class="panel-heading">
			<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
			<i class="fa fa-folder-o"></i> Add Materials</h3> 
		</div>
		<div class="panel-body">
	        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well' name=form1>
	            <fieldset>
	                <legend><i class="fa fa-user"></i> Materials Information</legend>

	                <div class="form-group">
	                    <label for='Accession_No.' class='col-md-3'>Accession No</label>
	                </div>
	                <div class="form-group">
	                    <div class="col-sm-3">
	                        <input name='accesion_no' pattern="([a-zA-Z0-9]|ñ|Ñ|.| |/|\|@|#|$|%|&)+" placeholder="Accession No." class='form-control' type='text' required autofocus style="text-transform:uppercase"/>
	                    </div>
	                </div>
	               	<div class="form-group">
	                    <label for='si_FNAME' class='col-md-3'>Author Name</label>
	                </div>
	                <div class="form-group">
	                    <div class="col-sm-3">
	                        <input name='user_FNAME' placeholder="First Name" class='form-control' type='text' required autofocus  style="text-transform:uppercase"/>
	                    </div>
	                    <div class="col-sm-3">
	                        <input name='user_MNAME' pattern="([a-zA-Z0-9]|ñ|Ñ|.| |/|\|@|#|$|%|&)+" placeholder="Middle Name"  class='form-control' type='text' style="text-transform:uppercase" />
	                    </div>
	                    <div class="col-sm-3">
	                        <input name='user_LNAME' style="text-transform:uppercase" pattern="([a-zA-Z0-9]|.|ñ|Ñ| |/|\|@|#|$|%|&)+" placeholder="Last Name" class='form-control' type='text' required/>
	                    </div>
	                    <div class="col-sm-2">
	                        <input name='suffix_name' style="text-transform:uppercase" pattern="([a-zA-Z0-9]|.|ñ|Ñ| |/|\|@|#|$|%|&)+" placeholder="Suffix" class='form-control' type='text' required/>
	                    </div>
	                </div>
	                <div class="form-group">
                        <div class="col-md-3">
                            <label for='mt' class=''>Material Title</label>
                        </div>
                        <div class="col-md-3">
                            <label for='mc' class=''>Material Copyright</label>
                        </div>
                        <div class="col-md-3">
                            <label for='mc' class=''>Material Source</label>
                        </div>
                        <div class="col-md-3">
                            <label for='mdr' class=''>Material Date Received</label>
                        </div>
                    </div>
                    <div class="form-group">
                     	<div class="col-sm-3">
                     		<input name='mt' id="lb_mt" placeholder="Material Title" class='form-control' type='text' style="text-transform:uppercase" /></div>
                     	<div class="col-sm-3">
                     		<input name='mc' id="lb_mc" placeholder="Material Copyright" class='form-control' type='text' style="text-transform:uppercase" /></div>
                     	<div class="col-sm-3">
                     		<input name='ms' id="lb_ms" placeholder="Material Source" class='form-control' type='text' style="text-transform:uppercase" /></div>
						<div class="col-sm-3">
							<input name='mdr' id="mdr_id" placeholder="Date Received" class='form-control datepicker' required/>
						</div>
                    </div>
	                <div class="form-group">
                        <div class="col-md-3">
                            <label for='rt' class=''>Resource Type</label>
                        </div>
                        <div class="col-md-3">
                            <label for='cn' class=''>Call Number</label>
                        </div>
                    </div>
                    <div class="form-group">
                     	<div class="col-sm-3">
                     		<select name="rt" id="rt_id" class='form-control'>  
                                <option value=''>Books</option>
                                <option value="">Periodicals</option>
                                <option value="">Others</option>
                            </select> 
                        </div>
                     	<div class="col-sm-3">
                     		<input name='mc' id="lb_cn"  placeholder="Call Number" class='form-control' type='text' style="text-transform:uppercase" />
                     	</div>
                    </div>
                    <div class="form-group">
	                    <div class="col-sm-3">
	                        <button  class='btn btn-success btn-block' type='btn'>
	                        <i class='fa fa-edit'></i> Add Material</button>
	                    </div>
                    </div>
	         	</fieldset>
	     	</form>
	 	</div>
	</div>
</div>
<?php } ?>
  
			</div>
            <?php 
			?>
			<?php include (LAYOUTS . "footer.php"); ?>

		</div>
	</body>
</html>
