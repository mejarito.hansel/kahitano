<?php 
//INITIALIZE INCLUDES
    include('init.php');
#SESSION::CheckLogin();

    //QUERIES
    if(Request::post()){
        switch(Request::get("action")) {
            case "add": 
            #var_dump(Request::post());
            USERS::addstudent(Request::post());
            #var_dump(Request::post());
            break;
            case "edit": USERS::update_student($_GET['id'],Request::post()); break;
            #case "update_blog":  BLOG::update(Request::post("blog_info")); break;
            
            #case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
            
            default: Fail::not_found();
        }
    }
    
    if(Request::get()){
        switch(Request::get("action")) {
            case "delete": ANNOUNCEMENT::delete($_GET['id']); break;
        
        }
    }       
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
      <title>Student Information | School Management System v2.0</title>
      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">

    </head>

    <body>
        <div class="modal fade" id="modal_updateinformation" tabindex="-1" role="dialog" aria-labelledby="modal_updateinformation">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-body text-center">
                <h3>Confirmation Box<br><br><small>Do you want to save your changes?</small></h3>
              </div>
              <div class="modal-footer" style="border-top:none; text-align:center !important; padding: 0px 20px 20px !important;">
                <button type="button" class="btn btn-xs btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-xs btn-primary btnUpdateInformation">Save changes</button>
              </div>
            </div>
          </div>
        </div>

        <!-- top nav -->
        <?php include(LAYOUTS . 'top_nav.php'); ?>
        <!-- end nav -->

        <div class="container">
            
            <!-- banner -->
            <?php include(LAYOUTS . "banner.php"); ?>
            <!-- end banner -->
            
          
            
            
            <!-- alert messages -->
            <?php SESSION::DisplayMsg(); ?>
            <!-- end of alert messages -->
            
            <!-- start row container -->
            <!-- 
            <div class="row">
                
                <div class="col-lg-12">

                
                    <ul class="breadcrumb">
                        <li class="active">Home</li>
                    </ul>
                

                </div>
                
            </div>
            -->
            <!-- end row container -->
            
   
             
            <!-- row container -->
            <div class="row">
            <!-- left nav -->

        
                <div class="col-md-4 col-lg-3">
                    <?php include(PAGES."navigation.php");?>
                </div>
                
                <!-- end left nav -->
                
                <!-- BODY -->
                
                
                <div class="col-lg-9 ">
                <?php if (SESSION::isLoggedIn()) { ?>
                    <?php
                    $CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
                    switch($_GET['action']){
                        case "add":
                        
                        AUDIT::ins($user['account_ID'],"Student Information","Visited Add Student Page");
                        
                        ?>
                            <div class="panel panel-default">
                            
                                    <div class="panel-heading">
                                        <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                        <i class="fa fa-folder-o"></i> Add Student Information </h4> 
                                    </div>
                            <div class="panel-body">
                                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well' name=form1>
                                            <fieldset>
                                                    <legend><i class="fa fa-user"></i> Student Information</legend>

                                                    <div class="form-group">
                                                        <label class="col-md-4">Student No.
                                                        </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                            <select onchange="getData(this);" required class="form-control col-md-4" name="student_choose">
                                                                <option value="">Please Choose</option>
                                                                <option value="auto">Autogenerate</option>
                                                                <option value="specify">Specify</option>
                                                            </select>    
                                                        </div>
                                                        
                                                     </div>
                                                     
                                                    <script>
                                                    function getData(dropdown) {
                                                      var value = dropdown.options[dropdown.selectedIndex].value;
                                                     if (value == 'specify'){
                                                      document.getElementById("specify").style.display = "block";
                                                      document.getElementById("user_STUDENT_ID").disabled = false;
                                                      document.getElementById("user_STUDENT_ID").focus();
                                                      document.getElementById("auto1").style.display = "none";
                                                      
                                                      
                                                     }
                                                     if(value == 'auto'){
                                                       document.getElementById("auto1").style.display = "block";
                                                       document.getElementById("specify").style.display = "none";
                                                       document.getElementById("user_STUDENT_ID").disabled = true;
                                                       
                                                     }
                                                      if(value == ''){
                                                       document.getElementById("auto1").style.display = "none";
                                                       document.getElementById("user_STUDENT_ID").disabled = true;
                                                       document.getElementById("specify").style.display = "none";
                                                     }
                                                    }</script>
                                                    
                                                    <div class="form-group" id="auto1" style="display:none">
                                                        <label for='user_EMAIL' class='col-md-4'>
                                                       
                                                        Autogenerate: <br />
                                                        <select class="form-control"  name="auto1" >
                                                        <?php for($x=date("y");$x>=18;$x--){?>

                                                            <option value="MMMA-<?=$x; ?>-">Prefix: MMMA-<?=$x; ?>-XXX</option>
                                                        <?php } ?> 
                                                        </select></label>
                                                    </div>
                                                        
                                                   <div class="form-group" id="specify" style="display:none">  
                                                        <label for='user_EMAIL' class='col-md-4'>
                                                       
                                                         Specifiy: <br />
                                                         <input id="user_STUDENT_ID" required name='user_STUDENT_ID' placeholder="Sxxxxxxx" class='form-control'  type='text' style="text-transform:uppercase" /></label>
                                                    </div>
                                                    
                                                    
                                                    <!--
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-3'>Student No.:</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-5"><input name='user_STUDENT_ID' placeholder="Sxxxxxxx" class='form-control' required type='text' style="text-transform:uppercase" /></div>     
                                                    </div> 
                                                    -->
                                                    
                                                       
                                                    
                                                    <!-- Name -->
                                                    <div class="form-group">
                                                            <label for='si_FNAME' class='col-md-3'>Student Name</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <input name='user_FNAME' pattern="([a-zA-Z0-9]|ñ|Ñ|.| |/|\|@|#|$|%|&)+" placeholder="First Name" class='form-control' type='text' required autofocus  style="text-transform:uppercase"/>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input name='user_MNAME' pattern="([a-zA-Z0-9]|ñ|Ñ|.| |/|\|@|#|$|%|&)+" placeholder="Middle Name"  class='form-control' type='text' style="text-transform:uppercase" />
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input name='user_LNAME' style="text-transform:uppercase" pattern="([a-zA-Z0-9]|.|ñ|Ñ| |/|\|@|#|$|%|&)+" placeholder="Last Name" class='form-control' type='text' required/>
                                                        </div>
                                                    </div>
                                                   <div class="form-group">
                                                        <div class="col-md-7">
                                                            <label for='user_DOB' >Date Of Birth</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for='user_GENDER' >Gender</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <select onchange="getAge();" name="user_MONTH" id="user_MONTH" class='form-control' >  
                                                            <option value='0' >MONTH</option>
                                                            <?php
                                                                $months = array('January','February','March','April','May','June','July ','August','September','October','November','December',);
                                                                $x = 1;
                                                                foreach ($months as $month) {
                                                                    echo "<option value=\"" . $x . "\">" . $month . "</option>";
                                                                    $x++;
                                                                }
                                                            ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <select onchange="getAge();" name="user_DATE" id="user_DATE"  class='form-control' >  
                                                                <option value='0'>DAY</option>
                                                            <?php for($i=1;$i<=31;$i++){?>
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php }?>
                                                            </select> 
                                                        </div>
                                                        <div class="col-sm-2">
                                                           <select onchange="getAge();"  name="user_YEAR" id="user_YEAR" class='form-control' >  
                                                                <option value='0'>YEAR</option>
                                                                <?php for($i=2014;$i>=1884;$i--){?>
                                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php }?>
                                                            </select> 
                                                        </div>
                                                        <script>
                                                            function getAge(value){
                                                                console.log($("#user_YEAR").val());
                                                                if($("#user_MONTH").val() != "0" && $("#user_YEAR").val() != "0" && $("#user_DATE").val()!= "0"){
                                                                var birthDate = new Date($("#user_YEAR").val()+"-"+$("#user_MONTH").val()+"-"+$("#user_DATE").val());
                                                                var ageDifMs = Date.now() - birthDate.getTime();
                                                                var ageDate = new Date(ageDifMs); // miliseconds from epoch
                                                                console.log(Math.abs(ageDate.getUTCFullYear() - 1970));
                                                                $("#sai_age").val(Math.abs(ageDate.getUTCFullYear() - 1970));
                                                                }
                                                            }
                                                        </script>



                                                        <label for="user_GENDER">
                                                            <div class="col-sm-7">
                                                                <div class="radio">
                                                                    <label>
                                                                    <input type="radio" checked="checked" name="user_GENDER" class="radio" value="Male" >Male
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="radio">
                                                                    <label>
                                                                    <input type="radio" name="user_GENDER" class="radio" Value="Female" >Female
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    
                                                    

                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for='user_Age' class=''>Age</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for='user_Height' class=''>Height(cm)</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for='user_Weight' class=''>Weight(kgs)</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                         <div class="col-sm-3"><input name='user_age' id="sai_age"  placeholder="Age" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_Height' id=sai_height  placeholder="Height" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_Weight' id="sai_weight"  placeholder="Weight" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for='user_Religion' class=''>Religion</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for='user_CivilStatus' class=''>Civil Status</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                         <div class="col-sm-3"><input name='user_Religion' id=""  placeholder="religion" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_CivilStatus' id=""  placeholder="Civil Status" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for='user_SpecialSkill' class=''>Special Skills</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-9"><input name='user_SpecialSkill' id=""   placeholder="Special Skills" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for='user_placeofBirth' class='col-md-3'>Place of Birth</label>
                                                    </div>
                                                    <div class="form-group">
                                                         <div class="col-sm-9"><input name='user_placeADDRESS'  placeholder="City, Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    
                                                    </div>
                                                    <!-- Address -->
                                                    <div class="form-group">
                                                        <label for='user_ADDRESS' class='col-md-3'>Present Address</label>
                                                    </div>
                                                    
                                                    <!--<div class="form-group">
                                                         <div class="col-sm-9"><input name='user_ADDRESS'  placeholder="BXX LXX ST., Subdv, City, Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    -->
                                                    
                                                    <div class="form-group">
                                                         <div class="col-sm-4"><input name='user_STREET' id="a1" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Street No. and St Address" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_BRGY' id="a2" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Barangay" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_CITY' id="a3" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="CITY" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3"><input name='user_DISTRICT' id="a4" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="District" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-4"><input name='user_PROVINCE' id="a5" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-3"><input name='present_ContactNo' id="a3" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Contact No." class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>  

                                                    <div class="form-group">
                                                        <label for='permanent_ADDRESS' class='col-md-3'>Permanent Address</label>
                                                    </div>
                                                    
                                                    <!--<div class="form-group">
                                                         <div class="col-sm-9"><input name='user_ADDRESS'  placeholder="BXX LXX ST., Subdv, City, Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    -->
                                                    
                                                    <div class="form-group">
                                                         <div class="col-sm-4"><input name='permanent_STREET' id="x"   placeholder="Street No. and St Address" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='permanent_BRGY' id="x" placeholder="Barangay" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='permanent_CITY' id="ax3"   placeholder="CITY" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3"><input name='permanent_DISTRICT'   placeholder="District" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-4"><input name='permanent_PROVINCE' id="x"   placeholder="Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-3"><input name='permanent_ContactNo' id="a3"   placeholder="Contact No." class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>   
                                                    
                                                    
                                                    
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label for='user_DOB' >Email Address</label>
                                                        </div>
                                                        <!-- <div class="col-md-4">
                                                            <label for='user_GENDER' >Contact No.</label>
                                                        </div> -->
                                                    </div>
                                                     <div class="form-group">
                                                            <div class="col-sm-4"><input name='user_EMAIL' placeholder="juandelacruz@yahoo.com" class='form-control' style="text-transform:uppercase" type='text' /></div>
                                                            <!-- <div class="col-sm-4"><input name='user_CONTACT' placeholder="09XXXXXXXXX" class='form-control' type='text' style="text-transform:uppercase" /></div> -->
                                                    </div>
                                                    
                                                    <!-- Email Address -->
                                                    
                                                    
                                                     <div class="form-group">
                                                        <label for='user_Course' class='col-md-3'>Course</label>
                                                    </div>
                                                    <div class="form-group">
                                                      <div class="col-sm-5"><input name='user_DEGREE' placeholder="0000000" class='form-control' type='hidden' style="text-transform:uppercase" />
                                                      
                                                      
                                                      
                                                  <select name=curricullum_ID class="form-control">
                                                    <?php $getallCurricullum= CECONTROLLER::getallCurricullum(array("course_ID"=>"ASC"));   
                                                    
                                                    
                                                        foreach($getallCurricullum as $key => $value){
                                                        
                                                            $cid = $value['course_ID'];
                                                            $getsingleCourse = CECONTROLLER::getsingleCourse(array("course_ID"=>"ASC"),$cid);
                                                            
            
                                                            
                                                            
                                                            foreach($getsingleCourse as $key2 => $val){
                                                    ?>
                                                    <option value=<?= $value['curricullum_ID']?>>
                                                        <?= $val['course_NAME']." - ".$value['curricullum_NAME']; ?>
                                                    </option>
                                                    <?php } } ?>
                                                </select>
                                                      
                                                     
                                                      </div>     
                                                         
                                                    </div>    
                                                    
                                                    <br/>
                                            
                                                        <!-- Requirements Submitted -->
                                                        <legend><i class="fa fa-edit"></i> Requirements Submitted</legend>
                                     
                                     
                                     
                                            <!-- STUDENT TYPE -->
                                                      <div class="form-group">
                                                            <label for="user_UNAME" class="control-label col-sm-2">Student Type</label>
                                                            <div class="col-md-5">
                                                                 <?php
                                                                        $student_type = USERS::getAllStudentType();
                                                                        ?>
                                                                      <select name="student_type_ID" id="student_type_ID" class="form-control">
                                                                        <?php foreach($student_type as $key => $val){ ?>
                                                                            <option value="<?= $val['student_type_ID']; ?>"><?= $val['student_type_NAME']; ?></option>
                                                                        <?php } ?>    
                                                              </select>
                                                              <!--
                                                                      <select name="student_type_ID2" id="student_type_ID2" class="form-control">
                                                                        <?php foreach($student_type as $key => $val){ ?>
                                                                        <option value="<?= $val['student_type_ID']; ?>">
                                                                          <?= $val['student_type_NAME']; ?>
                                                                        </option>
                                                                        <?php } ?>
                                                                      </select> 
                                                               -->       
                                                            </div>
                                            </div>
                                                       <!-- STUDENT TYPE --> 
                                            <script type="text/javascript">
                                                            $(document).ready(function(){
                                                                $("#student_type_ID").change(function(){
                                                                    $( "#student_type_ID option:selected").each(function(){
                                                                        
                                                                          <?php foreach($student_type as $key => $val){ ?>
                                                                        
                                                                        if($(this).attr("value")=="<?php print($val['student_type_ID']); ?>"){
                                                                               <?php foreach($student_type as $key1 => $val1){ ?>   
                                                                                    <?php if($val['student_type_ID'] == $val1['student_type_ID'])
                                                                                    { ?>     
                                                                                       $("#student_type_<?php echo $val1['student_type_ID']; ?>").show(); 
                                                                                          $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',false);                                                                       
                                                                                     <?php }else{ ?> 
                                                                                      $("#student_type_<?php echo $val1['student_type_ID']; ?>").hide();
                                                                                      $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',true);
                                                                                    <?php } ?>  
                                                                               <?php } ?>     
                                                                        }
                                                                        
                                                                        <?php } ?>
                                                                       
                                                                    });
                                                                }).change();
                                                            });
                                                        </script>
                                            
                                                        <div class="form-group">
                                                           
                                                           
                                                           <label for="user_UNAME" class="control-label col-sm-3">Requirements</label>
                                                             <label for="user_UNAME" class="control-label col-sm-2">Status</label>   
                                                        </div>
                                            
                                            
                                                      <!-- REQUIREMENTS -->
                                                      <?php
                                                        $student_type = USERS::getAllStudentType(); ?>
                                                            
                                                            <?php foreach($student_type as $key => $val){ ?>
                                                                    
                                                                      
                                                            <div id="student_type_<?= $val['student_type_ID']; ?>" style="/*display:none*/">
                                                                <?php $requirements = USERS::getStudentTypeRequirements($val['student_type_ID']); ?>
                                                                <?php foreach($requirements as $key1 => $val1){ ?>
                                                                            
                                                                    <div class="form-group">    
                                                                             <label for="user_UNAME" class="control-label col-sm-3">    <?= $val1['req_NAME']; ?></label>
                                                                            <div class="col-md-3">
                                                                          <select class="form-control" name="<?= $val1['req_ID']; ?>">
                                                                                <option value="0">Not Passed</option>
                                                                                <option value="1">Passed</option>
                                                                                
                                                                           </select>    
                                                                            
                                                                            </div>
                                                                    
                                                                    </div>
                                                                <?php } ?>  
                                                            </div>
                                                                    <?php } ?>    
                                                                  
                                                           <br/><br/>
                                                                  
                                            
                                                  <!-- REQUIREMENTS -->  
                                        
                                        
                                            <legend><i class="fa fa-user"></i> Contact Person (Parent/Guardian) In Case of Emergency</legend>
                                                    <!-- Guardian & Relationship-->
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>Guardian</label>
                                                        <label for='user_EMAIL' class='col-md-4'>Relationship</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>
                                                                <input name='user_GUARDIAN' style="text-transform:uppercase" class='form-control' type='text'/>
                                                        </label>
                                                        <label for='user_EMAIL' class='col-md-4'>
                                                                <input name='user_RELATIONSHIP' style="text-transform:uppercase" class='form-control' type='text' />
                                                        </label>
                                                    </div>
                                                    <!-- Occupation & Contact Number-->
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>Occupation</label>
                                                        <label for='user_EMAIL' class='col-md-4'>Contact No.</label>
                                                    </div>
                                            <div class="form-group">
                                                <label for='user_EMAIL' class='col-md-5'><input name='user_OCCUPATION' style="text-transform:uppercase" class='form-control' type='text' id="user_OCCUPATION" /></label>
                                                <label for='user_EMAIL' class='col-md-4'>
                                                        
                                                        <input name='user_GUARDIAN_CONTACT' style="text-transform:uppercase" class='form-control' type='text'/>
                                                </label>
                                              </div>
                                                    <!-- Guardian Address -->
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-3'>Guardian Address</label>
                                                    </div>
                                                    <div class="form-group">
                                                         <div class="col-sm-9"><input id="G1" name='user_GUARDIAN_ADDRESS' style="text-transform:uppercase" class='form-control' type='text' /></div>
                                                    </div>    
                                                    <!-- Elem & Graduated date-->
                                                    <legend><i class="fa fa-user"></i> Educational Attainment</legend>
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>Elementary</label>
                                                        <label for='user_EMAIL' class='col-md-4'>Date Graduated</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>
                                                                <input name='user_ELEMENTARY' style="text-transform:uppercase" class='form-control' type='text' list="elem" />
                                                                
                                                                 <?php
                                                                $elem = USERS::getAllElementary();
                                                                ?>
                                                                <datalist id="elem">
                                                                <?php foreach($elem as $key => $val){ ?>
                                                                    <option value="<?= $val['sai_elementary_graduated']; ?>"></option>
                                                                <?php } ?>    
                                                                </datalist>
                                                                
                                                                
                                                        </label>
                                                        <label for='user_EMAIL' class='col-md-4'>
                                                            <select name="user_ELEM_YR" class='form-control'>  
                                                            <option value='' readonly>YEAR</option>
                                                            <?php for($i=2014;$i>=1980;$i--){?>
                                                                <option value="<?php echo ($i-1)." - ".$i; ?>"><?php echo ($i-1)." - ".$i; ?></option>
                                                            <?php }?>
                                                            </select>          
                                                        </label>
                                                    </div>    
                                                    <!-- Highschool & Graduated date-->
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>High School</label>
                                                        <label for='user_EMAIL' class='col-md-4'>Date Graduated</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>
                                                                <input name='user_HIGHSCHOOL' style="text-transform:uppercase" class='form-control' list="highschools" type='text' />
                                                                
                                                                
                                                                <?php
                                                                $highschools = USERS::getAllHighSchool();
                                                                ?>
                                                                
                                                                <datalist id="highschools">
                                                                <?php foreach($highschools as $key => $val){ ?>
                                                                    <option value="<?= $val['sai_highschool_graduated']; ?>"></option>
                                                                <?php } ?>    
                                                                </datalist>
                                                                
                                                                
                                                        </label>
                                                        <label for='user_EMAIL' class='col-md-4'>
                                                            <select name="user_HS_YR" class='form-control'>  
                                                            <option value='' readonly>YEAR</option>
                                                            <?php for($i=2018;$i>=1980;$i--){?>
                                                                <option value="<?php echo ($i-1)." - ".$i; ?>"><?php echo ($i-1)." - ".$i; ?></option>
                                                            <?php }?>
                                                            </select>          
                                                        </label>
                                                    </div> 
                                                        <!-- Occupation & Contact Number-->
                                                        <div class="form-group">
                                                            <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-5'>College Last Attended</label>
                                                            <label for='user_CLA_DATE' class='col-md-4'>Date</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-5'>
                                                            <input style="text-transform:uppercase" name='user_COLLEGE_LAST_ATTENDED' list="college" class='form-control' type='text' />
                                                            <?php
                                                                    $college = USERS::getAllCollege();
                                                                    ?>
                                                                    
                                                                    <datalist id="college">
                                                                    <?php foreach($college as $key => $val){ ?>
                                                                        <option value="<?= $val['sai_college_last_attended']; ?>"></option>
                                                                    <?php } ?>    
                                                                    </datalist>
                                                          </label>
                                                            <label for='user_EMAIL' class='col-md-4'>
                                                                    <input name='user_CLA_DATE' class='form-control' type='text' />
                                                            </label>
                                            </div>    
                                            
                                            
                                 
                              
                                        <div class="form-group">
                                                <label for='si_FNAME' class='col-md-3'>Admitted Date</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <input name='user_DATEADMITTED'  value="<?= date("Y-m-d"); ?>" class='form-control datepicker' type='text' required   style="text-transform:uppercase"/>
                                            </div>
                                            
                                        </div>
                                            <br>
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label class="control-form">ENCODED BY: &nbsp;<?= $user['account_FNAME'];?> <?= $user['account_MNAME'];?> <?= $user['account_LNAME'];?></label>
                                                
                                                <input type="hidden" name="user_ADDEDBY" value="<?= $user['account_FNAME'];?> <?= $user['account_MNAME'];?> <?= $user['account_LNAME'];?>" />
                                                </div>
                                                <div class="col-md-5">
                                                    <button  class='btn btn-success btn-block' onclick="return confirm('Are you sure all data is correct?')" type='submit'><i class='fa fa-edit'></i> Add Student</button>
                                                </div>
                                            </div>
                                        
                                        
                                        </fieldset>
                                    </form>
               </div></div>
                    <?php break; 
                        case "edit":
                        $siiid =$_GET['id'];
                        $getsingle = USERS::getSingle(array("si_ID"=>"DESC"),$siiid  );
                        $getsingle1 = USERS::getSingleSAI(array("si_ID"=>"DESC"), $siiid );
                        /*print_r($getsingle);
                        print_r($getsingle1);*/
                        
                        AUDIT::ins($user['account_ID'],"Student Information","Visit Edit Student Page of (". $getsingle['student_ID'] .")".$getsingle['si_FNAME']." ".$getsingle['si_MNAME']." ".$getsingle['si_LNAME']);
                        
                        
                    ?>
                    <script type="text/javascript">
                            var payload = {"applicant_ID":<?php echo $_GET['id']; ?>}
                            var host = window.location.protocol+"//"+window.location.hostname ;
                            var d = $.Deferred();
                            $.ajax({
                                method: "POST",
                                // url: "http://sms.massiveits.com/api/index.php/Magsaysay_controller/fetchApplicantInfoByApplicantID",
                                url: host+"/api/index.php/Magsaysay_controller/fetchApplicantInfoByApplicantID",
                                dataType: "json",
                                data: JSON.stringify(payload)
                            }).done(function (result, textStatus, jqXHR) {
                                $(".btnUpdateInformation").attr("data-id",result.payload.applicant_id);
                                $(".applicant_number").val(result.payload.applicant_number);
                                $(".applicant_email").val(result.payload.c);
                                $(".applicant_BIRTHPLACE").val(result.payload.applicant_BIRTHPLACE);
                                $(".applicant_fname").val(result.payload.applicant_fname);
                                $(".applicant_mname").val(result.payload.applicant_mname);
                                $(".applicant_lname").val(result.payload.applicant_lname);
                                $(".applicant_STREET").val(result.payload.applicant_STREET);
                                $(".applicant_BRGY").val(result.payload.applicant_BRGY);
                                $(".applicant_CITY").val(result.payload.applicant_CITY);
                                $(".applicant_email").val(result.payload.applicant_email);
                                $(".applicant_DISTRICT").val(result.payload.applicant_DISTRICT);
                                $(".applicant_PROVINCE").val(result.payload.applicant_PROVINCE);
                                $(".applicant_contact").val(result.payload.applicant_contact);
                                $('.dob-month option[value="'+result.payload.applicant_BIRTHMONTH+'"]').attr("selected","selected");
                                $('.dob-day option[value="'+result.payload.applicant_BIRTHDAY+'"]').attr("selected","selected");
                                $('.dob-year option:contains('+result.payload.applicant_BIRTHYEAR+')').attr("selected","selected");
                                console.log("===================================")
                                console.log(result.payload.applicant_BIRTHMONTH)
                                console.log(result.payload.applicant_BIRTHDAY)
                                console.log(result.payload.applicant_BIRTHYEAR)
                                console.log("===================================")
                                if(result.payload.applicant_GENDER === "Male"){
                                    $(".gender").eq(0).attr("checked","checked");
                                }else{
                                    $(".gender").eq(1).attr("checked","checked");
                                }
                                $.cookie('applicant_number',result.payload.applicant_number);
                                $.cookie('applicant_fname',result.payload.applicant_fname);
                                $.cookie('applicant_mname',result.payload.applicant_mname);
                                $.cookie('applicant_lname',result.payload.applicant_lname);
                                $.cookie('applicant_email',result.payload.applicant_email);
                                $.cookie('applicant_birthplace',result.payload.applicant_BIRTHPLACE);
                                $.cookie('applicant_birthmonth',result.payload.applicant_BIRTHMONTH);
                                $.cookie('applicant_birthdate',result.payload.applicant_BIRTHDAY);
                                $.cookie('applicant_birthyear',result.payload.applicant_BIRTHYEAR);
                                $.cookie('applicant_gender',$(".gender:checked").data("gender"));
                                $.cookie('applicant_present_street',result.payload.applicant_STREET);
                                $.cookie('applicant_present_brgy',result.payload.applicant_BRGY);
                                $.cookie('applicant_present_city',result.payload.applicant_CITY);
                                $.cookie('applicant_present_district',result.payload.applicant_DISTRICT);
                                $.cookie('applicant_present_province',result.payload.applicant_PROVINCE);
                                $.cookie('applicant_present_contact',result.payload.applicant_contact);
                            }).fail(function (jqXHR, textStatus, errorThrown,request) {
                            });
                             var host = window.location.protocol+"//"+window.location.hostname ;
                            var payload = {"aai_ID":<?php echo $_GET['id']; ?>}
                            var d = $.Deferred();
                            $.ajax({
                                method: "POST",
                                // url: "http://sms.massiveits.com/api/index.php/Magsaysay_controller/fetchAdditionalApplicantInfoByID",
                                url: host+"/api/index.php/Magsaysay_controller/fetchAdditionalApplicantInfoByID",
                                dataType: "json",
                                data: JSON.stringify(payload)
                            }).done(function (result, textStatus, jqXHR) {
                                $(".aai_height").val(result.payload.aai_height);
                                $(".aai_weight").val(result.payload.aai_weight);
                                $(".aai_religion").val(result.payload.aai_religion);
                                $(".aai_civil").val(result.payload.aai_civil);
                                $(".aai_present_addr_st").val(result.payload.aai_present_addr_st);
                                $(".aai_present_brgy").val(result.payload.aai_present_brgy);
                                $(".aai_present_city").val(result.payload.aai_present_city);
                                $(".aai_present_district").val(result.payload.aai_present_district);
                                $(".aai_present_prov").val(result.payload.aai_present_prov);
                                $(".aai_present_contact").val(result.payload.aai_present_contact);
                                $(".aai_degree_title").val(result.payload.aai_degree_title);
                                $(".aai_elementary_graduated").val(result.payload.aai_elementary_graduated);
                                $(".aai_elementary_year_graduated").val(result.payload.aai_elementary_year_graduated);
                                $(".aai_highschool_graduated").val(result.payload.aai_highschool_graduated);
                                $(".aai_highschool_year_graduated").val(result.payload.aai_highschool_year_graduated);
                                $(".aai_college_last_attended").val(result.payload.aai_college_last_attended);
                                $(".aai_cla_inclusive_date").val(result.payload.aai_cla_inclusive_date);
                                $('.elementary-dob-year option:contains('+result.payload.aai_elementary_year_graduated+')').attr("selected","selected");
                                $('.highschool-dob-year option:contains('+result.payload.aai_highschool_year_graduated+')').attr("selected","selected");
                                $('.college-dob-year option:contains('+result.payload.aai_cla_inclusive_date+')').attr("selected","selected");
                                $(".aai_age").val(result.payload.aai_age);
                                $(".aai_name_of_guardian").val(result.payload.aai_name_of_guardian);
                                $(".aai_occupation").val(result.payload.aai_occupation);
                                $(".aai_tel_cell_number").val(result.payload.aai_tel_cell_number);
                                $(".aai_address").val(result.payload.aai_address);

                                $.cookie('applicant_degree',result.payload.aai_degree_title);
                                $.cookie('applicant_age',result.payload.aai_age);
                                $.cookie('applicant_height',result.payload.aai_height);
                                $.cookie('applicant_weight',result.payload.aai_weight);
                                $.cookie('applicant_religion',result.payload.aai_religion);
                                $.cookie('applicant_civil',result.payload.aai_civil);
                                $.cookie('applicant_permanent_street',result.payload.aai_present_addr_st);
                                $.cookie('applicant_permanent_brgy',result.payload.aai_present_brgy);
                                $.cookie('applicant_permanent_city',result.payload.aai_present_city);
                                $.cookie('applicant_permanent_district',result.payload.aai_present_district);
                                $.cookie('applicant_permanent_province',result.payload.aai_present_prov);
                                $.cookie('applicant_permanent_contact',result.payload.aai_present_contact);
                                $.cookie('applicant_parentguardian_name',result.payload.aai_name_of_guardian);
                                $.cookie('applicant_parentguardian_occupation',result.payload.aai_occupation);
                                $.cookie('applicant_parentguardian_address',result.payload.aai_address);
                                $.cookie('applicant_parentguardian_contactno',result.payload.aai_tel_cell_number);
                                $.cookie('applicant_elementary_school',result.payload.aai_elementary_graduated);
                                $.cookie('applicant_elementary_year',result.payload.aai_elementary_year_graduated);
                                $.cookie('applicant_highschool_school',result.payload.aai_highschool_graduated);
                                $.cookie('applicant_highschool_year',result.payload.aai_highschool_year_graduated);
                                $.cookie('applicant_college_school',result.payload.aai_college_last_attended);
                                $.cookie('applicant_college_year',result.payload.aai_cla_inclusive_date);
                            }).fail(function (jqXHR, textStatus, errorThrown,request) {
                            });
                        </script>
                    <div class="alert-applicant-update alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>Updated Successfully!</strong>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="container-fluid">
                                <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                    <i class="fa fa-user-circle"></i> Applicant Information
                                    <a href="#!" class="btn btn-success btn-xs pull-right" data-toggle="modal" data-target="#modal_updateinformation">Save Changes</a>
                                    <!-- <a href="student_information.php?action=view&id=<?= $_GET['id']; ?>" class="btn btn-success btn-xs pull-right">Update Information</a> -->
                                </h4> 
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="container" id="applicant_information_body">
                                <div class="row">
                                    <div class="col-lg-4 bold">Applicant Number:</div>  
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="applicant_number form-control input-sm" name=""></div>
                                        </div>
                                    </div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Applicant Name:</div>   
                                    <div class="applicant_fullname col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="applicant_fname form-control input-sm" name=""></div>
                                            <div class="col-lg-4"><input type="text" class="applicant_mname form-control input-sm" name=""></div>
                                            <div class="col-lg-4"><input type="text" class="applicant_lname form-control input-sm" name=""></div>
                                        </div>
                                    </div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Date Of Birth:</div>   
                                    <div class="col-lg-8 applicant_dob">
                                        <div class="row">
                                            <div class="col-lg-4"><select class="form-control input-sm dob-month"></select></div>
                                            <div class="col-lg-4"><select class="form-control input-sm dob-day">
                                                <option value="0">Day</option>
                                            </select></div>
                                            <div class="col-lg-4"><select class="form-control input-sm dob-year dob-year1">
                                                <option value="0">Year</option>
                                            </select></div>
                                        </div>
                                    </div>   
                                </div>
                                <script type="text/javascript">
                                    $(".alert-applicant-update").slideUp(0)
                                    var dob = {
                                        month: function(classname){
                                            var month = [{"name":"Month"},{"name":"January"},{"name":"February"},{"name":"March"},{"name":"April"},{"name":"May"},{"name":"June"},{"name":"July"},{"name":"August"},{"name":"September"},{"name":"October"},{"name":"November"},{"name":"December"}];
                                            for(i=0;i<month.length;i++){
                                                $(classname).append("<option value='"+(i < 10 ? "0" : "") + (i + 0)+"'>" + month[i].name + "</option>");
                                            }
                                        },
                                        day: function(classname){
                                            for(i=1;i<=31;i++){
                                                $(classname).append("<option value='"+(i < 10 ? "0" : "") + (i + 0)+"'>" + i + "</option>");
                                            }
                                        },
                                        year: function(classname){
                                            var d = new Date();
                                            var currentyear = d.getFullYear();
                                            for(i=currentyear;i>=1884;i--){
                                                $(classname).append("<option>" + i + "</option>");
                                            }
                                        }
                                    }
                                    dob.month(".dob-month");
                                    dob.day(".dob-day");
                                    dob.year(".dob-year");

                                </script>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Gender:</div>   
                                    <div class="col-lg-8 applicant_gender">
                                        <div class="row">
                                            <div class="col-lg-4"><input type="radio" class="gender" name="gender" data-gender="Male"> Male</div>
                                            <div class="col-lg-4"><input type="radio" class="gender" name="gender" data-gender="Female"> Female</div>
                                        </div>
                                    </div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Age:</div>   
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="aai_age form-control input-sm" name=""></div>
                                        </div>
                                    </div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Height:</div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="aai_height form-control input-sm" name=""></div>
                                        </div>
                                    </div> 
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Weight:</div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="aai_weight form-control input-sm" name=""></div>
                                        </div>
                                    </div> 
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Religion:</div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="aai_religion form-control input-sm" name=""></div>
                                        </div>
                                    </div>  
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Civil Status:</div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="aai_civil form-control input-sm" name=""></div>
                                        </div>
                                    </div> 
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Place of Birth:</div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="applicant_BIRTHPLACE form-control input-sm" name=""></div>
                                        </div>
                                    </div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Present Address:</div>
                                    <div class="applicant_fullname col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="applicant_STREET form-control input-sm" name=""></div>
                                            <div class="col-lg-4"><input type="text" class="applicant_BRGY form-control input-sm" name=""></div>
                                            <div class="col-lg-4"><input type="text" class="applicant_CITY form-control input-sm" name=""></div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="applicant_DISTRICT form-control input-sm" name=""></div>
                                            <div class="col-lg-4"><input type="text" class="applicant_PROVINCE form-control input-sm" name=""></div>
                                            <div class="col-lg-4"><input type="text" class="applicant_contact form-control input-sm" name=""></div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-lg-6 applicant_presentaddress"></div>    -->
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Permanent Address:</div>
                                    <div class="applicant_permanentaddress col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="aai_present_addr_st form-control input-sm" name=""></div>
                                            <div class="col-lg-4"><input type="text" class="aai_present_brgy form-control input-sm" name=""></div>
                                            <div class="col-lg-4"><input type="text" class="aai_present_city form-control input-sm" name=""></div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-lg-4"><input type="text" class="aai_present_district form-control input-sm" name=""></div>
                                            <div class="col-lg-4"><input type="text" class="aai_present_prov form-control input-sm" name=""></div>
                                            <div class="col-lg-4"><input type="text" class="aai_present_contact form-control input-sm" name=""></div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">E-mail Address:</div>   
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <input type="text" class="applicant_email form-control input-sm" name="">
                                        </div>   
                                    </div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Program:</div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-12"><input type="text" class="aai_degree_title form-control input-sm" name=""></div>
                                        </div>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                <i class="fa fa-user-circle"></i> Parent/Guardian Information</h4> 
                        </div>
                        <div class="panel-body">
                            <div class="column">
                                <div class="row">
                                    <div class="col-lg-4 bold">Parent/Guardian Name:</div>   
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <input type="text" class="aai_name_of_guardian form-control input-sm" name="">
                                        </div>   
                                    </div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Occupation:</div>   
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <input type="text" class="aai_occupation form-control input-sm" name="">
                                        </div>   
                                    </div>  
                                </div><br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Contact No.:</div>   
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <input type="text" class="aai_tel_cell_number form-control input-sm" name="">
                                        </div>   
                                    </div>  
                                </div><br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Present Address:</div>   
                                    <div class="row">
                                        <div class="col-lg-5">
                                            <input type="text" class="aai_address form-control input-sm" name="">
                                        </div>   
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                <i class="fa fa-graduation-cap"></i> Educational Background</h4> 
                        </div>
                        <div class="panel-body">
                            <div class="column">
                                <div class="row">
                                    <div class="col-lg-4 bold">Elementary:</div>
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-8"><input type="text" class="aai_elementary_graduated form-control input-sm" name=""></div>
                                            <div class="col-lg-4">
                                                <select class="form-control input-sm elementary-dob-year">
                                                    <option value="0">Year</option>        
                                                </select>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">High School:</div>   
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-8"><input type="text" class="aai_highschool_graduated form-control input-sm" name=""></div>
                                            <div class="col-lg-4">
                                                <select class="form-control input-sm highschool-dob-year">
                                                    <option value="0">Year</option>        
                                                </select>
                                            </div>
                                        </div>
                                    </div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">College Last Attended:</div>   
                                    <div class="col-lg-8">
                                        <div class="row">
                                            <div class="col-lg-8"><input type="text" class="aai_college_last_attended form-control input-sm" name=""></div>
                                            <div class="col-lg-4">
                                                <select class="form-control input-sm college-dob-year">
                                                    <option value="0">Year</option>        
                                                </select>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                                <script type="text/javascript">
                                    var dob = {
                                        month: function(classname){
                                            var month = [{"name":"Month"},{"name":"January"},{"name":"February"},{"name":"March"},{"name":"April"},{"name":"May"},{"name":"June"},{"name":"July"},{"name":"August"},{"name":"September"},{"name":"October"},{"name":"November"},{"name":"December"}];

                                            for(i=0;i<month.length;i++){
                                                $(classname).append("<option value='"+i+"'>" + month[i].name + "</option>");
                                            }
                                        },
                                        day: function(classname){
                                            for(i=1;i<=31;i++){
                                                $(classname).append("<option value="+i+">" + i + "</option>");
                                            }
                                        },
                                        year: function(classname){
                                            var d = new Date();
                                            var currentyear = d.getFullYear();
                                            for(i=currentyear;i>=1884;i--){
                                                $(classname).append("<option value="+i+">" + i + "</option>");
                                            }
                                        }
                                    }
                                    dob.year(".elementary-dob-year");
                                    dob.year(".highschool-dob-year");
                                    dob.year(".college-dob-year");

//                                     $( document ).ready(function(){
//                                         console.log($(".applicant_fname").val())
//                                     });
                                    $(".btnUpdateInformation").on("click", function(){

                                        var payload = {

                                            "applicant_id": $(this).attr("data-id"),
                                            "applicant_fname": $(".applicant_fname").val(),
                                            "applicant_mname": $(".applicant_mname").val(),
                                            "applicant_lname": $(".applicant_lname").val(),
                                            "applicant_email": $(".applicant_email").val(),
                                            "applicant_password": 1234,
                                            "applicant_address": "",
                                            "applicant_contact": $(".applicant_contact").val(),
                                            "applicant_BIRTHYEAR": $(".dob-year option:selected").val(),
                                            "applicant_BIRTHMONTH": $(".dob-month option:selected").val(),
                                            "applicant_BIRTHDAY": $(".dob-day option:selected").val(),
                                            "applicant_BIRTHPLACE": $(".applicant_BIRTHPLACE").val(),
                                            "applicant_GENDER": $(".gender:checked").data("gender"),
                                            "applicant_STREET": $(".applicant_STREET").val(),    
                                            "applicant_BRGY": $(".applicant_BRGY").val(),
                                            "applicant_CITY": $(".applicant_CITY").val(),
                                            "applicant_DISTRICT": $(".applicant_DISTRICT").val(),
                                            "applicant_PROVINCE" : $(".applicant_PROVINCE").val(),
                                            "applicant_online_status": ""
                                        }
                                         var host = window.location.protocol+"//"+window.location.hostname ;
                                        $.ajax({
                                            method: "POST",
                                            // url: "http://sms.massiveits.com/api/index.php/Magsaysay_controller/updateApplicantInfo",
                                            url: host+"/api/index.php/Magsaysay_controller/updateApplicantInfo",
                                            dataType: "json",
                                            data: JSON.stringify(payload)
                                        }).done(function (data, textStatus, jqXHR) {

                                        }).fail(function (jqXHR, textStatus, errorThrown,request) {
                                        });
                                        var payload1 = {
                                            "aai_ID": $(this).attr("data-id"),
                                            "applicant_ID": $(this).attr("data-id"),
                                            "aai_name_of_guardian": $('.aai_name_of_guardian').val(),
                                            "aai_relationship": "",
                                            "aai_occupation": $(".aai_occupation").val(),
                                            "aai_tel_cell_number": $(".aai_tel_cell_number").val(),
                                            "aai_address": $(".aai_address").val(),
                                            "aai_elementary_graduated": $(".aai_elementary_graduated").val(),
                                            "aai_elementary_year_graduated": $(".elementary-dob-year option:selected").val(),
                                            "aai_highschool_graduated": $(".aai_highschool_graduated").val(),
                                            "aai_highschool_year_graduated": $(".highschool-dob-year option:selected").val(),
                                            "aai_college_last_attended": $(".aai_college_last_attended").val(),    
                                            "aai_cla_inclusive_date": $(".college-dob-year option:selected").val(),
                                            "aai_degree_title": $(".aai_degree_title").val(),
                                            "aai_age": $(".aai_age").val(),
                                            "aai_status":"",
                                            "aai_graduating" : "",
                                            "aai_religion": $(".aai_religion").val(),
                                            "aai_civil" : $(".aai_civil").val(),
                                            "aai_height": $(".aai_height").val(),
                                            "aai_weight" : $(".aai_weight").val(),
                                            "aai_special_skills": "",
                                            "aai_present_addr_st" :  $(".aai_present_addr_st").val(),
                                            "aai_present_brgy":  $(".aai_present_brgy").val(),
                                            "aai_present_city" :  $(".aai_present_city").val(),
                                            "aai_present_district": $(".aai_present_district").val(),
                                            "aai_present_prov" :  $(".aai_present_prov").val(),
                                            "aai_present_contact":  $(".aai_present_contact").val()
                                        }
                                         var host = window.location.protocol+"//"+window.location.hostname ;
                                        $.ajax({
                                            method: "POST",
                                            // url: "http://sms.massiveits.com/api/index.php/Magsaysay_controller/updateAdditionalApplicantInfo",
                                            url: host+"/api/index.php/Magsaysay_controller/updateAdditionalApplicantInfo",
                                            dataType: "json",
                                            data: JSON.stringify(payload1)
                                        }).done(function (data, textStatus, jqXHR) {
                                           $("#modal_updateinformation").fadeOut(1000).modal("hide");
                                             $(".alert-applicant-update").slideDown(500).delay(2000).slideUp(500)
                                            setTimeout(function(){
                                               window.location.href = "list_of_applicants.php?action=view&id=<?php echo $_GET['id']; ?>";
                                           },2000);
                                        }).fail(function (jqXHR, textStatus, errorThrown,request) {
                                        });
                                        });
                                </script>
                            </div>
                        </div>
                    </div>
            
            
                    <?php break;  
                        case "search":
                        include "searching.php";                        
                        ?>
                            
                    <?php break; 
                        case "view":

                        // echo "<script> $(function(){ audit_trail.insert('".$user['account_ID']."','".$CLIENT_IP."','View','MODULE: 1 Admission / 1.1 List of Applicants, DESCRIPTION: User visists List of Applicants');}) </script>";
                        $user_id = $user['account_ID'];
                        $action_event = "View";
                        $event_desc = "MODULE: Admission / List of Applicants, DESCRIPTION: User visists List of Applicants";
                        $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
                    ?>
                        <?php
                         if(isset($_GET['id']))
                        {
                            $siiid =$_GET['id'];
                            $getsingle = USERS::getSingle(array("si_ID"=>"DESC"),$siiid  );
                            $getsingle1 = USERS::getSingleSAI(array("si_ID"=>"DESC"), $siiid );
                        ?>
                        <script type="text/javascript">
                            var payload = {"applicant_ID":<?php echo $_GET['id']; ?>}
                             var host = window.location.protocol+"//"+window.location.hostname ;
                            var d = $.Deferred();
                            $.ajax({
                                method: "POST",
                                // url: "http://sms.massiveits.com/api/index.php/Magsaysay_controller/fetchApplicantInfoByApplicantID",
                                url: host+"/api/index.php/Magsaysay_controller/fetchApplicantInfoByApplicantID",
                                dataType: "json",
                                data: JSON.stringify(payload)
                            }).done(function (result, textStatus, jqXHR) {
                                console.log(result)
                                var fullname = result.payload.applicant_fname +" "+ result.payload.applicant_mname +" "+ result.payload.applicant_lname;
                                fullname = fullname.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                    return letter.toUpperCase();
                                });
                                // alert(result.payload.applicant_BIRTHMONTH)
                                if(result.payload.applicant_BIRTHMONTH == '01' || result.payload.applicant_BIRTHMONTH == '1'){
                                    month = "January"
                                }else if(result.payload.applicant_BIRTHMONTH == '02' || result.payload.applicant_BIRTHMONTH === '2' ){
                                    month = "Febuary"
                                }else if(result.payload.applicant_BIRTHMONTH == '03' || result.payload.applicant_BIRTHMONTH === '3' ){
                                    var month = "March"
                                }else if(result.payload.applicant_BIRTHMONTH == '04' || result.payload.applicant_BIRTHMONTH === '4' ){
                                    var month = "April"
                                }else if(result.payload.applicant_BIRTHMONTH == '05' || result.payload.applicant_BIRTHMONTH === '5' ){
                                    var month = "May"
                                }else if(result.payload.applicant_BIRTHMONTH == '06' || result.payload.applicant_BIRTHMONTH === '6' ){
                                    var month = "June"
                                }else if(result.payload.applicant_BIRTHMONTH == '07' || result.payload.applicant_BIRTHMONTH === '7' ){
                                    var month = "July"
                                }else if(result.payload.applicant_BIRTHMONTH == '08' || result.payload.applicant_BIRTHMONTH === '8' ){
                                    var month = "August"
                                }else if(result.payload.applicant_BIRTHMONTH == '09' || result.payload.applicant_BIRTHMONTH === '9' ){
                                    var month = "September"
                                }else if(result.payload.applicant_BIRTHMONTH === '10' || result.payload.applicant_BIRTHMONTH === '10' ){
                                    var month = "October"
                                }else if(result.payload.applicant_BIRTHMONTH === '11' || result.payload.applicant_BIRTHMONTH === '11' ){
                                    var month = "November"
                                }else{
                                    var month = "December"
                                }
                                var monthNames = ["January", "February", "March", "April", "May", "June",
                                  "July", "August", "September", "October", "November", "December"
                                ];

                                var d = new Date(result.payload.applicant_BIRTHMONTH);
                                var b = new Date();
                                // var month = monthNames[d.getMonth()];
                                var year = b.getFullYear();
                                var str = ""+year+"";
                                str = str.substring(2, str.length-0);
                                $(".applicant_number").text(result.payload.applicant_number);
                                $(".applicant_fullname").text(fullname);
                                $(".applicant_dob").text(month +" "+ result.payload.applicant_BIRTHDAY +", "+ result.payload.applicant_BIRTHYEAR);
                                $(".applicant_gender").text(result.payload.applicant_GENDER);
                                $(".applicant_birthplace").text(result.payload.applicant_BIRTHPLACE);
                                $(".applicant_presentaddress").text(result.payload.applicant_STREET+", "+result.payload.applicant_BRGY+", "+result.payload.applicant_CITY+", "+result.payload.applicant_DISTRICT+", "+result.payload.applicant_PROVINCE);
                                $(".applicant_contact").text(result.payload.applicant_contact);
                                $(".applicant_email").text(result.payload.applicant_email);
                                audit_trail.insert('<?php echo $user['account_ID']; ?>','<?php echo $CLIENT_IP; ?>','View','MODULE: 1 Admission / 1.1 List of Applicants(View Full Information), DESCRIPTION: User visists student record of: <br><br>Applicant Number: '+ $('.applicant_number').text() +'<br>Applicant Name: '+ $('.applicant_fullname').text());
                            }).fail(function (jqXHR, textStatus, errorThrown,request) {
                            });
                             var host = window.location.protocol+"//"+window.location.hostname ;
                            var payload = {"aai_ID":<?php echo $_GET['id']; ?>}
                            var d = $.Deferred();
                            $.ajax({
                                method: "POST",
                                // url: "http://sms.massiveits.com/api/index.php/Magsaysay_controller/fetchAdditionalApplicantInfoByID",
                                url: host+"/api/index.php/Magsaysay_controller/fetchAdditionalApplicantInfoByID",
                                dataType: "json",
                                data: JSON.stringify(payload)
                            }).done(function (result, textStatus, jqXHR) {
                                console.log(result)
                                $(".aai_height").text(result.payload.aai_height);
                                $(".aai_weight").text(result.payload.aai_weight);
                                $(".aai_religion").text(result.payload.aai_religion);
                                $(".aai_civil").text(result.payload.aai_civil);
                                $(".applicant_permanentaddress").text(result.payload.aai_present_addr_st+", "+result.payload.aai_present_brgy+", "+result.payload.aai_present_city+", "+result.payload.aai_present_district+", "+result.payload.aai_present_prov);
                                $(".elementary").text(result.payload.aai_elementary_graduated +" - "+ result.payload.aai_elementary_year_graduated);
                                $(".highschool").text(result.payload.aai_highschool_graduated +" - "+ result.payload.aai_highschool_year_graduated);
                                $(".college").text(result.payload.aai_college_last_attended +" - "+ result.payload.aai_cla_inclusive_date);
                                $(".aai_age").text(result.payload.aai_age);
                                $(".parentguardian").text(result.payload.aai_name_of_guardian);
                                $(".occupation").text(result.payload.aai_occupation);
                                $(".parentguardiancontact").text(result.payload.aai_tel_cell_number);
                                $(".parentguardianaddress").text(result.payload.aai_address);
                                $(".applicant_course_take").text(result.payload.aai_degree_title);
                            }).fail(function (jqXHR, textStatus, errorThrown,request) {
                            });
                        </script>
                <form action="list_of_applicants.php?action=edit&id=<?= $_GET['id']; ?>" enctype='multipart/form-data' method='POST' class='form-horizontal' name=form1>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="container-fluid">
                                <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                    <i class="fa fa-user-circle"></i> Applicant Information
                                    <!-- <a id="P3C1_UPDATEINFORMATION" href="list_of_applicants.php?action=edit&id=<?= $_GET['id']; ?>" class="btn btn-success btn-xs pull-right">Update Information</a> -->
                                </h4> 
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 bold">Applicant Number:</div>   
                                    <div class="applicant_number col-lg-6"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Applicant Name:</div>   
                                    <div class="applicant_fullname col-lg-6"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Date Of Birth:</div>   
                                    <div class="col-lg-6 applicant_dob"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Gender:</div>   
                                    <div class="col-lg-6 applicant_gender"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Age:</div>   
                                    <div class="col-lg-6 aai_age"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Height:</div>   
                                    <div class="col-lg-6 aai_height"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Weight:</div>   
                                    <div class="col-lg-6 aai_weight"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Religion:</div>   
                                    <div class="col-lg-6 aai_religion"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Civil Status:</div>   
                                    <div class="col-lg-6 aai_civil"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Place of Birth:</div>   
                                    <div class="col-lg-6 applicant_birthplace"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Present Address:</div>   
                                    <div class="col-lg-6 applicant_presentaddress"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Contact No.:</div>   
                                    <div class="col-lg-6 applicant_contact"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Permanent Address:</div>   
                                    <div class="col-lg-6 applicant_permanentaddress"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Contact No.:</div>   
                                    <div class="col-lg-6"><?= $getsingle1['sai_present_contact']; ?></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">E-mail Address:</div>   
                                    <div class="col-lg-6 applicant_email"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Program:</div>   
                                    <div class="col-lg-6 applicant_course_take"></div>   
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                <i class="fa fa-list-alt"></i> Requirements Submitted </h4> 
                        </div>
                        <div class="panel-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-4 bold">Student Type:</div>   
                                    <div class="col-lg-6">
                                        <?php
                                        $student_type = USERS::getAllStudentType();
                                        ?>
                                        <select name="student_type_ID" id="student_type_ID" class="form-control input-sm">
                                        <?php foreach($student_type as $key => $val){ ?>
                                            <option value="<?= $val['student_type_ID']; ?>"  <?php if($getsingle['student_type_ID']==$val['student_type_ID']){echo "selected";} ?>><?= $val['student_type_NAME']; ?></option>
                                        <?php } ?> 
                                        </select>
                                    </div>
                                    <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("#student_type_ID").change(function(){
                                            $( "#student_type_ID option:selected").each(function(){
                                                <?php foreach($student_type as $key => $val){ ?>
                                                    if($(this).attr("value")=="<?php print($val['student_type_ID']); ?>"){
                                                        <?php foreach($student_type as $key1 => $val1){ ?>   
                                                        <?php if($val['student_type_ID'] == $val1['student_type_ID'])
                                                        { ?>     
                                                        $("#student_type_<?php echo $val1['student_type_ID']; ?>").show(); 
                                                        $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',true);                      
                                                        $("form :input").attr('disabled',true); 
                                                        $("form #update").attr('disabled',false);                                                                                 
                                                        $("form #print").attr('disabled',false);                                                                                 
                                                        <?php }else{ ?> 
                                                        $("#student_type_<?php echo $val1['student_type_ID']; ?>").hide();
                                                        $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',true);
                                                        <?php } ?>  
                                                        <?php } ?>     
                                                    }
                                                <?php } ?>
                                            });
                                        }).change();
                                    });
                                    </script>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold text-right">Requirements</div>   
                                    <div class="col-lg-6 bold text-left">Status</div>
                                </div>
                                <br>
                                <?php
                                    $student_type = USERS::getAllStudentType(); ?>
                                    <?php foreach($student_type as $key => $val){ ?>
                                    <div id="student_type_<?= $val['student_type_ID']; ?>" style="/*display:none*/">
                                    <?php $requirements = USERS::getStudentTypeRequirements($val['student_type_ID']); ?>
                                    <?php foreach($requirements as $key1 => $val1){ ?>
                                    <?php
                                        $check =  USERS::checkReqByUser($val1['req_ID'],$siiid);
                                    ?>
                                        <div class="row">
                                        <div class="col-lg-4 text-right">
                                            <?= $val1['req_NAME']; ?>
                                        </div>
                                        <div class="col-lg-6">
                                            <select class="form-control input-sm" name="<?= $val1['req_ID']; ?>">
                                                <option value="0" <?php if($check['status']==0){echo "selected";} ?>>Not Passed</option>
                                                <option value="1" <?php if($check['status']==1){echo "selected";} ?>>Passed</option>
                                            </select>
                                            <br>
                                        </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                </div>
                                </div>
                                </div>
                            </div>
                            
                        </div>

                    </div> -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                <i class="fa fa-user-circle"></i> Parent/Guardian Information</h4> 
                        </div>
                        <div class="panel-body">
                            <div class="column">
                                <div class="row">
                                    <div class="col-lg-4 bold">Parent/Guardian Name:</div>   
                                    <div class="col-lg-6 parentguardian"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Occupation:</div>   
                                    <div class="col-lg-6 occupation"></div>   
                                </div><br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Contact No.:</div>   
                                    <div class="col-lg-6 parentguardiancontact"></div>   
                                </div><br>
                                <div class="row">
                                    <div class="col-lg-4 bold">Present Address:</div>   
                                    <div class="col-lg-6 parentguardianaddress"></div>   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                <i class="fa fa-graduation-cap"></i> Educational Background</h4> 
                        </div>
                        <div class="panel-body">
                            <div class="column">
                                <div class="row">
                                    <div class="col-lg-4 bold">Elementary:</div>   
                                    <div class="col-lg-6 elementary"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">High School:</div>   
                                    <div class="col-lg-6 highschool"></div>   
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-4 bold">College Last Attended:</div>   
                                    <div class="col-lg-6 college"></div>   
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                    
                </form>


                        
                         <?php         
                        }else{
                        
                        
                        AUDIT::ins($user['account_ID'],"Student Information","Visit Student Information Module");
                        
                         ?>
                    
                
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <table>
                                        <tr>
                                            <td width="50%">
                                                <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px"><i class="fa fa-user-circle"></i> List of Applicants <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
                                            </td>

                                            <td>
                                                <div class="input-group col-md-12 hide">
                                                    
                                                    <input id="searchItem" class="searchItem form-control"  type="text" autofocus="autofocus"  style="text-transform: uppercase;"/>
                                                    
                                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                </div>
                                            </td>
                                                <!-- <script>
                                                $(document).ready(function() {
                                                $('#searchItem').keyup(function(){
                                                var s_Item = $('#searchItem').val();
                                                    
                                                    if(s_Item.length >= 3)
                                                    {
                                                         $.ajax({
                                                            type: "POST",
                                                            url: "search.php?action=si.php",
                                                            data: 'search_term=' + s_Item,
                                                            success: function(msg){
                                                                /* $('#resultip').html(msg); */
                                                                    $("#display_result").show();
                                                                    $("#display_result").html(msg);
                                                                    
                                                                    $("#display_hide").hide();
                                                                
                                                            }
                                            
                                                        }); // Ajax Call
                                                    
                                                        //alert(s_Item);
                                                        }else{
                                                            $("#display_hide").show();
                                                            $("#display_result").hide();   
                                                        }
                                                    });
                                                });         
                                                </script> -->
                                                <script>
                                                $(document).ready(function() {


                                                    function postData(){
                                                        var s_Item = $('#searchItem').val();
                                                        if(s_Item.length >= 3)
                                                        {
                                                            console.log("Searching")
                                                            $.ajax({
                                                                    type: "POST",
                                                                    url: "search.php?action=si.php",
                                                                    data: 'search_term=' + s_Item,
                                                                    beforeSend: function ( xhr ) {
                                                                        $("#spinner").show();
                                                                       //Add your image loader here
                                                                    },
                                                                    success: function(msg){
                                                                        /* $('#resultip').html(msg); */
                                                                        $("#spinner").hide();
                                                                        $('#resultip').html(msg); 
                                                                        $("#display_result").show();
                                                                        $("#display_result").html(msg);
                                                                        $("#display_hide").hide();
                                                                        
                                                                    }
                                                    
                                                                })
                                                        }else{
                                                            $("#display_hide").show();
                                                            $("#display_result").hide();
                                                        }
                                                        return false;
                                                    }

                                                    $(function() {
                                                        var timer;
                                                        $("#searchItem").bind('keyup input',function() {
                                                            timer && clearTimeout(timer);
                                                            timer = setTimeout(postData, 300);
                                                        });
                                                    });

                                                });         
                                                </script>
                                        </tr>
                                    </table>
                                </div>

                                <div class="panel-body">
                                    <div id="display_result"></div>
                                    <div id="display_hide">
                                    <br><table border="0" cellspacing="5" cellpadding="5" style="width:40%;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                <input name='' id="myInput" placeholder="Search by Applicant No., First Name, Last Name or Email" class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table><br>
                                    <table id="cadetView" class="display nowrap table table-hover table-responsive table-striped text-md" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Applicant No</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Email</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody">
                                            
                                        </tbody>
                                        <!-- <tfoot>
                                            <tr>
                                                <th>Applicant No</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot> -->
                                    </table>
                                    <script type="text/javascript">
                                        var host = window.location.protocol+"//"+window.location.hostname ;
                                        $.ajax({
                                            method: "GET",
                                        //    url: "http://sms.massiveits.com/api/index.php/Magsaysay_controller/getApplicantList",
                                           url: host+"/api/index.php/Magsaysay_controller/getApplicantList",
                                            // url: "http://20.20.25.41/student-portal-backend/index.php/Magsaysay_controller/fetchallApplicantInfo",
                                             // url: "/api/index.php/Magsaysay_controller/fetchallApplicantInfo",
                                            dataType: "json",
                                        }).done(function (result, textStatus, jqXHR) {
                                            if(result.status === "SUCCESS"){
                                                var items = result.payload;
                                                var data = [];
                                                for (i = 0; i < items.length; i++) {
                                                    var trc = $('<tr class="applicationStatus_'+result.payload[i].application_status+'" data-status="'+result.payload[i].applicant_status+'">');
                                                    trc.append($('<td class="row1">')
                                                    .append(result.payload[i].applicant_number));
                                                    trc.append($('<td class="row2">')
                                                    .append(result.payload[i].applicant_fname));
                                                    trc.append($('<td class="row3">')
                                                    .append(result.payload[i].applicant_lname));
                                                    trc.append($('<td class="row4">')
                                                    .append(result.payload[i].applicant_email));
                                                    trc.append($('<td class="row5">')
                                                    .append($('<a href="list_of_applicants.php?action=view&id='+result.payload[i].applicant_id+'" class="btn P2C1_VIEWFULLINFORMATION P3C1_VIEWFULLINFORMATION btn-success btn-xs btn-block" class="btn btn-success btn-xs view" id="view" ' + ' data-aid="' + result.payload[i].applicant_id + '" >View Full Information</a>')))
                
                                                    $('#tbody').append(trc);
                                                }
                                                var dataRow = $("#cadetView ").DataTable({
                                                    'destroy': true,
                                                    "scrollY": true,
                                                    "bFilter": false,
                                                    "bLengthChange": true,
                                                    fnDrawCallback: function() { // insert events every draw
                                                    // $('.view').unbind('click').on('click', function() {
                                                    //     rowID = $(this).parent().parent()
                                                    //     aid = $(this).data('aid')
                                                    //     console.log('ID ' + '= ' + aid)
                                                    //     $.cookie("applicant_id", aid);
                                                    //     console.log($.cookie("applicant_id", aid))
                                                    //     setTimeout(function() {
                                                    //         window.location.href = "view_applicant_info"
                                                    //     }, 200);
                                                    // });
                                                    }
                                                });
                                                $(".applicationStatus_1").remove()
                                                $(".applicationStatus_2").remove()
                                                $(".applicationStatus_3").remove()
                                                $(".applicationStatus_4").remove()
                                                $(".applicationStatus_5").remove()
                                                $(".applicationStatus_6").remove()
                                                $(".applicationStatus_-1").remove()
                                                $(".applicationStatus_-2").remove()
                                                $(".applicationStatus_-3").remove()
                                                $(".applicationStatus_-4").remove()
                                                $(".applicationStatus_-5").remove()
                                                $(".applicationStatus_-6").remove()
                                                $(".applicationStatus_-7").remove()
                                            }else{
                                                
                                            }
                                            
                                        }).fail(function (jqXHR, textStatus, errorThrown,request) {
                                        });
                                        
                                    </script>
                                </div>
                            
                            </div>
                            
                            </div>
                </div>
            </div>
                 <script type="text/javascript">

             // $(window).ready(function() {
             //        setTimeout(function () {
             //            //console.log("CONVERT")
             //        table =  $('#cadetView').DataTable({
             //                "order": [],
             //                 "lengthChange": false,
             //                 /*"searching": false,*/
             //                 "info": true, 
             //                 "ordering": false,
             //           });
             //           $("#cadetView").show();
             //           $(".dataTables_filter").addClass("hide")

             //           $('#cadetView thead .th').each( function () {
             //                var title = $(this).text();
             //                if(title != "Action"){
             //                    $(this).html( '<input type="text" class="sb form-control" placeholder="'+title+'" />' );
             //                }
             //            } );

             //           // Apply the search
             //            table.columns().every( function () {
             //                var that = this;
             //            console.log(this.header());
             //                $( 'input', this.header() ).on( 'keyup change', function () {
             //                    if ( that.search() !== this.value ) {
             //                        that
             //                            .search( this.value )
             //                            .draw();
             //                    }
             //                } );
             //            } );

             //        }, 1);
             //    })
            </script>





                  <?php } break;
                    default: href("?action=view");
                    }}
                  ?>
            <!-- end row container -->
            <!-- footer-->      
        
            <?php include (LAYOUTS . "footer.php"); ?>

           
            <!-- end footer -->
        </div>
        <!-- end container -->

    </body>
<script type="text/javascript">
    function myFunction() {
        var input, filter, table, tr, td, i;
          input = document.getElementById("myInput");
          console.log(input);
          filter = input.value.toUpperCase();
          table = document.getElementById("cadetView");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td2 = tr[i].getElementsByTagName("td")[1];
            td1 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            td4 = tr[i].getElementsByTagName("td")[4];
            if (td) {
              if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1 || td4.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }   
        }
    }
</script>
</html>
