<?php
include('init.php');

$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Accounting / Billing and Collection, DESCRIPTION: User visited Billing and Collection";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
header('location: accounting.php?action=view');
?>