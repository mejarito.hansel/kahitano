
<?php
include('init.php');
$user_id = $user['account_ID'];
$action_event = "Clicked";
$event_desc = "MODULE: Student's Portal, DESCRIPTION: User visited Student's Portal";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
if(isset($_SERVER['HTTPS']) &&
    ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||
    isset($_SERVER['HTTP_X_FORWARDED_PROTO']) &&
    $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https') {
  $protocol = 'https://';
}
else {
  $protocol = 'http://';
}
$host= $_SERVER["HTTP_HOST"];
 header('location: '.$protocol.''.$host.'/student/');
?>