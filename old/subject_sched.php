<?php
    include('init.php');
    //SESSION::CheckLogin();
    //QUERIES
    if(Request::post()){
        switch(Request::get("action")){
            case "add": 
                # ROOM::addroom(Request::post());
                #var_dump(Request::post());
                SUBJSCHED::addsubjsched(Request::post());
                #KS::addsubjsched(Request::post());
                // echo "<script>alert('working')</script>";
            break;

            case "edit": 
                SUBJSCHED::updateSubjSched($_GET['id'],Request::post()); 
                HTML::redirect('old/subject_sched.php?action=add');
                #msgbox(1);
            break;
                #case "update_blog":  BLOG::update(Request::post("blog_info")); break;
                #case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
            default: Fail::not_found();
        }
    } 
    if(Request::get()){
        switch(Request::get("action")) {
            case "delete": ROOM::delete($_GET['id']); 
            break;
        }
    } 
    function getAllCurricullum($order=array()){
        $sql = " 
            SELECT * 
            FROM curricullum_list cl1 
            LEFT JOIN course_list cl2 on cl1.course_ID = cl2.course_ID 
        ";
        if(sizeof($order) > 0) {
            $sql .= " ORDER BY ";
        }
        $x=1;
        foreach($order as $key => $value){
            if($x == 1){
                $sql .= "{$key} {$value}";
            }else{
                $sql .= ", {$key} {$value}";
            }

            $x++;
        }
        return SQL::find_all($sql);
        #echo $sql;
    }
    function getCurricullum_Subjects($id,$order){
        $sql = "
            SELECT  *
            FROM curricullum_subjects cs , subject_list sl
            WHERE cs.curricullum_ID = $id and cs.subject_ID = sl.subject_ID
        ";

        if(sizeof($order) > 0){
            $sql .= " ORDER BY ";
        }

        $x=1;
        foreach($order as $key => $value){
            if($x == 1){
                $sql .= "{$key} {$value}";
            }else{
                $sql .= ", {$key} {$value}";
            }
            $x++;
        }
        return SQL::find_all($sql);
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
        <title>Student Information | ACADEMIC MANAGEMENT SYSTEM</title>
        <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
        <style>
            .alert-danger {
                color: #ffffff;
                background-color: #e24444;
                border-color: none;
            }
            .alert-danger .close{
                position: relative;
                top: -27px;
                left: 25px;
                padding: 5px 6px 5px 6px;
            }
            button.close {
                padding: 0;
                cursor: pointer;
                background: white;
                border: 0;
                padding: 5px;
                border-radius: 16px;
                box-shadow: 0px 2px 10px 0px #82828296;
                -webkit-appearance: none;
            }
            .close {
                float: right;
                font-size: 15px;
                font-weight: bold;
                line-height: 1;
                color: #e24444;
                text-shadow: 0;
                opacity: 1;
                filter: 0;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
            $('#example').DataTable({
                "scrollX": true,
                "order": [[ 0, "desc" ]]
                });
            });
        </script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <style>
            #test th, td { white-space: nowrap; }
            
            table.display.nowrap.schedule_list.dataTable.no-footer.DTFC_Cloned{
                margin-top: 0px;
                border: none !important;
            }
            #test div.dataTables_wrapper {
                width: 800px;
                margin-left: 0px;
                width: 100%;
                
            }
            div.has-error .select2-selection {
                border-color: rgb(185, 74, 72) !important;
            }
            .btn-validate {
                color: #ffffff;
                background-color: #28305a;
                border-color: #252a46;
            }
            .btn-verified {
                color: #ffffff !important;
                background-color: #216718 !important;
                border-color: #22462f !important;
            }
            .btn-unverified {
                color: #ffffff !important;
                background-color: #a70000 !important;
                border-color: #3c0505 !important;
            }
            div.has-success .select2-selection {
                border-color: rgb(21, 115, 37) !important;;
            }
            /* .section-form-validation > .select2.select2-container.select2-container--default{
                width: auto !important;
            } */
        </style>
    </head>

    <body>

    <?php include(LAYOUTS . 'top_nav.php'); ?>
        <div class="container">
      
      <!-- banner -->
      <?php include(LAYOUTS . "banner.php"); ?>
      <!-- end banner -->
      
          
            
            
      <!-- alert messages -->
      <?php SESSION::DisplayMsg(); ?>
    <div class="col-md-3">

      <?php include(PAGES."navigation.php");?>
      </div>
        
  <?php

switch($_GET['action']){

case "add":
case "view": ?>

<div class="col-md-9">

<!-- JRML: START -->

    <div class="container">
        <!-- ENCODING OF SUBJECT: START -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-list"></i>  Section Encoder
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <!-- CURRICULM: START -->
                                <div class="col-lg-12">
                                    <form action="<?php echo $_SERVER['REQUEST_URI']?>" method="get">
                                        <label for="Subject" class="">Curriculum</label>
                                        <?php $viewSubjects = getAllCurricullum(array("curricullum_NAME"=>"ASC")); ?>
                                        <select name="curr_ID" id="curr_ID" class="select2 form-control" onchange="this.form.submit()">
                                            <option value>None</option>
                                            <?php foreach($viewSubjects as $key => $val){ ?>
                                            <option <?php if(isset($_GET['curr_ID'])){ if($_GET['curr_ID'] == $val['curricullum_ID']){ echo "SELECTED=SELECTED"; } } ?>  value="<?php echo $val['curricullum_ID']; ?>"><?php echo $val['course_NAME']; ?> (<?php echo $val['course_INIT']; ?>) - <b><?php echo $val['curricullum_NAME']; ?> Curriculum</b> </option>
                                            <?php } ?>    
                                        </select>
                                        <input type="hidden" name="action" value="add">
                                    </form>
                                </div>
                                <!-- CURRICULM: END -->
                            </div>
                            <br>
                            <form action="<?php echo $_SERVER['REQUEST_URI']?>" method="post">

                                <!-- ENCODING OF CLASS SCHEDULE: START -->

                                    <!-- SECTION/SUBJECT: START -->
                                        <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="alert-secsub alert alert-danger alert-dismissible fade in" role="alert" style="border-radius:0px;">
                                                        <button type="button" class="close alert-closesecsub"><span aria-hidden="true">×</span></button>
                                                        <strong>Error: </strong>
                                                        <span class="error-message"></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-7">
                                                    <label class="">Section <span class="text-danger">*</span></label>
                                                    <?php $sem = SEMSECTION::getAll(array("sem_ID"=>"DESC","course_ID"=>"ASC","semester_section_NAME"=>"ASC")); ?>
                                                    <select name="semester_section_ID" id="semester_section_ID" class="select2 form-control">
                                                        <option value="NONE">None</option>
                                                        <?php foreach($sem as $key => $val){ ?>
                                                        <option value="<?php echo $val['semester_section_ID']; ?>"    <?php if(isset($_POST['semester_section_ID'])){ if($_POST['semester_section_ID'] == $val['semester_section_ID']){ echo "SELECTED=SELECTED"; } } ?>>
                                                        <?php $sem = SEMSECTION::getID($val['semester_section_ID']); ?>
                                                        <?php $d = SEMSECTION::getSEM($sem['sem_ID']);
                                                        echo $d['sem_NAME']; ?>
                                                        -
                                                        <?php $c = COURSE::getbyID($val['course_ID']);
                                                        echo $c['course_INIT']; ?>-<?php echo $val['semester_section_NAME']; ?>
                                                        </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="col-lg-5">
                                                    <label for="Subject" class="">Subject <span class="text-danger">*</span></label>
                                                    <?php
                                                        if(isset($_GET['curr_ID']) && $_GET['curr_ID'] != ""){
                                                            $viewSubjects = getCurricullum_Subjects($_GET['curr_ID'],array("cs_ID"=>"ASC"));
                                                        }else{
                                                            $viewSubjects = SUBJECTS:: getAllSubjects(array("subject_CODE"=>"ASC"));
                                                        }
                                                    ?>
                                                    <select name="subject_ID" id="subject_ID" class="select2 form-control">
                                                        <option value="NONE">None</option>
                                                        <?php foreach($viewSubjects as $key => $val){ ?>
                                                        <option  value="<?php echo $val['subject_ID']; ?>"><?php echo $val['subject_CODE']; ?> - <?php echo $val['subject_DESCRIPTION']; ?> - <?php echo $val['LEC_UNIT']."/".$val['LAB_UNIT']; ?></option>
                                                        <?php } ?>    
                                                    </select>
                                                </div>
                                        </div>
                                        <br>
                                    <!-- SECTION/SUBJECT: END -->

                                    <!-- SECTION DAY 1: START -->
                                        <div class="alert-day1 alert alert-danger alert-dismissible fade in" role="alert" style="border-radius:0px;">
                                            <button type="button" class="close alert-close1"><span aria-hidden="true">×</span></button>
                                            <strong>Error: </strong>
                                            <span></span><span></span><span></span><span></span>
                                        </div>
                                        <div class="section-validation row">
                                            <div class="error-hint col-lg-2">
                                                <label for="Day1" >Day 1</label>
                                                <select name="day" id="day" class="validate-day validate-day-1 select2 form-control">
                                                    <option value="">None</option>
                                                    <option value="TBA" <?php if(isset($_POST['day']) && $_POST['day'] == 'TBA' ){ echo "SELECTED=SELECTED"; } ?> >TBA</option>
                                                    <option value="Monday" <?php if(isset($_POST['day']) && $_POST['day'] == 'Monday' ){ echo "SELECTED=SELECTED"; } ?>>MON</option>
                                                    <option value="Tuesday" <?php if(isset($_POST['day']) && $_POST['day'] == 'Tuesday' ){ echo "SELECTED=SELECTED"; } ?>>TUE</option>
                                                    <option value="Wednesday" <?php if(isset($_POST['day']) && $_POST['day'] == 'Wednesday' ){ echo "SELECTED=SELECTED"; } ?>>WED</option>
                                                    <option value="Thursday" <?php if(isset($_POST['day']) && $_POST['day'] == 'Thursday' ){ echo "SELECTED=SELECTED"; } ?>>THU</option>
                                                    <option value="Friday" <?php if(isset($_POST['day']) && $_POST['day'] == 'Friday' ){ echo "SELECTED=SELECTED"; } ?>>FRI</option>
                                                    <option value="Saturday" <?php if(isset($_POST['day']) && $_POST['day'] == 'Saturday' ){ echo "SELECTED=SELECTED"; } ?>>SAT</option>
                                                    <?php foreach($viewSubjects as $key => $val){ ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-4">
                                                <label for="Day1" >Room Name</label>
                                                <?php $viewStudents = ROOM:: getallrooms(array("room_NAME"=>"DESC")); ?>
                                                <select name="room_ID" id="room_ID" class="validate-room-1 select2 form-control">
                                                    <option value="">None</option>
                                                    <?php foreach($viewStudents as $key => $val){ ?>
                                                    <option <?php if(isset($_POST['room_ID'])){ if($_POST['room_ID'] == $val['room_ID']){ echo "SELECTED=SELECTED"; } } ?> value="<?php echo $val['room_ID']; ?>"><?php echo $val['room_NAME']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-2">
                                                <label for="Start" >Start</label>
                                                <select name="start" id="start" class="start-time validate-start-1 form-control select2">
                                                    <option value="" data-mt="NONE">None</option>
                                                    <option value="TBA" data-mt="TBA">TBA</option>  
                                                    <option value="7:00 AM" data-mt="700">7:00 AM</option>
                                                    <option value="7:15 AM" data-mt="715">7:15 AM</option>
                                                    <option value="7:30 AM" data-mt="730">7:30 AM</option>
                                                    <option value="7:45 AM" data-mt="745">7:45 AM</option>
                                                    <option value="8:00 AM" data-mt="800">8:00 AM</option>
                                                    <option value="8:15 AM" data-mt="815">8:15 AM</option>
                                                    <option value="8:30 AM" data-mt="830">8:30 AM</option>
                                                    <option value="8:45 AM" data-mt="845">8:45 AM</option>
                                                    <option value="9:00 AM" data-mt="900">9:00 AM</option>
                                                    <option value="9:15 AM" data-mt="915">9:15 AM</option>
                                                    <option value="9:30 AM" data-mt="930">9:30 AM</option>
                                                    <option value="9:45 AM" data-mt="945">9:45 AM</option>
                                                    <option value="10:00 AM" data-mt="1000">10:00 AM</option>
                                                    <option value="10:15 AM" data-mt="1015">10:15 AM</option>
                                                    <option value="10:30 AM" data-mt="1030">10:30 AM</option>
                                                    <option value="10:45 AM" data-mt="1045">10:45 AM</option>
                                                    <option value="11:00 AM" data-mt="1100">11:00 AM</option>
                                                    <option value="11:15 AM" data-mt="1115">11:15 AM</option>
                                                    <option value="11:30 AM" data-mt="1130">11:30 AM</option>
                                                    <option value="11:45 AM" data-mt="1145">11:45 AM</option>
                                                    <option value="12:00 PM" data-mt="1200">12:00 PM</option>
                                                    <option value="12:15 PM" data-mt="1215">12:15 PM</option>
                                                    <option value="12:30 PM" data-mt="1230">12:30 PM</option>
                                                    <option value="12:45 PM" data-mt="1245">12:45 PM</option>
                                                    <option value="1:00 PM" data-mt="1300">1:00 PM</option>
                                                    <option value="1:15 PM" data-mt="1315">1:15 PM</option>
                                                    <option value="1:30 PM" data-mt="1330">1:30 PM</option>
                                                    <option value="1:45 PM" data-mt="1345">1:45 PM</option>
                                                    <option value="2:00 PM" data-mt="1400">2:00 PM</option>
                                                    <option value="2:15 PM" data-mt="1415">2:15 PM</option>
                                                    <option value="2:30 PM" data-mt="1430">2:30 PM</option>
                                                    <option value="2:45 PM" data-mt="1445">2:45 PM</option>
                                                    <option value="3:00 PM" data-mt="1500">3:00 PM</option>
                                                    <option value="3:15 PM" data-mt="1515">3:15 PM</option>
                                                    <option value="3:30 PM" data-mt="1530">3:30 PM</option>
                                                    <option value="3:45 PM" data-mt="1545">3:45 PM</option>
                                                    <option value="4:00 PM" data-mt="1600">4:00 PM</option>
                                                    <option value="4:15 PM" data-mt="1615">4:15 PM</option>
                                                    <option value="4:30 PM" data-mt="1630">4:30 PM</option>
                                                    <option value="4:45 PM" data-mt="1645">4:45 PM</option>
                                                    <option value="5:00 PM" data-mt="1700">5:00 PM</option>
                                                    <option value="5:15 PM" data-mt="1715">5:15 PM</option>
                                                    <option value="5:30 PM" data-mt="1730">5:30 PM</option>
                                                    <option value="5:45 PM" data-mt="1745">5:45 PM</option>
                                                    <option value="6:00 PM" data-mt="1800">6:00 PM</option>
                                                    <option value="6:15 PM" data-mt="1815">6:15 PM</option>
                                                    <option value="6:30 PM" data-mt="1830">6:30 PM</option>
                                                    <option value="6:45 PM" data-mt="1845">6:45 PM</option>
                                                    <option value="7:00 PM" data-mt="1900">7:00 PM</option>
                                                    <option value="7:15 PM" data-mt="1915">7:15 PM</option>
                                                    <option value="7:30 PM" data-mt="1930">7:30 PM</option>
                                                    <option value="7:45 PM" data-mt="1945">7:45 PM</option>
                                                    <option value="8:00 PM" data-mt="2000">8:00 PM</option>
                                                    <option value="8:15 PM" data-mt="2015">8:15 PM</option>
                                                    <option value="8:30 PM" data-mt="2030">8:30 PM</option>
                                                    <option value="8:45 PM" data-mt="2045">8:45 PM</option>
                                                    <option value="9:00 PM" data-mt="2100">9:00 PM</option>
                                                    <option value="9:15 PM" data-mt="2115">9:15 PM</option>
                                                    <option value="9:30 PM" data-mt="2130">9:30 PM</option>
                                                    <option value="9:45 PM" data-mt="2145">9:45 PM</option>
                                                    <option value="10:00 PM" data-mt="2200">10:00 PM</option>
                                                    <option value="10:15 PM" data-mt="2215">10:15 PM</option>
                                                    <option value="10:30 PM" data-mt="2230">10:30 PM</option>
                                                    <option value="10:45 PM" data-mt="2245">10:45 PM</option>
                                                    <option value="11:00 PM" data-mt="2300">11:00 PM</option>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-2">
                                                <label for="End" >End</label>
                                                <select name="end" id="end" class="validate-end-1 form-control select2">
                                                    <option value="" data-mt="NONE">None</option>
                                                    <option value="TBA" data-mt="TBA">TBA</option>  
                                                    <option value="7:00 AM" data-mt="700">7:00 AM</option>
                                                    <option value="7:15 AM" data-mt="715">7:15 AM</option>
                                                    <option value="7:30 AM" data-mt="730">7:30 AM</option>
                                                    <option value="7:45 AM" data-mt="745">7:45 AM</option>
                                                    <option value="8:00 AM" data-mt="800">8:00 AM</option>
                                                    <option value="8:15 AM" data-mt="815">8:15 AM</option>
                                                    <option value="8:30 AM" data-mt="830">8:30 AM</option>
                                                    <option value="8:45 AM" data-mt="845">8:45 AM</option>
                                                    <option value="9:00 AM" data-mt="900">9:00 AM</option>
                                                    <option value="9:15 AM" data-mt="915">9:15 AM</option>
                                                    <option value="9:30 AM" data-mt="930">9:30 AM</option>
                                                    <option value="9:45 AM" data-mt="945">9:45 AM</option>
                                                    <option value="10:00 AM" data-mt="1000">10:00 AM</option>
                                                    <option value="10:15 AM" data-mt="1015">10:15 AM</option>
                                                    <option value="10:30 AM" data-mt="1030">10:30 AM</option>
                                                    <option value="10:45 AM" data-mt="1045">10:45 AM</option>
                                                    <option value="11:00 AM" data-mt="1100">11:00 AM</option>
                                                    <option value="11:15 AM" data-mt="1115">11:15 AM</option>
                                                    <option value="11:30 AM" data-mt="1130">11:30 AM</option>
                                                    <option value="11:45 AM" data-mt="1145">11:45 AM</option>
                                                    <option value="12:00 PM" data-mt="1200">12:00 PM</option>
                                                    <option value="12:15 PM" data-mt="1215">12:15 PM</option>
                                                    <option value="12:30 PM" data-mt="1230">12:30 PM</option>
                                                    <option value="12:45 PM" data-mt="1245">12:45 PM</option>
                                                    <option value="1:00 PM" data-mt="1300">1:00 PM</option>
                                                    <option value="1:15 PM" data-mt="1315">1:15 PM</option>
                                                    <option value="1:30 PM" data-mt="1330">1:30 PM</option>
                                                    <option value="1:45 PM" data-mt="1345">1:45 PM</option>
                                                    <option value="2:00 PM" data-mt="1400">2:00 PM</option>
                                                    <option value="2:15 PM" data-mt="1415">2:15 PM</option>
                                                    <option value="2:30 PM" data-mt="1430">2:30 PM</option>
                                                    <option value="2:45 PM" data-mt="1445">2:45 PM</option>
                                                    <option value="3:00 PM" data-mt="1500">3:00 PM</option>
                                                    <option value="3:15 PM" data-mt="1515">3:15 PM</option>
                                                    <option value="3:30 PM" data-mt="1530">3:30 PM</option>
                                                    <option value="3:45 PM" data-mt="1545">3:45 PM</option>
                                                    <option value="4:00 PM" data-mt="1600">4:00 PM</option>
                                                    <option value="4:15 PM" data-mt="1615">4:15 PM</option>
                                                    <option value="4:30 PM" data-mt="1630">4:30 PM</option>
                                                    <option value="4:45 PM" data-mt="1645">4:45 PM</option>
                                                    <option value="5:00 PM" data-mt="1700">5:00 PM</option>
                                                    <option value="5:15 PM" data-mt="1715">5:15 PM</option>
                                                    <option value="5:30 PM" data-mt="1730">5:30 PM</option>
                                                    <option value="5:45 PM" data-mt="1745">5:45 PM</option>
                                                    <option value="6:00 PM" data-mt="1800">6:00 PM</option>
                                                    <option value="6:15 PM" data-mt="1815">6:15 PM</option>
                                                    <option value="6:30 PM" data-mt="1830">6:30 PM</option>
                                                    <option value="6:45 PM" data-mt="1845">6:45 PM</option>
                                                    <option value="7:00 PM" data-mt="1900">7:00 PM</option>
                                                    <option value="7:15 PM" data-mt="1915">7:15 PM</option>
                                                    <option value="7:30 PM" data-mt="1930">7:30 PM</option>
                                                    <option value="7:45 PM" data-mt="1945">7:45 PM</option>
                                                    <option value="8:00 PM" data-mt="2000">8:00 PM</option>
                                                    <option value="8:15 PM" data-mt="2015">8:15 PM</option>
                                                    <option value="8:30 PM" data-mt="2030">8:30 PM</option>
                                                    <option value="8:45 PM" data-mt="2045">8:45 PM</option>
                                                    <option value="9:00 PM" data-mt="2100">9:00 PM</option>
                                                    <option value="9:15 PM" data-mt="2115">9:15 PM</option>
                                                    <option value="9:30 PM" data-mt="2130">9:30 PM</option>
                                                    <option value="9:45 PM" data-mt="2145">9:45 PM</option>
                                                    <option value="10:00 PM" data-mt="2200">10:00 PM</option>
                                                    <option value="10:15 PM" data-mt="2215">10:15 PM</option>
                                                    <option value="10:30 PM" data-mt="2230">10:30 PM</option>
                                                    <option value="10:45 PM" data-mt="2245">10:45 PM</option>
                                                    <option value="11:00 PM" data-mt="2300">11:00 PM</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <a class="btn btn-verify btn-validate btn-sm btn-block" style="margin-top: 25px; padding: 5px 0px 5px 0px;" data-status="false"><small>validate</small></a>
                                            </div>
                                        </div>
                                        <a class="btn btn-block btn-default btn-sm btn-addmoreday1" style="margin-top:2rem;">ADD MORE DAY</a>
                                        <br>
                                    <!-- SECTION DAY 1: END -->

                                    <!-- SECTION DAY 2: START -->
                                        <div class="alert-day2 alert alert-danger alert-dismissible fade in" role="alert" style="border-radius:0px;">
                                            <button type="button" class="close alert-close2"><span aria-hidden="true">×</span></button>
                                            <strong>Error: </strong>
                                            <span></span><span></span><span></span><span></span>
                                        </div>
                                        <div class="section-validation row">
                                            <div class="error-hint col-lg-2">
                                                <label for="Day2" >Day 2</label>
                                                <select name="day2" id="day2" class="validate-day validate-day-2 select2 form-control">
                                                    <option value="">None</option>
                                                    <option value="TBA">TBA</option>
                                                    <option value="Monday">MON</option>
                                                    <option value="Tuesday">TUE</option>
                                                    <option value="Wednesday">WED</option>
                                                    <option value="Thursday">THU</option>
                                                    <option value="Friday">FRI</option>
                                                    <option value="Saturday">SAT</option>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-4">
                                                <label for="Day1" >Room Name</label>
                                                <?php $viewStudents = ROOM:: getallrooms(array("room_NAME"=>"ASC")); ?>
                                                <select name="room_ID2" id="room_ID2" class="validate-room-2 form-control select2">
                                                    <option value="">None</option>
                                                    <?php foreach($viewStudents as $key => $val){ ?>
                                                    <option value="<?php echo $val['room_ID']; ?>"><?php echo $val['room_NAME']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-2">
                                                <label for="Start2">Start</label>
                                                <select name="start2" id="start2" class="start-time validate-start-2 form-control select2">
                                                    <option value="" data-mt="NONE">None</option>
                                                    <option value="TBA" data-mt="TBA">TBA</option>  
                                                    <option value="7:00 AM" data-mt="700">7:00 AM</option>
                                                    <option value="7:15 AM" data-mt="715">7:15 AM</option>
                                                    <option value="7:30 AM" data-mt="730">7:30 AM</option>
                                                    <option value="7:45 AM" data-mt="745">7:45 AM</option>
                                                    <option value="8:00 AM" data-mt="800">8:00 AM</option>
                                                    <option value="8:15 AM" data-mt="815">8:15 AM</option>
                                                    <option value="8:30 AM" data-mt="830">8:30 AM</option>
                                                    <option value="8:45 AM" data-mt="845">8:45 AM</option>
                                                    <option value="9:00 AM" data-mt="900">9:00 AM</option>
                                                    <option value="9:15 AM" data-mt="915">9:15 AM</option>
                                                    <option value="9:30 AM" data-mt="930">9:30 AM</option>
                                                    <option value="9:45 AM" data-mt="945">9:45 AM</option>
                                                    <option value="10:00 AM" data-mt="1000">10:00 AM</option>
                                                    <option value="10:15 AM" data-mt="1015">10:15 AM</option>
                                                    <option value="10:30 AM" data-mt="1030">10:30 AM</option>
                                                    <option value="10:45 AM" data-mt="1045">10:45 AM</option>
                                                    <option value="11:00 AM" data-mt="1100">11:00 AM</option>
                                                    <option value="11:15 AM" data-mt="1115">11:15 AM</option>
                                                    <option value="11:30 AM" data-mt="1130">11:30 AM</option>
                                                    <option value="11:45 AM" data-mt="1145">11:45 AM</option>
                                                    <option value="12:00 PM" data-mt="1200">12:00 PM</option>
                                                    <option value="12:15 PM" data-mt="1215">12:15 PM</option>
                                                    <option value="12:30 PM" data-mt="1230">12:30 PM</option>
                                                    <option value="12:45 PM" data-mt="1245">12:45 PM</option>
                                                    <option value="1:00 PM" data-mt="1300">1:00 PM</option>
                                                    <option value="1:15 PM" data-mt="1315">1:15 PM</option>
                                                    <option value="1:30 PM" data-mt="1330">1:30 PM</option>
                                                    <option value="1:45 PM" data-mt="1345">1:45 PM</option>
                                                    <option value="2:00 PM" data-mt="1400">2:00 PM</option>
                                                    <option value="2:15 PM" data-mt="1415">2:15 PM</option>
                                                    <option value="2:30 PM" data-mt="1430">2:30 PM</option>
                                                    <option value="2:45 PM" data-mt="1445">2:45 PM</option>
                                                    <option value="3:00 PM" data-mt="1500">3:00 PM</option>
                                                    <option value="3:15 PM" data-mt="1515">3:15 PM</option>
                                                    <option value="3:30 PM" data-mt="1530">3:30 PM</option>
                                                    <option value="3:45 PM" data-mt="1545">3:45 PM</option>
                                                    <option value="4:00 PM" data-mt="1600">4:00 PM</option>
                                                    <option value="4:15 PM" data-mt="1615">4:15 PM</option>
                                                    <option value="4:30 PM" data-mt="1630">4:30 PM</option>
                                                    <option value="4:45 PM" data-mt="1645">4:45 PM</option>
                                                    <option value="5:00 PM" data-mt="1700">5:00 PM</option>
                                                    <option value="5:15 PM" data-mt="1715">5:15 PM</option>
                                                    <option value="5:30 PM" data-mt="1730">5:30 PM</option>
                                                    <option value="5:45 PM" data-mt="1745">5:45 PM</option>
                                                    <option value="6:00 PM" data-mt="1800">6:00 PM</option>
                                                    <option value="6:15 PM" data-mt="1815">6:15 PM</option>
                                                    <option value="6:30 PM" data-mt="1830">6:30 PM</option>
                                                    <option value="6:45 PM" data-mt="1845">6:45 PM</option>
                                                    <option value="7:00 PM" data-mt="1900">7:00 PM</option>
                                                    <option value="7:15 PM" data-mt="1915">7:15 PM</option>
                                                    <option value="7:30 PM" data-mt="1930">7:30 PM</option>
                                                    <option value="7:45 PM" data-mt="1945">7:45 PM</option>
                                                    <option value="8:00 PM" data-mt="2000">8:00 PM</option>
                                                    <option value="8:15 PM" data-mt="2015">8:15 PM</option>
                                                    <option value="8:30 PM" data-mt="2030">8:30 PM</option>
                                                    <option value="8:45 PM" data-mt="2045">8:45 PM</option>
                                                    <option value="9:00 PM" data-mt="2100">9:00 PM</option>
                                                    <option value="9:15 PM" data-mt="2115">9:15 PM</option>
                                                    <option value="9:30 PM" data-mt="2130">9:30 PM</option>
                                                    <option value="9:45 PM" data-mt="2145">9:45 PM</option>
                                                    <option value="10:00 PM" data-mt="2200">10:00 PM</option>
                                                    <option value="10:15 PM" data-mt="2215">10:15 PM</option>
                                                    <option value="10:30 PM" data-mt="2230">10:30 PM</option>
                                                    <option value="10:45 PM" data-mt="2245">10:45 PM</option>
                                                    <option value="11:00 PM" data-mt="2300">11:00 PM</option>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-2">
                                                <label for="End2">End</label>
                                                <select name="end2" id="end2" class="validate-end-2 form-control select2">
                                                    <option value="" data-mt="NONE">None</option>
                                                    <option value="TBA" data-mt="TBA">TBA</option>  
                                                    <option value="7:00 AM" data-mt="700">7:00 AM</option>
                                                    <option value="7:15 AM" data-mt="715">7:15 AM</option>
                                                    <option value="7:30 AM" data-mt="730">7:30 AM</option>
                                                    <option value="7:45 AM" data-mt="745">7:45 AM</option>
                                                    <option value="8:00 AM" data-mt="800">8:00 AM</option>
                                                    <option value="8:15 AM" data-mt="815">8:15 AM</option>
                                                    <option value="8:30 AM" data-mt="830">8:30 AM</option>
                                                    <option value="8:45 AM" data-mt="845">8:45 AM</option>
                                                    <option value="9:00 AM" data-mt="900">9:00 AM</option>
                                                    <option value="9:15 AM" data-mt="915">9:15 AM</option>
                                                    <option value="9:30 AM" data-mt="930">9:30 AM</option>
                                                    <option value="9:45 AM" data-mt="945">9:45 AM</option>
                                                    <option value="10:00 AM" data-mt="1000">10:00 AM</option>
                                                    <option value="10:15 AM" data-mt="1015">10:15 AM</option>
                                                    <option value="10:30 AM" data-mt="1030">10:30 AM</option>
                                                    <option value="10:45 AM" data-mt="1045">10:45 AM</option>
                                                    <option value="11:00 AM" data-mt="1100">11:00 AM</option>
                                                    <option value="11:15 AM" data-mt="1115">11:15 AM</option>
                                                    <option value="11:30 AM" data-mt="1130">11:30 AM</option>
                                                    <option value="11:45 AM" data-mt="1145">11:45 AM</option>
                                                    <option value="12:00 PM" data-mt="1200">12:00 PM</option>
                                                    <option value="12:15 PM" data-mt="1215">12:15 PM</option>
                                                    <option value="12:30 PM" data-mt="1230">12:30 PM</option>
                                                    <option value="12:45 PM" data-mt="1245">12:45 PM</option>
                                                    <option value="1:00 PM" data-mt="1300">1:00 PM</option>
                                                    <option value="1:15 PM" data-mt="1315">1:15 PM</option>
                                                    <option value="1:30 PM" data-mt="1330">1:30 PM</option>
                                                    <option value="1:45 PM" data-mt="1345">1:45 PM</option>
                                                    <option value="2:00 PM" data-mt="1400">2:00 PM</option>
                                                    <option value="2:15 PM" data-mt="1415">2:15 PM</option>
                                                    <option value="2:30 PM" data-mt="1430">2:30 PM</option>
                                                    <option value="2:45 PM" data-mt="1445">2:45 PM</option>
                                                    <option value="3:00 PM" data-mt="1500">3:00 PM</option>
                                                    <option value="3:15 PM" data-mt="1515">3:15 PM</option>
                                                    <option value="3:30 PM" data-mt="1530">3:30 PM</option>
                                                    <option value="3:45 PM" data-mt="1545">3:45 PM</option>
                                                    <option value="4:00 PM" data-mt="1600">4:00 PM</option>
                                                    <option value="4:15 PM" data-mt="1615">4:15 PM</option>
                                                    <option value="4:30 PM" data-mt="1630">4:30 PM</option>
                                                    <option value="4:45 PM" data-mt="1645">4:45 PM</option>
                                                    <option value="5:00 PM" data-mt="1700">5:00 PM</option>
                                                    <option value="5:15 PM" data-mt="1715">5:15 PM</option>
                                                    <option value="5:30 PM" data-mt="1730">5:30 PM</option>
                                                    <option value="5:45 PM" data-mt="1745">5:45 PM</option>
                                                    <option value="6:00 PM" data-mt="1800">6:00 PM</option>
                                                    <option value="6:15 PM" data-mt="1815">6:15 PM</option>
                                                    <option value="6:30 PM" data-mt="1830">6:30 PM</option>
                                                    <option value="6:45 PM" data-mt="1845">6:45 PM</option>
                                                    <option value="7:00 PM" data-mt="1900">7:00 PM</option>
                                                    <option value="7:15 PM" data-mt="1915">7:15 PM</option>
                                                    <option value="7:30 PM" data-mt="1930">7:30 PM</option>
                                                    <option value="7:45 PM" data-mt="1945">7:45 PM</option>
                                                    <option value="8:00 PM" data-mt="2000">8:00 PM</option>
                                                    <option value="8:15 PM" data-mt="2015">8:15 PM</option>
                                                    <option value="8:30 PM" data-mt="2030">8:30 PM</option>
                                                    <option value="8:45 PM" data-mt="2045">8:45 PM</option>
                                                    <option value="9:00 PM" data-mt="2100">9:00 PM</option>
                                                    <option value="9:15 PM" data-mt="2115">9:15 PM</option>
                                                    <option value="9:30 PM" data-mt="2130">9:30 PM</option>
                                                    <option value="9:45 PM" data-mt="2145">9:45 PM</option>
                                                    <option value="10:00 PM" data-mt="2200">10:00 PM</option>
                                                    <option value="10:15 PM" data-mt="2215">10:15 PM</option>
                                                    <option value="10:30 PM" data-mt="2230">10:30 PM</option>
                                                    <option value="10:45 PM" data-mt="2245">10:45 PM</option>
                                                    <option value="11:00 PM" data-mt="2300">11:00 PM</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <a class="btn btn-verify btn-validate btn-sm btn-block" style="margin-top: 25px; padding: 5px 0px 5px 0px;"><small>verify</small></a>
                                            </div>
                                        </div>
                                        <a class="btn btn-block btn-default btn-sm btn-addmoreday2" style="margin-top:2rem;">ADD MORE DAY</a>
                                        <br>
                                    <!-- SECTION DAY 2: END -->
                                    
                                    <!-- SECTION DAY 3: START -->
                                        <div class="alert-day3 alert alert-danger alert-dismissible fade in" role="alert" style="border-radius:0px;">
                                            <button type="button" class="close alert-close3"><span aria-hidden="true">×</span></button>
                                            <strong>Error: </strong>
                                            <span></span><span></span><span></span><span></span>
                                        </div>
                                        <div class="section-validation row">
                                            <div class="error-hint col-lg-2">
                                                <label for="Day3" >Day 3</label>
                                                <select name="day3" id="day3" class="validate-day validate-day-3 select2 form-control">
                                                    <option value="">None</option>
                                                    <option value="TBA">TBA</option>
                                                    <option value="Monday">MON</option>
                                                    <option value="Tuesday">TUE</option>
                                                    <option value="Wednesday">WED</option>
                                                    <option value="Thursday">THU</option>
                                                    <option value="Friday">FRI</option>
                                                    <option value="Saturday">SAT</option>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-4">
                                                <label for="Room3" >Room Name</label>
                                                <?php $viewStudents = ROOM:: getallrooms(array("room_NAME"=>"ASC")); ?>
                                                <select name="room_ID3" id="room_ID3" class="validate-room-3 form-control select2">
                                                    <option value="">None</option>
                                                    <?php foreach($viewStudents as $key => $val){ ?>
                                                    <option value="<?php echo $val['room_ID']; ?>"><?php echo $val['room_NAME']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-2">
                                                <label for="Start3">Start</label>
                                                <select name="start3" id="start3" class="start-time validate-start-3 form-control select2">
                                                    <option value="" data-mt="NONE">None</option>
                                                    <option value="TBA" data-mt="TBA">TBA</option>  
                                                    <option value="7:00 AM" data-mt="700">7:00 AM</option>
                                                    <option value="7:15 AM" data-mt="715">7:15 AM</option>
                                                    <option value="7:30 AM" data-mt="730">7:30 AM</option>
                                                    <option value="7:45 AM" data-mt="745">7:45 AM</option>
                                                    <option value="8:00 AM" data-mt="800">8:00 AM</option>
                                                    <option value="8:15 AM" data-mt="815">8:15 AM</option>
                                                    <option value="8:30 AM" data-mt="830">8:30 AM</option>
                                                    <option value="8:45 AM" data-mt="845">8:45 AM</option>
                                                    <option value="9:00 AM" data-mt="900">9:00 AM</option>
                                                    <option value="9:15 AM" data-mt="915">9:15 AM</option>
                                                    <option value="9:30 AM" data-mt="930">9:30 AM</option>
                                                    <option value="9:45 AM" data-mt="945">9:45 AM</option>
                                                    <option value="10:00 AM" data-mt="1000">10:00 AM</option>
                                                    <option value="10:15 AM" data-mt="1015">10:15 AM</option>
                                                    <option value="10:30 AM" data-mt="1030">10:30 AM</option>
                                                    <option value="10:45 AM" data-mt="1045">10:45 AM</option>
                                                    <option value="11:00 AM" data-mt="1100">11:00 AM</option>
                                                    <option value="11:15 AM" data-mt="1115">11:15 AM</option>
                                                    <option value="11:30 AM" data-mt="1130">11:30 AM</option>
                                                    <option value="11:45 AM" data-mt="1145">11:45 AM</option>
                                                    <option value="12:00 PM" data-mt="1200">12:00 PM</option>
                                                    <option value="12:15 PM" data-mt="1215">12:15 PM</option>
                                                    <option value="12:30 PM" data-mt="1230">12:30 PM</option>
                                                    <option value="12:45 PM" data-mt="1245">12:45 PM</option>
                                                    <option value="1:00 PM" data-mt="1300">1:00 PM</option>
                                                    <option value="1:15 PM" data-mt="1315">1:15 PM</option>
                                                    <option value="1:30 PM" data-mt="1330">1:30 PM</option>
                                                    <option value="1:45 PM" data-mt="1345">1:45 PM</option>
                                                    <option value="2:00 PM" data-mt="1400">2:00 PM</option>
                                                    <option value="2:15 PM" data-mt="1415">2:15 PM</option>
                                                    <option value="2:30 PM" data-mt="1430">2:30 PM</option>
                                                    <option value="2:45 PM" data-mt="1445">2:45 PM</option>
                                                    <option value="3:00 PM" data-mt="1500">3:00 PM</option>
                                                    <option value="3:15 PM" data-mt="1515">3:15 PM</option>
                                                    <option value="3:30 PM" data-mt="1530">3:30 PM</option>
                                                    <option value="3:45 PM" data-mt="1545">3:45 PM</option>
                                                    <option value="4:00 PM" data-mt="1600">4:00 PM</option>
                                                    <option value="4:15 PM" data-mt="1615">4:15 PM</option>
                                                    <option value="4:30 PM" data-mt="1630">4:30 PM</option>
                                                    <option value="4:45 PM" data-mt="1645">4:45 PM</option>
                                                    <option value="5:00 PM" data-mt="1700">5:00 PM</option>
                                                    <option value="5:15 PM" data-mt="1715">5:15 PM</option>
                                                    <option value="5:30 PM" data-mt="1730">5:30 PM</option>
                                                    <option value="5:45 PM" data-mt="1745">5:45 PM</option>
                                                    <option value="6:00 PM" data-mt="1800">6:00 PM</option>
                                                    <option value="6:15 PM" data-mt="1815">6:15 PM</option>
                                                    <option value="6:30 PM" data-mt="1830">6:30 PM</option>
                                                    <option value="6:45 PM" data-mt="1845">6:45 PM</option>
                                                    <option value="7:00 PM" data-mt="1900">7:00 PM</option>
                                                    <option value="7:15 PM" data-mt="1915">7:15 PM</option>
                                                    <option value="7:30 PM" data-mt="1930">7:30 PM</option>
                                                    <option value="7:45 PM" data-mt="1945">7:45 PM</option>
                                                    <option value="8:00 PM" data-mt="2000">8:00 PM</option>
                                                    <option value="8:15 PM" data-mt="2015">8:15 PM</option>
                                                    <option value="8:30 PM" data-mt="2030">8:30 PM</option>
                                                    <option value="8:45 PM" data-mt="2045">8:45 PM</option>
                                                    <option value="9:00 PM" data-mt="2100">9:00 PM</option>
                                                    <option value="9:15 PM" data-mt="2115">9:15 PM</option>
                                                    <option value="9:30 PM" data-mt="2130">9:30 PM</option>
                                                    <option value="9:45 PM" data-mt="2145">9:45 PM</option>
                                                    <option value="10:00 PM" data-mt="2200">10:00 PM</option>
                                                    <option value="10:15 PM" data-mt="2215">10:15 PM</option>
                                                    <option value="10:30 PM" data-mt="2230">10:30 PM</option>
                                                    <option value="10:45 PM" data-mt="2245">10:45 PM</option>
                                                    <option value="11:00 PM" data-mt="2300">11:00 PM</option>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-2">
                                                <label for="End3">End</label>
                                                <select name="end3" id="end3" class="validate-end-3 form-control select2">
                                                    <option value="" data-mt="NONE">None</option>
                                                    <option value="TBA" data-mt="TBA">TBA</option>  
                                                    <option value="7:00 AM" data-mt="700">7:00 AM</option>
                                                    <option value="7:15 AM" data-mt="715">7:15 AM</option>
                                                    <option value="7:30 AM" data-mt="730">7:30 AM</option>
                                                    <option value="7:45 AM" data-mt="745">7:45 AM</option>
                                                    <option value="8:00 AM" data-mt="800">8:00 AM</option>
                                                    <option value="8:15 AM" data-mt="815">8:15 AM</option>
                                                    <option value="8:30 AM" data-mt="830">8:30 AM</option>
                                                    <option value="8:45 AM" data-mt="845">8:45 AM</option>
                                                    <option value="9:00 AM" data-mt="900">9:00 AM</option>
                                                    <option value="9:15 AM" data-mt="915">9:15 AM</option>
                                                    <option value="9:30 AM" data-mt="930">9:30 AM</option>
                                                    <option value="9:45 AM" data-mt="945">9:45 AM</option>
                                                    <option value="10:00 AM" data-mt="1000">10:00 AM</option>
                                                    <option value="10:15 AM" data-mt="1015">10:15 AM</option>
                                                    <option value="10:30 AM" data-mt="1030">10:30 AM</option>
                                                    <option value="10:45 AM" data-mt="1045">10:45 AM</option>
                                                    <option value="11:00 AM" data-mt="1100">11:00 AM</option>
                                                    <option value="11:15 AM" data-mt="1115">11:15 AM</option>
                                                    <option value="11:30 AM" data-mt="1130">11:30 AM</option>
                                                    <option value="11:45 AM" data-mt="1145">11:45 AM</option>
                                                    <option value="12:00 PM" data-mt="1200">12:00 PM</option>
                                                    <option value="12:15 PM" data-mt="1215">12:15 PM</option>
                                                    <option value="12:30 PM" data-mt="1230">12:30 PM</option>
                                                    <option value="12:45 PM" data-mt="1245">12:45 PM</option>
                                                    <option value="1:00 PM" data-mt="1300">1:00 PM</option>
                                                    <option value="1:15 PM" data-mt="1315">1:15 PM</option>
                                                    <option value="1:30 PM" data-mt="1330">1:30 PM</option>
                                                    <option value="1:45 PM" data-mt="1345">1:45 PM</option>
                                                    <option value="2:00 PM" data-mt="1400">2:00 PM</option>
                                                    <option value="2:15 PM" data-mt="1415">2:15 PM</option>
                                                    <option value="2:30 PM" data-mt="1430">2:30 PM</option>
                                                    <option value="2:45 PM" data-mt="1445">2:45 PM</option>
                                                    <option value="3:00 PM" data-mt="1500">3:00 PM</option>
                                                    <option value="3:15 PM" data-mt="1515">3:15 PM</option>
                                                    <option value="3:30 PM" data-mt="1530">3:30 PM</option>
                                                    <option value="3:45 PM" data-mt="1545">3:45 PM</option>
                                                    <option value="4:00 PM" data-mt="1600">4:00 PM</option>
                                                    <option value="4:15 PM" data-mt="1615">4:15 PM</option>
                                                    <option value="4:30 PM" data-mt="1630">4:30 PM</option>
                                                    <option value="4:45 PM" data-mt="1645">4:45 PM</option>
                                                    <option value="5:00 PM" data-mt="1700">5:00 PM</option>
                                                    <option value="5:15 PM" data-mt="1715">5:15 PM</option>
                                                    <option value="5:30 PM" data-mt="1730">5:30 PM</option>
                                                    <option value="5:45 PM" data-mt="1745">5:45 PM</option>
                                                    <option value="6:00 PM" data-mt="1800">6:00 PM</option>
                                                    <option value="6:15 PM" data-mt="1815">6:15 PM</option>
                                                    <option value="6:30 PM" data-mt="1830">6:30 PM</option>
                                                    <option value="6:45 PM" data-mt="1845">6:45 PM</option>
                                                    <option value="7:00 PM" data-mt="1900">7:00 PM</option>
                                                    <option value="7:15 PM" data-mt="1915">7:15 PM</option>
                                                    <option value="7:30 PM" data-mt="1930">7:30 PM</option>
                                                    <option value="7:45 PM" data-mt="1945">7:45 PM</option>
                                                    <option value="8:00 PM" data-mt="2000">8:00 PM</option>
                                                    <option value="8:15 PM" data-mt="2015">8:15 PM</option>
                                                    <option value="8:30 PM" data-mt="2030">8:30 PM</option>
                                                    <option value="8:45 PM" data-mt="2045">8:45 PM</option>
                                                    <option value="9:00 PM" data-mt="2100">9:00 PM</option>
                                                    <option value="9:15 PM" data-mt="2115">9:15 PM</option>
                                                    <option value="9:30 PM" data-mt="2130">9:30 PM</option>
                                                    <option value="9:45 PM" data-mt="2145">9:45 PM</option>
                                                    <option value="10:00 PM" data-mt="2200">10:00 PM</option>
                                                    <option value="10:15 PM" data-mt="2215">10:15 PM</option>
                                                    <option value="10:30 PM" data-mt="2230">10:30 PM</option>
                                                    <option value="10:45 PM" data-mt="2245">10:45 PM</option>
                                                    <option value="11:00 PM" data-mt="2300">11:00 PM</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <a class="btn btn-verify btn-validate btn-sm btn-block" style="margin-top: 25px; padding: 5px 0px 5px 0px;"><small>verify</small></a>
                                            </div>
                                        </div>
                                        <a class="btn btn-block btn-default btn-sm btn-addmoreday3" style="margin-top:2rem;">ADD MORE DAY</a>
                                        <br>
                                    <!-- SECTION DAY 3: END -->

                                    <!-- SECTION DAY 4: START -->
                                        <div class="alert-day4 alert alert-danger alert-dismissible fade in" role="alert" style="border-radius:0px;">
                                            <button type="button" class="close alert-close"><span aria-hidden="true">×</span></button>
                                            <strong>Error: </strong>
                                            <span></span><span></span><span></span><span></span>
                                        </div>
                                        <div class="section-validation row">
                                            <div class="error-hint col-lg-2">
                                                <label for="Day4" >Day 4</label>
                                                <select name="day4" id="day4" class="validate-day validate-day-4 select2 form-control">
                                                    <option value="">None</option>
                                                    <option value="TBA">TBA</option>
                                                    <option value="Monday">MON</option>
                                                    <option value="Tuesday">TUE</option>
                                                    <option value="Wednesday">WED</option>
                                                    <option value="Thursday">THU</option>
                                                    <option value="Friday">FRI</option>
                                                    <option value="Saturday">SAT</option>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-4">
                                                <label for="Room4" >Room Name</label>
                                                <?php $viewStudents = ROOM:: getallrooms(array("room_NAME"=>"ASC")); ?>
                                                <select name="room_ID4" id="room_ID4" class="validate-room-4 form-control select2">
                                                    <option value="">None</option>
                                                    <?php foreach($viewStudents as $key => $val){ ?>
                                                    <option value="<?php echo $val['room_ID']; ?>"><?php echo $val['room_NAME']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-2">
                                                <label for="Start4">Start</label>
                                                <select name="start4" id="start4" class="start-time validate-start-4 form-control select2">
                                                    <option value="" data-mt="NONE">None</option>
                                                    <option value="TBA" data-mt="TBA">TBA</option>  
                                                    <option value="7:00 AM" data-mt="700">7:00 AM</option>
                                                    <option value="7:15 AM" data-mt="715">7:15 AM</option>
                                                    <option value="7:30 AM" data-mt="730">7:30 AM</option>
                                                    <option value="7:45 AM" data-mt="745">7:45 AM</option>
                                                    <option value="8:00 AM" data-mt="800">8:00 AM</option>
                                                    <option value="8:15 AM" data-mt="815">8:15 AM</option>
                                                    <option value="8:30 AM" data-mt="830">8:30 AM</option>
                                                    <option value="8:45 AM" data-mt="845">8:45 AM</option>
                                                    <option value="9:00 AM" data-mt="900">9:00 AM</option>
                                                    <option value="9:15 AM" data-mt="915">9:15 AM</option>
                                                    <option value="9:30 AM" data-mt="930">9:30 AM</option>
                                                    <option value="9:45 AM" data-mt="945">9:45 AM</option>
                                                    <option value="10:00 AM" data-mt="1000">10:00 AM</option>
                                                    <option value="10:15 AM" data-mt="1015">10:15 AM</option>
                                                    <option value="10:30 AM" data-mt="1030">10:30 AM</option>
                                                    <option value="10:45 AM" data-mt="1045">10:45 AM</option>
                                                    <option value="11:00 AM" data-mt="1100">11:00 AM</option>
                                                    <option value="11:15 AM" data-mt="1115">11:15 AM</option>
                                                    <option value="11:30 AM" data-mt="1130">11:30 AM</option>
                                                    <option value="11:45 AM" data-mt="1145">11:45 AM</option>
                                                    <option value="12:00 PM" data-mt="1200">12:00 PM</option>
                                                    <option value="12:15 PM" data-mt="1215">12:15 PM</option>
                                                    <option value="12:30 PM" data-mt="1230">12:30 PM</option>
                                                    <option value="12:45 PM" data-mt="1245">12:45 PM</option>
                                                    <option value="1:00 PM" data-mt="1300">1:00 PM</option>
                                                    <option value="1:15 PM" data-mt="1315">1:15 PM</option>
                                                    <option value="1:30 PM" data-mt="1330">1:30 PM</option>
                                                    <option value="1:45 PM" data-mt="1345">1:45 PM</option>
                                                    <option value="2:00 PM" data-mt="1400">2:00 PM</option>
                                                    <option value="2:15 PM" data-mt="1415">2:15 PM</option>
                                                    <option value="2:30 PM" data-mt="1430">2:30 PM</option>
                                                    <option value="2:45 PM" data-mt="1445">2:45 PM</option>
                                                    <option value="3:00 PM" data-mt="1500">3:00 PM</option>
                                                    <option value="3:15 PM" data-mt="1515">3:15 PM</option>
                                                    <option value="3:30 PM" data-mt="1530">3:30 PM</option>
                                                    <option value="3:45 PM" data-mt="1545">3:45 PM</option>
                                                    <option value="4:00 PM" data-mt="1600">4:00 PM</option>
                                                    <option value="4:15 PM" data-mt="1615">4:15 PM</option>
                                                    <option value="4:30 PM" data-mt="1630">4:30 PM</option>
                                                    <option value="4:45 PM" data-mt="1645">4:45 PM</option>
                                                    <option value="5:00 PM" data-mt="1700">5:00 PM</option>
                                                    <option value="5:15 PM" data-mt="1715">5:15 PM</option>
                                                    <option value="5:30 PM" data-mt="1730">5:30 PM</option>
                                                    <option value="5:45 PM" data-mt="1745">5:45 PM</option>
                                                    <option value="6:00 PM" data-mt="1800">6:00 PM</option>
                                                    <option value="6:15 PM" data-mt="1815">6:15 PM</option>
                                                    <option value="6:30 PM" data-mt="1830">6:30 PM</option>
                                                    <option value="6:45 PM" data-mt="1845">6:45 PM</option>
                                                    <option value="7:00 PM" data-mt="1900">7:00 PM</option>
                                                    <option value="7:15 PM" data-mt="1915">7:15 PM</option>
                                                    <option value="7:30 PM" data-mt="1930">7:30 PM</option>
                                                    <option value="7:45 PM" data-mt="1945">7:45 PM</option>
                                                    <option value="8:00 PM" data-mt="2000">8:00 PM</option>
                                                    <option value="8:15 PM" data-mt="2015">8:15 PM</option>
                                                    <option value="8:30 PM" data-mt="2030">8:30 PM</option>
                                                    <option value="8:45 PM" data-mt="2045">8:45 PM</option>
                                                    <option value="9:00 PM" data-mt="2100">9:00 PM</option>
                                                    <option value="9:15 PM" data-mt="2115">9:15 PM</option>
                                                    <option value="9:30 PM" data-mt="2130">9:30 PM</option>
                                                    <option value="9:45 PM" data-mt="2145">9:45 PM</option>
                                                    <option value="10:00 PM" data-mt="2200">10:00 PM</option>
                                                    <option value="10:15 PM" data-mt="2215">10:15 PM</option>
                                                    <option value="10:30 PM" data-mt="2230">10:30 PM</option>
                                                    <option value="10:45 PM" data-mt="2245">10:45 PM</option>
                                                    <option value="11:00 PM" data-mt="2300">11:00 PM</option>
                                                </select>
                                            </div>
                                            <div class="error-hint col-lg-2">
                                                <label for="End4">End</label>
                                                <select name="end4" id="end4" class="validate-end-4 form-control select2">
                                                    <option value="" data-mt="NONE">None</option>
                                                    <option value="TBA" data-mt="TBA">TBA</option>  
                                                    <option value="7:00 AM" data-mt="700">7:00 AM</option>
                                                    <option value="7:15 AM" data-mt="715">7:15 AM</option>
                                                    <option value="7:30 AM" data-mt="730">7:30 AM</option>
                                                    <option value="7:45 AM" data-mt="745">7:45 AM</option>
                                                    <option value="8:00 AM" data-mt="800">8:00 AM</option>
                                                    <option value="8:15 AM" data-mt="815">8:15 AM</option>
                                                    <option value="8:30 AM" data-mt="830">8:30 AM</option>
                                                    <option value="8:45 AM" data-mt="845">8:45 AM</option>
                                                    <option value="9:00 AM" data-mt="900">9:00 AM</option>
                                                    <option value="9:15 AM" data-mt="915">9:15 AM</option>
                                                    <option value="9:30 AM" data-mt="930">9:30 AM</option>
                                                    <option value="9:45 AM" data-mt="945">9:45 AM</option>
                                                    <option value="10:00 AM" data-mt="1000">10:00 AM</option>
                                                    <option value="10:15 AM" data-mt="1015">10:15 AM</option>
                                                    <option value="10:30 AM" data-mt="1030">10:30 AM</option>
                                                    <option value="10:45 AM" data-mt="1045">10:45 AM</option>
                                                    <option value="11:00 AM" data-mt="1100">11:00 AM</option>
                                                    <option value="11:15 AM" data-mt="1115">11:15 AM</option>
                                                    <option value="11:30 AM" data-mt="1130">11:30 AM</option>
                                                    <option value="11:45 AM" data-mt="1145">11:45 AM</option>
                                                    <option value="12:00 PM" data-mt="1200">12:00 PM</option>
                                                    <option value="12:15 PM" data-mt="1215">12:15 PM</option>
                                                    <option value="12:30 PM" data-mt="1230">12:30 PM</option>
                                                    <option value="12:45 PM" data-mt="1245">12:45 PM</option>
                                                    <option value="1:00 PM" data-mt="1300">1:00 PM</option>
                                                    <option value="1:15 PM" data-mt="1315">1:15 PM</option>
                                                    <option value="1:30 PM" data-mt="1330">1:30 PM</option>
                                                    <option value="1:45 PM" data-mt="1345">1:45 PM</option>
                                                    <option value="2:00 PM" data-mt="1400">2:00 PM</option>
                                                    <option value="2:15 PM" data-mt="1415">2:15 PM</option>
                                                    <option value="2:30 PM" data-mt="1430">2:30 PM</option>
                                                    <option value="2:45 PM" data-mt="1445">2:45 PM</option>
                                                    <option value="3:00 PM" data-mt="1500">3:00 PM</option>
                                                    <option value="3:15 PM" data-mt="1515">3:15 PM</option>
                                                    <option value="3:30 PM" data-mt="1530">3:30 PM</option>
                                                    <option value="3:45 PM" data-mt="1545">3:45 PM</option>
                                                    <option value="4:00 PM" data-mt="1600">4:00 PM</option>
                                                    <option value="4:15 PM" data-mt="1615">4:15 PM</option>
                                                    <option value="4:30 PM" data-mt="1630">4:30 PM</option>
                                                    <option value="4:45 PM" data-mt="1645">4:45 PM</option>
                                                    <option value="5:00 PM" data-mt="1700">5:00 PM</option>
                                                    <option value="5:15 PM" data-mt="1715">5:15 PM</option>
                                                    <option value="5:30 PM" data-mt="1730">5:30 PM</option>
                                                    <option value="5:45 PM" data-mt="1745">5:45 PM</option>
                                                    <option value="6:00 PM" data-mt="1800">6:00 PM</option>
                                                    <option value="6:15 PM" data-mt="1815">6:15 PM</option>
                                                    <option value="6:30 PM" data-mt="1830">6:30 PM</option>
                                                    <option value="6:45 PM" data-mt="1845">6:45 PM</option>
                                                    <option value="7:00 PM" data-mt="1900">7:00 PM</option>
                                                    <option value="7:15 PM" data-mt="1915">7:15 PM</option>
                                                    <option value="7:30 PM" data-mt="1930">7:30 PM</option>
                                                    <option value="7:45 PM" data-mt="1945">7:45 PM</option>
                                                    <option value="8:00 PM" data-mt="2000">8:00 PM</option>
                                                    <option value="8:15 PM" data-mt="2015">8:15 PM</option>
                                                    <option value="8:30 PM" data-mt="2030">8:30 PM</option>
                                                    <option value="8:45 PM" data-mt="2045">8:45 PM</option>
                                                    <option value="9:00 PM" data-mt="2100">9:00 PM</option>
                                                    <option value="9:15 PM" data-mt="2115">9:15 PM</option>
                                                    <option value="9:30 PM" data-mt="2130">9:30 PM</option>
                                                    <option value="9:45 PM" data-mt="2145">9:45 PM</option>
                                                    <option value="10:00 PM" data-mt="2200">10:00 PM</option>
                                                    <option value="10:15 PM" data-mt="2215">10:15 PM</option>
                                                    <option value="10:30 PM" data-mt="2230">10:30 PM</option>
                                                    <option value="10:45 PM" data-mt="2245">10:45 PM</option>
                                                    <option value="11:00 PM" data-mt="2300">11:00 PM</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <a class="btn btn-verify btn-validate btn-sm btn-block" style="margin-top: 25px; padding: 5px 0px 5px 0px;"><small>verify</small></a>
                                            </div>
                                        </div>
                                        <br>
                                        <!-- <a class="btn btn-block btn-default btn-sm btn-addmoreday3" style="margin-top:2rem;">ADD MORE DAY</a> -->
                                    <!-- SECTION DAY 4: END -->

                                    <!-- INSTRUCTOR/MAX STUDENT: START -->
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label for="Instructor" class="">Instructor</label>
                                                <?php
                                                $viewIntructors = INSTRUCTORS:: getAllInstructorsbyStatus(array("instructor_ID"=>"ASC"));
                                                ?>
                                                <select name="instructor_ID" id="instructor_ID" class="select2 form-control">
                                                <option value="">None</option>
                                                <?php foreach($viewIntructors as $key => $val){ ?>

                                                <option value="<?php echo $val['instructor_ID']; ?>">
                                                <?php echo $val['instructor_NAME']; ?> <?php echo $val['instructor_LNAME']; ?>  
                                                </option>
                                                <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-6">
                                                <label for="max_student" class="">Max Student</label>
                                                <input class="form-control input-sm" name="max_student" type="number" id="max_student" value="30" style="background-color: #fff; border: 1px solid #aaa; border-radius: 4px; height: 28px;"/>
                                            </div>
                                        </div>
                                        <br>
                                    <!-- INSTRUCTOR/MAX STUDENT: END -->

                                    <!-- CONFLICT CHECKER/SUBMIT: START -->
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading text-center" role="tab" id="headingTwo">
                                                            <h4 class="panel-title count-conflict">
                                                                You have <span class='noc text-danger'>0</span> conflict schedule <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><small>(view)</small></a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body" style="padding:0px;">
                                                        <div class="list-group list-of-conflict">
                                                        </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br/>
                                                <a class="btn btn-block btn-warning btn-conflictChecker">Conflict Checker</a>
                                                <button id="button" class="btn btnAdd-subjectsection btn-success btn-block P4C7_ADDSUBJECTSECTION P5C8_ADDSUBJECTSECTION P5C10_ADDSUBJECTSECTION" type="submit">Add Subject Section</button>
                                            </div>
                                        </div>
                                    <!-- CONFLICT CHECKER/SUBMIT: END -->

                                <!-- ENCODING OF CLASS SCHEDULE: END -->

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <!-- ENCODING OF SUBJECT: END -->

        <!-- FULL LIST OF SUBJECT ENCODED: START -->
            <!-- <div class="master-list"> -->
            <div class="master-list" hidden="hidden">
            <?php $subject_section_list = SUBJSCHED::getAll(array("subject_sched_ID" => "ASC")); ?>
            <?php foreach($subject_section_list as $key => $payload){ ?>
                <ul class="no-reference">
                    <!-- A. REFERENCE NUMBER -->
                    <li><?php echo $payload['subject_sched_ID']; ?></li>
                    <!-- B. SECTION NUMBER -->
                    <?php $sem = SEMSECTION::getID($payload['semester_section_ID']); ?>
                    <li><?php echo $sem['semester_section_ID'] ?></li>
                    <!-- C. SUBJECT NUMBER -->
                    <?php $c = SUBJECTS::getID($payload['subject_ID']); ?>
                    <li><?php echo $c['subject_ID'] ?></li>

                    <!-- DAY 1 -->
                    <li><?php if($payload['day'] != NULL){?><?php echo $payload['day'];?><?php }else{ echo "NONE"; }?></li>
                    <li data-roomname1="<?php $r = ROOM::getID($payload['room_ID']); echo $r['room_NAME']; ?>"><?php $r = ROOM::getID($payload['room_ID']); echo $r['room_ID']; ?></li>
                    <li data-starttime="<?php echo $payload['start']; ?>" class="start1_<?php echo $payload['subject_sched_ID']; ?>"></li>
                    <li data-endtime="<?php echo $payload['end']; ?>" class="end1_<?php echo $payload['subject_sched_ID']; ?>"></li>
                    <!-- DAY 2 -->
                    <li><?php if($payload['day2'] != NULL){?><?php echo $payload['day2'];?><?php }else{ echo "NONE"; }?></li>
                    <li data-roomname2="<?php $r = ROOM::getID($payload['room_ID2']); echo $r['room_NAME']; ?>"><?php $r = ROOM::getID($payload['room_ID2']); echo $r['room_ID']; ?></li>
                    <li data-starttime2="<?php echo $payload['start2']; ?>" class="start2_<?php echo $payload['subject_sched_ID']; ?>"></li>
                    <li data-endtime2="<?php echo $payload['end2']; ?>" class="end2_<?php echo $payload['subject_sched_ID']; ?>"></li>
                    <!-- DAY 3 -->
                    <li><?php if($payload['day3'] != NULL){?><?php echo $payload['day3'];?><?php }else{ echo "NONE"; }?></li>
                    <li data-roomname3="<?php $r = ROOM::getID($payload['room_ID3']); echo $r['room_NAME']; ?>"><?php $r = ROOM::getID($payload['room_ID3']); echo $r['room_ID']; ?></li>
                    <li data-starttime3="<?php echo $payload['start3']; ?>" class="start3_<?php echo $payload['subject_sched_ID']; ?>"></li>
                    <li data-endtime3="<?php echo $payload['end3']; ?>" class="end3_<?php echo $payload['subject_sched_ID']; ?>"></li>
                    <!-- DAY 4 -->
                    <li><?php if($payload['day4'] != NULL){?><?php echo $payload['day4'];?><?php }else{ echo "NONE"; }?></li>
                    <li data-roomname4="<?php $r = ROOM::getID($payload['room_ID4']); echo $r['room_NAME']; ?>"><?php $r = ROOM::getID($payload['room_ID4']); echo $r['room_ID']; ?></li>
                    <li data-starttime4="<?php echo $payload['start4']; ?>" class="start4_<?php echo $payload['subject_sched_ID']; ?>"></li>
                    <li data-endtime4="<?php echo $payload['end4']; ?>" class="end4_<?php echo $payload['subject_sched_ID']; ?>"></li>
                </ul>

                <script>
                        function convertToMilitaryTime(classname,time){
                            if(time === "7:00 AM"){
                                var ctime = 700
                            }else if(time === "7:15 AM"){
                                var ctime = 715
                            }else if(time === "7:30 AM"){
                                var ctime = 730
                            }else if(time === "7:45 AM"){
                                var ctime = 745
                            }else if(time === "8:00 AM"){
                                var ctime = 800
                            }else if(time === "8:15 AM"){
                                var ctime = 815
                            }else if(time === "8:30 AM"){
                                var ctime = 830
                            }else if(time === "8:45 AM"){
                                var ctime = 845
                            }else if(time === "9:00 AM"){
                                var ctime = 900
                            }else if(time === "9:15 AM"){
                                var ctime = 915
                            }else if(time === "9:30 AM"){
                                var ctime = 930
                            }else if(time === "9:45 AM"){
                                var ctime = 945
                            }else if(time === "10:00 AM"){
                                var ctime = 1000
                            }else if(time === "10:15 AM"){
                                var ctime = 1015
                            }else if(time === "10:30 AM"){
                                var ctime = 1030
                            }else if(time === "10:45 AM"){
                                var ctime = 1045
                            }else if(time === "11:00 AM"){
                                var ctime = 1100
                            }else if(time === "11:15 AM"){
                                var ctime = 1115
                            }else if(time === "11:30 AM"){
                                var ctime = 1130
                            }else if(time === "11:45 AM"){
                                var ctime = 1145
                            }else if(time === "12:00 PM"){
                                var ctime = 1200
                            }else if(time === "12:15 PM"){
                                var ctime = 1215
                            }else if(time === "12:30 PM"){
                                var ctime = 1230
                            }else if(time === "12:45 PM"){
                                var ctime = 1245
                            }else if(time === "1:00 PM"){
                                var ctime = 1300
                            }else if(time === "1:15 PM"){
                                var ctime = 1315
                            }else if(time === "1:30 PM"){
                                var ctime = 1330
                            }else if(time === "1:45 PM"){
                                var ctime = 1345
                            }else if(time === "2:00 PM"){
                                var ctime = 1400
                            }else if(time === "2:15 PM"){
                                var ctime = 1415
                            }else if(time === "2:30 PM"){
                                var ctime = 1430
                            }else if(time === "2:45 PM"){
                                var ctime = 1445
                            }else if(time === "3:00 PM"){
                                var ctime = 1500
                            }else if(time === "3:15 PM"){
                                var ctime = 1515
                            }else if(time === "3:30 PM"){
                                var ctime = 1530
                            }else if(time === "3:45 PM"){
                                var ctime = 1545
                            }else if(time === "4:00 PM"){
                                var ctime = 1600
                            }else if(time === "4:15 PM"){
                                var ctime = 1615
                            }else if(time === "4:30 PM"){
                                var ctime = 1630
                            }else if(time === "4:45 PM"){
                                var ctime = 1645
                            }else if(time === "5:00 PM"){
                                var ctime = 1700
                            }else if(time === "5:15 PM"){
                                var ctime = 1715
                            }else if(time === "5:30 PM"){
                                var ctime = 1730
                            }else if(time === "5:45 PM"){
                                var ctime = 1745
                            }else if(time === "6:00 PM"){
                                var ctime = 1800
                            }else if(time === "6:15 PM"){
                                var ctime = 1815
                            }else if(time === "6:30 PM"){
                                var ctime = 1830
                            }else if(time === "6:45 PM"){
                                var ctime = 1845
                            }else if(time === "7:00 PM"){
                                var ctime = 1900
                            }else if(time === "7:15 PM"){
                                var ctime = 1915
                            }else if(time === "7:30 PM"){
                                var ctime = 1930
                            }else if(time === "7:45 PM"){
                                var ctime = 1945
                            }else if(time === "8:00 PM"){
                                var ctime = 2000
                            }else if(time === "8:15 PM"){
                                var ctime = 2015
                            }else if(time === "8:30 PM"){
                                var ctime = 2030
                            }else if(time === "8:45 PM"){
                                var ctime = 2045
                            }else if(time === "9:00 PM"){
                                var ctime = 2100
                            }else if(time === "9:15 PM"){
                                var ctime = 2115
                            }else if(time === "9:30 PM"){
                                var ctime = 2130
                            }else if(time === "9:45 PM"){
                                var ctime = 2145
                            }else if(time === "10:00 PM"){
                                var ctime = 2200
                            }else if(time === "10:15 PM"){
                                var ctime = 2215
                            }else if(time === "10:30 PM"){
                                var ctime = 2230
                            }else if(time === "10:45 PM"){
                                var ctime = 2245
                            }else if(time === "11:00 PM"){
                                var ctime = 2300
                            }else if(time === "TBA"){
                                var ctime = "TBA"
                            }else{
                                var ctime = "NONE"
                            }
                            $("."+classname).text(ctime)
                        }
                        function start1(){
                            convertToMilitaryTime("start1_<?php echo $payload['subject_sched_ID']; ?>","<?php echo $payload['start']; ?>")  
                        }
                        function end1(){
                            convertToMilitaryTime("end1_<?php echo $payload['subject_sched_ID']; ?>","<?php echo $payload['end']; ?>")  
                        }
                        function start2(){
                            convertToMilitaryTime("start2_<?php echo $payload['subject_sched_ID']; ?>","<?php echo $payload['start2']; ?>")  
                        }
                        function end2(){
                            convertToMilitaryTime("end2_<?php echo $payload['subject_sched_ID']; ?>","<?php echo $payload['end2']; ?>")  
                        }
                        function start3(){
                            convertToMilitaryTime("start3_<?php echo $payload['subject_sched_ID']; ?>","<?php echo $payload['start3']; ?>")  
                        }
                        function end3(){
                            convertToMilitaryTime("end3_<?php echo $payload['subject_sched_ID']; ?>","<?php echo $payload['end3']; ?>")  
                        }
                        function start4(){
                            convertToMilitaryTime("start4_<?php echo $payload['subject_sched_ID']; ?>","<?php echo $payload['start4']; ?>")  
                        }
                        function end4(){
                            convertToMilitaryTime("end4_<?php echo $payload['subject_sched_ID']; ?>","<?php echo $payload['end4']; ?>")  
                        }
                        start1(),end1(),start2(),end2(),start3(),end3(),start4(),end4();
                    </script>
            <?php } ?>
            
            </div>
        <!-- FULL LIST OF SUBJECT ENCODED: END -->


        <!-- LIST OF SUBJECT: START -->
            <div class="row">
                <div class="col-lg-12">
                    <?php $record = SUBJSCHED::getAll(array("subject_sched_ID" => "ASC")); ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                    <i class="fa fa-list"></i>  List of Schedules  
                    </div>
                    <div class="panel-body">
                    <table id="test" class="display nowrap schedule_list" cellspacing="0" width="100%">
                        <thead>
                            <tr style="color: grey">
                                <th width="" style="background: #ffffff; height: 76px;">Ref</th>
                                <th width="" style="background: #ffffff; height: 76px;">Section</th>
                                <th width="" style="background: #ffffff; height: 76px;">Subject</th>
                                <th width="">Day</th>
                                <th width="">Room</th>
                                <th width="">Start</th>
                                <th width="">End</th>
                                <th width="">Instructor</th>
                                <th width="" hidden>Day 2</th>
                                <th width="" hidden>Room 2</th>
                                <th width="" hidden>Start 2</th>
                                <th width="" hidden>End 2</th>
                                <th width="" hidden>Instructor 2</th>
                                <th width="">Max<br>Capacity</th>
                                <th width="">Option</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                // $x= 1;
                                foreach($record as $key => $val){
                                // if($x <= 50){
                            ?>
                            <tr id="REFERENCE_<?php echo $val['subject_sched_ID']; ?>" class="per-row">
                                <td style="background: none;">
                                    <p><?php echo $val['subject_sched_ID']; ?></p>
                                </td>
                                <td>
                                    <?php $sem = SEMSECTION::getID($val['semester_section_ID']); ?>
                                    <?php $d = SEMSECTION::getSEM($sem['sem_ID']);
                                    echo $d['sem_NAME']; ?>
                                    <br>
                                    <?php $c = COURSE::getbyID($sem['course_ID']);
                                    echo $c['course_INIT']; ?>
                                    -
                                    <?php echo $sem['semester_section_NAME']; ?>
                                    <p data-id="<?php echo $sem['semester_section_ID'] ?>" hidden><?php echo $d['sem_NAME']." - ".$c['course_INIT']."-".$sem['semester_section_NAME'] ?><p>
                                </td>
                                <td>
                                    <?php $c = SUBJECTS::getID( $val['subject_ID']);
                                    echo "(".$c['subject_CODE'].")".substr($c['subject_DESCRIPTION'], 0, 30)."<br>"; 
                                    echo substr($c['subject_DESCRIPTION'], 30, 35)."<br>";
                                    echo substr($c['subject_DESCRIPTION'], 65, 35); ?>
                                    <p hidden><?php echo $c['subject_CODE']." - ".$c['subject_DESCRIPTION']." - ".$c['LEC_UNIT']."/".$c['LEC_UNIT'] ?></p>
                                </td>
                                <td>
                                    <span><?php echo $val['day']; ?></span><br><span><?php if($val['day2'] != NULL){ ?><?php echo $val['day2']; ?> <?php } ?></span><br><?php if($val['day3'] != NULL){ ?><?php echo $val['day3']; ?> <?php } ?><br><?php if($val['day4'] != NULL){ ?><?php echo $val['day4']; ?> <?php } ?>
                                    <p hidden><?php if($val['day'] != NULL){?><?php echo $val['day'];?><?php }else{ echo "NONE"; }?></p>
                                    <p hidden><?php if($val['day2'] != NULL){?><?php echo $val['day2'];?><?php }else{ echo "NONE"; }?></p>
                                    <p hidden><?php if($val['day3'] != NULL){?><?php echo $val['day3'];?><?php }else{ echo "NONE"; }?></p>
                                    <p hidden><?php if($val['day4'] != NULL){?><?php echo $val['day4'];?><?php }else{ echo "NONE"; }?></p>
                                </td>
                                <td>
                                    <span class="los_room1"><?php $r = ROOM::getID($val['room_ID']); echo $r['room_NAME']; ?></span>
                                    <br>
                                    <?php if($val['room_ID2'] != NULL){ ?>
                                    <?php
                                    $r = ROOM::getID($val['room_ID2']);
                                    echo $r['room_NAME'];
                                    } ?>
                                    <br>
                                    <?php if($val['room_ID3'] != NULL){ ?>
                                    <?php
                                    $r = ROOM::getID($val['room_ID3']);
                                    echo $r['room_NAME'];
                                    } ?>
                                    <br>
                                    <?php if($val['room_ID4'] != NULL){ ?>
                                    <?php
                                    $r = ROOM::getID($val['room_ID4']);
                                    echo $r['room_NAME'];
                                    } ?>
                                    <p hidden><?php if($val['room_ID2'] != NULL){?><?php $r = ROOM::getID($val['room_ID']); echo $r['room_NAME']; } else { echo "NONE"; } ?></p>
                                    <p hidden><?php if($val['room_ID2'] != NULL){?><?php $r = ROOM::getID($val['room_ID2']); echo $r['room_NAME']; } else { echo "NONE"; } ?></p>
                                    <p hidden><?php if($val['room_ID3'] != NULL){?><?php $r = ROOM::getID($val['room_ID3']); echo $r['room_NAME']; } else { echo "NONE"; } ?></p>
                                    <p hidden><?php if($val['room_ID4'] != NULL){?><?php $r = ROOM::getID($val['room_ID4']); echo $r['room_NAME']; } else { echo "NONE"; } ?></p>
                                
                                </td>
                                <td>
                                    <?php echo $val['start']; ?><br><?php if($val['start2'] != NULL){ ?><?php echo $val['start2']; ?><?php } ?><br><?php if($val['start3'] != NULL){ ?><?php echo $val['start3']; ?><?php } ?><br><?php if($val['start4'] != NULL){ ?><?php echo $val['start4']; ?><?php } ?>
                                    <p hidden><?php if($val['start'] != NULL){ ?><?php echo $val['start']; ?><?php } else { echo "NONE"; } ?></p>
                                    <p hidden><?php if($val['start2'] != NULL){ ?><?php echo $val['start2']; ?><?php } else { echo "NONE"; } ?></p>
                                    <p hidden><?php if($val['start3'] != NULL){ ?><?php echo $val['start3']; ?><?php } else { echo "NONE"; } ?></p>
                                    <p hidden><?php if($val['start4'] != NULL){ ?><?php echo $val['start4']; ?><?php } else { echo "NONE"; } ?></p>
                                </td>
                                <td>
                                    <?php echo $val['end']; ?><br><?php if($val['end2'] != NULL){ ?><?php echo $val['end2']; ?><?php } ?><br><?php if($val['end3'] != NULL){ ?><?php echo $val['end3']; ?><?php } ?><br><?php if($val['end4'] != NULL){ ?><?php echo $val['end4']; ?><?php } ?>
                                    <p hidden><?php if($val['end'] != NULL){ ?><?php echo $val['end']; ?><?php } else { echo "NONE"; } ?></p>
                                    <p hidden><?php if($val['end2'] != NULL){ ?><?php echo $val['end2']; ?><?php } else { echo "NONE"; } ?></p>
                                    <p hidden><?php if($val['end3'] != NULL){ ?><?php echo $val['end3']; ?><?php } else { echo "NONE"; } ?></p>
                                    <p hidden><?php if($val['end4'] != NULL){ ?><?php echo $val['end4']; ?><?php } else { echo "NONE"; } ?></p>
                                </td>
                                <td>
                                    <?php $i = INSTRUCTORS::getSingle(array(),$val['instructor_ID']);
                                    echo $i['instructor_NAME']." ".$i['instructor_LNAME']; ?></td>
                                </td>
                                <td hidden>
                                    <?php if($val['day2'] != NULL){ ?><?php echo $val['day2']; ?> <?php } ?>
                                </td>
                                <td hidden> 
                                    <?php if($val['room_ID2'] != NULL){ ?>
                                    <?php $r = ROOM::getID($val['room_ID2']); echo $r['room_NAME']; } ?>
                                </td>
                                <td hidden></td>
                                <td hidden></td>
                                <td hidden>
                                    <?php $i = INSTRUCTORS::getSingle(array(),$val['instructor_ID']);
                                    echo $i['instructor_NAME']." ".$i['instructor_LNAME']; ?></td>
                                <td>
                                    <?php echo $val['max_student']; ?>
                                </td>
                                <td>
                                    <a href="subject_sched.php?action=edit&id=<?php echo $val['subject_sched_ID']; ?>">Edit</a>
                                </td>
                            </tr>
                            <?php  
                        // }
                            // $x++;
                            }
                            ?>
                        </tbody>
                    </table>
                    <script>
                        $(document).ready(function() {
                            var table = $('#test').DataTable( {
                                scrollY:        "300px",
                                scrollX:        true,
                                scrollCollapse: true,
                                paging:         true,
                                fixedColumns:   {
                                    leftColumns: 3
                                }
                            } );
                        } );
                    </script>
                    </div>
                    </div>
                </div>
            </div>
        <!-- LIST OF SUBJECT: END -->
              </div>
          </form>
        
          <?php
          #$record = SUBJSCHED::getAll(array("subject_sched_ID" => "DESC"));
          #print_r($record);
            //   echo "<script>console.log(".count($record).")</script>"  
          ?>
              
        </div>
    </div>

<!-- JRML: END -->


<script type="text/javascript"> 
    $(document).ready(function() {
        $('.select2').select2();
            $(document).on('keydown', '.select2', function(e) {
                if (e.originalEvent && e.which == 40) {
                    e.preventDefault();
                $(this).siblings('select').select2('open');
            }
        });
        $('select').select2({
            selectOnClose: true
        });
    });
</script>

<?php break; 

case "edit": ?>
<?php 
    $ssid1 = $_GET['id'];
    # msgbox($ssid);
    $ssid = SUBJSCHED::getSubjectSchedID($ssid1);
    # print_r($ssid);
?>
<div class="col-md-9">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
          <i class="fa fa-folder-o"></i>  Edit Subject Schedule </h3> 
        </div>
        <div class="panel-body">
          <form action="<?php echo $_SERVER['REQUEST_URI']?>" method="post">
              <div class="row">
                <div class="col-lg-6">
                  <label class="">Section</label>
                    <?php
                      #$sem = SEMSECTION::getAll(array("sem_ID"=>"DESC");
                      $sem = SEMSECTION::getAll(array("semester_section_ID"=>"DESC"));
                      #print_r($sem);
                    ?>
                    <select name="semester_section_ID" id="semester_section_ID" class="form-control">
                        <?php foreach($sem as $key => $val){ ?>
                      <option value="<?php echo $val['semester_section_ID']; ?>"
                                    <?php
                                        if(($val['semester_section_ID']) == ($ssid['semester_section_ID']))
                                        { 
                                            echo "selected=selected"; 
                                        }
                                    ?>
                                    >
                          <?php $sem = SEMSECTION::getID($val['semester_section_ID']); ?>
                          <?php $d = SEMSECTION::getSEM($sem['sem_ID']);
                          echo $d['sem_NAME']; ?>
                          -
                          <?php $c = COURSE::getbyID($val['course_ID']);
                          echo $c['course_INIT']; ?>-<?php echo $val['semester_section_NAME']; ?></option>
                            <?php } ?>
                    </select>
                </div>
                <div class="col-lg-6">
                  <label for="Subject" class="">Subject</label>
                    <?php
                       $viewSubjects = SUBJECTS:: getAllSubjects(array("subject_CODE"=>"ASC"));
                    ?>
                      
                      <select name="subject_ID" id="subject_ID" class="form-control">
                          <?php foreach($viewSubjects as $key => $val){ ?>
                          <option value="<?php echo $val['subject_ID']; ?>"
                                   <?php
                                      if(($val['subject_ID']) == ($ssid['subject_ID']))
                                      { 
                                          echo "selected=selected"; 
                                      }
                                  ?>
                                  
                          ><?php echo $val['subject_CODE']; ?> - <?php echo $val['subject_DESCRIPTION']; ?> - <?php echo $val['LEC_UNIT']."/".$val['LAB_UNIT']; ?></option>
                          <?php } ?>    
                      </select>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-lg-3">
                  <label for="Day" class="">Day</label>
                  <select name="day" id="day" class="form-control">
                    <option value="Monday"    
                            <?php
                                  if("Monday" == $ssid['day'])
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                              >MON</option>
                    <option value="Tuesday" <?php
                                  if("Tuesday"== ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> >TUE</option>
                    <option value="Wednesday"<?php
                                  if("Wednesday" == ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>WED</option>
                    <option value="Thursday"<?php
                                  if("Thursday" == ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>THU</option>
                    <option value="Friday"<?php
                                  if("Friday" == ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>FRI</option>
                    <option value="Saturday"<?php
                                  if("Saturday" == ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>SAT</option>
                        <option value="TBA"<?php
                                  if("TBA" == ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>TBA</option>         
                    <?php foreach($viewSubjects as $key => $val){ ?>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Room" class="">Room</label>
                    <?php
                       $viewStudents = ROOM:: getallrooms(array("room_ID"=>"ASC"));
                    ?>
                    <select name="room_ID" id="room_ID" class="form-control">
                      <?php foreach($viewStudents as $key => $val){ ?>
                      <option value="<?php echo $val['room_ID']; ?>" 
                              <?php
                                  if($val['room_ID']  == ($ssid['room_ID']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> ><?php echo $val['room_NAME']; ?></option>
                      <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3">
                  <label for="Start" class="">Start</label>
                  <select name="start" id="start" class="form-control">
                    <option value="">None</option>
                    <option  <?php
                                  if("TBA"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> value="TBA">TBA</option> 
                    <option value="7:00 AM"
                            <?php
                                  if("7:00 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 AM</option>
                    <option value="7:15 AM"
                            <?php
                                  if("7:15 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 AM</option>          
                    <option value="7:30 AM"
                            <?php
                                  if("7:30 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:30 AM</option>
                    <option value="7:45 AM"
                            <?php
                                  if("7:45 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:45 AM</option>        
                    <option value="8:00 AM"
                            <?php
                                  if("8:00 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:00 AM</option>
                    <option value="8:15 AM"
                            <?php
                                  if("8:15 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:15 AM</option>        
                    <option value="8:30 AM"
                            <?php
                                  if("8:30 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:30 AM</option>
                    <option value="8:45 AM"
                            <?php
                                  if("8:45 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:45 AM</option>        
                    <option value="9:00 AM" 
                            <?php
                                  if("9:00 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 AM</option>
                    <option value="9:15 AM" 
                            <?php
                                  if("9:15 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 AM</option>
                    <option value="9:30 AM"
                            <?php
                                  if("9:30 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:30 AM</option>
                    <option value="9:45 AM"
                            <?php
                                  if("9:45 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:45 AM</option>
                    <option value="10:00 AM"
                            <?php
                                  if("10:00 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:00 AM</option>
                    <option value="10:15 AM"
                            <?php
                                  if("10:15 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:15 AM</option>
                    <option value="10:30 AM" 
                            <?php
                                  if("10:30 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 AM</option>
                    <option value="10:45 AM" 
                            <?php
                                  if("10:45 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 AM</option>
                    <option value="11:00 AM" <?php
                                  if("11:00 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 AM</option>
                    <option value="11:15 AM" <?php
                                  if("11:15 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:15 AM</option>
                    <option value="11:30 AM" <?php
                                  if("11:30 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:30 AM</option>
                    <option value="11:45 AM" <?php
                                  if("11:45 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:45 AM</option>
                    <option value="12:00 PM"<?php
                                  if("12:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:00 PM</option>
                    <option value="12:15 PM"<?php
                                  if("12:15 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:15 PM</option>
                    <option value="12:30 PM"
                            <?php
                                  if("12:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:30 PM</option>
                    <option value="12:45 PM"
                            <?php
                                  if("12:45 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:45 PM</option>
                    <option value="1:00 PM" <?php
                                  if("1:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:00 PM</option>
                    <option value="1:15 PM" <?php
                                  if("1:15 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:15 PM</option>
                    <option value="1:30 PM" <?php
                                  if("1:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:30 PM</option>
                    <option value="1:45 PM" <?php
                                  if("1:45 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:45 PM</option>
                    <option value="2:00 PM" <?php
                                  if("2:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:00 PM</option>
                    <option value="2:15 PM" <?php
                                  if("2:15 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:15 PM</option>
                    <option value="2:30 PM" <?php
                                  if("2:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:30 PM</option>
                    <option value="2:45 PM" <?php
                                  if("2:45 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:45 PM</option>
                    <option value="3:00 PM" <?php
                                  if("3:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:00 PM</option>
                    <option value="3:15 PM" <?php
                                  if("3:15 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:15 PM</option>
                    <option value="3:30 PM" <?php
                                  if("3:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:30 PM</option>
                    <option value="3:45 PM" <?php
                                  if("3:45 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:45 PM</option>
                    <option value="4:00 PM" <?php
                                  if("4:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:00 PM</option>
                    <option value="4:15 PM" <?php
                                  if("4:15 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:15 PM</option>
                    <option value="4:30 PM" <?php
                                  if("4:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:30 PM</option>
                    <option value="4:45 PM" <?php
                                  if("4:45 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:45 PM</option>
                    <option value="5:00 PM" <?php
                                  if("5:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:00 PM</option>
                    <option value="5:15 PM" <?php
                                  if("5:15 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:15 PM</option>
                    <option value="5:30 PM" <?php
                                  if("5:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:30 PM</option>
                    <option value="5:45 PM" <?php
                                  if("5:45 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:45 PM</option>
                    <option value="6:00 PM"
                            <?php
                                  if("6:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:00 PM</option>
                    <option value="6:15 PM"
                            <?php
                                  if("6:15 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:15 PM</option>
                    <option value="6:30 PM" <?php
                                  if("6:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:30 PM</option>
                    <option value="6:45 PM"
                            <?php
                                  if("6:45 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:45 PM</option>
                    <option value="7:00 PM" <?php
                                  if("7:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 PM</option>
                    <option value="7:15 PM" <?php
                                  if("7:15 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 PM</option>
                    <option value="7:30 PM" <?php
                                  if("7:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:30 PM</option> 
                    <option value="7:45 PM" <?php
                                  if("7:45 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:45 PM</option>
                    <option value="8:00 PM" <?php
                                  if("8:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:00 PM</option>       
                    <option value="8:15 PM" <?php
                                  if("8:15 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:15 PM</option> 
                    <option value="8:30 PM" <?php
                                  if("8:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:30 PM</option> 
                    <option value="8:45 PM" <?php
                                  if("8:45 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:45 PM</option> 
                    <option value="9:00 PM" <?php
                                  if("9:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 PM</option>          
                    <option value="9:15 PM" <?php
                                  if("9:15 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 PM</option> 
                    <option value="9:30 PM" <?php
                                  if("9:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:30 PM</option> 
                    <option value="9:45 PM" <?php
                                  if("9:45 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:45 PM</option> 
                    <option value="10:00 PM" <?php
                                  if("10:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:00 PM</option>  
                    <option value="10:15 PM" <?php
                                  if("10:15 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:15 PM</option>
                    <option value="10:30 PM" <?php
                                  if("10:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 PM</option> 
                    <option value="10:45 PM" <?php
                                  if("10:45 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 PM</option> 
                    <option value="11:00 PM" <?php
                                  if("11:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 PM</option> 
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Installment" class="">End</label>
                  <select name="end" id="start" class="form-control">
                    <option value="">None</option>
                    <option  <?php
                                  if("TBA"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> value="TBA">TBA</option> 
                    <option value="7:00 AM"
                            <?php
                                  if("7:00 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 AM</option>
                    <option value="7:15 AM"
                            <?php
                                  if("7:15 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 AM</option>          
                    <option value="7:30 AM"
                            <?php
                                  if("7:30 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:30 AM</option>
                    <option value="7:45 AM"
                            <?php
                                  if("7:45 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:45 AM</option>        
                    <option value="8:00 AM"
                            <?php
                                  if("8:00 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:00 AM</option>
                    <option value="8:15 AM"
                            <?php
                                  if("8:15 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:15 AM</option>        
                    <option value="8:30 AM"
                            <?php
                                  if("8:30 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:30 AM</option>
                    <option value="8:45 AM"
                            <?php
                                  if("8:45 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:45 AM</option>        
                    <option value="9:00 AM" 
                            <?php
                                  if("9:00 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 AM</option>
                    <option value="9:15 AM" 
                            <?php
                                  if("9:15 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 AM</option>
                    <option value="9:30 AM"
                            <?php
                                  if("9:30 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:30 AM</option>
                    <option value="9:45 AM"
                            <?php
                                  if("9:45 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:45 AM</option>
                    <option value="10:00 AM"
                            <?php
                                  if("10:00 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:00 AM</option>
                    <option value="10:15 AM"
                            <?php
                                  if("10:15 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:15 AM</option>
                    <option value="10:30 AM" 
                            <?php
                                  if("10:30 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 AM</option>
                    <option value="10:45 AM" 
                            <?php
                                  if("10:45 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 AM</option>
                    <option value="11:00 AM" <?php
                                  if("11:00 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 AM</option>
                    <option value="11:15 AM" <?php
                                  if("11:15 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:15 AM</option>
                    <option value="11:30 AM" <?php
                                  if("11:30 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:30 AM</option>
                    <option value="11:45 AM" <?php
                                  if("11:45 AM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:45 AM</option>
                    <option value="12:00 PM"<?php
                                  if("12:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:00 PM</option>
                    <option value="12:15 PM"<?php
                                  if("12:15 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:15 PM</option>
                    <option value="12:30 PM"
                            <?php
                                  if("12:30 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:30 PM</option>
                    <option value="12:45 PM"
                            <?php
                                  if("12:45 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:45 PM</option>
                    <option value="1:00 PM" <?php
                                  if("1:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:00 PM</option>
                    <option value="1:15 PM" <?php
                                  if("1:15 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:15 PM</option>
                    <option value="1:30 PM" <?php
                                  if("1:30 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:30 PM</option>
                    <option value="1:45 PM" <?php
                                  if("1:45 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:45 PM</option>
                    <option value="2:00 PM" <?php
                                  if("2:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:00 PM</option>
                    <option value="2:15 PM" <?php
                                  if("2:15 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:15 PM</option>
                    <option value="2:30 PM" <?php
                                  if("2:30 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:30 PM</option>
                    <option value="2:45 PM" <?php
                                  if("2:45 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:45 PM</option>
                    <option value="3:00 PM" <?php
                                  if("3:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:00 PM</option>
                    <option value="3:15 PM" <?php
                                  if("3:15 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:15 PM</option>
                    <option value="3:30 PM" <?php
                                  if("3:30 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:30 PM</option>
                    <option value="3:45 PM" <?php
                                  if("3:45 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:45 PM</option>
                    <option value="4:00 PM" <?php
                                  if("4:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:00 PM</option>
                    <option value="4:15 PM" <?php
                                  if("4:15 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:15 PM</option>
                    <option value="4:30 PM" <?php
                                  if("4:30 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:30 PM</option>
                    <option value="4:45 PM" <?php
                                  if("4:45 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:45 PM</option>
                    <option value="5:00 PM" <?php
                                  if("5:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:00 PM</option>
                    <option value="5:15 PM" <?php
                                  if("5:15 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:15 PM</option>
                    <option value="5:30 PM" <?php
                                  if("5:30 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:30 PM</option>
                    <option value="5:45 PM" <?php
                                  if("5:45 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:45 PM</option>
                    <option value="6:00 PM"
                            <?php
                                  if("6:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:00 PM</option>
                    <option value="6:15 PM"
                            <?php
                                  if("6:15 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:15 PM</option>
                    <option value="6:30 PM" <?php
                                  if("6:30 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:30 PM</option>
                    <option value="6:45 PM"
                            <?php
                                  if("6:45 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:45 PM</option>
                    <option value="7:00 PM" <?php
                                  if("7:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 PM</option>
                    <option value="7:15 PM" <?php
                                  if("7:15 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 PM</option>
                    <option value="7:30 PM" <?php
                                  if("7:30 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:30 PM</option> 
                    <option value="7:45 PM" <?php
                                  if("7:45 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:45 PM</option>
                    <option value="8:00 PM" <?php
                                  if("8:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:00 PM</option>       
                    <option value="8:15 PM" <?php
                                  if("8:15 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:15 PM</option> 
                    <option value="8:30 PM" <?php
                                  if("8:30 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:30 PM</option> 
                    <option value="8:45 PM" <?php
                                  if("8:45 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:45 PM</option> 
                    <option value="9:00 PM" <?php
                                  if("9:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 PM</option>          
                    <option value="9:15 PM" <?php
                                  if("9:15 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 PM</option> 
                    <option value="9:30 PM" <?php
                                  if("9:30 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:30 PM</option> 
                    <option value="9:45 PM" <?php
                                  if("9:45 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:45 PM</option> 
                    <option value="10:00 PM" <?php
                                  if("10:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:00 PM</option>  
                    <option value="10:15 PM" <?php
                                  if("10:15 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:15 PM</option>
                    <option value="10:30 PM" <?php
                                  if("10:30 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 PM</option> 
                    <option value="10:45 PM" <?php
                                  if("10:45 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 PM</option> 
                    <option value="11:00 PM" <?php
                                  if("11:00 PM"  == ($ssid['end']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 PM</option>  
                  </select>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-lg-3">
                  <label for="Day2" class="">Day2</label>
                  <select name="day2" id="day" class="form-control">
                    <option value="">None</option>
                    <option value="Monday"    
                            <?php
                                  if("Monday" == $ssid['day2'])
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                              >MON</option>
                    <option value="Tuesday" <?php
                                  if("Tuesday"== ($ssid['day2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> >TUE</option>
                    <option value="Wednesday"<?php
                                  if("Wednesday" == ($ssid['day2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>WED</option>
                    <option value="Thursday"<?php
                                  if("Thursday" == ($ssid['day2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>THU</option>
                    <option value="Friday"<?php
                                  if("Friday" == ($ssid['day2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>FRI</option>
                    <option value="Saturday"<?php
                                  if("Saturday" == ($ssid['day2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>SAT</option>
                    <option value="TBA"<?php
                              if("TBA" == ($ssid['day2']))
                              { 
                                  echo "selected=selected"; 
                              }
                              ?>>TBA</option>         
                      <?php foreach($viewSubjects as $key => $val){ ?>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Room2" class="">Room2</label>
                    <?php
                       $viewStudents = ROOM:: getallrooms(array("room_ID"=>"ASC"));
                    ?>
                        <select name="room_ID2" id="room_ID2" class="form-control">
                          <option value="">None</option>
                          <?php foreach($viewStudents as $key => $val){ ?>
                          <option value="<?php echo $val['room_ID']; ?>" 
                                  <?php
                                      if($val['room_ID']  == ($ssid['room_ID2']))
                                      { 
                                          echo "selected=selected"; 
                                      }
                                  ?> ><?php echo $val['room_NAME']; ?></option>
                          <?php } ?>
                        </select>
                </div>
                <div class="col-lg-3">
                  <label for="Start2" class="">Start2</label>
                  <select name="start2" id="start2" class="form-control">
                    <option value="">None</option>
                    <option  <?php
                                  if("TBA"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> value="TBA">TBA</option> 
                    <option value="7:00 AM"
                            <?php
                                  if("7:00 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 AM</option>
                    <option value="7:15 AM"
                            <?php
                                  if("7:15 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 AM</option>          
                    <option value="7:30 AM"
                            <?php
                                  if("7:30 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:30 AM</option>
                    <option value="7:45 AM"
                            <?php
                                  if("7:45 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:45 AM</option>        
                    <option value="8:00 AM"
                            <?php
                                  if("8:00 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:00 AM</option>
                    <option value="8:15 AM"
                            <?php
                                  if("8:15 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:15 AM</option>        
                    <option value="8:30 AM"
                            <?php
                                  if("8:30 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:30 AM</option>
                    <option value="8:45 AM"
                            <?php
                                  if("8:45 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:45 AM</option>        
                    <option value="9:00 AM" 
                            <?php
                                  if("9:00 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 AM</option>
                    <option value="9:15 AM" 
                            <?php
                                  if("9:15 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 AM</option>
                    <option value="9:30 AM"
                            <?php
                                  if("9:30 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:30 AM</option>
                    <option value="9:45 AM"
                            <?php
                                  if("9:45 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:45 AM</option>
                    <option value="10:00 AM"
                            <?php
                                  if("10:00 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:00 AM</option>
                    <option value="10:15 AM"
                            <?php
                                  if("10:15 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:15 AM</option>
                    <option value="10:30 AM" 
                            <?php
                                  if("10:30 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 AM</option>
                    <option value="10:45 AM" 
                            <?php
                                  if("10:45 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 AM</option>
                    <option value="11:00 AM" <?php
                                  if("11:00 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 AM</option>
                    <option value="11:15 AM" <?php
                                  if("11:15 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:15 AM</option>
                    <option value="11:30 AM" <?php
                                  if("11:30 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:30 AM</option>
                    <option value="11:45 AM" <?php
                                  if("11:45 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:45 AM</option>
                    <option value="12:00 PM"<?php
                                  if("12:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:00 PM</option>
                    <option value="12:15 PM"<?php
                                  if("12:15 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:15 PM</option>
                    <option value="12:30 PM"
                            <?php
                                  if("12:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:30 PM</option>
                    <option value="12:45 PM"
                            <?php
                                  if("12:45 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:45 PM</option>
                    <option value="1:00 PM" <?php
                                  if("1:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:00 PM</option>
                    <option value="1:15 PM" <?php
                                  if("1:15 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:15 PM</option>
                    <option value="1:30 PM" <?php
                                  if("1:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:30 PM</option>
                    <option value="1:45 PM" <?php
                                  if("1:45 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:45 PM</option>
                    <option value="2:00 PM" <?php
                                  if("2:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:00 PM</option>
                    <option value="2:15 PM" <?php
                                  if("2:15 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:15 PM</option>
                    <option value="2:30 PM" <?php
                                  if("2:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:30 PM</option>
                    <option value="2:45 PM" <?php
                                  if("2:45 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:45 PM</option>
                    <option value="3:00 PM" <?php
                                  if("3:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:00 PM</option>
                    <option value="3:15 PM" <?php
                                  if("3:15 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:15 PM</option>
                    <option value="3:30 PM" <?php
                                  if("3:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:30 PM</option>
                    <option value="3:45 PM" <?php
                                  if("3:45 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:45 PM</option>
                    <option value="4:00 PM" <?php
                                  if("4:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:00 PM</option>
                    <option value="4:15 PM" <?php
                                  if("4:15 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:15 PM</option>
                    <option value="4:30 PM" <?php
                                  if("4:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:30 PM</option>
                    <option value="4:45 PM" <?php
                                  if("4:45 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:45 PM</option>
                    <option value="5:00 PM" <?php
                                  if("5:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:00 PM</option>
                    <option value="5:15 PM" <?php
                                  if("5:15 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:15 PM</option>
                    <option value="5:30 PM" <?php
                                  if("5:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:30 PM</option>
                    <option value="5:45 PM" <?php
                                  if("5:45 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:45 PM</option>
                    <option value="6:00 PM"
                            <?php
                                  if("6:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:00 PM</option>
                    <option value="6:15 PM"
                            <?php
                                  if("6:15 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:15 PM</option>
                    <option value="6:30 PM" <?php
                                  if("6:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:30 PM</option>
                    <option value="6:45 PM"
                            <?php
                                  if("6:45 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:45 PM</option>
                    <option value="7:00 PM" <?php
                                  if("7:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 PM</option>
                    <option value="7:15 PM" <?php
                                  if("7:15 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 PM</option>
                    <option value="7:30 PM" <?php
                                  if("7:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:30 PM</option> 
                    <option value="7:45 PM" <?php
                                  if("7:45 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:45 PM</option>
                    <option value="8:00 PM" <?php
                                  if("8:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:00 PM</option>       
                    <option value="8:15 PM" <?php
                                  if("8:15 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:15 PM</option> 
                    <option value="8:30 PM" <?php
                                  if("8:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:30 PM</option> 
                    <option value="8:45 PM" <?php
                                  if("8:45 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:45 PM</option> 
                    <option value="9:00 PM" <?php
                                  if("9:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 PM</option>          
                    <option value="9:15 PM" <?php
                                  if("9:15 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 PM</option> 
                    <option value="9:30 PM" <?php
                                  if("9:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:30 PM</option> 
                    <option value="9:45 PM" <?php
                                  if("9:45 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:45 PM</option> 
                    <option value="10:00 PM" <?php
                                  if("10:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:00 PM</option>  
                    <option value="10:15 PM" <?php
                                  if("10:15 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:15 PM</option>
                    <option value="10:30 PM" <?php
                                  if("10:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 PM</option> 
                    <option value="10:45 PM" <?php
                                  if("10:45 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 PM</option> 
                    <option value="11:00 PM" <?php
                                  if("11:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 PM</option>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="End2" class="">End2</label>
                  <select name="end2" id="end2" class="form-control">
                    <option value="">None</option>
                    <option  <?php
                                  if("TBA"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> value="TBA">TBA</option> 
                    <option value="7:00 AM"
                            <?php
                                  if("7:00 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 AM</option>
                    <option value="7:15 AM"
                            <?php
                                  if("7:15 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 AM</option>          
                    <option value="7:30 AM"
                            <?php
                                  if("7:30 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:30 AM</option>
                    <option value="7:45 AM"
                            <?php
                                  if("7:45 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:45 AM</option>        
                    <option value="8:00 AM"
                            <?php
                                  if("8:00 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:00 AM</option>
                    <option value="8:15 AM"
                            <?php
                                  if("8:15 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:15 AM</option>        
                    <option value="8:30 AM"
                            <?php
                                  if("8:30 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:30 AM</option>
                    <option value="8:45 AM"
                            <?php
                                  if("8:45 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:45 AM</option>        
                    <option value="9:00 AM" 
                            <?php
                                  if("9:00 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 AM</option>
                    <option value="9:15 AM" 
                            <?php
                                  if("9:15 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 AM</option>
                    <option value="9:30 AM"
                            <?php
                                  if("9:30 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:30 AM</option>
                    <option value="9:45 AM"
                            <?php
                                  if("9:45 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:45 AM</option>
                    <option value="10:00 AM"
                            <?php
                                  if("10:00 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:00 AM</option>
                    <option value="10:15 AM"
                            <?php
                                  if("10:15 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:15 AM</option>
                    <option value="10:30 AM" 
                            <?php
                                  if("10:30 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 AM</option>
                    <option value="10:45 AM" 
                            <?php
                                  if("10:45 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 AM</option>
                    <option value="11:00 AM" <?php
                                  if("11:00 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 AM</option>
                    <option value="11:15 AM" <?php
                                  if("11:15 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:15 AM</option>
                    <option value="11:30 AM" <?php
                                  if("11:30 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:30 AM</option>
                    <option value="11:45 AM" <?php
                                  if("11:45 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:45 AM</option>
                    <option value="12:00 PM"<?php
                                  if("12:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:00 PM</option>
                    <option value="12:15 PM"<?php
                                  if("12:15 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:15 PM</option>
                    <option value="12:30 PM"
                            <?php
                                  if("12:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:30 PM</option>
                    <option value="12:45 PM"
                            <?php
                                  if("12:45 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:45 PM</option>
                    <option value="1:00 PM" <?php
                                  if("1:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:00 PM</option>
                    <option value="1:15 PM" <?php
                                  if("1:15 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:15 PM</option>
                    <option value="1:30 PM" <?php
                                  if("1:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:30 PM</option>
                    <option value="1:45 PM" <?php
                                  if("1:45 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:45 PM</option>
                    <option value="2:00 PM" <?php
                                  if("2:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:00 PM</option>
                    <option value="2:15 PM" <?php
                                  if("2:15 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:15 PM</option>
                    <option value="2:30 PM" <?php
                                  if("2:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:30 PM</option>
                    <option value="2:45 PM" <?php
                                  if("2:45 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:45 PM</option>
                    <option value="3:00 PM" <?php
                                  if("3:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:00 PM</option>
                    <option value="3:15 PM" <?php
                                  if("3:15 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:15 PM</option>
                    <option value="3:30 PM" <?php
                                  if("3:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:30 PM</option>
                    <option value="3:45 PM" <?php
                                  if("3:45 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:45 PM</option>
                    <option value="4:00 PM" <?php
                                  if("4:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:00 PM</option>
                    <option value="4:15 PM" <?php
                                  if("4:15 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:15 PM</option>
                    <option value="4:30 PM" <?php
                                  if("4:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:30 PM</option>
                    <option value="4:45 PM" <?php
                                  if("4:45 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:45 PM</option>
                    <option value="5:00 PM" <?php
                                  if("5:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:00 PM</option>
                    <option value="5:15 PM" <?php
                                  if("5:15 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:15 PM</option>
                    <option value="5:30 PM" <?php
                                  if("5:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:30 PM</option>
                    <option value="5:45 PM" <?php
                                  if("5:45 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:45 PM</option>
                    <option value="6:00 PM"
                            <?php
                                  if("6:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:00 PM</option>
                    <option value="6:15 PM"
                            <?php
                                  if("6:15 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:15 PM</option>
                    <option value="6:30 PM" <?php
                                  if("6:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:30 PM</option>
                    <option value="6:45 PM"
                            <?php
                                  if("6:45 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:45 PM</option>
                    <option value="7:00 PM" <?php
                                  if("7:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 PM</option>
                    <option value="7:15 PM" <?php
                                  if("7:15 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 PM</option>
                    <option value="7:30 PM" <?php
                                  if("7:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:30 PM</option> 
                    <option value="7:45 PM" <?php
                                  if("7:45 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:45 PM</option>
                    <option value="8:00 PM" <?php
                                  if("8:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:00 PM</option>       
                    <option value="8:15 PM" <?php
                                  if("8:15 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:15 PM</option> 
                    <option value="8:30 PM" <?php
                                  if("8:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:30 PM</option> 
                    <option value="8:45 PM" <?php
                                  if("8:45 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:45 PM</option> 
                    <option value="9:00 PM" <?php
                                  if("9:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 PM</option>          
                    <option value="9:15 PM" <?php
                                  if("9:15 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 PM</option> 
                    <option value="9:30 PM" <?php
                                  if("9:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:30 PM</option> 
                    <option value="9:45 PM" <?php
                                  if("9:45 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:45 PM</option> 
                    <option value="10:00 PM" <?php
                                  if("10:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:00 PM</option>  
                    <option value="10:15 PM" <?php
                                  if("10:15 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:15 PM</option>
                    <option value="10:30 PM" <?php
                                  if("10:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 PM</option> 
                    <option value="10:45 PM" <?php
                                  if("10:45 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 PM</option> 
                    <option value="11:00 PM" <?php
                                  if("11:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 PM</option>
                  </select>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-lg-3">
                  <label for="Day3" class="">Day3</label>
                  <select name="day3" id="day" class="form-control">
                    <option value="">None</option>
                    <option value="Monday"    
                            <?php
                                  if("Monday" == $ssid['day3'])
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                              >MON</option>
                    <option value="Tuesday" <?php
                                  if("Tuesday"== ($ssid['day3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> >TUE</option>
                    <option value="Wednesday"<?php
                                  if("Wednesday" == ($ssid['day3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>WED</option>
                    <option value="Thursday"<?php
                                  if("Thursday" == ($ssid['day3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>THU</option>
                    <option value="Friday"<?php
                                  if("Friday" == ($ssid['day3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>FRI</option>
                    <option value="Saturday"<?php
                                  if("Saturday" == ($ssid['day3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>SAT</option>
                    <option value="TBA"<?php
                              if("TBA" == ($ssid['day3']))
                              { 
                                  echo "selected=selected"; 
                              }
                              ?>>TBA</option>         
                      <?php foreach($viewSubjects as $key => $val){ ?>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Room2" class="">Room3</label>
                    <?php
                       $viewStudents = ROOM:: getallrooms(array("room_ID"=>"ASC"));
                    ?>
                        <select name="room_ID3" id="room_ID2" class="form-control">
                          <option value="">None</option>
                          <?php foreach($viewStudents as $key => $val){ ?>
                          <option value="<?php echo $val['room_ID']; ?>" 
                                  <?php
                                      if($val['room_ID']  == ($ssid['room_ID3']))
                                      { 
                                          echo "selected=selected"; 
                                      }
                                  ?> ><?php echo $val['room_NAME']; ?></option>
                          <?php } ?>
                        </select>
                </div>
                <div class="col-lg-3">
                  <label for="Start3" class="">Start3</label>
                  <select name="start3" id="start3" class="form-control">
                    <option value="">None</option>
                    <option  <?php
                                  if("TBA"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> value="TBA">TBA</option> 
                    <option value="7:00 AM"
                            <?php
                                  if("7:00 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 AM</option>
                    <option value="7:15 AM"
                            <?php
                                  if("7:15 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 AM</option>          
                    <option value="7:30 AM"
                            <?php
                                  if("7:30 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:30 AM</option>
                    <option value="7:45 AM"
                            <?php
                                  if("7:45 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:45 AM</option>        
                    <option value="8:00 AM"
                            <?php
                                  if("8:00 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:00 AM</option>
                    <option value="8:15 AM"
                            <?php
                                  if("8:15 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:15 AM</option>        
                    <option value="8:30 AM"
                            <?php
                                  if("8:30 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:30 AM</option>
                    <option value="8:45 AM"
                            <?php
                                  if("8:45 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:45 AM</option>        
                    <option value="9:00 AM" 
                            <?php
                                  if("9:00 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 AM</option>
                    <option value="9:15 AM" 
                            <?php
                                  if("9:15 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 AM</option>
                    <option value="9:30 AM"
                            <?php
                                  if("9:30 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:30 AM</option>
                    <option value="9:45 AM"
                            <?php
                                  if("9:45 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:45 AM</option>
                    <option value="10:00 AM"
                            <?php
                                  if("10:00 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:00 AM</option>
                    <option value="10:15 AM"
                            <?php
                                  if("10:15 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:15 AM</option>
                    <option value="10:30 AM" 
                            <?php
                                  if("10:30 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 AM</option>
                    <option value="10:45 AM" 
                            <?php
                                  if("10:45 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 AM</option>
                    <option value="11:00 AM" <?php
                                  if("11:00 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 AM</option>
                    <option value="11:15 AM" <?php
                                  if("11:15 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:15 AM</option>
                    <option value="11:30 AM" <?php
                                  if("11:30 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:30 AM</option>
                    <option value="11:45 AM" <?php
                                  if("11:45 AM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:45 AM</option>
                    <option value="12:00 PM"<?php
                                  if("12:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:00 PM</option>
                    <option value="12:15 PM"<?php
                                  if("12:15 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:15 PM</option>
                    <option value="12:30 PM"
                            <?php
                                  if("12:30 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:30 PM</option>
                    <option value="12:45 PM"
                            <?php
                                  if("12:45 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:45 PM</option>
                    <option value="1:00 PM" <?php
                                  if("1:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:00 PM</option>
                    <option value="1:15 PM" <?php
                                  if("1:15 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:15 PM</option>
                    <option value="1:30 PM" <?php
                                  if("1:30 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:30 PM</option>
                    <option value="1:45 PM" <?php
                                  if("1:45 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:45 PM</option>
                    <option value="2:00 PM" <?php
                                  if("2:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:00 PM</option>
                    <option value="2:15 PM" <?php
                                  if("2:15 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:15 PM</option>
                    <option value="2:30 PM" <?php
                                  if("2:30 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:30 PM</option>
                    <option value="2:45 PM" <?php
                                  if("2:45 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:45 PM</option>
                    <option value="3:00 PM" <?php
                                  if("3:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:00 PM</option>
                    <option value="3:15 PM" <?php
                                  if("3:15 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:15 PM</option>
                    <option value="3:30 PM" <?php
                                  if("3:30 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:30 PM</option>
                    <option value="3:45 PM" <?php
                                  if("3:45 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:45 PM</option>
                    <option value="4:00 PM" <?php
                                  if("4:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:00 PM</option>
                    <option value="4:15 PM" <?php
                                  if("4:15 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:15 PM</option>
                    <option value="4:30 PM" <?php
                                  if("4:30 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:30 PM</option>
                    <option value="4:45 PM" <?php
                                  if("4:45 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:45 PM</option>
                    <option value="5:00 PM" <?php
                                  if("5:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:00 PM</option>
                    <option value="5:15 PM" <?php
                                  if("5:15 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:15 PM</option>
                    <option value="5:30 PM" <?php
                                  if("5:30 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:30 PM</option>
                    <option value="5:45 PM" <?php
                                  if("5:45 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:45 PM</option>
                    <option value="6:00 PM"
                            <?php
                                  if("6:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:00 PM</option>
                    <option value="6:15 PM"
                            <?php
                                  if("6:15 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:15 PM</option>
                    <option value="6:30 PM" <?php
                                  if("6:30 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:30 PM</option>
                    <option value="6:45 PM"
                            <?php
                                  if("6:45 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:45 PM</option>
                    <option value="7:00 PM" <?php
                                  if("7:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 PM</option>
                    <option value="7:15 PM" <?php
                                  if("7:15 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 PM</option>
                    <option value="7:30 PM" <?php
                                  if("7:30 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:30 PM</option> 
                    <option value="7:45 PM" <?php
                                  if("7:45 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:45 PM</option>
                    <option value="8:00 PM" <?php
                                  if("8:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:00 PM</option>       
                    <option value="8:15 PM" <?php
                                  if("8:15 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:15 PM</option> 
                    <option value="8:30 PM" <?php
                                  if("8:30 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:30 PM</option> 
                    <option value="8:45 PM" <?php
                                  if("8:45 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:45 PM</option> 
                    <option value="9:00 PM" <?php
                                  if("9:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 PM</option>          
                    <option value="9:15 PM" <?php
                                  if("9:15 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 PM</option> 
                    <option value="9:30 PM" <?php
                                  if("9:30 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:30 PM</option> 
                    <option value="9:45 PM" <?php
                                  if("9:45 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:45 PM</option> 
                    <option value="10:00 PM" <?php
                                  if("10:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:00 PM</option>  
                    <option value="10:15 PM" <?php
                                  if("10:15 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:15 PM</option>
                    <option value="10:30 PM" <?php
                                  if("10:30 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 PM</option> 
                    <option value="10:45 PM" <?php
                                  if("10:45 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 PM</option> 
                    <option value="11:00 PM" <?php
                                  if("11:00 PM"  == ($ssid['start3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 PM</option>  
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="End3" class="">End3</label>
                  <select name="end3" id="end3" class="form-control">
                    <option value="">None</option>
                    <option  <?php
                                  if("TBA"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> value="TBA">TBA</option> 
                    <option value="7:00 AM"
                            <?php
                                  if("7:00 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 AM</option>
                    <option value="7:15 AM"
                            <?php
                                  if("7:15 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 AM</option>          
                    <option value="7:30 AM"
                            <?php
                                  if("7:30 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:30 AM</option>
                    <option value="7:45 AM"
                            <?php
                                  if("7:45 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:45 AM</option>        
                    <option value="8:00 AM"
                            <?php
                                  if("8:00 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:00 AM</option>
                    <option value="8:15 AM"
                            <?php
                                  if("8:15 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:15 AM</option>        
                    <option value="8:30 AM"
                            <?php
                                  if("8:30 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:30 AM</option>
                    <option value="8:45 AM"
                            <?php
                                  if("8:45 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:45 AM</option>        
                    <option value="9:00 AM" 
                            <?php
                                  if("9:00 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 AM</option>
                    <option value="9:15 AM" 
                            <?php
                                  if("9:15 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 AM</option>
                    <option value="9:30 AM"
                            <?php
                                  if("9:30 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:30 AM</option>
                    <option value="9:45 AM"
                            <?php
                                  if("9:45 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:45 AM</option>
                    <option value="10:00 AM"
                            <?php
                                  if("10:00 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:00 AM</option>
                    <option value="10:15 AM"
                            <?php
                                  if("10:15 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:15 AM</option>
                    <option value="10:30 AM" 
                            <?php
                                  if("10:30 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 AM</option>
                    <option value="10:45 AM" 
                            <?php
                                  if("10:45 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 AM</option>
                    <option value="11:00 AM" <?php
                                  if("11:00 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 AM</option>
                    <option value="11:15 AM" <?php
                                  if("11:15 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:15 AM</option>
                    <option value="11:30 AM" <?php
                                  if("11:30 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:30 AM</option>
                    <option value="11:45 AM" <?php
                                  if("11:45 AM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:45 AM</option>
                    <option value="12:00 PM"<?php
                                  if("12:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:00 PM</option>
                    <option value="12:15 PM"<?php
                                  if("12:15 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:15 PM</option>
                    <option value="12:30 PM"
                            <?php
                                  if("12:30 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:30 PM</option>
                    <option value="12:45 PM"
                            <?php
                                  if("12:45 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:45 PM</option>
                    <option value="1:00 PM" <?php
                                  if("1:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:00 PM</option>
                    <option value="1:15 PM" <?php
                                  if("1:15 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:15 PM</option>
                    <option value="1:30 PM" <?php
                                  if("1:30 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:30 PM</option>
                    <option value="1:45 PM" <?php
                                  if("1:45 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:45 PM</option>
                    <option value="2:00 PM" <?php
                                  if("2:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:00 PM</option>
                    <option value="2:15 PM" <?php
                                  if("2:15 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:15 PM</option>
                    <option value="2:30 PM" <?php
                                  if("2:30 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:30 PM</option>
                    <option value="2:45 PM" <?php
                                  if("2:45 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:45 PM</option>
                    <option value="3:00 PM" <?php
                                  if("3:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:00 PM</option>
                    <option value="3:15 PM" <?php
                                  if("3:15 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:15 PM</option>
                    <option value="3:30 PM" <?php
                                  if("3:30 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:30 PM</option>
                    <option value="3:45 PM" <?php
                                  if("3:45 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:45 PM</option>
                    <option value="4:00 PM" <?php
                                  if("4:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:00 PM</option>
                    <option value="4:15 PM" <?php
                                  if("4:15 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:15 PM</option>
                    <option value="4:30 PM" <?php
                                  if("4:30 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:30 PM</option>
                    <option value="4:45 PM" <?php
                                  if("4:45 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:45 PM</option>
                    <option value="5:00 PM" <?php
                                  if("5:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:00 PM</option>
                    <option value="5:15 PM" <?php
                                  if("5:15 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:15 PM</option>
                    <option value="5:30 PM" <?php
                                  if("5:30 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:30 PM</option>
                    <option value="5:45 PM" <?php
                                  if("5:45 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:45 PM</option>
                    <option value="6:00 PM"
                            <?php
                                  if("6:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:00 PM</option>
                    <option value="6:15 PM"
                            <?php
                                  if("6:15 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:15 PM</option>
                    <option value="6:30 PM" <?php
                                  if("6:30 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:30 PM</option>
                    <option value="6:45 PM"
                            <?php
                                  if("6:45 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:45 PM</option>
                    <option value="7:00 PM" <?php
                                  if("7:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 PM</option>
                    <option value="7:15 PM" <?php
                                  if("7:15 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 PM</option>
                    <option value="7:30 PM" <?php
                                  if("7:30 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:30 PM</option> 
                    <option value="7:45 PM" <?php
                                  if("7:45 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:45 PM</option>
                    <option value="8:00 PM" <?php
                                  if("8:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:00 PM</option>       
                    <option value="8:15 PM" <?php
                                  if("8:15 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:15 PM</option> 
                    <option value="8:30 PM" <?php
                                  if("8:30 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:30 PM</option> 
                    <option value="8:45 PM" <?php
                                  if("8:45 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:45 PM</option> 
                    <option value="9:00 PM" <?php
                                  if("9:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 PM</option>          
                    <option value="9:15 PM" <?php
                                  if("9:15 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 PM</option> 
                    <option value="9:30 PM" <?php
                                  if("9:30 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:30 PM</option> 
                    <option value="9:45 PM" <?php
                                  if("9:45 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:45 PM</option> 
                    <option value="10:00 PM" <?php
                                  if("10:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:00 PM</option>  
                    <option value="10:15 PM" <?php
                                  if("10:15 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:15 PM</option>
                    <option value="10:30 PM" <?php
                                  if("10:30 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 PM</option> 
                    <option value="10:45 PM" <?php
                                  if("10:45 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 PM</option> 
                    <option value="11:00 PM" <?php
                                  if("11:00 PM"  == ($ssid['end3']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 PM</option>  
                  </select>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-lg-3">
                  <label for="Day4" class="">Day4</label>
                  <select name="day4" id="day" class="form-control">
                    <option value="">None</option>
                    <option value="Monday"    
                            <?php
                                  if("Monday" == $ssid['day4'])
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                              >MON</option>
                    <option value="Tuesday" <?php
                                  if("Tuesday"== ($ssid['day4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> >TUE</option>
                    <option value="Wednesday"<?php
                                  if("Wednesday" == ($ssid['day4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>WED</option>
                    <option value="Thursday"<?php
                                  if("Thursday" == ($ssid['day4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>THU</option>
                    <option value="Friday"<?php
                                  if("Friday" == ($ssid['day4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>FRI</option>
                    <option value="Saturday"<?php
                                  if("Saturday" == ($ssid['day4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>SAT</option>
                    <option value="TBA"<?php
                              if("TBA" == ($ssid['day4']))
                              { 
                                  echo "selected=selected"; 
                              }
                              ?>>TBA</option>         
                      <?php foreach($viewSubjects as $key => $val){ ?>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Room4" class="">Room4</label>
                    <?php
                       $viewStudents = ROOM:: getallrooms(array("room_ID"=>"ASC"));
                    ?>
                        <select name="room_ID4" id="room_ID4" class="form-control">
                          <option value="">None</option>
                          <?php foreach($viewStudents as $key => $val){ ?>
                          <option value="<?php echo $val['room_ID']; ?>" 
                                  <?php
                                      if($val['room_ID']  == ($ssid['room_ID4']))
                                      { 
                                          echo "selected=selected"; 
                                      }
                                  ?> ><?php echo $val['room_NAME']; ?></option>
                          <?php } ?>
                        </select>
                </div>
                <div class="col-lg-3">
                  <label for="Start4" class="">Start4</label>
                  <select name="start4" id="start4" class="form-control">
                    <option value="">None</option>
                    <option  <?php
                                  if("TBA"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> value="TBA">TBA</option> 
                    <option value="7:00 AM"
                            <?php
                                  if("7:00 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 AM</option>
                    <option value="7:15 AM"
                            <?php
                                  if("7:15 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 AM</option>          
                    <option value="7:30 AM"
                            <?php
                                  if("7:30 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:30 AM</option>
                    <option value="7:45 AM"
                            <?php
                                  if("7:45 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:45 AM</option>        
                    <option value="8:00 AM"
                            <?php
                                  if("8:00 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:00 AM</option>
                    <option value="8:15 AM"
                            <?php
                                  if("8:15 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:15 AM</option>        
                    <option value="8:30 AM"
                            <?php
                                  if("8:30 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:30 AM</option>
                    <option value="8:45 AM"
                            <?php
                                  if("8:45 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:45 AM</option>        
                    <option value="9:00 AM" 
                            <?php
                                  if("9:00 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 AM</option>
                    <option value="9:15 AM" 
                            <?php
                                  if("9:15 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 AM</option>
                    <option value="9:30 AM"
                            <?php
                                  if("9:30 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:30 AM</option>
                    <option value="9:45 AM"
                            <?php
                                  if("9:45 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:45 AM</option>
                    <option value="10:00 AM"
                            <?php
                                  if("10:00 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:00 AM</option>
                    <option value="10:15 AM"
                            <?php
                                  if("10:15 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:15 AM</option>
                    <option value="10:30 AM" 
                            <?php
                                  if("10:30 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 AM</option>
                    <option value="10:45 AM" 
                            <?php
                                  if("10:45 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 AM</option>
                    <option value="11:00 AM" <?php
                                  if("11:00 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 AM</option>
                    <option value="11:15 AM" <?php
                                  if("11:15 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:15 AM</option>
                    <option value="11:30 AM" <?php
                                  if("11:30 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:30 AM</option>
                    <option value="11:45 AM" <?php
                                  if("11:45 AM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:45 AM</option>
                    <option value="12:00 PM"<?php
                                  if("12:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:00 PM</option>
                    <option value="12:15 PM"<?php
                                  if("12:15 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:15 PM</option>
                    <option value="12:30 PM"
                            <?php
                                  if("12:30 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:30 PM</option>
                    <option value="12:45 PM"
                            <?php
                                  if("12:45 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:45 PM</option>
                    <option value="1:00 PM" <?php
                                  if("1:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:00 PM</option>
                    <option value="1:15 PM" <?php
                                  if("1:15 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:15 PM</option>
                    <option value="1:30 PM" <?php
                                  if("1:30 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:30 PM</option>
                    <option value="1:45 PM" <?php
                                  if("1:45 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:45 PM</option>
                    <option value="2:00 PM" <?php
                                  if("2:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:00 PM</option>
                    <option value="2:15 PM" <?php
                                  if("2:15 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:15 PM</option>
                    <option value="2:30 PM" <?php
                                  if("2:30 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:30 PM</option>
                    <option value="2:45 PM" <?php
                                  if("2:45 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:45 PM</option>
                    <option value="3:00 PM" <?php
                                  if("3:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:00 PM</option>
                    <option value="3:15 PM" <?php
                                  if("3:15 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:15 PM</option>
                    <option value="3:30 PM" <?php
                                  if("3:30 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:30 PM</option>
                    <option value="3:45 PM" <?php
                                  if("3:45 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:45 PM</option>
                    <option value="4:00 PM" <?php
                                  if("4:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:00 PM</option>
                    <option value="4:15 PM" <?php
                                  if("4:15 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:15 PM</option>
                    <option value="4:30 PM" <?php
                                  if("4:30 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:30 PM</option>
                    <option value="4:45 PM" <?php
                                  if("4:45 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:45 PM</option>
                    <option value="5:00 PM" <?php
                                  if("5:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:00 PM</option>
                    <option value="5:15 PM" <?php
                                  if("5:15 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:15 PM</option>
                    <option value="5:30 PM" <?php
                                  if("5:30 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:30 PM</option>
                    <option value="5:45 PM" <?php
                                  if("5:45 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:45 PM</option>
                    <option value="6:00 PM"
                            <?php
                                  if("6:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:00 PM</option>
                    <option value="6:15 PM"
                            <?php
                                  if("6:15 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:15 PM</option>
                    <option value="6:30 PM" <?php
                                  if("6:30 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:30 PM</option>
                    <option value="6:45 PM"
                            <?php
                                  if("6:45 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:45 PM</option>
                    <option value="7:00 PM" <?php
                                  if("7:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 PM</option>
                    <option value="7:15 PM" <?php
                                  if("7:15 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 PM</option>
                    <option value="7:30 PM" <?php
                                  if("7:30 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:30 PM</option> 
                    <option value="7:45 PM" <?php
                                  if("7:45 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:45 PM</option>
                    <option value="8:00 PM" <?php
                                  if("8:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:00 PM</option>       
                    <option value="8:15 PM" <?php
                                  if("8:15 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:15 PM</option> 
                    <option value="8:30 PM" <?php
                                  if("8:30 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:30 PM</option> 
                    <option value="8:45 PM" <?php
                                  if("8:45 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:45 PM</option> 
                    <option value="9:00 PM" <?php
                                  if("9:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 PM</option>          
                    <option value="9:15 PM" <?php
                                  if("9:15 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 PM</option> 
                    <option value="9:30 PM" <?php
                                  if("9:30 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:30 PM</option> 
                    <option value="9:45 PM" <?php
                                  if("9:45 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:45 PM</option> 
                    <option value="10:00 PM" <?php
                                  if("10:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:00 PM</option>  
                    <option value="10:15 PM" <?php
                                  if("10:15 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:15 PM</option>
                    <option value="10:30 PM" <?php
                                  if("10:30 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 PM</option> 
                    <option value="10:45 PM" <?php
                                  if("10:45 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 PM</option> 
                    <option value="11:00 PM" <?php
                                  if("11:00 PM"  == ($ssid['start4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 PM</option>  
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="End4" class="">End4</label>
                  <select name="end4" id="end4" class="form-control">
                    <option value="">None</option>
                    <option  <?php
                                  if("TBA"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> value="TBA">TBA</option> 
                    <option value="7:00 AM"
                            <?php
                                  if("7:00 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 AM</option>
                    <option value="7:15 AM"
                            <?php
                                  if("7:15 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 AM</option>          
                    <option value="7:30 AM"
                            <?php
                                  if("7:30 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:30 AM</option>
                    <option value="7:45 AM"
                            <?php
                                  if("7:45 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:45 AM</option>        
                    <option value="8:00 AM"
                            <?php
                                  if("8:00 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:00 AM</option>
                    <option value="8:15 AM"
                            <?php
                                  if("8:15 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:15 AM</option>        
                    <option value="8:30 AM"
                            <?php
                                  if("8:30 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:30 AM</option>
                    <option value="8:45 AM"
                            <?php
                                  if("8:45 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:45 AM</option>        
                    <option value="9:00 AM" 
                            <?php
                                  if("9:00 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 AM</option>
                    <option value="9:15 AM" 
                            <?php
                                  if("9:15 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 AM</option>
                    <option value="9:30 AM"
                            <?php
                                  if("9:30 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:30 AM</option>
                    <option value="9:45 AM"
                            <?php
                                  if("9:45 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:45 AM</option>
                    <option value="10:00 AM"
                            <?php
                                  if("10:00 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:00 AM</option>
                    <option value="10:15 AM"
                            <?php
                                  if("10:15 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:15 AM</option>
                    <option value="10:30 AM" 
                            <?php
                                  if("10:30 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 AM</option>
                    <option value="10:45 AM" 
                            <?php
                                  if("10:45 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 AM</option>
                    <option value="11:00 AM" <?php
                                  if("11:00 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 AM</option>
                    <option value="11:15 AM" <?php
                                  if("11:15 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:15 AM</option>
                    <option value="11:30 AM" <?php
                                  if("11:30 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:30 AM</option>
                    <option value="11:45 AM" <?php
                                  if("11:45 AM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:45 AM</option>
                    <option value="12:00 PM"<?php
                                  if("12:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:00 PM</option>
                    <option value="12:15 PM"<?php
                                  if("12:15 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:15 PM</option>
                    <option value="12:30 PM"
                            <?php
                                  if("12:30 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:30 PM</option>
                    <option value="12:45 PM"
                            <?php
                                  if("12:45 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:45 PM</option>
                    <option value="1:00 PM" <?php
                                  if("1:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:00 PM</option>
                    <option value="1:15 PM" <?php
                                  if("1:15 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:15 PM</option>
                    <option value="1:30 PM" <?php
                                  if("1:30 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:30 PM</option>
                    <option value="1:45 PM" <?php
                                  if("1:45 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:45 PM</option>
                    <option value="2:00 PM" <?php
                                  if("2:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:00 PM</option>
                    <option value="2:15 PM" <?php
                                  if("2:15 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:15 PM</option>
                    <option value="2:30 PM" <?php
                                  if("2:30 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:30 PM</option>
                    <option value="2:45 PM" <?php
                                  if("2:45 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:45 PM</option>
                    <option value="3:00 PM" <?php
                                  if("3:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:00 PM</option>
                    <option value="3:15 PM" <?php
                                  if("3:15 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:15 PM</option>
                    <option value="3:30 PM" <?php
                                  if("3:30 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:30 PM</option>
                    <option value="3:45 PM" <?php
                                  if("3:45 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:45 PM</option>
                    <option value="4:00 PM" <?php
                                  if("4:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:00 PM</option>
                    <option value="4:15 PM" <?php
                                  if("4:15 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:15 PM</option>
                    <option value="4:30 PM" <?php
                                  if("4:30 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:30 PM</option>
                    <option value="4:45 PM" <?php
                                  if("4:45 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:45 PM</option>
                    <option value="5:00 PM" <?php
                                  if("5:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:00 PM</option>
                    <option value="5:15 PM" <?php
                                  if("5:15 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:15 PM</option>
                    <option value="5:30 PM" <?php
                                  if("5:30 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:30 PM</option>
                    <option value="5:45 PM" <?php
                                  if("5:45 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:45 PM</option>
                    <option value="6:00 PM"
                            <?php
                                  if("6:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:00 PM</option>
                    <option value="6:15 PM"
                            <?php
                                  if("6:15 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:15 PM</option>
                    <option value="6:30 PM" <?php
                                  if("6:30 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:30 PM</option>
                    <option value="6:45 PM"
                            <?php
                                  if("6:45 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:45 PM</option>
                    <option value="7:00 PM" <?php
                                  if("7:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 PM</option>
                    <option value="7:15 PM" <?php
                                  if("7:15 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:15 PM</option>
                    <option value="7:30 PM" <?php
                                  if("7:30 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:30 PM</option> 
                    <option value="7:45 PM" <?php
                                  if("7:45 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:45 PM</option>
                    <option value="8:00 PM" <?php
                                  if("8:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:00 PM</option>       
                    <option value="8:15 PM" <?php
                                  if("8:15 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:15 PM</option> 
                    <option value="8:30 PM" <?php
                                  if("8:30 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:30 PM</option> 
                    <option value="8:45 PM" <?php
                                  if("8:45 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:45 PM</option> 
                    <option value="9:00 PM" <?php
                                  if("9:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 PM</option>          
                    <option value="9:15 PM" <?php
                                  if("9:15 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:15 PM</option> 
                    <option value="9:30 PM" <?php
                                  if("9:30 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:30 PM</option> 
                    <option value="9:45 PM" <?php
                                  if("9:45 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:45 PM</option> 
                    <option value="10:00 PM" <?php
                                  if("10:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:00 PM</option>  
                    <option value="10:15 PM" <?php
                                  if("10:15 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:15 PM</option>
                    <option value="10:30 PM" <?php
                                  if("10:30 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 PM</option> 
                    <option value="10:45 PM" <?php
                                  if("10:45 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:45 PM</option> 
                    <option value="11:00 PM" <?php
                                  if("11:00 PM"  == ($ssid['end4']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 PM</option>  
                  </select>
                </div>
              </div>


              <br>  
              <div class="row">
                <div class="col-lg-6">
                  <label for="Instructor" class="">Instructor </label>
                    <?php
                      $viewIntructors = INSTRUCTORS:: getAllInstructorsbyStatus(array("instructor_ID"=>"ASC"));
                    ?>
                        <select name="instructor_ID" id="instructor_ID" class="form-control">
                       <option value="">None</option>
                          <?php foreach($viewIntructors as $key => $val){ ?>
                          <option value="<?php echo $val['instructor_ID']; ?>" <?php
                                      if( $val['instructor_ID'] == ($ssid['instructor_ID']))
                                      { 
                                          echo "selected=selected"; 
                                      }
                                  ?>
                                  ><?php echo $val['instructor_NAME']." ".$val['instructor_LNAME']; ?></option>
                          <?php } ?>
                        </select>
                </div>
                <div class="col-lg-6">
                  <label for="max_student" class="">Max Student</label>
                 <!--  <input class="form-control" name="max_student" type="number" id="max_student" value="30" /> -->
                  <input class="form-control" name="max_student" type="number" id="max_student" value="<?= $ssid['max_student'];?>" />
                   <input type="hidden" name =subject_sched_ID value="<?= $ssid['subject_sched_ID'];?>">

                  <input type="hidden" name =beforesemester_section_ID value="<?= $ssid['semester_section_ID'];?>">
                  <input type="hidden" name =beforesubject_ID value="<?= $ssid['subject_ID'];?>">
                  <input type="hidden" name =beforeday value="<?= $ssid['day'];?>">
                  <input type="hidden" name =beforeroom_ID value="<?= $ssid['room_ID'];?>">
                  <input type="hidden" name =beforestart value="<?= $ssid['start'];?>">
                  <input type="hidden" name =beforeend value="<?= $ssid['end'];?>">
                  <input type="hidden" name =beforeday2 value="<?= $ssid['day2'];?>">
                  <input type="hidden" name =beforeroom_ID2 value="<?= $ssid['room_ID2'];?>">
                  <input type="hidden" name =beforestart2 value="<?= $ssid['start2'];?>">
                  <input type="hidden" name =beforeend2 value="<?= $ssid['end2'];?>">
                  <input type="hidden" name =beforeinstructor_ID value="<?= $ssid['instructor_ID'];?>">
                  <input type="hidden" name =beforemax_student value="<?= $ssid['max_student'];?>">

                  <input type="hidden" name =beforeday3 value="<?= $ssid['day3'];?>">
                  <input type="hidden" name =beforeroom_ID3 value="<?= $ssid['room_ID3'];?>">
                  <input type="hidden" name =beforestart3 value="<?= $ssid['start3'];?>">
                  <input type="hidden" name =beforeend3 value="<?= $ssid['end3'];?>"> 
                </div>
              </div> 
              <div class="row ">
                <div class="col-lg-12 ">
                  <div class="row">
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-12"><br/>
                      <button id="button" class="btn btn-success btn-block" type="submit" onclick="return confirm('Do You Want Save ?')"><i class="fa fa-edit"></i> Save
                      </button>
                    </div>
                    <div class="col-lg-1">
                    </div>
                  </div>
                </div>
              </div>
          </form>
    </div>
  </div>
</div>
</div>
    
<?php break; 
case "delete": ?>

<?php break;
    default: header("location:?action=add");
    }
?>
<script src="<?php echo HOME; ?>old/assets/js/custom/mmma-masterAccess.js"></script>
<script src="<?php echo HOME; ?>old/assets/js/custom/mmma-conflictChecker.js"></script>
</body>
</html>
