<?php
include('init.php');
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Academic Affairs / Section Status Listing, DESCRIPTION: User visited Section Status Listing";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

header("Location: sectioning.php?action=search");

?>