<?php 
//INITIALIZE INCLUDES
    include('init.php');
#SESSION::CheckLogin();

    //QUERIES
    if(Request::post()){
        switch(Request::get("action")) {
            case "add": 
			
			#var_dump(Request::post());
			
            USERS::addstudent(Request::post());
            
			
			#var_dump(Request::post());
            
            break;
            case "edit": USERS::update_student($_GET['id'],Request::post()); break;
            #case "update_blog":  BLOG::update(Request::post("blog_info")); break;
            
            #case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
            
            default: Fail::not_found();
        }
    }
    
    if(Request::get()){
        switch(Request::get("action")) {
            case "delete": ANNOUNCEMENT::delete($_GET['id']); break;
        
        }
    }       
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
      <title>HOME | SJB CAINTA SIMS</title>
    </head>

    <body>

        <!-- top nav -->
        <?php include(LAYOUTS . 'top_nav.php'); ?>
        <!-- end nav -->

        <div class="container">
            
            <!-- banner -->
            <?php include(LAYOUTS . "banner.php"); ?>
            <!-- end banner -->
            
          
            
            
            <!-- alert messages -->
            <?php SESSION::DisplayMsg(); ?>
            <!-- end of alert messages -->
            
            <!-- start row container -->
            <!-- 
            <div class="row">
                
                <div class="col-lg-12">

                
                    <ul class="breadcrumb">
                        <li class="active">Home</li>
                    </ul>
                

                </div>
                
            </div>
            -->
            <!-- end row container -->
            
   
             
            <!-- row container -->
            <div class="row">
			<!-- left nav -->

      	
                <div class="col-md-2">
					<?php include(PAGES."navigation.php");?>
                </div>
				
				<!-- end left nav -->
                
                <!-- BODY -->
                
                
                <div class="col-md-10">
                <?php if (SESSION::isLoggedIn()) { ?>
                    <?php
                    switch($_GET['action']){
                         case "view":
                    ?>
                       
                    
                    
							<div class="panel panel-default">
								<div class="panel-heading">
									<table>
                                		<tr>
                                			<td width="50%">
												<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> View Transaction Logs</h3>											</td>

										 	<script>
                                    				function term(value){
                                            			$.post("search.php",{search_term:value}, function(data){
                                                		$("#display_result").html(data).hide().fadeIn();   
                                                		$("#display_hide").hide();
                                                			if(value==""){
                                                 				$("#display_hide").show();
                                                			}
                                            			});
                                        			}
                                    			</script>
                               			</tr>
                                	</table>
							  </div>

								<div class="panel-body">
									<div id="display_result"></div>
                                	<div id="display_hide">
									<table class="table table-hover table-responsive table-striped text-md">
										 <th  style="width:12%;">Student ID</th>
                                        <th  style="width:25%;">Name</th>
                                        <th style="width:5%;">Course</th>
                                        
                                        <th style="width:8%;">OR #</th>
                                        <th style="width:10%;">Amount</th>
                                        <th style="width:15%;">Date</th>
                                        <th style="width:;">Note</th>
                                        <?php
										 //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
										$viewStudents = ACCT:: getallpayments(array("payment_ID"=>"DESC"));
										 if(count($viewStudents)>=20) 
										 {
											 $pagination2 = new Pagination($viewStudents, 20, Request::get("p"));
										 }else{
											 $pagination2 = new Pagination($viewStudents, 20, NULL);
										 }

										 $viewStudents = $pagination2->get_array();

										 if($viewStudents) {

											foreach($viewStudents as $key => $value){
											
											
											$user = USERS::getSingle(array("si_ID"=>"ASC"),$value['si_ID']);
										?>
											<tr>
												<td>
													<?php echo strtoupper($user['student_ID']); ?>
												<td>
													<a href="accounting.php?action=view&id=<?= $value['si_ID']; ?>" title="View Account"><?php echo $user['si_LNAME']; ?>, <?php echo $user['si_FNAME']; ?></a>
												<td><?php echo USERS::getCourse($value['si_ID']); ?>
												
											  <td><?php echo $value['payment_OR']; ?></td>
                                              <td>
                                              <?php 
                                             // echo printf("%.2f",$value['payment_AMOUNT']);
                                              echo number_format($value['payment_AMOUNT'],2, '.', ',');
											  ?>
											  </td>
                                              <td><?php echo date("M-j-Y", strtotime($value['payment_DATE'])); ?></td>
                                             
                                             
											  
                                              <td><?php echo $value['payment_NOTE']; ?></td>
                                              											</tr>
											<?php
											 } 
											}
										?>
									</table> 

								</div>
							</div>
								<center>
									<?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
								</center>
							</div>
                        </div>
                    </div>
                  <?php  break;
						
						
						case "reports":
						?>
 
							<div class="panel panel-default">
								<div class="panel-heading">
									
                                    <form action="" method="GET">
                                    <table border="0" width="100%">
                                		<tr>
                                			<td width="" style="width:40%">
												<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> View Reports</h3>											</td>

                                                <script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
												<script>
                                                webshims.setOptions('forms-ext', {types: 'date'});
                                                webshims.polyfill('forms forms-ext');
                                                </script>
                                              
                                                
                                                
                                              <td>Start: <input id="start" type="date" value="<?php if(isset($_GET['start'])){ echo $_GET['start']; } ?>" name="start" /></td>  
                                              <td>End: <input id="end" type="date" value="<?php if(isset($_GET['end'])){ echo $_GET['end']; } ?>" name="end" /></td>
                                              <td><input type="submit" value="Go" /></td>
                                             <input type="hidden" name="action" value="reports" />   
                                             
                                             
                                             
                                             <script>
											 
											 $(document).ready(function() {                                       
												/*$("#start").on("input", function() {
												  $("#end").val($(this).val());
												  alert($(this).val());
												})*/
											 });
                                             </script>
                                             
                                             
                               			</tr>
                                	</table>
                                    
                                    </form>
							  </div>

								<div class="panel-body">
									<div id="display_result"></div>
                                	<div id="display_hide">
									<!-- <table class="table table-hover table-responsive table-striped text-md">
										 <tr>
                                         <th  style="width:3%;">#</th>
                                         <th  style="width:12%;">Student ID</th>
                                           
                                           <th  style="width:25%;">Name</th>
                                        <th style="width:5%;">Course</th>
										<th style="width:8%;">Particulars</th>
                                        <th style="width:8%;">OR #</th>
                                        <th style="width:15%;">Amount</th>
                                        <th style="width:15%;">Date</th>
                                        <th style="width:;">Note</th> -->
                                        
                               	Export: <button class="btnCSV">CSV</button>
                               	<button class="btnXLS">XLS</button>
                                    <table class="table table-hover table-responsive table-striped text-md" id="transactionLogs">
									


                                   <thead >
                                        <th >#</th>
                                     	<th >Student ID</th>
                                       	<th >Name</th>
                                        <th >Course</th>
										<th >Particulars</th>
                                        <th >OR #</th>
                                        <th >Amount</th>
                                        <th >Date</th>
                                        <th >Note</th>
                                    </thead>
                                    <thead id="drp">
                                        <th >#</th>
                                     	<th >Student ID</th>
                                       	<th >Name</th>
                                        <th >Course</th>
										<th >Particulars</th>
                                        <th >OR #</th>
                                        <th >Amount</th>
                                        <th >Date</th>
                                        <th >Note</th>
                                    </thead>
                                       
                                    <tbody>


										<?php
										if(isset($_GET['start']) && isset($_GET['end']))
										{
										$start = date("Y-m-d H:i:s",strtotime($_GET['start']));
										#$end = date("Y-m-d H:i:s",strtotime($_GET['end']));
										$end = $_GET['end'];
										$viewStudents = ACCT:: getallpaymentsbydate(array("payment_ID"=>"DESC"),$start,$end);
										
										
										 
										}else{
										 $viewStudents = NULL;
										}
										
										 if($viewStudents) {
											$x=1;
											$total_amnt = 0;
											
											foreach($viewStudents as $key => $value){
											
											
											$user = USERS::getSingle(array("si_ID"=>"ASC"),$value['si_ID']);
												?>
											<tr>
                                            	<td><?= $x++; ?></td>
												<td>
													<?php echo strtoupper($user['student_ID']); ?>
												                                               
												<td>
										<?php echo $user['si_LNAME']; ?>, <?php echo $user['si_FNAME']; ?>
												</td>
												
												<td><?php echo USERS::getCourse($value['si_ID']); ?></td>
												<td>  <?php
												$p = PARTICULARS::getSingleParticular($value['particular_ID']);
												
												echo $p['particular_NAME'];
												
												?></td>
											  <td><?php echo $value['payment_OR']; ?></td>
                                              <td>
                                              <?php 
                                              echo number_format($value['payment_AMOUNT'],2, '.', ',');
											  
											  $total_amnt += $value['payment_AMOUNT'];
											  
											  ?>											  </td>
                                              <!--<td><?php echo date("M-j-Y", strtotime($value['payment_DATE'])); ?></td>-->
											  <td><?php echo date("m/d/Y", strtotime($value['payment_DATE'])); ?></td>
											  
											  
                                              <td><?php echo $value['payment_NOTE']; ?></td>
                                              											</tr>
											<?php
											 } 
										
										?>
                                        
                                         <?php } ?> 
									</tbody>
									</table> 



									<!--  <b>Total</b>
                                         <?php echo number_format($total_amnt,2, '.', ',');
                                            ?> -->
                                         
								</div>
							</div>
								<center>
									
								</center>
							</div>
                        </div>
                    </div>
                     <script type="text/javascript">

			             $(window).ready(function() {
			                    setTimeout(function () {
			                        //console.log("CONVERT")
			                    table =  $('#transactionLogs').DataTable({
			                            "order": [],
			                             dom: 'Bfrtip',
			                             "lengthChange": false,
			                             "searching": true,
			                             "info": true, 
			                             "ordering": true,
			                             buttons: [
				                            {   extend: 'csvHtml5',
				                               	title: "Transaction Report " + $("#start").val(),download: 'open'
				                            },
				                            
				                            {   extend: 'excelHtml5',
				                            	title: "Transaction Report " + $("#start").val(),download: 'open'
				                               /* exportOptions: {
				                                    columns: [ 0, 1 ]
				                                }, title: "Station: "+$(".selected-station").html()+" Date: "+ $(".selected-date").val(), download: 'open'*/
				                            },
				                        ],
			                             initComplete: function () {
		                                this.api().columns().every(function () {
		                                    var column = this;
		                                    //console.log(column.text())
		                                    $("#drp").find("th").eq(column.index()).html("")
		                                    if(column.index() == "3" || column.index() == "4" || column.index() == "7"){

			                                    var select = $('<select class="form-control"><option value=""></option></select>')
			                                        .appendTo($("#transactionLogs #drp").find("th").eq(column.index()))
			                                        .on('change', function () {

			                                        var val = $.fn.dataTable.util.escapeRegex(
			                                        $(this).val());                                     

			                                        column.search(val, true, false)
			                                            .draw();
			                                    });

			                                    //console.log(select);

			                                    column.data().unique().sort().each(function (d, j) {
			                                        select.append('<option value="' + d + '">' + d + '</option>')
			                                    });
		                                	}

		                                	if(column.index() == "1" || column.index() == "2" || column.index() == "5" || column.index() == "6" || column.index() == "8"){
			                                    var select = $('<input type="text" class="form-control">')
			                                        .appendTo($("#transactionLogs #drp").find("th").eq(column.index()))
			                                        .on('keyup', function () {
			                                        var val = $.fn.dataTable.util.escapeRegex(
			                                        $(this).val());                                     

			                                        column.search(val, true, false)
			                                            .draw();
			                                    });

			                                    //console.log(select);

			                                    /*column.data().unique().sort().each(function (d, j) {
			                                        select.append('<option value="' + d + '">' + d + '</option>')
			                                    });*/
		                                	}
		                                });
		                            }
			                       });
			                       $("#transactionLogs").show();
			                       $(".dataTables_filter").addClass("hide")


			                       $(".dt-buttons").addClass("hidden")
				                    $(".btnCSV").unbind('click').on("click",function(){ $(".buttons-csv").click(); })
				                    $(".btnPDF").unbind('click').on("click",function(){ $(".buttons-pdf").click(); })
				                    $(".btnXLS").unbind('click').on("click",function(){ $(".buttons-excel").click(); })

			                       /*$('#transactionLogs thead .th').each( function () {
			                            var title = $(this).text();
			                            if(title != "Action"){
			                                $(this).html( '<input type="text" class="sb form-control" placeholder="'+title+'" />' );
			                            }
			                        } );*/

			                       // Apply the search
			                        table.columns().every( function () {
			                            var that = this;
			                        console.log(this.header());
			                            $( 'input', this.header() ).on( 'keyup change', function () {
			                                if ( that.search() !== this.value ) {
			                                    that
			                                        .search( this.value )
			                                        .draw();
			                                }
			                            } );
			                        } );

			                    }, 1);
			                })
			            </script>
					<?php
					
					break;
					default: href("?action=view");
					
					
					}}
				  ?>
         
            
            <!-- end row container -->
            
            

            <!-- footer-->      
        
            <?php include (LAYOUTS . "footer.php"); ?>
            <!-- end footer -->
		</div>
        <!-- end container -->

    </body>
</html>
