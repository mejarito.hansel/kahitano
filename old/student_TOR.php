<?php
	include('init.php');
    $siiid =$_GET['id'];
    $getsingle = USERS::getSingle(array("si_ID"=>"DESC"),$siiid  );
    $getsingle1 = USERS::getSingleSAI(array("si_ID"=>"DESC"), $siiid );
    // print_r($getsingle);
    // print_r($getsingle1);
    // var_dump($getsingle);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="pages\htmlfolder\assets/css/jquery.dataTables.min.css">
  	<link href="pages\htmlfolder\assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
  	<link href="pages\htmlfolder\assets/css/style.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  	<title>AMS</title>

</head>
<body onload="window.print();">
<!-- <body> -->
	<div class="container text-capitalize" style="margin-bottom: 15px; margin-top: 15px">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<img src="pages\htmlfolder\assets/img/logo.png">
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<h4 class="user_info">OFFICIAL TRANSCRIPT OF RECORDS</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-1 col-md-1 col-sm-1">
				<br><br><br>
				<span class="">Name:</span> 
			</div>
			<div class="col-lg-8 col-md-8 col-sm-8">
				<br><br><br>
				<div class="user_name font-weight-bold">
					<div class="row">
						<div class="col-lg-4 col-sm-4 col-md-4  col-sm-4 col-md-4">
							<span class=""><?=$getsingle['si_LNAME']?></span>
						</div>	
						<div class="col-lg-4 col-sm-4 col-md-4  col-sm-4 col-md-4">
							<span class=""><?=$getsingle['si_FNAME']?> </span>
						</div>	
						<div class="col-lg-4 col-sm-4 col-md-4  col-sm-4 col-md-4">
							<span class=""><?=$getsingle['si_MNAME']?></span>
						</div>	
						<!-- <div class="col-lg-3 col-sm-3 col-md-3  col-sm-3 col-md-3">
							<span class="">v</span>
						</div> -->
					</div>
				</div>
				<div class="user_fullname font-italic">
					<div class="row">
						<div class="col-lg-4 col-sm-4 col-md-4  col-sm-4 col-md-4">
							<p class="">Last Name</p>
						</div>	
						<div class="col-lg-4 col-sm-4 col-md-4  col-sm-4 col-md-4">
							<p class="">First Name</p> 
						</div>	
						<div class="col-lg-4 col-sm-4 col-md-4  col-sm-4 col-md-4">
							<p class="">Middle Name</p>
						</div>
						<!-- <div class="col-lg-3 col-sm-3 col-md-3  col-sm-3 col-md-3">
							<p class="">Middle Initial</p>
						</div>	 -->
					</div>
				</div>
			</div>
			<div class="col-lg-1 col-md-1 col-sm-1">
			</div>
			<div class="col-lg-2 col-md-2 col-sm-2 ">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 ">
						<div class="row">
							<img src="pages\htmlfolder\assets/img/f1.png">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 ">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Identification no.:</span> 	
							</div>
							<div class="col-lg-4 col-sm-4 col-md-4 ">
								<div class="bottomBorder">
									<span class="">none</span>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Gender:</span> 	
							</div>
							<div class="col-lg-4 col-sm-4 col-md-4 ">
								<div class="bottomBorder">
									<span class=""><?=$getsingle['si_GENDER']?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Date of Birth:</span> 	
							</div>
							<div class="col-lg-4 col-sm-4 col-md-4 ">
								<div class="bottomBorder">
									<span class=""><?=$getsingle['si_BIRTHYEAR']?></span>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Place of Birth:</span> 	
							</div>
							<div class="col-lg-4 col-sm-4 col-md-4 ">
								<div class="bottomBorder">
									<span class=""><?=$getsingle['si_BIRTHPLACE']?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Nationality:</span> 	
							</div>
							<div class="col-lg-4 col-sm-4 col-md-4 ">
								<div class="bottomBorder">
									<br>
									<span class=""></span>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Parent's/Guardian's Name:</span> 	
							</div>
							<div class="col-lg-4 col-sm-4 col-md-4 ">
								<div class="bottomBorder">
									<span class=""><?php if ($getsingle1['sai_name_of_guardian'] == " " || $getsingle1['sai_name_of_guardian'] == null) {
									echo "<br>";
									} else {
									echo $getsingle1['sai_name_of_guardian']; 
									}
									?>
									</span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Address:</span> 	
							</div>
							<div class="col-lg-10 col-sm-10 col-md-10">
								<div class="bottomBorder">
									<span class=""><?=$getsingle['address']?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-4 col-sm-4 col-md-4 ">
								<span>Candidate for the Title/ Degree:</span> 	
							</div>
							<div class="col-lg-4 col-sm-4 col-md-4 ">
								<div class="bottomBorder">
									<br>
									<span class=""></span>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Date of Graduation:</span> 	
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="bottomBorder">
									<br>
									<span class=""></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>SPECIAL ORDER No:</span> 	
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="bottomBorder">
									<br>
									<span class=""></span>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>DATE ISSUED:</span> 	
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="bottomBorder">
									<br>
									<span class=""></span>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Ref no:</span> 	
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<div class="bottomBorder">
									<br>
									<span class=""></span>
								</div>
							</div>
						</div><br>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<span>ENTRANCE DATA</span> 	
								<div class="row">
									<div class="col-lg-1 col-sm-1 col-md-1">
									</div>
									<div class="col-lg-3 col-sm-3 col-md-3 ">
										<span>Academic Year Attended:</span> 	
									</div>
									<div class="col-lg-3 col-sm-3 col-md-3 ">
										<div class="bottomBorder">
											<br>
											<span class=""></span>
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>Last School Attended:</span> 	
									</div>
									<div class="col-lg-3 col-sm-3 col-md-3 ">
										<div class="bottomBorder">
											<span class="">
												<?php 
												if ($getsingle1['sai_college_last_attended'] == null|| $getsingle1['sai_college_last_attended'] == " ") {
													echo $getsingle1['sai_highschool_graduated'];
												} else {
													echo $getsingle1['sai_college_last_attended'];
												}
												?>
											</span>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-1 col-sm-1 col-md-1">
									</div>
									<div class="col-lg-3 col-sm-3 col-md-3 ">
										<span>Status of Admission:</span> 	
									</div>
									<div class="col-lg-3 col-sm-3 col-md-3 ">
										<div class="bottomBorder">
											<br>
											<span class=""></span>
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>Date Grad/Attended:</span> 	
									</div>
									<div class="col-lg-3 col-sm-3 col-md-3 ">
										<div class="bottomBorder">
											<span class="">
												<?php 
												if ($getsingle1['sai_cla_inclusive_date'] == null|| $getsingle1['sai_cla_inclusive_date'] == " ") {
													echo $getsingle1['sai_highschool_year_graduated'];
												} else {
													echo $getsingle1['sai_cla_inclusive_date'];
												}
												?>
											</span>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-1 col-sm-1 col-md-1">
									</div>
									<div class="col-lg-3 col-sm-3 col-md-3 ">
										<span>Basis for Admission:</span> 	
									</div>
									<div class="col-lg-3 col-sm-3 col-md-3 ">
										<div class="bottomBorder">
											<br>
											<span class=""></span>
										</div>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>Entrance Credentials:</span> 	
									</div>
									<div class="col-lg-3 col-sm-3 col-md-3 ">
										<div class="bottomBorder">
											<br>
											<span class=""></span>
										</div>
									</div>
								</div>
							</div>
						</div><br>
						<?php
$id = clean($_GET['id']);
				$info =  USERS::viewSingleStudent($_GET['id']);
				$info2 = USERS::getSingleSAI(array("si_ID"=>"ASC"),$_GET['id']);
				#var_dump($info);
				
				#print_r($info2);
			
				#$mygrade = SUBJENROL::gradebyid($id);
				#var_dump($mygrade);
				?>
                <?php $i = CECONTROLLER::getAllinfo($info['si_ID']); 
				#print_r($i);
				?>
                
                <br />
               
                <?php 
				$def = array();
				?>
				<!-- START -->
				
				
<table cellpadding="0" cellspacing="0" class="" width="100%" >
                                    <?php
                                    $getAllSemester = CURRICULLUM:: getAllSemester(array("yr_sem_ID"=>"ASC"));
                                    if($getAllSemester) {
                                    $counTemp = 1;
									
									
									
                                        foreach($getAllSemester as $key => $value){




                                            $curr_subj = CURRICULLUM::getSubjectbycurr($value['yr_sem_ID'],$i['curricullum_ID']);
                   # print_r($curr_subj);


                       # print_r($curr_subj);
                                            /*
                                            foreach($curr_subj as $key => $val_a){
                                            $subject_ID_a = $val_a['subject_ID'];
                                            $SubjectGET_a = CURRICULLUM:: getSubject($subject_ID_a);
                                            */
                                            //print_r($SubjectGET_a);

                                            //if($SubjectGET_a['subject_ID'] != $val_a['subject_ID'])

                                                //if((sizeof($curr_subj))%2 OR (sizeof($curr_subj))==0)
                                            //(sizeof($curr_subj))

                                            /*
                                               if($counTemp==2)
                                                {
                                                    $counTemp = 1;
                                               ?>
                                                        <td>
                                                <?php
                                                }else{
                                                ?>
                                                    <tr>
                                                        <td>
                                               <?php 
                                                    $counTemp ++;
                                                }
                                            */
                                            ?>

                                            <?php
                                            $totalCount = sizeof($curr_subj);
                                            //if(($totalCount %2 != 2) OR ($temppppp > 0)){
                                            if(($totalCount %2)){
                                                if($counTemp == 2)
                                                {
                                                    ?>
                                                    <tr>
                                                      <td style="border-top: solid 1px black;  border-bottom: solid 1px black">
                                                    <?php
                                                    $counTemp = 1;
                                                }else{
                                                ?>
                                                    
                                                      <td style="border-top: solid 1px black;  border-bottom: solid 1px black">
                                                <?php
                                                }
                                                    $counTemp ++;
                                            }
                                    ?>

                                    <!--
                                    <table id="sem<?= $value['yr_sem_ID']; ?>" border="1" class="" style="border-top: solid 4px black; border-bottom: solid 2px black;">
                                    -->
                                   
                                    <table cellpadding="0" cellspacing="0" id="sem<?= $value['yr_sem_ID']; ?>" border="2"  width="100%" style="border-top: solid 1px black; ;  border-bottom: solid 1px black;font-family:Verdana, Arial, Helvetica, sans-serif; font-size:10px;">
                                    
                                                            <th style="border-bottom: solid 0px black;"><?= $value['yr_sem_NAME']; ?>
                                                              <tr>
                                                                    <td>
                                                                        <table border='0' cellpadding="0" cellspacing="0" class="" width="100%">
                                                                            <!-- <th width="10%">S-ID -->
                                                                            <tr><th width="10%">Code
                                                                            <th width="30%">Subject Description
                                                                            <th width="10%"  style="text-align:center">Units
                                                                            <th width="20%" style="text-align:center">Prerequisite
                                                                            <th width="15%" style="text-align:center">Grade
                                                                    		 <th width="15%" style="text-align:center"><div align="center">Remarks 
                                                                               
                                                            <?php
                                                                foreach($curr_subj as $key => $val)
                                                                {
                                                            ?>
                                                                                                                                                       
                                                                            </div>
                                                                            <tr>
                                                                    <!-- <td colspan='6'> -->
                                                                        <!--<table border='1'>-->
                                                                        <?php 
                                                                        /*
                                                                            foreach($curr_subj as $key => $val_a){
                                                                            //if(($subject_ID_a)=($val_a['subject_ID'])){
                                                                            $subject_ID_a = $val_a['subject_ID'];
                                                                            $SubjectjectGET_a = CURRICULLUM:: getSubject($subject_ID_a);
                                                                            */
                                                                            $subject_ID = $val['subject_ID'];
                                                                            $SubjectGET = CURRICULLUM:: getSubject($subject_ID);
                                                                            //print_r($SubjectGET);
                                                                            foreach($SubjectGET as $subj => $val1)
                                                                            {
                                                                            ?>
                                                                          <tr>
                                                                                   <!-- <td><?= $val1['subject_ID'];?> -->
                                                                                    <td><?= $val1['subject_CODE'];?>
                                                                                    <td><?= $val1['subject_DESCRIPTION'];?>
                                                                                    <td align="center"><?= ($val1['LEC_UNIT']+$val1['LAB_UNIT']);?>
        <td align="center"><?php 
			$preq = SUBJECTS::getID($val['prerequisite_subject_ID']);
			echo $preq['subject_CODE'];

					if(!is_array($preq)){ echo "None"; }
		 	?>
                  <td align="center">
                          <?php
              	$grade = CECONTROLLER::getEnrolledGrade($info['si_ID'],$val1['subject_ID']);
																				  
																				 #print_r($grade);
																				  
              	$g = $grade['grade'];
  				if(!is_null($grade['grade']) && $grade['is_check'] == 0)
				  {
				  $g = "NE";
				  echo "NE";
				  }else{
                  echo $grade['grade'];
				  }
																				  
																				  
																				  
																				  
				  switch($grade['grade']){
                          case '5.00': case 'INC': case 'DRP':
						  	$def[] = $val1['subject_ID'];
						  break;
						 }   
				  
				  if(is_null($grade['grade']) and !is_null($grade['subject_enrolled_ID']))
				  {
				  $def[] = $val1['subject_ID'];
				  echo $g = "NE";
				  }else if(is_null($grade['grade']) and is_null($grade['subject_enrolled_ID']))
				  {
				 	$grade = CECONTROLLER::getEncodedGrade($info['si_ID'],$val1['subject_ID']);
					
					#print_r($grade);
					# echo $grade['grade'];
					 
                     
                     if(!is_null($grade['grade'])){
					 ?>
					 <!--
					 <form>
                      <select name="grade<?= $val1['subject_ID']; ?>" class="form-control">
                        <option value="NG">NO GRADE</option>
                        <option value="1.00" <?php if($grade['grade'] == "1.00"){ echo " SELECTED=SELECTED "; } ?>>1.00</option>
                        <option value="1.25" <?php if($grade['grade'] == "1.25"){ echo " SELECTED=SELECTED "; } ?>>1.25</option>
                        <option value="1.50" <?php if($grade['grade'] == "1.50"){ echo " SELECTED=SELECTED "; } ?>>1.50</option>
                        <option value="1.75" <?php if($grade['grade'] == "1.75"){ echo " SELECTED=SELECTED "; } ?>>1.75</option>
                        <option value="2.00" <?php if($grade['grade'] == "2.00"){ echo " SELECTED=SELECTED "; } ?>>2.00</option>
                        <option value="2.25" <?php if($grade['grade'] == "2.25"){ echo " SELECTED=SELECTED "; } ?>>2.25</option>
                        <option value="2.50" <?php if($grade['grade'] == "2.50"){ echo " SELECTED=SELECTED "; } ?>>2.50</option>
                        <option value="2.75" <?php if($grade['grade'] == "2.75"){ echo " SELECTED=SELECTED "; } ?>>2.75</option>
                        <option value="3.00" <?php if($grade['grade'] == "3.00"){ echo " SELECTED=SELECTED "; } ?>>3.00</option>
                        <option value="5.00" <?php if($grade['grade'] == "5.00"){ echo " SELECTED=SELECTED "; } ?>>5.00</option>
                        <option value="INC" <?php if($grade['grade'] == "INC"){ echo " SELECTED=SELECTED "; } ?>>INCOMPLETE</option>
                        <option value="DRP" <?php if($grade['grade'] == "DRP"){ echo " SELECTED=SELECTED "; } ?>>DROPPED</option>
                        
                    </select>   
                    
                    </form>
                    -->
                    <?= $g =$grade['grade']; ?>
                    <?php 
						switch($grade['grade']){
                          case '5.00': case 'INC': case 'DRP':
						  	$def[] = $val1['subject_ID'];
						  break;
						 }   
                    ?>
					 <?php } ?>
					 
					 <?php
				  }
				 
				  
				  
				  
				  
				  if( is_null($grade['grade']) and is_null($grade['subject_enrolled_ID']))
				  {
				  
				  ?>

               NG 
               
                  <?php
				  $g = "NG";
				  
				  $def[] = $val1['subject_ID'];
				  }
				  
                  ?>
                  
                 <span id="result<?php echo $val['subject_ID']; ?>"></span>
            <?php
            }   
			
        ?>
       			
        <!--</table>-->
        <?// = $val['subject_ID']; ?>                                                                    </td>
                  <td align="center"><?php
				  switch($g)
				  {
				  	case '1.00': 
					case '1.25':
					case '1.50': 
					case '1.75': 
					case '2.00': 
					case '2.25': 
					case '2.50': 
					case '2.75': 
					case '3.00':
					echo "PASSED";
					break;
					case '5.00': echo "FAILED";
					break;
case "UW": echo "UNAUTHORIZED WITHDRAWAL";
break;
case "AW": echo "AUTHORIZED WITHDRAWAL";
break;
					case 'INC': echo "INCOMPLETE";  break;
					case 'DRP': echo "DROPPED"; break; 
					case 'NG': echo "NO GRADE"; break;
					case 'NE': echo "NOT ENCODED"; break;
					
				  }
				   ?></td>
                  <?php
}

if(sizeof($curr_subj)==0)  
	{
		
		if(isset($_GET['hidden']) && $_GET['hidden'] == 'yes')
		{
		
		}else{
		?>
		
			<script type="text/javascript">
                $("#sem<?php echo $value['yr_sem_ID']; ?>").toggle();
            </script>
		<?php
		}
		
		
	} 
	
	
	
																	
																	
																	
															?>		
																	
															<script>
								$(document).ready(function(){
								
								
									<?php  foreach($curr_subj as $key => $val){ ?>
										  $('[name="grade<?php echo $val['subject_ID']; ?>"]').each(function() {
												$(this).change(function() {
													var mydata = $('[name="grade<?php echo $val['subject_ID']; ?>"]').val();//$(this).val();
													var inputdata = <?php echo $val['subject_ID']; ?>;
													var studentID = <?php echo $info['si_ID']; ?>;
												//alert(mydata)
												//alert(inputdata)
												//alert(studentID)
											
													$.ajax({   
													   type: 'GET',   
													   url: 'update_ajax.php',   
													   data: {action:'grade_encoding_admin.php',subject_ID:inputdata, grade:mydata, student_ID:studentID},
															success: function(data){
															
															  $('#result<?php echo $val['subject_ID']; ?>').html(data);
															}													
														});
												
												
													});
													
										  	});
									<?php } ?>		
								});
								</script>		
																	
														<?php			
																
                                                            //loop of subjects
                                                                        /*
                                                                ?>
                                                                <script type="text/javascript">
                                                                $(document).ready(function(){
                                                               
                                                                <?php
                                                                $getAllSemester = CURRICULLUM:: getAllSemester(array("yr_sem_ID"=>"ASC"));
                                                                if($getAllSemester)
                                                                {
                                                                    foreach($getAllSemester as $key => $value)
                                                                    {
                                                                        $curr_subj = CURRICULLUM::getSubjectbycurr($value['yr_sem_ID'],$cur_ID);
                                                                    foreach($curr_subj as $key => $val)
                                                                    {
                                                                          
                                                                    }
                                                                }
                                                                ?>
                                                                });
                                                                </script>
                                                                <?php
                                                                
                                                            } //last
                                                            */
															
                                                            ?>
                                                                        </table>
                                    </table>                                    
                        <?php    
                    } //foreach getallsemester
                } //if getallsemester

                    ?>		
</table><br />
                
                <?php
               $getsingle['student_type_ID'] =  $info['student_type_ID'];
			   
                ?>
                
<div class="form-group" style="display:none">
													<label for="user_UNAME" class="control-label col-sm-2">Student Type</label>
													<div class="col-md-5">
														 <?php
																$student_type = USERS::getAllStudentType();
																?>
															  <select  name="student_type_ID" id="student_type_ID" class="form-control">
																<?php foreach($student_type as $key => $val){ ?>
																<option value="<?= $val['student_type_ID']; ?>"  <?php if($getsingle['student_type_ID']==$val['student_type_ID']){echo "selected"; $type=$val['student_type_NAME'];} ?>><?= $val['student_type_NAME']; ?></option>
																<?php } ?>    
													  </select>	
													</div>
</div>
											   <!-- STUDENT TYPE --> 
<script type="text/javascript">
													$(document).ready(function(){
														$("#student_type_ID").change(function(){
															$( "#student_type_ID option:selected").each(function(){
																
																  <?php foreach($student_type as $key => $val){ ?>
																
																if($(this).attr("value")=="<?php print($val['student_type_ID']); ?>"){
																	   <?php foreach($student_type as $key1 => $val1){ ?>   
																			<?php if($val['student_type_ID'] == $val1['student_type_ID'])
																			{ ?>     
																			   $("#student_type_<?php echo $val1['student_type_ID']; ?>").show(); 
																				  $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',true);						
																				  
																				  
		$("form :input").attr('disabled',true);	
		$("form #update").attr('disabled',false);																				  
																			 <?php }else{ ?> 
																			  $("#student_type_<?php echo $val1['student_type_ID']; ?>").hide();
																			  $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',true);
																			<?php } ?>  
																	   <?php } ?>	  
																}
																
																<?php } ?>
															   
															});
														}).change();
													});
												</script>
									
<div class="form-group "  style="display:none">
												   
												   
												   <label for="user_UNAME" class="control-label col-sm-3">Requirements</label>
													 <label for="user_UNAME" class="control-label col-sm-2">Status</label>   
</div>
											  <!-- REQUIREMENTS -->
                                              
											  <?php
											  $req = array();
												$student_type = USERS::getAllStudentType();	?>
													
													<?php foreach($student_type as $key => $val){ ?>
															
															  
													<div id="student_type_<?= $val['student_type_ID']; ?>" style="/*display:none*/">
														<?php $requirements = USERS::getStudentTypeRequirements($val['student_type_ID']); ?>
														<?php foreach($requirements as $key1 => $val1){ ?>
																	
														   <?php
														  $check =  USERS::checkReqByUser($val1['req_ID'],$_GET['id']);
														   ?>    
																	
																	
													  <div class="form-group"  style="display:none">	
																	 <label for="user_UNAME" class="control-label col-sm-3">	<?= $val1['req_NAME']; ?></label>
																	<div class="col-md-3">
																  <select  class="form-control" name="<?= $val1['req_ID']; ?>">
																		<option  value="0" <?php if($check['status']==0){echo "selected";} ?>>Not Passed</option>
																		<option value="1"  <?php if($check['status']==1){echo "selected";} ?>>Passed</option>
																		
                                                                        
                                                                        <?php if($check['status']==0 and $val['student_type_ID'] == $info['student_type_ID'])
																		{
																		$req[] = $val1['req_NAME'];
																		}
																		?>
                                                                        
																   </select>	
																	
																	</div>
															
													  </div>
														<?php } ?>	
													</div>
															<?php } ?>    
														  

</body>
</html>


<?php
	#print_r($info);
?>


								<script type="text/javascript">
													$(document).ready(function(){
															$("#type").text("<?= strtoupper($type); ?> STUDENT");
															
													});
								</script>		
						<!-- <div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 border">
								<div class="row border">
									<div class="col-lg-2 col-md-2 col-sm-2 border">
										<span class="">Term</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 border">
										<span>Course Code</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4  border">
										<span>Decriptive Title</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 border">
										<span>Credit Unit</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 border">
										<span>Final Grade</span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2 ">
										<span>First Year</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2 ">
										<span>First semester</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4  ">
										<span>AY</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>MASHP 1</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>hand and measuring tools</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>2</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>Draw</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Maritime Drawings and Diagrams</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>1</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-C1</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Understanding Self*</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-C2</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Readings of philippine history</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-E1</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>math, science & tech - living in the IT Era**</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>NAVARCH</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Naval Architecture + Seamanship</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>4</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>PES</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Practical Engine Skills****</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-M2</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Filipino</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>PE-1</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Swimming/First Aid</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>NSTP-1</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>CWTS</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>Second Semester</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>AY</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>MASHOP 2</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Machining Tools</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>2</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>EMATS</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span> Engineering Materials</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>4</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>THERMO</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Thermodynamics</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>4</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-C3</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Math in the Modern World*</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-C4</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>The Contemporary World*</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-E2</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Social Science & Philo-The Entrep Mind**</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>SFC</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Safety and Familiarization w/ KYT**** </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>2</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-AD1</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Society & Culture w/ Family Planning, STD/HIV/AIDS**</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>

								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>PE 2</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Aquatic Survival </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>2</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>NSTP 2</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>CWTS</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div><br>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>Second Year</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>First Semester</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>AY</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>ELECTRO 1</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Basic Electricity</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>4</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>AUXMACH 1</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Auxiliary Machinery 1 </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>6</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>CHEM</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Industrial Chemistry and Tribology</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>  3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-C5 </span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Science, Technology and Society*</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-C6</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Art Appreciation*</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>  3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-E3</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Math, Science & Tech - Environmental Science**</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>PE 3</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Team Sports</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>2</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>Second Semester</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>AY</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>MASHOP 3</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Gas and Electric Welding</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>2</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>ELECTRO 2</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Marine Electronics and Electrical Maintenance</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>5</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>MARLAW </span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Maritime Law w/ PMMR</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>4</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-C7</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span> Purposive Communication w/ IMO SMCP* </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-C8</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Ethics-Naval Leadership & Ethics*</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>GENED-M1</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Life and Works of Dr. Jose Rizal**</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>PE 4 </span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Dual Sports - Self Defense</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>2</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>Summer</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span> </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>SAFETY 1</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Basic Training</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div><br>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>Third Year</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>First Semester</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>AY</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>ELECTRO 3</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Marine Electricity</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>5</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>AUXMACH 2</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Auxiliary Machinery 2 (Deck Mach, St Gear, Purif)</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>5</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>PPD</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Power Plant Diese</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>5</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>PASGT</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Propulsion Ancillary System and Gas Turbine</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>AUTO 1</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Basic Control Engineering</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>4</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>PERSMAN</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Leadership and Teamwork</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div><br>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>Second Semester</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>AY</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>PPS</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Power Plant Steam</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>6</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>AUTO 2</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Marine Automation</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>4</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>MARENV</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Protection of the Marine Environment</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>MAINT</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Maintenance and Repair </span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>MECH</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Mechanics and Hydromechanics</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>EWATCH</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Engineering Watchkeeping</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>3</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>LIC</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Licensure Examination Seminar**</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>2</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>Summer</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>AY</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>SAT & SDSD</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Security Awareness Training, Seafarer with Designated Security Duties</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span>SAFETY 2</span>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<span>Advanced Fire Fighting, Proficiency in Survival Craft and Rescue Boat other than Fast Rescue Boats, Medical First Aid</span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
									<div class="col-lg-2 col-md-2 col-sm-2">
										<span></span>
									</div>
								</div>


							</div>
						</div> -->
						<br>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<table class="table table-bordered">
								    <thead>
								      <tr>
								        <th>SEAGOING SERVICE</th>
								        <th>VESSEL NAME</th>
								        <th>EMBARKATION</th>
								        <th>DISEMBARKATION</th>
								        <th>DURATION</th>
								      </tr>
								    </thead>
								    <tbody>
								     
								      <tr>
								        <td></td>
								        <td></td>
								        <td></td>
								        <td>Total Sea Service</td>
								        <td>days</td>
								      </tr>
								    </tbody>
								  </table>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12">
									<span>*** GRADUATED with the degree of BACHELOR OF SCIENCE IN MARINE ENGINEERING (BSMarE) from this Academy on ______________, as per Special Order No. ____________________, issued by the Commission on Higher Education, Region ___, City of Dasmariñas, Cavite on ______________________.</span>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<span>*************************************************************** NOTHING FOLLOWS ***************************************************************</span>
									</div>
									
								</div>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="" style="border: 2px solid black">
											<div class="row">
												<div class="col-lg-1 col-sm-1 col-md-1">
												</div>
												<div class="col-lg-1 col-sm-1 col-md-1">
													<span>Credits:</span>
												</div>
												<div class="col-lg-10 col-sm-10 col-md-10">
													<span> One unit of credit is one hour of recitation/lecture for the period of a complete semester. Three hours of laboratory, draw ing, field or shopw ork are regarded as equivalent of one unit of credit.</span>
												</div>
											</div>
											
											
										</div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12" >
										<div class="" style="border: 2px solid black">
											<div class="row">
												<div class="col-lg-1 col-sm-1 col-md-1">
												</div>
												<div class="col-lg-1 col-sm-1 col-md-1">
													<span>Note:</span>
												</div>
												<div class="col-lg-10 col-sm-10 col-md-10">
													<span>The transcript is valid only w hen it bears the seal of the Academy and the original signature in ink of the Academy Registrar. Any erasure or alteration on this transcript renders the w hole transcript null and void.</span>
												</div>
											</div>
											
										</div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12" >
										<div class="row">
											<div class="col-lg-1 col-sm-1 col-md-1">
												<span>Remarks:</span>
											</div>
											<div class="col-lg-11 col-sm-11 col-md-11">
												<div class="bottomBorder">
													<br>
													<span class=""></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<table class="table table-bordered">
										    <thead>
										      <tr>
										        <th>Equivalent Figure</th>
										        <th>Percentage Grade</th>
										      </tr>
										    </thead>
										    <tbody>
										      <tr>
										        <td>1.00</td>
										        <td>100% - 97%</td>
										      </tr>
										      <tr>
										        <td>1.25</td>
										        <td>96% - 93%</td>
										      </tr>
										      <tr>
										        <td>1.50</td>
										        <td>92% - 89%</td>
										      </tr>
										      <tr>
										        <td>1.75</td>
										        <td>88% - 85%</td>
										      </tr>
										      <tr>
										        <td>2.00</td>
										        <td>84% - 80%</td>
										      </tr>
										      <tr>
										        <td>2.25</td>
										        <td>79% - 75%</td>
										      </tr>
										      <tr>
										        <td>2.50</td>
										        <td>74% - 70%</td>
										      </tr>
										      <tr>
										        <td>2.75</td>
										        <td>69% - 65%</td>
										      </tr>
										      <tr>
										        <td>3.00</td>
										        <td>64% - 60%</td>
										      </tr>
										      <tr>
										        <td>5.00</td>
										        <td>59% - below</td>
										      </tr>
										    </tbody>
										  </table>
									</div>
									<div class="col-lg-4 col-sm-4 col-md-4 "></div>
									<div class="col-lg-4 col-sm-4 col-md-4 ">
										<br>
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<span>Prepared by/Date:</span>
												<div class="col-lg-12 col-md-12 col-sm-12 user_name">
													<br>
													<br>
												<span></span>
												</div>
												<div class="col-lg-12 col-md-12 col-sm-12 user_fullname font-italic">
													<p class="" >Records Assistant</p>
												</div>
											</div>
										</div><br>
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<span>Verified by/Date:</span>
												<div class="col-lg-12 col-md-12 col-sm-12 user_name">
													<br>
													<br>
												<span></span>
												</div>
												<div class="col-lg-12 col-md-12 col-sm-12 user_fullname font-italic">
													<p class="" >Dean of Academic Affairs</p>
												</div>
											</div>
										</div><br>
										<div class="row">
											<div class="col-lg-12 col-md-12 col-sm-12">
												<span>Attested by/Date:</span>
												<div class="col-lg-12 col-md-12 col-sm-12 user_name">
													<br>
													<br>
												<span></span>
												</div>
												<div class="col-lg-12 col-md-12 col-sm-12 user_fullname font-italic">
													<p class="" >Academy Registrar</p>
												</div>
											</div>
										</div>
									</div>
								</div>

							</div>
						</div><br><br><br>
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<span>MMMA-FM-08-14 Rev.00</span>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-6 col-sm-6 col-md-6">
										<span>Not Valid Without Academy Dry Seal</span>
									</div>
									<div class="col-lg-6 col-sm-6 col-md-6 ">
										<span class="pull-right">TOR-xxxx-John Doe X</span>
									</div>
								</div>
							</div>
						</div>

					</div>

				</div>
				

			</div>
		</div><br>
	</div>

    <!-- <script src="pages\htmlfolder\assets/js/jquery.min.js"></script> -->
    <script src="pages\htmlfolder\assets/js/jquery.dataTables.min.js"></script>
  	<script src="pages\htmlfolder\assets/js/bootstrap.min.js"></script>

    

</body>
</html>
