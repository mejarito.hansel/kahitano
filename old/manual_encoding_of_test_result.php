<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	$user_id = $user['account_ID'];
    $action_event = "View";
    $event_desc = "MODULE: Admission / Manual Encoding of Test Result, DESCRIPTION: User visited Manual Encoding of Test Result";
    $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}

	



if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
    

    
  
    
	if ($user) 
	{
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();
	
			  
	
			
				SESSION::login($user);
                msgbox("Login Success");
               
               
				AUDIT::insert($user['account_ID'],"LOGGED IN");
				
				
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}



	//QUERIES




	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
	  	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>

      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
      <title>HOME | School Management System v2.0</title>
	</head>

	<body>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
            
			<!-- alert messages -->
			<?php 
			
			SESSION::DisplayMsg(); 
			
			
			
			?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
				
				//audit
				
				
				
					?>
				<div class="col-md-3">
            		<?php include(PAGES."navigation.php");?>
				</div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 


								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>
								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

									</div>
								</div>
                                
                                	<div class="form-group row">
									<div class="col-lg-offset-9">
										 <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                                
                                </fieldset>
                    </form>
                    </div>	
                </div>
                
                <?php   }else{
				
				AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
				
				?>

<div class="col-md-3">

<?php include(PAGES."navigation.php");?>
</div>
              
                
<div class="col-md-9">
	<div class="panel panel-default">

		<div class="panel-heading">
			<table>
        		<tr>
        			<td width="50%">
						<h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 9px"> Manual Encoding of Test Result <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
					</td>
				 	<td>
            			<div class="input-group col-md-12 hide">
              				
              				<input id="searchItem" class="searchItem form-control"  type="text" autofocus="autofocus"  style="text-transform: uppercase;"/>
                            
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            			</div>
        			</td>
        			<!-- <td width="5%">
	    				<div class="col-md-1">   
	    					<button class='btn btn-success btn-ms btnCSV' >Exports as CSV</button>
	    				</div>
	    			</td> -->
	    		</tr>
	    	</table>
		</div>
		<div class="panel-body"><br>
			<!-- <div class='form-group'>
	            <input type='text' class="form-control" id='datetimepicker' placeholder="Date Accession No." />
	            <span style=" margin-top: -30px; float: right; margin-right: 7px;"><i class="fa fa-calendar"></i></span>
	        </div> -->
	        <div class="form-group">
			 	<label class="col-md-12">Applicant No.
			    </label>
			</div>
			<?php
           		$CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];


           	?>
	        <div class='form-group'>
	        	<div class="col-sm-4">
            		<input id="applicantID" type='text' class="form-control" placeholder=" Applicant No."/>
	        	</div>
		        <div class='col-xs-2'>
	          		<button type="submit" class="btn btn-success btn-block" id="searchBtn" data-ip="<?php echo $CLIENT_IP;?>" data-acct="<?php echo $user['account_ID'];?>"> Search</button>
		        </div>
	        </div><br><br><br>
	        <div class=" formYieh">
		        <div class="form-group">
				 	<label class="col-md-12">Student Name
				    </label>
				</div>
		        <div class='form-group'>
		        	<div class="col-sm-4">
	            		<input id="studName" type='text' class="form-control" placeholder="Student Name" disabled="" />
		        	</div>
	        	</div><br><br><br><!-- 
		        <div class="form-group">
				 	<label class="col-md-12">Student Score
				    </label>
				</div> -->
		        <div class='form-group'>
	        		<div class="col-sm-2">
				 		<label class="">IQ-Score</label>
		        	</div>
		        	<div class="col-sm-2">
		            	<input id="iqScore" type='number' class="form-control" placeholder=" IQ Score"/>
		            	<span class="errorRequired hide" id="iqError">*required</span>
		        	</div>
	        		<div class="col-sm-1">
				 		<label class="">EQ-A</label>
		        	</div>
		        	<div class="col-sm-2">
		            	<input id="eqA" type='number' class="form-control" placeholder=" EQ-A"/>
		            	<span class="errorRequired hide" id="eqAerror">*required</span>
		        	</div>
	        		<div class="col-sm-1">
				 		<label class="">EQ-B</label>
		        	</div>
		        	<div class="col-sm-2">
		            	<input id="eqB" type='number' class="form-control" placeholder=" EQ-B"/>
		            	<span class="errorRequired hide" id="eqBerror">*required</span>
		        	</div>
		        </div><br><br>
		        <div class='form-group'>
	        		<div class="col-sm-2">
				 		<label class="">EQ-C</label>
		        	</div>
		        	<div class="col-sm-2">
		            	<input id="eqC" type='number' class="form-control" placeholder=" EQ-C"/>
		            	<span class="errorRequired hide" id="eqCerror">*required</span>
		        	</div>
	        		<div class="col-sm-1">
				 		<label class="">EQ-D</label>
		        	</div>
		        	<div class="col-sm-2">
		            	<input id="eqD" type='number' class="form-control" placeholder=" EQ-D"/>
		            	<span class="errorRequired hide" id="eqDerror">*required</span>
		        	</div>
	        		<div class="col-sm-1">
				 		<label class="">EQ-E</label>
		        	</div>
		        	<div class="col-sm-2">
		            	<input id="eqE" type='number' class="form-control" placeholder=" EQ-E"/>
		            	<span class="errorRequired hide" id="eqEerror">*required</span>
		        	</div>
		        </div>
		        <!-- <div class='form-group'>
		            <input type='text' class="form-control" id='datetimepicker4' placeholder="Date Received" />
		            <span style=" margin-top: -30px; float: right; margin-right: 7px;"><i class="fa fa-calendar"></i></span>
		        </div> --><br><br>
		      	<div class="col-lg-12 hide" id="saveSuccess">
		      		<div class="success col-lg-12"><strong>Success!</strong> Saved Successfully!</div>
		        </div>
		      	<div class="col-lg-12 hide" id="updateSuccess">
		      		<div class="success col-lg-12"><strong>Success!</strong> Updated Successfully!</div>
		        </div>
		        <div class='col-xs-4 hide' id="savebutton">
	          		<button type="submit" class="btn btn-success btn-block" id="saveBtn"> Save</button>
		        </div>
		        <div class='col-xs-4 hide' id="updatebutton">
	          		<button type="submit" class="btn btn-success btn-block P2C6_UPDATE" id="updateBtn"> Update</button>
		        </div>
	        </div>
	 	</div>
	</div>
</div>
<?php } ?>
  	
			</div>
            <?php 
			?>
			<?php include (LAYOUTS . "footer.php"); ?>
 			<script src="<?php echo HOME; ?>old/assets/js/custom/manual_encoding.js"></script>

		</div>
  <script type="text/javascript">
  	$( window ).ready(function(){
        setTimeout(function() {

      		$.main.manual.manual_encode();
          
        },100);
  	})
  	$('#applicantID').keypress(function(e){
        var txt = String.fromCharCode(e.which);
	    // console.log(txt + ' : ' + e.which);
	    var key = e.which;
	    if(key == 13)
	    {
	      $('#searchBtn').click();
	      return false;
	    }
  	});
  </script>
	</body>
</html>
