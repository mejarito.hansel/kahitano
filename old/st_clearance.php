<?php 
//INITIALIZE INCLUDES
    include('init.php');
#SESSION::CheckLogin();
    $user_id = $user['account_ID'];
    $action_event = "View";
    $event_desc = "MODULE: Registrar / Student Clearance, DESCRIPTION: User visited Student Clearance";
    $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
    //QUERIES
    if(Request::post()){
        switch(Request::get("action")) {
            case "add": 
            #var_dump(Request::post());
            USERS::addstudent(Request::post());
            #var_dump(Request::post());
            break;
            case "edit": USERS::update_student($_GET['id'],Request::post()); break;
            #case "update_blog":  BLOG::update(Request::post("blog_info")); break;
            
            #case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
            
            default: Fail::not_found();
        }
    }
    
    if(Request::get()){
        switch(Request::get("action")) {
            case "delete": ANNOUNCEMENT::delete($_GET['id']); break;
        
        }
    }       
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
      <title>Student Information | School Management System v2.0</title>
      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
    </head>

    <body>

        <!-- top nav -->
        <?php include(LAYOUTS . 'top_nav.php'); ?>
        <!-- end nav -->

        <div class="container">
            
            <!-- banner -->
            <?php include(LAYOUTS . "banner.php"); ?>
            <!-- end banner -->
            
          
            
            
            <!-- alert messages -->
            <?php SESSION::DisplayMsg(); ?>
            <!-- end of alert messages -->
            
            <!-- start row container -->
            <!-- 
            <div class="row">
                
                <div class="col-lg-12">

                
                    <ul class="breadcrumb">
                        <li class="active">Home</li>
                    </ul>
                

                </div>
                
            </div>
            -->
            <!-- end row container -->
            
   
             
            <!-- row container -->
            <div class="row">
            <!-- left nav -->

        
                <div class="col-md-4 col-lg-3">
                    <?php include(PAGES."navigation.php");?>
                </div>
                
                <!-- end left nav -->
                
                <!-- BODY -->
                
                
                <div class="col-lg-9 ">
                <?php if (SESSION::isLoggedIn()) { ?>
                    <?php
                    switch($_GET['action']){
                        case "add":
                        
                        AUDIT::ins($user['account_ID'],"Student Information","Visit Add Student Page");
                        
                        ?>
                            <div class="panel panel-default">
                            
                                    <div class="panel-heading">
                                        <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                        <i class="fa fa-folder-o"></i> Add Student Information </h4> 
                                    </div>
                            <div class="panel-body">
                                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well' name=form1>
                                            <fieldset>
                                                    <legend><i class="fa fa-user"></i> Student Information</legend>
                                                                 
                                                                 
                                                                 
                                                    <div class="form-group">
                                                        <label class="col-md-4">Student No.
                                                        </label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-4">
                                                            <select onchange="getData(this);" required class="form-control col-md-4" name="student_choose">
                                                                <option value="">Please Choose</option>
                                                                <option value="auto">Autogenerate</option>
                                                                <option value="specify">Specify</option>
                                                            </select>    
                                                        </div>
                                                        
                                                     </div>
                                                     
                                                    <script>
                                                    function getData(dropdown) {
                                                      var value = dropdown.options[dropdown.selectedIndex].value;
                                                     if (value == 'specify'){
                                                      document.getElementById("specify").style.display = "block";
                                                      document.getElementById("user_STUDENT_ID").disabled = false;
                                                      document.getElementById("user_STUDENT_ID").focus();
                                                      document.getElementById("auto1").style.display = "none";
                                                      
                                                      
                                                     }
                                                     if(value == 'auto'){
                                                       document.getElementById("auto1").style.display = "block";
                                                       document.getElementById("specify").style.display = "none";
                                                       document.getElementById("user_STUDENT_ID").disabled = true;
                                                       
                                                     }
                                                      if(value == ''){
                                                       document.getElementById("auto1").style.display = "none";
                                                       document.getElementById("user_STUDENT_ID").disabled = true;
                                                       document.getElementById("specify").style.display = "none";
                                                     }
                                                    }</script>
                                                    
                                                    <div class="form-group" id="auto1" style="display:none">
                                                        <label for='user_EMAIL' class='col-md-4'>
                                                       
                                                        Autogenerate: <br />
                                                        <select class="form-control"  name="auto1" >
                                                        <?php for($x=date("y");$x>=18;$x--){?>
                                                            <option value="MMMA-<?=$x; ?>-">Prefix: MMMA-<?=$x; ?>-XXX</option>
                                                        <?php } ?>    
                                                        </select></label>
                                                    </div>
                                                        
                                                   <div class="form-group" id="specify" style="display:none">  
                                                        <label for='user_EMAIL' class='col-md-4'>
                                                       
                                                         Specifiy: <br />
                                                         <input id="user_STUDENT_ID" required name='user_STUDENT_ID' placeholder="Sxxxxxxx" class='form-control'  type='text' style="text-transform:uppercase" /></label>
                                                    </div>
                                                    
                                                    
                                                    <!--
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-3'>Student No.:</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-5"><input name='user_STUDENT_ID' placeholder="Sxxxxxxx" class='form-control' required type='text' style="text-transform:uppercase" /></div>     
                                                    </div> 
                                                    -->
                                                    
                                                       
                                                    
                                                    <!-- Name -->
                                                    <div class="form-group">
                                                            <label for='si_FNAME' class='col-md-3'>Student Name</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3">
                                                            <input name='user_FNAME' pattern="([a-zA-Z0-9]|ñ|Ñ|.| |/|\|@|#|$|%|&)+" placeholder="First Name" class='form-control' type='text' required autofocus  style="text-transform:uppercase"/>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input name='user_MNAME' pattern="([a-zA-Z0-9]|ñ|Ñ|.| |/|\|@|#|$|%|&)+" placeholder="Middle Name"  class='form-control' type='text' style="text-transform:uppercase" />
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input name='user_LNAME' style="text-transform:uppercase" pattern="([a-zA-Z0-9]|.|ñ|Ñ| |/|\|@|#|$|%|&)+" placeholder="Last Name" class='form-control' type='text' required/>
                                                        </div>
                                                    </div>
                                                   <div class="form-group">
                                                        <div class="col-md-7">
                                                            <label for='user_DOB' >Date Of Birth</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for='user_GENDER' >Gender</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <select onchange="getAge();" name="user_MONTH" id="user_MONTH" class='form-control' >  
                                                            <option value='0' >MONTH</option>
                                                            <?php
                                                                $months = array('January','February','March','April','May','June','July ','August','September','October','November','December',);
                                                                $x = 1;
                                                                foreach ($months as $month) {
                                                                    echo "<option value=\"" . $x . "\">" . $month . "</option>";
                                                                    $x++;
                                                                }
                                                            ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <select onchange="getAge();" name="user_DATE" id="user_DATE"  class='form-control' >  
                                                                <option value='0'>DAY</option>
                                                            <?php for($i=1;$i<=31;$i++){?>
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php }?>
                                                            </select> 
                                                        </div>
                                                        <div class="col-sm-2">
                                                           <select onchange="getAge();"  name="user_YEAR" id="user_YEAR" class='form-control' >  
                                                                <option value='0'>YEAR</option>
                                                                <?php for($i=2014;$i>=1884;$i--){?>
                                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                                <?php }?>
                                                            </select> 
                                                        </div>
                                                        <script>
                                                            function getAge(value){
                                                                console.log($("#user_YEAR").val());
                                                                if($("#user_MONTH").val() != "0" && $("#user_YEAR").val() != "0" && $("#user_DATE").val()!= "0"){
                                                                var birthDate = new Date($("#user_YEAR").val()+"-"+$("#user_MONTH").val()+"-"+$("#user_DATE").val());
                                                                var ageDifMs = Date.now() - birthDate.getTime();
                                                                var ageDate = new Date(ageDifMs); // miliseconds from epoch
                                                                console.log(Math.abs(ageDate.getUTCFullYear() - 1970));
                                                                $("#sai_age").val(Math.abs(ageDate.getUTCFullYear() - 1970));
                                                                }
                                                            }
                                                        </script>



                                                        <label for="user_GENDER">
                                                            <div class="col-sm-7">
                                                                <div class="radio">
                                                                    <label>
                                                                    <input type="radio" checked="checked" name="user_GENDER" class="radio" value="Male" >Male
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-2">
                                                                <div class="radio">
                                                                    <label>
                                                                    <input type="radio" name="user_GENDER" class="radio" Value="Female" >Female
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </label>
                                                    </div>
                                                    
                                                    

                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for='user_Age' class=''>Age</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for='user_Height' class=''>Height(cm)</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for='user_Weight' class=''>Weight(kgs)</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                         <div class="col-sm-3"><input name='user_age' id="sai_age"  placeholder="Age" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_Height' id=sai_height  placeholder="Height" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_Weight' id="sai_weight"  placeholder="Weight" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for='user_Religion' class=''>Religion</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for='user_CivilStatus' class=''>Civil Status</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                         <div class="col-sm-3"><input name='user_Religion' id=""  placeholder="religion" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_CivilStatus' id=""  placeholder="Civil Status" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for='user_SpecialSkill' class=''>Special Skills</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-9"><input name='user_SpecialSkill' id=""   placeholder="Special Skills" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for='user_placeofBirth' class='col-md-3'>Place of Birth</label>
                                                    </div>
                                                    <div class="form-group">
                                                         <div class="col-sm-9"><input name='user_placeADDRESS'  placeholder="City, Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    
                                                    </div>
                                                    <!-- Address -->
                                                    <div class="form-group">
                                                        <label for='user_ADDRESS' class='col-md-3'>Present Address</label>
                                                    </div>
                                                    
                                                    <!--<div class="form-group">
                                                         <div class="col-sm-9"><input name='user_ADDRESS'  placeholder="BXX LXX ST., Subdv, City, Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    -->
                                                    
                                                    <div class="form-group">
                                                         <div class="col-sm-4"><input name='user_STREET' id="a1" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Street No. and St Address" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_BRGY' id="a2" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Barangay" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_CITY' id="a3" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="CITY" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3"><input name='user_DISTRICT' id="a4" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="District" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-4"><input name='user_PROVINCE' id="a5" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-3"><input name='present_ContactNo' id="a3" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Contact No." class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>  

                                                    <div class="form-group">
                                                        <label for='permanent_ADDRESS' class='col-md-3'>Permanent Address</label>
                                                    </div>
                                                    
                                                    <!--<div class="form-group">
                                                         <div class="col-sm-9"><input name='user_ADDRESS'  placeholder="BXX LXX ST., Subdv, City, Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    -->
                                                    
                                                    <div class="form-group">
                                                         <div class="col-sm-4"><input name='permanent_STREET' id="x"   placeholder="Street No. and St Address" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='permanent_BRGY' id="x" placeholder="Barangay" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='permanent_CITY' id="ax3"   placeholder="CITY" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3"><input name='permanent_DISTRICT'   placeholder="District" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-4"><input name='permanent_PROVINCE' id="x"   placeholder="Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-3"><input name='permanent_ContactNo' id="a3"   placeholder="Contact No." class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>   
                                                    
                                                    
                                                    
                                                    <div class="form-group">
                                                        <div class="col-md-4">
                                                            <label for='user_DOB' >Email Address</label>
                                                        </div>
                                                        <!-- <div class="col-md-4">
                                                            <label for='user_GENDER' >Contact No.</label>
                                                        </div> -->
                                                    </div>
                                                     <div class="form-group">
                                                            <div class="col-sm-4"><input name='user_EMAIL' placeholder="juandelacruz@yahoo.com" class='form-control' style="text-transform:uppercase" type='text' /></div>
                                                            <!-- <div class="col-sm-4"><input name='user_CONTACT' placeholder="09XXXXXXXXX" class='form-control' type='text' style="text-transform:uppercase" /></div> -->
                                                    </div>
                                                    
                                                    <!-- Email Address -->
                                                    
                                                    
                                                     <div class="form-group">
                                                        <label for='user_Course' class='col-md-3'>Course</label>
                                                    </div>
                                                    <div class="form-group">
                                                      <div class="col-sm-5"><input name='user_DEGREE' placeholder="0000000" class='form-control' type='hidden' style="text-transform:uppercase" />
                                                      
                                                      
                                                      
                                                  <select name=curricullum_ID class="form-control">
                                                    <?php $getallCurricullum= CECONTROLLER::getallCurricullum(array("course_ID"=>"ASC"));   
                                                    
                                                    
                                                        foreach($getallCurricullum as $key => $value){
                                                        
                                                            $cid = $value['course_ID'];
                                                            $getsingleCourse = CECONTROLLER::getsingleCourse(array("course_ID"=>"ASC"),$cid);
                                                            
            
                                                            
                                                            
                                                            foreach($getsingleCourse as $key2 => $val){
                                                    ?>
                                                    <option value=<?= $value['curricullum_ID']?>>
                                                        <?= $val['course_NAME']." - ".$value['curricullum_NAME']; ?>
                                                    </option>
                                                    <?php } } ?>
                                                </select>
                                                      
                                                     
                                                      </div>     
                                                         
                                                    </div>    
                                                    
                                                    <br/>
                                            
                                                        <!-- Requirements Submitted -->
                                                        <legend><i class="fa fa-edit"></i> Requirements Submitted</legend>
                                     
                                     
                                     
                                            <!-- STUDENT TYPE -->
                                                      <div class="form-group">
                                                            <label for="user_UNAME" class="control-label col-sm-2">Student Type</label>
                                                            <div class="col-md-5">
                                                                 <?php
                                                                        $student_type = USERS::getAllStudentType();
                                                                        ?>
                                                                      <select name="student_type_ID" id="student_type_ID" class="form-control">
                                                                        <?php foreach($student_type as $key => $val){ ?>
                                                                            <option value="<?= $val['student_type_ID']; ?>"><?= $val['student_type_NAME']; ?></option>
                                                                        <?php } ?>    
                                                              </select>
                                                              <!--
                                                                      <select name="student_type_ID2" id="student_type_ID2" class="form-control">
                                                                        <?php foreach($student_type as $key => $val){ ?>
                                                                        <option value="<?= $val['student_type_ID']; ?>">
                                                                          <?= $val['student_type_NAME']; ?>
                                                                        </option>
                                                                        <?php } ?>
                                                                      </select> 
                                                               -->       
                                                            </div>
                                            </div>
                                                       <!-- STUDENT TYPE --> 
                                            <script type="text/javascript">
                                                            $(document).ready(function(){
                                                                $("#student_type_ID").change(function(){
                                                                    $( "#student_type_ID option:selected").each(function(){
                                                                        
                                                                          <?php foreach($student_type as $key => $val){ ?>
                                                                        
                                                                        if($(this).attr("value")=="<?php print($val['student_type_ID']); ?>"){
                                                                               <?php foreach($student_type as $key1 => $val1){ ?>   
                                                                                    <?php if($val['student_type_ID'] == $val1['student_type_ID'])
                                                                                    { ?>     
                                                                                       $("#student_type_<?php echo $val1['student_type_ID']; ?>").show(); 
                                                                                          $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',false);                                                                       
                                                                                     <?php }else{ ?> 
                                                                                      $("#student_type_<?php echo $val1['student_type_ID']; ?>").hide();
                                                                                      $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',true);
                                                                                    <?php } ?>  
                                                                               <?php } ?>     
                                                                        }
                                                                        
                                                                        <?php } ?>
                                                                       
                                                                    });
                                                                }).change();
                                                            });
                                                        </script>
                                            
                                                        <div class="form-group">
                                                           
                                                           
                                                           <label for="user_UNAME" class="control-label col-sm-3">Requirements</label>
                                                             <label for="user_UNAME" class="control-label col-sm-2">Status</label>   
                                                        </div>
                                            
                                            
                                                      <!-- REQUIREMENTS -->
                                                      <?php
                                                        $student_type = USERS::getAllStudentType(); ?>
                                                            
                                                            <?php foreach($student_type as $key => $val){ ?>
                                                                    
                                                                      
                                                            <div id="student_type_<?= $val['student_type_ID']; ?>" style="/*display:none*/">
                                                                <?php $requirements = USERS::getStudentTypeRequirements($val['student_type_ID']); ?>
                                                                <?php foreach($requirements as $key1 => $val1){ ?>
                                                                            
                                                                    <div class="form-group">    
                                                                             <label for="user_UNAME" class="control-label col-sm-3">    <?= $val1['req_NAME']; ?></label>
                                                                            <div class="col-md-3">
                                                                          <select class="form-control" name="<?= $val1['req_ID']; ?>">
                                                                                <option value="0">Not Passed</option>
                                                                                <option value="1">Passed</option>
                                                                                
                                                                           </select>    
                                                                            
                                                                            </div>
                                                                    
                                                                    </div>
                                                                <?php } ?>  
                                                            </div>
                                                                    <?php } ?>    
                                                                  
                                                           <br/><br/>
                                                                  
                                            
                                                  <!-- REQUIREMENTS -->  
                                        
                                        
                                            <legend><i class="fa fa-user"></i> Contact Person (Parent/Guardian) In Case of Emergency</legend>
                                                    <!-- Guardian & Relationship-->
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>Guardian</label>
                                                        <label for='user_EMAIL' class='col-md-4'>Relationship</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>
                                                                <input name='user_GUARDIAN' style="text-transform:uppercase" class='form-control' type='text'/>
                                                        </label>
                                                        <label for='user_EMAIL' class='col-md-4'>
                                                                <input name='user_RELATIONSHIP' style="text-transform:uppercase" class='form-control' type='text' />
                                                        </label>
                                                    </div>
                                                    <!-- Occupation & Contact Number-->
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>Occupation</label>
                                                        <label for='user_EMAIL' class='col-md-4'>Contact No.</label>
                                                    </div>
                                            <div class="form-group">
                                                <label for='user_EMAIL' class='col-md-5'><input name='user_OCCUPATION' style="text-transform:uppercase" class='form-control' type='text' id="user_OCCUPATION" /></label>
                                                <label for='user_EMAIL' class='col-md-4'>
                                                        
                                                        <input name='user_GUARDIAN_CONTACT' style="text-transform:uppercase" class='form-control' type='text'/>
                                                </label>
                                              </div>
                                                    <!-- Guardian Address -->
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-3'>Guardian Address</label>
                                                    </div>
                                                    <div class="form-group">
                                                         <div class="col-sm-9"><input id="G1" name='user_GUARDIAN_ADDRESS' style="text-transform:uppercase" class='form-control' type='text' /></div>
                                                    </div>    
                                                    <!-- Elem & Graduated date-->
                                                    <legend><i class="fa fa-user"></i> Educational Attainment</legend>
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>Elementary</label>
                                                        <label for='user_EMAIL' class='col-md-4'>Date Graduated</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>
                                                                <input name='user_ELEMENTARY' style="text-transform:uppercase" class='form-control' type='text' list="elem" />
                                                                
                                                                 <?php
                                                                $elem = USERS::getAllElementary();
                                                                ?>
                                                                <datalist id="elem">
                                                                <?php foreach($elem as $key => $val){ ?>
                                                                    <option value="<?= $val['sai_elementary_graduated']; ?>"></option>
                                                                <?php } ?>    
                                                                </datalist>
                                                                
                                                                
                                                        </label>
                                                        <label for='user_EMAIL' class='col-md-4'>
                                                            <select name="user_ELEM_YR" class='form-control'>  
                                                            <option value='' readonly>YEAR</option>
                                                            <?php for($i=2014;$i>=1980;$i--){?>
                                                                <option value="<?php echo ($i-1)." - ".$i; ?>"><?php echo ($i-1)." - ".$i; ?></option>
                                                            <?php }?>
                                                            </select>          
                                                        </label>
                                                    </div>    
                                                    <!-- Highschool & Graduated date-->
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>High School</label>
                                                        <label for='user_EMAIL' class='col-md-4'>Date Graduated</label>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for='user_EMAIL' class='col-md-5'>
                                                                <input name='user_HIGHSCHOOL' style="text-transform:uppercase" class='form-control' list="highschools" type='text' />
                                                                
                                                                
                                                                <?php
                                                                $highschools = USERS::getAllHighSchool();
                                                                ?>
                                                                
                                                                <datalist id="highschools">
                                                                <?php foreach($highschools as $key => $val){ ?>
                                                                    <option value="<?= $val['sai_highschool_graduated']; ?>"></option>
                                                                <?php } ?>    
                                                                </datalist>
                                                                
                                                                
                                                        </label>
                                                        <label for='user_EMAIL' class='col-md-4'>
                                                            <select name="user_HS_YR" class='form-control'>  
                                                            <option value='' readonly>YEAR</option>
                                                            <?php for($i=2018;$i>=1980;$i--){?>
                                                                <option value="<?php echo ($i-1)." - ".$i; ?>"><?php echo ($i-1)." - ".$i; ?></option>
                                                            <?php }?>
                                                            </select>          
                                                        </label>
                                                    </div> 
                                                        <!-- Occupation & Contact Number-->
                                                        <div class="form-group">
                                                            <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-5'>College Last Attended</label>
                                                            <label for='user_CLA_DATE' class='col-md-4'>Date</label>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-5'>
                                                            <input style="text-transform:uppercase" name='user_COLLEGE_LAST_ATTENDED' list="college" class='form-control' type='text' />
                                                            <?php
                                                                    $college = USERS::getAllCollege();
                                                                    ?>
                                                                    
                                                                    <datalist id="college">
                                                                    <?php foreach($college as $key => $val){ ?>
                                                                        <option value="<?= $val['sai_college_last_attended']; ?>"></option>
                                                                    <?php } ?>    
                                                                    </datalist>
                                                          </label>
                                                            <label for='user_EMAIL' class='col-md-4'>
                                                                    <input name='user_CLA_DATE' class='form-control' type='text' />
                                                            </label>
                                            </div>    
                                            
                                            
                                 
                              
                                        <div class="form-group">
                                                <label for='si_FNAME' class='col-md-3'>Admitted Date</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <input name='user_DATEADMITTED'  value="<?= date("Y-m-d"); ?>" class='form-control datepicker' type='text' required   style="text-transform:uppercase"/>
                                            </div>
                                            
                                        </div>
                                            <br>
                                            <div class="form-group">
                                                <div class="col-md-5">
                                                    <label class="control-form">ENCODED BY: &nbsp;<?= $user['account_FNAME'];?> <?= $user['account_MNAME'];?> <?= $user['account_LNAME'];?></label>
                                                
                                                <input type="hidden" name="user_ADDEDBY" value="<?= $user['account_FNAME'];?> <?= $user['account_MNAME'];?> <?= $user['account_LNAME'];?>" />
                                                </div>
                                                <div class="col-md-5">
                                                    <button  class='btn btn-success btn-block' onclick="return confirm('Are you sure all data is correct?')" type='submit'><i class='fa fa-edit'></i> Add Student</button>
                                                </div>
                                            </div>
                                        
                                        
                                        </fieldset>
                                    </form>
               </div></div>
                    <?php break; 
                        case "edit":
                        $siiid =$_GET['id'];
                        $getsingle = USERS::getSingle(array("si_ID"=>"DESC"),$siiid  );
                        $getsingle1 = USERS::getSingleSAI(array("si_ID"=>"DESC"), $siiid );
                        /*print_r($getsingle);
                        print_r($getsingle1);*/
                        
                        AUDIT::ins($user['account_ID'],"Student Information","Visit Edit Student Page of (". $getsingle['student_ID'] .")".$getsingle['si_FNAME']." ".$getsingle['si_MNAME']." ".$getsingle['si_LNAME']);
                        
                        
                    ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Update Student Information </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well' name=form1>
                                <fieldset>
                                        <legend><i class="fa fa-user"></i> Personal Information</legend>
                                        
                                         <div class="form-group">
                                            <label for='user_EMAIL' class='col-md-3'>Student No.</label>
                                        </div>
                                        <div class="form-group">
                                          <div class="col-sm-4"><input autofocus name='user_STUDENT_ID' placeholder="0000000" class='form-control' type='text' style="text-transform:uppercase" value="<?= $getsingle['student_ID'];?>" /></div>     
                                             
                                             
                                             
                                        </div>    
                                        
                                            <!-- Name -->
                                        <div class="form-group">
                                                <label for='si_FNAME' class='col-md-3'>Student Name</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <input name='user_FNAME' pattern="([a-zA-Z0-9]|ñ|Ñ| |/|\|@|#|$|%|&)+" placeholder="First Name" class='form-control' type='text' required  value="<?=  $getsingle['si_FNAME'] ?>"  style="text-transform:uppercase"/>
                                            </div>
                                            <div class="col-sm-3">
                                                <input name='user_MNAME' pattern="([a-zA-Z0-9]|ñ|Ñ| |/|\|@|#|$|%|&)+" placeholder="Middle Name"  class='form-control' type='text' style="text-transform:uppercase" value="<?=  $getsingle['si_MNAME'] ?>"  />
                                            </div>
                                            <div class="col-sm-3">
                                                <input name='user_LNAME' style="text-transform:uppercase" pattern="([a-zA-Z0-9]|ñ|Ñ| |/|\|@|#|$|%|&)+" placeholder="Last Name" class='form-control' type='text' value="<?=  $getsingle['si_LNAME'] ?>"  required/>
                                            </div>
                                        </div>
                                        <!-- Date of Birth & Gender -->
                                        <div class="form-group">
                                            <div class="col-md-7">
                                            <label for='user_DOB' >Date Of Birth</label>
                                            </div>
                                                <div class="col-md-3">
                                            <label for='user_GENDER' >Gender</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <!--<input name='user_MONTH'   class='form-control' type='text' placeholder="Month" required/>-->
                                                <select name="user_MONTH"  onchange="getAge();"  id="user_MONTH" class='form-control'>  
                                                <option value='' readonly>MONTH</option>
                                                <?php
                                                
                                                $birthday = $getsingle['si_BIRTHDATE'];
                                                $day = explode("-",$birthday);
                                                $year = $day[0];
                                                $monthsary = $day[1];
                                                $date = $day[2];

                                                    $months = array('January','February','March','April','May','June','July ','August','September','October','November','December',);
                                                    $x = 1;
                                                    foreach ($months as $month) {
                                                    ?>
                                                    
                                                <!--        echo "<option value=\"" . $x . "\">" . $month . "</option>";-->
                                                          <option value="<?php echo $x; ?>"  <?php if($x == $monthsary){ echo 'selected=selected';} ?>/><?php echo $month; ?> 
                                        </option>
                                                   <?php     
                                                        $x++;
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <!--
                                                <input name='user_DATE'   class='form-control' type='text' placeholder="DAY"/>
                                                -->
                                                <select name="user_DATE"  onchange="getAge();"  id="user_DATE" class='form-control'>  
                                                    <option value='' readonly>DAY</option>
                                                <?php for($i=1;$i<=31;$i++){?>
                                                    <option value="<?php echo $i; ?>" <?php if($i==$date){ echo 'selected=selected';}?>><?php echo $i; ?></option>
                                                <?php }?>
                                                </select> 
                                            </div>
                                            <div class="col-sm-2">
                                                <!--
                                                <input name='user_YEAR'   class='form-control' type='text' placeholder="YEAR"/>
                                                -->
                                                <select name="user_YEAR"  onchange="getAge();"   id="user_YEAR" class='form-control' >  
                                                <option value='' readonly>YEAR</option>
                                                <?php for($i=2014;$i>=1884;$i--){?>
                                                    <option value="<?php echo $i; ?>" <?php if($i==$year){ echo 'selected=selected';}?>><?php echo $i; ?></option>
                                                <?php }?>
                                                </select> 
                                            </div>
                                            <label for="user_GENDER">
                                                <div class="col-sm-7">
                                                    <div class="radio">
                                                        <label>
                                                        <input type="radio" name="user_GENDER" class="radio" value="Male" required <?php if($getsingle['si_GENDER']=='Male'){ echo 'CHECKED';}?>>Male
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="radio">
                                                        <label>
                                                        <input type="radio" name="user_GENDER" class="radio" Value="Female" required <?php if($getsingle['si_GENDER']=='Female'){ echo 'CHECKED';}?> >Female
                                                        </label>
                                                    </div>
                                                </div>
                                            </label>
                                        </div>

                                        <script type="text/javascript">
                                            function getAge(value){
                                                    console.log($("#user_YEAR").val());
                                                    if($("#user_MONTH").val() != "0" && $("#user_YEAR").val() != "0" && $("#user_DATE").val()!= "0"){
                                                    var birthDate = new Date($("#user_YEAR").val()+"-"+$("#user_MONTH").val()+"-"+$("#user_DATE").val());
                                                    var ageDifMs = Date.now() - birthDate.getTime();
                                                    var ageDate = new Date(ageDifMs); // miliseconds from epoch
                                                    console.log(Math.abs(ageDate.getUTCFullYear() - 1970));
                                                    $("#sai_age").val(Math.abs(ageDate.getUTCFullYear() - 1970));
                                                    }
                                                }

                                            $(document).ready(function(){
                                                
                                                getAge();

                                            });
                                        </script>
                                            <!-- Student ID -->
                                          <!--
                                            <div class="form-group">
                                                <label for='user_ADDRESS' class='col-md-3'>Student ID</label>
                                            </div>
                                            <div class="form-group">
                                                 <div class="col-sm-9"><input name='user_STUDENT_ID' class='form-control' type='text' required/></div>
                                            </div>
                                           -->
                             
                                            <!-- Address -->
                                        <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for='user_Age' class=''>Age</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for='user_Height' class=''>Height(cm)</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for='user_Weight' class=''>Weight(kgs)</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                         <div class="col-sm-3"><input name='user_age' id="sai_age"  placeholder="Age" class='form-control' type='text' style="text-transform:uppercase"  /></div>
                                                         <div class="col-sm-3"><input name='user_Height'  " class='form-control' type='text' style="text-transform:uppercase" value="<?= $getsingle1['sai_height']; ?>" /></div>
                                                         <div class="col-sm-3"><input name='user_Weight'   class='form-control' type='text' style="text-transform:uppercase" value="<?= $getsingle1['sai_weight']; ?>" /></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for='user_Religion' class=''>Religion</label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <label for='user_CivilStatus' class=''>Civil Status</label>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                         <div class="col-sm-3"><input name='user_Religion' value="<?= $getsingle1['sai_religion']; ?>" placeholder="religion" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_CivilStatus' value="<?= $getsingle1['sai_civil']; ?>"  placeholder="Civil Status" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-3">
                                                            <label for='user_SpecialSkill' class=''>Special Skills</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-9"><input name='user_SpecialSkill' value="<?= $getsingle1['sai_special_skills']; ?>" id=""   placeholder="Special Skills" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for='user_placeofBirth' class='col-md-3'>Place of Birth</label>
                                                    </div>
                                                    <div class="form-group">
                                                         <div class="col-sm-9"><input name='user_placeADDRESS' value="<?= $getsingle['si_BIRTHPLACE']; ?>" placeholder="City, Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    
                                                    </div>
                                                    <!-- Address -->
                                                    <div class="form-group">
                                                        <label for='user_ADDRESS' class='col-md-3'>Present Address</label>
                                                    </div>
                                                    
                                                    <!--<div class="form-group">
                                                         <div class="col-sm-9"><input name='user_ADDRESS'  placeholder="BXX LXX ST., Subdv, City, Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    -->
                                                    <div class="form-group">
                                                         <div class="col-sm-4"><input name='permanent_STREET' value="<?= $getsingle1['sai_present_addr_st']; ?>"  id="a1" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Street No. and St Address" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='permanent_BRGY' value="<?= $getsingle1['sai_present_brgy']; ?>"  id="a2" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Barangay" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='permanent_CITY' value="<?= $getsingle1['sai_present_city']; ?>"  id="a3" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="CITY" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3"><input name='permanent_DISTRICT' value="<?= $getsingle1['sai_present_district']; ?>"  id="a4" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="District" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-4"><input name='permanent_PROVINCE' value="<?= $getsingle1['sai_present_prov']; ?>"  id="a5" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-3"><input name='permanent_ContactNo'  value="<?= $getsingle1['sai_present_contact']; ?>"  id="a5" onkeyup="document.getElementById('G1').value = document.getElementById('a1').value + ',' + document.getElementById('a2').value + ',' + document.getElementById('a3').value + ',' + document.getElementById('a4').value + ',' + document.getElementById('a5').value"  placeholder="Contact No." class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>   
                                                   

                                                    <div class="form-group">
                                                        <label for='permanent_ADDRESS' class='col-md-3'>Permanent Address</label>
                                                    </div>
                                                    
                                                    <!--<div class="form-group">
                                                         <div class="col-sm-9"><input name='user_ADDRESS'  placeholder="BXX LXX ST., Subdv, City, Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    -->
                                                    
                                                     <div class="form-group">
                                                         <div class="col-sm-4"><input name='user_STREET' value="<?= $getsingle['si_STREET']; ?>"    placeholder="Street No. and St Address" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_BRGY' value="<?= $getsingle['si_BRGY']; ?>"    placeholder="Barangay" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                         <div class="col-sm-3"><input name='user_CITY' value="<?= $getsingle['si_CITY']; ?>"   placeholder="CITY" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-sm-3"><input name='user_DISTRICT' value="<?= $getsingle['si_DISTRICT']; ?>"   placeholder="District" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-4"><input name='user_PROVINCE' value="<?= $getsingle['si_PROVINCE']; ?>"  placeholder="Province" class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                        <div class="col-sm-3"><input name='present_ContactNo' value="<?= $getsingle['si_CONTACT']; ?>"   placeholder="Contact No." class='form-control' type='text' style="text-transform:uppercase" /></div>
                                                    </div>  
                                        <!-- Email Address -->
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <label for='user_EMAIL' class=''>E-mail Address</label>
                                            </div>
                                            <!-- <div class="col-md-4">
                                                <label for='user_EMAIL' class=''>Contact No.</label>
                                            </div> -->
                                        </div>
                                        <div class="form-group">
                                             <div class="col-sm-4"><input name='user_EMAIL' placeholder="juandelacruz@yahoo.com" class='form-control' style="text-transform:uppercase" type='text' value="<?= $getsingle['si_EMAIL'];?>" /></div>
                                             <!-- <div class="col-sm-4"><input name='user_CONTACT' placeholder="09XXXXXXXXX" class='form-control' type='text' style="text-transform:uppercase" value="<?= $getsingle['si_CONTACT'];?>" /></div> -->
                                        </div>
                                        <div class="form-group">
                                                        <label for='user_Course' class='col-md-3'>Course</label>
                                                    </div>
                                                    <div class="form-group">
                                                      <div class="col-sm-5"><input name='user_DEGREE' placeholder="0000000" class='form-control' type='hidden' style="text-transform:uppercase" />
                                                      
                                                      
                                                      
                                                  <select name=curricullum_ID class="form-control">
                                                    <?php $getallCurricullum= CECONTROLLER::getallCurricullum(array("course_ID"=>"ASC"));   
                                                    
                                                    
                                                        foreach($getallCurricullum as $key => $value){
                                                        
                                                            $cid = $value['course_ID'];
                                                            $getsingleCourse = CECONTROLLER::getsingleCourse(array("course_ID"=>"ASC"),$cid);
                                                            
            
                                                            
                                                            
                                                            foreach($getsingleCourse as $key2 => $val){
                                                    ?>
                                                    <option value=<?= $value['curricullum_ID']?>>
                                                        <?= $val['course_NAME']." - ".$value['curricullum_NAME']; ?>
                                                    </option>
                                                    <?php } } ?>
                                                </select>
                                                      
                                                     
                                                      </div>     
                                                         
                                                    </div>   
                                             
                                        
                                        <br/>
                                        
                                
                                
                                            <!-- Requirements Submitted -->
                                            <legend><i class="fa fa-edit"></i> Requirements Submitted</legend>
                         
                         
                         
                                <!-- STUDENT TYPE -->
                                          <div class="form-group">
                                                <label for="user_UNAME" class="control-label col-sm-2">Student Type</label>
                                                <div class="col-md-5">
                                                     <?php
                                                            $student_type = USERS::getAllStudentType();
                                                            ?>
                                                          <select name="student_type_ID" id="student_type_ID" class="form-control">
                                                            <?php foreach($student_type as $key => $val){ ?>
                                                                <option value="<?= $val['student_type_ID']; ?>"  <?php if($getsingle['student_type_ID']==$val['student_type_ID']){echo "selected";} ?>><?= $val['student_type_NAME']; ?></option>
                                                            <?php } ?>    
                                                  </select> 
                                                </div>
                                </div>
                                           <!-- STUDENT TYPE --> 
                                <script type="text/javascript">
                                                $(document).ready(function(){
                                                    $("#student_type_ID").change(function(){
                                                        $( "#student_type_ID option:selected").each(function(){
                                                            
                                                              <?php foreach($student_type as $key => $val){ ?>
                                                            
                                                            if($(this).attr("value")=="<?php print($val['student_type_ID']); ?>"){
                                                                   <?php foreach($student_type as $key1 => $val1){ ?>   
                                                                        <?php if($val['student_type_ID'] == $val1['student_type_ID'])
                                                                        { ?>     
                                                                           $("#student_type_<?php echo $val1['student_type_ID']; ?>").show(); 
                                                                              $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',false);                                                                       
                                                                         <?php }else{ ?> 
                                                                          $("#student_type_<?php echo $val1['student_type_ID']; ?>").hide();
                                                                          $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',true);
                                                                        <?php } ?>  
                                                                   <?php } ?>     
                                                            }
                                                            
                                                            <?php } ?>
                                                           
                                                        });
                                                    }).change();
                                                });
                                            </script>
                                
                                            <div class="form-group">
                                               
                                               
                                               <label for="user_UNAME" class="control-label col-sm-3">Requirements</label>
                                                 <label for="user_UNAME" class="control-label col-sm-2">Status</label>   
                                            </div>
                                
                                
                                          <!-- REQUIREMENTS -->
                                          <?php
                                            $student_type = USERS::getAllStudentType(); ?>
                                                
                                                <?php foreach($student_type as $key => $val){ ?>
                                                        
                                                          
                                                <div id="student_type_<?= $val['student_type_ID']; ?>" style="/*display:none*/">
                                                    <?php $requirements = USERS::getStudentTypeRequirements($val['student_type_ID']); ?>
                                                    <?php foreach($requirements as $key1 => $val1){ ?>
                                                                
                                                       <?php
                                                      $check =  USERS::checkReqByUser($val1['req_ID'],$siiid);
                                                       ?>    
                                                                
                                                                
                                                        <div class="form-group">    
                                                                 <label for="user_UNAME" class="control-label col-sm-3">    <?= $val1['req_NAME']; ?></label>
                                                                <div class="col-md-3">
                                                              <select class="form-control" name="<?= $val1['req_ID']; ?>">
                                                                    <option  value="0" <?php if($check['status']==0){echo "selected";} ?>>Not Passed</option>
                                                                    <option value="1"  <?php if($check['status']==1){echo "selected";} ?>>Passed</option>
                                                                    
                                                               </select>    
                                                                
                                                                </div>
                                                        
                                                        </div>
                                                    <?php } ?>  
                                                </div>
                                                        <?php } ?>    
                                                      
                                               <br/><br/>
                                                      
                                
                                      <!-- REQUIREMENTS -->  
                            
                            
                                <legend><i class="fa fa-user"></i> CONTACT PERSON (Parent/Guardian) In Case of Emergency:</legend>
                                        <!-- Guardian & Relationship-->
                                        <div class="form-group">
                                            <label for='user_EMAIL' class='col-md-5'>Guardian</label>
                                            <label for='user_EMAIL' class='col-md-4'>Relationship</label>
                                        </div>
                                        <div class="form-group">
                                            <label for='user_EMAIL' class='col-md-5'>
                                                    <input name='user_GUARDIAN' value="<?= $getsingle1['sai_name_of_guardian']; ?>" class='form-control' type='text'/>
                                            </label>
                                            <label for='user_EMAIL' class='col-md-4'>
                                                    <input name='user_RELATIONSHIP' value="<?= $getsingle1['sai_relationship']; ?>"  class='form-control' type='text' />
                                            </label>
                                        </div>
                                        <!-- Occupation & Contact Number-->
                                        <div class="form-group">
                                            <label for='user_EMAIL' class='col-md-5'>Occupation</label>
                                            <label for='user_EMAIL' class='col-md-4'>Contact No.</label>
                                        </div>
                                <div class="form-group">
                                    <label for='user_EMAIL' class='col-md-5'><input name='user_OCCUPATION' class='form-control' type='text' id="user_OCCUPATION" value="<?= $getsingle1['sai_occupation']; ?>" /></label>
                                    <label for='user_EMAIL'  class='col-md-4'>
                                            
                                            <input name='user_GUARDIAN_CONTACT' value="<?= $getsingle1['sai_tel_cell_number']; ?>" class='form-control' type='text'/>
                                    </label>
                                  </div>
                                        <!-- Guardian Address -->
                                        <div class="form-group">
                                            <label for='user_EMAIL' class='col-md-3'>Guardian Address</label>
                                        </div>
                                        <div class="form-group">
                                             <div class="col-sm-9"><input id="G1" name='user_GUARDIAN_ADDRESS' class='form-control' type='text' value="<?= $getsingle1['sai_address']; ?>" /></div>
                                        </div>    
                                        <!-- Elem & Graduated date-->
                                        <legend><i class="fa fa-user"></i> Educational Attainment</legend>
                                        <div class="form-group">
                                            <label for='user_EMAIL' class='col-md-5'>Elementary</label>
                                            <label for='user_EMAIL' class='col-md-4'>Date Graduated</label>
                                        </div>
                                        <div class="form-group">
                                            <label for='user_EMAIL' class='col-md-5'>
                                                    <input name='user_ELEMENTARY' class='form-control' type='text' list="elem" value="<?= $getsingle1['sai_elementary_graduated']; ?>" />
                                                    
                                                     <?php
                                                    $elem = USERS::getAllElementary();
                                                    ?>
                                                    <datalist id="elem">
                                                    <?php foreach($elem as $key => $val){ ?>
                                                        <option value="<?= $val['sai_elementary_graduated']; ?>" ></option>
                                                    <?php } ?>    
                                                    </datalist>
                                                    
                                                    
                                            </label>
                                            <label for='user_EMAIL' class='col-md-4'>
                                                <select name="user_ELEM_YR" class='form-control'>  
                                                <option value='' readonly>YEAR</option>
                                                <?php for($i=2014;$i>=1980;$i--){?>
                                                    <option value="<?php echo ($i-1)." - ".$i; ?>" <?php if($getsingle1['sai_elementary_year_graduated']==($i-1).' - '.$i){echo "selected";} ?>><?php echo ($i-1)." - ".$i; ?></option>
                                                <?php }?>
                                                </select>          
                                            </label>
                                        </div>    
                                        <!-- Highschool & Graduated date-->
                                        <div class="form-group">
                                            <label for='user_EMAIL' class='col-md-5'>High School</label>
                                            <label for='user_EMAIL' class='col-md-4'>Date Graduated</label>
                                        </div>
                                        <div class="form-group">
                                            <label for='user_EMAIL' class='col-md-5'>
                                                    <input name='user_HIGHSCHOOL' class='form-control' list="highschools" type='text' value="<?= $getsingle1['sai_highschool_graduated']; ?>" />
                                                    
                                                    
                                                    <?php
                                                    $highschools = USERS::getAllHighSchool();
                                                    ?>
                                                    
                                                    <datalist id="highschools">
                                                    <?php foreach($highschools as $key => $val){ ?>
                                                        <option value="<?= $val['sai_highschool_graduated']; ?>"></option>
                                                    <?php } ?>    
                                                    </datalist>
                                                    
                                                    
                                            </label>
                                            <label for='user_EMAIL' class='col-md-4'>
                                                <select name="user_HS_YR" class='form-control'>  
                                                <option value='' readonly>YEAR</option>
                                                <?php for($i=2018;$i>=1980;$i--){?>
                                                    <option value="<?php echo ($i-1)." - ".$i; ?>" <?php if($getsingle1['sai_highschool_year_graduated']==($i-1).' - '.$i){echo "selected";} ?>><?php echo ($i-1)." - ".$i; ?></option>
                                                <?php }?>
                                                </select>          
                                            </label>
                                        </div> 
                                            <!-- Occupation & Contact Number-->
                                            <div class="form-group">
                                                <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-5'>College Last Attended</label>
                                                <label for='user_CLA_DATE' class='col-md-4'>Date</label>
                                            </div>
                                            <div class="form-group">
                                                <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-5'>
                                                <input name='user_COLLEGE_LAST_ATTENDED' list="college" class='form-control' type='text' value="<?= $getsingle1['sai_college_last_attended']; ?>" />
                                                <?php
                                                        $college = USERS::getAllCollege();
                                                        ?>
                                                        
                                                        <datalist id="college">
                                                        <?php foreach($college as $key => $val){ ?>
                                                            <option value="<?= $val['sai_college_last_attended']; ?>"></option>
                                                        <?php } ?>    
                                                        </datalist>
                                              </label>
                                                <label for='user_EMAIL' class='col-md-4'>
                                                        <input name='user_CLA_DATE' class='form-control' type='text' value="<?= $getsingle1['sai_cla_inclusive_date']; ?>"/>
                                                </label>
                                </div>    
                                
                                    
                                    
                                    <div class="form-group">
                                                <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-5'>Student Status</label>
                                                <label for='user_EMAIL' class='col-md-4'>Graduating</label>
                                            </div>
                                    
                                    
                                <div class="form-group">
                                                <div>
                                                <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-5'>
                                                    
                                                          <select name="student_status" id="student_type_ID" class="form-control">
                                                          <option readonly='' value="0">Status</option>
                                                          
                                                          <?php
                                                          $status = USERS::getAllstudentStatus();
                                                          ?>
                                                        <?php foreach($status as $key => $val){ ?>
                                                            <option <?php if($val['student_status_ID'] == $getsingle1['sai_status']) { echo 'SELECTED=SELECTED'; }  ?> value="<?= $val['student_status_ID'];?>"><?= $val['student_status_NAME'];?></option>
                                                        <?php } ?>
                                                        
                                                             </select>  
                                                 </label>
                                                <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-5'>
                                                         <select name="student_graduating" id="student_type_ID" class="form-control">
                                                            <option <?php if(1 == $getsingle1['sai_graduating']) { echo 'SELECTED=SELECTED'; }  ?> value="1">Yes</option>
                                                            <option <?php if(0 == $getsingle1['sai_graduating']) { echo 'SELECTED=SELECTED'; }  ?> value="0">NO</option>
                                                         </select>
                                                </label>
                                                </div>
                                                
                                                
                                              
                                </div>      
                             
                                 <div class="form-group">
                                            <label for='user_EMAIL' class='col-md-6'>How did you know about us:</label>
                                        </div>
                                        <div class="form-group">
                                             <div class="col-sm-6">
                                              <?php $admittedList = USERS::getAdmittedList();
                                                #print_r($admittedList);
                                                 ?>
                                                <select class="form-control" name="student_admit_ID" >
                                               <option value="">Please Choose:</option>
                                                <?php foreach ($admittedList as $key => $val){ ?>
                                                    <option value="<?= $val['student_admit_ID']; ?>" <?php if($val['student_admit_ID'] == $getsingle['student_admit_ID']){ echo "SELECTED=SELECTED"; } ?>><?= $val['student_admit_NAME']; ?></option>
                                                <?php } ?>    
                                                </select>
                                             </div>
                                         </div>
                                            
                                           <?php
                                          # print_r($getsingle1);
                                           ?>
                                
                                
                                
                                        <div class="form-group">
                                                <label for='si_FNAME' class='col-md-3'>Admitted Date</label>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3">
                                                <input name='user_DATEADMITTED'  class='form-control datepicker' type='text' required value="<?= $getsingle['admitted_date']; ?>"   style="text-transform:uppercase"/>
                                            </div>
                                            
                                        </div>
                                           
                                            <br/>
                                            <br>
                                <div class="form-group">
                                    <div class="col-md-5">
                                        <label class="control-form">ENCODED BY: &nbsp;<?= $getsingle['added_by']; ?></label>
                                    <input name='sai_ID' placeholder="0000000" class='form-control' type='hidden' style="text-transform:uppercase" value="<?= $getsingle1['sai_ID']; ?>" />
                            
                                    </div>
                                    <div class="col-md-5">
                                        <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Update User</button>
                                    </div>
                                </div>
                            
                            
                            </fieldset>
                        </form>
                    </div>
                </div>
            
            
                    <?php break;  
                        case "search":
                        include "searching.php";                        
                        ?>
                            
                    <?php break; 
                        case "view":
                    ?>
                        <?php
                         if(isset($_GET['id']))
                        {
                       
                            $siiid =$_GET['id'];
                            $getsingle = USERS::getSingle(array("si_ID"=>"DESC"),$siiid  );
                            $getsingle1 = USERS::getSingleSAI(array("si_ID"=>"DESC"), $siiid );
                            #print_r($getsingle);
                            #print_r($getsingle1);
                            AUDIT::ins($user['account_ID'],"Student Information","Visit student record of (". $getsingle['student_ID'] .")".$getsingle['si_FNAME']." ".$getsingle['si_MNAME']." ".$getsingle['si_LNAME']);
                        
                            
                        ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                <i class="fa fa-folder-o"></i> Diploma </h4> 
                        </div>
                        <div class="panel-body">
                            <form action="student_information.php?action=edit&id=<?= $_GET['id']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well' name=form1>
                                    <fieldset>
                                            <legend><i class="fa fa-user"></i> Student Personal Information</legend>
                                            
                                            
                                            <div class="form-group">
                                                <label for='user_EMAIL' class='col-md-3'>Student No.</label>
                                                <label for='user_EMAIL' class='col-md-3'> <?= $getsingle['student_ID'];?> </label>
                                            </div>
                                            
                                            <!-- Name -->
                                            <div class="form-group">
                                                <label for='si_FNAME' class='col-md-3'>Name</label>
                                                <label class="col-sm-6"><?=  $getsingle['si_FNAME'] ?>
                                                    <?=  $getsingle['si_MNAME'] ?>
                                                    <?=  $getsingle['si_LNAME'] ?>
                                                </label>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label  class='col-md-3'>Date Of Birth</label>
                                                <div   class='col-md-3'><?= date("F/j/Y",strtotime( $getsingle['si_BIRTHDATE'])); ?></div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for='user_GENDER' class='col-md-3' >Gender</label>
                                                <div for='user_GENDER' class='col-md-3' ><?= $getsingle['si_GENDER']; ?></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="user_Age" class="col-md-3">Age</label>
                                                <?php
                                         $date = new DateTime($getsingle['si_BIRTHDATE']);
                                         $now = new DateTime();
                                         $interval = $now->diff($date);
                                        ?>
                                                <div for='user_Age' class='col-md-3' ><?= $interval->y ?></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="user_Height" class="col-md-3">Height</label>
                                                <div for='user_Height' class='col-md-3' ><?= $getsingle1['sai_height']; ?></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="user_Weight" class="col-md-3">Weight</label>
                                                <div for='user_Weight' class='col-md-3' ><?= $getsingle1['sai_weight']; ?></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="user_Religion" class="col-md-3">Religion</label>
                                                <div for='user_Religion' class='col-md-3' ><?= $getsingle1['sai_religion']; ?></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="user_CivilStatus" class="col-md-3">Civil Status</label>
                                                <div for='user_CivilStatus' class='col-md-3' ><?= $getsingle1['sai_civil']; ?></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="user_SpecialSkill" class="col-md-3">Special Skills</label>
                                                <div for='user_SpecialSkill' class='col-md-6' ><?= $getsingle1['sai_special_skills']; ?></div>
                                            </div>
                                            <div class="form-group">
                                                <label for="user_placeADDRESS" class="col-md-3">Place of Birth</label>
                                                <div for='user_placeADDRESS' class='col-md-6' ><?= $getsingle['si_BIRTHPLACE']; ?></div>
                                            </div>
                                           
                                            <!-- Present Address -->
                                            <div class="form-group">
                                                <label for='user_ADDRESS' class='col-md-3'>Present Address</label>
                                                <div for='user_ADDRESS' class='col-md-6'><?= $getsingle['si_STREET']; ?>, <?= $getsingle['si_BRGY']; ?>, <?= $getsingle['si_CITY']; ?>, <?= $getsingle['si_DISTRICT']; ?>,<?= $getsingle['si_PROVINCE']; ?></div>
                                            </div>
                                            <div class="form-group">
                                               
                                                <label for="present_ContactNo." class="col-md-3">Contact No.</label>
                                                <div for='user_ADDRESS' class='col-md-6'><?= $getsingle['si_CONTACT']; ?></div>
                                            </div>

                                            <!--  Permanent Address -->
                                            <div class="form-group">
                                                <label for='user_ADDRESS' class='col-md-3'>Permanent Address</label>
                                                <div for='user_ADDRESS' class='col-md-6'><?= $getsingle1['sai_present_addr_st']; ?>, <?= $getsingle1['sai_present_brgy']; ?>, <?= $getsingle1['sai_present_city']; ?>, <?= $getsingle1['sai_present_district']; ?>,<?= $getsingle1['sai_present_prov']; ?></div>
                                            </div>

                                            <div class="form-group">
                                                
                                                <label for="permanent_ContactNo." class="col-md-3">Contact No.</label>
                                                <div for='user_ADDRESS' class='col-md-6'><?= $getsingle1['sai_present_contact']; ?></div>
                                            </div>
                                            
                                            
                                            <!-- Email Address -->
                                            <div class="form-group">
                                                    <label for='user_EMAIL' class='col-md-3'>E-mail Address</label>
                                                    <div class='col-md-6' ><?= $getsingle['si_EMAIL'];?></div>
                                            </div>
                                            <!-- <div class="form-group">
                                                    <label for='user_EMAIL' class='col-md-3'>Contact No.</label>
                                                    <div class="col-md-6"><?= $getsingle['si_CONTACT'];?></div>
                                            </div> -->
                                            <div class="form-group">
                                                    <label for='user_Course' class='col-md-3'>Course</label>
                                                    <div class="col-md-6">111111&nbsp;</div>
                                            </div>
                                            
                                            <br/>
                                            
                                    
                                    
                                                <!-- Requirements Submitted -->
                                                <legend><i class="fa fa-edit"></i> Requirements Submitted</legend>
                             
                             
                             
                                    <!-- STUDENT TYPE -->
                                              <div class="form-group">
                                                    <label for="user_UNAME" class="control-label col-sm-2">Student Type</label>
                                                    <div class="col-md-5">
                                                         <?php
                                                                $student_type = USERS::getAllStudentType();
                                                                ?>
                                                              <select  name="student_type_ID" id="student_type_ID" class="form-control">
                                                                <?php foreach($student_type as $key => $val){ ?>
                                                                    <option value="<?= $val['student_type_ID']; ?>"  <?php if($getsingle['student_type_ID']==$val['student_type_ID']){echo "selected";} ?>><?= $val['student_type_NAME']; ?></option>
                                                                <?php } ?>    
                                                      </select> 
                                                    </div>
                                    </div>
                                               <!-- STUDENT TYPE --> 
                                    <script type="text/javascript">
                                                    $(document).ready(function(){
                                                        $("#student_type_ID").change(function(){
                                                            $( "#student_type_ID option:selected").each(function(){
                                                                
                                                                  <?php foreach($student_type as $key => $val){ ?>
                                                                
                                                                if($(this).attr("value")=="<?php print($val['student_type_ID']); ?>"){
                                                                       <?php foreach($student_type as $key1 => $val1){ ?>   
                                                                            <?php if($val['student_type_ID'] == $val1['student_type_ID'])
                                                                            { ?>     
                                                                               $("#student_type_<?php echo $val1['student_type_ID']; ?>").show(); 
                                                                                  $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',true);                      
                                                                                  
                                                                                  
        $("form :input").attr('disabled',true); 
        $("form #update").attr('disabled',false);                                                                                 
        $("form #print").attr('disabled',false);                                                                                 
                                                                             <?php }else{ ?> 
                                                                              $("#student_type_<?php echo $val1['student_type_ID']; ?>").hide();
                                                                              $("#student_type_<?php echo $val1['student_type_ID']; ?> :input").attr('disabled',true);
                                                                            <?php } ?>  
                                                                       <?php } ?>     
                                                                }
                                                                
                                                                <?php } ?>
                                                               
                                                            });
                                                        }).change();
                                                    });
                                                </script>
                                    
                                                <div class="form-group">
                                                   
                                                   
                                                   <label for="user_UNAME" class="control-label col-sm-3">Requirements</label>
                                                     <label for="user_UNAME" class="control-label col-sm-2">Status</label>   
                                                </div>
                                    
                                    
                                              <!-- REQUIREMENTS -->
                                              <?php
                                                $student_type = USERS::getAllStudentType(); ?>
                                                    
                                                    <?php foreach($student_type as $key => $val){ ?>
                                                            
                                                              
                                                    <div id="student_type_<?= $val['student_type_ID']; ?>" style="/*display:none*/">
                                                        <?php $requirements = USERS::getStudentTypeRequirements($val['student_type_ID']); ?>
                                                        <?php foreach($requirements as $key1 => $val1){ ?>
                                                                    
                                                           <?php
                                                          $check =  USERS::checkReqByUser($val1['req_ID'],$siiid);
                                                           ?>    
                                                                    
                                                                    
                                                            <div class="form-group">    
                                                                     <label for="user_UNAME" class="control-label col-sm-3">    <?= $val1['req_NAME']; ?></label>
                                                                    <div class="col-md-3">
                                                                  <select  class="form-control" name="<?= $val1['req_ID']; ?>">
                                                                        <option  value="0" <?php if($check['status']==0){echo "selected";} ?>>Not Passed</option>
                                                                        <option value="1"  <?php if($check['status']==1){echo "selected";} ?>>Passed</option>
                                                                        
                                                                   </select>    
                                                                    
                                                                    </div>
                                                            
                                                            </div>
                                                        <?php } ?>  
                                                    </div>
                                                            <?php } ?>    
                                                          
                                                   <br/><br/>
                                                          
                                    
                                          <!-- REQUIREMENTS -->  
                                
                                
                                    <legend><i class="fa fa-user"></i> Guardian Information</legend>
                                            <!-- Guardian & Relationship-->
                                            <div class="form-group">
                                                <label for='user_EMAIL' class='col-md-3'>Guardian</label>
                                                <span><?= $getsingle1['sai_name_of_guardian']; ?> / <?= $getsingle1['sai_relationship']; ?></span>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for='user_EMAIL' class='col-md-3'>Occupation</label>
                                                <?= $getsingle1['sai_occupation']; ?>
                                            </div>
                                    
                                    <div class="form-group">
                                        <label for='user_EMAIL' class='col-md-3'>Contact No.</label>
                                        <?= $getsingle1['sai_tel_cell_number']; ?>
                                      </div>
                                      
                                    <div class="form-group">
                                        <label for='user_EMAIL' class='col-md-3'>Guardian Address</label>
                                        <?= $getsingle1['sai_address']; ?>
                                    </div>
                                    
                                    <br />
                                    <legend><i class="fa fa-user"></i> Educational Background</legend>      
                                            <!-- Elem & Graduated date-->
                                            <div class="form-group">
                                                <label for='user_EMAIL' class='col-md-3'>Elementary</label>
                                                <?= strtoupper($getsingle1['sai_elementary_graduated']); ?> / <?= $getsingle1['sai_elementary_year_graduated']; ?>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for='user_EMAIL' class='col-md-3'>High School</label>
                                                <?= $getsingle1['sai_highschool_graduated']; ?> / <?= $getsingle1['sai_highschool_year_graduated']; ?>
                                            </div>
                                            
                                            <div class="form-group">
                                                    <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-3'>College Last Attended</label>
                                                    <?php if($getsingle1['sai_college_last_attended'] == NULL){ echo "None"; }else{ echo $getsingle1['sai_college_last_attended']; } ?> / 
                                                    <?php if($getsingle1['sai_cla_inclusive_date']== NULL){ echo "None"; }else{ echo $getsingle1['sai_cla_inclusive_date']; } ?>
                                            </div>    
                                    
                                   <br />
                                         <legend><i class="fa fa-user"></i> Student Residency</legend>   
                                            <div class="form-group">
                                                <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-3'>Status Type</label>
                                                        <?php
                                                          if($getsingle1['sai_status'] == NULL){
                                                            echo "N/A";
                                                          }else{
                                                              $status = USERS::getAllstudentStatus();
                                                              foreach($status as $key => $val){
                                                                
                                                                 if($val['student_status_ID'] == $getsingle1['sai_status']) 
                                                                 { echo $val['student_status_NAME']; } 
                                                             } 
                                                         }
                                                         ?>
                                            </div>
                                            <div class="form-group">
                                                <label for='user_COLLEGE_LAST_ATTENDED' class='col-md-3'>Graduating</label>
                                                <?php if(1 == $getsingle1['sai_graduating']) { echo 'YES'; }else{ echo "NO"; } ?>
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <label  class='col-md-3'>Admitted Date</label>
                                                
                                                <?php
                                                #print_r($getsingle1);
                                                ?>
                                                <?= date("F j, Y", strtotime($getsingle['admitted_date'])); ?>
                                                </label>
                                            </div>
                                            
                                             <div class="form-group">
                                                <label  class='col-md-3'>How Did You Know About Us?</label>
                                                
                                                <?php
                                                #print_r($getsingle1);
                                                ?>
                                                <?php $type = USERS::getAdmittedbyID($getsingle['student_admit_ID']); 
                                                    echo $type['student_admit_NAME'];
                                                ?>
                                                </label>
                                            </div>
                                
                                
                                    <legend><i class="fa fa-user"></i> Enrolment History</legend>   

                                    <div class="form-group">
                                        <div class="col-md-12">
                                             <?php
                                                $sem = SEM::getallsemsofstudent($getsingle['si_ID']);
                                                #print_r($sem);
                                              ?>

                                            <table class="table">
                                                <tr>
                                                    <td class="b">Academic Year / Semester</td>
                                                    <td class="b">Date Enrolled</td>
                                                    <td class="b">Payment Option</td>
                                                </tr>
                                                 <?php foreach($sem as $key1 => $val1){ ?> 
                                                    <?php 
                                                    $accinfo =  SUBJENROL::getenrollmentofstudentpersem($getsingle['si_ID'],$val1['sem_ID']);
                                                    #$info =  USERS::viewSingleStudent($accinfo['si_ID']);
                                                    #var_dump($accinfo);

                                                    #$mygrade = SUBJENROL::gradebyid($id);
                                                    #var_dump($mygrade);
                                                    
                                                    $po_info = SUBJSCHED::getPaymentOptionByID($accinfo['po_ID']);
                                                    ?>
                                                <tr>
                                                    <td><?= $val1['sem_NAME']; ?></td>
                                                  
                                                    <td><?= $accinfo['date_enrolled']; ?></td>
                                                    <td><?= $po_info['po_NAME']; ?></td>
                                                </tr>
                                                <?php  } ?>
                                            </table>
                                        </div>
                                    </div>
               
                              
                                    <br>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label class="control-form">ENCODED BY: &nbsp;<?= $getsingle['added_by']; ?></label>
                                        <input name='sai_ID' placeholder="0000000" class='form-control' type='hidden' style="text-transform:uppercase" value="<?= $getsingle1['sai_ID']; ?>" />
                                
                                        </div>
                                        <div class="col-md-4">
                                              <a target="_new" href="student_clearance.php?id=<?php echo $_GET['id']; ?>" id="print" type="button" class='btn btn-success btn-block'><i class='fa fa-print'></i> Print Student Clearance</a>
                                        </div>
                                        
                                        <!-- <div class="col-md-4">
                                            <button id="update"  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Update Cadet</button>

                                        </div>
                                        <div class="col-md-4">
                                            <a target="_new" href="info_sheet.php?id=<?php echo $_GET['id']; ?>" id="print" type="button" class='btn btn-success btn-block'><i class='fa fa-print'></i> Print Cadet Info</a>
                                            
                                        </div> -->
                                    </div>

                                    <!-- <div class="form-group">
                                        <div class="col-md-4">
                                            
                                        </div>
                                        <div class="col-md-4">
                                              <a target="_new" href="student_clearance.php?id=<?php echo $_GET['id']; ?>" id="print" type="button" class='btn btn-success btn-block'><i class='fa fa-print'></i> Print Cadet Clearance</a>
                                        </div>
                                        <div class="col-md-4">
                                            <a target="_new" href="student_good_moral.php?id=<?php echo $_GET['id']; ?>" id="print" type="button" class='btn btn-success btn-block'><i class='fa fa-edit'></i> Print Good Moral</a>
                                            
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <div class="col-md-4">
                                            
                                        </div>
                                        <div class="col-md-4">
                                              <a target="_new" href="student_honorable.php?id=<?php echo $_GET['id']; ?>" id="print" type="button" class='btn btn-success btn-block'><i class='fa fa-print'></i> Print Honorable Dismissal</a>
                                        </div>
                                        <div class="col-md-4">
                                            <a target="_new" href="student_grade_ledger.php?id=<?php echo $_GET['id']; ?>" id="print" type="button" class='btn btn-success btn-block'><i class='fa fa-edit'></i> Print Grade Ledger</a>
                                            
                                        </div>
                                    </div> -->
                                
                                 <!-- <div class="form-group">
                                        <div class="col-md-4">
                                            
                                        </div>
                                        <div class="col-md-4">
                                              <a target="_new" href="student_diploma.php?id=<?php echo $_GET['id']; ?>" id="print" type="button" class='btn btn-success btn-block'><i class='fa fa-print'></i> Print Diploma</a>
                                        </div>
                                        <div class="col-md-4">
                                            <a target="_new" href="student_scholastic_trail.php?id=<?php echo $_GET['id']; ?>" id="print" type="button" class='btn btn-success btn-block'><i class='fa fa-edit'></i> Print Scholastic Trail</a>
                                            
                                        </div>
                                    </div>
                                 -->
                                
                                </fieldset>
                            </form>
                        </div>
                    </div>
                        
                        
                        
                        
                        
                         <?php         
                        }else{
                        
                        
                        AUDIT::ins($user['account_ID'],"Student Information","Visit Student Information Module");
                        
                         ?>
                    
                    
                    
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <table>
                                        <tr>
                                            <td width="50%">
                                                <h4 class="nav-pills" style="margin-top: 9px; margin-bottom: 9px"><i class="fa fa-folder-o"></i> Print Student Clearance <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
                                            </td>

                                            <td>
                                                <div class="input-group col-md-12 hide">
                                                    
                                                    <input id="searchItem" class="searchItem form-control"  type="text" autofocus="autofocus"  style="text-transform: uppercase;"/>
                                                    
                                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                </div>
                                            </td>
                                                <!-- <script>
                                                $(document).ready(function() {
                                                $('#searchItem').keyup(function(){
                                                var s_Item = $('#searchItem').val();
                                                    
                                                    if(s_Item.length >= 3)
                                                    {
                                                         $.ajax({
                                                            type: "POST",
                                                            url: "search.php?action=si.php",
                                                            data: 'search_term=' + s_Item,
                                                            success: function(msg){
                                                                /* $('#resultip').html(msg); */
                                                                    $("#display_result").show();
                                                                    $("#display_result").html(msg);
                                                                    
                                                                    $("#display_hide").hide();
                                                                
                                                            }
                                            
                                                        }); // Ajax Call
                                                    
                                                        //alert(s_Item);
                                                        }else{
                                                            $("#display_hide").show();
                                                            $("#display_result").hide();   
                                                        }
                                                    });
                                                });         
                                                </script> -->
                                                <script>
                                                $(document).ready(function() {


                                                    function postData(){
                                                        var s_Item = $('#searchItem').val();
                                                        if(s_Item.length >= 3)
                                                        {
                                                            console.log("Searching")
                                                            $.ajax({
                                                                    type: "POST",
                                                                    url: "search.php?action=si.php",
                                                                    data: 'search_term=' + s_Item,
                                                                    beforeSend: function ( xhr ) {
                                                                        $("#spinner").show();
                                                                       //Add your image loader here
                                                                    },
                                                                    success: function(msg){
                                                                        /* $('#resultip').html(msg); */
                                                                        $("#spinner").hide();
                                                                        $('#resultip').html(msg); 
                                                                        $("#display_result").show();
                                                                        $("#display_result").html(msg);
                                                                        $("#display_hide").hide();
                                                                        
                                                                    }
                                                    
                                                                })
                                                        }else{
                                                            $("#display_hide").show();
                                                            $("#display_result").hide();
                                                        }
                                                        return false;
                                                    }

                                                    $(function() {
                                                        var timer;
                                                        $("#searchItem").bind('keyup input',function() {
                                                            timer && clearTimeout(timer);
                                                            timer = setTimeout(postData, 300);
                                                        });
                                                    });


                                                    /*$('#searchItem').keyup(function(){
                                                    var s_Item = $('#searchItem').val();
                                                    
                                                        if(s_Item.length >= 3)
                                                        {
                                                             $.ajax({
                                                                type: "POST",
                                                                url: "search.php?action=si.php",
                                                                data: 'search_term=' + s_Item,
                                                                success: function(msg){
                                                                     $('#resultip').html(msg); 
                                                                        $("#display_result").show();
                                                                        $("#display_result").html(msg);
                                                                        
                                                                        $("#display_hide").hide();
                                                                    
                                                                }
                                                
                                                            }); 
                                                        }else{
                                                            $("#display_hide").show();
                                                            $("#display_result").hide();   
                                                        }
                                                    });*/
                                                });         
                                                </script>
                                            
                                            <td width="5%">
                                                <div class="col-md-1">   
                                                    <!-- <button style="margin-top: 3px; margin-bottom: 3px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('st_diploma.php?action=add','_self');"><i class='fa fa-plus'></i></button> -->
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="panel-body">
                                    <div id="display_result"></div>
                                    <div id="display_hide">
                                   
                                    <table border="0" cellspacing="5" cellpadding="5" style="width:40%;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <input name='' id="myInput" placeholder="Search by Student No., First Name, Middle Name, Last Name or Program" class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
                                                    </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="display nowrap table table-hover table-responsive table-striped text-md" id="cadetView">
                                      <thead>
                                        <th style="width:15%;">Student No</th>
                                        <th style="width:15%;">First Name</th>
                                        <th style="width:15%;">Middle Name</th>
                                        <th style="width:15%;">Last Name</th>
                                        <th style="width:15%;">Program</th>
                                        <th style="">Action</th>  
                                    </thead>
                                    <tbody>
                                        <?php
                                         //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                        $viewStudents = USERS:: getAllStudentInformation(array("si_ID"=>"DESC"));
                                        /* if(count($viewStudents)>=10) 
                                         {
                                             $pagination2 = new Pagination($viewStudents, 10, Request::get("p"));
                                         }else{
                                             $pagination2 = new Pagination($viewStudents, 10, NULL);
                                         }

                                         $viewStudents = $pagination2->get_array();*/

                                         if($viewStudents) {

                                            foreach($viewStudents as $key => $value){
                                        ?>
                                            <tr>
                                                <td>
                                                    <?php echo strtoupper($value['student_ID']); ?>
                                                <td>
                                                    <?php echo $value['si_FNAME']; ?>
                                                <td>
                                                    <?php echo $value['si_MNAME']; ?>
                                                <td>
                                                    <?php echo $value['si_LNAME']; ?>
                                                <td>
                                                    <?php echo USERS::getCourse1($value['si_ID']); ?>
                                                <td>
                                                    <a target="_new" href="student_clearance.php?id=<?php echo $value['si_ID']; $_GET['id']; ?>" id="print" type="button" class='btn btn-success P3C4_PRINTCLEARANCE btn-xs'> Print Clearance</a>
                                                    <!-- <a href="st_diploma.php?action=view&id=<?php echo $value['si_ID']; ?>" class='btn btn-success btn-xs'>
                                                            View full info
                                                    </a>

                                                    <a href="st_diploma.php?action=edit&id=<?php echo $value['si_ID']; ?>" class='btn btn-warning btn-xs'>
                                                            Edit
                                                    </a> -->
                                                </td>
                                            </tr>
                                            <?php
                                             } 
                                            }
                                        ?>
                                    </tbody>
                                    </table> 

                                </div>
                            </div>
                                <center>

                                                            </center>
                            </div>
                </div>
            </div>
                 <script type="text/javascript">

             $(window).ready(function() {
                    setTimeout(function () {
                        //console.log("CONVERT")
                    table =  $('#cadetView').DataTable({
                            "order": [],
                             "lengthChange": false,
                             /*"searching": false,*/
                             "info": true, 
                             "ordering": false,
                       });
                       $("#cadetView").show();
                       $(".dataTables_filter").addClass("hide")

                       $('#cadetView thead .th').each( function () {
                            var title = $(this).text();
                            if(title != "Action"){
                                $(this).html( '<input type="text" class="sb form-control" placeholder="'+title+'" />' );
                            }
                        } );

                       // Apply the search
                        table.columns().every(function () {
                            var that = this;
                        // console.log(this.header());
                            $( 'input', this.header() ).on( 'keyup change', function () {
                                if ( that.search() !== this.value ) {
                                    that
                                        .search( this.value )
                                        .draw();
                                }
                            } );
                        } );

                    }, 1);
                })
            </script>

                  <?php } break;
                    default: href("?action=view");
                    }}
                  ?>
            <!-- end row container -->
            <!-- footer-->      
            <?php include (LAYOUTS . "footer.php"); ?>

            <!-- end footer -->
        </div>
        <!-- end container -->

    </body>
</html>
<script type="text/javascript">
    function myFunction() {
        var input, filter, table, tr, td, i;
          input = document.getElementById("myInput");
          // console.log(input);
          filter = input.value.toUpperCase();
          table = document.getElementById("cadetView");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td2 = tr[i].getElementsByTagName("td")[1];
            td1 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            td4 = tr[i].getElementsByTagName("td")[4];
            if (td) {
              if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1 || td4.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }   
        }
    }
</script>