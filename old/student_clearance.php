<?php
include('init.php');
$siiid = clean($_GET['id']);
$getsingle = USERS::getSingle(array("si_ID"=>"DESC"),$siiid  );
$getsingle1 = USERS::getSingleSAI(array("si_ID"=>"DESC"), $siiid );
$clearance = CLEARANCE::getAllClearanceByStudID($siiid);


$user_id = $user['account_ID'];
$action_event = "Print";
$event_desc = "Module: 2 Registrar / 2.2 Find Student, DESCRIPTION: User clicked Print Student Clearance for ".$getsingle['si_FNAME'].' '.$getsingle['si_MNAME'].' '.$getsingle['si_LNAME'];
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="pages\htmlfolder\assets/css/jquery.dataTables.min.css">
  	<!-- <link href="ams\old\pages\htmlfolder\assets/css/style.css" rel="stylesheet"> -->
  	<link href="pages\htmlfolder\assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  	<link href="pages\htmlfolder\assets/css/style.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  	<title>AMS</title>

</head>
<body onload="window.print();">
	<div class="container text-capitalize" style="margin-bottom: 15px; margin-top: 15px">
	    <div class="row">
	      <div class="col-lg-12 col-sm-12 col-md-12">
	        <img src="pages\htmlfolder\assets/img/logo.png">
	      </div>
	    </div>
	    <div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<h4 class="user_info">STUDENT CLEARANCE</h4>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-sm-2 col-md-2">
				<span>Student no:</span>
			</div>
			<div class="col-lg-4 col-sm-4 col-md-4">
				<span><?= $getsingle['student_ID']; ?></span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-sm-2 col-md-2">
				<span>Student Name:</span>
			</div>
			<div class="col-lg-4 col-sm-4 col-md-4">
				<span><?= $getsingle['si_LNAME']; ?>, <?= $getsingle['si_FNAME']; ?> <?= $getsingle['si_MNAME']; ?></span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-sm-2 col-md-2">
				<span>Program:</span>
			</div>
			<div class="col-lg-4 col-sm-4 col-md-4">
				<span><?= USERS::getCourse($getsingle['si_ID']); ?></span>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<table class="table table-bordered">
				    <thead class="">
				      <tr align="center">
				        <th>Academic Year & Semester</th>
				        <th>Reason</th>
				        <th>Date Blocked</th>
				        <th>Blocked by</th>
				        <th>Unblocked Date</th>
				        <th>Unblocked Remarks</th>
				        <th>Unblocked by</th>
				        
				        <th>Status</th>
				      </tr>
				    </thead>
				   
				    <tbody>
				    	<?php foreach($clearance as $key => $val){ ?> 
				    	<tr>
				        	<td><?= $val['sem_NAME']; ?></td>
				        	<td><?= $val['blocked_remakrs']; ?></td>
				        	<td><?= $val['blocked_date']; ?></td>

				        	<td><?php $u1 =  USERS::getID($val['blocked_by']);
				        				echo $u1['account_FNAME']." ".$u1['account_LNAME'];
				        		 ?></td>

				        	<td><?= $val['unblocked_date']; ?></td>
				        	<td><?= $val['unblocked_remarks']; ?></td>
				        	<td>
			        			<?php $u1 =  USERS::getID($val['unblocked_by']);
			        				echo $u1['account_FNAME']." ".$u1['account_LNAME'];
				        		 ?>
				        			 
			        		</td>
				        	<td><?php if($val['clearance_STAT'] == "1"){ echo "Unblocked"; } else { echo "Blocked"; }; ?></td>
				     	</tr>
				     	<?php } ?>
				      <!-- <tr>
				        <td>ACCT BAL.</td>
				        <td>08/20/2017</td>
				        <td>ACCOUNTING</td>
				        <td></td>
				        <td>BLOCKED</td>
				      </tr> -->
				    </tbody>
				  </table>


				  Released By:            <br />
       	     <br />
   	      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b><?= $user['account_FNAME']." ".substr($user['account_MNAME'],0,1).". ".$user['account_LNAME']; ?></b>
          <br />
          &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Admin Staff</td>   
            <br /><br />Released Date: <br />
               &nbsp;&nbsp;&nbsp;<?php echo date("M j, Y"); ?></td>
			</div>
		</div>
	</div>



    <script src="pages\htmlfolder\assets/js/jquery.min.js"></script>
    <script src="pages\htmlfolder\assets/js/jquery.dataTables.min.js"></script>
  	<script src="pages\htmlfolder\assets/js/bootstrap.min.js"></script>

     <?php #var_dump($clearance); ?>

</body>
</html>
