<?php 
//INITIALIZE INCLUDES
    include('init.php');
#SESSION::CheckLogin();
  
    //QUERIES
    if(Request::post()){
        switch(Request::get("action")) {
            case "add": 
            CURRICULLUM::addCurricullum(Request::post());
            #var_dump(Request::post());
            
            break;
            case "edit": CURRICULLUM::update_curriculum($_GET['id'],Request::post()); break;
            #case "update_blog":  BLOG::update(Request::post("blog_info")); break;
            
            #case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
            
            default: Fail::not_found();
        }
    }
    
    if(Request::get()){
        switch(Request::get("action")) {
            case "delete": CURRICULLUM::delete($_GET['id']); break;
        
        }
    }       
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style>
            .select2 {
                width:100%!important;
            }
            
        </style>
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>SCHOOL MANAGEMENT SYSTEM</title>
    </head>
    <body>
        <!-- top nav -->
        <?php include(LAYOUTS . 'top_nav.php'); ?>
        <!-- end nav -->
        <div class="container">
            <!-- banner -->
            <?php include(LAYOUTS . "banner.php"); ?>
            <!-- end banner -->
            <!-- alert messages -->
            <?php SESSION::DisplayMsg(); ?>
            <!-- end of alert messages -->
            <!-- start row container -->
            <!-- 
            <div class="row">
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li class="active">Home</li>
                    </ul>
            </div>  
            </div>
            -->
            <!-- end row container -->
            <!-- row container -->
            <div class="row">
                <!-- left nav -->   
            <?php
                if (!SESSION::isLoggedIn()) {
                    ?>
                <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> Course Categories </h3>
                    </div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                        <li >
                            <a style="cursor:pointer" id="1-link">
                                <span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=1','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CCS (2)
                                </span>
                            </a>
                        </li>
                        <li >
                            <a style="cursor:pointer" id="2-link">
                                <span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=2','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CBA</span>
                            </a>
                        </li>
                        <script>

                                  $( "#1-link" ).click(function() {
                             $( "#1-content" ).slideToggle( "fast" );
                         });
                                  $( "#2-link" ).click(function() {
                             $( "#2-content" ).slideToggle( "fast" );
                         });

                        </script>
                        <!--
                            <li class="active"><a href="#">Computer Studies (#)</a></li>
                            <li class=""><a href="#">Business Course</a></li>
                            <li class="dropdown"><a class="dropdown-toggle" id="link" data-toggle="dropdown" href="#">Cat3 <span class="caret"></span></a>
                                <ul class="nav nav-pills nav-stacked nav-sub"  >
                                    <li><a href="#">Cat3 Sub 1 <span class="caret"></span></a>

                                    <ul class="nav nav-pills nav-stacked  nav-sub"  style="">

                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                    </ul>

                                    </li>
                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                </ul>
                            </li>

                           --> 
                        </ul>
                    </div>
                </div>
                </div>
                <?php   }else{?>
                <div class="col-md-3">
    
                <?php include(PAGES."navigation.php");?>
                </div>
                <?php } ?>
                <!-- end left nav -->
                
                <!-- BODY -->
                
                
                <div class="col-md-9">
                <?php if (SESSION::isLoggedIn()) { ?>
                    <?php
                    switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                            <i class="fa fa-folder-o"></i> Add Curriculum </h4> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <table border='0' class="table table-responsive text-md">
                                    <tr>
                                        <td>&nbsp;
                                            
                                        </td>

                                        <td>
                                            <label for='curricullum_NAME' class=''>Curriculum Name</label>
                                        </td>

                                        <td>

                                            <input name='curricullum_NAME' placholder='Curricullum Name' class='form-control' type='text'/>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;
                                            
                                        </td>

                                        <td>
                                            <label for='course_NAME' class=''>Course Name</label>
                                        </td>

                                        <td>
                                            <select name="course_ID" class="form-control">
                                            <?php
                                                //get all the course name
                                                $getAllCourseName = CURRICULLUM:: getAllCourseName(array("course_NAME"=>"ASC"));
                                                    if($getAllCourseName) {
                                                        foreach($getAllCourseName as $key => $value){
                                                            $coursed_NAME = $value['course_NAME'];
                                                            $coursed_ID = $value['course_ID'];
                                                                echo '<option value="'.$coursed_ID.'">'.$coursed_NAME.'</option>';
                                                        }
                                                    }
                                                
                                            ?>
                                        </select>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan='3' align='right'>
                                            <input type="hidden" name =AccountID value="<?= $user['account_ID'];?>">
                                            <button  class='btn btn-success btn-ms' type='submit'><i class='fa fa-edit'></i> Add curriculum</button>
                                        </td>
                                    </tr>
                            </table>
                        </form>
                    </div>
                    </div>
                    <?php break; 
                        case "edit":
                        $cur_ID =$_GET['id'];
                        $getsingle = CURRICULLUM::getSingle(array("curricullum_ID"=>"DESC"),$cur_ID  );
                        $getCurricullumName1 = CURRICULLUM:: getID($getsingle['course_ID']);
                        //$getCurricullumName1['course_NAME']
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                            <i class="fa fa-folder-o"></i> Update Curriculum </h4> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <table border='0' class="table table-responsive text-md">
                                    <tr>
                                        <td>&nbsp;
                                            
                                        </td>

                                        <td>
                                            <label for='curricullum_ID' class='col-md-6'>Curriculum ID</label>
                                        </td>

                                        <td>
                                            <input name='id' class='form-control' type='hidden' value=""/>
                                            <input name='curricullum_ID' disabled class='form-control' type='text' value="<?= $getsingle['curricullum_ID'];?>"/>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;
                                            
                                        </td>

                                        <td>
                                            <label for='curricullum_NAME' class='col-md-6'>Curriculum Name</label>
                                        </td>

                                        <td>
                                            <input name='curricullum_NAME' class='form-control' type='text' value="<?= $getsingle['curricullum_NAME'];?>"/>
                                            <input type="hidden" name="c_name" value="<?= $getsingle['curricullum_NAME']?>">
                                            <input type="hidden" name="acct_id" value="<?= $user['account_ID'];?>">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>&nbsp;
                                            
                                        </td>

                                        <td>
                                            <label for='course_NAME' class='col-md-6'>Course Name</label>
                                        </td>

                                        <td>
                                            <!--
                                            <input name='course_NAME' class='form-control' type='text' value="<?= $getsingle['course_ID'];?>"/>
                                            -->
                                            <select class="form-control" name="course_ID">
                                            <?php
                                            $course_ID = $getsingle['course_ID'];
                                                $getCurricullumName = CURRICULLUM:: getID($course_ID);
                                                $courseGet_NAME = $getCurricullumName['course_NAME'];
                                                //get all the course name
                                                $getAllCourseName = CURRICULLUM:: getAllCourseName(array("course_NAME"=>"ASC"));
                                                    if($getAllCourseName) {
                                                        //printing the curricullum name
                                                        //the first one to display in list box
                                                        echo '<option value="'.$course_ID.'">'.$courseGet_NAME.'</option>';
                                                        
                                                        foreach($getAllCourseName as $key => $value){
                                                            $coursed_NAME = $value['course_NAME'];
                                                            $coursed_ID = $value['course_ID'];

                                                            if($coursed_NAME!=$courseGet_NAME)
                                                            {
                                                                echo '<option value="'.$coursed_ID.'">'.$coursed_NAME.'</option>';
                                                            }
                                                        }
                                                    }
                                                
                                            ?>
                                            </select>
                                             <input type="hidden" name="course_n" value="<?= $courseGet_NAME;?>">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan='3' align='right'>
                                            <input type="hidden" name =AccountID value="<?= $user['account_ID'];?>">
                                            <input type="hidden" name =beforecurricullum_NAME value="<?= $getsingle['curricullum_NAME'];?>">
                                            <input type="hidden" name =beforecourse_ID value="<?= $getsingle['course_ID'];?>">
                                            <button  class='btn btn-success btn-ms' type='submit'><i class='fa fa-edit'></i> Update Subject</button>
                                        </td>
                                    </tr>
                            </table>
                        </form>
                    </div>
                    </div>
                    <?php break;  
                        case "search":
                        include "searching.php";                        
                        ?>
                            
                    <?php break; 
                        case "view":
                        

                        ?>
                        </script>
                        <script>
                                     function showDiv(divId){
                                                $('#'+divId).show();
                                    }   
                        </script>
                        <?php
                                if(isset($_GET['id']))
                                {
                                $cur_ID = $_GET['id'];

                                //2013-2014
                                    $getCurricullumName = CURRICULLUM:: getCurricullumName($cur_ID);
                                    $curricullumNameGET = $getCurricullumName['curricullum_NAME'];



                                 //$course_ID = $getsingle['course_ID'];
                                   
                                    //Course Name
                                    $course_ID = $getCurricullumName['course_ID'];
                                    $getCourseName = CURRICULLUM:: getID($course_ID);
                                    $courseGet_NAME = $getCourseName['course_NAME']
                                    //msgbox();
                                    

                                    
                                        ?>
                                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                    <!--    
                                        <i class="fa fa-user"></i>

                                        <u><b>".$courseGet_NAME."</b></u>
                                    -->
                                    &nbsp;
                                    <table border='0' width="100%" >
                                        <tr>
                                            <td align="center">
                                                
                                                <u><b><?php echo $courseGet_NAME; ?></b></u>
                                        <tr>
                                            <td align='center'>
                                                <font size='3px'>(<?= $curricullumNameGET ?>)
                                                </font>
                                        
                                    </table>
                                   
                                    
                                </h3>
                                
                            </div>
                            
                            <div class="panel-body">
                            
                                 <center>
                                 <?php if(isset($_GET['hidden']) && $_GET['hidden'] == 'yes') { ?>
                                 <sub><a href="curriculum1.php?action=view&id=<?= $_GET['id'];?>&hidden=no">Hide Unused Semesters</a></sub>
                                   <?php }else{ ?>
                                    <sub><a href="curriculum1.php?action=view&id=<?= $_GET['id'];?>&hidden=yes">Show Hidden Semesters</a></sub>
                                  <?php } ?>  
                                </center>    
                               
                               
                                <table border='0' class="table table-responsive text-md">
                                    
                                    <?php
                                    $getAllSemester = CURRICULLUM:: getAllSemester(array("yr_sem_ID"=>"ASC"));
                                    if($getAllSemester) {
                                    $counTemp = 1;
                                    
                                    
                                    
                                        foreach($getAllSemester as $key => $value){




                                            $curr_subj = CURRICULLUM::getSubjectbycurr($value['yr_sem_ID'],$cur_ID);
                   # print_r($curr_subj);


                       # print_r($curr_subj);
                                            /*
                                            foreach($curr_subj as $key => $val_a){
                                            $subject_ID_a = $val_a['subject_ID'];
                                            $SubjectGET_a = CURRICULLUM:: getSubject($subject_ID_a);
                                            */
                                            //print_r($SubjectGET_a);

                                            //if($SubjectGET_a['subject_ID'] != $val_a['subject_ID'])

                                                //if((sizeof($curr_subj))%2 OR (sizeof($curr_subj))==0)
                                            //(sizeof($curr_subj))

                                            /*
                                               if($counTemp==2)
                                                {
                                                    $counTemp = 1;
                                               ?>
                                                        <td>
                                                <?php
                                                }else{
                                                ?>
                                                    <tr>
                                                        <td>
                                               <?php 
                                                    $counTemp ++;
                                                }
                                            */
                                            ?>

                                            <?php
                                            $totalCount = sizeof($curr_subj);
                                            //if(($totalCount %2 != 2) OR ($temppppp > 0)){
                                            if(($totalCount %2)){
                                                if($counTemp == 2)
                                                {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                    <?php
                                                    $counTemp = 1;
                                                }else{
                                                ?>
                                                    
                                                        <td>
                                                <?php
                                                }
                                                    $counTemp ++;
                                            }
                                    ?>

                                    <!--
                                    <table id="sem<?= $value['yr_sem_ID']; ?>" border="1" class="" style="border-top: solid 4px black; border-bottom: solid 2px black;">
                                    -->
                                    
                                    <table id="sem<?= $value['yr_sem_ID']; ?>" border="1" class="table table-responsive text-md" style="border-top: solid 4px black; border-bottom: solid 2px black;">
                                    
                                                            <th style="border-bottom: solid 2px black;"><?= $value['yr_sem_NAME']; ?> <sup><a style="cursor:pointer;" class="P3C6_ADDSUBJECT" onclick="showDiv('<?= $value['yr_sem_ID']; ?>');">[Add Subject]</a></sup>
                                                                <tr>
                                                                    <td>
                                                                        <table border='0' class="table table-responsive text-md">
                                                                            <th width="10%" hidden="hidden">S-ID
                                                                            <th width="10%">S-Code
                                                                            <th width="30%">Subject Description
                                                                            <th width="10%"  style="text-align:center">Units
                                                                            <th width="10%" style="text-align:center">PreRequisite
                                                                            <th width="10%" style="text-align:center">CoRequisite
                                                                            <th width="" style="text-align:center">Option
                                                                     
                                                                     <form action="curricullum_subjects.php?action=addfcur" method="post"> 

                                                                     <tr id="<?= $value['yr_sem_ID']; ?>" style="display:none;">
                                                                                    <td valign="middle">Subject
                                                                                    
                                                                                    <td colspan="3"> 
                                                                                    <select name="subject_ID" class="form-control">
                                                                                        <?php $getallsubject2= CSCONTROLLER::getallsubjects(array("subject_CODE"=>"ASC"));   
                                                                                            foreach($getallsubject2 as $key2 => $value2){
                                                                                        ?>
                                                                                        <option value=<?= $value2['subject_ID']?>><?= $value2['subject_CODE']."(".$value2['subject_ID'].") ".$value2['subject_DESCRIPTION']." - ".$value2['LEC_UNIT']."/".$value2['LAB_UNIT']?></option>
                                                                                        <?php }  ?>
                                                                                        
                                                                                    </select>
                                                                                    <td colspan="1">
                                                                                    <select name=pre_req[] id="pre_req<?= $value['yr_sem_ID']; ?>" class="pre_req form-control" multiple="multiple">
                                                                                        <option value="">None</option>
                                                                                        <?php $getallsubject= CSCONTROLLER::getallsubjects(array("subject_CODE"=>"ASC"));   
                                                                                            foreach($getallsubject as $key3 => $value3){
                                                                                        ?>
                                                                                        <option value=<?= $value3['subject_ID']?>><?= $value3['subject_CODE']." - ".$value3['subject_DESCRIPTION']?></option>
                                                                                        <?php }  ?>
                                                                                    </select></td>
                                                                                    <td colspan="1">
                                                                                    <select name=co_req[] id="pre_req<?= $value['yr_sem_ID']; ?>" class="pre_req form-control" multiple="multiple">
                                                                                        <option value="">None</option>
                                                                                        <?php $getallsubject= CSCONTROLLER::getallsubjects(array("subject_CODE"=>"ASC"));   
                                                                                            foreach($getallsubject as $key3 => $value3){
                                                                                        ?>
                                                                                        <option value=<?= $value3['subject_ID']?>><?= $value3['subject_CODE']." - ".$value3['subject_DESCRIPTION']?></option>
                                                                                        <?php }  ?>
                                                                                    </select></td>

                                                                                    <td>
                                                                                    
                                                                                    
                                                                                            <button  class='btn btn-success btn-ms' type='submit'><i class='fa fa-edit'></i> Add Subject</button>
                                                                                            
                                                                                                <input type="hidden" value="<?= $_GET['id']; ?>" name="curricullum_ID" />
                                                                                                <input type="hidden" value="<?= $value['yr_sem_ID']; ?>" name="yr_sem_ID" />
                                                                   </tr>   
                                                                   </form>                   
                                                            <?php
                                                                foreach($curr_subj as $key => $val)
                                                                {
                                                            ?>
                                                                <tr>
                                                                    <!-- <td colspan='6'> -->
                                                                        <!--<table border='1'>-->
                                                                        <?php 
                                                                        /*
                                                                            foreach($curr_subj as $key => $val_a){
                                                                            //if(($subject_ID_a)=($val_a['subject_ID'])){
                                                                            $subject_ID_a = $val_a['subject_ID'];
                                                                            $SubjectjectGET_a = CURRICULLUM:: getSubject($subject_ID_a);
                                                                            */
                                                                            $subject_ID = $val['subject_ID'];
                                                                            $SubjectGET = CURRICULLUM:: getSubject($subject_ID);
                                                                            //print_r($SubjectGET);
                                                                            foreach($SubjectGET as $subj => $val1)
                                                                            {
                                                                            ?>
                                                                                <tr>
                                                                                    <td><?= $val1['subject_ID'];?>
                                                                                    <td><a href="subjects.php?action=edit&id=<?= $val1['subject_ID']; ?>" target="_blank" title="Edit Subject"><?= $val1['subject_CODE'];?></a>
                                                                                    <td><?= $val1['subject_DESCRIPTION'];?>
                                                                                    <td align="center"><?= $val1['TOTAL_UNIT'];?>
                                                                                    <td align="center">

                                                                                                <?php 

                                                                                                    if (strpos($val['prerequisite_subject_ID'], ',') !== false) {
                                                                                                        #echo 'true';
                                                                                                        $x = explode(",",$val['prerequisite_subject_ID']);
                                                                                                        foreach($x as $key2 => $val2){
                                                                                                                 #secho $val2."<br>";
                                                                                                                 $preq = SUBJECTS::getID($val2);
                                                                                                                 echo $preq['subject_CODE']."<br>";
                                                                                                        }
                                                                                                    }else{
                                                                                                        $preq = SUBJECTS::getID($val['prerequisite_subject_ID']);
                                                                                                        echo $preq['subject_CODE'];
                                                                                                        
                                                                                                        if(sizeof($preq) == 1){ echo "None"; }
                                                                                                    }   
                                                                                                    
                                                                                                 ?>

                                                                                     <td align="center">

                                                                                              
                                                                                                <?php 

                                                                                                    if (strpos($val['corequisite_subject_ID'], ',') !== false) {
                                                                                                        #echo 'true';
                                                                                                        $x = explode(",",$val['corequisite_subject_ID']);
                                                                                                        foreach($x as $key2 => $val2){
                                                                                                                 #secho $val2."<br>";
                                                                                                                 $preq = SUBJECTS::getID($val2);
                                                                                                                 echo $preq['subject_CODE']."<br>";
                                                                                                        }
                                                                                                    }else{
                                                                                                        $preq = SUBJECTS::getID($val['corequisite_subject_ID']);
                                                                                                        echo $preq['subject_CODE'];
                                                                                                        
                                                                                                        if(sizeof($preq) == 1){ echo "None"; }
                                                                                                    }   
                                                                                                    
                                                                                                 ?>

                                                                                    </td>
                                                                                    <td align="center"><a target="_blank" href="curricullum_subjects.php?action=edit&id=<?= $val['cs_ID']; ?>">Edit</a> | <a onclick="return confirm('Are you sure to remove this subject in this curriculum?');" href="curricullum_subjects.php?action=delete&id=<?= $val['cs_ID']; ?>">Delete</a>
                                                                                
                                                                            <?php
                                                                            }   
                                                                            
                                                                        ?>
                                                                                
                                                                        <!--</table>-->
                                                                        <?// = $val['subject_ID']; ?>
                                                                    </td>
                                                            <?php
                                                            }

                                                                if(sizeof($curr_subj)==0)  
                                                                    {
                                                                        
                                                                        if(isset($_GET['hidden']) && $_GET['hidden'] == 'yes')
                                                                        {
                                                                        
                                                                        }else{
                                                                        ?>
                                                                        
                                                                            <script type="text/javascript">
                                                                                $("#sem<?php echo $value['yr_sem_ID']; ?>").toggle();
                                                                            </script>
                                                                        <?php
                                                                        }
                                                                        
                                                                        
                                                                    } 
                                                                    
                                                                    
                                                                
                                                            //loop of subjects
                                                                        /*
                                                                ?>
                                                                <script type="text/javascript">
                                                                $(document).ready(function(){
                                                               
                                                                <?php
                                                                $getAllSemester = CURRICULLUM:: getAllSemester(array("yr_sem_ID"=>"ASC"));
                                                                if($getAllSemester)
                                                                {
                                                                    foreach($getAllSemester as $key => $value)
                                                                    {
                                                                        $curr_subj = CURRICULLUM::getSubjectbycurr($value['yr_sem_ID'],$cur_ID);
                                                                    foreach($curr_subj as $key => $val)
                                                                    {
                                                                          
                                                                    }
                                                                }
                                                                ?>
                                                                });
                                                                </script>
                                                                <?php
                                                                
                                                            } //last
                                                            */
                                                            
                                                            ?>
                                                                  
                                                                     
                                                                        </table>
                                    </table>                                    
                        <?php    
                    } //foreach getallsemester
                } //if getallsemester

                    ?>
                                </table>
                            
                            </div>
                            
                        </div>
                            <div align='right'>
                                <input type='button' class='btn P3C6_PRINT btn-success' value='Print'>
                              </div>
                    <?php
                                
                                }else{
                    ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <table>
                                <tr>
                                <td width="50%">
                                    <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                        <i class="fa fa-folder-o"></i> Encoding of Curriculum </h4>
                                </td>
                                <!-- <script src="jquery.js" type="text/javascript"></script> -->
                                    <script>
                                    $(document).ready(function(){
                                    
                                    
                                    function showDiv(divId){
                                                $('#'+divId).show();
                                                
                                    }
                                    
                                    
                                    
                                      $("#flip").click(function(){
                                        $("#panel").slideToggle("fast");
                                      });
                                    });
                                    </script>
                                <td >
                                    <div class="input-group col-md-12">
                                      <?php //include('searching_curricullum.php');?>
                                        <!--
                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                        -->
                                    </div>
                                </td>
                                <td width="5%">
                                <div class="col-md-1">   
                                <button style="margin-top: 3px; margin-bottom: 2px" class='btn btn-success btn-ms P5C5_ADDCURRICULUM' type='submit' onclick="javascript:window.open('curricullum.php?action=add','_self');"> Add Curriculum</button>
                                </div>
                                </td>
                                </tr>
                                </table>
                            </div>
                            
                            <div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
                                <table class="table table-hover table-responsive table-striped text-md">
                                    <!-- <th style="width:5%;"> ID</th> -->
                                    <th style="width:40%;"><span style="width:40%;">Program Name</span></th>
                                    <th style="width:25%;"><span style="width:25%;">Curriculum Name</span></th>
                                    <th style="">Option</th>
                                    
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    $viewCurricullum = CURRICULLUM:: getAllCurricullum(array("course_ID"=>"ASC","curricullum_NAME"=>"ASC"));
                                     if(count($viewCurricullum)>=10) 
                                     {
                                         $pagination2 = new Pagination($viewCurricullum, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($viewCurricullum, 10, NULL);
                                     }

                                     $viewCurricullum = $pagination2->get_array();

                                     if($viewCurricullum) {
                                        foreach($viewCurricullum as $key => $value){
                                    ?>
                                        <tr>
                                           <!--  <td><?php echo $value['curricullum_ID']; ?> -->
                                            <td><?php //echo $value['course_ID'];
                                                $course_ID = $value['course_ID'];
                                                //echo $course_ID;

                                                $getCurricullumName = CURRICULLUM:: getID($course_ID);
                                                echo $getCurricullumName['course_NAME'];
                                                     
                                            ?>
                                          <td><?php echo $value['curricullum_NAME']; ?>
<td>
                                                <a href="curriculum1.php?action=view&id=<?php echo $value['curricullum_ID']; ?>" class='btn btn-success btn-xs P5C5_VIEW'>View</a>
                                                <a href="curricullum.php?action=edit&id=<?php echo $value['curricullum_ID']; ?>" class='btn btn-warning btn-xs P5C5_EDIT'>Edit</a>
                                                <a href="curricullum.php?action=delete&id=<?php echo $value['curricullum_ID']; ?>" class='btn btn-danger btn-xs P5C5_DELETE'>Delete</a>
                                            </td>
                                        </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
                                </table> 
                                        <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                                        </center>
                                 </div>
                            </div>
                        </div>
                            
                  <?php } break; ?>
                  
                    <?php 
                        case "delete":
                       
                        ?>
                        
                        
                <?php break;
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                    
                    
                 </div>

                    
                </div>
                
                <!-- end right nav -->
                
            <!-- end row container -->
            
            
            <script>
            $(document).ready(function() {
                $('.pre_req').select2();
            });
            </script>
            <!-- footer-->      
            <?php include (LAYOUTS . "footer.php"); ?>
            
            <!-- end footer -->

        </div>
        <!-- end container -->

    </body>
</html>
