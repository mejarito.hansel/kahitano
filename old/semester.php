<?php 
//INITIALIZE INCLUDES
    include('init.php');
#SESSION::CheckLogin();

if (SESSION::isLoggedIn()) {
    #HTML::redirect();
}
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
        <script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>
      
      <title>SEMESTER MANAGEMENT | School Management System v2.0</title>
    </head>
    <body>
        <!-- top nav -->
        <?php include(LAYOUTS . 'top_nav.php'); ?>
        <!-- end nav -->
<div class="container">
    <!-- banner -->
    <?php include(LAYOUTS . "banner.php"); ?>
    <!-- end banner -->
    <!-- alert messages -->
    <?php SESSION::DisplayMsg(); ?>
    <!-- end of alert messages -->
    <!-- start row container -->
    <!-- 
    <div class="row">
        <div class="col-lg-12">
            <ul class="breadcrumb">
                <li class="active">Home</li>
            </ul>
    </div>  
    </div>
    -->
    <!-- end row container -->
    <!-- row container -->
    <div class="row">
        <!-- left nav -->   
  
        <div class="col-md-3">

        <?php include(PAGES."navigation.php");?>
        </div>
       
        <!-- end left nav -->
        
        <!-- BODY -->
        
        
        <div class="col-md-9">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <table>
                        <tr>
                            <td width="50%">
                                <h4 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px">
                                <i class="fa fa-folder-o"></i> View Trimester </h4>
                            </td>
                            <td>
                                <div class="input-group col-md-12 hide">
                                    
                                    <input id="searchItem" class="searchItem form-control"  type="text" autofocus="autofocus"  style="text-transform: uppercase;"/>
                                    
                                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                </div>
                            </td>
                            <td width="5%">
                                <div class="col-md-1">   
                                    <a id="P2C2_EXPORTSASCSV" class='btn btn-success btn-ms btnAddTri'>Create New Trimester</a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="panel-body"><br>
                    <!-- <div class="row">
                        <div class="form-group">
                            <div class="col-sm-5">
                                <input name='' id="myInput" placeholder="Search by Name or Applicant Number" class='form-control' onkeyup="myFunction()" type='text' required autofocus style="font-style: italic;"/>
                            </div>
                        </div>
                    </div><br> -->
                    <div id="dvData">
                        <table border="0" cellspacing="5" cellpadding="5" style="width:40%;">
                            <!-- <tbody>
                                <tr>
                                    <td><input name='' id="myInput" placeholder="Search by Name or Applicant Number" class='form-control' onkeyup="myFunction()" type='text' required autofocus style="font-style: italic;"/></td>
                                </tr>
                            </tbody> -->
                        </table><br>
                        <table id="myTable" class="table table-hover table-responsive table-striped text-md" style="width:100%;">
                            <thead>
                                <tr>
                                  <th>Trimester Name</th>
                                  <th>Status</th>
                                  <th>Operation</th>
                                </tr>
                            </thead>
                            <tbody id="tbody">
                            </tbody>
                        </table>
                    </div>
                    <div class="modal fade" id="modalNewTri" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="" style="padding:40px 40px 10px 40px;">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 style=" text-align: center;">Add Trimester</h4>
                            </div>
                            <div class="modal-body" style="padding:20px 90px;">
                                <div class="failed hide" id="exst"><strong>Error!</strong> TRIMESTER ALREADY EXISTS</div>
                                <div class="success hide" id="Asuccess"><strong>Success!</strong> Successfully Trimester Created!</div>
                                <div class="failed hide" id="Arequiredfield"><strong>Error!</strong> Required all fields</div>
                                <div class="form-group">
                                    <label>Trimester Name</label>
                                    <input type="text" class="form-control" id="triName" placeholder="Trimester Name">
                                </div>
                                <div class="form-group">
                                    <label>Trimester Status</label>
                                    <select id="triStats" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Trimester Enrolment</label>
                                    <select id="triEnrol" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Encoding of Grades</label>
                                    <select id="encodeGrades" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Show Teacher's Loading</label>
                                    <select id="triLoading" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div><br>
                                <button type="submit" class="btn btn-success btn-block " id="btnSave"> Save</button>
                                <button type="submit" class="btn btn-success btn-block" id="btnSaveM"> Add More?</button>
                                <br>
                            </div>
                          </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalUpdateTri" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="" style="padding:40px 40px 10px 40px;">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 style=" text-align: center;">Update Trimester</h4>
                            </div>
                            <div class="modal-body" style="padding:20px 90px;">
                                <div class="failed hide" id="exstU"><strong>Error!</strong> TRIMESTER ALREADY EXISTS</div>
                                <div class="success hide" id="Usuccess"><strong>Success!</strong> Successfully Trimester Updated!</div>
                                <div class="failed hide" id="requiredfield"><strong>Error!</strong> Required all fields</div>
                                <div class="form-group">
                                    <label>Trimester Name</label>
                                    <input type="" class="form-control" id="UtriName" placeholder="Trimester Name">
                                </div>
                                <div class="form-group">
                                    <label>Trimester Status</label>
                                    <select id="UtriStats" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Trimester Enrolment</label>
                                    <select id="UtriEnrol" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Encoding of Grades</label>
                                    <select id="UencodeGrades" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Show Teacher's Loading</label>
                                    <select id="UtriLoading" class="form-control">
                                        <option value="1">Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div><br>
                                <button type="submit" class="btn btn-success btn-block" id="btnUpdate"> Save</button>
                                <br>
                            </div>
                          </div>
                        </div>
                    </div>  

                    <div class="modal fade" id="modalLockCheck" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                                <div class="" style="padding:40px 40px 10px 40px;">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 style=" text-align: center;">Change Trimester Grade Status</h4>
                                </div>
                            <div class="modal-body" style="padding:20px 90px;">
                                <div class="success hide" id="Bsuccess"><strong>Success!</strong>  Trimester Status Changed!</div>
                                <p class="txt"></p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <a type="button" class="btn btn-success hide" id="btnConfirm">Yes</a>
                                <a type="button" class="btn btn-success hide" id="btnConfirm1">Yes</a>
                                <a type="button" class="btn btn-success hide" id="btnConfirm2">Yes</a>
                                <a type="button" class="btn btn-success hide" id="btnConfirm3">Yes</a>
                            </div>
                          </div>
                        </div>
                    </div> 
                </div>
            </div>
      
        </div>
    </div>
</div>
                    

            <!-- footer-->      
            <?php include (LAYOUTS . "footer.php"); ?>
            <script src="<?php echo HOME; ?>old/assets/js/custom/trimester.js"></script>
            <!-- end footer -->

        <!-- end container -->
        <script type="text/javascript">
            $( window ).ready(function(){
                setTimeout(function() {
                    $.main.lock.trimesterTbl();
                  
                },100);
            })
        </script>
    </body>
</html>
