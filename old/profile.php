<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();
	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
            ROOM::addroom(Request::post());
			#var_dump(Request::post());
			break;
			case "edit": ROOM::update_room($_GET['id'],Request::post()); break;
			case "change_password":USERS::changePassword($_SESSION['USER']['account_ID'],Request::post());
			break;
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ROOM::delete($_GET['id']); break;
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
       
      <title>PROFILE | School Management System v2.0</title>
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
	
			<div class="row">
				<!-- left nav -->   
                <div class="col-md-3">
                <?php include(PAGES."navigation.php");?>
                </div>
				<!-- end left nav -->
				<!-- BODY -->
                <div class="col-md-9">
                    <?php
					switch($_GET['action']){
                      
					    case "change_password":
             
                    ?>
					 
						     <div class="panel panel-default">
			
                   					 <div class="panel-heading">
                        						<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                          						  <i class="fa fa-folder-o"></i> Change Password
                                                </h3>
                                     </div>
                    			<div class="panel-body">
                                   <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                     
                   					 <div class="panel-body">
                                        <div class="form-group">
                                            <?= FORM::label("pass_old", "Old Password", array("class"=>"control-label col-sm-4")) ?>
                                            <div class="col-md-6"><?= FORM::password("cpassword",'', array("class"=>"form-control","autofocus"=>"autofocus","required")) ?></div>
                                        </div>
            
                                                    
                                        <div class="form-group">
                                            <?= FORM::label("new", "New Password", array("class"=>"control-label col-sm-4")) ?>
                                            <div class="col-md-6"><?= FORM::password("npassword", '', array("required", "class"=>"form-control", "id"=>"newPass", "onkeyup"=>"validatePassword(this.value)")) ?></div>
                                        </div> 
                                        <div class="form-group">
	                                        <div class="col-md-4" >                                      
											</div>
	                                        <div class="col-md-6" >                                      
	                                        <span id="strength"></span>

							                <div class="progress" hidden>
											  <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
											    <span class="sr-only">80% Complete (danger)</span>
											  </div>
											</div>
										   	<span id="msg"></span>
										   	</div> 
									   	</div> 
                                        <div class="form-group">
                                            <?= FORM::label("pass_new2", "Retype Password", array("class"=>"control-label col-sm-4")) ?>
                                            <div class="col-md-6"><?= FORM::password("vpassword", '', array("required", "class"=>"form-control", "id"=>"retypePass")) ?></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-6">
                                                <?= FORM::submit(null, Arr::get($user, "change_pass") ? "Change Password": "Change Password", array("class"=>"btn btn-success btn-block btnsave" )) ?>
                                            </div>
                                        </div>

		                             </div>
                                     
                                     </form>
								</div>			  
							</div>			  
					  
					 </div>
                     
					  <?php break;
					    case "add":?>
                    
                    <?php break; 
                        case "edit":
             
                    ?>
                    
                    <?php break;  
                        case "search":
									
						?>
							
                    <?php break; 
                        case "view":?>
                      
                  <?php  break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
				default: HTML::redirect("profile.php?action=view");
                    }
					
			 ?>
                	
				    
                 </div>

				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

 <script>
            function validatePassword(password) {
                if($("#newPass").val().length > 7){
                	$(".strengthHelp").eq(0).css("color","green");
                }else{
                	$(".strengthHelp").eq(0).css("color","black");	
                }
                // Do not show anything when the length of password is zero.
                if (password.length === 0) {
                    document.getElementById("msg").innerHTML = "";
                    $(".progress-bar").css("width","0%");
                    $(".progress").attr("hidden","hidden");
                    return;
                }
                // Create an array and push all possible values that you want in password
                var matchedCase = new Array();
                matchedCase.push("[$@$!%*#?&]"); // Special Charector
                matchedCase.push("[A-Z]");      // Uppercase Alpabates
                matchedCase.push("[0-9]");      // Numbers
                matchedCase.push("[a-z]");     // Lowercase Alphabates

                // Check the conditions
                var ctr = 0;
                for (var i = 0; i < matchedCase.length; i++) {
                    if (new RegExp(matchedCase[i]).test(password)) {
                        ctr++;
                    }
                }
                // Display it
                var color = "";
                var strength = "";
                switch (ctr) {
                    case 0:
                    case 1:
                    case 2:
                    	console.log("Working")
                        strength = "Very Weak";
                        color = "red";
                        $(".progress").removeAttr("hidden")
                        $(".progress-bar").css("width","10%");
                        if($("#newPass").val().length > 4){
                        	$(".progress-bar").css("width","20%");
                        	$(".strengthHelp").eq(1).css("color","black");
                        	$(".strengthHelp").eq(2).css("color","black");
                        }
                        $(".progress-bar").removeClass("progress-bar-info");
                        $(".progress-bar").removeClass("progress-bar-success").addClass("progress-bar-danger");
                        break;
                    case 3:
                        strength = "Medium";
                        color = "orange";
                        $(".progress").removeAttr("hidden");
                        $(".progress-bar").css("width","40%");
                        if($("#newPass").val().length > 1){
                        	$(".progress-bar").css("width","60%");
                        	$(".strengthHelp").eq(1).css("color","green");
                        }else if($("#newPass").val().length > 0){
                        	$(".strengthHelp").eq(1).css("color","black");
                        }else{
                        	$(".strengthHelp").eq(1).css("color","black");
                        }
                        $(".progress-bar").removeClass("progress-bar-danger");
                        $(".progress-bar").removeClass("progress-bar-success").addClass("progress-bar-info");
                        break;
                    case 4:
                        strength = "Strong";
                        color = "green";
                        $(".progress").removeAttr("hidden");
                        $(".progress-bar").css("width","80%");
                        if($("#newPass").val().length > 10){
                        	$(".progress-bar").css("width","100%");
                        	$(".strengthHelp").eq(2).css("color","green");
                        }
                        $(".progress-bar").removeClass("progress-bar-danger");
                        $(".progress-bar").removeClass("progress-bar-info").addClass("progress-bar-success");
                        break;
                }
                document.getElementById("msg").style.color = color;
            }
        
					   $(document).ready(function() {
					  
					$(function() {
					    $('.btnsave').attr('disabled', 'disabled');
					});
			
					$('input[type=password]').keyup(function() {
					        
					    if ($('#newPass').val() !=''&&
					      $('#retypePass').val() != ''){
 
					    	 if (($('#newPass').val()!="") && ($('#retypePass').val()!="")){

						       if (($('#newPass').val())==($('#retypePass').val()))
						          {
						          $('.btnsave').removeAttr('disabled');
						        }

					         }

					      }

					     else {
					        $('.btnsave').attr('disabled', 'disabled');
					    }
					});
			  });
	   	</script>
		<script type="text/javascript">
          	$(".userid").unbind("click").on("click",function(){
	            userid = $(this).data("userid")
				console.log(userid)

			})
		</script>
	</body>
</html>
