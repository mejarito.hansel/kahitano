<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::post("action")) {
			case "add": 
			
			break;
			case "change_lock":
				SUBJSCHED::change_lock(Request::post());
			break;
			
			case "change_check":
				SUBJSCHED::change_check(Request::post());
			break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ROOM::delete($_GET['id']); break;

		}
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>FACULTY LOADING | School Management System v2.0</title>
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
          <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script> 
      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end of alert messages -->
			<!-- start row container -->
			<!-- 
            <div class="row">
				<div class="col-lg-12">
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
            </div>	
			</div>
            -->
			<!-- end row container -->
            <!-- row container -->
			<div class="row">
				<!-- left nav -->   
          
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
               
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Semester Section</h3> 
                    </div>
                    <div class="panel-body">
                    
                    <?php
					$sem = SEM::getallsems(array("sem_ID"=>"DESC"));
					#print_r($sem);
					
					$course = COURSE::getallcourse(array("course_NAME"=>"ASC"));
					?>
                    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Semester Section Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                              <div class="col-sm-6">
                                  <label for='si_FNAME' class=''>Semester: </label>
                                  <select class='form-control' name="sem_ID">
                                  	<?php foreach($sem as $key => $val){ ?>
                                    <option value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
                                    <?php } ?>
                                  </select>
                              </div>
                            </div>
                            
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Course:</label>
                                    <select class='form-control' name="course_ID">
										<?php foreach($course as $key => $val){ ?>
                                        <option value="<?php echo $val['course_ID']; ?>"><?php echo $val['course_NAME']; ?></option>
                                        <?php } ?>    
                                    </select>
                               </div>
                             </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Section Name:</label>
                                    <input name='semester_section_NAME' placeholder="Section Name" class='form-control' type='text' required="required"/>
                               </div>
                            </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''></label>
                               </div>
                            </div>                            
                            
                            
                             <div class="form-group"></div>                            
                            
                            
                            
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Add Semester Section</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":

          			$geThis = SEMSECTION::geThis($_GET['id']);
					#print_r($geThis);
					?>
                    <?php
					$sem = SEM::getallsems(array("sem_ID"=>"DESC"));
					#print_r($sem);
					
					$course = COURSE::getallcourse(array("course_NAME"=>"ASC"));
					?>
                                        
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Update Semester Section</h3> 
                    </div>
                    <div class="panel-body">
                       <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Semester Section Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                              <div class="col-sm-6">
                                  <label for='si_FNAME' class=''>Semester: </label>
                                  <select class='form-control' name="sem_ID">
                                  	<?php foreach($sem as $key => $val){ ?>
                                    <option value="<?php echo $val['sem_ID']; ?>"
                                    
                                    <?php
									  if($val['sem_ID'] == $geThis['sem_ID']){
										echo "selected=selected";
									  }
									?>
                                    
                                    ><?php echo $val['sem_NAME']; ?></option>
                                    <?php } ?>
                                  </select>
                              </div>
                            </div>
                            
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Course:</label>
                                    <select class='form-control' name="course_ID">
										<?php foreach($course as $key => $val){ ?>
                                        <option value="<?php echo $val['course_ID']; ?>" 
                                        
                                          <?php
											  if($geThis['course_ID'] == $val['course_ID']){
												echo "selected=selected";
											  }
											?>
                                        
                                        ><?php echo $val['course_NAME']; ?></option>
                                        <?php } ?>    
                                    </select>
                               </div>
                             </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Section Name: </label>
                                    <input name='semester_section_NAME' value="<?php echo $geThis['semester_section_NAME']; ?>" placeholder="Section Name" class='form-control' type='text' required="required"/>
                               </div>
                            </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''></label>
                               </div>
                            </div>                            
                            
                            
                             <div class="form-group"></div>                            
                            
                            
                            
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Update Semester Section</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
						include "searching.php";						
						?>
							
                    <?php break; 
                          case "view":
                          ;?>
                       
                       <ul class="breadcrumb">
                                        <li class="active"><a href="faculty_loading_user.php?action=view">FACULTY LOADING</a></li>
                                        
                                        <?php if(isset($_GET['sem_ID']))
										{	$sem = SEM::getSingleSem($_GET['sem_ID']) ?>
                                        
                                        <li class="active"><a href="?action=view&sem_ID=<?= $_GET['sem_ID'] ?>"><?php echo $sem['sem_NAME']; ?></a></li>
                                    	<?php } ?>
                                        
                                         <?php if(isset($_GET['fac_id']))
										{	$f = INSTRUCTORS::getSingle1($_GET['fac_id']) ?>
                                        
                                        <li class="active"><a href="?action=view&sem_ID=<?= $_GET['sem_ID'] ?>&fac_id=<?= $_GET['fac_id']; ?>"><?php  echo $f['instructor_NAME']; ?></a></li>
                                    	<?php } ?>
                                        
                                        <?php if(isset($_GET['subject_sched_id'])){ 
										$su = SUBJSCHED::getSched($_GET['subject_sched_id']) ;
										#print_r($su);
										?>
                                        <li class="active"><a href="?action=view&sem_ID=<?= $_GET['sem_ID'] ?>&fac_id=<?= $_GET['fac_id']; ?>&subject_sched_id=<?= $_GET['subject_sched_id']; ?>">
                                        
                                         <?php $c=COURSE::getbyID($su['course_ID']); 
												echo $c['course_INIT']."-";
                                              echo $su['semester_section_NAME']; ?> -
                                           <?php $s = SUBJECTS::getID($su['subject_ID']);
												echo $s['subject_CODE'];
												 ?> 
                                        
                                        </a></li>
                                    	
                                        <?php } ?>
                                        
                                        </ul>
                       
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table width="100%">
                                <tr>
                                    <td colspan="6">
                                    
                                    
                                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                            <i class="fa fa-folder-o"></i> FACULTY LOADING</h3>                    

                                        <?php 
                                        if(isset($_GET['fac_id'])){
                                        
                                        ?>
                                        <span class="pull-right">

                                          <a target="_new" href="subject_sched.php?action=add&fac_id=<?= $_GET['fac_id']; ?>" class="btn btn-success hidden"><i class="fa fa-plus"></i> Add Subject</a>

                                          <a target="_new" href="faculty_loading_print.php?action=view&sem_ID=<?= $_GET['sem_ID'] ?>&fac_id=<?= $_GET['fac_id']; ?>" class="btn btn-success"><i class="fa fa-print"></i> Print</a>

                                        </span>

                                        <?php  } ?>                              
                                      <!--
                                        <div class="col-md-1">   
                                            <button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('?action=add','_self');"><i class='fa fa-plus'></i></button>
                                        </div>
                                    -->                                        </td>
                                  </tr>
                                <tr>
                                  <td width="10%">Semester:</td>
                                  <td width="28%" >
                                  <?php
								  $sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
								  ?>
                                  <form action="<?= $_SERVER['REQUEST_URI']; ?>" method="GET">
                                  <input type="hidden" value="view" name="action" />
                                  
                                  <select class='form-control' name="sem_ID" onchange="this.form.submit()">
								                    <option value="">Please Choose</option>
                                		<?php foreach($sem as $key => $val){ ?>
                                        <option <?php if(isset($_GET['sem_ID'])){ if($_GET['sem_ID'] == $val['sem_ID']){ echo " SELECTED=SELECTED "; } } ?> value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
                                        <?php } ?>
                                  </select>
                                  </form></td>
                                  <td width="3%" >&nbsp;</td>
                                  
                                  <?php if(isset($_GET['sem_ID'])){ 
                                    //start of audit trail viewing of student
                                    if($_GET['fac_id'] == ""){
                                $user_info_id = $user['account_ID'];
                                $event_sem = SEM::getSingleSem($_GET['sem_ID']);
                                $action_event = "SELECT";
                                $event_desc = " FACULTY LOADING, <br> DESCRIPTION: SELECTED SEMESTER ID: ".$_GET['sem_ID']."<br> SEMESTER NAME: ".$event_sem['sem_NAME'] ;
                                $audit = Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
                                  }
                                    //end of audit trail viewing of student

                                    ?>
                                  <?php $faculty = SEM::allfacultypersem($_GET['sem_ID']);      

								                 #print_r($faculty);
                                 			  ?>  
                                  <td width="5%">&nbsp;</td>
                                  <td width="31%">&nbsp;</td>
                                  
                                  <?php } ?>
                                  <td width="23%">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>Faculty:</td>
                                  <td><form action="<?= $_SERVER['REQUEST_URI']; ?>" method="get">
                                      <input type="hidden" value="view" name="action" />
                                      <input type="hidden" value="<?= $_GET['sem_ID']; ?>" name="sem_ID" />
                                      <select class='select2 form-control' name="fac_id" onchange="this.form.submit()">
                                        <option   value="">Please Chooses</option>
                                        <?php foreach($faculty as $key => $val){ ?>
                                        <option <?php if(isset($_GET['fac_id'])){ if($_GET['fac_id'] == $val['instructor_ID']){ echo " SELECTED=SELECTED "; } } ?> value="<?php echo $val['instructor_ID']; ?>">
                                          <?php
									  $f = INSTRUCTORS::getSingle1($val['instructor_ID']); 
											echo $f['instructor_NAME']; 
									   ?>
                                        </option>
                                        <?php } ?>
                                      </select>
                                  </form></td>
                                  <td >&nbsp;</td>
                                  
                                  <?php if(isset($_GET['fac_id'])){ ?>
                                 
                                  <?php } ?>
                                  <td>&nbsp;</td>
                                </tr>
                                
                                </table>
                          </div>
							
							<div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
                                
                                <?php 
								 if(isset($_GET['fac_id']))
								{
								?>
                                
                                <table class="table table-hover table-responsive table-striped text-md">
                                    <th class='hidden' style="">#</th>
                                    <th style=";">Program</th>
                                    <th style="">Section</th>
                                    <th style="">Days</th>
                                    <th style="">Time</th>
                                    <th style="">Room</th>
                  									<th style="">Enrolled</th>
                                    <th style="">Option</th>
                                    
                                    <?php
									
									$getmysubj = SUBJSCHED::getByfaculty($_GET['sem_ID'], $_GET['fac_id']);
                   //echo $val['sem_NAME']; 
									#print_r($getmysubj);
                  
                  //start of audit trail viewing of student
                  if(($_GET['sem_ID'] != "") &&($_GET['fac_id'] != ""))
                  {
                       $event_instructor = INSTRUCTORS::getSingle1($_GET['fac_id']) ;
                       
                       $event_sem = SEM::getSingleSem($_GET['sem_ID']);
                       
                       $user_info_id = $user['account_ID'];
                       
                       $action_event = "SELECT";
                       
                       $event_desc = " FACULTY LOADING, <br> DESCRIPTION: SELECTED SEMESTER ID: ".$_GET['sem_ID']."<brSEMESTER NAME: ".$event_sem['sem_NAME'].
                       "<br> SELECTED FACULTY ID: ".$_GET['fac_id']."<br> FACULTY NAME: ".$event_instructor['instructor_NAME'];
                   
                    $audit = Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
                  }
                  //end of audit trail viewing of student

									$totalcount = count($getmysubj);
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                   // $viewSems = SEMSECTION::getAll(array("semester_section_ID" => "DESC"));
									
									
                                     if(count($getmysubj)>=50) 
                                     {
                                         $pagination2 = new Pagination($getmysubj, 50, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($getmysubj, 50, NULL);
                                     }

                                     $getmysubj = $pagination2->get_array();
                                    # var_dump($getmysubj);
									                   $x = 1;

                                     if($getmysubj) {
                                        foreach($getmysubj as $key => $value){
                                    ?>
                                        <tr>
                                            <td class='hidden'>
											<?php 
												if(isset($_GET['p']))
												{
												$p = $_GET['p'];
												
												$num = 	$x+($p*10)-10;
												echo $num;
												$x++;
												
												}else{
													echo $x++;
												}
											 ?>
                          <td><?php $s = SUBJECTS::getID($value['subject_ID']);
                                echo $s['subject_CODE']."/".$s['subject_DESCRIPTION'];
                               ?> 

                          <td><?php $c=COURSE::getbyID($value['course_ID']); 
											          echo $c['course_INIT']."-";
                                echo $value['semester_section_NAME']; 
                              ?>
                                      
												 </td>
                        
                        <td><?php echo $value['day']; ?> 
                        <?php if($value['day2'] != NULL){ ?>/<br /><?php echo $value['day2']; ?><?php } 
                        ?>
                        <?php if($value['day3'] != NULL){ ?>/<br /><?php echo $value['day3']; ?><?php } 
                        ?>
                        <?php if($value['day4'] != NULL){ ?>/<br /><?php echo $value['day4']; ?><?php } 
                        ?>
                      </td>
                        <td><?php echo $value['start']; ?>-<?php echo $value['end']; ?>
                            <?php if($value['start2'] != NULL){ ?>/<br /><?php echo $value['start2']; ?>-<?php echo $value['end2']; ?><?php } 
                            ?>
                            <?php if($value['start3'] != NULL){ ?>/<br /><?php echo $value['start3']; ?>-<?php echo $value['end3']; ?><?php } 
                            ?>
                            <?php if($value['start4'] != NULL){ ?>/<br /><?php echo $value['start4']; ?>-<?php echo $value['end4']; ?><?php } 
                            ?>
                        </td>
                        <td><?php
                            $r = ROOM::getID($value['room_ID']);
                            echo $r['room_NAME'];
                             ?>
                               <?php if($value['room_ID2'] != NULL){ ?>/<br />
                               <?php
                                   $r = ROOM::getID($value['room_ID2']);
                                  echo $r['room_NAME'];
                             } ?>
                               <?php if($value['room_ID3'] != NULL){ ?>/<br />
                               <?php
                                   $r = ROOM::getID($value['room_ID3']);
                                  echo $r['room_NAME'];
                             } ?>
                               <?php if($value['room_ID4'] != NULL){ ?>/<br />
                               <?php
                                   $r = ROOM::getID($value['room_ID4']);
                                  echo $r['room_NAME'];
                             } ?>
                             <td align="center"> <?= SUBJSCHED::checkRemaining($value['subject_sched_ID']); ?></td>
                             <td><a class="btn btn-success" target="-new" href="classlist.php?action=view&id=<?= $value['subject_sched_ID']; ?>">Class List</a></td>
                                  </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
                </table> 
                                Total Records: <?=  $totalcount; ?>
                                        <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view&sem_ID='.$_GET['sem_ID'].'&fac_id='.$_GET['fac_id'].'') ?>
                                        </center>
                                        
                                  <?php }else{ ?>  
                                  
                                  <table class="table table-hover table-responsive table-striped text-md">
									
                                    <th style="width:5%;">#</th>
                                    <th style="width:25%;">Faculty</th>
                  									<th style="width:20%;"><div align="center">No. of Subjects</div></th>
                  									<!-- <th style="width:20%;"><div align="center">Checked Subjects</div></th> -->
                                    <th style=""><div align="center">Operation</div></th>
                                     <?php     
                                  	$faculty = SEM::allfacultypersem($_GET['sem_ID']);      
									#print_r($faculty);
                                    ?>   
                                <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
									 
									 
									 
                                 
									
									
                                     if(count($faculty)>20) 
                                     {
                                         $pagination2 = new Pagination($faculty, 20, Request::get("p"));
						
                                     }else{
                                         $pagination2 = new Pagination($faculty, 20, NULL);
							
                                     }
			
                                     $faculty = $pagination2->get_array();
									 $x = 1;
									

                                     if($faculty) {
                                        foreach($faculty as $key => $value){
                                    ?>
                                        <tr>
                                            <td><?php echo $x++; ?>.
                                            <td><?php
											$f = INSTRUCTORS::getSingle1($value['instructor_ID']); 
											echo $f['instructor_NAME']; 
											?>
                                            <td><div align="center"><?php echo SEM::numofsubjpersemperfac($_GET['sem_ID'],$value['instructor_ID']); ?>
                                            </div>
                                            <!-- <td><div align="center"><?php echo SEM::numofchksubjpersemperfac($_GET['sem_ID'],$value['instructor_ID']); ?> -->
                                            </div>
                                            <td><div align="center"><a href="?action=view&sem_ID=<?= $_GET['sem_ID']; ?>&fac_id=<?= $value['instructor_ID']; ?>" class='btn btn-warning btn-xs'>View Subjects</a></div></td>
                                  </tr>
                                    
                                        <?php
                                         } 
                                        }else{
                                    ?>
                                        <tr>
                                          <td colspan="5" align="center">No Faculty</td>
                                        </tr>
                                        <?php } ?>
								</table> 
          <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view&sem_ID='.$_GET['sem_ID']) ?>
                                        </center>
                                
                                        
                                  <?php } ?>      
                           
                </div>
                  <?php  break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
					default: href("?action=view");
				
				
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                	
				    
                 </div>

				    
				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
            

			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->
    <script type="text/javascript"> 
    $(document).ready(function() {
        $('.select2').select2();
        $(document).on('keydown', '.select2', function(e) {
          if (e.originalEvent && e.which == 40) {
            e.preventDefault();
            $(this).siblings('select').select2('open');
          }
        });

        $('select').select2({
          selectOnClose: true
        });
   });
  </script>
	</body>
</html>
