<?php
include('init.php');
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Accounting / Subsidiary Ledger, DESCRIPTION: User visited Subsidiary Ledger";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
header('location: acct_transaction_logs1.php?action=view');
?>