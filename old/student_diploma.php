<?php
include('init.php');
$siiid = clean($_GET['id']);
$getsingle = USERS::getSingle(array("si_ID"=>"DESC"),$siiid  );
$getsingle1 = USERS::getSingleSAI(array("si_ID"=>"DESC"), $siiid );

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/jquery.dataTables.min.css">
    <!-- <link href="ams\old\pages\htmlfolder\assets/css/style.css" rel="stylesheet"> -->
    <link href="pages\htmlfolder\assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="pages\htmlfolder\assets/css/style.css" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>AMS</title>

</head>
 <?php if(!isset($_GET['action'])){ ?>
<body>
<?php }else{ ?>
<body onload="window.print();">
<?php } ?>
  <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>?action=print&id=<?php echo $_GET['id']; ?>">
  <div class="container" style="margin-bottom: 15px; margin-top: 15px">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-md-12">
        <h4 class="user_info">DIPLOMA</h4>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-sm-4 col-md-4">
        <div class="diplomalogoStyle">
          <br>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <img src="pages\htmlfolder\assets/img/mmmatoplogo2.png">
            </div>
          </div>
          <br><br>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <img src="pages\htmlfolder\assets/img/logocent2.png">
            </div>
          </div>
          <br><br>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <div class="row">
                <div class="col-lg-12 col-sm-12 col-md-12">
                  <img src="pages\htmlfolder\assets/img/f1.png">
                </div>
              </div>
            </div>
          </div>
          <br>
        </div>
      </div>
      <div class="col-lg-8 col-sm-8 col-md-8 ">
      <br><br>
        <div class="diplomaStyle">
          <div class="row ">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <img src="pages\htmlfolder\assets/img/mmma.png  ">
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <span>Greetings To All Men Whom These Presents May Come:</span>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <span>The Board of Trustees through the Academy President, by authority of the Commission on Higher Education and on recommendation of the Dean of Academic Affairs, hereby confers upon</span>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <h3><span><?= $getsingle['si_LNAME']; ?>, <?= $getsingle['si_FNAME']; ?> <?= $getsingle['si_MNAME']; ?></span></h3>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <span>Who has fulfilled the requirements there of, the degree of</span>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <h4><?= strtoupper(USERS::getFullCourse($getsingle['si_ID'])); ?></h4>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <span>With all the rights, honors, and privileges, as well as the obligations and responsibilities here unto appertaining. </span>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <span>In testimony whereof, the seal of the Academy, the signatures of the Academy Registrar and the President , are here unto affixed.</span>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <div class="row">
                <div class="col-lg-2 col-sm-2 col-md-2">
                </div>
                <div class="col-lg-2 col-sm-2 col-md-2">
                  <span>Given in</span>
                </div>
                <div class="col-lg-3 col-sm-3 col-md-3">
                  <div class="bottomBorder">
                    <span class="">
                        <?php if(!isset($_GET['action'])){ ?>
                        <input type="text" name="field1" class="form-control">
                        <?php }else{ echo $_POST['field1'];} ?>
                    </span>
                  </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-md-3">
                  <span>, Philippines, this</span>
                </div>
                <div class="col-lg-2 col-sm-2 col-md-2">
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <div class="row">
                <div class="col-lg-2 col-sm-2 col-md-2">
                  <div class="bottomBorder">
                    <span class="">
                      <?php if(!isset($_GET['action'])){ ?>
                      <input type="text" name="field2" class="form-control">
                      <?php }else{ echo $_POST['field2']; } ?>
                    </span>
                  </div>
                </div>
                <div class="col-lg-2 col-sm-2 col-md-2">
                  <span>day of</span>
                </div>
                <div class="col-lg-3 col-sm-3 col-md-3">
                  <div class="bottomBorder">
                    <span class="">
                      <?php if(!isset($_GET['action'])){ ?>
                      <input type="text" name="field3" class="form-control">
                      <?php }else{ echo $_POST['field3']; } ?>
                    </span>
                  </div>
                </div>
                <div class="col-lg-5 col-sm-5 col-md-5">
                  <span>in the year of our Lord Two Thousand</span>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-sm-12 col-md-12">
              <div class="row">
                <div class="col-lg-4 col-sm-4 col-md-4">
                </div>
                <div class="col-lg-2 col-sm-2 col-md-2">
                  <span>and</span>
                </div>
                <div class="col-lg-2 col-sm-2 col-md-2">
                  <div class="bottomBorder">
                    <span class="">
                        <?php if(!isset($_GET['action'])){ ?>
                        <input name="field4" type="text" class="form-control">
                        <?php } else { echo $_POST['field4']; } ?>
                      </span>
                  </div>
                </div>
                <div class="col-lg-1 col-sm-1 col-md-1">
                    <span class="">.</span>
                </div>
              </div>
            </div>
          </div>
          <br><br><br>
          <div class="row">
            <div class="col-lg-4 col-sm-4 col-md-4">
              <div class="col-lg-12 col-sm-12 col-md-12 user_name">
                <br>
              <span>
                  <?php if(!isset($_GET['action'])){ ?>
                  <input type="text" class="form-control" name="field5">
                  <?php } else { echo $_POST['field5']; } ?>
              </span>
              </div>
              <div class="col-lg-12 col-sm-12 col-md-12 user_fullname font-italic">
                <p class="">Academy Registrar</p>
              </div>
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4">
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4">
              <div class="col-lg-12 col-sm-12 col-md-12 user_name">
                <br>
              <span>
                  <?php if(!isset($_GET['action'])){ ?>
                  <input type="text" class="form-control" name="field6">
                  <?php }else{ echo $_POST['field6']; } ?>
              </span>
              </div>
              <div class="col-lg-12 col-sm-12 col-md-12 user_fullname font-italic">
                <p class="">President</p>
              </div>
            </div>
          </div>
        </div>
<?php if(!isset($_GET['action'])){ ?>
        <button type="submit">Print</button>
<?php } ?>


      </div>
    </div>
  </div>

</form>

    <script src="pages\htmlfolder\assets/js/jquery.min.js"></script>
    <script src="pages\htmlfolder\assets/js/jquery.dataTables.min.js"></script>
    <script src="pages\htmlfolder\assets/js/bootstrap.min.js"></script>

    

</body>
</html>
<?php 

$user_id = $user['account_ID'];
  $action_event = "Print";
  if(!isset($_POST['field1'],$_POST['field2'],$_POST['field3'],$_POST['field4'],$_POST['field5'],$_POST['field6']))
  {

  $event_desc = "Module: 2 Registrar / 2.2 Find Student, DESCRIPTION: User clicked Print Diploma for ".$getsingle['si_FNAME'].' '.$getsingle['si_MNAME'].' '.$getsingle['si_LNAME'];

  }else{

  $event_desc = "Module: 2 Registrar / 2.2 Find Student, DESCRIPTION: User clicked Print Diploma for ".$getsingle['si_FNAME'].' '.$getsingle['si_MNAME'].' '.$getsingle['si_LNAME'].'. Given in'.' '.$_POST['field1'].',Philippines, this '.$_POST['field2'].' day of'.$_POST['field3'].' in the year of our lord ' .$_POST['field4']. '. Academy Registrar Name: '.$_POST['field5'].' '.'. President Name: '.$_POST['field6'];

  }
 $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
?>