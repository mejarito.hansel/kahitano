<?php
include('init.php');
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Student Affairs / Faculty Schedule, DESCRIPTION: User visited Faculty Schedule";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
header('location: faculty_loading.php?action=view');

?>