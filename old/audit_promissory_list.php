<?php
include('init.php');
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Accounting / Promissory list, DESCRIPTION: User visited Promissory list";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
header('location: promi_list.php');
?>