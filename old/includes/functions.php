<?php
//FUNCTIONS
date_default_timezone_set('Asia/Manila');
		function checkfile($file)
		{
			if(strtoupper($file) == strtoupper(basename($_SERVER["SCRIPT_FILENAME"], '.php')) )
			 {
			 return true;	
			 }else
			 {
			 return false;
			 }

		}
		
		function money($money)
		{
			return number_format($money, 2, '.', ',');
		
		}
		


		function msgbox($msg)
		{
				?>
                 	<script>
                    	alert("<?php print($msg); ?>");
                    </script>
                 <?php
		}


		function href($page)
		{
				?>
					<script>
						window.location.href="<?php print($page); ?>"
					</script>
				<?php
		}

		function close()
		{
				?>
					<script>
						window.close();
					</script>
				<?php
		}



		function clean($var)
		{
				return	 ($var);
		}


		function chksession()
		{
			session_start();
				if(!isset($_SESSION['username']))
				{
					msgbox("Session not active!");
					href("index.php");
				}
		}

?>
