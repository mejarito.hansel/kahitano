<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();


if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}

	



if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
    

    
  
    
	if ($user) 
	{
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();
	
			  
	
			
				SESSION::login($user);
                msgbox("Login Success");
               
               
				AUDIT::insert($user['account_ID'],"LOGGED IN");
				
				
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}



	//QUERIES




	
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
	  	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>

      <title>HOME | CSJP-II-AS</title>
	</head>
	<style>
	 .hide{
		 display: none;
	 }
	 .show{
		 display: inline-block;
	 }
	</style>
	<body>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
            
			<!-- alert messages -->
			<?php 
			
			SESSION::DisplayMsg(); 
			
			
			
			?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
				
				//audit
				
				
				
					?>
				<div class="col-md-3">
            		<?php include(PAGES."navigation.php");?>
				</div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 


								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>
								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

									</div>
								</div>
                                
                                	<div class="form-group row">
									<div class="col-lg-offset-9">
										 <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                                
                                </fieldset>
                    </form>
                    </div>	
                </div>
                
                <?php   }else{
				
				AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
				
				?>

<div class="col-md-3">

<?php include(PAGES."navigation.php");
 $CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
?>


</div>
              
                
<div class="col-md-9">
	<div class="panel panel-default">

		<div class="panel-heading">
			<table>
        		<tr>
        			<td width="50%">
						<h4 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"> Add Tuition Template <i id="spinner" style="display:none" class="fa fa-circle-o-notch fast-spin fa-fw"></i></h4>
					</td>
				 	<td>
            			<div class="input-group col-md-12 hide">
              				
              				<input id="searchItem" class="searchItem form-control"  type="text"  style="text-transform: uppercase;"/>
                            
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
            			</div>
        			</td>
        			<td width="5%">
	    				<div class="col-md-1">   
	    					<a id="export" class='btn btn-warning btn-ms' href='tuit_conf.php' >Back</a>
	    				</div>
	    			</td>
          </tr>
          <table border='0' class="table table-responsive text-md" style="width:80%" id="getaccountidforTuit" data-acct="<?php echo $user['account_ID']; ?>" data-acctip ="<?php echo $CLIENT_IP?>">
									<tr>
										<td>&nbsp;</td>
											<td><label for='template_name' class='col-md-12 h4'>TEMPLATE NAME</label></td>
											<td><input name='template_name' required class='form-control txtTemp' type='text'/>
											</td>
                            		</tr>
									<tr>
										<td>&nbsp;</td>
											<td><label for='tuition_FEE' class='col-md-12 h4'>TUITION FEE</label></td>
											<td><input name='tuition_FEE' required class='form-control sum1' value='0.00' type='number'/>
											</td>
                            		</tr>

									<tr >
                        				<td>&nbsp;</td>
											<td><label for='other_FEE' class='col-md-12 h4 regiMin'>REGISTRATION/OTHER</label></td>
											<td>
											<!-- <input name='other_FEE' required class='form-control' type='text'/> -->
											</td>
                            		</tr>
										

										
                            		<tr class='registrationFee'>
										<td>&nbsp;</td>
											<td><label for='athletics_FEE' class='col-md-12 mgLeft'>ATHLETICS FEE</label></td>
											<td><input name='athletics_FEE' required class='form-control sum2 '  type='number' value='0.00'/></td>
                            		</tr>

                            		<tr  class='registrationFee'>
										<td>&nbsp;</td>
											<td><label for='avr_FEE' class='col-md-12 mgLeft'>AVR FEE</label></td>
											<td><input name='avr_FEE'  required class='form-control sum3' type='number' value='0.00'/></td>
                            		</tr>

                            		<tr  class='registrationFee'>
										<td>&nbsp;</td>
											<td><label for='community_FEE' class='col-md-12 mgLeft'>COMMUNITY EXTENSION FEE</label></td>
											<td><input name='community_FEE' required class='form-control sum4' type='number' value='0.00'/></td>
                            		</tr>

                            		<tr  class='registrationFee'>
										<td>&nbsp;</td>
											<td><label for='dev_FEE' class='col-md-12 mgLeft'>DEVELOPMENTAL FEE</label></td>
											<td><input name='dev_FEE' required class='form-control sum5' type='number' value='0.00'/></td>
                                    </tr>

                                    <tr  class='registrationFee'>
										<td>&nbsp;</td>
											<td><label for='lib_FEE' class='col-md-12 mgLeft'>LIBRARY FEE</label></td>
											<td><input name='lib_FEE'required class='form-control sum37'type='number' value='0.00'/></td>
                            		</tr>

									<tr  class='registrationFee'>
                        				<td>&nbsp;</td>
											<td><label for='guidance_FEE' class='col-md-12 mgLeft'>GUIDANCE & COUNSELING</label></td>
											<td><input name='guidance_FEE' required class='form-control sum38' type='number' value='0.00'/></td>
                            		</tr>

									<tr  class='registrationFee'>
										<td>&nbsp;</td>
											<td><label for='it_FEE' class='col-md-12 mgLeft'>IT SERVICE FEE</label></td>
											<td><input name='it_FEE' required class='form-control sum6' type='number' value='0.00'/></td>
                            		</tr>

									<tr  class='registrationFee'>
										<td>&nbsp;</td>
											<td><label for='internet_FEE' class='col-md-12 mgLeft'>INTERNET FEE</label></td>
											<td><input name='internet_FEE' required class='form-control sum7' type='number' value='0.00'/></td>
                            		</tr>

									<tr  class='registrationFee'>
										<td>&nbsp;</td>
											<td><label for='dent_FEE' class='col-md-12 mgLeft'>MEDICAL/DENTAL FEE</label></td>
											<td><input name='dent_FEE' required class='form-control sum8' type='number' value='0.00'/></td>
                            		</tr>

									<tr  class='registrationFee'>
                        				<td>&nbsp;</td>
											<td><label for='medx_FEE' class='col-md-12 mgLeft'>MEDICAL EXAM (yearly)</label></td>
											<td><input name='medx_FEE' required class='form-control sum9' type='number' value='0.00'/></td>
                            		</tr>

									<tr  class='registrationFee'>
                        				<td>&nbsp;</td>
											<td><label for='pub_FEE' class='col-md-12 mgLeft'>PUBLICATION</label></td>
											<td><input name='pub_FEE' required class='form-control sum10' type='number' value='0.00'/></td>
                            		</tr>

									<tr  class='registrationFee'>
										<td>&nbsp;</td>
											<td><label for='reg_FEE' class='col-md-12 mgLeft'>REGISTRATION FEE</label></td>
											<td><input name='reg_FEE' required class='form-control sum36' type='number' value='0.00'/></td>
                            		</tr>

									<tr  class='registrationFee'>
                        				<td>&nbsp;</td>
											<td><label for='id_FEE' class='col-md-12 mgLeft'>ID</label></td>
											<td><input name='id_FEE' required class='form-control sum11' type='number' value='0.00'/></td>
                            		</tr>

									<tr  class='registrationFee'>
                        				<td>&nbsp;</td>
											<td><label for='guide_FEE' class='col-md-12 mgLeft'>GUIDENOTES</label></td>
											<td><input name='guide_FEE' required class='form-control sum12' type='number' value='0.00'/></td>
									</tr>
							
									<!-- LABORATORY FEES -->
									<tr>
                        				<td>&nbsp;</td>
											<td><label for='labs_FEE' class='col-md-12 h4 labMin' >LABORATORY</label></td>
											<td>
											<!-- <input name='labs_FEE' required class='form-control' type='text' value='0.00'/> -->
											</td>
									</tr>
									
									<tr  class='labFee'>
                        				<td>&nbsp;</td>
											<td><label for='seagull_FEE' class='col-md-12 mgLeft'>SEAGULL</label></td>
											<td><input name='seagull_FEE' required class='form-control sum13' type='number' value='0.00'/></td>
									</tr>

									<tr  class='labFee'>
                        				<td>&nbsp;</td>
											<td><label for='gen_FEE' class='col-md-12 mgLeft'>GEN. ED. LAB FEE</label></td>
											<td><input name='gen_FEE' required class='form-control sum14' type='number' value='0.00'/></td>
									</tr>

									<tr  class='labFee'>
                        				<td>&nbsp;</td>
											<td><label for='techlab_FEE' class='col-md-12 mgLeft'>TECH LAB FEE</label></td>
											<td><input name='techlab_FEE' required class='form-control sum15' type='number' value='0.00'/></td>
									</tr>

									<tr  class='labFee'>
                        				<td>&nbsp;</td>
											<td><label for='comp_FEE' class='col-md-12 mgLeft'>COMPUTER LAB FEE</label></td>
											<td><input name='comp_FEE' required class='form-control sum16' type='number' value='0.00'/></td>
									</tr>

									<tr  class='labFee'>
                        				<td>&nbsp;</td>
											<td><label for='ship_FEE' class='col-md-12 mgLeft'>SHIPBOARD FAMILIARIZATION</label></td>
											<td><input name='ship_FEE' required class='form-control sum17' type='number' value='0.00'/></td>
									</tr>

									<tr  class='labFee'>
                        				<td>&nbsp;</td>
											<td><label for='kumon_FEE' class='col-md-12 mgLeft'>KUMON MATH</label></td>
											<td><input name='kumon_FEE' required class='form-control sum18' type='number' value='0.00'/></td>
									</tr>

									<!-- BOOKS -->
									<tr>
                        				<td>&nbsp;</td>
											<td><label for='books_FEE' class='col-md-12 h4 bookMin'>BOOKS</label></td>
											<td>
											<!-- <input name='books_FEE' required class='form-control' type='text' value='0.00'/> -->
											</td>
									</tr>

									<tr class='bookFee'>
                        				<td>&nbsp;</td>
											<td><label for='mart_FEE' class='col-md-12 mgLeft'>MARITIME TEXT BOOKS</label></td>
											<td><input name='mart_FEE' required class='form-control sum19 ' type='number' value='0.00'/></td>
									</tr>

									<tr class='bookFee'>
                        				<td>&nbsp;</td>
											<td><label for='genBooks_FEE' class='col-md-12 mgLeft'>GEN ED BOOKS</label></td>
											<td><input name='genBooks_FEE' required class='form-control sum20' type='number' value='0.00'/></td>
									</tr>

									<!-- MISCELLANEOUS FEES -->

									<tr>
                        				<td>&nbsp;</td>
											<td><label for='misce_FEE' class='col-md-12 h4 miscMin'>MISCELLANEOUS</label></td>
											<td>
											<!-- <input name='misce_FEE' required class='form-control' type='text' value='0.00'/> -->
											</td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='accommodation_FEE' class='col-md-12 mgLeft'>ACCOMMODATION</label></td>
											<td><input name='accommodation_FEE' required class='form-control sum21' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='mess_FEE' class='col-md-12 mgLeft'>MESS SERVICE</label></td>
											<td><input name='mess_FEE' required class='form-control sum22' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='transpo_FEE' class='col-md-12 mgLeft'>TRANSPORTATION</label></td>
											<td><input name='transpo_FEE' required class='form-control sum23' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='uni_FEE' class='col-md-12 mgLeft'>UNIFORMS</label></td>
											<td><input name='uni_FEE' required class='form-control sum24' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='grad_FEE' class='col-md-12 mgLeft'>GRADUATION</label></td>
											<td><input name='grad_FEE' required class='form-control sum25' type='number' value='0.00'/></td>
										</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='tor_FEE' class='col-md-12 mgLeft'>DIPLOMA/TOR</label></td>
											<td><input name='tor_FEE' required class='form-control sum26' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='examMat_FEE' class='col-md-12 mgLeft'>EXAM MATERIALS</label></td>
											<td><input name='examMat_FEE' required class='form-control sum27' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='research_FEE' class='col-md-12 mgLeft'>RESEARCH FEE</label></td>
											<td><input name='research_FEE' required class='form-control sum28' type='text' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='nstp_FEE' class='col-md-12 mgLeft'>NSTP FEE</label></td>
											<td><input name='nstp_FEE' required class='form-control sum29' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='prisaa_FEE' class='col-md-12 mgLeft'>PRISAA FEE</label></td>
											<td><input name='prissa_FEE' required class='form-control sum30' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='recreational_FEE' class='col-md-12 mgLeft'>RECREATIONAL FEE</label></td>
											<td><input name='recreational_FEE' required class='form-control sum31' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='sports_FEE' class='col-md-12 mgLeft'>SPORTS/TOURNAMENT FEE</label></td>
											<td><input name='sports_FEE' required class='form-control sum32' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='org_FEE' class='col-md-12 mgLeft'>STUDENT ORGANIZATION FEE</label></td>
											<td><input name='org_FEE' required class='form-control sum33' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='yearBook_FEE' class='col-md-12 mgLeft'>YEAR BOOK FEE</label></td>
											<td><input name='yearBook_FEE' required class='form-control sum34' type='number' value='0.00'/></td>
									</tr>

									<tr class='miscFee'>
                        				<td>&nbsp;</td>
											<td><label for='insurance_FEE' class='col-md-12 mgLeft'>INSURANCE</label></td>
											<td><input name='insurance_FEE' required class='form-control sum35' type='number' value='0.00'/></td>
									</tr>

									<tr>
                        				<td>&nbsp;</td>
											<td><label for='total_FEE' class='col-md-12 mgLeft'><h3>TOTAL:</h3></label></td>
											<td><input id='total_FEE' name='total_FEE'required disabled class='form-control mgTop mgBottom' type='number'/></td>
									</tr>

                            		<tr>
                            		  	<td class='' colspan='2'> 
	                            		  	<div class="err alert alert-danger hide">
												<strong>Warning!</strong> <span class='errorMsg'></span>
											</div> 
										</td>                                   
                            		  
                       		        <td align='center'><button id='btnSum' class='btn btn-success btn-ms mgTop'><i class='fa fa-save' style='color: white'></i> SAVE </button></td>
                       		  </tr>
                            </table>
	    	</table>
		</div>
		<div class="panel-body"><br>
			<div class="row">
            	<div class="form-group">
	               
            	</div>
           	</div><br>
           	<div id="dvData">
	        
		    </div>
		 
	 	</div>
	</div>
</div>
<?php } ?>
  	
			</div>
            <?php 
			?>
			<?php include (LAYOUTS . "footer.php"); ?>
 			<script src="<?php echo HOME; ?>old/assets/js/custom/tuit.js"></script>

		</div>
  <script type="text/javascript">
  	$( window ).ready(function(){
        setTimeout(function() {

      		$.main.tuit.tuitSum();
          
        },100);
  	})

  </script>
</script>
	</body>
</html>
