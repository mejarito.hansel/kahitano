<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();


if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}
if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
	if ($user) 
	{
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();

				SESSION::login($user);
                msgbox("Login Success");
               
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}

	//QUERIES
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
      <title>HOME | School Management System v2.0</title>

      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
	</head>

	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
            
			<!-- alert messages -->
			<?php 
			
			SESSION::DisplayMsg(); 
			
			
			
			?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
				
				//audit
				
				
				
					?>
				<div class="col-md-3">
            		<?php include(PAGES."navigation.php");?>
				</div>
                <div class="col-md-9">
                <div class="well">
                    <form class="form-horizontal" method="POST">
                    
                    <fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 


								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>
								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>

									</div>
								</div>
                                
                                	<div class="form-group row">
									<div class="col-lg-offset-9">
										 <button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
                                         
                                       
                                        
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                                
                                </fieldset>
                    </form>
                    </div>	
                </div>
                
                <?php   }else{
					// AUDIT::insert_new(,"View", "MODULE: Home, DESCRIPTION: User visits home page");

					// echo "<script> $(function(){ audit_trail.insert('".$user['account_ID']."','".$_SERVER['REMOTE_ADDR']."','View','MODULE: Home, DESCRIPTION: User visists home page');}) </script>";
				?>
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
              

                
                <div class="col-md-9">
             		
                    
                    
                    	<div class="panel panel-default">  
    
							<div class="panel-heading"> 
                 
                    
				<center >
                <br />	<h4>WELCOME <b><?=  strtoupper($user['account_FNAME'])." ".strtoupper($user['account_LNAME']);?></b> TO THE MANAGEMENT PORTAL! </h4><br />
                <br />
                </center>    
			
            
            		<div class="col-lg-12" style="text-align:justify">Please note that every activity is monitored closely. For any problem in the system, contact IT Manager for details. Click the links under NAVIGATION to select operation. It is recommended to logout by clicking the logout button everytime you leave your PC.
                    <br /> <br />
                    If you do not agree with the terms and conditions or you are not <b><?php echo $user['account_UNAME']; ?></b>, please <a href="logout.php">logout</a>.
                 

                    </div>
            	
                  
                  <!-- <h2> <?php echo $user['access_NAME']; ?> -->
         <br />
		 
							
				    
                    </div>
                    
                    
                    </div>


					<div class="col-md-12">
					
						<?php if($acid == '5'){ ?>
		 
							
								<table class="table table-hover table-responsive table-striped text-md">
									
                                    <th style="width:5%;">#</th>
                                    <th style="width:15%;">Section</th>
                                    <th style="width:40%;">Subject</th>
									<th style="width:10%%;">Locked</th>
									<th style="width:10%;">Checked</th>
                                    <th style="">Operation</th>
										
                                    <?php
									
									$getmysubj = SUBJSCHED::getByfaculty(14, $instructor_ID);
									#print_r($getmysubj);
									$totalcount = count($getmysubj);
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                   // $viewSems = SEMSECTION::getAll(array("semester_section_ID" => "DESC"));
									
									
                                     if(count($getmysubj)>=50) 
                                     {
                                         $pagination2 = new Pagination($getmysubj, 50, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($getmysubj, 50, NULL);
                                     }

                                     $getmysubj = $pagination2->get_array();
									 $x = 1;

                                     if($getmysubj) {
                                        foreach($getmysubj as $key => $value){
                                    ?>
                                        <tr>
                                            <td>
											<?php 
												if(isset($_GET['p']))
												{
												$p = $_GET['p'];
												
												$num = 	$x+($p*10)-10;
												echo $num;
												$x++;
												
												}else{
													echo $x++;
												}
											 ?>
                                            <td><?php $c=COURSE::getbyID($value['course_ID']); 
												echo $c['course_INIT']."-";
                                              echo $value['semester_section_NAME']; ?>
                                            
                                          
                                            <td><?php $s = SUBJECTS::getID($value['subject_ID']);
												echo $s['subject_CODE']."/".$s['subject_DESCRIPTION'];
												 ?> 
												 </td>
                                            <td ><?php switch($value['is_lock'])
											{
												case 0: echo "Unlocked"; break;
												case 1: echo "Locked"; break;	
											} ?></td>
                                            <td ><?php switch($value['is_check'])
											{
												case 0: echo "Unchecked"; break;
											 	case 1: echo "Checked"; break;
											}
											?>
                                            <td>
											
													<a href="grade_encoding.php?action=view&subject_sched_ID=<?= $value['subject_sched_ID']; ?>" class='btn btn-warning btn-xs'>View</a>
													
													<a onclick="return popitup('gs.php?id=<?php echo $value['subject_sched_ID']; ?>')" href="#" class='btn btn-success btn-xs'>GS</a>
                                            </td>
                                  </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
                                Total Records: <?=  $totalcount; ?>
                                        <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view&sem_ID='.$_GET['sem_ID'].'&fac_id='.$_GET['fac_id'].'') ?>
                                        </center>
		 
		 
		 
							<?php } ?>
					
					</div>
                    
                   <div class="col-md-6">
				   

                    <?php #include(PAGES."announcement.php");?>
                    
                       <?php 
					
					
					
						#CHANGELOGS::getRecentLogs();
					#SESSION::StoreUpd("User Successfully Updated!<br>1111", "changes");
					
					
					
				
					
					 ?>
				</div>
                    
                 </div>








				 
				      <?php } ?>
                
                
				<!-- END BODY -->
				
				<!-- right nav -->
               
              
               
               

					
			</div>
                
				<!-- end right nav -->
				
		
			<!-- end row container -->
            
            

			<!-- footer-->      
            
            <?php 
	
			?>
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->
		<script type="text/javascript">
		  $("#btnLogout").click(function(){
		    $("#myModal").modal('show');
		  });

		</script>
	</body>
</html>
