<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

 $user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Accounting / Payment Option, DESCRIPTION: User visited Payment Option";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
if (SESSION::isLoggedIn()) {
	#HTML::redirect();
}

	



if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
    

    
  
    
	if ($user) 
	{
		#$ip =get_client_ip();
		#	$security_option = navCat::security_option();
	
			  
	
			
				SESSION::login($user);
                msgbox("Login Success");
               
               
				AUDIT::insert($user['account_ID'],"LOGGED IN");
				
				
				if(isset($_SESSION['previous_page']))
				{
					header("location:$_SESSION[previous_page]");
				}else{
					HTML::redirect();
				}
		

	} else {
       
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}



	//QUERIES


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
	  	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
		<script src="<?php echo HOME; ?>old/assets/js/bootstrap.minv3.js"></script>
		

      <title>Payment Option Management | School System Management</title>
	</head>

	<body>
			<div class="modal fade" id="myModal" class="classModal" role="dialog">
			    <div class="modal-dialog">
			      	<div class="modal-content" >
			        	<div class="" style="padding:40px 40px 0px 40px; text-align: center;">
			          		<button type="button" class="close" data-dismiss="modal">&times;</button>
			          		<h4 style=""><i class="fa fa-edit"></i>Update Cadet Payment</h4>
			        	</div>
			       		<div class="panel-body" style="padding:40px 60px;">
				        		<div class="row">
					                <div class="col-lg-12">
					                	<center><h3 style="color:green;" id="update" class="hidden">Successfully Updated</h3></center>
				                  		<label for="Day" class=""> Semester</label>
				                  		<select name="day" id="UpSem" class="form-control">
				                  			<option selected>-- Select Year & Semester --</option>
				                  		</select>
						                    
				                	</div>
              					</div>
              					<br>
				            	<div class="row">
				            		<div class="col-lg-12">
				                  		<label class=""> Name</label>
				                  		<input name='sem_NAME' id="UpName" class='form-control' type='text' required/>
                					</div>
              					</div>
              					<br>
              					<div class="row">
				                	<div class="col-lg-4">
				                  		<label for="Day" class=""> Book Fees</label>
				                  		<input name='sem_NAME' id="UpBookFees" class='form-control' type='text' required/>
				                	</div>
					                <div class="col-lg-4">
				                  		<label for="Day" class=""> RegOther Fees</label>
				                  		<input name='sem_NAME' id="UpRegOtherFees" class='form-control' type='text' required/>
				                	</div>
					                <div class="col-lg-4">
				                  		<label for="Day" class=""> Laboratory Fees</label>
				                  		<input name='sem_NAME' id="UpLabFees" class='form-control' type='text' required/>
				                	</div>
					                
				            	</div>
				                <div class="row">
				                	<div class="col-lg-4">
				                  		<label for="Day" class=""> Laboratory</label>
				                  		<input name='sem_NAME' id="UpLab" class='form-control' type='text' required/>
				                	</div>
					                <div class="col-lg-4">
				                  		<label for="Day" class=""> Lecture</label>
				                  		<input name='sem_NAME' id="UpLec" class='form-control' type='text' required/>
				                	</div>
					                <div class="col-lg-4">
				                  		<label for="Day" class=""> Miscellaneous</label>
				                  		<input name='sem_NAME' id="UpMisc" class='form-control' type='text' required/>
				                	</div>
					                
				            	</div>
              					<br>
                				<div class="row">
                					<div class="col-lg-4">
				                  		<label for="Day" class=""> Discount Tuition Fee ( % ) </label>
				                  		<select name='up_po_discount_tf' id="up_po_discount_tf" class='form-control' required>
										</select>
				                	</div>
					                <div class="col-lg-4">
				                  		<label for="Day" class=""> Discount Miscellaneous ( % ) </label>
				                  		<select name='up_po_discount_misc' id="up_po_discount_misc" class='form-control' required>
										</select>
				                	</div>
				                	<div class="col-lg-4">
				                  		<label for="Day" class=""> Installment Fee ( fixed ) </label><br><br>
				                  		<input name='sem_NAME' id="up_po_installment_fee" class='form-control' type='text' required/>
				                	</div>
				                	
              					</div> 
              					<br>
					            <div class="row ">
					                <div class="col-lg-4">
				                  		<label for="Day" class=""> Additional Tuition Fee ( % ) </label><br><br>
				                  		<select name='up_po_additional_tf' id="up_po_additional_tf" class='form-control' required>
										</select>
				                	</div>
				                	<div class="col-lg-4">
				                  		<label for="Day" class=""> Additional Tution Fee ( fixed ) </label><br><br>
				                  		<input name='sem_NAME' id="up_po_additional_tfee" class='form-control' type='text' required/>
				                	</div>
				                	<div class="col-lg-4">
				                  		<label for="Day" class=""> Additional Miscellaneous ( % ) </label><br><br>
				                  		<select name='up_po_additional_misc' id="up_po_additional_misc" class='form-control' required>
										</select>
				                	</div>
					            </div>
              					<br>  
              					<div class="row">
					                <div class="col-lg-4">
						                <label for="Day2" class="">Date 1</label>
						                <input type="date" id="up_po_DATE1" class='form-control'>
					                </div>
					                <div class="col-lg-4">
						                <label for="Day2" class="">Date 2</label>
						                <input type="date" id="up_po_DATE2" class='form-control'>
					                </div>
					                <div class="col-lg-4">
						                <label for="Day2" class="">Date 3</label>
						                <input type="date" id="up_po_DATE3" class='form-control'>
					                </div>
					            </div>
           						<br>  
              					<div class="row">
              						<div class="col-lg-4">
						            	<label for="Day2" class="">Date 4</label>
						            	<input type="date" id="up_po_DATE4" class='form-control'>
					            	</div>
              						<div class="col-lg-4">
						                <label for="Day2" class="">Date 5</label>
						                <input type="date" id="up_po_DATE5" class='form-control'>
					                </div>
					                <div class="col-lg-4">
						                <label for="Day2" class="">PO SNPL</label>
						                <select name='up_po_SNPL' id="up_po_SNPL" class='form-control' required>
										</select>
					                </div>
              					</div>
              					<br>
			          			<button class="btn btn-success btn-block" id="updatePaymentOption">Update</button>
			        	</div>
			       	</div>
			    </div>
		  	</div> 
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
            
			<!-- alert messages -->
			<?php 
			
			SESSION::DisplayMsg(); 
			
			
			
			?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
				
				//audit
				
				
				
					?>
				<div class="col-md-3">
            		<?php include( PAGES."navigation.php");?>
				</div>
                <div class="col-md-9">
                	<div class="well">
                    	<form class="form-horizontal" method="POST">
                    		<fieldset>
								<legend>Login</legend>

								<!-- if errors --> 
								<?php SESSION::DisplayMsg(); ?>
								<!-- end errors --> 

								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputUsername" class="col-lg-2 control-label">Username</label>
									<div class="col-lg-10">
										<input type="text" class="form-control" id="inputUsername" placeholder="Username" autofocus required autocomplete="off" name="username">
									</div>
								</div>

								<div class="form-group <?php if (isset($_SESSION['errors'])) { ?>has-error<?php } ?>">
									<label for="inputPassword" class="col-lg-2 control-label">Password</label>
									<div class="col-lg-10">
										<input type="password" class="form-control" id="inputPassword" name="password" placeholder="Password" required>
									</div>
								</div>
                                
                                <div class="form-group row">
									<div class="col-lg-offset-9">
										<button type="button" onclick="parent.location='reset.php'" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Forgot</button>
										<button type="submit" class="btn btn-primary" style="border-color: <?php #echo $getTheme[0]['link_onactive']; ?>;background:<?php #echo $getTheme[0]['top_backcolor']; ?>; ">Submit</button>
									</div>
								</div>
                            </fieldset>
                    	</form>
                    </div>	
                </div>
                
                <?php   }else{
				
				AUDIT::insert($user['account_ID'],"VISITS HOMEPAGE");
				
				?>

				<div class="col-md-3">
				<?php include(PAGES."navigation.php");?>
				</div>
              
              	<div class="col-md-9">
					<div class="panel panel-default">
						<div class="panel-heading">
          					<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
          					<i class="fa fa-credit-card"></i>  Payment Options </h3> 
        				</div>
        				<?php
			           		$CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];


			           	?>
				        <div class="panel-body">
				        	<table id="myTable" data-ip="<?php echo $CLIENT_IP;?>" data-acct="<?php echo $user['account_ID'];?>">
				        		<h3 style="color:green;" id="success" class="hidden">Successfully Added Payment Option</h3>
				        		<div class="row">
					                <div class="col-lg-6">
				                  		<label for="Day" class=""> Semester</label>
				                  		
				                  		<select name="day" id="day" class="form-control">
				                  			<option value="">--- SELECT YEAR & SEMESTER ---</option>
				                  		</select>
				                	</div>
					                <div class="col-lg-6">
				                  		<label for="Day" class="">Fee</label>
				                  		<h3 style="color:green;" id="success" class="hidden">Successfully Added Payment Option</h3>
				                  		<select name="fee" id="fee" class="form-control">
				                  			<option value="">--- SELECT FEE CONFIG ---</option>
				                  		</select>     
				                	</div>
              					</div>
              					<br>
				            	<div class="row">
				            		<div class="col-lg-12">
				                  		<label class=""> Name</label>
				                  		<input name='sem_NAME' id="po_NAME" class='form-control' type='text'/>
                					</div>
              					</div>
              					<br>
				                <div class="row">
				                	<div class="col-lg-6">
				                  		<label for="Day" class=""> Book Fees</label>
				                  		<input name='sem_NAME' id="po_book_fees"class='form-control 4' type='text'  />
				                	</div>
					                <div class="col-lg-6">
				                  		<label for="Day" class=""> Registration/Others Fees</label>
				                  		<input name='sem_NAME' id="po_regother_fees" class='form-control' type='text' />
				                	</div>
				                </div>
				                <div class="row">
					                <div class="col-lg-6">
				                  		<label for="Day" class=""> Laboratory Fees</label>
				                  		<input name='sem_NAME' id="po_lab_fees" class='form-control' type='text' />
				                	</div>
					                <div class="col-lg-6">
				                  		<label for="Day" class=""> Miscellaneous</label>
				                  		<input name='sem_NAME' id="po_MISC" class='form-control' type='text' />
				                	</div>
					                
				            	</div>
              					<br>
              					<div class="row">
              						<div class="col-lg-6">
				                  		<label for="Day" class=""> Lecture</label>
				                  		<input name='sem_NAME' id="po_LEC"class='form-control 4' type='text' />
				                	</div>
				                	<div class="col-lg-6">
				                  		<label for="Day" class=""> Laboratory</label>
				                  		<input name='sem_NAME' id="po_LAB"class='form-control 4' type='text'  />
				                	</div>
              					</div>
                				<div class="row">
                					<div class="col-lg-4">
				                  		<label for="Day" class=""> Discount Tuition Fee (%)</label>
				                  		<!-- <input name='sem_NAME' id="po_discount_tf" class='form-control' type='text' required/> -->
				                  		<select name='sem_NAME' id="po_discount_tf" class='form-control' required>
											<option value="">--- Select Discount ---</option>
											<?php
												for($i=1; $i<=9; $i++){
													?>
													<option value="<?php echo '.0'.$i;?>"><?php echo '0'.$i.'%';?></option>
													<?php
												}
												for($i=10; $i<=99; $i++){
													?>
													<option value="<?php echo '.'.$i;?>"><?php echo $i.'%';?></option>
													<?php
												}
											?>
											<option value="1">100%</option>
										</select>
				                	</div>
					                <div class="col-lg-4">
				                  		<label for="Day" class=""> Discount Miscellaneous (%)</label>
				                  		<!-- <input name='sem_NAME' id="po_discount_misc" class='form-control' type='text' required/> -->
				                  		<select name='sem_NAME' id="po_discount_misc" class='form-control' required>
											<option value="">--- Select Discount ---</option>
											<?php
												for($i=1; $i<=9; $i++){
													?>
													<option value="<?php echo '.0'.$i;?>"><?php echo '0'.$i.'%';?></option>
													<?php
												}
												for($i=10; $i<=99; $i++){
													?>
													<option value="<?php echo '.'.$i;?>"><?php echo $i.'%';?></option>
													<?php
												}
											?>
											<option value="1">100%</option>
										</select>
				                	</div>
				                	<div class="col-lg-4">
				                  		<label for="Day" class=""> Installment Fee (fixed)</label>
				                  		<input name='sem_NAME' id="po_installment_fee" class='form-control' type='text' required/>
				                	</div>
              					</div> 
              					<br>
					            <div class="row ">
					                <div class="col-lg-4">
				                  		<label for="Day" class=""> Additional Tuition Fee (%) </label>
				                  		<!-- <input name='sem_NAME' id="po_additional_tf" class='form-control' type='text' required/> -->
				                  		<select name='sem_NAME' id="po_additional_tf" class='form-control' required>
											<option value="">--- Select Discount ---</option>
											<?php
												for($i=1; $i<=9; $i++){
													?>
													<option value="<?php echo '.0'.$i;?>"><?php echo '0'.$i.'%';?></option>
													<?php
												}
												for($i=10; $i<=99; $i++){
													?>
													<option value="<?php echo '.'.$i;?>"><?php echo $i.'%';?></option>
													<?php
												}
											?>
											<option value="1">100%</option>
										</select>
				                	</div>
				                	<div class="col-lg-4">
				                  		<label for="Day" class=""> Additional Tution Fee (fixed) </label>
				                  		<input name='sem_NAME' id="po_additional_tfee" class='form-control' type='text' required/>
				                	</div>
				                	<div class="col-lg-4">
				                  		<label for="Day" class=""> Additional Miscellaneous (%)</label>
				                  		<!-- <input name='sem_NAME' id="po_additional_misc" class='form-control' type='text' required/> -->
				                  		<select name='sem_NAME' id="po_additional_misc" class='form-control' required>
											<option value="">--- Select Discount ---</option>
											<?php
												for($i=1; $i<=9; $i++){
													?>
													<option value="<?php echo '.0'.$i;?>"><?php echo '0'.$i.'%';?></option>
													<?php
												}
												for($i=10; $i<=99; $i++){
													?>
													<option value="<?php echo '.'.$i;?>"><?php echo $i.'%';?></option>
													<?php
												}
											?>
											<option value="1">100%</option>
										</select>
				                	</div>
					            </div>
              					<br>  
              					<div class="row">
					                <div class="col-lg-4">
						                <label for="Day2" class="">Date 1</label>
						                <input type="date" id="po_DATE1" class='form-control'>
					                </div>
					                <div class="col-lg-4">
						                <label for="Day2" class="">Date 2</label>
						                <input type="date" id="po_DATE2" class='form-control'>
					                </div>
					                <div class="col-lg-4">
						                <label for="Day2" class="">Date 3</label>
						                <input type="date" id="po_DATE3" class='form-control'>
					                </div>
					            </div>
           						<br>  
              					<div class="row">
              						<div class="col-lg-4">
						            	<label for="Day2" class="">Date 4</label>
						            	<input type="date" id="po_DATE4" class='form-control'>
					            	</div>
              						<div class="col-lg-4">
						                <label for="Day2" class="">Date 5</label>
						                <input type="date" value="" id="po_DATE5" class='form-control'>
					                </div>
					                <div class="col-lg-4">
						                <label for="Day2" class="">Payment Option SNPL</label>
						                <select name='po_SNPL' id="po_SNPL" class='form-control' required>
											<option value="">--- Select Discount ---</option>
											<?php
												for($i=1; $i<=9; $i++){
													?>
													<option value="<?php echo '.0'.$i;?>"><?php echo '0'.$i.'%';?></option>
													<?php
												}
												for($i=10; $i<=99; $i++){
													?>
													<option value="<?php echo '.'.$i;?>"><?php echo $i.'%';?></option>
													<?php
												}
											?>
											<option value="1">100%</option>
										</select>
					                </div>
              					</div>
              					<br> 
					            <div class="row ">
					                <!-- <div class="col-lg-12 "> -->
					                  	<!-- <div class="row"> -->
					                <div class="col-lg-4">
					                </div>
					                <div class="col-lg-4"><br/>
					                    <button id="button" class="btn btn-success btn-block P6C9_SAVE" type="submit" onclick="return confirm('Do You Want Save ?')"><i class="fa fa-edit"></i> Save
					                    </button>
					                </div>
					                <div class="col-lg-4">
					                </div>
					                    	<!-- <div class="col-lg-1">
					                    	</div> -->
					                    	
					                    	<!-- <div class="col-lg-1"></div> -->
					                  	<!-- </div> -->
					            </div>
					        </table>
					    </div>
					</div>

					<div class="panel panel-default">
						<div class="panel-heading">
          					<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
          					<i class="fa fa-credit-card"></i>  Payments</h3> 
        				</div>
						<div class="panel-body"><br>
		           			<hr>
		           			<div id="dvData">
			        			<table id="myTable1" class="table table-hover table-responsive table-striped text-md">
			        				 <div class="col-sm-5">
                                    <input name='' id="myInput" placeholder="Search by Semester, Name, Book Fees, Lab Fees, Reg Fees, Lab, Lec or Date" class='form-control' type='text' onkeyup="myFunction()" required autofocus style="font-style: italic;"/>
                                </div>
					        		<thead>

									    <tr> <!-- padding:top right bottom left; -->
									        <th style="padding: 1rem 6rem 1rem 6rem;"> Semester</th>
									        <th style="padding: 1rem 9rem 1rem 9rem;"> Name</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Book Fees</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Laboratory Fees</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Register/Other Fees</th>
									        <th style="padding: 1rem 4rem 1rem 4rem;"> Laboratory</th>
									        <th style="padding: 1rem 4rem 1rem 4rem;"> Lecture</th>
									        <th style="padding: 1rem 4rem 1rem 4rem;"> Miscellaneous</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Discount Tuition Fee</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Discount Miscellaneous</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Installment Fee</th>
									        <th style="padding: 1rem 4rem 1rem 4rem;"> Additional Tuition Fee(%)</th>
									       	<th style="padding: 1rem 4rem 1rem 4rem;"> Additional Tuition Fee(fixed)</th>
									        <th style="padding: 1rem 4rem 1rem 4rem;"> Additional Miscellaneous</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Date 1</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Date 2</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Date 3</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Date 4</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Date 5</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Payment Option SNPL</th>
									        <th style="padding: 1rem 3rem 1rem 3rem;"> Action</th>
										</tr>
					        		</thead>
					        		<tbody id="tbody">
					        			
					        		</tbody>
					    		</table>
							</div>
		 				</div>
		 			</div>
				</div>
          	</div>
     	</div>
     
				<?php } ?>
				  	
		
				            <?php 
							?>
							<?php include (LAYOUTS . "footer.php"); ?>
 		<script src="<?php echo HOME; ?>old/assets/js/custom/payment.js"></script>

 		<script type="text/javascript">
 			$( window ).ready(function(){
		        setTimeout(function() {
		      		$.main.payment.getAllPaymentOption();
		        },100);
		  	})
 		</script>	  	
	</body>
</html>
<script type="text/javascript">
    function myFunction() {
        var input, filter, table, tr, td, i;
          input = document.getElementById("myInput");
          console.log(input);
          filter = input.value.toUpperCase();
          table = document.getElementById("myTable1");
          tr = table.getElementsByTagName("tr");
          for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            td2 = tr[i].getElementsByTagName("td")[1];
            td1 = tr[i].getElementsByTagName("td")[2];
            td3 = tr[i].getElementsByTagName("td")[3];
            td4 = tr[i].getElementsByTagName("td")[4];
            td5 = tr[i].getElementsByTagName("td")[5];
            td6 = tr[i].getElementsByTagName("td")[6];
            td7 = tr[i].getElementsByTagName("td")[7];
            if (td) {
              if (td.innerHTML.toUpperCase().indexOf(filter) > -1 || td1.innerHTML.toUpperCase().indexOf(filter) > -1 || td2.innerHTML.toUpperCase().indexOf(filter) > -1 || td3.innerHTML.toUpperCase().indexOf(filter) > -1 || td4.innerHTML.toUpperCase().indexOf(filter) > -1 || td5.innerHTML.toUpperCase().indexOf(filter) > -1 || td6.innerHTML.toUpperCase().indexOf(filter) > -1 || td7.innerHTML.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
              } else {
                tr[i].style.display = "none";
              }
            }   
        }
    }
</script>
<style type="text/css">
    .dataTables_filter, .dataTables_info { display: none; }

</style>