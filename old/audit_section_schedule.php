<?php
include('init.php');
$user_id = $user['account_ID'];
$action_event = "View";
$event_desc = "MODULE: Student Affairs / Section Schedule, DESCRIPTION: User visited Section Schedule";
$audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);

header("Location: sectioning.php?action=search");

?>