<?php
class NAV{

	
	
 	public static function SIS(){
		?><a href="#" data-toggle="modal" data-target="#modal_student_information">
			<div class="tile col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-file-text "></span></tile>
				<br>Student Information
			</div>
		</a>
		<!-- Modal SIS -->
		<div id="modal_student_information" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-file-text"></i> Student Information</h4>
			  </div>
			  <div class="modal-body">
					<a href="old/student_information.php?action=add"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-plus-square pull-left"></i> Add Student</button></a>
					<a href="old/student_information.php?action=view"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-file-text pull-left"></i> View Student</button></a>
					<a href="old/curricullum_enrolled.php?action=add"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-outdent pull-left"></i> Student Course Selection</button></a>
					<a href="old/curricullum_enrolled.php?action=view"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-list-ul pull-left"></i> Curriculum Enrolled</button></a>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>	
		<!-- Modal SIS -->
		
		<?php
	}	
	
		public static function CREDENTIALS(){
		?><a href="#" data-toggle="modal" data-target="#modal_credentials">
			<div class="tile col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-file-text "></span></tile>
				<br>Credentials
			</div>
		</a>
		<!-- Modal SIS -->
		<div id="modal_credentials" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-file-text"></i> Credentials</h4>
			  </div>
			  <div class="modal-body">
					<a href="old/credentials.php?action=add"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-plus-square pull-left"></i> Add</button></a>
					<a href="old/credentials.php?action=process"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-file-text pull-left"></i> On Process</button></a>
					<a href="old/credentials.php?action=release"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-outdent pull-left"></i> For Release</button></a>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>	
		<!-- Modal SIS -->
		
		<?php
	}	
	
 	public static function ACCTG(){
		?><a href="#" data-toggle="modal" data-target="#modal_accounting">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-bank "></span></tile>
				<br>Accounting
			</div> 
		</a>
		<!-- Modal Accounting -->
		<div id="modal_accounting" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-bank"></i> Accounting</h4>
			  </div>
			  <div class="modal-body"><a href="old/accounting.php?action=view"><button  class="btn btn-primary btn-lg btn-block "><i class="fa fa-search pull-left"></i> Transact</button></a>
					
					<a href="old/promi_list.php?action=reports"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-file-text-o pull-left"></i> Promissory List <br><b style="font-size:12px;">(<?php
									$od_promi = ACCT::getExpiredPromiPerSem();
											$c = count($od_promi);
											echo $c;
											?> Needs action)</b></button></a>
						
					<a href="old/particulars.php?action=view"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-gears pull-left" ></i> Particulars Management</button></a>
					
					<a href="old/accounting_expense.php?action=view"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-shopping-cart pull-left"></i> Expense Management</button></a>
					
					<a href="old/accounting_deposits.php?action=view"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-money pull-left"></i> Deposit Management</button></a>
					
					<a href="old/acct_transaction_logs.php?action=view"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-file-text-o pull-left"></i> Transaction Logs</button></a>
					<a href="old/collection_daily.php"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-file-text-o pull-left"></i> Daily Collection Report</button></a>
					
					<a href="old/acct_transaction_logs.php?action=reports">
						<button  class="btn btn-primary btn-lg btn-block">
							<i class="fa fa-file-text-o pull-left"></i> Transaction Reports
						</button>
					</a>
					<a href="old/sales_report.php">
						<button  class="btn btn-primary btn-lg btn-block">
							<i class="fa fa-file-text-o pull-left"></i> Sales Report
						</button>
					</a>
					
					
			  </div>

			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		
		
		<?php
	}
 	public static function SUBJ_MAN(){
		?><a href="old/subjects.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Subject Management
			</div> 
		</a><?php
	}
 	public static function ROOM_MAN(){
		?><a href="old/room.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Room Management
			</div> 
		</a><?php
	}
 	public static function ASSESSMENTLIST(){
		?><a href="old/report_assesment_list.php?action=choose">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-list"></span></tile>
				<br>Assessment List
			</div> 
		</a><?php
	}
 	public static function SEM_MAN(){
		?><a href="old/semester.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Semester Management
			</div> 
		</a><?php
	}
 	public static function SEC_MAN(){
		?><a href="old/semester_section.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Section Management
			</div> 
		</a><?php
	}
 	public static function INS_MAN(){
		?><a href="old/instructors.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Instructor Management
			</div> 
		</a><?php
	}
 	public static function COURSE_MAN(){
		?><a href="old/course.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Course Management
			</div> 
		</a><?php
	}
 	public static function CURR_MAN(){
		?><a href="#" data-toggle="modal" data-target="#modal_curriculum">
			<div class="tile col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-gear "></span></tile>
				<br>Curriculum Management
			</div>
		</a>
		<!-- Modal Curriculum Management-->
		<div id="modal_curriculum" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-gear"></i> Curriculum Management</h4>
			  </div>
			  <div class="modal-body">
					<a href="old/curricullum.php?action=view"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-list pull-left"></i> Curriculum List</button></a>
					
					<a href="old/curricullum_subjects.php?action=view"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-book pull-left"></i> Curriculum Subjects</button></a>
					
					<a href="old/curricullum_enrolled.php?action=view"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-book pull-left"></i> Enrolled Curriculum</button></a>
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>	
		
		<?php
	}
 	public static function GRADE_MAN(){
		?><a href="#" data-toggle="modal" data-target="#modal_grade">
			<div class="tile modal_grade  col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-tasks "></span></tile>
				<br>Grade Management
			</div>
        </a>
		<!-- Modal Grade Management -->
		<div id="modal_grade" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> Grade Management</h4>
			  </div>
			  <div class="modal-body">
					<a href="old/grade_encoding_admin.php?action=search"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-sort-numeric-asc pull-left"></i> Grade Encoding</button></a>
					
					<a href="old/grade_inquiry.php?action=search"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-print pull-left"></i> Print Grades</button></a>
					
					<a href="old/grade_checking.php?action=view&sem_ID="><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-check pull-left"></i> Grade Verification</button></a>
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>	
		<!-- Modal Grade Management -->
		<?php
	}
 	public static function ENROLMENT(){
		?><a href="#" data-toggle="modal" data-target="#modal_enrolment">
			<div class="tile modal_grade  col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-archive "></span></tile>
				<br>Enrolment
			</div>
        </a>
		<!-- Modal Enrolment Management -->
		<div id="modal_enrolment" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-archive"></i> Enrolment Management</h4>
			  </div>
			  <div class="modal-body">
					<a href="old/sectioning.php?action=search"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-outdent pull-left"></i> Sectioning</button></a>
					
					<a href="old/enrolment_list.php?action=choose"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-list pull-left"></i> Enrolment List</button></a>
					
					<a href="old/report_semester_per_subject.php?action=choose"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-print pull-left"></i> Print Per Subject Reports</button></a>
				
					<a href="old/fin_enrolment2.php?action=search"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-print pull-left"></i> Print Enrolment Card <br>2015+ (New)</button></a>
				
					<a href="old/fin_enrolment.php?action=search"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-print pull-left"></i> Print Enrolment Card <br>14-15 2nd Tri (Old)</button></a>
					
					<a href="old/print_deficiencies.php?action=search"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-print pull-left"></i> Print Deficiencies Reports</button></a>
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>	
		
		<?php
	}
 	public static function CHANGE_PASS(){
		$force_login = false;
		#print_r($user);
		/*
		if($user['account_CHANGEPASS'] == 1){
			$force_login = true;
		}else{
			$force_login = false;
		}
		*/
		
		if($force_login){
			?>
			<script type="text/javascript">
				$(window).load(function(){
					$('#modal_password').modal({backdrop: 'static', keyboard: false});
				});
			</script>
			<?php
		}
		
		
		?><a href="#" data-toggle="modal" data-target="#modal_password" <?php if($force_login){ echo 'data-backdrop="static" data-keyboard="false"'; } ?>  >
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-unlock-alt "></span></tile>
				<br>Update Password
			</div>
		</a>
		<!-- Modal Change Password -->
		<div id="modal_password" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<?php if(!$force_login){ ?><button type="button" class="close" data-dismiss="modal">&times;</button><?php } ?>
				<h4 class="modal-title"><i class="fa fa-key"></i> Change Password</h4>
			  </div>
				<div class="modal-body">
					<form action="javascript:void(0);">
						 <div id="error"></div>
						 
						 <?php if($force_login) { SESSION::DisplayCustomMsg('warning','Required to change password!'); } ?> 
						 
						 <div class="form-group">
							<b>Enter Your Current Password</b>
							<input type="password" id="cpassword" name="cpassword" class="form-control" autofocus="autofocus" required="required">
						 </div>
						 <div class="form-group">
							<b>Enter Your New Password</b>
							<input type="password" id="npassword" name="npassword" class="form-control" autofocus="autofocus" required="required">
						 </div>
						 <div class="form-group">
							<b>Retype Your New Password</b>
							<input type="password" id="vpassword" name="vpassword" class="form-control" autofocus="autofocus" required="required">
						 </div>
						 <div class="form-group">
							<button  class="btn btn-primary btn-lg btn-block" id="pass_button_save"><i class="fa fa-file-text-o pull-left"></i> Save</button>
							<script>
														$(document).ready(function() {
														$('#pass_button_save').click(function(){
														var cpassword = $('#cpassword').val();
														var npassword = $('#npassword').val();
														var vpassword = $('#vpassword').val();
															if( (cpassword.length > 0) && (npassword.length > 0) && (vpassword.length > 0))
															{
																$.ajax({
																	type: "POST",
																	url: "<?= HOME ?>old/search.php?action=cp",
																	data: 'cpassword=' + cpassword +'&npassword='+ npassword +'&vpassword=' +vpassword,
																	beforeSend : function() {
																			$('#modal_loading').modal('show');
																    }, 
																	success: function(msg){
																		//alert(msg);
																			
																			$('#modal_loading').modal('hide');
																			
																			switch(msg.replace(/\s/g,''))
																			{
																				
																				case 'E_NR': 
																				$("#error").html("<div class=\"alert alert-dismissable alert-danger\"><h4> Error! </h4>New Password and Re-type Password didn't matched	</div>");
																				
																				break;
																				case 'I_OP':
																				
																				$("#error").html("<div class=\"alert alert-dismissable alert-danger\"><h4> Error! </h4>Incorrect Old Password</div>");
																				
																				break;
																				
																				case 'S':
																				$("#error").html("<div class=\"alert alert-dismissable alert-success\"><h4> Success! </h4>Password successfully changed!</div>");
																				
																				
																				setTimeout(function(){
																					   $('#modal_password').modal('hide');
																				   }, 1500);
																				
																				
																				break;
																				
																			}
																				
																			$('#cpassword').val("");
																			$('#npassword').val("");
																			$('#vpassword').val("");
																			$('#cpassword').focus();
																			
																	}
																}); // Ajax Call
															}
														})
														});			
														</script>
						 </div>
					</form>	 
				</div>
				

			</div>

		  </div>
		</div>


		<!-- Modal  Loading -->
		<div id="modal_loading" class="modal" role="dialog" data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body">
					 <center><img src="<?= IMG; ?>/ring.gif"></center>
				</div>
			</div>
			</div>
		</div>	
		
		
		<?php
	}
 	public static function LOGOUT(){
		?><a href="<?= HOME?>old/logout.php">
			<div class="tile tile6 col-xs-6 col-sm-4" 
			onclick="window.location.assign('index.php');">
				<tile><span class="tile-icon fa fa-sign-out "></span></tile>
				<br>Logout
			</div>
		</a><?php
	}
	
	
	/* ACCTG TILES */
	public static function ACCTG_TRANSACT(){
		?><a href="old/accounting.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-search "></span></tile>
				<br>Transact
			</div>
		</a><?php
	}
	
	public static function ACCTG_PROMISSORY(){
		?><a href="old/promi_list.php?action=reports">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-file-text-o "></span></tile>
				<br>Promissory List
			</div>
		</a><?php
	}
	public static function ACCTG_PARTICULARS(){
		?><a href="old/particulars.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-gear"></span></tile>
				<br>Particulars Management
			</div>
		</a><?php
	}
	public static function ACCTG_EXPENSE(){
		?><a href="old/accounting_expense.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-shopping-cart"></span></tile>
				<br>Expense Management
			</div>
		</a><?php
	}
	public static function ACCTG_DEPOSIT(){
		?><a href="old/accounting_deposits.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-money"></span></tile>
				<br>Deposit Management
			</div>
		</a><?php
	}
	public static function ACCTG_TRANSACTION_LOGS(){
		?><a href="old/acct_transaction_logs.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-file-text-o"></span></tile>
				<br>Transaction Logs
			</div>
		</a><?php
	}
	public static function ACCTG_TRANSACTION_REPORTS(){
		?><a href="old/acct_transaction_logs.php?action=reports">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-file-text-o"></span></tile>
				<br>Transaction Reports
			</div>
		</a><?php
	}
	
	public static function ENROLMENT_REPORT(){
		?><a href="#" data-toggle="modal" data-target="#enrolment_report"> 
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-group"></span></tile>
				<br>Enrolment Report
			</div> 
		</a>
		
		<div id="enrolment_report" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> Enrolment Report</h4>
			  </div>
			  
				<form  action="<?= HOME ?>modules/enrolment_report.php?action=view" method="GET"> 
					<input type="hidden" name="action" value="view">
				  <div class="modal-body">
				  <?php
				  $sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
				  #print_r($sem);
				  ?>
				  
					<div>
						Semester:
						<select name="sem_ID" class="form-control">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
				  </div>
				</form>
			  
			</div>
		  </div>
		</div>	
		<!-- Modal Enrolment Report Management -->
		<?php
	}
	
	
	public static function ENROLMENT_COMPARATIVE_REPORT(){
		?><a href="#" data-toggle="modal" data-target="#ENROLMENT_COMPARATIVE_REPORT"> 
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-exchange"></span></tile>
				<br>Comparative Report
			</div> 
		</a>
		
		<div id="ENROLMENT_COMPARATIVE_REPORT" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-exchange"></i> Comparative Report</h4>
			  </div>
			  
				<form  action="<?= HOME ?>modules/enrolment_comparative.php?action=view" method="GET"> 
					<input type="hidden" name="action" value="view">
				  <div class="modal-body">
				  <?php
				  $sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
				  #print_r($sem);
				  ?>
				  
					<div>
						From Semester:
						<select name="sem_ID_from" class="form-control">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
					<br>
					<div>
						To Semester:
						<select name="sem_ID_to" class="form-control">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
				  </div>
				</form>
			  
			</div>
		  </div>
		</div>	
		<!-- Modal Enrolment Report Management -->
		<?php
	}
	
	public static function ACCTG_REPORT(){
		?><a href="#" data-toggle="modal" data-target="#modal_accounting_report"> 
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-file-o"></span></tile>
				<br>Accounting Report
			</div> 
		</a>
		
		
		<!-- Modal Accounting -->
		<div id="modal_accounting_report" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			<form  action="<?= HOME ?>modules/accounting_report.php" method="GET"> 
			 <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-bank"></i> Accounting Report</h4>
			  </div>
			  
					<div class="modal-body">
					
					<!--
					<a href="<?= HOME ?>modules/accounting_report.php?action=collection"><button  class="btn btn-primary btn-lg btn-block "><i class="fa fa-search pull-left"></i> Collection Report</button></a>
					
					<a href="<?= HOME ?>modules/accounting_report.php?action=expense"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-shopping-cart pull-left"></i> Expense Report</button></a>
					
					<a href="<?= HOME ?>modules/accounting_report.php?action=deposit"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-money pull-left"></i> Deposit Report</button></a>
					-->
					<div>
						Report:
						<select id="report" name="action" class="form-control">
						<option>Please Select</option>
						<option value="collection">Collection Report</option>
						<option value="expense">Expense Report</option>
						<option value="deposit">Deposit Report</option>
						</select>
					</div>	
					<br>
					<div id="semester" style="display:none">
						Semester:
						<select id="sem_ID" name="sem_ID" class="form-control">
						<?php
						$sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>	
			  </div>
			   <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
			   </div>
			</form> 
			</div>

		  </div>
		</div>
		<script>
		 $("#report").change(function () {
			var select = this.value;
			switch(select)
			{
				case 'collection': 
					$("#semester").show(); 
					$("#sem_ID").prop( "disabled", false );
					break;
				case 'expense': $("#semester").hide(); 
					$("#sem_ID").prop( "disabled", true );
					break;
				case 'deposit': $("#semester").hide(); 
					$("#sem_ID").prop( "disabled", true );
					break;
				default:  $("#semester").hide(); 
						$("#sem_ID").prop( "disabled", true );
				break;
			}
		});
		</script>
		<?php
	}

	public static function MY_SUBJECTS(){
		?>
		<a href="#" data-toggle="modal" data-target="#subject_load">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-group"></span></tile>
				<br>My Subject Load
			</div> 
		</a>
		
		<!-- Modal Grade Management -->
		<div id="subject_load" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> My Subject Load</h4>
			  </div>
			  <div class="modal-body">
					<a href="#" data-toggle="modal" data-dismiss="modal" data-target="#subject_load_sem"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-sort-numeric-asc pull-left"></i> Encode Grades</button></a>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>	
		<!-- Modal Grade Management -->
		<?php
	}	
	
	public static function OLD_PORTAL(){
		?><a href="old/index.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-home"></span></tile>
				<br>Please Access Old Portal
			</div> 
		</a>	<?php
	}	
	
	
	public static function ENCODE_GRADES(){
		?>
		<a href="#" data-toggle="modal" data-target="#subject_load">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-group"></span></tile>
				<br>Encode Grades
			</div> 
		</a>
		
		<!-- Modal Grade Management -->
		<div id="subject_load" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> Encode Grades</h4>
			  </div>
			  
				<form action="<?= HOME ?>modules/faculty_grade_encoding.php" method="GET"> 
					<input type="hidden" name="action" value="view">
				  <div class="modal-body">
				  <?php
				  $sem = SEM::getallsemsencoding(array("sem_ID"=>"ASC"));
				  #print_r($sem);
				  ?>
				  
					<div>
						Semester:
						
						<select name="sem_ID">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
				  </div>
				</form>
			  
			</div>
		  </div>
		</div>	
		<!-- Modal Grade Management -->
		<?php
	}
	
	public static function STUDENT_LIST(){
		?>
		<a href="#" data-toggle="modal" data-target="#student_list">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-group"></span></tile>
				<br>Student List
			</div> 
		</a>
		
		<!-- Modal Grade Management -->
		<div id="student_list" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> Student List</h4>
			  </div>
			  
				<form action="<?= HOME ?>modules/faculty_view_students.php" method="GET"> 
					<input type="hidden" name="action" value="view">
				  <div class="modal-body">
				  <?php
				  $sem = SEM::getallsemsloading(array("sem_ID"=>"DESC"));
				  #print_r($sem);
				  ?>
				  
					<div>
						Semester:
						
						<select name="sem_ID">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
				  </div>
				</form>
			  
			</div>
		  </div>
		</div>	
		<!-- Modal Grade Management -->
		<?php
	}	
	
	
	
	public static function ACCTG_AUDIT(){
		?>
		<a href="#" data-toggle="modal" data-target="#student_list">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-group"></span></tile>
				<br>Accounting Audit
			</div> 
		</a>
		
		<!-- Modal Grade Management -->
		<div id="student_list" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> Accounting Audit</h4>
			  </div>
			  
				<form action="<?= HOME ?>modules/accounting_audit.php" method="GET"> 
					<input type="hidden" name="action" value="view">
				  <div class="modal-body">
				  <?php
				  $sem = SEM::getallsems(array("sem_ID"=>"DESC"));
				  #print_r($sem);
				  ?>
				  
					<div>
						Semester:
						
						<select name="sem_ID">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
				  </div>
				</form>
			  
			</div>
		  </div>
		</div>	
		<!-- Modal Grade Management -->
		<?php
	}	
	
	public static function AUDIT_REPORT(){
		?><a href="<?= HOME ?>modules/audit_report.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-file-text-o"></span></tile>
				<br>Audit Reports
			</div>
		</a>
		<?php
	}
}


class KS{


	public static function submitpayment2($info,$id,$sem_ID,$account_ID)
	{
	
		if(ACCT::checkOR($info['OR'],$info['si_ID'],$sem_ID) == 1)
		{
			$sql = "
			UPDATE
			enrolment
			set or_ID = ?
			 where si_ID = ? and sem_ID = ?
			";
			$param = array(
					$info['OR'],
					$id,
					$sem_ID
					);
			
			SQL::execute($sql, $param);
			///////////////////////////// AUDIT TRAIL START (JEELBERT) ////////////////////////////////////////
			$info1 =  USERS::viewSingleStudent($id);
			$user_id = $_SESSION['USER']['account_ID'];
	        $action_event = "Update";
	        $event_desc = "MODULE: Registrar / Student Sectioning, DESCRIPTION: User Inserted/Added OR NO# of ".$info1['si_LNAME'].", ".$info1['si_FNAME']." ".$info1['si_MNAME'].', OR No# '.$info['OR'];
	        
	        
	        $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);	
			///////////////////////////// AUDIT TRAIL END (JEELBERT) ////////////////////////////////////////
			$total_tuition = $info['totaltf'];
			$or = ACCT::checkORData($info['OR'],$id,$sem_ID);
			$amount = $or['payment_AMOUNT'];
			if ($info['snpl'] > 0) {
				#$bal = ($total_tf*$SNPL)-$down;
				#$total = ($total_tuition*$info['snpl']) - $amount;
				$total = ($total_tuition-($total_tuition*$info['snpl']) - $amount);
				$dd = ($total_tuition*$info['snpl']);
				$d1yr =  date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"));
				ACCT::addp_sem($info['si_ID'], $dd, $d1yr, 1,'SNPL',$sem_ID);
			}else{
				$total = $total_tuition - $amount;
			}
			
			
			$bal = $total;
			$details = $info['details'];
			$p = $amount;
			$d = date("Y-m-d");
			ACCT::addp_sem($info['si_ID'], $p, $d, 1,'DOWNPAYMENT FEE',$sem_ID);
			
				if($info['date1'] == NULL)
				{
					$slice = 1;
				}else if($info['date2'] == NULL){
					$slice = 2;
				}else if($info['date3'] == NULL){
					$slice = 3;
				}else if($info['date4'] == NULL){
					$slice = 4;
				}else if($info['date5'] == NULL){
					$slice = 5;
				}
				
				$slice -= 1;
				#msgbox($slice);
				
				
				if($slice == 1)
				{
				$p1 = $bal;
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE'.$details,$sem_ID);
				
				}else if($slice == 2)
				{
				$p1 = floor($bal/2);
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE'.$details,$sem_ID);
				
				$p2 = $bal-$p1;
					ACCT:: addp_sem($info['si_ID'], $p2, $info['date2'], 1,'2ND INSTALLMENT FEE',$sem_ID);
				}else if($slice == 3)
				{
					$p1 = floor($bal/3);
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE'.$details,$sem_ID);
					
					$p2 = floor(($bal-$p1) / 2);
					ACCT::addp_sem($info['si_ID'], $p2, $info['date2'], 1,'2ND INSTALLMENT FEE',$sem_ID);
					
					$p3 = $bal - ($p1+$p2);
					ACCT::addp_sem($info['si_ID'], $p3, $info['date3'], 1,'3RD INSTALLMENT FEE',$sem_ID);
				}else if($slice == 4)
				{
					$p1 = floor($bal/4);
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE'.$details,$sem_ID);
					$p2 = $p1;
					ACCT::addp_sem($info['si_ID'], $p2, $info['date2'], 1,'2ND INSTALLMENT FEE',$sem_ID);
					$p3 = $p2;
					ACCT::addp_sem($info['si_ID'], $p3, $info['date3'], 1,'3RD INSTALLMENT FEE',$sem_ID);
					$p4 = $bal-($p1+$p2+$p3);
					ACCT::addp_sem($info['si_ID'], $p4, $info['date4'], 1,'4TH INSTALLMENT FEE',$sem_ID);
				}	
			
			
		
		}else{
		
		SESSION::StoreMsg("Invalid OR No!","error");
		
		}
	
	
			
	
	
	}

	public static function voucherGenerate($info,$id,$sem_ID,$account_ID)
	{
		
			$sql = "
			UPDATE
			enrolment
			set or_ID = ?
			 where si_ID = ? and sem_ID = ?
			";
			$param = array(
					0,
					$id,
					$sem_ID
					);
			
			SQL::execute($sql, $param);
				
			$total_tuition = $info['totaltf'];
			#$or = ACCT::checkORData($info['OR'],$id,$sem_ID);
			$amount = 0;
			///////////////////////////// AUDIT TRAIL START (JEELBERT) ////////////////////////////////////////
			$info =  USERS::viewSingleStudent($id);
			$user_id = $account_ID;
	        $action_event = "Update";
	        $event_desc = "MODULE: Registrar / Student Sectioning, DESCRIPTION: User Inserted/Added OR NO# of ".$info['si_LNAME'].", ".$info['si_FNAME']." ".$info['si_MNAME'].', OR No# 0 ';
	        
	        
	        $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);	
			///////////////////////////// AUDIT TRAIL END (JEELBERT) ////////////////////////////////////////
			if ($info['snpl'] > 0) {
				#$bal = ($total_tf*$SNPL)-$down;
				#$total = ($total_tuition*$info['snpl']) - $amount;
				$total = ($total_tuition-($total_tuition*$info['snpl']) - $amount);


				$dd = ($total_tuition*$info['snpl']);
				$d1yr =  date('Y-m-d',strtotime(date("Y-m-d", mktime()) . " + 365 day"));
				ACCT::addp_sem($info['si_ID'], $dd, $d1yr, 1,'SNPL',$sem_ID);
			}else{
				$total = $total_tuition - $amount;
			}
			
			



			$bal = $total;
			
			$p = $amount;
			$d = date("Y-m-d");
			$details = $info['details'];
			#ACCT::addp_sem($info['si_ID'], $p, $d, 1,'DOWNPAYMENT FEE',$sem_ID);
			
				if($info['date1'] == NULL)
				{
					$slice = 1;
				}else if($info['date2'] == NULL){
					$slice = 2;
				}else if($info['date3'] == NULL){
					$slice = 3;
				}else if($info['date4'] == NULL){
					$slice = 4;
				}else if($info['date5'] == NULL){
					$slice = 5;
				}
				
				$slice -= 1;
				#msgbox($slice);
				
				
				if($slice == 1)
				{
				$p1 = $bal;
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE'.$details,$sem_ID);
				
				}else if($slice == 2)
				{
				$p1 = floor($bal/2);
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE'.$details,$sem_ID);
				
				$p2 = $bal-$p1;
					ACCT:: addp_sem($info['si_ID'], $p2, $info['date2'], 1,'2ND INSTALLMENT FEE',$sem_ID);
				}else if($slice == 3)
				{
					$p1 = floor($bal/3);
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE'.$details,$sem_ID);
					
					$p2 = floor(($bal-$p1) / 2);
					ACCT::addp_sem($info['si_ID'], $p2, $info['date2'], 1,'2ND INSTALLMENT FEE',$sem_ID);
					
					$p3 = $bal - ($p1+$p2);
					ACCT::addp_sem($info['si_ID'], $p3, $info['date3'], 1,'3RD INSTALLMENT FEE',$sem_ID);
				}else if($slice == 4)
				{
					$p1 = floor($bal/4);
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE'.$details,$sem_ID);
					$p2 = $p1;
					ACCT::addp_sem($info['si_ID'], $p2, $info['date2'], 1,'2ND INSTALLMENT FEE',$sem_ID);
					$p3 = $p2;
					ACCT::addp_sem($info['si_ID'], $p3, $info['date3'], 1,'3RD INSTALLMENT FEE',$sem_ID);
					$p4 = $bal-($p1+$p2+$p3);
					ACCT::addp_sem($info['si_ID'], $p4, $info['date4'], 1,'4TH INSTALLMENT FEE',$sem_ID);
				}	
			
	
	}	


	public static function deletesubjectfromsem($sem_ID,$si_ID,$subject_sched_ID){
 		$sql = "
			 DELETE a
			  FROM subject_enrolled a
			  JOIN subject_sched b ON a.subject_sched_ID = b.subject_sched_ID
			  JOIN semester_section c ON c.semester_section_ID = b.semester_section_ID
			 WHERE c.sem_ID = ? and a.si_ID = ? and b.subject_sched_ID = ?
				
			";
			$param = array(
					$sem_ID,
					$si_ID,
					$subject_sched_ID
			);
			SQL::execute($sql, $param);
            
			#$id = SQL::last_ID();
			#href("fin_enrolment2.php?action=view&id=$si_ID&sem_ID=$sem_ID");
			
	}	

	public static function removePayable($sem_ID,$si_ID,$particular)
		{
				$datenow = date("Y-m-d H:i:s");
			   
				$sql = "
				DELETE
				FROM  acc_payables
				WHERE  
					si_ID = ? and
					sem_ID = ? and 
					particular_ID = ?
					
					
				";
				$param = array(
					$si_ID,
					$sem_ID,
					$particular
				);
				SQL::execute($sql, $param);
				#SESSION::StoreMsg("Payables Successfully Added","success");
				#$url = $_SERVER['PHP_SELF'];
				#$exploded = explode("/",$url);
			   # HTML::redirect($exploded[2]."?action=view");
				# HTML::redirect("accounting.php?action=view&id=$id");
		}


 	public static function updatenrol($info,$id){
	
	
	
		$po_info = SUBJSCHED::getPaymentOptionByID($info['payment_option']);
		
		#msgbox(var_dump($po_info));
		
		$po_lab_rate = $po_info['po_LAB'];
		$po_lec_rate = $po_info['po_LEC'];	
		$po_misc = $po_info['po_MISC'];	
		
		
		$sem = $info['sem_ID'];
		#var_dump($info);
		#echo $info['si_ID'];
		
		$mysubjects = SUBJENROL::mysubjects($info['si_ID'],$sem);
		$existing = array();
		foreach($mysubjects as $key1 => $val1) { 
			$existing[] = $val1['subject_sched_ID'];
		}
		//var_dump($existing);
		//echo "<br>";
		//var_dump($info['id']);
		echo "<br>";
		//
		#For Deletion
		$old = array_diff($existing,$info['id']);
		//print_r($old);
		
		//echo "<br>";
		#For Addition
		$new = array_diff($info['id'],$existing);
		//print_r($new);
		
		#SUBJENROL::deletesubjectsfromsem($sem,$id);
		#ACCT::remp_sem($sem,$id);
		KS::removePayable($sem,$info['si_ID'],1);
		//for($i=0;$i<sizeof($old);$i++)
		foreach($old as $key3 => $val3) 
		{
			KS::deletesubjectfromsem($sem,$info['si_ID'],$val3);
		}

		$po =  $info['payment_option'];
		$length = sizeof($info['id']);
		$total_lec = 0;
		$total_lab = 0;
		$acc_ID = $info['acc_ID'];
		
		#for($i=0;$i<$length;$i++)
		#for($i=0;$i<sizeof($new);$i++)
		foreach($new as $key2 => $val2) 
		{
			$subject_sched_ID =$val2;
			$si_ID = $id;
			
				
				
				
				SUBJENROL::enrol($subject_sched_ID,$si_ID);
				
				
				/*$total_lec += $subject_info['LEC_UNIT'];
				$total_lab += $subject_info['LAB_UNIT'];*/
			
		}



		for($i=0;$i<sizeof($info['id']);$i++)
		{
			$subject_sched_ID = $info['id'][$i];
			$subject_sched = SUBJSCHED::getID($subject_sched_ID);
			$subject_info = SUBJECTS::getID($subject_sched['subject_ID']);	
			$total_lec += $subject_info['LEC_UNIT'];
			$total_lab += $subject_info['LAB_UNIT'];	
			#$existing[] = $val1['subject_sched_ID'];
		}
		#exit();

	 $assess = explode(",",$info['assess_ID']);
	 $y = count($assess);
	 $afee = 0;
	 for($x=0;$x<$y;$x++)
	 {
	 	$af = SUBJSCHED::getFAbyID($assess[$x]);
		$afee += $af['af_PRICE'];
	 }	
	   "AF:".$afee."<BR>";
		
	  $grad_fee = 0;		
	  if($info['gf_ID'] != '')
	  {
	  	$gf = SUBJSCHED::getGFbyID($info['gf_ID']);
		$grad_fee = $gf['gf_AMOUNT'];
	  }	
		
	 "GF: ".$grad_fee."<BR>";;	
		
	  $thesis_fee = 0;		
	  if($info['tf_ID'] != '')
	  {
	  	$gf = SUBJSCHED::getTFbyID($info['tf_ID']);
		$thesis_fee = $gf['tf_AMOUNT'];
		KS::removePayable($sem,$info['si_ID'],39);
	  }		
	   "TF: ".$thesis_fee."<BR>";;
		
		
	    $lab_fee = $total_lab*$po_lab_rate;
	  echo "<br>";
	  	$lec_fee = $total_lec*$po_lec_rate;
	  echo "<br>";
	   $po_misc;
	  echo "<br>";
	   $install_fee = $po_info['po_installment_fee'];
	  echo "<br>";
	   $discount_tf = ($lab_fee+$lec_fee)/$po_info['po_discount_tf'];
	  echo "<br>";
	   $discount_mf = ($po_misc)/$po_info['po_discount_misc'];
	  echo "<br>";
	   $additional_tf = ($lab_fee+lec_fee)/$po_info['po_additional_tf'];
	   echo "<br>";
	   $additional_mf = ($po_misc)/$po_info['po_additional_misc'];
	  $total_tf = $thesis_fee + $grad_fee + $afee + $po_misc + $lab_fee + $lec_fee - ($discount_tf + $discount_mf) + ($additional_tf + $additional_mf) + $install_fee;
	//  echo "TOTAL TF: " .$total_tf;
		$si_ID = clean($_GET['id']);
			  
			  
		//echo $misc."<br>";
		$subject_sched_ID = implode(',',$info['id']);   
		echo $subject_sched_ID;
		//echo "<br>".$sem; 	   
			   
			   
			   
	            //SESSION::StoreMsg("!","success");

			$user_info_id = $info['acc_ID'];
			$action_event = "UPDATED";
			$event_desc =  "MODULE: Payment Option DESCRIPTION: User added Payment Option <br>DETAILS: <br>PROGRAM NAME: ".$info['course_NAME']."<br>PROGRAM INITIAL: ".$info['course_INIT'];
			$test = array("BEFORE"=>$old,
           					"AFTER"=>$new);
  	       	$result2 =preg_replace('/[{-}"]/', '', json_encode($test));

			if(($new == null)||($new == ""))
  	       {

  	       	$event_desc = "MODULE: Payment Option <br>DESCRIPTION: NO CHANGES";
  	       
  	       }else
  	       {

  	       	$event_desc = "MODULE: Payment Option <br>DESCRIPTION: DETAILS: ".$result2;

  	       }
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);					
			SUBJENROL::updateEnrolment($info['si_ID'],$sem,$subject_sched_ID,$info['payment_option'],$info['assess_ID'],$info['gf_ID'],$info['tf_ID'],$info['acc_ID']);


				
				
	}
}
?>