<?php
$time_start = microtime(true); 
#error_reporting("E_ALL");
//START SESSION
#if(session_status() == PHP_SESSION_NONE)
	session_start();
	ob_start();


//SET TIMEZONE;
date_default_timezone_set('Asia/Manila');

//CONSTANTS
defined('URL') ? null : define('URL','/');
defined('HOME') ? null : define('HOME', 'http://' . $_SERVER['HTTP_HOST'] . ''.URL);
defined('ROOT') ? null : define('ROOT', $_SERVER['DOCUMENT_ROOT'] . '/'.URL);
defined('LAYOUTS') ? null : define('LAYOUTS',  ROOT . 'old/pages/layouts/');
defined('PAGES') ? null : define('PAGES',  ROOT . 'old/pages/frontpage/');
defined('LAYOUTS_N') ? null : define('LAYOUTS_N',  ROOT . 'pages/layouts/');
defined('IMG') ? null : define('IMG',  HOME . 'assets/images/');
defined('INCLUDES') ? null : define('INCLUDES',  ROOT . 'old/includes/');
defined('IMAGES') ? null : define('IMAGES',  HOME . '/old/assets/img/');
defined('MIN')?null:define('MIN',HOME.'min/?f=/');
defined('CONTROLLERS')?null:define('CONTROLLERS','old/controllers/');

!defined("DB_HOST") ? define("DB_HOST", "localhost") : null;
!defined("DB_USER") ? define("DB_USER", "itekniks_ams") : null;
!defined("DB_PASS") ? define("DB_PASS", "JhT185H*rT?S") : null;
!defined("DB_NAME") ? define("DB_NAME", "itekniks_ams") : null;
ini_set('display_errors', 'Off');
//INCLUDES
include(INCLUDES . 'functions.php');

//CONTROLLERS
include(ROOT .  CONTROLLERS. 'mainFunction.php');

//HTML
include(ROOT .  CONTROLLERS. 'helpers/helper.php');
include(ROOT .  CONTROLLERS. 'navigationController.php');
