<?php
$site_options['site_NAME'] = 'CSJPIIAS - School Management System v2.0';

if(SESSION::isLoggedIn()){
	$user = SESSION::getUser();
}

    function relativedate($secs) {
    $second = 1;
    $minute = 60;
    $hour = 60*60;
    $day = 60*60*24;
    $week = 60*60*24*7;
    $month = 60*60*24*7*30;
    $year = 60*60*24*7*30*365;
     
    if ($secs <= 0) { $output = "now";
    }elseif ($secs > $second && $secs < $minute) { $output = round($secs/$second)." second";
    }elseif ($secs >= $minute && $secs < $hour) { $output = round($secs/$minute)." minute";
    }elseif ($secs >= $hour && $secs < $day) { $output = round($secs/$hour)." hour";
    }elseif ($secs >= $day && $secs < $week) { $output = round($secs/$day)." day";
    }elseif ($secs >= $week && $secs < $month) { $output = round($secs/$week)." week";
    }elseif ($secs >= $month && $secs < $year) { $output = round($secs/$month)." month";
    }elseif ($secs >= $year && $secs < $year*10) { $output = round($secs/$year)." year";
    }else{ $output = " more than a decade ago"; }
     
    if ($output <> "now"){
    $output = (substr($output,0,2)<>"1 ") ? $output."s" : $output;
    }
    return $output;
    }


class AL {

	public static function is_admin() {
		return SESSION::getUser()['access_ID'] == ADMINISTRATOR;
	}

	public static function is_cb() {
		return SESSION::getUser()['access_ID'] == COURSE_BUILDER;
	}

	public static function is_user() {
		return SESSION::getUser()['access_ID'] == USER;
	}

}

?>
<?php

class ACCESS {
	
	public static function getAll() {
		$sql = "
		SELECT * 
		FROM account_level
		ORDER BY access_NAME ASC
		";
		return SQL::find_all($sql);
	}
	
	
	public static function getByID($id) {
		$sql = "
		SELECT * 
		FROM account_level
		WHERE access_ID = ?
		";
		$param = array($id);
		return SQL::find_all($sql, $param);
	}

	public static function getID($id) {
		$sql = "
		SELECT * 
		FROM account_level
		WHERE access_ID = ?
		";
		$param = array($id);
		return SQL::find_id($sql, $param);
	}

}

?>
<?php
class ACCT{

// PROMI START
		public static function penalize_promi($promi_ID,$id)
		{
			$promi = ACCT::getSinglePromi($promi_ID);
			$datenow = date("Y-m-d");
			$id_2 = ACCT::addpay_sem2($promi['si_ID'],($promi['promi_INTEREST']*$promi['promi_AMOUNT']),$datenow,$id,'PROMISSORY PENALTY',$promi['sem_ID'],$promi['particular_ID']);
			
				$sql = 
				"Update acc_promi
				SET
					promi_PENALTY = ?,
					promi_penalty_PAYABLE_ID = ?
				WHERE
					promi_ID = ?	
				";
				$param = array(
					1,
					$id_2,
					$promi_ID
				);
				SQL::execute($sql, $param);
				
				
				#AUDIT TRAIL#
				#$last_id = SQL::last_ID();
				$promi = self::getSinglePromi($promi_ID);
				$user = $_SESSION['USER']['account_ID'];
				$u = USERS::viewSingleStudent($promi['si_ID']);
				$sem = SEM::getSingleSem($promi['sem_ID']);
				$particular = PARTICULARS::getSingleParticular($promi['particular_ID']);
				$action = "PENALIZED PROMISSORY ($particular[particular_NAME]) amounting $promi[promi_AMOUNT] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME] Reference: #$promi_ID";
				$module = "Accounting";
				AUDIT::ins($user, $module, $action);
				#AUDIT TRAIL#
//===========================================================================================================
			//START AUDIT TRAIL ELIN
			$user_info_id = $_SESSION['USER']['account_ID'];
			$action_event = "UPDATED";
			$event_desc =  "MODULE: PROMISSORY LIST DESCRIPTION: User PENALIZED PROMISSORY <br>DETAILS: <br>STUDENT ID: ".$u['student_ID']."<br>STUDENT NAME: ".$u['si_LNAME'].", ".$u['si_FNAME']."<br>REFERENCE: #".$promi_ID."<br>PARTICULAR NAME: ".$particular["particular_NAME"]."<br>AMOUNT: ".$promi["promi_AMOUNT"];
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);				
			//END AUDIT TRAIL ELIN
//=============================================================================================================


				
				
				echo "<span style='color:red'>Penalized!</span>";
			}		
		

		#Update Promi
		public static function update_promi($info)
		{
			$promi_ID = $info['promi_ID'];
			$promi = ACCT::getSinglePromi($promi_ID);
		
			
				$datenow =  date('Y-m-d', strtotime($info['date_added']));
				$date_promi = date('Y-m-d', strtotime($info['date']));
		
						 ACCT::updatepay_sem2($promi['payable_ID_1'],-abs($info['amount']),$datenow,$info['sem_ID'],$info['particular_ID']);
						 ACCT::updatepay_sem2($promi['payable_ID_2'],abs($info['amount']),$date_promi,$info['sem_ID'],$info['particular_ID']);
					

				$sql = "
				update acc_promi
				SET
					sem_ID = ?,
					particular_ID = ?,
					promi_AMOUNT = ?,
					promi_DATE = ?,
					promi_INTEREST = ?,
					promi_PAID = ?,
					promi_DATEPOSTED = ?
				WHERE
					promi_ID = ?	
				";
				$param = array(
					$info['sem_ID'],
					$info['particular_ID'],
					$info['amount'],
					$date_promi,
					$info['interest'],
					$info['promi_PAID'],
					$datenow,
					$info['promi_ID']
				);
				SQL::execute($sql, $param);
				
			   
			   
			    #AUDIT TRAIL#
				#$last_id = SQL::last_ID();
				$promi = self::getSinglePromi($info['promi_ID']);
				$user = $_SESSION['USER']['account_ID'];
				$u = USERS::viewSingleStudent($promi['si_ID']);
				$sem = SEM::getSingleSem($info['sem_ID']);
				$particular = PARTICULARS::getSingleParticular($info['particular_ID']);
				
				$action = "UPDATED PROMISSORY ($particular[particular_NAME]) amounting $info[amount] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME] PROMISE DATE: $date_promi Semester: $sem[sem_NAME] Reference: #$info[promi_ID]";
				$module = "Accounting";
				AUDIT::ins($user, $module, $action);
				#AUDIT TRAIL#
//==============================================================================================================
				//AUDIT TRAIL START ELIN
				$PROMISTATUS = ($info['promi_PAID'] = 0 ? "UNPAID" : "PAID");
			$after = array(
					"SEM_ID" => $info['sem_ID'],
					"PARTICULAR_ID" => $info['particular_ID'],
					"AMOUNT" => $info['amount'],
					"DATE_PROMI" => $date_promi,
					"INTEREST" => $info['interest'],
					"PROMISTATUS" => $info['promi_PAID'],
					"STATUS" => $PROMISTATUS,
					"DATE_NOW" => $datenow
				);

			$beforedatenow =  date('Y-m-d', strtotime($info['before_promi_DATEPOSTED']));
			$beforedate_promi = date('Y-m-d', strtotime($info['before_promi_DATE']));
			$BEFOREPROMISTATUS = ($info['before_promi_PAID'] = 0 ? "UNPAID" : "PAID");
			$before = array(
					"SEM_ID" => $info['before_sem_ID'],
					"PARTICULAR_ID" => $info['before_particular_ID'],
					"AMOUNT" => $info['before_promi_AMOUNT'],
					"DATE_PROMI" => $beforedate_promi,
					"INTEREST" => $info['before_promi_INTEREST'],
					"PROMISTATUS" => $info['before_promi_PAID'],
					"STATUS" => $BEFOREPROMISTATUS,
					"DATE_NOW" => $beforedatenow
				);

			$arraydiffto = array_diff($after, $before);
            $arraydifffrom= array_diff($before, $after);
           	$user_info_id = $info['Account_ID'];
           	$action_event = "UPDATED";
           	$test = array("<br>BEFORE"=>$arraydifffrom,
           					"<br>AFTER"=>$arraydiffto);
  	       		$result2 =preg_replace('/[{-}"]/', '', json_encode($test));

  	       if(($arraydiffto == null)||($arraydiffto == ""))
  	       {

  	       	$event_desc = "MODULE: Edit Promisorry <br>DESCRIPTION: NO CHANGES";
  	       
  	       }else
  	       {

  	       	$event_desc = "MODULE: Edit Promisorry <br>DESCRIPTION: User updated ".$result2;

  	       }
  	       	Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
  	       	//AUDIT TRAIL END ELIN
//==============================================================================================================

			   
			
			   
			   
			   SESSION::StoreMsg("Promisorry Successfully Updated","success");
			   
			   href("accounting.php?action=edit_promi&promi_ID=$promi_ID&id=$u[si_ID]");
		}


		#Adding Promi
		public static function add_promi($info)
		{
				$datenow =  date('Y-m-d', strtotime($info['date_added']));
				$date_promi = date('Y-m-d', strtotime($info['date']));
		
				$id_1 = ACCT::addpay_sem2($info['si_ID'],-abs($info['amount']),$datenow,$info['user_ID'],'PROMI',$info['sem_ID'],$info['particular_ID']);
				$id_2 = ACCT::addpay_sem2($info['si_ID'],$info['amount'],$date_promi,$info['user_ID'],'PROMI',$info['sem_ID'],$info['particular_ID']);
				

				$sql = "
				INSERT INTO	acc_promi
				SET
					si_ID = ?,
					sem_ID = ?,
					particular_ID = ?,
					promi_AMOUNT = ?,
					promi_DATE = ?,
					payable_ID_1 = ?,
					payable_ID_2 = ?,
					promi_INTEREST = ?,
					promi_DATEPOSTED = ?,
					account_ID = ?
				";
				$param = array(
					$info['si_ID'],
					$info['sem_ID'],
					$info['particular_ID'],
					$info['amount'],
					$date_promi,
					$id_1,
					$id_2,
					$info['interest'],
					$datenow,
					$info['user_ID']
				);
				SQL::execute($sql, $param);
				
				
				#AUDIT TRAIL#
				$last_id = SQL::last_ID();
				$user = $_SESSION['USER']['account_ID'];
				$u = USERS::viewSingleStudent($info['si_ID']);
				$sem = SEM::getSingleSem($info['sem_ID']);
				$particular = PARTICULARS::getSingleParticular($info['particular_ID']);
				
				$action = "ADDED PROMISSORY ($particular[particular_NAME]) amounting $info[amount] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME] Semester: $sem[sem_NAME] PROMISED DATE: $date_promi Reference:#$last_id (P: $id_1, $id_2)";
				$module = "Accounting";
				AUDIT::ins($user, $module, $action);
				#AUDIT TRAIL#

//=======================================================================================================================
					//START AUDIT ELIN 
			$user_info_id = $user ;
			$action_event = "ADD";
			$event_desc =  "MODULE: Add Promisorry DESCRIPTION: User added Promisorry <br>DETAILS: <br>STUDENT ID: ".$u['student_ID']."<br>STUDENT NAME:".$u['si_LNAME'].", ".$u['si_FNAME']."<br>PARTICULAR NAME: ".$particular['particular_NAME']."<br>AMOUNT: ".$info['amount']."<br>SEMESTER: ".$sem['sem_NAME']."<br>PROMISED DATE: ". $date_promi."<br>Reference: #".$last_id."<br>P: ".$id_1.", ".$id_2;
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
					//END AUDIT ELIN 
//=======================================================================================================================				
				
			   $id = $info['si_ID'];
			  href("accounting.php?action=view&id=$id");
		}


	public static function getExpiredPromiPerSem()
	{
	$date = date("Y-m-d 00:00:00");
		//SELECT DISTINCT(payables_DATE)
		//SELECT payables_DATE
		$sql = "
		SELECT *
		FROM acc_promi
		WHERE promi_STATUS = 1 
		and promi_PAID = 0 and promi_DATE < '$date' and promi_PENALTY = 0
		";
		return SQL::find_all($sql);
	}

	public static function getActivePromiPerSem($id,$sem_ID)
	{
		//SELECT DISTINCT(payables_DATE)
		//SELECT payables_DATE
		$sql = "
		SELECT *
		FROM acc_promi
		WHERE si_ID = '$id' and promi_STATUS = 1 and sem_ID = '$sem_ID'
		ORDER BY promi_DATE ASC, promi_PAID DESC 
		";
		return SQL::find_all($sql);
	}
	
	public static function getPromiPerSem($sem_ID)
	{
		//SELECT DISTINCT(payables_DATE)
		//SELECT payables_DATE
		$sql = "
		SELECT *
		FROM acc_promi
		WHERE promi_STATUS = 1 and sem_ID = '$sem_ID'
		ORDER BY promi_DATE DESC 
		";
		return SQL::find_all($sql);
	}
	
	public static function getPromiPerSemPerDate($sem_ID,$start,$end,$status)
	{
			if($end == NULL)
			{
				$end = $start;
			}
	
		//SELECT DISTINCT(payables_DATE)
		//SELECT payables_DATE
			if($start == "")
			{
				if($status != NULL)
				{
					if($status != 'ALL')
					{
					$add = "and promi_PAID = '$status'";
					}
				}
				
				
				$sql = "
				SELECT *
				FROM acc_promi
				WHERE promi_STATUS = 1 and sem_ID = '$sem_ID' $add 
				ORDER BY promi_DATE DESC 
				";
			}else{
				if($status != NULL)
				{
					if($status != 'ALL')
					{
					$add = "and promi_PAID = '$status'";
					}
				}
				
				$start = date("Y-m-d",strtotime($start));
				$end = date("Y-m-d",strtotime($end));
				$sql = "
				SELECT *
				FROM acc_promi
				WHERE promi_STATUS = 1 and sem_ID = '$sem_ID' and promi_DATE >= '$start' and promi_DATE <= '$end' $add
				ORDER BY promi_DATE DESC 
				";
			}
		return SQL::find_all($sql);
	}
	
	
	
	public static function getSinglePromi($id)
	{
		//SELECT DISTINCT(payables_DATE)
		//SELECT payables_DATE
		$sql = "
		SELECT *
		FROM acc_promi
		WHERE promi_ID = '$id'
		";
		return SQL::find_id($sql);
	}


		public static function remove_promi($promi_ID,$id)
		{
			 $promi = ACCT::getSinglePromi($promi_ID);
			 			 
				#AUDIT TRAIL#
				#$last_id = SQL::last_ID();
				$user = $_SESSION['USER']['account_ID'];
				$u = USERS::viewSingleStudent($promi['si_ID']);
				$sem = SEM::getSingleSem($promi['sem_ID']);
				$particular = PARTICULARS::getSingleParticular($promi['particular_ID']);
				
				$action = "REVOKED PROMISSORY ($particular[particular_NAME]) amounting $promi[promi_AMOUNT] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME] Semester: $sem[sem_NAME] Reference: #$promi[promi_ID]";
				$module = "Accounting";
				AUDIT::ins($user, $module, $action);
				#AUDIT TRAIL#
			 
			 
			 ACCT::hidePayableID2($promi['payable_ID_1'],'');
			 ACCT::hidePayableID2($promi['payable_ID_2'],'');
				
				$sql = "
				UPDATE acc_promi
				SET
					promi_STATUS = 0
				WHERE
					promi_ID = ?
				";
				$param = array(
					$promi_ID
				);
				SQL::execute($sql, $param);
				
			  
			  
			  
			  
			  
			  href("accounting.php?action=view&id=$id");
		}


// PROMI END


// =============================================================================
	public static function getAllsemOfPayables($id)
	{
		//SELECT DISTINCT(payables_DATE)
		//SELECT payables_DATE
		$sql = "
		SELECT DISTINCT(sem_ID)
		FROM acc_payables
		WHERE si_ID = '$id' and status = 1
		ORDER BY sem_ID DESC
		";
		return SQL::find_all($sql);
	}
	
		public static function getAllsemOfPayment($id)
	{
		//SELECT DISTINCT(payables_DATE)
		//SELECT payables_DATE
		$sql = "
		SELECT DISTINCT(sem_ID)
		FROM acc_payment
		WHERE si_ID = '$id' and status = 1
		ORDER BY sem_ID DESC
		";
		return SQL::find_all($sql);
	}
	
	
	
	
	
	public static function getAllDateOfPayables($id)
	{
		//SELECT DISTINCT(payables_DATE)
		//SELECT payables_DATE
		$sql = "
		SELECT DISTINCT(payables_DATE), particular_ID, payables_AMOUNT, payables_NOTE
		FROM acc_payables
		WHERE si_ID = '$id' and status = 1
		";
		return SQL::find_all($sql);
	}
	
	
	public static function getAllDateOfPayablesPerSem($id,$sem)
	{
		//SELECT DISTINCT(payables_DATE)
		//SELECT payables_DATE
		$sql = "
		SELECT DISTINCT(payables_DATE), particular_ID, payables_AMOUNT, payables_NOTE
		FROM acc_payables
		WHERE si_ID = '$id' and status = 1 and sem_ID = '$sem'
		";
		return SQL::find_all($sql);
	}
	
		public static function getAllDateOfPayablesNull($id)
	{
		//SELECT DISTINCT(payables_DATE)
		//SELECT payables_DATE
		$sql = "
		SELECT DISTINCT(payables_DATE), particular_ID, payables_AMOUNT, payables_NOTE
		FROM acc_payables
		WHERE si_ID = '$id' and status = 1 and sem_ID is NULL
		";
		return SQL::find_all($sql);
	}
	
	
	public static function getAllDateOfPayments($id){
		$sql = "
		SELECT DISTINCT(payment_DATE), particular_ID, payment_AMOUNT, payment_NOTE, payment_OR
		FROM acc_payment
		WHERE si_ID = '$id' and status = 1
		";
		return SQL::find_all($sql);
	}
	
	public static function getAllDateOfPaymentsPerSem($id,$sem){
		$sql = "
		SELECT DISTINCT(payment_DATE), particular_ID, payment_AMOUNT, payment_NOTE, payment_OR
		FROM acc_payment
		WHERE si_ID = '$id' and status = 1 and sem_ID = '$sem'
		";
		return SQL::find_all($sql);
	}
	
		public static function getAllDateOfPaymentsNull($id){
		$sql = "
		SELECT DISTINCT(payment_DATE), particular_ID, payment_AMOUNT, payment_NOTE, payment_OR
		FROM acc_payment
		WHERE si_ID = '$id' and status = 1 and sem_ID is NULL
		";
		return SQL::find_all($sql);
	}	
	
	
	

// =============================================================================

	
	/*------------------- PARTICULAR MODULE---------------------- */
		#Get All Particulars
		public static function getallparticulars(){
			$sql = "
			SELECT *
			FROM acc_particular
			";
			foreach($order as $key => $value){
				if($key ==  0) {
					$sql .= "ORDER BY {$key} {$value}";
				} else {
					$sql .= ", {$key} {$value}";
				}
			}
			return SQL::find_all($sql);
		}
			
		#Get All Particular Name	
		public static function getParticularName($id)
		{
			//SELECT DISTINCT(payables_DATE)
			//SELECT payables_DATE
			$sql = "
			SELECT particular_ID, particular_NAME
			FROM acc_particular
			WHERE particular_ID = '$id'
			";
			return SQL::find_all($sql);
		}
	
	/*------------------- PARTICULAR MODULE---------------------- */	
	
	
	/*------------------- PAYABLE MODULE------------------------- */
	#Remove Payables
		public static function remp_sem($sem_ID,$si_ID)
		{
				$datenow = date("Y-m-d H:i:s");
			   
				$sql = "
				DELETE
				FROM  acc_payables
				WHERE  
					si_ID = ? and
					sem_ID = ?
					and (payables_NOTE = 'DOWNPAYMENT FEE' or payables_NOTE ='1ST INSTALLMENT FEE' or payables_NOTE  )
					
				";
				$param = array(
					$si_ID,
					$sem_ID
				);
				SQL::execute($sql, $param);
				#SESSION::StoreMsg("Payables Successfully Added","success");
				#$url = $_SERVER['PHP_SELF'];
				#$exploded = explode("/",$url);
			   # HTML::redirect($exploded[2]."?action=view");
				# HTML::redirect("accounting.php?action=view&id=$id");
		}	
	
	
		#Insert Payables
		public static function addp($si_ID,$amount,$date,$ui,$note)
		{
				$datenow = date("Y-m-d H:i:s");
			   
				$sql = "
				INSERT INTO	acc_payables
				SET
				  
					si_ID = ?,
					payables_AMOUNT = ?,
					payables_DATE = ?,
					payables_USER_ID = ?,
					payables_DATE_ADDED = ?,
					particular_ID = ?,
					payables_NOTE = ?
				";
				$param = array(
					
					$si_ID,
					$amount,
					$date,
					$ui,
					$datenow,
					1,
					$note
				);
				
				
				SQL::execute($sql, $param);
				
				
				#SESSION::StoreMsg("Payables Successfully Added","success");
				#$url = $_SERVER['PHP_SELF'];
				#$exploded = explode("/",$url);
			   # HTML::redirect($exploded[2]."?action=view");
				# HTML::redirect("accounting.php?action=view&id=$id");
		}		


	public static function addpay_sem2($si_ID,$amount,$date,$ui,$note,$sem_ID,$particular)
		{
				$datenow = date("Y-m-d H:i:s");
			   
				$sql = "
				INSERT INTO	acc_payables
				SET
					si_ID = ?,
					sem_ID = ?,
					payables_AMOUNT = ?,
					payables_DATE = ?,
					payables_USER_ID = ?,
					payables_DATE_ADDED = ?,
					particular_ID = ?,
					payables_NOTE = ?
				";
				$param = array(
					
					$si_ID,
					$sem_ID,
					$amount,
					$date,
					$ui,
					$datenow,
					$particular,
					$note
				);
				SQL::execute($sql, $param);
				return SQL::last_ID();
				
				#SESSION::StoreMsg("Payables Successfully Added","success");
				#$url = $_SERVER['PHP_SELF'];
				#$exploded = explode("/",$url);
			   # HTML::redirect($exploded[2]."?action=view");
				# HTML::redirect("accounting.php?action=view&id=$id");
		}	


		public static function updatepay_sem2($p_ID,$amount,$date,$sem_ID,$particular)
		{
		
		
				$datenow = date("Y-m-d H:i:s");
		
		if($date == "")
		{
			$sql = "
				UPDATE	acc_payables
				SET
					sem_ID = ?,
					payables_AMOUNT = ?,
					
					particular_ID = ?
				WHERE payables_ID = ?
				";
				
				$param = array(
					
					$sem_ID,
					$amount,
					
					$particular,
					$p_ID
				);
		}else{
			   
				$sql = "
				UPDATE	acc_payables
				SET
					sem_ID = ?,
					payables_AMOUNT = ?,
					payables_DATE = ?,
					particular_ID = ?
				WHERE payables_ID = ?
				";
				
				$param = array(
					
					$sem_ID,
					$amount,
					$date,
					$particular,
					$p_ID
				);
		}			
				
				SQL::execute($sql, $param);
				
				#SESSION::StoreMsg("Payables Successfully Added","success");
				#$url = $_SERVER['PHP_SELF'];
				#$exploded = explode("/",$url);
			   # HTML::redirect($exploded[2]."?action=view");
				# HTML::redirect("accounting.php?action=view&id=$id");
		}	


		#Insert Payables
		public static function addp_sem($si_ID,$amount,$date,$ui,$note,$sem_ID)
		{
				$datenow = date("Y-m-d H:i:s");
			   
				$sql = "
				INSERT INTO	acc_payables
				SET
					si_ID = ?,
					sem_ID = ?,
					payables_AMOUNT = ?,
					payables_DATE = ?,
					payables_USER_ID = ?,
					payables_DATE_ADDED = ?,
					particular_ID = ?,
					payables_NOTE = ?
				";
				$param = array(
					
					$si_ID,
					$sem_ID,
					$amount,
					$date,
					$ui,
					$datenow,
					1,
					$note
				);
				SQL::execute($sql, $param);
				return SQL::last_ID();
				
				#SESSION::StoreMsg("Payables Successfully Added","success");
				#$url = $_SERVER['PHP_SELF'];
				#$exploded = explode("/",$url);
			   # HTML::redirect($exploded[2]."?action=view");
				# HTML::redirect("accounting.php?action=view&id=$id");
		}		

	
		#Adding Payable from Single Entry
		public static function addpayables($i,$id,$id1)
		{
				$datenow = date("Y-m-d H:i:s");
				$time = date('Y-m-d H:i:s', strtotime($i['date']));
				$sql = "
				INSERT INTO	acc_payables
				SET
					payables_ID = ?,
					si_ID = ?,
					sem_ID = ?,
					payables_AMOUNT = ?,
					payables_DATE = ?,
					payables_USER_ID = ?,
					payables_DATE_ADDED = ?,
					particular_ID = ?,
					payables_NOTE = ?
				";
				$param = array(
					NULL ,
					$id,
					$i['sem_ID'],
					$i['amount'],
					$time,
					$id1,
					$datenow,
					$i['particulars'],
					$i['note']    
				);

				$i['Account_ID'];
				
				
				
				
				
				SQL::execute($sql, $param);
				SESSION::StoreMsg("Payables Successfully Added","success");
				$url = $_SERVER['PHP_SELF'];
				$exploded = explode("/",$url);
				
				$last_id = SQL::last_ID();
				
				#AUDIT TRAIL#
				$user = $id1;
				$u = USERS::viewSingleStudent($id);
				$particular = PARTICULARS::getSingleParticular($i['particulars']);
				$sem = SEM::getSingleSem($i['sem_ID']);
				// $action = "ADDED PAYABLE ($particular[particular_NAME]) amounting $i[amount] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME] Semester: $sem[sem_NAME] Reference: #$last_id";
				// $module = "Accounting";
				// AUDIT::ins($user, $module, $action);

//==================================================================================================================
				//AUDIT TRAIL ELIN START

			$user_info_id = $i['Account_ID'];
			$action_event = "ADD";	
			$event_desc =  "MODULE: Add Payables DESCRIPTION: User added Payables <br>DETAILS: <br>STUDENT ID :".$u['student_ID']."<br> STUDENT NAME ".$u['si_LNAME'].", ".$u['si_FNAME']."<br>PARTICULAR: ".$particular['particular_NAME']."<br>AMOUNT: ".$i['amount']."<br>SEMESTER: ".$sem['sem_NAME']."<br>REFERENCE: ".$last_id;
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
				
				//AUDIT TRAIL ELIN END
//==================================================================================================================
				#AUDIT TRAIL#
				
			    # HTML::redirect($exploded[2]."?action=view");
				href("accounting.php?action=view&id=$id");
		}
	
	
	
		
		#Adding Payables from Double Entry
		public static function addpayables2($i,$id,$id1)
		{
				$datenow = date("Y-m-d H:i:s");
				$time = date('Y-m-d H:i:s', strtotime($i['date']));
				$sql = "
				INSERT INTO	acc_payables
				SET
					payables_ID = ?,
					si_ID = ?,
					sem_ID = ?,
					payables_AMOUNT = ?,
					payables_DATE = ?,
					payables_USER_ID = ?,
					payables_DATE_ADDED = ?,
					particular_ID = ?,
					payables_NOTE = ?
				";
				$param = array(
					NULL ,
					$id,
					$i['sem_ID'],
					$i['amount'],
					$time,
					$id1,
					$datenow,
					$i['particulars'],
					$i['note']    
				);
				SQL::execute($sql, $param);
				SESSION::StoreMsg("Payables Successfully Added","success");
				$url = $_SERVER['PHP_SELF'];
				$exploded = explode("/",$url);
				
				$last_id = SQL::last_ID();
				#AUDIT TRAIL#
				$user = $id1;
				$u = USERS::viewSingleStudent($id);
				$particular = PARTICULARS::getSingleParticular($i['particulars']);
				$sem = SEM::getSingleSem($i['sem_ID']);
				$action = "ADDED PAYABLE ($particular[particular_NAME]) amounting $i[amount] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME] Semester: $sem[sem_NAME] Reference: #$last_id";
				$module = "Accounting";
				AUDIT::ins($user, $module, $action);
				#AUDIT TRAIL#

//==================================================================================================================
				//AUDIT TRAIL ELIN START

			$user_info_id = $i['Account_ID'];
			$action_event = "ADD";	
			$event_desc =  "MODULE: Add Payables DESCRIPTION: User added Payables <br>DETAILS: <br>STUDENT ID :".$u['student_ID']."<br> STUDENT NAME ".$u['si_LNAME'].", ".$u['si_FNAME']."<br>PARTICULAR: ".$particular['particular_NAME']."<br>AMOUNT: ".$i['amount']."<br>SEMESTER: ".$sem['sem_NAME']."<br>REFERENCE: ".$last_id;
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
				
				//AUDIT TRAIL ELIN END
//==================================================================================================================				
				
				
				
			   # HTML::redirect($exploded[2]."?action=view");
			  #HTML::redirect("accounting.php?action=view&id=$id");
		}

	
		#Get All distinct date
		public static function getAllPayablesDate($order=array(),$id)
		{
			//SELECT DISTINCT(payables_DATE)
			//SELECT payables_DATE
			$sql = "
			SELECT DISTINCT(a.payables_DATE)
			FROM acc_payables a
			WHERE a.si_ID = '$id'
			";
			foreach($order as $key => $value){
				if($key ==  0) {
					$sql .= "ORDER BY {$key} {$value}";
				} else {
					$sql .= ", {$key} {$value}";
				}
			}
			return SQL::find_all($sql);
		}
		
		#Get All Payable Amount
		public static function getAllPayablesAmount($date,$id)
		{
			$sql = "
			SELECT payables_ID, payables_DATE, payables_AMOUNT, particular_ID, payables_NOTE
			FROM acc_payables
			WHERE payables_DATE = '$date' and
			si_ID = '$id' and status = '1'
			";
			return SQL::find_all($sql);
		}
		
		public static function getAllPayablesAmountPerSem($date,$id,$sem)
		{
			$sql = "
			SELECT payables_ID, payables_DATE, payables_AMOUNT, particular_ID, payables_NOTE
			FROM acc_payables
			WHERE payables_DATE = '$date' and sem_ID = '$sem' and
			si_ID = '$id' and status = '1'
			";
			return SQL::find_all($sql);
		}
		
		public static function getAllPayablesAmountNull($date,$id)
		{
			$sql = "
			SELECT payables_ID, payables_DATE, payables_AMOUNT, particular_ID, payables_NOTE
			FROM acc_payables
			WHERE payables_DATE = '$date' and sem_ID is NULL and
			si_ID = '$id' and status = '1'
			";
			return SQL::find_all($sql);
		}
	

		#Find Payable by Student ID
		public static function findPayables($order=array(),$id)
		{
			$sql = "
			SELECT *
			FROM acc_payables
			WHERE si_ID = ?
			";
			$param = array($id);
			
			foreach($order as $key => $value){
				if($key ==  0) {
					$sql .= "ORDER BY {$key} {$value}";
				} else {
					$sql .= ", {$key} {$value}";
				}
			}
			return SQL::find_id($sql,$param);
		}
		
		#Get Payable Info by Payable ID	
		public static function getPayableByID($id)
		{
			$sql = "
			SELECT *
			FROM acc_payables a
			WHERE a.payables_ID = '$id'
			";
			
			return SQL::find_id($sql);
		}
		
		#Update Payable By Payable ID
		public static function updatePayableByID($data)
		{
			#print_r($data);
			$sql = "
			UPDATE acc_payables 
			set payables_AMOUNT = ?,
				sem_ID = ?,
				payables_NOTE = ?,
				particular_ID = ?,
				payables_DATE = ?
				
			WHERE payables_ID = ?
			";
			$date = date("Y-m-d H:i:s",strtotime($data['pay_date']));
				$param = array(
							$data['amount'],
							$data['sem_ID'],
							$data['note'],
							$data['particulars'],
							$date,
							$data['payables_ID']
						);
			
			#AUDIT TRAIL#
			$payable = SELF::getPayableByID($data['payables_ID']);
			$user = $_SESSION['USER']['account_ID'];
			$u = USERS::viewSingleStudent($payable['si_ID']);
			$particular = PARTICULARS::getSingleParticular($payable['particular_ID']);
			$particular2 = PARTICULARS::getSingleParticular($data['particulars']);
			$sem = SEM::getSingleSem($data['sem_ID']);
			$action = "UPDATED PAYABLE REFERENCE #$data[payables_ID]: ($particular2[particular_NAME]) amounting $data[amount] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME] Semester: $sem[sem_NAME], Date: $date" ;
			$module = "Accounting";
			AUDIT::ins($user, $module, $action);
			#AUDIT TRAIL#
			



			
			SQL::execute($sql, $param);		
			SESSION::StoreMsg("Payables Updated!", "success");
		}	
	
		#Hide Payable
		public static function hidePayableID($id,$uid)
		{
			$sql = "
			UPDATE acc_payables 
			set status = '0'
			WHERE payables_ID = ?
			";
			
				$param = array(
							$id
						);
			
			#AUDIT TRAIL#
			$payable = SELF::getPayableByID($id);
			$user = $_SESSION['USER']['account_ID'];
			$u = USERS::viewSingleStudent($payable['si_ID']);
			
			$particular = PARTICULARS::getSingleParticular($payable['particular_ID']);
		
			
			$action = "REVOKED PAYABLE REFERENCE #$id: ($particular[particular_NAME]) amounting $payable[payables_AMOUNT] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME]" ;
			$module = "Accounting";
			AUDIT::ins($user, $module, $action);
			#AUDIT TRAIL#







			
			SQL::execute($sql, $param);		
			SESSION::StoreMsg("Payable Hidden!", "success");
			href("accounting.php?action=view&id=$uid");
		}
		
		
		
		public static function getTFPayablesPerSem($id,$sem)
		{
			$sql = "
			SELECT SUM(payables_AMOUNT)
			FROM acc_payables
			WHERE sem_ID = '$sem' and
			si_ID = '$id' and status = '1'  and particular_ID =  1
			";
			return SQL::find_scalar($sql);
		}

		//2018-0815
		public static function getSPayablesPerSem($id,$sem)
		{
			$sql = "
			SELECT SUM(payables_AMOUNT)
			FROM acc_payables
			WHERE sem_ID = '$sem' and
			si_ID = '$id' and status = '1' 
			";
			return SQL::find_scalar($sql);
		}

		public static function getSPaymentPerSem($id,$sem)
		{
			$sql = "
			SELECT SUM(payment_AMOUNT)
			FROM acc_payment
			WHERE sem_ID = '$sem' and
			si_ID = '$id' and status = '1' 
			";
			return SQL::find_scalar($sql);
		}
		
		public static function getOFPayablesPerSem($id,$sem)
		{
			$sql = "
			SELECT SUM(payables_AMOUNT)
			FROM acc_payables
			WHERE sem_ID = '$sem' and
			si_ID = '$id' and status = '1'  and particular_ID !=  1
			";
			return SQL::find_scalar($sql);
		}
		
		
		public static function getPayablesPerSembyPID($pid,$id,$sem)
		{
			$sql = "
			SELECT SUM(payables_AMOUNT)
			FROM acc_payables
			WHERE sem_ID = '$sem' and
			si_ID = '$id' and status = '1'  and particular_ID = '$pid'
			";
			return SQL::find_scalar($sql);
		}
		public static function getPaymentPerSembyPID($pid,$id,$sem)
		{
			$sql = "
			SELECT SUM(payment_AMOUNT)
			FROM acc_payment
			WHERE sem_ID = '$sem' and
			si_ID = '$id' and status = '1'  and particular_ID = '$pid'
			";
			return SQL::find_scalar($sql);
		}
		
		
		public static function getAllPaymentPerSembyPID($pid,$id,$sem)
		{
			$sql = "
			SELECT *
			FROM acc_payment
			WHERE sem_ID = '$sem' and
			si_ID = '$id' and status = '1'  and particular_ID = '$pid'
			";
			return SQL::find_all($sql);
		}
		
		public static function isPayablesPerSembyPIDexist($pid,$sem)
		{
			$sql = "
			SELECT count(*)
			FROM acc_payables
			WHERE sem_ID = '$sem' and status = '1'  and particular_ID = '$pid'
			";
			return SQL::find_scalar($sql);
		}
		public static function isPaymentPerSembyPIDexist($pid,$sem)
		{
			$sql = "
			SELECT count(*)
			FROM acc_payment
			WHERE sem_ID = '$sem' and status = '1'  and particular_ID = '$pid'
			";
			return SQL::find_scalar($sql);
		}
		
		
		
		
		
		public static function isPayablesPerSembyPIDexistByCourse($pid,$sem,$course_ID)
		{
			$sql = "
			SELECT count(*) 
			FROM acc_payables a, curricullum_enrolled b, curricullum_list c 
			WHERE a.sem_ID = '$sem' and a.status = '1' and a.particular_ID = '$pid' and a.si_ID = b.si_ID and b.curricullum_ID = c.curricullum_ID and c.course_ID = '$course_ID'
			";
			return SQL::find_scalar($sql);
		}
		
		public static function isPaymentPerSembyPIDexistByCourse($pid,$sem,$course_ID)
		{
			$sql = "
			SELECT count(*) 
			FROM acc_payment a, curricullum_enrolled b, curricullum_list c 
			WHERE a.sem_ID = '$sem' and a.status = '1' and a.particular_ID = '$pid' and a.si_ID = b.si_ID and b.curricullum_ID = c.curricullum_ID and c.course_ID = '$course_ID'
			";
			return SQL::find_scalar($sql);
		}
	
		
		
		
		public static function getTFPaymentPerSem($id,$sem)
		{
			$sql = "
			SELECT SUM(payment_AMOUNT)
			FROM acc_payment
			WHERE sem_ID = '$sem' and
			si_ID = '$id' and status = '1'  and particular_ID =  1
			";
			return SQL::find_scalar($sql);
		}
		
		public static function getOFPaymentPerSem($id,$sem)
		{
			$sql = "
			SELECT SUM(payment_AMOUNT)
			FROM acc_payment
			WHERE sem_ID = '$sem' and
			si_ID = '$id' and status = '1'  and particular_ID !=  1
			";
			return SQL::find_scalar($sql);
		}
		
		
		
		public static function hidePayableID2($id,$uid)
		{
			$sql = "
			UPDATE acc_payables 
			set status = '0'
			WHERE payables_ID = ?
			";
			
				$param = array(
							$id
						);
			
					
			SQL::execute($sql, $param);		
			
			#AUDIT TRAIL#
			$payable = SELF::getPayableByID($id);
			$user = $_SESSION['USER']['account_ID'];
			$u = USERS::viewSingleStudent($payable['si_ID']);
			
			$particular = PARTICULARS::getSingleParticular($payable['particular_ID']);
		
			
			$action = "REVOKED PAYABLE REFERENCE #$id: ($particular[particular_NAME]) amounting $payable[payables_AMOUNT] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME]" ;
			$module = "Accounting";
			AUDIT::ins($user, $module, $action);
			#AUDIT TRAIL#
				
			
			#SESSION::StoreMsg("Payable Hidden!", "success");
			#HTML::redirect("accounting.php?action=view&id=$uid");
		}
	
	/*------------------- PAYABLE MODULE------------------------- */
	
		
		
		
		
		
	/*------------------- PAYMENT MODULE------------------------- */
		
		#Adding Payment from Single Entry
		public static function addpayment($i,$id,$id1){
				$datenow = date("Y-m-d H:i:s");
				$time = date('Y-m-d H:i:s', strtotime($i['date']));
				$sql = "
				INSERT INTO	acc_payment
				SET
					payment_ID = ?,
					si_ID = ?,
					sem_ID = ?,
					payment_OR = ?,
					payment_AMOUNT = ?,
					payment_DATE = ?,
					payment_UI = ?,
					payment_DATE_ADDED = ?,
					particular_ID = ?,
					payment_NOTE = ?
				";
				$param = array(
					NULL ,
					$id,
					$i['sem_ID'],
					$i['ornum'],
					$i['amount'],
					$time,
					$id1,
					$datenow,
					$i['particulars'],
					$i['note']    
				);
				SQL::execute($sql, $param);
				SESSION::StoreMsg("Payments Successfully Added","success");
				$url = $_SERVER['PHP_SELF'];
				$exploded = explode("/",$url);
				
				
				
				
				#AUDIT TRAIL#
				$last_id = SQL::last_ID();
				$user = $id1;
				$u = USERS::viewSingleStudent($id);
				$particular = PARTICULARS::getSingleParticular($i['particulars']);
				$sem = SEM::getSingleSem($i['sem_ID']);
				$action = "ADDED PAYMENT ($particular[particular_NAME]) amounting (OR#$i[ornum]) $i[amount] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME] Semester: $sem[sem_NAME] Reference: #$last_id";
				$module = "Accounting";
				AUDIT::ins($user, $module, $action);
				#AUDIT TRAIL#

//==================================================================================================================
			// AUDIT TRAIL START ELIN

			$user_info_id = $i['Account_ID'];
			$action_event = "ADD";
			$event_desc =  "MODULE: Add Payment DESCRIPTION: User Add Payment <br>DETAILS: <br>*SINGLE PAYMENT*<br>STUDENT ID :".$u['student_ID']."<br> STUDENT NAME".$u['si_LNAME'].", ".$u['si_FNAME']."<br>PARTICULAR: ".$particular['particular_NAME']."<br> OR # ".$i["ornum"]."<br>AMOUNT: ".$i['amount']."<br>SEMESTER: ".$sem['sem_NAME']."<br>REFERENCE: ".$last_id;
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);

			// AUDIT TRAIL START ELIN		
//==================================================================================================================				
				
				
				
				
				
				#HTML::redirect($exploded[2]."?action=view");
				// var r = confirm("Press a button!\nEither OK or Cancel.\nThe button you pressed will be displayed in the result window.");
				echo '<script>
				
					var txt;
					var r = confirm("Do you want to print a reciept.");
					if (r == true) {
						window.location = "assets/print_template/print_or.php?id='.$id.'&stud_number='.$i['mmma_student_id'].'&stud_name='.$i['mmma_student_name'].'&amount='.$i['amount'].'&amounttowords='.$i['amounttowords'].'&poNAME='.$i['mmma_student_paymentOption'].'";
					} else {
						window.location = "accounting.php?action=view&id='.$id.'";
					}
				
				</script>';
				//  href("accounting.php?action=view&id=$id");
			  #  http://localhost/isjb/accounting.php?action=view&id=1
		}
			
	   #Adding Payment from Double Entry
	   public static function addpayment2($i,$id,$id1)
	   {
			$datenow = date("Y-m-d H:i:s");
			$time = date('Y-m-d H:i:s', strtotime($i['date']));
			$sql = "
			INSERT INTO	acc_payment
			SET
				payment_ID = ?,
				si_ID = ?,
				sem_ID = ?,
				payment_OR = ?,
				payment_AMOUNT = ?,
				payment_DATE = ?,
				payment_UI = ?,
				payment_DATE_ADDED = ?,
				particular_ID = ?,
				payment_NOTE = ?
			";
			$param = array(
				NULL ,
				$id,
				$i['sem_ID'],
				$i['ornum'],
				$i['amount'],
				$time,
				$id1,
				$datenow,
				$i['particulars'],
				$i['note']    
			);
			SQL::execute($sql, $param);
			SESSION::StoreMsg("Payments Successfully Added","success");
			$url = $_SERVER['PHP_SELF'];
			$exploded = explode("/",$url);
			
			
			
			#AUDIT TRAIL#
			$last_id = SQL::last_ID();
			$user = $id1;
			$u = USERS::viewSingleStudent($id);
			$particular = PARTICULARS::getSingleParticular($i['particulars']);
			$sem = SEM::getSingleSem($i['sem_ID']);
			$action = "ADDED PAYMENT ($particular[particular_NAME]) amounting (OR#$i[ornum]) $i[amount] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME] Semester: $sem[sem_NAME] Reference: #$last_id";
			$module = "Accounting";
			AUDIT::ins($user, $module, $action);
			#AUDIT TRAIL#
			
//==================================================================================================================
			// AUDIT TRAIL START ELIN

			$user_info_id = $i['Account_ID'];
			$action_event = "ADD";
			$event_desc =  "MODULE: Add Payment DESCRIPTION: User Add Payment <br>DETAILS: <br>*SINGLE PAYMENT*<br>STUDENT ID :".$u['student_ID']."<br> STUDENT NAME".$u['si_LNAME'].", ".$u['si_FNAME']."<br>PARTICULAR: ".$particular['particular_NAME']."<br> OR # ".$i["ornum"]."<br>AMOUNT: ".$i['amount']."<br>SEMESTER: ".$sem['sem_NAME']."<br>REFERENCE: ".$last_id;
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);

			// AUDIT TRAIL END ELIN		
//==================================================================================================================	



			
			#HTML::redirect($exploded[2]."?action=view");
			# HTML::redirect("accounting.php?action=view&id=$id");
		  #  http://localhost/isjb/accounting.php?action=view&id=1
		}
		
		#Update Payments
		public static function updatePaymentByID($data)
		{
			#print_r($data);
			$sql = "
			UPDATE acc_payment 
			set payment_AMOUNT = ?,
				sem_ID = ?,
				payment_NOTE = ?,
				particular_ID = ?,
				payment_OR = ?,
				payment_DATE = ?
			WHERE payment_ID = ?
			";
			$date = date("Y-m-d H:i:s",strtotime($data['date']));
			$param = array(
							$data['amount'],
							$data['sem_ID'],
							$data['note'],
							$data['particulars'],
							$data['ornum'],
							$date,
							$data['payment_ID']
							
						);
							
			SQL::execute($sql, $param);		
			
			
			#AUDIT TRAIL#
			$payment = SELF::getPaymentByID($data['payment_ID']);
			$user = $_SESSION['USER']['account_ID'];
			$u = USERS::viewSingleStudent($payment['si_ID']);
			$particular = PARTICULARS::getSingleParticular($payment['particular_ID']);
			$particular2 = PARTICULARS::getSingleParticular($data['particulars']);
			$sem = SEM::getSingleSem($data['sem_ID']);
			$action = "UPDATED PAYMENT REFERENCE #$data[payment_ID]: ($particular2[particular_NAME]) amounting (#$data[ornum]) $data[amount] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME] Semester: $sem[sem_NAME], Date: $date" ;
			$module = "Accounting";
			AUDIT::ins($user, $module, $action);
			#AUDIT TRAIL#
			
			
			
			
			
			
			
			
			
			
			
			SESSION::StoreMsg("Payment Updated!", "success");
			
		}		
		
		#Hide Payment by Payment ID
		public static function hidePaymentID($id,$uid)
		{
			$sql = "
			UPDATE acc_payment
			set status = '0'
			WHERE payment_ID = ?
			";
			
				$param = array(
							$id
						);
			
			
			#AUDIT TRAIL#
			$payment = SELF::getPaymentByID($id);
			$user = $_SESSION['USER']['account_ID'];
			$u = USERS::viewSingleStudent($payment['si_ID']);
			$particular = PARTICULARS::getSingleParticular($payment['particular_ID']);
			$action = "REVOKED PAYMENT REFERENCE #$id: ($particular[particular_NAME]) amounting (OR#$payment[payment_OR]) $payment[payment_AMOUNT] to ($u[student_ID])$u[si_LNAME], $u[si_FNAME]" ;
			$module = "Accounting";
			AUDIT::ins($user, $module, $action);
			#AUDIT TRAIL#
			
			
			
			
			SQL::execute($sql, $param);		
			SESSION::StoreMsg("Payment Hidden!", "success");
			href("accounting.php?action=view&id=$uid");
		}
		
		#Get Payments by Student ID and Date		
		public static function getAllPayment($date,$id)
		{
			$sql = "
			SELECT payment_ID, payment_DATE, payment_AMOUNT, particular_ID, payment_NOTE, payment_OR
			FROM acc_payment
			WHERE payment_DATE = '$date' and
			si_ID = '$id' and status = '1'
			";
			return SQL::find_all($sql);
		}
		
		
		public static function getAllPaymentPerSem($date,$id,$sem)
		{
			$sql = "
			SELECT payment_ID, payment_DATE, payment_AMOUNT, particular_ID, payment_NOTE, payment_OR
			FROM acc_payment
			WHERE payment_DATE = '$date' and sem_ID = '$sem' and
			si_ID = '$id' and status = '1'
			";
			return SQL::find_all($sql);
		}
		
		public static function getAllPaySem($sem,$order=array())
		{
			$sql = "
			SELECT payment_DATE,SUM(payment_AMOUNT) as `total`
			FROM acc_payment 
			WHERE sem_ID = ? and status = 1
			GROUP BY payment_DATE
			";
			
			foreach($order as $key => $value)
				{
					if($key ==  0) 
					{
						$sql .= "ORDER BY {$key} {$value}";
					} else {
						$sql .= ", {$key} {$value}";
					}
				}
			
			
			$param = array(
						$sem
					);
			
			return SQL::find_all($sql,$param);
		}
		
		
		public static function getAllPay($order=array())
		{
			$sql = "
			SELECT payment_DATE,SUM(payment_AMOUNT) as `total`
			FROM acc_payment 
			WHERE  status = 1
			GROUP BY payment_DATE 
			
			";
			
			foreach($order as $key => $value)
				{
					if($key ==  0) 
					{
						$sql .= "ORDER BY {$key} {$value}";
					} else {
						$sql .= ", {$key} {$value}";
					}
				}
			
			
			$param = array(
					
					);
			
			return SQL::find_all($sql,$param);
		}
		
		public static function getAllPaymentNull($date,$id)
		{
			$sql = "
			SELECT payment_ID, payment_DATE, payment_AMOUNT, particular_ID, payment_NOTE, payment_OR
			FROM acc_payment
			WHERE payment_DATE = '$date' and sem_ID is NULL and
			si_ID = '$id' and status = '1'
			";
			return SQL::find_all($sql);
		}
	
		#Get Payments by Student ID
		public static function getPaymentByID($id)
		{
			$sql = "
			SELECT *
			FROM acc_payment a
			WHERE a.payment_ID = '$id'
			";
			
			return SQL::find_id($sql);
		}	
		
		#Get All Payments
		public static function getallpayments($order=array())
		{
			$sql = "
			SELECT *
			FROM acc_payment WHERE status = 1
			";
			foreach($order as $key => $value){
				if($key ==  0) {
					$sql .= "ORDER BY {$key} {$value}";
				} else {
					$sql .= ", {$key} {$value}";
				}
			}
			return SQL::find_all($sql);
		}	
	
		#Get All Payments By Date
		public static function getallpaymentsbydate($order=array(),$start,$end)
		{
			if($end == NULL)
			{
				$end = date("Y-m-d 23:59:00",strtotime($start));
			}else{
				$end = date("Y-m-d 11:59:00",strtotime($end));
			}
			
				$sql = "
				SELECT *
				FROM acc_payment
				WHERE payment_DATE >= '$start' and payment_DATE <= '$end' and status != 0
				";
				foreach($order as $key => $value)
				{
					if($key ==  0) 
					{
						$sql .= "ORDER BY {$key} {$value}";
					} else {
						$sql .= ", {$key} {$value}";
					}
				}
			return SQL::find_all($sql);
		}
	
	
		#Check OR IF Exist
		public static function orchecker($x)
		{
				$sql = "
				SELECT *
				FROM acc_payment
				WHERE payment_OR = ? and status = 1 and sem_ID = ?
				";
			$param = array($x['ornum'], $x['sem_ID']);
			return SQL::find_id($sql,$param);
			
		}
		
		#Check OR for Student
		public static function checkOR($or,$si_ID,$sem_ID)
		{
				$sql = "
				SELECT count(*)
				FROM acc_payment
				WHERE payment_OR = ? and si_ID = ? and status = 1 and sem_ID = ?
				";
				$param = array($or,$si_ID,$sem_ID);
				return SQL::find_scalar($sql,$param);	
		}
		
		#GET OR Information
		public static function checkORData($or,$si_ID,$sem_ID)
		{
				$sql = "
				SELECT *
				FROM acc_payment
				WHERE payment_OR = ? and status = 1 and si_ID = ? and sem_ID = ?
				";
			$param = array($or,$si_ID,$sem_ID);
			return SQL::find_id($sql,$param);
			
		}	
		
		
		public static function getAllPaymentPerSemTF($id,$sem)
		{
			$sql = "
			SELECT payment_ID, payment_DATE, payment_AMOUNT, particular_ID, payment_NOTE, payment_OR
			FROM acc_payment
			WHERE sem_ID = '$sem' and
			si_ID = '$id' and status = '1' and particular_ID = 1
			";
			return SQL::find_all($sql);
		}
		
	/*------------------- PAYMENT MODULE------------------------- */	
	
	/*------------------- EXPENSE MODULE------------------------- */
	
		#Adding Expense
		public static function addExpense($info,$id1)
		{
				$datenow = date("Y-m-d H:i:s");
				
				$sql = "
				INSERT INTO	acc_expenses
				SET
					department_ID = ?,
					ae_PARTICULARS = ?,
					ae_AMOUNT = ?,
					ae_DATE = ?,
					ae_REQUEST = ?,
					ae_RECEIVED = ?,
					account_ID = ?,
					dateposted = ?,
					NOTE = ?
				";
				$date = date("Y-m-d",strtotime($info['ae_DATE']));
				
				$param = array(
					$info['department_ID'],
					$info['ae_PARTICULARS'],
					$info['ae_AMOUNT'],
					$date,
					$info['ae_REQUEST'],
					$info['ae_RECEIVED'],
					$id1,
					$datenow,
					$info['NOTE']
					
				);
				SQL::execute($sql, $param);
				SESSION::StoreMsg("Expense Successfully Added","success"); 
				
			 	#HTML::redirect("accounting_expense.php?action=view");
				
				#AUDIT TRAIL#
				$last_id = SQL::last_ID();
				$user = $_SESSION['USER']['account_ID'];
				$dept = DEPT::getdeptbyID($info['department_ID']);
				$action = "ADDED EXPENSE ($info[ae_PARTICULARS]) to $dept[department_NAME] amounting $info[ae_AMOUNT] Date: $date  Reference: #$last_id";
				$module = "Accounting";
				AUDIT::ins($user, $module, $action);
				#AUDIT TRAIL#
				
				SESSION::StoreMsg("Expense Added!", "success");
			 	href("accounting_expense.php?action=view");
		}
		
		
		
		
		public static function getAllExpensePerday($order=array())
		{
			$sql = "
			SELECT ae_DATE,SUM(ae_AMOUNT) as `total`
			FROM acc_expenses 
			WHERE  status = 1
			GROUP BY ae_DATE
			";
			foreach($order as $key => $value)
				{
					if($key ==  0) 
					{
						$sql .= "ORDER BY {$key} {$value}";
					} else {
						$sql .= ", {$key} {$value}";
					}
				}
			return SQL::find_all($sql);
		}
		
		
		
	
		
		#Get All Expense
		public static function getAllExpense($order=array())
		{
			$sql = "
			SELECT *
			FROM acc_expenses
			";
			
				foreach($order as $key => $value){
					if($key ==  0) {
						$sql .= "ORDER BY {$key} {$value}";
					} else {
						$sql .= ", {$key} {$value}";
					}
				}
			
			
			return SQL::find_all($sql);
		}
		
		#Get Single Expense 
		public static function getSingleExpense($id)
		{
			$sql = "
			SELECT *
			FROM acc_expenses
			WHERE
				ae_ID = '$id'
			";
			return SQL::find_id($sql);
		}
		
		
		
		#Get All Expense Inactive	
		public static function getAllExpenseInactive()
		{
			$sql = "
			SELECT *
			FROM acc_expenses
			WHERE status = '0'
			";
			return SQL::find_all($sql);
		}
		
		
		#Get All Expense Active	
		public static function getAllExpenseActive($order=array())
		{
			$sql = "
			SELECT *
			FROM acc_expenses
			WHERE status = '1'
			";
			
			
				foreach($order as $key => $value){
					if($key ==  0) {
						$sql .= " ORDER BY {$key} {$value}";
					} else {
						$sql .= ", {$key} {$value}";
					}
				}
			
			
			return SQL::find_all($sql);
		}
		
		
		#Get Expense By Date and Particular
		public static function getAllActiveExpenseByParticularDate($particular,$date)
		{
			$sql = "
			SELECT SUM(ae_AMOUNT) as `SUM`
			FROM acc_expenses
			WHERE 
			ae_DATE >= ? and ae_DATE <= ? 
			and status = 1 and
			ae_PARTICULARS = ?
			";
			
							$param = array(
					$date,
					$date,
					array($particular, PDO::PARAM_STR)
					
				);
			
			return SQL::find_id($sql,$param);
		}
		
		
		
		#Get All Expense By Date	
		public static function getAllActiveExpenseByDate($info)
		{
			$sql = "
			SELECT *
			FROM acc_expenses
			WHERE 
			ae_DATE >= ? and ae_DATE <= ? 
			and status = 1
			";
			
							$param = array(
					$info['date_start'],
					$info['date_end']
					
				);
			
			return SQL::find_all($sql,$param);
		}
		
		public static function getAllAEParticularsActiveByMonthByParticulars($month,$particular)
		{
	
				
			$sql = "
			SELECT SUM(ae_AMOUNT) as `SUM`
			FROM acc_expenses
			WHERE ae_DATE LIKE '%$month%'
			and status = 1 and ae_PARTICULARS = ?
			";
		
			
			#echo $sql;
			$param = array(
				array($particular, PDO::PARAM_STR));
				
			return SQL::find_id($sql,$param);
		}
		
		
		
		
		
		
		public static function getAllActiveExpenseByDateCustom($info)
		{
			$sql = "
			SELECT *
			FROM acc_expenses
			WHERE 
			ae_DATE >= ? and ae_DATE <= ? 
			and status = 1
			";
			
			$x = explode(",",$info['show']);
			
			$i = 0;
				foreach($x as $key => $value){
				$value = clean($value);
					if($i ==  0) {
						$sql .= "and (";
					} 
					
					if($i == 0)
					{
						$sql .= " ae_PARTICULARS ='$value' ";
					}else{
						$sql .= " or ae_PARTICULARS ='$value'";
					}
					
					
					$i++;
					
					if(count($x) == $i)
					{
					$sql .= ")";
					}
					
				}
			
					$param = array(
						date("Y-m-d",strtotime($info['date'])),
						date("Y-m-d",strtotime($info['date']))
							
						);
			#echo $sql;
			return SQL::find_all($sql,$param);
		}
		
		
		
		
		
		
		#Get All Inative Expense By Date	
		public static function getAllInactiveExpenseByDate($info)
		{
			$sql = "
			SELECT *
			FROM acc_expenses
			WHERE 
			ae_DATE >= ? and ae_DATE <= ? 
			and status = 0
			";
			
							$param = array(
					$info['date_start'],
					$info['date_end']
					
				);
			
			return SQL::find_all($sql,$param);
		}
		
		#Get All Particulars
		public static function getAllAEParticulars()
		{
			$sql = "
			SELECT distinct(ae_PARTICULARS)
			FROM acc_expenses
			";
			
			return SQL::find_all($sql);
		}
		
		
		#Get All Particulars By Month
		public static function getAllAEParticularsActiveByMonth($month)
		{
	
			$sql = "
			SELECT distinct(ae_PARTICULARS)
			FROM acc_expenses
			WHERE ae_DATE LIKE '%$month%'
			and status = 1
			";
		
			
			#echo $sql;
			return SQL::find_all($sql);
		}
		
		
		
		
		
		#Get All REQUEST
		public static function getAllRequest()
		{
			$sql = "
			SELECT distinct(ae_REQUEST)
			FROM acc_expenses
			";
			
			return SQL::find_all($sql);
		}
		
		#Get All RECEIVED
		public static function getAllReceived()
		{
			$sql = "
			SELECT distinct(ae_RECEIVED)
			FROM acc_expenses
			";
			
			return SQL::find_all($sql);
		}
		
		#Get All Date
		public static function getAllDate()
		{
			$sql = "
			SELECT distinct(ae_DATE)
			FROM acc_expenses
			";
			
			return SQL::find_all($sql);
		}
		
		
		#Updating Expense
		public static function updateExpense($info,$id1)
		{
				$datenow = date("Y-m-d H:i:s");
				
				$sql = "
				Update	acc_expenses
				SET
					department_ID = ?,
					ae_PARTICULARS = ?,
					ae_AMOUNT = ?,
					ae_DATE = ?,
					ae_REQUEST = ?,
					ae_RECEIVED = ?,
					account_ID = ?,
					dateposted = ?,
					NOTE = ?
				WHERE
					ae_ID = ?
				";
				$date = date("Y-m-d",strtotime($info['ae_DATE']));
				$param = array(
					$info['department_ID'],
					$info['ae_PARTICULARS'],
					$info['ae_AMOUNT'],
					$date,
					$info['ae_REQUEST'],
					$info['ae_RECEIVED'],
					$id1,
					$datenow,
					$info['NOTE'],
					$info['ae_ID']
					
				);
				
				
				
				
				#AUDIT TRAIL#
				$user = $_SESSION['USER']['account_ID'];
				$dept = DEPT::getdeptbyID($info['department_ID']);
				$action = "UPDATED EXPENSE ($info[ae_PARTICULARS]) to $dept[department_NAME] amounting $info[ae_AMOUNT] Date: $date  Reference: #$ae_ID";
				$module = "Accounting";
				AUDIT::ins($user, $module, $action);
				#AUDIT TRAIL#
				
				
				
				
				SQL::execute($sql, $param);
				SESSION::StoreMsg("Expense Successfully Updated","success"); 
			 	#HTML::redirect("accounting_expense.php?action=view");
		}
		
		
		#Hide Expense
		public static function hideExpense($id,$id1)
		{
				$datenow = date("Y-m-d H:i:s");
				
				$sql = "
				Update	acc_expenses
				SET
					
					status = 0
				WHERE
					ae_ID = '$id'
				";
				
				SQL::execute($sql);
				
				#AUDIT TRAIL#
				$user = $_SESSION['USER']['account_ID'];
				$action = "REVOKED EXPENSE Reference: #$ae_ID";
				$module = "Accounting";
				AUDIT::ins($user, $module, $action);
				#AUDIT TRAIL#
				
				SESSION::StoreMsg("Expense Successfully Deleted!","success"); 
			 	href("accounting_expense.php?action=view");
		}
		
	/*------------------- EXPENSE MODULE------------------------- */
	
	
	/*------------------- DEPOSIT MODULE------------------------- */
		#Adding Deposit
		public static function addDeposit($info,$id1)
		{
				$datenow = date("Y-m-d H:i:s");
				$sql = "
				INSERT INTO	acc_deposits
				SET
					
					ad_AMOUNT = ?,
					ad_DATE = ?,
					account_ID = ?,
					dateposted = ?,
					ad_NOTE = ?
				";
				$date = date("Y-m-d",strtotime($info['ad_DATE']));
				$param = array(
					$info['ad_AMOUNT'],
					$date,
					$id1,
					$datenow,
					$info['ad_NOTE']
				);
				SQL::execute($sql, $param);
				
				$p_id = SQL::last_ID();
				
				if(isset($_FILES['ad_FILE']) && ($_FILES['ad_FILE']['size'] > 0))
				{
						$target_path = "images/deposit/";
						$target_path = $target_path . $p_id .".jpg"; 
						$file_name = $p_id.".jpg";
						
						if(	move_uploaded_file($_FILES['ad_FILE']['tmp_name'], $target_path))
						{
									$sql = "
									Update acc_deposits
									SET
										ad_FILE = ?
									WHERE ad_ID = ?	
									";
									$param = array(
										$file_name,
										$p_id
									);
									SQL::execute($sql, $param);
											
						}else{
							#SESSION::StoreMsg("DepositSuccessfully  Added","success"); 
						}
				}	
				
				
				#AUDIT TRAIL#
				$user = $_SESSION['USER']['account_ID'];
				$dept = DEPT::getdeptbyID($info['department_ID']);
				$action = "ADDED DEPOSIT amounting to $info[ad_AMOUNT] Date: $date Reference: #$p_id";
				$module = "Accounting";
				AUDIT::ins($user, $module, $action);
				#AUDIT TRAIL#
				
				SESSION::StoreMsg("Deposit Successfully Added","success"); 
			 	href("accounting_deposits.php?action=view");
		}
		
		public static function getAllDepositPerday($order=array())
		{
			$sql = "
			SELECT ad_DATE,SUM(ad_AMOUNT) as `total`
			FROM acc_deposits 
			WHERE  status = 1
			GROUP BY ad_DATE
			";
			foreach($order as $key => $value)
				{
					if($key ==  0) 
					{
						$sql .= "ORDER BY {$key} {$value}";
					} else {
						$sql .= ", {$key} {$value}";
					}
				}
			return SQL::find_all($sql);
		}
		
		
		
		#Update Deposit
		public static function updateDeposit($info,$id1)
		{
				$datenow = date("Y-m-d H:i:s");
				
				$sql = "
				UPDATE	acc_deposits
				SET
					
					ad_AMOUNT = ?,
					ad_DATE = ?,
					account_ID = ?,
					ad_NOTE = ?
				WHERE ad_ID = ?
				";
				$param = array(
					$info['ad_AMOUNT'],
					date("Y-m-d",strtotime($info['ad_DATE'])),
					$id1,
					$info['NOTE'],
					$info['ad_ID']
				);
				SQL::execute($sql, $param);
				SESSION::StoreMsg("Deposit Successfully Updated","success"); 
			 	href("accounting_deposits.php?action=view");
		}
	
	
	
	
	
	
	
	
	
	
		#Get All Deposit By Date	
		public static function getAllActiveDepositByDate($info)
		{
			$sql = "
			SELECT *
			FROM acc_deposits
			WHERE 
			ad_DATE >= ? and ad_DATE <= ? 
			and status = 1
			";
			
			$param = array(
					date("Y-m-d",strtotime($info['date'])),
					date("Y-m-d",strtotime($info['date']))
					
				);
			
			return SQL::find_all($sql,$param);
		}
		
		#Get All SUM DEPOSIT BY DATE	
		public static function getSUMActiveDepositByDate($info)
		{
			$sql = "
			SELECT SUM(ad_AMOUNT) as `SUM`
			FROM acc_deposits
			WHERE 
			ad_DATE >= ? and ad_DATE <= ? 
			and status = 1
			";
			
			$param = array(
					date("Y-m-d",strtotime($info['date'])),
					date("Y-m-d",strtotime($info['date']))
					
				);
			
			return SQL::find_id($sql,$param);
		}
	
	
	#Get All Deposit Active	
		public static function getAllDepositActive($order=array())
		{
			$sql = "
			SELECT *
			FROM acc_deposits
			WHERE status = '1'
			";
			
			
				foreach($order as $key => $value){
					if($key ==  0) {
						$sql .= " ORDER BY {$key} {$value}";
					} else {
						$sql .= ", {$key} {$value}";
					}
				}
			
			
			return SQL::find_all($sql);
		}
	
	
	#Get Single Deposit 
		public static function getSingleDeposit($id)
		{
			$sql = "
			SELECT *
			FROM acc_deposits
			WHERE
				ad_ID = '$id'
			";
			return SQL::find_id($sql);
		}
	
	#Hide Deposit
		public static function hideDeposit($id,$id1)
		{
				$datenow = date("Y-m-d H:i:s");
				
				$sql = "
				Update	acc_deposits
				SET
					
					status = 0
				WHERE
					ad_ID = '$id'
				";
				
				SQL::execute($sql);
				
				
				
				#AUDIT TRAIL#
				$user = $_SESSION['USER']['account_ID'];
				$dept = DEPT::getdeptbyID($info['department_ID']);
				$action = "REVOKED DEPOSIT Reference: #$p_id Date: $datenow ";
				$module = "Accounting";
				AUDIT::ins($user, $module, $action);
				#AUDIT TRAIL#
				
				
				
				
				
				SESSION::StoreMsg("Deposit Successfully Deleted!","success"); 
			 	href("accounting_deposits.php?action=view");
		}
	
	
	/*------------------- DEPOSIT MODULE------------------------- */
	
	
	
	/* - */
	
	   public static function getSingle($order=array(),$id)
	{
		
			$sql = "
			SELECT *
			FROM student_information
			WHERE si_ID = ?
			";
			

		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function getSingleSAI($order=array(),$id)
	{
		
			$sql = "
			SELECT *
			FROM student_additional_info
			WHERE si_ID = ?
			";
			

		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function getSingleSRP($order=array(),$id)
	{
		
			$sql = "
			SELECT *
			FROM student_requirements_passed
			WHERE req_ID = ?
			";

		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    #MOJA END

    #KYL
 //    public static function getAllStudentInformation($order=array()){
	// 	$sql = "
	// 	SELECT *
	// 	FROM student_information
	// 	";
	// 	foreach($order as $key => $value){
	// 		if($key ==  0) {
	// 			$sql .= "ORDER BY {$key} {$value}";
	// 		} else {
	// 			$sql .= ", {$key} {$value}";
	// 		}
	// 	}
	// 	return SQL::find_all($sql);
	// }
	
    public static function viewStudents($order=array()){
		$sql = "
		SELECT *
		FROM student_information
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}

	public static function view1Student($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}

	
	/* - */
		
		
	
 	public static function getallAF($order=array()){
		$sql = "
		SELECT *
		FROM fee_assessment
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
	
	public static function getAF($id){
		$sql = "
		SELECT *
		FROM fee_assessment
		WHERE
		af_ID = '$id'
		";
		
		return SQL::find_id($sql);
	}
	
	public static function getALLpaidAF($sem,$af){
		$sql = "
		SELECT *
		FROM enrolment
		WHERE
		sem_ID = '$sem' and af_ID LIKE '%$af%'
		";
		
		return SQL::find_all($sql);
	}

	
		
}


?>
<?php
class AUDIT
{
	
	
	public static function insert_new($ACCOUNT_ID,$ACTION_EVENT,$EVENT_DESCRIPTION){
	 		$EVENT_DATETIME = date("Y-m-d H:i:s");
	 		$CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
           	$sql = "
			INSERT INTO	audit_trail
			SET
                ACCOUNT_ID = ?,
				CLIENT_IP = ?,
				ACTION_EVENT = ?,
				EVENT_DESCRIPTION = ?,
				EVENT_DATETIME = ?
			";
			$param = array(
				$ACCOUNT_ID,
				$CLIENT_IP,
				$ACTION_EVENT,
				$EVENT_DESCRIPTION,
				$EVENT_DATETIME
			);
			SQL::execute($sql, $param);
	}	
	

	 public static function insert($account_ID,$note){
	 
	 $d = date("Y-m-d H:i:s");
	 
	 
           $sql = "
			INSERT INTO	audit_logs
			SET
                account_ID = ?,
				audit_DATE = ?,
				audit_NOTE = ?
			";
			
			$param = array(
				$account_ID,
				$d,
				$note
			);

			SQL::execute($sql, $param);
	}	
	
	
	
	 public static function ins($account_ID,$module,$note){
	 
	 $d = date("Y-m-d H:i:s");
	 
	 
           $sql = "
			INSERT INTO	audit_logs
			SET
                account_ID = ?,
				audit_DATE = ?,
				audit_NOTE = ?,
				audit_MODULE = ?
			";
			
			$param = array(
				$account_ID,
				$d,
				$note,
				$module
			);

			SQL::execute($sql, $param);
	}	
	
	
	
	
	
	public static function getAllAudit($order=array()){
		$sql = "
		SELECT 	*
		FROM audit_logs
		";
		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";
		}
		$x=1;
			foreach($order as $key => $value)
			{
					if($x == 1)
					{
							$sql .= "{$key} {$value}";
					}	else{
						$sql .= ", {$key} {$value}";
					}
			$x++;
			}
		$sql .= " LIMIT 10000";
		return SQL::find_all($sql);
	}
	
	
	public static function getAuditby($date,$module,$user,$action,$order=array()){
		$sql = "
		SELECT 	*
		FROM audit_logs
		";
		
		if(isset($date) or isset($module) or isset($user) or isset($action) )
		{
			$sql .= "WHERE ";
			
			if(isset($date)){
				$sql .= "audit_DATE LIKE '%$date%'";
			}
	
		}
		
		
		
		
		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";
		}
		$x=1;
			foreach($order as $key => $value)
			{
					if($x == 1)
					{
							$sql .= "{$key} {$value}";
					}	else{
						$sql .= ", {$key} {$value}";
					}
			$x++;
			}
		$sql .= " LIMIT 10000";
		return SQL::find_all($sql);
	}
	
	public static function getAllModule(){
		$sql = "
		SELECT DISTINCT(audit_MODULE) FROM `audit_logs`
		";

		return SQL::find_all($sql);
		
	}

	
}

?>
<?php
class CHANGELOGS{


	public static function getLogByID($id)
	{

		$sql = "SELECT * FROM changelogs where id = '$id'";
		
		return SQL::find_id($sql);
	}


	public static function getRecentLogs()
	{

		$sql = "SELECT * FROM changelogs ORDER BY id DESC LIMIT 3";
		
		CHANGELOGS::fetch(SQL::find_all($sql));
		#return SQL::find_all($sql);
	}
 
 
 	 public static function insert($data){
	 
	 $d = date("Y-m-d H:i:s");
           $sql = "
			INSERT INTO	changelogs
			SET
                content = ?,
				date = ?
                
				
			";
			$param = array(
				$data['content'],
				$d
			);

			SQL::execute($sql, $param);
	}	
	
	
 	public static function update($data){
	# var_dump($data);
	 $d = date("Y-m-d H:i:s");
           $sql = "
			UPDATE	changelogs
			SET
                content = ?,
				date = ?
                WHERE id = ?
				
			";
			$param = array(
				$data['content'],
				$d,
				$data['id']
			);

			SQL::execute($sql, $param);
	}		


	public static function getAllLogs($order=array()){
		$sql = "
		SELECT 	*
		FROM changelogs
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{
					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}
			$x++;
			}
		return SQL::find_all($sql);
		
	}


	//Displays the current session flash message and then unset it

	
	public static function fetch($data){
	$msg = "";
	$latest_date = '';
	$x=1;
		foreach($data as $key => $val)
		{
			if($x==1)
			{
				$latest_date = $val['date'];
			}
		
			$msg .= "<b>".date("F j, Y:",strtotime($val['date']))."</b><br>".$val['content']."<br>";
			$x++;
		}
		
		
		CHANGELOGS::storeUpd($msg,$latest_date);
	}


	public static function StoreUpd($msg,$date, $type = 'changes'){
		$_SESSION['UPD']['txt'] = nl2br($msg);
		$_SESSION['UPD']['type'] = $type;
		
		CHANGELOGS::DisplayUpdates($date);
	}

	public static function DisplayUpdates($date){
		if(isset($_SESSION['UPD'])){				
			switch($_SESSION['UPD']['type']){
				case 'changes' : 
					$type = "alert-info"; 
					$title = "Changelogs!";
					break;
				case 'success' : 
					$type = "alert-success"; 
					$title = "Success!";
					break;
				case 'error' : 
					$type = "alert-danger"; 
					$title = "Error!";
					break;
				case 'warning' : 
					$type = "alert-warning";
					$title = "Warning!";
					break;
				case 'info' : 
					$type = "alert-info";
					$title = "Heads Up!";
					break;
				default: 
					$type = "alert-success";
					$title = "Success!";
			}
			
			?>

			<div class="alert alert-dismissable <?php echo $type; ?>">
				<button type="button" class="close" data-dismiss="alert"></button>
				<h4> <?php echo $title; ?> </h4>
                <h5>Last update as of <?= date("M j, Y h:i A",strtotime($date)); ?></h5>
				<?php echo $_SESSION['UPD']['txt']; ?>
                
                
			</div>

			<?php
			unset($_SESSION['MSG']);
			return true;
		} else {
			return false;
		}
	}	
	
	
	
	
	
	
	
	

	

  
}

?>
<?php
class COURSE{
	
	  public static function getallcourse($order=array()){
		$sql = "
		SELECT *
		FROM course_list
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	} 
	
	public static function getallcourseAssociate($order=array()){
		$sql = "
		SELECT *
		FROM course_list where course_INIT LIKE 'A%'
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
	
	public static function getallcourseBachelor($order=array()){
		$sql = "
		SELECT *
		FROM course_list where course_INIT LIKE 'B%'
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
 
 	public static function getbyID($id){
		$sql = "
		SELECT *
		FROM course_list
		WHERE course_ID = $id
		";
		return SQL::find_id($sql);
	}

    public static function addcourse($info){
			$sql = "
			INSERT INTO	
				course_list
			SET
                 course_NAME = ?,
				 course_INIT = ?
			";
			$param = array(
               $info['course_NAME'],
			   $info['course_INIT']
			);
			$user_info_id = $info['AccountID'];
			$action_event = "ADD";
			$event_desc =  "MODULE: Add Program DESCRIPTION: User added Program <br>DETAILS: <br>PROGRAM NAME: ".$info['course_NAME']."<br>PROGRAM INITIAL: ".$info['course_INIT'];
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Course Successfully Added","success");
	}


	public static function update_course($info){
			$sql = "
			Update course_list
			SET
                course_NAME = ?,
				course_INIT = ?
            WHERE
                course_ID =? 
			";
			$param = array(
				$info['course_NAME'],
				$info['course_INIT'],
                $info['course_ID']
			);

			$after = array(
				"COURSE_NAME" => $info['course_NAME'],
				"COURSE_INIT" => $info['course_INIT']
			);

			$before = array(
				"COURSE_NAME" => $info['beforecourse_NAME'],
				"COURSE_INIT" => $info['beforecourse_INIT']
			);

			$user_info_id =$info['AccountID'];
			$action_event = "UPDATED";
			$arraydifffrom = array_diff($before, $after);
			$arraydiffto =array_diff($after, $before);
           	$test = array("BEFORE"=>$arraydifffrom,
           					"AFTER"=>$arraydiffto);
  	       	$result2 =preg_replace('/[{-}"]/', '', json_encode($test));

  	       if(($arraydiffto == null)||($arraydiffto == ""))
  	       {

  	       	$event_desc = "MODULE: Update Course <br>DESCRIPTION: NO CHANGES";
  	       
  	       }else
  	       {

  	       	$event_desc = "MODULE: Update Course <br>DESCRIPTION: User updated ".$result2;

  	       }

            Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Course Successfully Updated","success");
	}

	public static function enrolled_percourse($course_ID,$sem_ID){
	
		$sql = "SELECT count(*)
		FROM enrolment a, student_information b, curricullum_enrolled c, curricullum_list d,
		course_list e

		WHERE a.or_ID is not null and  a.si_ID = b.si_ID and c.si_ID = b.si_ID and c.status = 'ENROLLED' and d.curricullum_ID = c.curricullum_ID and d.course_ID = e.course_ID and e.course_ID = ? and a.sem_ID = ?";
		
			$param = array(
				$course_ID,
                $sem_ID
			);
		return SQL::find_scalar($sql,$param);
	}
	public static function enrolled_persem($sem_ID){
	
		$sql = "SELECT count(*)
		FROM enrolment

		WHERE sem_ID = ?  and or_ID is not NULL";
		
			$param = array(
                $sem_ID
			);
		return SQL::find_scalar($sql,$param);
	}
	
	public static function enrolled_persemAssociate($sem_ID){
	
		$sql = "SELECT count(*)
		FROM enrolment a, student_information b, curricullum_enrolled c, curricullum_list d,
		course_list e

		WHERE  a.or_ID is not NULL and a.si_ID = b.si_ID and c.si_ID = b.si_ID and c.status = 'ENROLLED' and d.curricullum_ID = c.curricullum_ID and d.course_ID = e.course_ID and e.course_INIT LIKE 'A%' and a.sem_ID = ?";
		
			$param = array(
                $sem_ID
			);
		return SQL::find_scalar($sql,$param);
	}
	public static function enrolled_persemBachelor($sem_ID){
	
		$sql = "SELECT count(*)
		FROM enrolment a, student_information b, curricullum_enrolled c, curricullum_list d,
		course_list e

		WHERE a.or_ID is not NULL and  a.si_ID = b.si_ID and c.si_ID = b.si_ID and c.status = 'ENROLLED' and d.curricullum_ID = c.curricullum_ID and d.course_ID = e.course_ID and e.course_INIT LIKE 'B%' and a.sem_ID = ?";
		
			$param = array(
                $sem_ID
			);
		return SQL::find_scalar($sql,$param);
	}
	
    
   public static function update_room($id,$roominfo){
			$sql = "
			Update course_list
			SET
                course_NAME =?
            WHERE
                course_ID =? 
			";
			$param = array(
				$roominfo['room_NAME'],
                $id
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("User Successfully Updated","success");
	}
    public static function getallrooms($order=array()){
		$sql = "
		SELECT *
		FROM course_list
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function view1room($id){
		$sql = "
		SELECT *
		FROM course_list
		WHERE course_ID = ?
		";
		return SQL::find_all($sql);
	}
	
	
	
	
    public static function getSingle($order=array(),$id)
	{
			$sql = "
			SELECT *
			FROM course_list
			WHERE course_ID = ?
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function delete($instructor_ID)
	{
	$sql = "
		DELETE FROM course_list where course_ID = ?
		";
		$param = array(
			array($instructor_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Instructor successfully deleted!", "success");
			href("course.php?action=view");
	}
}

?>
<?php
class CECONTROLLER{

    public static function add($data){
           $sql = "
			INSERT INTO	curricullum_enrolled
			SET
                ce_ID = ?,
                curricullum_ID =?,
                si_ID = ?,
                status = ?
			";
			$param = array(
                NULL,
                $data['curricullum_ID'],
				$data['si_ID'],
                'ENROLLED'
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Successfully Added","success");
            href("curricullum_enrolled.php?action=view");
	}

    public static function add1($data){
           $sql = "
			INSERT INTO	curricullum_enrolled
			SET
                ce_ID = ?,
                curricullum_ID =?,
                si_ID = ?,
                status = ?
			";
			$param = array(
                NULL,
                $data['curricullum_ID'],
				$data['si_ID'],
                'ENROLLED'
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Successfully Added","success");
            href("student_information.php?action=view");
	}
	
    public static function addfromadd($si_ID,$curr_ID){
           $sql = "
			INSERT INTO	curricullum_enrolled
			SET
                ce_ID = ?,
                curricullum_ID =?,
                si_ID = ?,
                status = ?
			";
			$param = array(
                NULL,
               $curr_ID,
			   $si_ID,
                'ENROLLED'
			);
			SQL::execute($sql, $param);
           # SESSION::StoreMsg("Successfully Added","success");
           # href("curricullum_enrolled.php?action=view");
	}	
	
    public static function update($id,$roominfo){
			$sql = "
			UPDATE	curricullum_enrolled
			SET
                curricullum_ID = ?,
                si_ID = ?,
                status = ?
            WHERE ce_ID = ?
			";
			$param = array(
                $roominfo['curricullum_ID'],
				$roominfo['si_ID'],
				$roominfo['status'],
                $id
                
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Successfully Updated","success");
            href("curricullum_enrolled.php?action=view");
	}


	public static function update2($id){
			$sql = "
			UPDATE	curricullum_enrolled
			SET
                status = ?
            WHERE si_ID = ?
			";
			$param = array(
                "DROPPED",
				$id
			);
			SQL::execute($sql, $param);
            #SESSION::StoreMsg("Successfully Updated","success");
            #href("curricullum_enrolled.php?action=view");


	}


    public static function enroll($id){
			$sql = "
			UPDATE	curricullum_enrolled
			SET
                status = ?
            WHERE ce_ID = ?
			";
			$param = array(
                'ENROLLED',
                $id
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Successfully Updated","success");
            href("curricullum_enrolled.php?action=view");
	} 
    public static function drop($id){
			$sql = "
			UPDATE	curricullum_enrolled
			SET
                status = ?
            WHERE ce_ID = ?
			";
			$param = array(
                'DROP',
                $id
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Successfully Updated","success");
            href("curricullum_enrolled.php?action=view");
	}
    public static function getallCurricullum($order=array()){
		$sql = "
		SELECT *
		FROM curricullum_list
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function getsingleCurricullum($order=array(),$id){
		$sql = "
		SELECT *
		FROM curricullum_list
        WHERE curricullum_ID = ?
		";
        $param= array($id);
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql,$param);
	}
    
    public static function getsingleCourse($order=array(),$id){
		$sql = "
		SELECT *
		FROM course_list
        WHERE
        course_ID = ?
		";
        $param = array($id);
        
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql,$param);
	}
    public static function getallstudent($order=array()){
		$sql = "
			SELECT a.*
			FROM    student_information a
			LEFT JOIN
				  curricullum_enrolled b
			On  a.si_ID = b.si_ID
			WHERE b.si_ID is NULL or b.status ='DROP'
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
	
	
	public static function isEnrolled($id)
	{
	$sql = "
				SELECT count(*)
				FROM    curricullum_enrolled b
				WHERE b.status ='ENROLLED' and b.si_ID = '$id'
		";
		
		return SQL::find_scalar($sql);
	}

	public static function getEnrolled($id)
	{
	$sql = "
				SELECT *
				FROM    curricullum_enrolled b
				WHERE b.status ='ENROLLED' and b.si_ID = '$id'
		";
		
		return SQL::find_id($sql);
	}
	
	
	
    public static function getallce($order=array()){
		$sql = "
		SELECT *
		FROM curricullum_enrolled
        ";
        
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function getsinglestudent($order=array(),$id){
		$sql = "
		SELECT *
		FROM student_information
        WHERE si_ID = ?
		";
        $param= array($id);
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql,$param);
	}
    public static function view1room($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}
    public static function getSingle($order=array(),$id)
	{
			$sql = "
			SELECT *
			FROM curricullum_enrolled
			WHERE ce_ID = ?
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function delete($instructor_ID)
	{
	$sql = "
		DELETE FROM curricullum_enrolled where ce_ID = ?
		";
		$param = array(
			array($instructor_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Instructor successfully deleted!", "success");
			href("curricullum_enrolled.php?action=view");
	}
    public static function getAllSemester($order=array()){
		$sql = "
		SELECT 	*
		FROM yr_sem
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{
					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}
			$x++;
			}
		return SQL::find_all($sql);
		#echo $sql;
	}
	
	
	public static function getInfo($id)
	{
	$sql = "
				SELECT *
				FROM    curricullum_enrolled b
				WHERE b.status ='ENROLLED' and b.si_ID = '$id'
		";
		
		return SQL::find_id($sql);
	}
	
	public static function getAllinfo($id)
	{
	$sql = "
				SELECT *
				FROM    curricullum_enrolled b,
						curricullum_list a,
						course_list c
				WHERE 
					b.curricullum_ID = a.curricullum_ID and
					a.course_ID = c.course_ID and
					b.status ='ENROLLED' and 
					b.si_ID = '$id'
		";
		
		return SQL::find_id($sql);
	
	
	}
	
	
	
	
	public static function getEnrolledGrade($si_ID,$subject_ID)
	{
	$sql = "
				SELECT a.*, b.is_check
				FROM    subject_enrolled a,
						subject_sched b
				WHERE 
					a.subject_sched_ID = b.subject_sched_ID and
					b.subject_ID = '$subject_ID' and
					a.si_ID = '$si_ID' and
					a.status = 1
					ORDER BY a.subject_enrolled_ID DESC
					LIMIT 1
					
					
		";
		
		return SQL::find_id($sql);
	
	
	}
	
	
	public static function getEncodedGrade($si_ID,$subject_ID)
	{
	$sql = "
				SELECT b.*
				FROM    subject_encoded b
						
				WHERE 
					
					b.subject_ID = '$subject_ID' and
					b.si_ID = '$si_ID' and
					b.status = 1
					ORDER BY b.encoded_date DESC
					LIMIT 1
					
					
		";
		
		return SQL::find_id($sql);
	
	
	}
	
	
	
	
	
}

?>
<?php
class CSCONTROLLER{

    public static function add($roominfo){
           $sql = "
			INSERT INTO	curricullum_subjects
			SET
                cs_ID = ?,
                curricullum_ID = ?,
                subject_ID = ?,
                prerequisite_subject_ID = ?,
                yr_sem_ID = ?
			";
			$param = array(
                NULL,
                $roominfo['curricullum_ID'],
				$roominfo['subject_ID'],
				$roominfo['pre_req'],
                $roominfo['yr_sem_ID']
			);
			$subjname = SUBJECTS::getID($roominfo['subject_ID']);
			$semname =SEM::getSingleSem($roominfo['yr_sem_ID']);
			$user_info_id = $roominfo['AccountID'];
			$action_event = "ADD";
			$event_desc =  "MODULE: Add Subject DESCRIPTION: User added Subject <br>DETAILS: <br>SUBJECT NAME: ".$subjname['subject_CODE']."<br>SUBJECT DESCRIPTION: ".$subjname['subject_DESCRIPTION']."<br>SEM_NAME: ".$semname['sem_NAME'];
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);



			SQL::execute($sql, $param);
            SESSION::StoreMsg("Successfully Added","success");
	}

	public static function add2($roominfo){
           $sql = "
			INSERT INTO	curricullum_subjects
			SET
                cs_ID = ?,
                curricullum_ID = ?,
                subject_ID = ?,
                prerequisite_subject_ID = ?,
                corequisite_subject_ID = ?,
                yr_sem_ID = ?
			";
			$param = array(
                NULL,
                $roominfo['curricullum_ID'],
				$roominfo['subject_ID'],
				implode(",",$roominfo['pre_req']),
				implode(",", $roominfo['co_req']),
                $roominfo['yr_sem_ID']
			);
			$subjname = SUBJECTS::getID($roominfo['subject_ID']);
			$semname =SEM::getSingleSem($roominfo['yr_sem_ID']);
			$user_info_id = $roominfo['AccountID'];
			$action_event = "ADD";			
			$event_desc =  "MODULE: Add Subject DESCRIPTION: User added Subject <br>DETAILS: <br>SUBJECT NAME: ".$subjname['subject_CODE']."<br>SUBJECT DESCRIPTION: ".$subjname['subject_DESCRIPTION']."<br>SEM_NAME: ".$semname['sem_NAME'];
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);


			SQL::execute($sql, $param);
            SESSION::StoreMsg("Successfully Added","success");
	}


    public static function update($id,$roominfo){
			$sql = "
			UPDATE	curricullum_subjects
			SET
                curricullum_ID = ?,
                subject_ID = ?,
                prerequisite_subject_ID = ?,
                yr_sem_ID = ?
            WHERE cs_ID = ?
			";
			$param = array(
                $roominfo['curricullum_ID'],
				$roominfo['subject_ID'],
				$roominfo['pre_req'],
                $roominfo['yr_sem_ID'],
                $id
                
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Successfully Updated","success");
	}

	 public static function update2($id,$roominfo){
			$sql = "
			UPDATE	curricullum_subjects
			SET
                curricullum_ID = ?,
                subject_ID = ?,
                prerequisite_subject_ID = ?,
                corequisite_subject_ID = ?,
                yr_sem_ID = ?
            WHERE cs_ID = ?
			";
			$param = array(
                $roominfo['curricullum_ID'],
				$roominfo['subject_ID'],
				#$roominfo['pre_req'],
				implode(",",$roominfo['pre_req']),
				implode(",", $roominfo['co_req']),
                $roominfo['yr_sem_ID'],
                $id
                
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Successfully Updated","success");
	}
	
	

	
	
	
	
	
	
	
	
	
	
	
    public static function getallCurricullum($order=array()){
		$sql = "
		SELECT *
		FROM curricullum_list
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function getsingleCurricullum($order=array(),$id){
		$sql = "
		SELECT *
		FROM curricullum_list
        WHERE curricullum_ID = ?
		";
        $param= array($id);
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql,$param);
	}
    
    public static function getsingleCourse($order=array(),$id){
		$sql = "
		SELECT *
		FROM course_list
        WHERE
        course_ID = ?
		";
        $param = array($id);
        
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql,$param);
	}
    public static function getallyrsem($order=array()){
		$sql = "
		SELECT *
		FROM yr_sem
        ";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function getsingleyrsem($order=array(),$id){
		$sql = "
		SELECT *
		FROM yr_sem
        WHERE yr_sem_ID = ?
        ";
        $param = array($id);
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql,$param);
	}
    public static function getallsubjects($order=array()){
		$sql = "
		SELECT *
		FROM subject_list
        ";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function getsinglesubjects($order=array(),$id){
		$sql = "
		SELECT *
		FROM subject_list
        WHERE
        subject_ID = ?
        ";
        $param = array($id);
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql,$param);
	}
    public static function getallcs($order=array()){
		$sql = "
		SELECT *
		FROM curricullum_subjects
        ";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function view1room($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}
    public static function getSingle($order=array(),$id)
	{
			$sql = "
			SELECT *
			FROM curricullum_subjects
			WHERE cs_ID = ?
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function delete($instructor_ID)
	{
	$sql = "
		DELETE FROM curricullum_subjects where cs_ID = ?
		";
		$param = array(
			array($instructor_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Subject successfully deleted!", "success");
			href($_SERVER['HTTP_REFERER']);
	}
    public static function getAllSemester($order=array()){
		$sql = "
		SELECT 	*
		FROM yr_sem
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{
					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}
			$x++;
			}
		return SQL::find_all($sql);
		#echo $sql;
	}
}

?>
<?php
class CURRICULLUM{

	public static function getSubject($id)
	{

		$sql = "SELECT * FROM subject_list where subject_ID = '$id'";
		
		return SQL::find_all($sql);
	}

	public static function getSubjectbycurr($yr_id,$curricullum_ID)
	{

		$sql = "SELECT * FROM curricullum_subjects where curricullum_ID = '$curricullum_ID'
					and yr_sem_ID = '$yr_id'";
		
		return SQL::find_all($sql);
	}



	public static function checkCurricullum_Subjects($id){
		$sql = "
		SELECT 	*
		FROM curricullum_subjects
		WHERE curricullum_ID = $id
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{
					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}
			$x++;
			}
		return SQL::find_all($sql);
	}
	


	public static function getAllSemester($order=array()){
		$sql = "
		SELECT 	*
		FROM yr_sem
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{
					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}
			$x++;
			}
		return SQL::find_all($sql);
		#echo $sql;
	}


	public static function getCurricullumName($id){
		$sql = "
		SELECT 
			*
		FROM curricullum_list
		WHERE curricullum_ID = ?
		";
		$param = array(
			array($id, PDO::PARAM_INT)
		);
		return SQL::find_id($sql, $param);
	}

	

    public static function addCurricullum($roominfo){
           $sql = "
			INSERT INTO	curricullum_list
			SET
				curricullum_ID = ?,
                curricullum_NAME = ?,
                course_ID = ?
			";
			//$roominfo['course_ID']
			$param = array(
                NULL ,
				$roominfo['curricullum_NAME'],
				$roominfo['course_ID'],
			);


			$user_info_id = $roominfo['AccountID'];
			$action_event = "ADD";
			$event_desc =  "MODULE: Add Curriculum DESCRIPTION: User added Curriculum <br>DETAILS: <br>CURRICULUM NAME: ".$roominfo['curricullum_NAME']."<br>COURSE ID: ".$roominfo['course_ID'];
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);


			SQL::execute($sql, $param);
            SESSION::StoreMsg("Curricullum successfully added","success");
			href("curricullum.php?action=view");
	}
    public static function update_curriculum($id,$curricullum){
			$sql = "
			Update curricullum_list
			SET
                curricullum_NAME = ?,
                course_ID = ?
            WHERE
                curricullum_ID = ?
			";
			$param = array(
				$curricullum['curricullum_NAME'],
				$curricullum['course_ID'],
                $id
			);
			///////////////////////////////// Audit Trail Start (Jeelbert) ///////////////////////////
			
			$getCurricullumName = CURRICULLUM:: getID($curricullum['course_ID']);
            
			$data = array(
					'Curriculum Name'=>$curricullum['c_name'],
					'Course Name'=>$curricullum['course_n']
			);
			$data1 = array(

					'Curriculum Name'=>$curricullum['curricullum_NAME'],
					'Course Name'=>$getCurricullumName['course_NAME']
			);
			$before = array_diff_assoc($data, $data1);
			$after = array_diff_assoc($data1, $data);
			
			$b_fore = preg_replace('/[{-}"]/', '', json_encode($before));
			$a_fter = preg_replace('/[{-}"]/', '', json_encode($after));
			$user_info_id = $curricullum['acct_id'];

			$action_event = "Update";
			if($after == null || $after == ""){
				$event_desc = "Module: Academic Affairs / Encoding of Curriculum. Description: No Changes in Curriculum ID ".$id;
			}else{
			$event_desc =  "MODULE: Academic Affairs / Encoding of Curriculum. DESCRIPTION: Updated Curriculum with an ID of ".$id.".  BEFORE : ".$b_fore.' AFTER: '.$a_fter;
			}
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);

			///////////////////////////////// Audit Trail End (Jeelbert) /////////////////////////////
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Curricullum successfully updated","success");


	}

	public static function viewCurricullum($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}

	public static function getSingle($order=array(),$id)
	{
			$sql = "
			SELECT *
			FROM curricullum_list
			WHERE curricullum_ID = ?
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}

	public static function getAllCourseName($order=array()){
		$sql = "
		SELECT 	*
		FROM course_list
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{
					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}
			$x++;
			}
		return SQL::find_all($sql);
		#echo $sql;
	}

    public static function getAllCurricullum($order=array()){
		$sql = "
		SELECT 	*
		FROM curricullum_list
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{

					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}

			
				
			$x++;
			}
		

		
		return SQL::find_all($sql);
		#echo $sql;
	}


	#2018-10-23
	public static function getCurricullumByCourseID($course_ID, $order=array()){
		$sql = "
		SELECT 	*
		FROM curricullum_list WHERE course_ID = '$course_ID'
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{

					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}

			
				
			$x++;
			}
		

		
		return SQL::find_all($sql);
		#echo $sql;
	}

    public static function getID($course_ID){
		$sql = "
		SELECT 
			*
		FROM course_list
		WHERE course_ID = ?
		";
		$param = array(
			array($course_ID, PDO::PARAM_INT)
		);
		return SQL::find_id($sql, $param);
	}

    public static function delete($curricullum_ID)
	{
	$sql = "
		DELETE FROM curricullum_list where curricullum_ID = ?
		";
		$param = array(
			array($curricullum_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Curricullum successfully deleted!", "success");
			href("curricullum.php?action=view");
	}
}

?>
<?php
class INSTRUCTORS{

	#JRML:START
	public static function fetchInstructorByID($order=array(),$id){
			$sql = "
			SELECT *
			FROM account_info
			WHERE account_ID = ? AND access_ID = 8
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
	#JRML:END

	public static function addInstructor($userinfo)
	{
			$sql = "
			INSERT INTO	instructor
			SET
				instructor_NAME = ?,
				instructor_MNAME = ?,
				instructor_LNAME = ?
				
			";
			$param = array(
				$userinfo['instructor_FNAME'],
				$userinfo['instructor_MNAME'],
				$userinfo['instructor_LNAME']
			);
			
			SQL::execute($sql, $param);
			
			$instructor_ID = SQL::last_ID();
			$account_ID = USERS::addUserInstructor($userinfo);
			
			INSTRUCTORS::addInstructorRoles($instructor_ID,$account_ID);
			
			SESSION::StoreMsg("Instructor successfully added!", "success");
			#href("users.php");
	}

	//20180817
	//@kmsj13
	public static function getInstructorDepartment($id){
			$sql = "
			SELECT d.department_NAME 
			FROM 
				user_instructor ui
			LEFT JOIN account_info ai on ai.account_ID = ui.account_ID
			LEFT JOIN department_list d on d.department_ID = ai.department_ID
			WHERE
				instructor_ID = ?
				
				
			";
			$param = array($id);
		
			return SQL::find_scalar($sql,$param);
	}

	//20180817
	//@kmsj13
	public static function getInstructorDetails($id)
	{
		
			$sql = "
			SELECT ins.*,ai.*  
			FROM 
				instructor ins 

			LEFT JOIN user_instructor ui on ins.instructor_ID = ui.instructor_ID
			LEFT JOIN account_info ai on ai.account_ID = ui.account_ID
			LEFT JOIN department_list d on d.department_ID = ai.department_ID
			WHERE
				ins.instructor_ID = ?
				
				
			";
			$param = array($id);
			

		$param = array($id);
		
		
		
		return SQL::find_id($sql,$param);
		
	}

	public static function addInstructorRoles($ins,$acc){
			$sql = "
			INSERT INTO	user_instructor
			SET
				instructor_ID = ?,
				account_ID = ?
				
			";
			$param = array(
				$ins,
				$acc
			);
			
			SQL::execute($sql, $param);
	}

    public static function update_Instructor($id,$userinfo){
			$sql = "
			UPDATE instructor
			SET
                instructor_NAME = ?
                WHERE instructor_ID = ?
			";
			$param = array(
				$userinfo['instructor_NAME'],
                $id
			);
			SQL::execute($sql, $param);
			SESSION::StoreMsg("Instructor successfully updated!", "success");
			
			

					#href("users.php");

				
	}

	public static function update_Instructor2($id,$userinfo){
			if(isset($userinfo['instructor_UPASS']) && $userinfo['instructor_UPASS'] != "")
			{
				#SESSION::StoreMsg("Faculty successfully updated1!", "success");
				$sql = "
				UPDATE account_info
				SET
	                account_UNAME = ?,
	                account_FNAME = ?,
	                account_MNAME = ?,
	                account_LNAME = ?,
	                department_ID = ?,
	                account_PASS = ?
	                WHERE account_ID = ?
				";
				$param = array(
					$userinfo['instructor_UNAME'],
					$userinfo['instructor_FNAME'],
					$userinfo['instructor_MNAME'],
					$userinfo['instructor_LNAME'],
					$userinfo['department_ID'],
					base64_encode($userinfo['instructor_UPASS']),
					$userinfo['account_ID'],
				);
				SQL::execute($sql, $param);	
				SESSION::StoreMsg("Faculty successfully updated!", "success");
			}else{
				$sql = "
				UPDATE account_info
				SET
	                account_UNAME = ?,
	                account_FNAME = ?,
	                account_MNAME = ?,
	                account_LNAME = ?,
	                department_ID = ?
	                WHERE account_ID = ?
				";
				$param = array(
					$userinfo['instructor_UNAME'],
					$userinfo['instructor_FNAME'],
					$userinfo['instructor_MNAME'],
					$userinfo['instructor_LNAME'],
					$userinfo['department_ID'],
					$userinfo['account_ID'],
				);
				SQL::execute($sql, $param);	
				SESSION::StoreMsg("Faculty successfully updated!", "success");
			}
			
			// $sql = "
			// UPDATE instructor
			// SET
   //              instructor_NAME = ?
   //              WHERE instructor_ID = ?
			// ";
			// $param = array(
			// 	$userinfo['instructor_NAME'],
   //              $id
			// );
			// SQL::execute($sql, $param);
			// SESSION::StoreMsg("Faculty successfully updated!", "success");
			
			

					#href("users.php");

				
	}


	
	
    public static function getSingle($order=array(),$id)
	{
		
			$sql = "
			SELECT *
			FROM instructor
			WHERE instructor_ID = ?
			";
			

		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}


	public static function getInstructorIDbyAccountID($id)
	{
		
			$sql = "
			SELECT *
			FROM user_instructor ui
			WHERE ui.account_ID = ?
			";
			
		$param = array($id);
		
		return SQL::find_id($sql,$param);
		
	}

    public static function getSingleSAI($order=array(),$id)
	{
		
			$sql = "
			SELECT *
			FROM student_additional_info
			WHERE si_ID = ?
			";
			

		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function getSingleSRP($order=array(),$id)
	{
		
			$sql = "
			SELECT *
			FROM student_requirements_passed
			WHERE req_ID = ?
			";
			

		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    #MOJA END

	public static function getAllInstructors($order=array()){
		$sql = "
		SELECT *
		FROM account_info
		WHERE access_ID = '8'
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}

    #KYL
    public static function getAllInstructorsbyStatus($order=array()){
		$sql = "
		SELECT *
		FROM instructor
		WHERE status = '1'
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
	
    public static function viewSubjects($order=array()){
		$sql = "
		SELECT *
		FROM subject_list
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}

	public static function view1Student($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}

	public static function delete($instructor_ID)
	{
	$sql = "
		DELETE FROM instructor where instructor_ID = ?
		";
		$param = array(
			array($instructor_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Instructor successfully deleted!", "success");
			href("instructors.php?action=view");
	}
	
	    public static function getSingle1($id)
	{
		
			$sql = "
			SELECT *
			FROM instructor
			WHERE instructor_ID = ?
			";
			

		$param = array($id);
	
		return SQL::find_id($sql,$param);
		
	}
	
	
	
	
	
	
	
}
?>
<?php
class PARTICULARS{

    public static function addroom($roominfo){
			$sql = "
			INSERT INTO	acc_particular
			SET
                particular_ID = ?,
                particular_NAME = ?
			";
			$param = array(
                NULL ,
				$roominfo['room_NAME']
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("User Successfully Added","success");
	}
    public static function update_room($id,$roominfo){
			$sql = "
			Update acc_particular
			SET
                particular_NAME =?
            WHERE
                particular_ID =? 
			";
			$param = array(
				$roominfo['room_NAME'],
                $id
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("User Successfully Updated","success");
	}
    public static function getallrooms($order=array()){
		$sql = "
		SELECT *
		FROM acc_particular
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function view1room($id){
		$sql = "
		SELECT *
		FROM acc_particular
		WHERE particular_ID = ?
		";
		return SQL::find_all($sql);
	}
	
	 public static function getSingleParticular($id){
		$sql = "
		SELECT *
		FROM acc_particular
		WHERE particular_ID = '$id'
		";
		return SQL::find_id($sql);
	}
	
	
    public static function getSingle($order=array(),$id)
	{
			$sql = "
			SELECT *
			FROM acc_particular
			WHERE particular_ID = ?
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function delete($instructor_ID)
	{
	$sql = "
		DELETE FROM acc_particular where particular_ID = ?
		";
		$param = array(
			array($instructor_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Instructor successfully deleted!", "success");
			href("course.php?action=view");
	}
}

?>
<?php
class PER_COURSE{


	public static function getSubject($id)
	{

		$sql = "SELECT * FROM course_list where course_ID = '$id'";
		
		return SQL::find_all($sql);
	}

	public static function getCoursebycurr($course_id)
	{

		$sql = "SELECT b.si_LNAME, b.si_FNAME, b.si_MNAME, c.curricullum_NAME, d.course_NAME  from curricullum_enrolled a, student_information b, curricullum_list c, course_list d
	WHERE a.si_ID = b.si_ID
		AND
			a.curricullum_ID = c.curricullum_ID
		AND
			c.course_ID = d.course_ID
		AND
			d.course_ID = '$course_id'";
		
		return SQL::find_all($sql);
	}



	public static function checkCurricullum_Subjects($id){
		$sql = "
		SELECT 	*
		FROM curricullum_subjects
		WHERE curricullum_ID = $id
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{
					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}
			$x++;
			}
		return SQL::find_all($sql);
	}
	


	public static function getAllCourse($order=array()){
		$sql = "
		SELECT 	*
		FROM course_list
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{
					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}
			$x++;
			}
		return SQL::find_all($sql);
		#echo $sql;
	}


	public static function getCurricullumName($id){
		$sql = "
		SELECT 
			*
		FROM curricullum_list
		WHERE curricullum_ID = ?
		";
		$param = array(
			array($id, PDO::PARAM_INT)
		);
		return SQL::find_id($sql, $param);
	}

	

    public static function addCurricullum($roominfo){
           $sql = "
			INSERT INTO	curricullum_list
			SET
				curricullum_ID = ?,
                curricullum_NAME = ?,
                course_ID = ?
			";
			//$roominfo['course_ID'],
			$param = array(
                NULL ,
				$roominfo['curricullum_NAME'],
				"69",
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Curricullum successfully added","success");
			href("curricullum.php?action=view");
	}
    public static function update_curriculum($id,$curricullum){
			$sql = "
			Update curricullum_list
			SET
                curricullum_NAME = ?,
                course_ID = ?
            WHERE
                curricullum_ID = ?
			";
			$param = array(
				$curricullum['curricullum_NAME'],
				$curricullum['course_ID'],
                $id
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Curricullum successfully updated","success");
	}

	public static function viewCurricullum($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}

	public static function getSingle($order=array(),$id)
	{
			$sql = "
			SELECT *
			FROM curricullum_list
			WHERE curricullum_ID = ?
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}

	public static function getAllCourseName($order=array()){
		$sql = "
		SELECT 	*
		FROM course_list
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{
					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}
			$x++;
			}
		return SQL::find_all($sql);
		#echo $sql;
	}

    public static function getAllCurricullum($order=array()){
		$sql = "
		SELECT 	*
		FROM curricullum_list
		";

		if(sizeof($order) > 0)
		{
			$sql .= " ORDER BY ";

		}

		$x=1;
			foreach($order as $key => $value)
			{

					if($x == 1)
					{
							$sql .= "{$key} {$value}";

					}	else{
						$sql .= ", {$key} {$value}";
					}

			
				
			$x++;
			}
		

		
		return SQL::find_all($sql);
		#echo $sql;
	}

    public static function getID($course_ID){
		$sql = "
		SELECT 
			*
		FROM course_list
		WHERE course_ID = ?
		";
		$param = array(
			array($course_ID, PDO::PARAM_INT)
		);
		return SQL::find_id($sql, $param);
	}

    public static function delete($curricullum_ID)
	{
	$sql = "
		DELETE FROM curricullum_list where curricullum_ID = ?
		";
		$param = array(
			array($curricullum_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Curricullum successfully deleted!", "success");
			href("curricullum.php?action=view");
	}
}

?>
<?php
class REPORT{
	
}

?>
<?php
class ROOM{

    public static function addroom($roominfo){
           $sql = "
			INSERT INTO	room_list
			SET
                room_NAME = ?,
				room_SIZE = ?
			";
			$param = array(
				$roominfo['room_NAME'],
				$roominfo['room_SIZE']
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Room Successfully Added","success");
	}
    public static function update_room($id,$roominfo){
			$sql = "
			Update room_list
			SET
                room_NAME = ?,
				room_SIZE = ?
            WHERE
                room_ID = ?
				
			";
			$param = array(
				$roominfo['room_NAME'],
				$roominfo['room_SIZE'],
                $id
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Room Successfully Updated!","success");
	}
    public static function getallrooms($order=array()){
		$sql = "
		SELECT *
		FROM room_list
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function view1room($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}
 	public static function getID($id)
	{
			$sql = "
			SELECT *
			FROM room_list
			WHERE room_ID = ?
			";
		$param = array($id);
		
		
		return SQL::find_id($sql,$param);
		
	}	
	
	
	
	
	
	
	
	
	
	
	
	
    public static function getSingle($order=array(),$id)
	{
			$sql = "
			SELECT *
			FROM room_list
			WHERE room_ID = ?
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function delete($instructor_ID)
	{
	$sql = "
		DELETE FROM room_list where room_ID = ?
		";
		$param = array(
			array($instructor_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Instructor successfully deleted!", "success");
			href("room.php?action=view");
	}
}

?>
<?php
class SEM{
 	public static function getallsems($order=array()){
		$sql = "
		SELECT *
		FROM semester
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
	
	public static function getallsemsofstudent($id){
		$sql = "
		SELECT
			DISTINCT(d.sem_ID),d.sem_NAME
			FROM 
				subject_enrolled a,
				subject_sched b,
				semester_section c,
				semester d
				
			WHERE
				a.subject_sched_ID = b.subject_sched_ID and
				b.semester_section_ID = c.semester_section_ID and
				c.sem_ID = d.sem_ID and
				a.si_ID = '$id'
			ORDER by d.sem_NAME DESC
			
		";
		return SQL::find_all($sql);
	}
	
	public static function getallsemsin_accr($id){
		$sql = "
		SELECT
			DISTINCT(a.sem_ID)
			FROM 
				acc_records a
				
			WHERE
				a.si_ID = '$id' 
			ORDER by a.id DESC
			
		";
		return SQL::find_all($sql);
	}	
	
	public static function getallsemsin_enrol($id){
		$sql = "
		SELECT
			DISTINCT(a.sem_ID), po_ID
			FROM 
				enrolment a
				
			WHERE
				a.si_ID = '$id' 
			ORDER by a.sem_ID DESC
			
		";
		return SQL::find_all($sql);
	}	
	#LIM:START
	public static function getPObyID($order=array(),$id){
		$sql = "
		SELECT po_ID
		FROM enrolment
		WHERE si_ID = '$id'
		";
		$param = array($id);
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_id($sql,$param);
	}
	public static function getPONamebyID($order=array(),$id){
		$sql = "
		SELECT po_NAME
		FROM payment_options
		WHERE po_ID = '$id'
		";
		$param = array($id);
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_id($sql,$param);
	}
	#LIM:END
	public static function getallsemsactive($order=array()){
		$sql = "
		SELECT *
		FROM semester
		WHERE sem_STATUS = 1 ";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
	
	public static function getallsemsencoding($order=array()){
		$sql = "
		SELECT *
		FROM semester
		WHERE sem_ENCODING = 1 ";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}	
	
	public static function getallsemsloading($order=array()){
		$sql = "
		SELECT *
		FROM semester
		WHERE sem_LOADING = 1 ";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
	
	public static function isSemEncoding($sem_ID){
		
		$sql = "
		SELECT count(*)
		FROM semester
		WHERE sem_ENCODING = 1 and sem_ID = $sem_ID";
		return SQL::find_scalar($sql);
	}
	
	public static function isSemLoading($sem_ID){
		
		$sql = "
		SELECT count(*)
		FROM semester
		WHERE sem_LOADING = 1 and sem_ID = $sem_ID";
		return SQL::find_scalar($sql);
	}
	
	public static function getallsemsenrollment($order=array()){
		$sql = "
		SELECT *
		FROM semester
		WHERE sem_ENROLMENT = 1 ";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}	
	
	public static function getSingleSem($id){
			$sql = "
			SELECT *
			FROM semester
			WHERE sem_ID = ?
			";
		$param = array($id);
		
		
		
		return SQL::find_id($sql,$param);
		
	}	
	
	public static function getactivesem(){
		$sql = "
		SELECT *
		FROM semester
		WHERE sem_ENROLMENT = 1 ";
		
		return SQL::find_id($sql);
	}
	
	public static function insert($data){
           $sql = "
			INSERT INTO	semester
			SET
                sem_NAME = ?,
                sem_STATUS = ?,
				sem_ENROLMENT = ?,
				sem_ENCODING = ?,
				sem_LOADING = ?
				
			";
			$param = array(
				$data['sem_NAME'],
				$data['sem_STATUS'],
				$data['sem_ENROLMENT'],
				$data['sem_ENCODING'],
				$data['sem_LOADING']
			);
			$semstatus =  ($data['sem_STATUS'] == "1" ? "Active" : "Inactive");
			$enrolmentstatus = ($data['sem_ENROLMENT'] == "1" ? "Active" : "Inactive");
			$encodingstatus = ($data['sem_ENCODING'] == "1" ? "Active" : "Inactive");
			$loadingstatus = ($data['sem_LOADING'] == "1" ? "Active" : "Inactive");
			$user_info_id = $data['AccountID'];
			$action_event = "ADD";
			$event_desc =  "MODULE: Add Semester DESCRIPTION: User added Semester <br>DETAILS: <br>SEMESTER NAME: ".$data['sem_NAME']."<br>SEMESTER STATUS: ".$semstatus."<br>SEMESTER ENROLMENT: ".$enrolmentstatus."<br>ENCODING OF GRADES: ".$encodingstatus."<br>SHOW TEACHERS LOADING: ".$loadingstatus;
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Semester Added!","success");
	}	

	public static function update($data){
           $sql = "
			update	semester
			SET
                sem_NAME = ?,
                sem_STATUS = ?,
				sem_ENROLMENT = ?,
				sem_ENCODING = ?,
				sem_LOADING = ?
			WHERE
				sem_ID = ?
				
			";
			$param = array(
				$data['sem_NAME'],
				$data['sem_STATUS'],
				$data['sem_ENROLMENT'],
				$data['sem_ENCODING'],
				$data['sem_LOADING'],
				$data['sem_ID']
			);

			$after = array(
							'SEM_NAME' =>  $data['sem_NAME'],
							'SEM_STATUS' => ($data['sem_STATUS'] == "1" ? "Active" : "Inactive"),
							'SEM_ENROLMENT' => ($data['sem_ENROLMENT'] == "1" ? "Active" : "Inactive"),
							'SEM_ENCODING' => ($data['sem_ENCODING'] == "1" ? "Active" : "Inactive"),
							'SEM_LOADING' => ($data['sem_LOADING'] == "1" ? "Active" : "Inactive")
							);
			$before = array(							
							'SEM_NAME' =>  $data['beforesem_NAME'],
							'SEM_STATUS' => ($data['beforesem_STATUS'] == "1" ? "Active" : "Inactive"),
							'SEM_ENROLMENT' => ($data['beforesem_ENROLMENT'] == "1" ? "Active" : "Inactive"),
							'SEM_ENCODING' => ($data['beforesem_ENCODING'] == "1" ? "Active" : "Inactive"),
							'SEM_LOADING' => ($data['beforesem_LOADING'] == "1" ? "Active" : "Inactive")
							);
			$arraydiffto = array_diff($after, $before);
            $arraydifffrom= array_diff($before, $after);
           	$user_info_id = $data['AccountID'];
           	$action_event = "UPDATED";
           	$test = array("BEFORE"=>$arraydifffrom,
           					"AFTER"=>$arraydiffto);
  	       		$result2 =preg_replace('/[{-}"]/', '', json_encode($test));

  	       if(($arraydiffto == null)||($arraydiffto == ""))
  	       {

  	       	$event_desc = "MODULE: Update Semester <br>DESCRIPTION: NO CHANGES";
  	       
  	       }else
  	       {

  	       	$event_desc = "MODULE: Update Semester <br>DESCRIPTION: User updated ".$result2;

  	       }


 	  	    Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Semester Update!","success");
	}		
	
	#END#
	public static function addsemsec($semsecinfo){
           $sql = "
			INSERT INTO	semester_section
			SET
                sem_ID = ?,
                course_ID = ?,
				semester_section_NAME = ?
			";
			$param = array(
				$semsecinfo['sem_ID'],
				$semsecinfo['course_ID'],
				$semsecinfo['semester_section_NAME']
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Semester Section Added!","success");
	}

    public static function update_room($id,$roominfo){
			$sql = "
			Update room_list
			SET
                room_NAME = ?
            WHERE
                room_ID = ?
			";
			$param = array(
				$roominfo['room_NAME'],
                $id
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("User Successfully Updated","success");
	}
   
    public static function view1room($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}
    public static function getSingle($order=array(),$id){
			$sql = "
			SELECT *
			FROM room_list
			WHERE room_ID = ?
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}

    public static function delete($instructor_ID){
		$sql = "
		DELETE FROM room_list where room_ID = ?
		";
		$param = array(
			array($instructor_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Instructor successfully deleted!", "success");
			href("room.php?action=view");
	}
	
	public static function allfacultypersem($sem_ID){
		$sql = "
		SELECT DISTINCT(a.instructor_ID)
		FROM
		subject_sched a, semester_section b, semester c
		WHERE
		a.semester_section_ID = b.semester_section_ID and
		b.sem_ID = c.sem_ID and c.sem_ID = ? ";
		
		
		$param = array(
			$sem_ID
		);
		
		return SQL::find_all($sql,$param);
	}		
	
	public static function numofsubjpersemperfac($sem_ID,$fac_ID){
		$sql = "
		SELECT COUNT(a.is_check)
		FROM
		subject_sched a, semester_section b, semester c
		WHERE
		a.semester_section_ID = b.semester_section_ID and
		b.sem_ID = c.sem_ID and c.sem_ID = ? and a.instructor_ID = ?";
		
		
		$param = array(
			$sem_ID, $fac_ID
		);
		
		return SQL::find_scalar($sql,$param);
	}		

	public static function numofsubjpersemperroom($sem_ID,$r_ID){
		$sql = "
		SELECT COUNT(*)
		FROM
		subject_sched a, semester_section b, semester c
		WHERE
		a.semester_section_ID = b.semester_section_ID and
		b.sem_ID = c.sem_ID and c.sem_ID = ? and a.room_ID = ?";
		
		$param = array(
			$sem_ID, $r_ID
		);

		return SQL::find_scalar($sql,$param);
	}
	
	public static function numofchksubjpersemperfac($sem_ID,$fac_ID){
		$sql = "
		SELECT SUM(a.is_check)
		FROM
		subject_sched a, semester_section b, semester c
		WHERE
		a.semester_section_ID = b.semester_section_ID and
		b.sem_ID = c.sem_ID and c.sem_ID = ? and a.instructor_ID = ?";
		
		
		$param = array(
			$sem_ID, $fac_ID
		);
		
		return SQL::find_scalar($sql,$param);
	}		
}

?>
<?php
class SEMSECTION{

    public static function addsemsec($semsecinfo){
           $sql = "
			INSERT INTO	semester_section
			SET
                sem_ID = ?,
                course_ID = ?,
				semester_section_NAME = ?
			";
			//$semsecinfo['semester_section_NAME']
			$param = array(
				$semsecinfo['sem_ID'],
				$semsecinfo['course_ID'],
				$semsecinfo['semester_section_NAME']
			);

			$user_info_id = $semsecinfo['AccountID'];
			$action_event = "ADD";
			$event_desc =  "MODULE: Add Semester Section DESCRIPTION: User Added SEMESTER SECTION <br>DETAILS: <br>SEMESTER ID: ".$semsecinfo['sem_ID']."<br>COURSE_ID: ".$semsecinfo['course_ID']."<br> SEMESTER SECTION NAME: ".$semsecinfo['semester_section_NAME'];
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);

			SQL::execute($sql, $param);
            SESSION::StoreMsg("Semester Section Added!","success");
	}

    public static function updatesemsec($id,$semsecc){
	#echo $id;
	#print_r($semsecc);
	
			$sql = "
			Update semester_section
			SET
				course_ID = ?,
                sem_ID = ?,
                semester_section_NAME = ?
            WHERE
                semester_section_ID = ?
			";
			$param = array(
				$semsecc['course_ID'],
				$semsecc['sem_ID'],
				$semsecc['semester_section_NAME'],
                $id
			);
			$after = array(
				'COURSE_ID' => $semsecc['course_ID'],
				'SEM_ID' => $semsecc['sem_ID'],
				'SECTION_NAME' => $semsecc['semester_section_NAME']
			);
			$before = array(
				'COURSE_ID' => $semsecc['beforecourse_ID'],
				'SEM_ID' => $semsecc['beforesem_ID'],
				'SECTION_NAME' => $semsecc['beforesemester_section_NAME']
			);
			$arraydiffto = array_diff($after, $before);
            $arraydifffrom= array_diff($before, $after);
           	$user_info_id = $semsecc['AccountID'];
           	$action_event = "UPDATED";
           	$test = array("BEFORE"=>$arraydifffrom,
           					"AFTER"=>$arraydiffto);
  	       		$result2 =preg_replace('/[{-}"]/', '', json_encode($test));

  	       if(($arraydiffto == null)||($arraydiffto == ""))
  	       {

  	       	$event_desc = "MODULE: Update Semester Section <br>DESCRIPTION: NO CHANGES";
  	       
  	       }else
  	       {

  	       	$event_desc = "MODULE: Update Semester Section <br>DESCRIPTION: User updated ".$result2;

  	       }
  	       	Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
			#print_r($sql);
			#print_r($param);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Semester section Successfully Updated","success");
	}
	
    public static function getAll($order=array())
	{
			$sql = "
			SELECT *
			FROM semester_section
			";
		$param = array();
		
		$x = 1;
		foreach($order as $key => $value){
		
		
		
			if($x ==  1) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
			$x++;
		}
		
		return SQL::find_all($sql,$param);
		
	}
	
	public static function getAll2($id,$order=array())
	{
			$sql = "
			SELECT *
			FROM semester_section where sem_ID = '$id'
			";
		$param = array();
		
		$x = 1;
		foreach($order as $key => $value){
		
		
		
			if($x ==  1) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
			$x++;
		}
		
		return SQL::find_all($sql,$param);
		
	}


	public static function getAll3($order=array())
	{
			$sql = "
			SELECT *
			FROM semester_section
			";
		$param = array();
		
		$x = 1;
		foreach($order as $key => $value){
		
		
		
			if($x ==  1) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
			$x++;
		}

		$sql .= " LIMIT 30 ";
		
		return SQL::find_all($sql,$param);
		
	}

	public static function geThis($id)
	{
			$sql = "
			SELECT *
			FROM semester_section
			where semester_section_ID = '$id'
			";
		$param = array();
		
		
		
		return SQL::find_id($sql,$param);
		
	}
	public static function getAllSemName($order=array())
	{
			$sql = "
			SELECT *
			FROM semester
			";
		$param = array();
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql,$param);
	}
	public static function getAllCourseName($order=array())
	{
			$sql = "
			SELECT *
			FROM course_list
			";
		$param = array();
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql,$param);
	}
	public static function getAllSecName($order=array())
	{
			$sql = "
			SELECT *
			FROM course_list
			";
		$param = array();
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql,$param);
	}
	public static function getAllSemName1($id)
	{
			$sql = "
			SELECT *
			FROM semester
			where sem_ID = '$id'
			";
		$param = array();
		return SQL::find_id($sql,$param);
	}
	
	
	public static function getAllActive($order=array())
	{
			$sql = "
			SELECT *
			FROM semester_section a, semester b
			WHERE a.sem_ID = b.sem_ID and b.sem_ENROLMENT = 1
			";
		$param = array();
		$x = 1;
		foreach($order as $key => $value){
		
		
		
			if($x ==  1) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
			$x++;
		}
		
		return SQL::find_all($sql,$param);
		
	}
	
	public static function getAllActivePerSem($order=array(),$sem_ID)
	{
			$sql = "
			SELECT *
			FROM semester_section a, semester b
			WHERE a.sem_ID = b.sem_ID and b.sem_ENROLMENT = 1 and a.sem_ID = '$sem_ID'
			";
		$param = array();
		$x = 1;
		foreach($order as $key => $value){
		
		
		
			if($x ==  1) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
			$x++;
		}
		
		return SQL::find_all($sql,$param);
		
	}
	

	public static function getAllbyID($order=array(),$id)
	{
			$sql = "
			SELECT *
			FROM semester_section a, semester b
			WHERE a.sem_ID = b.sem_ID and b.sem_ID = ?
			";
		$param = array(
		$id
		);
		$x = 1;
		foreach($order as $key => $value){
		
		
		
			if($x ==  1) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
			$x++;
		}
		
		return SQL::find_all($sql,$param);
		
	}



	
	
	
	
	public static function getID($id)
	{
			$sql = "
			SELECT *
			FROM semester_section
			where semester_section_ID = '$id'
			";
		$param = array();
		
		
		
		return SQL::find_id($sql,$param);
		
	}
	
	
  public static function getSEM($id)
	{
			$sql = "
			SELECT *
			FROM semester
			where sem_ID = '$id'
			";
		$param = array();
		
		
		
		return SQL::find_id($sql,$param);
		
	}
	
	#END#
	
	
	
	
    public static function update_room($id,$roominfo){
			$sql = "
			Update room_list
			SET
                room_NAME = ?
            WHERE
                room_ID = ?
			";
			$param = array(
				$roominfo['room_NAME'],
                $id
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("User Successfully Updated","success");
	}
    public static function getallrooms($order=array()){
		$sql = "
		SELECT *
		FROM room_list
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function view1room($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}
    public static function getSingle($order=array(),$id)
	{
			$sql = "
			SELECT *
			FROM room_list
			WHERE room_ID = ?
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function delete($instructor_ID)
	{
	$sql = "
		DELETE FROM room_list where room_ID = ?
		";
		$param = array(
			array($instructor_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Instructor successfully deleted!", "success");
			href("room.php?action=view");
	}
}

?>
<?php
class SUBJECTS{
public static function addSubject($userinfo){
			$sql = "
			INSERT INTO	subject_list
			SET
                subject_ID = ?,
				subject_CODE = ?,
                subject_DESCRIPTION = ?,
                LEC_UNIT = ?,
				LAB_UNIT = ?,
				TOTAL_UNIT = ?
			";
			$param = array(
                NULL ,
				$userinfo['subject_CODE'],
				$userinfo['subject_DESCRIPTION'],
				$userinfo['LEC_UNIT'],
				$userinfo['LAB_UNIT'],				
				$userinfo['TOTAL_UNIT']				
			);

			$user_info_id = $userinfo['AccountID'];
			$action_event = "ADD";
			$event_desc =  "MODULE: Add Course DESCRIPTION: User added Course <br>DETAILS: <br>COURSE CODE: ".$userinfo['subject_CODE']."<br>COURSE TITLE: ".$userinfo['subject_DESCRIPTION']."<br>LEC HOURS: ".$userinfo['LEC_UNIT']."<br>LEC HOURS: ".$userinfo['LAB_UNIT']."<br>TOTAL UNIT: ".$userinfo['TOTAL_UNIT'];
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);



			SQL::execute($sql, $param);
			SESSION::StoreMsg("Subject successfully added!", "success");
			#href("users.php");			
	}
	
	  public static function update_Subject($id,$userinfo){
			$sql = "
			UPDATE	subject_list
			SET
                
				subject_CODE = ?,
                subject_DESCRIPTION = ?,
                LEC_UNIT = ?,
				LAB_UNIT = ?,
				TOTAL_UNIT = ?
                WHERE subject_ID = ?
			";
			$param = array(
				$userinfo['subject_CODE'],
				$userinfo['subject_DESCRIPTION'],
				$userinfo['LEC_UNIT'],
				$userinfo['LAB_UNIT'],
				$userinfo['TOTAL_UNIT'],
                $id
			);

			$after = array(
				"SUBJECTCODE" => $userinfo['subject_CODE'],
				"SUBJECTDESCRIPTION" => $userinfo['subject_DESCRIPTION'],
				"LEC_HOURS" => $userinfo['LEC_UNIT'],
				"LAB HOURS" => $userinfo['LAB_UNIT'],
				"TOTAL_UNIT" => $userinfo['TOTAL_UNIT']);

			$before = array(
				"SUBJECTCODE" => $userinfo['beforesubject_CODE'],
				"SUBJECTDESCRIPTION" => $userinfo['beforesubject_DESCRIPTION'],
				"LEC_HOURS" => $userinfo['beforeLEC_UNIT'],
				"LAB HOURS" => $userinfo['beforeLAB_UNIT'],
				"TOTAL_UNIT" => $userinfo['beforeTOTAL_UNIT']);

			$arraydiffto = array_diff($after, $before);
            $arraydifffrom= array_diff($before, $after);
           	$user_info_id = $userinfo['AccountID'];
			$action_event = "UPDATED";
           	$test = array("BEFORE"=>$arraydifffrom,
           					"AFTER"=>$arraydiffto);
  	       		$result2 =preg_replace('/[{-}"]/', '', json_encode($test));

  	       if(($arraydiffto == null)||($arraydiffto == ""))
  	       {

  	       	$event_desc = "MODULE: Update Subject <br>DESCRIPTION: NO CHANGES";
  	       
  	       }else
  	       {

  	       	$event_desc = "MODULE: Update Subject <br>DESCRIPTION: User updated ".$result2;

  	       }

			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
			SQL::execute($sql, $param);
			SESSION::StoreMsg("Subject successfully updated!", "success");
			
			

					#href("users.php");

				
	}
	
	public static function getID($id)
	{
		
			$sql = "
			SELECT *
			FROM subject_list
			WHERE subject_ID = ?
			";
			

		$param = array($id);
		
		
		
		return SQL::find_id($sql,$param);
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
    public static function getSingle($order=array(),$id)
	{
		
			$sql = "
			SELECT *
			FROM subject_list
			WHERE subject_ID = ?
			";
			

		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}

    #KYL
    public static function getAllSubjects($order=array()){
		$sql = "
		SELECT *
		FROM subject_list
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
	
    public static function viewSubjects($order=array()){
		$sql = "
		SELECT *
		FROM subject_list
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}

	public static function view1Student($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}

	public static function delete($subject_ID)
	{
	$sql = "
		DELETE FROM subject_list where subject_ID = ?
		";
		$param = array(
			array($subject_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Subject successfully deleted!", "success");
			href("subjects.php?action=view");
	}
}
?>
<?php
class SUBJENROL{

public static function checkdateenrolled($sem_ID,$si_ID)
{
	$sql = "SELECT date_enrolled
			FROM enrolment where
			sem_ID = '$sem_ID' and si_ID = '$si_ID'
			";
			
	return SQL::find_id($sql);	

}



public static function checkenrolpersem($sem_ID,$si_ID)
{
	$sql = "SELECT e_ID
			FROM enrolment where
			sem_ID = '$sem_ID' and si_ID = '$si_ID'
			";
			
	return SQL::find_scalar($sql);	

}

public static function checkSubjEnrolled($subject_sched_ID,$si_ID)
{
	$sql = "SELECT count(*)
			FROM subject_enrolled where
			subject_sched_ID = '$subject_sched_ID' and si_ID = '$si_ID'
			";
			
	return SQL::find_scalar($sql);	

}

public static function deletesubjectsfromsem($sem_ID,$si_ID){
 		$sql = "
			 DELETE a
			  FROM subject_enrolled a
			  JOIN subject_sched b ON a.subject_sched_ID = b.subject_sched_ID
			  JOIN semester_section c ON c.semester_section_ID = b.semester_section_ID
			 WHERE c.sem_ID = ? and a.si_ID = ?
				
			";
			$param = array(
					$sem_ID,
					$si_ID
			);
			SQL::execute($sql, $param);
            
			#$id = SQL::last_ID();
			#href("fin_enrolment2.php?action=view&id=$si_ID&sem_ID=$sem_ID");
			
	}	

 public static function updatenrol($info,$id){
	
	// $user_id = $info['userid'];
	
	$po_info = SUBJSCHED::getPaymentOptionByID($info['payment_option']);
	
	#msgbox(var_dump($po_info));
	
	$po_lab_rate = $po_info['po_LAB'];
	$po_lec_rate = $po_info['po_LEC'];	
	$po_misc = $po_info['po_MISC'];	
	
	
	$sem = $info['sem_ID'];
	
	SUBJENROL::deletesubjectsfromsem($sem,$id);
	ACCT::remp_sem($sem,$id);
	
	$po =  $info['payment_option'];
	$length = sizeof($info['id']);
	$total_lec = 0;
	$total_lab = 0;
	$acc_ID = $info['acc_ID'];
	
	for($i=0;$i<$length;$i++)
	{
		$subject_sched_ID = $info['id'][$i];
		$si_ID = $id;
		
			$subject_sched = SUBJSCHED::getID($subject_sched_ID);
			$subject_info = SUBJECTS::getID($subject_sched['subject_ID']);	
			

			
			SUBJENROL::enrol($subject_sched_ID,$si_ID);
			
			
			$total_lec += $subject_info['LEC_UNIT'];
			$total_lab += $subject_info['LAB_UNIT'];
		
	}
 $assess = explode(",",$info['assess_ID']);
 $y = count($assess);
 $afee = 0;
 for($x=0;$x<$y;$x++)
 {
 	$af = SUBJSCHED::getFAbyID($assess[$x]);
	$afee += $af['af_PRICE'];
 }	
  echo "AF:".$afee."<BR>";
	
  $grad_fee = 0;		
  if($info['gf_ID'] != '')
  {
  	$gf = SUBJSCHED::getGFbyID($info['gf_ID']);
	$grad_fee = $gf['gf_AMOUNT'];
  }	
	
echo "GF: ".$grad_fee."<BR>";;	
	
  $thesis_fee = 0;		
  if($info['tf_ID'] != '')
  {
  	$gf = SUBJSCHED::getTFbyID($info['tf_ID']);
	$thesis_fee = $gf['tf_AMOUNT'];
  }		
  echo "TF: ".$thesis_fee."<BR>";;
	
	
  echo  $lab_fee = $total_lab*$po_lab_rate;
  echo "<br>";
  echo	$lec_fee = $total_lec*$po_lec_rate;
  echo "<br>";
  echo $po_misc;
  echo "<br>";
  echo $install_fee = $po_info['po_installment_fee'];
  echo "<br>";
  echo $discount_tf = ($lab_fee+$lec_fee)/$po_info['po_discount_tf'];
  echo "<br>";
  echo $discount_mf = ($po_misc)/$po_info['po_discount_misc'];
  echo "<br>";
  echo $additional_tf = ($lab_fee+lec_fee)/$po_info['po_additional_tf'];
   echo "<br>";
  echo $additional_mf = ($po_misc)/$po_info['po_additional_misc'];
  $total_tf = $thesis_fee + $grad_fee + $afee + $po_misc + $lab_fee + $lec_fee - ($discount_tf + $discount_mf) + ($additional_tf + $additional_mf) + $install_fee;
  echo "TOTAL TF: " .$total_tf;
	$si_ID = clean($_GET['id']);
		  
		  
	echo $misc."<br>";
	$subject_sched_ID = implode(',',$info['id']);   
	echo $subject_sched_ID;
	echo "<br>".$sem; 	   
		   
		   
		   
            SESSION::StoreMsg("Student Schedule Added!","success");
			
		
			SUBJENROL::updateEnrolment($info['si_ID'],$sem,$subject_sched_ID,$info['payment_option'],$info['assess_ID'],$info['gf_ID'],$info['tf_ID'],$info['acc_ID']);
			
	}	
	public static function updateEnrolment($si_ID,$sem_ID,$ss_ID,$po_ID,$af_ID,$gf_ID,$tf_ID,$acc_ID){
   	$datetime = date("Y-m-d H:i:s");
    $date = date("Y-m-d");
   
           $sql = "
			UPDATE	enrolment
			SET
				subject_sched_ID = ?,
				po_ID = ?,
				af_ID = ?,
				gf_ID = ?,
				tf_ID = ?,
				account_ID = ?,
				or_ID = ?
			WHERE
				si_ID = ? and
				sem_ID = ?
				
			";
			$param = array(
				$ss_ID,
				$po_ID,
				$af_ID,
				$gf_ID,
				$tf_ID,
				$acc_ID,
				NULL,
				$si_ID,
				$sem_ID
			);
			SQL::execute($sql, $param);
            
			#$id = SQL::last_ID();
			href("fin_enrolment2.php?action=view&id=$si_ID&sem_ID=$sem_ID");
			
			$po_info = SUBJSCHED::getPaymentOptionByID($accinfo['po_ID']);
			
			/////////////////////////////////// Audit Trail Start (Jeelbert) /////////////////////////////////
			$info =  USERS::viewSingleStudent($si_ID);
			$explode = explode(",", $ss_ID);
			foreach($explode as $DI_si){
				$asd = SUBJSCHED::getSched($DI_si['subject_sched_ID']);
				$wew = SUBJECTS::getID($asd['subject_ID']);
				$room = ROOM::getID($DI_si['room_ID']);

				$room2 = $room['room_NAME'];
				$day = $asd['day'];
				$start = $asd['start'];
				$end = $asd['end'];
				$desc = $wew['subject_DESCRIPTION'];
			
			$array = array(
				'Subject Description'=>$desc,
				'Day'=>$day,
				'Start Time'=>$start,
				'End Time'=>$end,
				'Room '=>$room2.' '
			);
			$output2 .= implode(', ', array_map(
			    function ($v, $k) {
			        if(is_array($v)){
			            return $k.'[]:'.implode('&'.$k.'[]:', $v);
			        }else{
			            return $k.':'.$v;
			        }
			    }, 
			    $array, 
			    array_keys($array)
			));
			}
			$user_id = $acc_ID;
	        $action_event = "Update";
	        $event_desc = "MODULE: Registrar / Student Sectioning, DESCRIPTION: User Updated Student Schedule of  ".$info['si_LNAME'].", ".$info['si_FNAME']." ".$info['si_MNAME']." ".$output2.' ';
	        
	        
	        $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);		
			/////////////////////////////////// Audit Trail End (Jeelbert) /////////////////////////////////
			
			
			
	}
	










    public static function addsubjenr($info){
           $sql = "
			INSERT INTO	subject_enrolled
			SET
                subject_sched_ID = ?,
				si_ID = ?,
				status = 1

			";
			$param = array(
				
				$info['subject_sched_ID'],
				$info['si_ID']
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Subject Student Added!","success");
	}
	



    public static function enrol($ssid,$si_ID){
           
		   
		$c =  SUBJENROL::checkSubjEnrolled($ssid,$si_ID);		   
		
		if($c == 0){
		   $sql = "
			INSERT INTO	subject_enrolled
			SET
                subject_sched_ID = ?,
				si_ID = ?,
				status = 1

			";
			$param = array(
				
				$ssid,
				$si_ID
			);
			SQL::execute($sql, $param);
		}	
            #SESSION::StoreMsg("Subject Student Added!","success");
	}

	
	
	
	
	
	
	 public static function enrolbulk2($info,$id){
		 $sem = $info['sem_ID'];
	 
		$check = SUBJENROL::checkenrolpersem($sem,$id);
		
		if($check > 0)
		{
			echo "Redirecting...";
					href("old/fin_enrolment2.php?action=view&id=$id&sem_ID=$sem");
			
		}else{
		
					$date_added = date('Y-m-d', strtotime($info['en_DATE']));
					#var_dump($info);
					$po_info = SUBJSCHED::getPaymentOptionByID($info['payment_option']);
					#msgbox(var_dump($po_info));
					$po_lab_rate = $po_info['po_LAB'];
					$po_lec_rate = $po_info['po_LEC'];	
					$po_misc = $po_info['po_MISC'];	
					
					$po =  $info['payment_option'];
					
					$total_lec = 0;
					$total_lab = 0;
					$acc_ID = $info['acc_ID'];
					$result = array_unique($info['id']);
					$length = sizeof($result);
					
					for($i=0;$i<$length;$i++)
					{
						$subject_sched_ID = $result[$i];
						$si_ID = $id;
						
							$subject_sched = SUBJSCHED::getID($subject_sched_ID);
							$subject_info = SUBJECTS::getID($subject_sched['subject_ID']);	
							SUBJENROL::enrol($subject_sched_ID,$si_ID);
							$total_lec += $subject_info['LEC_UNIT'];
							$total_lab += $subject_info['LAB_UNIT'];
						
					}
				 $assess = explode(",",$info['assess_ID']);
				 $y = count($assess);
				 $afee = 0;
				 for($x=0;$x<$y;$x++)
				 {
					$af = SUBJSCHED::getFAbyID($assess[$x]);
					$afee += $af['af_PRICE'];
				 }	
				  echo "AF:".$afee."<BR>";
					
				  $grad_fee = 0;		
				  if($info['gf_ID'] != '')
				  {
					$gf = SUBJSCHED::getGFbyID($info['gf_ID']);
					$grad_fee = $gf['gf_AMOUNT'];
				  }	
					
				echo "GF: ".$grad_fee."<BR>";;	
					
				  $thesis_fee = 0;		
				  if($info['tf_ID'] != '')
				  {
					$gf = SUBJSCHED::getTFbyID($info['tf_ID']);
					$thesis_fee = $gf['tf_AMOUNT'];
				  }		
				  echo "TF: ".$thesis_fee."<BR>";;
					
					
				  echo  $lab_fee = $total_lab*$po_lab_rate;
				  echo "<br>";
				  echo	$lec_fee = $total_lec*$po_lec_rate;
				  echo "<br>";
				  echo $po_misc;
				  echo "<br>";
				  echo $install_fee = $po_info['po_installment_fee'];
				  echo "<br>";
				  echo $discount_tf = ($lab_fee+$lec_fee)*$po_info['po_discount_tf'];
				  echo "<br>";
				  echo $discount_mf = ($po_misc)*$po_info['po_discount_misc'];
				  echo "<br>";
				  echo $additional_tf = ($lab_fee+lec_fee)*$po_info['po_additional_tf'];
				   echo "<br>";
				  echo $additional_mf = ($po_misc)*$po_info['po_additional_misc'];
				  $total_tf = $thesis_fee + $grad_fee + $afee + $po_misc + $lab_fee + $lec_fee - ($discount_tf + $discount_mf) + ($additional_tf + $additional_mf) + $install_fee;
				  echo "TOTAL TF: " .$total_tf;
					$si_ID = clean($_GET['id']);
						  
						  
					echo $misc."<br>";
					$subject_sched_ID = implode(',',$info['id']);   
					echo $subject_sched_ID;
					echo "<br>".$sem; 	   
						   
						   
						   
							SESSION::StoreMsg("Subject Student Added!","success");
							
							#SUBJENROL::addaccrecords($si_ID,$misc,$po,$sem,$total_lec,$total_lab,$thesis_fee,$acc_ID);
							
							SUBJENROL::addenrolment($info['si_ID'],$sem,$subject_sched_ID,$info['payment_option'],$info['assess_ID'],$info['gf_ID'],$info['tf_ID'],$info['acc_ID'],$date_added);
		}			
	}	
	public static function addenrolment($si_ID,$sem_ID,$ss_ID,$po_ID,$af_ID,$gf_ID,$tf_ID,$acc_ID,$date_added){
   	$datetime = date("Y-m-d H:i:s");
    $date = date("Y-m-d");
   
           $sql = "
			INSERT INTO	enrolment
			SET
				si_ID = ?,                	
				sem_ID = ?,
				subject_sched_ID = ?,
				po_ID = ?,
				af_ID = ?,
				gf_ID = ?,
				tf_ID = ?,
				account_ID = ?,
				date_enrolled = ?,
				date_encoded = ?
				
			";
			$param = array(
				$si_ID,
				$sem_ID,
				$ss_ID,
				$po_ID,
				$af_ID,
				$gf_ID,
				$tf_ID,
				$acc_ID,
				$date_added,
				$datetime
			);
			SQL::execute($sql, $param);
            
			$id = SQL::last_ID();
			href("fin_enrolment2.php?action=view&id=$si_ID&sem_ID=$sem_ID");
			
			/////////////////////////////////// Audit Trail Start (Jeelbert) /////////////////////////////////
			$info =  USERS::viewSingleStudent($si_ID);
			$sem = SEM::getSingleSem($sem_ID);
			$explode = explode(",", $ss_ID);
			foreach($explode as $DI_si){
				$asd = SUBJSCHED::getSched($DI_si['subject_sched_ID']);
				$wew = SUBJECTS::getID($asd['subject_ID']);
				$room = ROOM::getID($DI_si['room_ID']);

				$room2 = $room['room_NAME'];
				$day = $asd['day'];
				$start = $asd['start'];
				$end = $asd['end'];
				$desc = $wew['subject_DESCRIPTION'];
			
			$array = array(
				'Subject Description'=>$desc,
				'Day'=>$day,
				'Start Time'=>$start,
				'End Time'=>$end,
				'Room '=>$room2.' '
			);
			$output2 .= implode(', ', array_map(
			    function ($v, $k) {
			        if(is_array($v)){
			            return $k.'[]:'.implode('&'.$k.'[]:', $v);
			        }else{
			            return $k.':'.$v;
			        }
			    }, 
			    $array, 
			    array_keys($array)
			));
			}
			$user_id = $acc_ID;
	        $action_event = "Update";
	        $event_desc = "MODULE: Registrar / Student Sectioning, DESCRIPTION: User Updated Student Schedule of  ".$info['si_LNAME'].", ".$info['si_FNAME']." ".$info['si_MNAME']." School Year/Semester: ".$sem['sem_NAME'].' '.$output2;
	        
	        
	        $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
			/////////////////////////////////// Audit Trail End (Jeelbert) ///////////////////////////////////
			
			
			
			
			
	}
	
	
	
	
	
	
	
	
	
	
	
	
    public static function enrolbulk($info,$id)
	{
	
	
		$length = sizeof($info['id']);
		$total_lec = 0;
		$total_lab = 0;
		$acc_ID = $info['acc_ID'];
		
		for($i=0;$i<$length;$i++)
		{
			$subject_sched_ID = $info['id'][$i];
			$si_ID = $id;
			
				$subject_sched = SUBJSCHED::getID($subject_sched_ID);
				$subject_info = SUBJECTS::getID($subject_sched['subject_ID']);	
				SUBJENROL::enrol($subject_sched_ID,$si_ID);
				
				#echo  $subject_info['LEC_UNIT']. " " .$subject_info['LAB_UNIT'];
				#echo "<br>";
				
				$total_lec += $subject_info['LEC_UNIT'];
				$total_lab += $subject_info['LAB_UNIT'];
		}
		
		if($info['misc'] == 'yes')
		{
			$misc = "1695.00";
		}else{
			$misc = "0.00";
		}
		
		if($info['thesis'] == 'yes')
		{
			$thesis_fee = "500.00";
		}else{
			$thesis_fee = "0.00";
		}
		
		$sem = $info['sem_ID'];
		$po =  $info['payment_option'];
		$si_ID = clean($_GET['id']);
		
		echo $misc."<br>";
		echo $thesis_fee;	  
			   
				SESSION::StoreMsg("Subject Student Added!","success");
				SUBJENROL::addaccrecords($si_ID,$misc,$po,$sem,$total_lec,$total_lab,$thesis_fee,$acc_ID);
				
	}	
	
	
	
	
	
	
	
	
	
	
	
	
	public static function addaccrecords($si_ID,$misc,$payment_option,$sem_ID,$lec_unit,$lab_unit,$thesis_fee,$acc_ID){
   
   	$lec_amount = $lec_unit * 275.00;
	$lab_amount = $lab_unit * 295.00;
   
           $sql = "
			INSERT INTO	acc_records
			SET
                
				si_ID = ?,
				misc = ?,
				payment_option = ?,
				sem_ID = ?,
				lec_units = ?,
				lab_units = ?,
				lec_amount = ?,
				lab_amount = ?,
				thesis_fee = ?,
				account_ID = ?
				
			";
			$param = array(
				$si_ID,
				$misc,
				$payment_option,
				$sem_ID,
				$lec_unit,
				$lab_unit,
				$lec_amount,
				$lab_amount,
				$thesis_fee,
				$acc_ID
			);
			SQL::execute($sql, $param);
            
			$id = SQL::last_ID();
			href("fin_enrolment.php?action=view&id=$id");
			
			
			
			
			
			
	}
	
	
	
	
	
	
	
	public static function getaccrecord($id)
	{
			$sql = "
			SELECT * FROM acc_records where id = ?
			";
		$param = array($id);
			
		return SQL::find_id($sql,$param);	
	
	
	}
	


 	public static function getaccrecordpersem($id,$sem_ID)
	{
			$sql = "
			SELECT * FROM acc_records where id = ? and sem_ID = '$sem_ID'
			";
		$param = array($id);
			
		return SQL::find_id($sql,$param);	
	
	
	}


	

 	public static function getenrollmentofstudentpersem($id,$sem_ID)
	{
			$sql = "
			SELECT * FROM enrolment where si_ID = ? and sem_ID = '$sem_ID'
			";
		$param = array($id);
			
		return SQL::find_id($sql,$param);	
	}
	
	
	 public static function getenrollmentpersem($sem_ID)
	{
			$sql = "
			SELECT * FROM enrolment where sem_ID = '$sem_ID' and or_ID is not NULL
			";
		$param = array($id);
			
		return SQL::find_all($sql,$param);	
	}
	
	public static function getenrollmentpersempercourse($order=array(),$course_ID,$sem_ID)
	{
		
			$sql = "
			SELECT a.* 
			FROM enrolment a, student_information a1,
			curricullum_enrolled b, curricullum_list c, course_list d
		
			WHERE 	a.si_ID = b.si_ID and 
				a.si_ID = a1.si_ID and
				b.curricullum_ID = c.curricullum_ID	and 
				c.course_ID = d.course_ID and
				c.course_ID = ? and
				b.status = 'ENROLLED'
				and
			a.sem_ID = ? and a.or_ID is not NULL
			
			";
		$param = array($course_ID,$sem_ID);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql,$param);	
	}
	

		#Get All Payments By Date
		public static function getenrollmentpersemperdate($order=array(),$sem_ID,$start,$end)
		{
			if($end == NULL)
			{
				$end = $start;
			}
			
				$sql = "
				SELECT *
				FROM enrolment
				WHERE date_enrolled >= '$start' and date_enrolled <= '$end' and sem_ID = '$sem_ID' and or_ID is not NULL
				";
				foreach($order as $key => $value)
				{
					if($key ==  0) 
					{
						$sql .= "ORDER BY {$key} {$value}";
					} else {
						$sql .= ", {$key} {$value}";
					}
				}
			return SQL::find_all($sql);
		}

	
	
	
    public static function getAll($order=array())
	{
			$sql = "
			SELECT *
			FROM subject_enrolled
			";
		$param = array();
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql,$param);
	}
	
	
	
	 public static function getAllTOP100($order=array())
	{
			$sql = "
			SELECT *
			FROM subject_enrolled
			";
		$param = array();
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		$sql .= " LIMIT 100";
		return SQL::find_all($sql,$param);
	}
	
	
	
	
	public static function delete($id)
	{
	$sql = "
		DELETE FROM subject_enrolled where subject_enrolled_ID = ?
		";
		$param = array(
			array($id, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Successfully deleted!", "success");
			href("subject_student_beta.php?action=add");
	}
	


	public static function gradebyid($id,$sem_ID)
	{
			$sql = "
			SELECT a.*
			FROM subject_enrolled a, subject_sched b, semester_section c, semester d
			WHERE a.subject_sched_ID = b.subject_sched_ID and b.semester_section_ID = c.semester_section_ID
			and c.sem_ID = d.sem_ID and d.sem_ID = ? and a.si_ID = ?
			";
		$param = array($sem_ID,$id);
			
		return SQL::find_all($sql,$param);	
	
	
	}
	
	public static function mysubjects($id,$sem_ID)
	{
			$sql = "
			SELECT a.*
			FROM subject_enrolled a, subject_sched b, semester_section c, semester d, subject_list e
			WHERE a.subject_sched_ID = b.subject_sched_ID and b.semester_section_ID = c.semester_section_ID
			and c.sem_ID = d.sem_ID and d.sem_ID = ? and a.si_ID = ? and b.subject_ID = e.subject_ID
			ORDER BY FIELD(b.day, 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'), b.end ASC
			";
		$param = array($sem_ID,$id);
			
		return SQL::find_all($sql,$param);	
	
	
	}
	
	
	public static function submitpayment($info,$id)
	{
	
		if(ACCT::checkOR($info['OR'],$info['si_ID']) == 1)
		{
		$d1 = "2014-11-14";
		$d2 = "2014-12-12";
		$d3 = "2015-01-23";
		
		
			$sql = "
			UPDATE
			acc_records
			set down_OR = ?, payment1 = ?, payment2 = ?, payment3 = ? where id = ?
			";
			$param = array(
					$info['OR'],
					
					$d1,
					$d2,
					$d3,
					$id,);
			
			SQL::execute($sql, $param);
		
			$total_tuition = $info['total'];
			$or = ACCT::checkORData($info['OR']);
			
			
			
			$amount = $or['payment_AMOUNT'];
			
			$total = $total_tuition - $amount;
			
			$p = $amount;
			$d = date("Y-m-d");
			ACCT:: addp($info['si_ID'], $p, $d, 1,'DOWNPAYMENT FEE');
			
			$p1 = floor($total/3);
			ACCT:: addp($info['si_ID'], $p1, $d1, 1,'1ST INSTALLMENT FEE');
															
			$p2 = floor(($total-$p1)/2);
			ACCT:: addp($info['si_ID'], $p2, $d2, 1,'2ND INSTALLMENT FEE');	
				
			$p3 = $total - ($p1+$p2);
			ACCT:: addp($info['si_ID'], $p3, $d3, 1,'3RD INSTALLMENT FEE');	
		
		
		}else{
		
		SESSION::StoreMsg("Invalid OR No!","error");
		
		}
	
	
			
	
	
	}	
	
	
	
	
	
	public static function submitpayment2($info,$id,$sem_ID)
	{
	
		if(ACCT::checkOR($info['OR'],$info['si_ID'],$sem_ID) == 1)
		{
			$sql = "
			UPDATE
			enrolment
			set or_ID = ?
			 where si_ID = ? and sem_ID = ?
			";
			$param = array(
					$info['OR'],
					$id,
					$sem_ID
					);
			
			SQL::execute($sql, $param);
		
			$total_tuition = $info['totaltf'];
			$or = ACCT::checkORData($info['OR'],$id,$sem_ID);
			$amount = $or['payment_AMOUNT'];
			$total = $total_tuition - $amount;
			$bal = $total;
			
			$p = $amount;
			$d = date("Y-m-d");
			ACCT::addp_sem($info['si_ID'], $p, $d, 1,'DOWNPAYMENT FEE',$sem_ID);
			
				if($info['date1'] == NULL)
				{
					$slice = 1;
				}else if($info['date2'] == NULL){
					$slice = 2;
				}else if($info['date3'] == NULL){
					$slice = 3;
				}else if($info['date4'] == NULL){
					$slice = 4;
				}else if($info['date5'] == NULL){
					$slice = 5;
				}
				
				$slice -= 1;
				#msgbox($slice);
				
				
				if($slice == 1)
				{
				$p1 = $bal;
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE',$sem_ID);
				
				}else if($slice == 2)
				{
				$p1 = floor($bal/2);
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE',$sem_ID);
				
				$p2 = $bal-$p1;
					ACCT:: addp_sem($info['si_ID'], $p2, $info['date2'], 1,'2ND INSTALLMENT FEE',$sem_ID);
				}else if($slice == 3)
				{
					$p1 = floor($bal/3);
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE',$sem_ID);
					
					$p2 = floor(($bal-$p1) / 2);
					ACCT::addp_sem($info['si_ID'], $p2, $info['date2'], 1,'2ND INSTALLMENT FEE',$sem_ID);
					
					$p3 = $bal - ($p1+$p2);
					ACCT::addp_sem($info['si_ID'], $p3, $info['date3'], 1,'3RD INSTALLMENT FEE',$sem_ID);
				}else if($slice == 4)
				{
					$p1 = floor($bal/4);
					ACCT::addp_sem($info['si_ID'], $p1, $info['date1'], 1,'1ST INSTALLMENT FEE',$sem_ID);
					$p2 = $p1;
					ACCT::addp_sem($info['si_ID'], $p2, $info['date2'], 1,'2ND INSTALLMENT FEE',$sem_ID);
					$p3 = $p2;
					ACCT::addp_sem($info['si_ID'], $p3, $info['date3'], 1,'3RD INSTALLMENT FEE',$sem_ID);
					$p4 = $bal-($p1+$p2+$p3);
					ACCT::addp_sem($info['si_ID'], $p4, $info['date4'], 1,'4TH INSTALLMENT FEE',$sem_ID);
				}	
			
			
		
		}else{
		
		SESSION::StoreMsg("Invalid OR No!","error");
		
		}
	
	
			
	
	
	}	
			
	
	
	
	
	public static function hideGrade($id)
	{
			$sql = "
			UPDATE
			subject_enrolled
			set status = 0 where subject_enrolled_ID = ?
			";
		$param = array($id);
			
		SQL::execute($sql, $param);
	
	
	}	
		
	
	
	####END###
	
	

}

?>
<?php
class SUBJSCHED{

    public static function addsubjsched($subjschedinfo){
           $sql = "
			INSERT INTO	subject_sched
			SET
                semester_section_ID = ?,
                subject_ID = ?,
				day = ?,
				room_ID = ?,
				start = ?,
				end = ?,
				day2 = ?,
				room_ID2 = ?,
				start2 = ?,
				end2 = ?,
				day3 = ?,
				room_ID3 = ?,
				start3 = ?,
				end3 = ?,
				day4 = ?,
				room_ID4 = ?,
				start4 = ?,
				end4 = ?,
				instructor_ID = ?,				
				max_student = ?
			";
			$param = array(
				$subjschedinfo['semester_section_ID'],
				$subjschedinfo['subject_ID'],
				$subjschedinfo['day'],
				$subjschedinfo['room_ID'],
				$subjschedinfo['start'],
				$subjschedinfo['end'],
				$subjschedinfo['day2'],
				$subjschedinfo['room_ID2'],
				$subjschedinfo['start2'],
				$subjschedinfo['end2'],
				$subjschedinfo['day3'],
				$subjschedinfo['room_ID3'],
				$subjschedinfo['start3'],
				$subjschedinfo['end3'],
				$subjschedinfo['day4'],
				$subjschedinfo['room_ID4'],
				$subjschedinfo['start4'],
				$subjschedinfo['end4'],
				$subjschedinfo['instructor_ID'],
				$subjschedinfo['max_student']
			);
			$result = preg_replace('/[{-}"]/', '', json_encode($param));
			$user_info_id = $_SESSION['USER']['account_ID'];
			$action_event = "ADD";
			$event_desc = "MODULE: Subject Schedule DESCRIPTION: User Added a Subject Schedule <br>DETAILS: ".$result;
			Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Subject Schedule Added!","success");
	}
	
    public static function getAll($order=array())
	{
			$sql = "
			SELECT *
			FROM subject_sched
			";
		$param = array();
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql,$param);
	}
	
	
	public static function getAllperSEM($sem_ID,$order=array())
	{
			$sql = "
			SELECT a.*
			FROM subject_sched a, semester_section b 
			WHERE a.semester_section_ID = b.semester_section_ID and b.sem_ID = '$sem_ID'
			";
		$param = array();
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql,$param);
	}
	

	public static function getAllperSEM2($order=array())
	{
			$sql = "
			SELECT a.*
			FROM subject_sched a, semester_section b 
			WHERE a.semester_section_ID = b.semester_section_ID
			";
		$param = array();
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql,$param);
	}




    public static function getsubjects($sem_ID,$course_ID,$order=array())
	{
			$sql = "
			SELECT *
			FROM subject_sched
			";
		$param = array();
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql,$param);
	}



	
	
	
	public static function getID($id)
	{
			$sql = "
			SELECT *
			FROM subject_sched
			where subject_sched_ID = '$id'
			";
		$param = array();
		return SQL::find_id($sql,$param);
		
	}
	
	public static function getByfaculty($sem_id,$instructor_id)
	{
			$sql = "
			SELECT *,  STR_TO_DATE(start, '%h:%i %p') as startA
			
			FROM

				subject_sched a,
				semester_section b,
				instructor c

				WHERE 
				a.semester_section_ID = b.semester_section_ID and
				b.sem_ID = ? and a.instructor_ID = c. instructor_ID and a.instructor_ID = ?
				ORDER BY
				FIELD(a.day, 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY') ASC, startA ASC
				
			";
		$param = array(
		
				$sem_id,
				$instructor_id
		
		);
		
		
		
		return SQL::find_all($sql,$param);
		
	}

	public static function getByRoom($sem_id,$r_ID)
	{
			$sql = "
			SELECT *,  STR_TO_DATE(start, '%h:%i %p') as startA
			
			FROM

				subject_sched a,
				semester_section b,
				instructor c
				
				WHERE 
				a.semester_section_ID = b.semester_section_ID and
				b.sem_ID = ? and a.instructor_ID = c.instructor_ID and a.room_ID = ?
				ORDER BY
				FIELD(a.day, 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY') ASC, startA ASC
				
			";
		$param = array(
		
				$sem_id,
				$r_ID
		
		);
		
		
		
		return SQL::find_all($sql,$param);
		
	}	


public static function getSched($id)
	{
			$sql = "
			SELECT *
			FROM subject_sched a, semester_section b
			where a.subject_sched_ID = '$id' and a.semester_section_ID = b.semester_section_ID
			";
		$param = array();
		
		
		
		return SQL::find_id($sql,$param);
		
	}



	public static function getBySubjectSchedID($subject_sched_ID, $instructor_ID)
	{
			$sql = "
			SELECT b.*,c.si_FNAME,c.si_LNAME,c.si_MNAME,c.si_ID,c.student_ID, c.si_CONTACT FROM
				subject_sched a, subject_enrolled b, student_information c
				WHERE a.subject_sched_ID = b.subject_sched_ID
				and a.subject_sched_ID = ? and a.instructor_ID = ? and b.si_ID = c.si_ID
				ORDER BY c.si_LNAME ASC
			";
		$param = array(
		
				$subject_sched_ID,
				$instructor_ID
				
		
		);
		return SQL::find_all($sql,$param);	
	}	
		
		
	
	
	public static function getBySubjectSchedID2($subject_sched_ID)
	{
			$sql = "
			SELECT c.* FROM
				subject_sched a, subject_enrolled b, semester_section c
				WHERE a.subject_sched_ID = b.subject_sched_ID and a.semester_section_ID = c.semester_section_ID
				and a.subject_sched_ID = ?
				
			";
		$param = array(
		
				$subject_sched_ID
				
		
		);	
		
		
		
		return SQL::find_id($sql,$param);
		
	}	
	
	
    public static function uploadGrade($subject_enrolled_ID,$grade){
           $sql = "
			UPDATE subject_enrolled SET grade =?, grade_date = ?
			WHERE subject_enrolled_ID = ? 
			
			";
			$date = date("Y-m-d H:i:s");
			$param = array(
					$grade,
					$date,
					$subject_enrolled_ID
			);
			SQL::execute($sql, $param);
		   # echo "<span style='color:green'>Grade Updated!</span>";
           # SESSION::StoreMsg("Subject Schedule Added!","success");
	}	
	
	
	
	

    public static function updateGrade($subject_enrolled_ID,$grade,$grade_mid){
           $sql = "
			UPDATE subject_enrolled SET grade =? ,grade_mid =?
			WHERE subject_enrolled_ID = ? 
			
			";
			$param = array(
					$grade,
					$grade_mid,
					$subject_enrolled_ID
			);
			SQL::execute($sql, $param);
			echo "<span style='color:green'>Grade Updated!</span>";
           # SESSION::StoreMsg("Subject Schedule Added!","success");
	}
	
	
	
    public static function encodeGrade($subject,$grade,$student_ID){
          
		  $sql = "
			SELECT count(*)
			FROM subject_encoded
			WHERE subject_ID = ? and 
				  si_ID = ?
			";
		  
		  $param = array($subject,$student_ID);
		  
		 $num = SQL::find_scalar($sql,$param);	
		 
		 $id = $_SESSION['USER']['account_ID'];
		 $date = date("Y-m-d H:i:s");
		 
		 if($num == 0)
		 {
		 //insert
		 	$sql = "
			INSERT INTO subject_encoded SET grade =?, si_ID =?, subject_ID = ? , encoded_by = ?, encoded_date = ?
			";
			$param = array(
					$grade,
					$student_ID,
					$subject,
					$id,
					$date
					
			);
			SQL::execute($sql, $param);
			echo "<span style='color:green'>Grade Encoded!</span>";
		 
		 }else{
			$sql = "
			UPDATE subject_encoded SET grade =? ,encoded_by = ? ,encoded_date = ?
			WHERE subject_ID = ?  and si_ID = ?
			
			";
			$param = array(
					$grade,
					$id,
					$date,
					$subject,
					$student_ID
					
			);
			SQL::execute($sql, $param);
			echo "<span style='color:green'>Grade Updated!</span>";
		}
		 
			#echo "<span style='color:green'>Grade Updated!</span>";
           # SESSION::StoreMsg("Subject Schedule Added!","success");
	}	
	
	
	
	
	public static function getActiveSubjects()
	{
			$sql = "
			SELECT *
			FROM subject_sched b, semester_section c, semester d
			WHERE  b.semester_section_ID = c.semester_section_ID
			and c.sem_ID = d.sem_ID and d.sem_ENROLMENT = 1 
			ORDER BY b.semester_section_ID ASC
			";
		$param = array();
			
		return SQL::find_all($sql,$param);	
	
	
	}
	
	
	public static function getActiveSubjectsPerSem($sem_ID)
	{
			$sql = "
			SELECT *
			FROM subject_sched b, semester_section c, semester d
			WHERE  b.semester_section_ID = c.semester_section_ID
			and c.sem_ID = d.sem_ID and d.sem_ENROLMENT = 1  and c.sem_ID ='$sem_ID'
			ORDER BY b.semester_section_ID ASC
			";
		$param = array();
			
		return SQL::find_all($sql,$param);	
	
	
	}	
	
	public static function getActiveSubjectsBySearch($s)
	{
			$sql = "
			SELECT *
			FROM subject_sched b, semester_section c, semester d, subject_list e
			WHERE  b.semester_section_ID = c.semester_section_ID
			and c.sem_ID = d.sem_ID and d.sem_ENROLMENT = 1 and b.subject_ID = e.subject_ID
			and (e.subject_CODE LIKE '%$s%' or e.subject_DESCRIPTION LIKE '%$s%')
			ORDER BY b.semester_section_ID ASC
			";
	
			
		return SQL::find_all($sql);	
	
	
	}
	
	public static function getActiveSubjectsBySearchPerSem($s,$sem)
	{
			$sql = "
			SELECT *
			FROM subject_sched b, semester_section c, semester d, subject_list e
			WHERE  b.semester_section_ID = c.semester_section_ID
			and c.sem_ID = d.sem_ID and c.sem_ID = '$sem' and d.sem_ENROLMENT = 1 and b.subject_ID = e.subject_ID
			and (e.subject_CODE LIKE '%$s%' or e.subject_DESCRIPTION LIKE '%$s%')
			ORDER BY b.semester_section_ID ASC
			";
	
			
		return SQL::find_all($sql);	
	
	
	}
	
	public static function getActiveSubjectsByCoursePerSem($id,$sem_ID)
	{
			$sql = "
			SELECT *
			FROM subject_sched b, semester_section c, semester d
			WHERE  b.semester_section_ID = c.semester_section_ID
			and c.sem_ID = d.sem_ID and d.sem_ENROLMENT = 1 and c.course_ID = ? and c.sem_ID = '$sem_ID'
			ORDER BY b.semester_section_ID ASC
			";
		$param = array($id);
			
		return SQL::find_all($sql,$param);	
	
	
	}	
	
	public static function getSubjectsBySection($id)
	{
			$sql = "
			SELECT *
			FROM subject_sched b, semester_section c, semester d
			WHERE  b.semester_section_ID = c.semester_section_ID
			and c.sem_ID = d.sem_ID and c.semester_section_ID = ?
			ORDER BY FIELD(b.day, 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'), b.semester_section_ID ASC,		 b.end ASC
			";
		$param = array($id);
			
		return SQL::find_all($sql,$param);	
	}
	public static function getActiveSubjectsBySection($id)
	{
			$sql = "
			SELECT *
			FROM subject_sched b, semester_section c, semester d
			WHERE  b.semester_section_ID = c.semester_section_ID
			and c.sem_ID = d.sem_ID and d.sem_ENROLMENT = 1 and c.semester_section_ID = ?
			ORDER BY FIELD(b.day, 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'), b.semester_section_ID ASC, b.START ASC
			";
		$param = array($id);
			
		return SQL::find_all($sql,$param);	
	}	
	
	
	public static function CountSubjectsBySection($id)
	{
			$sql = "SELECT COUNT(*)as `count`
			FROM subject_sched b, semester_section c, semester d
			WHERE  b.semester_section_ID = c.semester_section_ID
			and c.sem_ID = d.sem_ID and d.sem_ENROLMENT = 1 and c.semester_section_ID = ?
			ORDER BY FIELD(b.day, 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY'), b.semester_section_ID ASC, b.END ASC";
		$param = array($id);
			
		return SQL::find_scalar($sql,$param);	
	}	
			
	
	
	
	public static function checkRemaining($id)
	{
			$sql = "
			SELECT count(*)
			FROM subject_enrolled
			WHERE subject_sched_ID = ? and status = 1
			";
		$param = array($id);
			
		return SQL::find_scalar($sql,$param);	
	
	
	}		
	
	
	public static function checkIfEnrolled($id,$si_ID)
	{
			$sql = "
			SELECT count(*)
			FROM subject_enrolled
			WHERE subject_sched_ID = ? and status = 1 and si_ID = ?
			";
		$param = array($id,$si_ID);
			
		return SQL::find_scalar($sql,$param);	
	
	
	}			
	
	
	
	
	
	
	
	
	
	
	
	####END###
	
	
	
    public static function update_room($id,$roominfo){
			$sql = "
			Update room_list
			SET
                room_NAME = ?
            WHERE
                room_ID = ?
			";
			$param = array(
				$roominfo['room_NAME'],
                $id
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("User Successfully Updated","success");
	}
    public static function getallrooms($order=array()){
		$sql = "
		SELECT *
		FROM room_list
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function view1room($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}
    public static function getSingle($order=array(),$id)
	{
			$sql = "
			SELECT *
			FROM room_list
			WHERE room_ID = ?
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function getSubjectSchedID($id){
        	$sql = "
			SELECT *
			FROM subject_sched
			WHERE subject_sched_ID = ?
			";
		$param = array($id);
		
		return SQL::find_id($sql,$param);
		
		
    }
    public static function deleteSubjectSchedId($id1){
    	$sql = "
    	DELETE 
    	FROM subject_sched 
    	WHERE subject_sched_ID = ?
    	";
    	$param = array($id1);
    	SQL::execute($sql,$param);
    	SESSION::StoreMsg("Subject Schudule Successfully Deleted!", "success");
    	href("subject_sched.php?action=add");
    }
	
	
	public static function updateFileSubjectSched($filename,$id){
        	$sql = "
			UPDATE	subject_sched SET file_grade = ?
			WHERE subject_sched_ID = ?
			";
		$param = array($filename,$id);
		
		SQL::execute($sql,$param);
		
    }
	
	
	
	
	
	
	
	
    public static function updateSubjSched($id,$subjschedinfo){
           $sql = "
			UPDATE	subject_sched
			SET
                semester_section_ID = ?,
                subject_ID = ?,
				day = ?,
				room_ID = ?,
				start = ?,
				end = ?,
				day2 = ?,
				room_ID2 = ?,
				start2 = ?,
				end2 = ?,
				day3 = ?,
				room_ID3 = ?,
				start3 = ?,
				end3 = ?,
				day4 = ?,
				room_ID4 = ?,
				start4 = ?,
				end4 = ?,
				instructor_ID = ?,				
				max_student = ?
                WHERE subject_sched_ID = ?
                
			";
			$param = array(
				$subjschedinfo['semester_section_ID'],
				$subjschedinfo['subject_ID'],
				$subjschedinfo['day'],
				$subjschedinfo['room_ID'],
				$subjschedinfo['start'],
				$subjschedinfo['end'],
				$subjschedinfo['day2'],
				$subjschedinfo['room_ID2'],
				$subjschedinfo['start2'],
				$subjschedinfo['end2'],
				$subjschedinfo['day3'],
				$subjschedinfo['room_ID3'],
				$subjschedinfo['start3'],
				$subjschedinfo['end3'],
				$subjschedinfo['day4'],
				$subjschedinfo['room_ID4'],
				$subjschedinfo['start4'],
				$subjschedinfo['end4'],
				$subjschedinfo['instructor_ID'],
				$subjschedinfo['max_student'],
                $id
			);
			$after =  array("semester_section_ID" => $subjschedinfo['semester_section_ID'],
				"subject_ID" => $subjschedinfo['subject_ID'],
				"day"=>$subjschedinfo['day'],
				"room_ID"=>$subjschedinfo['room_ID'],
				"start"=>$subjschedinfo['start'],
				"end"=>$subjschedinfo['end'],
				"day2"=>$subjschedinfo['day2'],
				"room_ID2"=>$subjschedinfo['room_ID2'],
				"start2"=>$subjschedinfo['start2'],
				"end2"=>$subjschedinfo['end2'],
				"day3"=>$subjschedinfo['day3'],
				"room_ID3"=>$subjschedinfo['room_ID3'],
				"start3"=>$subjschedinfo['start3'],
				"end3"=>$subjschedinfo['end3'],
				"instructor_ID"=>$subjschedinfo['instructor_ID'],
				"max_student"=>$subjschedinfo['max_student']
			);
			$before =  array("semester_section_ID" => $subjschedinfo['beforesemester_section_ID'],
				"subject_ID" =>$subjschedinfo['beforesubject_ID'],
				"day" =>$subjschedinfo['beforeday'],
				"room_ID" =>$subjschedinfo['beforeroom_ID'],
				"start" =>$$subjschedinfo['beforestart'],
				"end" =>$subjschedinfo['beforeend'],
				"day2" =>$subjschedinfo['beforeday2'],
				"room_ID2" =>$subjschedinfo['beforeroom_ID2'],
				"start2" =>$subjschedinfo['beforestart2'],
				"end2" =>$subjschedinfo['beforeend2'],
				"day3"=>$subjschedinfo['beforeday3'],
				"room_ID3"=>$subjschedinfo['beforeroom_ID3'],
				"start3"=>$subjschedinfo['beforestart3'],
				"end3"=>$subjschedinfo['beforeend3'],
				"instructor_ID" =>$subjschedinfo['beforeinstructor_ID'],
				"max_student" =>$subjschedinfo['beforemax_student']);
            //print_r($subjschedinfo);
            $arraydiffto = array_diff($after, $before);
            $arraydifffrom= array_diff($before, $after);
           	$user_info_id = $_SESSION['USER']['account_ID'];
           	$action_event = "UPDATED";
           	$event_desc =  $arraydiff;
           	$test = array("BEFORE"=>$arraydifffrom,
           					"AFTER"=>$arraydiffto);
  	       		$result2 =preg_replace('/[{-}"]/', '', json_encode($test));

  	       if(($arraydiffto == null)||($arraydiffto == ""))
  	       {

  	       	$event_desc = "MODULE: Subject Schedule <br>DESCRIPTION: NO CHANGES";
  	       
  	       }else
  	       {

  	       	$event_desc = "MODULE: Subject Schedule <br>DESCRIPTION: User updated ".$result2;

  	       }

            Audit_final::audit_trail($user_info_id, $action_event,$event_desc);
//           	print_r($before);
  			SQL::execute($sql, $param);
            SESSION::StoreMsg("Subject Schedule Updated!","success");
	}
	
	
	public static function change_lock($info)
	{
	$sql = "
		UPDATE subject_sched SET is_lock = ? where subject_sched_ID = ?
		";
		$param = array(
			$info['is_lock'],
			$info['subject_sched_id']
			
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Lock Status Changed!", "success");
			#header("refresh:0;url=".$info['url']);
	}
	
	public static function change_check($info)
	{
	$sql = "
		UPDATE 
		subject_sched SET is_check = ? 
		where subject_sched_ID = ?
		";
		$param = array(
			$info['is_check'],
			$info['subject_sched_id']
			
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Check Status Changed!", "success");
			#header("refresh:0;url=".$info['url']);
	}
	
    public static function getPaymentOption()
	{
	      	$sql = "
			SELECT *
			FROM payment_options
			
			";
		$param = array();
		
		return SQL::find_all($sql,$param);
	}
	
	public static function getPaymentOptionPerSem($sem_ID)
	{
	      	$sql = "
			SELECT *
			FROM payment_options a
			WHERE sem_ID = ? 
			ORDER BY a.po_NAME ASC
			";
		$param = array($sem_ID);
		
		return SQL::find_all($sql,$param);
	}
	
	
	
	public static function getPaymentOptionByID($po_ID)
	{
	      	$sql = "
			SELECT *
			FROM payment_options
			WHERE po_ID = ?
			";
		$param = array($po_ID);
		
		return SQL::find_id($sql,$param);
	}	
	
	public static function getAllActiveTF()
	{
	      	$sql = "
			SELECT *
			FROM fee_thesis
			WHERE tf_status = 1
			";
		$param = array();
		
		return SQL::find_all($sql,$param);
	}
	
	public static function getAllActiveGF()
	{
	      	$sql = "
			SELECT *
			FROM fee_grad
			WHERE gf_status = 1
			";
		$param = array();
		
		return SQL::find_all($sql,$param);
	}
	
		public static function getAllActiveFA()
	{
	      	$sql = "
			SELECT *
			FROM fee_assessment
			WHERE af_status = 1
			";
		$param = array();
		
		return SQL::find_all($sql,$param);
	}
	
		public static function getFAbyID($id)
	{
	      	$sql = "
			SELECT *
			FROM fee_assessment
			WHERE af_ID = ?
			";
		$param = array($id);
		
		return SQL::find_id($sql,$param);
	}
	public static function getGFbyID($id)
	{
	      	$sql = "
			SELECT *
			FROM fee_grad
			WHERE gf_ID = ?
			";
		$param = array($id);
		
		return SQL::find_id($sql,$param);
	}
	public static function getTFbyID($id)
	{
	      	$sql = "
			SELECT *
			FROM fee_thesis
			WHERE tf_ID = ?
			";
		$param = array($id);
		
		return SQL::find_id($sql,$param);
	}
	
}

?>
<?php
class ui{


	public static function ui_link($id)
	{
		$sql = "SELECT * FROM
				account_info a, user_instructor b, instructor c
				WHERE
					b.account_ID = a.account_ID and
					b.instructor_ID = c.instructor_ID and
					b.account_ID = ?";	
					
			$param = array(
				$id
			);	
	
			return SQL::find_all($sql,$param);
	}
	
	
	public static function getInstructorID($id)
	{
		$sql = "SELECT c.instructor_ID FROM
				account_info a, user_instructor b, instructor c
				WHERE
					b.account_ID = a.account_ID and
					b.instructor_ID = c.instructor_ID and
					b.account_ID = ?";	
					
			$param = array(
				$id
			);
	
			return SQL::find_id($sql,$param);
	}








    public static function addsubjsched($subjschedinfo){
           $sql = "
			INSERT INTO	subject_sched
			SET
                semester_section_ID = ?,
                subject_ID = ?,
				day = ?,
				room_ID = ?,
				start = ?,
				end = ?,
				instructor_ID = ?,				
				max_student = ?
			";
			$param = array(
				$subjschedinfo['semester_section_ID'],
				$subjschedinfo['subject_ID'],
				$subjschedinfo['day'],
				$subjschedinfo['room_ID'],
				$subjschedinfo['start'],
				$subjschedinfo['end'],
				$subjschedinfo['instructor_ID'],
				$subjschedinfo['max_student']
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("Subject Schedule Added!","success");
	}
	
	
    public static function getAll($order=array())
	{
			$sql = "
			SELECT *
			FROM subject_sched
			";
		$param = array();
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql,$param);
	}
	
	
	
	
	public static function getID($id)
	{
			$sql = "
			SELECT *
			FROM subject_sched
			where subject_sched_ID = '$id'
			";
		$param = array();
		
		
		
		return SQL::find_id($sql,$param);
		
	}
	
	
	
	####END###
	
	
	
    public static function update_room($id,$roominfo){
			$sql = "
			Update room_list
			SET
                room_NAME = ?
            WHERE
                room_ID = ?
			";
			$param = array(
				$roominfo['room_NAME'],
                $id
			);
			SQL::execute($sql, $param);
            SESSION::StoreMsg("User Successfully Updated","success");
	}
    public static function getallrooms($order=array()){
		$sql = "
		SELECT *
		FROM room_list
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
    public static function view1room($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}
    public static function getSingle($order=array(),$id)
	{
			$sql = "
			SELECT *
			FROM room_list
			WHERE room_ID = ?
			";
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function delete($instructor_ID)
	{
	$sql = "
		DELETE FROM room_list where room_ID = ?
		";
		$param = array(
			array($instructor_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Instructor successfully deleted!", "success");
			href("room.php?action=view");
	}
}
?>
<?php
class USERS{

	public function setActiveInactive($account_ID, $access_status) {
		$sql = "
		UPDATE account_info
		SET
			access_status = ?
		WHERE 
			account_ID = ?
		";

		$param = array(
			$account_ID, 
			$access_status
		);
		SQL::execute($sql, $param);
		// SESSION::StoreMsg("User successfully updated!", "success");
	}


	public static function addUserInstructor($userinfo){
			$sql = "
			INSERT INTO	account_info
			SET
				access_ID = ?,
				account_UNAME = ?,
				account_PASS = ?,
				account_FNAME = ?,
				account_MNAME = ?,
				account_LNAME = ?,
				department_ID = ?
				
			";
			$param = array(
				5,
				$userinfo['instructor_UNAME'],
				base64_encode($userinfo['instructor_UPASS']),
				$userinfo['instructor_FNAME'],
				$userinfo['instructor_MNAME'],
				$userinfo['instructor_LNAME'],
				$userinfo['department_ID']
			);
			
			
			SQL::execute($sql, $param);
			SESSION::StoreMsg("Instructor successfully added!", "success");
			return SQL::last_ID();
			#href("users.php");			
	}


	public static function getCourse($ID)
	{
		$sql ="
		SELECT 
			d.course_INIT
		FROM student_information a, curricullum_enrolled b, curricullum_list c, course_list d
		
		WHERE 	a.si_ID = b.si_ID and 
				b.curricullum_ID = c.curricullum_ID	and 
				c.course_ID = d.course_ID and
				a.si_ID = ? and b.status = 'ENROLLED'
		";
		$param = array(
				array($ID, PDO::PARAM_STR));
				
		return SQL::find_scalar($sql, $param);		
		
	}

	public static function getCourse1($ID)
	{
		$sql ="
		SELECT 
			d.course_INIT
		FROM student_information a, course_list d
		
		WHERE 	a.course_ID = d.course_ID and
				a.si_ID = ?
		";
		$param = array(
				array($ID, PDO::PARAM_STR));
				
		return SQL::find_scalar($sql, $param);		
		
	}
	
	public static function getStatus($ID)
	{
		$sql ="
		SELECT 
		 a.sai_STATUS, b.student_status_NAME
		FROM student_additional_info a, student_status b
		
		WHERE 	a.si_ID = ? and 
				a.sai_status = b.student_status_ID
		";
		$param = array(
				array($ID, PDO::PARAM_STR));
				
		return SQL::find_id($sql, $param);		
		
	}
	
	
	
	public static function getFullCourse($ID)
	{
		$sql ="
		SELECT 
			d.course_NAME
		FROM student_information a, curricullum_enrolled b, curricullum_list c, course_list d
		
		WHERE 	a.si_ID = b.si_ID and 
				b.curricullum_ID = c.curricullum_ID	and 
				c.course_ID = d.course_ID and
				a.si_ID = ? and b.status = 'ENROLLED'
		";
		$param = array(
				array($ID, PDO::PARAM_STR));
				
		return SQL::find_scalar($sql, $param);		
		
	}	

	public static function getID($ID, $password = null){
		if($password){
			$sql = "
			SELECT U.*, AL.*
			FROM account_info AS U
			INNER JOIN account_level AS AL
			ON AL.access_ID = U.access_ID
			WHERE U.account_UNAME = ? 
			AND U.account_PASS = ?
			";
			$param = array(
				array($ID, PDO::PARAM_STR),
				array(base64_encode($password), PDO::PARAM_STR)
			);
		} else {
			$sql = "
			SELECT U.*, AL.*
			FROM account_info AS U
			INNER JOIN account_level AS AL
			ON AL.access_ID = U.access_ID
			WHERE U.account_ID = ? 
			";
			$param = array(
				array($ID, PDO::PARAM_INT)
			);
		}
		return SQL::find_id($sql, $param);
		// and U.account_ACTIVE = 1 (deleted)

	}

	public static function getAll($order=array()){
		$sql = "
		SELECT *
		FROM account_info
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}

	
	public static function getAllActive($order=array()){
		$sql = "
		SELECT *
		FROM account_info where account_ACTIVE=1
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}

	public static function getAllAccoutTypes(){
		$sql = "
		SELECT *
		FROM access_level
		ORDER BY access_NAME ASC
		";
		return SQL::find_all($sql);
	}

	public static function isUserExists($username){
		$sql = "
		SELECT 1 
		FROM users
		WHERE user_UNAME = ?";
		$param = array(
			array($username, PDO::PARAM_STR)
		);
		return SQL::find_scalar($sql, $param);
	}
	
	public static function register($userinfo){
		if(self::isUserExists($userinfo['user_UNAME'])){
			SESSION::StoreMsg("Username Already Exists!", "error");
		} elseif ($userinfo['user_UPASS'] != $userinfo['user_CPASS']) {
			SESSION::StoreMsg("Passwords did not matched!", "error");
		} else {
			$sql = "
			INSERT INTO	users
			SET
				access_ID = ?,
				user_UNAME = ?,
				user_UPASS = ?,
				user_ACTIVE = ?
			";
			$param = array(
				$userinfo['access_ID'],
				$userinfo['user_UNAME'],
				base64_encode($userinfo['user_UPASS']),
				$userinfo['user_ACTIVE']
			);
			SQL::execute($sql, $param);
			$id = SQL::last_ID();
			$sql = "
			INSERT INTO	users_info
			SET 
				user_ID = ?,
				user_FNAME = ?,
				user_MNAME = ?,
				user_LNAME = ?,
				user_EMAIL = ?
			";
			$param = array(
				$id,
				$userinfo['user_FNAME'],
				$userinfo['user_MNAME'],
				$userinfo['user_LNAME'],
				$userinfo['user_EMAIL']
			);
			SQL::execute($sql, $param);
			SESSION::StoreMsg("User Successfully Registered!", "success");
			href("users.php");
		}		
	}
	
	
	public static function isEmailExists($email){
		$sql = "
		SELECT 1 
		FROM users_info
		WHERE user_EMAIL = ?";
		$param = array(
			array($email, PDO::PARAM_STR)
		);
		return SQL::find_scalar($sql, $param);
	}
	
	

	public static function update($id, $userinfo, $file = NULL){
		global $user;
		$sql = "
		UPDATE users
		SET
			access_ID = ?,
			user_ACTIVE = ?
		WHERE user_ID = ?
		";
		$param = array(
			$userinfo['access_ID'],
			$userinfo['user_ACTIVE'],
			$id
		);
		SQL::execute($sql, $param);
		$sql = "
		UPDATE users_info
		SET 
			user_FNAME = ?,
			user_MNAME = ?,
			user_LNAME = ?,
			user_EMAIL = ?
		WHERE user_ID = ?
		";
		$param = array(
			$userinfo['user_FNAME'],
			$userinfo['user_MNAME'],
			$userinfo['user_LNAME'],
			$userinfo['user_EMAIL'],
			$id
		);
		SQL::execute($sql, $param);
		SESSION::StoreMsg("User Successfully Updated!", "success");

		//Change Profile Picture
		if($file && FILE::checkUpload($file)) {
			self::changePicture($id, $file);
		}

		if($user['access_ID'] == 3){ 
			href("users.php?action=edit&id=$id");	
		}else{
			href("users.php");	
		}
		
	}

	public static function changePicture($user_id, $file) {
		$user = self::getID($user_id);
		$file_id = FILE::upload($file);
		$file = FILE::getID($file_id);
		if(!FILE::isSupported($file['file_TYPE'], "Image")) {
			return FALSE;
		}
		if($user_id['file_ID']) {
			FILE::delete($user['file_ID']);
		}
		$sql = "
		UPDATE users
		SET file_ID = ?
		WHERE user_ID = ?
		";
		SQL::execute($sql, array($file_id, $user['user_ID']));
	}
	
	
	
	//forusersonly
	public static function user_update($id, $userinfo, $file = NULL){
		global $user;
		
		$sql = "
		UPDATE users_info
		SET 
			user_FNAME = ?,
			user_MNAME = ?,
			user_LNAME = ?,
			user_EMAIL = ?
		WHERE user_ID = ?
		";
		$param = array(
			$userinfo['user_FNAME'],
			$userinfo['user_MNAME'],
			$userinfo['user_LNAME'],
			$userinfo['user_EMAIL'],
			$id
		);
		SQL::execute($sql, $param);
		SESSION::StoreMsg("User Successfully Updated!", "success");

		//Change Profile Picture
		if($file && FILE::checkUpload($file)) {
			self::changePicture($id, $file);
		}

		if($user['access_ID'] == 3){ 
		
		unset($user);
		$user = $userinfo;
		
		
		
			href("users.php?action=edit_profile&id=$id");	
		}else{
			href("users.php");	
		}
		
	}
	
	
	
	
	
	
	public static function changePassword($user_ID, $userinfo){
		$user = self::getID($user_ID);
		
		#print_r($user);
		
		if($userinfo['npassword'] != $userinfo['vpassword']){
			SESSION::StoreMsg("Passwords did not matched!", "error");
		} elseif (base64_encode($userinfo['cpassword']) != $user['account_PASS']) {
		
	#	echo "1".$userinfo['cpassword']." 2".$user['account_PASS'];
		
			SESSION::StoreMsg("Old Password Incorrect!", "error");
		} else {
			$sql = "
			UPDATE account_info
			SET account_PASS = ?
			WHERE account_ID = ?
			";
			$param = array(
				array(base64_encode($userinfo['npassword']), PDO::PARAM_STR),
				array($user_ID, PDO::PARAM_INT)
			);
			SQL::execute($sql, $param);
			SESSION::StoreMsg("Password Successfully Changed!", "success");
		}
	}
	
	
	
	public static function changePasswordAJAX($uid, $cpassword,$npassword, $vpassword){
		$user = self::getID($uid);
		if($npassword != $vpassword){
			#SESSION::StoreMsg("Passwords did not matched!", "error");
			#return SESSION::DisplayCustomMsg('error','New Password and Re-type Password didn\'t matched');
			$status = 'E_NR';
		} elseif (base64_encode($cpassword) != $user['account_PASS']) {
			#SESSION::StoreMsg("Old Password Incorrect!", "error");
			#return SESSION::DisplayCustomMsg('error','Incorrect Old Password');;
			$status = 'I_OP';
		} else {
			$sql = "
			UPDATE account_info
			SET account_PASS = ?, account_CHANGEPASS = 0
			WHERE account_ID = ?
			";
			$param = array(
				array(base64_encode($npassword), PDO::PARAM_STR),
				array($uid, PDO::PARAM_INT)
			);
			#return SESSION::DisplayCustomMsg('success','Password successfully changed!');;
			$status = 'S';
			SQL::execute($sql, $param);
			#SESSION::StoreMsg("Password Successfully Changed!", "success");
		}
		return $status;
	}
	
	
	
	
	

	public static function setActive($id, $status) {
		$sql = "
		UPDATE users
		SET
			user_ACTIVE = ?
		WHERE user_ID = ?
		";
		$param = array($status, $id);
		SQL::execute($sql, $param);
		SESSION::StoreMsg("User successfully updated!", "success");
		href("users.php");
	}

	public static function getName($user) {
		return $user['user_FNAME'].' '.$user['user_LNAME'];
	}


	public static function changePasswordUser($userinfo){
		$user = self::getID($userinfo['user_ID']);
		$user_ID = $userinfo['user_ID'];
		
		if($userinfo['npassword'] != $userinfo['vpassword']){
			SESSION::StoreMsg("Passwords did not matched!", "error");
			return 0;
		} else {
			$sql = "
			UPDATE users
			SET user_UPASS = ?
			WHERE user_ID = ?
			";
			$param = array(
				array(base64_encode($userinfo['npassword']), PDO::PARAM_STR),
				array($user_ID, PDO::PARAM_INT)
			);
			
			SQL::execute($sql, $param);
			SESSION::StoreMsg("Password Successfully Changed!", "info");
			
			return 1;
		}
		
		
	}
	
	
	
	
	public static function getEmail($email){
		
			$sql = "
			SELECT * FROM
			users_info U1, users U2
			WHERE U1.user_ID = U2.user_ID and
			U1.user_EMAIL = ?
			";
			$param = array(
				array($email, PDO::PARAM_STR)
			);
		
		return SQL::find_id($sql, $param);
	}
	
	
	public static function getUsername($un){
		
			$sql = "
			SELECT * FROM
			users_info U1, users U2
			WHERE U1.user_ID = U2.user_ID and
			U2.user_UNAME = ?
			";
			$param = array(
				array($un, PDO::PARAM_STR)
			);
		
		return SQL::find_id($sql, $param);
	}


	public static function sendEmail($u){
		
		$id = $u['user_ID'];
		$key = strtotime(date("Y-m-d H:i:s")).uniqid (rand ());
		$datenow = date("Y-m-d H:i:s");
	
		$sql = "INSERT INTO password_reset set
				request_KEY = '$key',
				user_ID = '$id',
				datetime = '$datenow'";
				SQL::execute($sql);
				
		$to = $u['user_EMAIL'];
		$subject = "Reset Password | IDLE";
		$message = "Please Click the link to reset: 
		http://idle.itekniks.net/lost.php?key=$key

		-IDLE
		";
		$from = "no-reply@idle.com";
		$headers = "From:" . $from;		
			mail($to,$subject,$message,$headers);	
			SESSION::StoreMsg("Email containing password reset sent!", "info");
	
	}
	
	
	public static function getkey($key){
		
			$sql = "
			SELECT * FROM
			users_info U1, users U2, password_reset U3
			WHERE U1.user_ID = U2.user_ID and
			U3.user_ID = U2.user_ID and U3.request_KEY = ?
			";
			$param = array(
				array($key, PDO::PARAM_STR)
			);
		
		return SQL::find_id($sql, $param);
	}


	public static function deletekey($key){
		
			$sql = "
			DELETE FROM
			password_reset
			WHERE request_KEY = ?
			";
			$param = array(
				array($key, PDO::PARAM_STR)
			);
		
		return SQL::find_id($sql, $param);
	}
	
	
	
	
	
	
	public static function addBulk($userinfo){
		if(self::isUserExists($userinfo['user_UNAME'])){
			SESSION::StoreMsg("Username Already Exists!", "error");
		} elseif ($userinfo['user_UPASS'] != $userinfo['user_CPASS']) {
			SESSION::StoreMsg("Passwords did not matched!", "error");
		} else {
			$sql = "
			INSERT INTO	users
			SET
				access_ID = ?,
				user_UNAME = ?,
				user_UPASS = ?,
				user_ACTIVE = ?
			";
			$param = array(
				$userinfo['access_ID'],
				$userinfo['user_UNAME'],
				base64_encode($userinfo['user_UPASS']),
				$userinfo['user_ACTIVE']
			);
			SQL::execute($sql, $param);
			$id = SQL::last_ID();
			$sql = "
			INSERT INTO	users_info
			SET 
				user_ID = ?,
				user_FNAME = ?,
				user_MNAME = ?,
				user_LNAME = ?,
				user_EMAIL = ?
			";
			$param = array(
				$id,
				$userinfo['user_FNAME'],
				$userinfo['user_MNAME'],
				$userinfo['user_LNAME'],
				$userinfo['user_EMAIL']
			);
			SQL::execute($sql, $param);
			return $id;
		}		
	}
		
    #MOJA
	public static function getRequirements($order=array()){
		$sql = "
		SELECT *
		FROM student_requirements
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}

	public static function addstudent($userinfo){
	
            $birthdate=$userinfo['user_YEAR'].'-'.$userinfo['user_MONTH'].'-'.$userinfo['user_DATE'];		
			$choose = $userinfo['student_choose'];
			
			if($choose == 'auto')
			{
				$auto = $userinfo['auto1'];
				#msgbox($auto);
				#echo $auto;
				$si_ID = USERS::getLastNumber($auto);
				$si_ID =   $auto.str_pad($si_ID['si_ID']+1,3,0,STR_PAD_LEFT);
				#echo $si_ID;
				#exit();
			}else{
					$si_ID = $userinfo['user_STUDENT_ID'];
			}
			
			$sql = "
			INSERT INTO	student_information
			SET
				si_FNAME = ?,
                si_MNAME = ?,
                si_LNAME = ?,
                /*si_BIRTHDATE = ?,*/
                si_BIRTHYEAR = ?,
                si_BIRTHMONTH = ?,
                si_BIRTHDAY = ?,
                si_BIRTHPLACE = ?,
                si_GENDER = ?,
                student_ID = ?,
                password = ?,
				si_STREET = ?,
				si_BRGY = ?,
				si_CITY = ?,
				si_DISTRICT = ?,
				si_PROVINCE = ?,
				si_EMAIL = ?,
                si_CONTACT = ?, 
                student_type_ID = ?,
                access_ID =?,
				added_by = ?,
				admitted_date= ?,
				course_ID = ?
                
			";
			$param = array(
			
				array(	strtoupper($userinfo['user_FNAME']), PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_MNAME']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_LNAME']),PDO::PARAM_STR),
				/*array(  $birthdate,PDO::PARAM_STR),*/
				array(	strtoupper($userinfo['user_YEAR']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_MONTH']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_DATE']),PDO::PARAM_STR),

				array(	$userinfo['user_placeADDRESS'],PDO::PARAM_STR),
				array(	$userinfo['user_GENDER'],PDO::PARAM_STR),
				array(  $si_ID,PDO::PARAM_STR),
				array(	base64_encode("1234"),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_STREET']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_BRGY']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_CITY']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_DISTRICT']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_PROVINCE']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_EMAIL']),PDO::PARAM_STR),
				array(	$userinfo['present_ContactNo'],PDO::PARAM_STR),
				array(	$userinfo['student_type_ID'],PDO::PARAM_STR),
				6,
				array(	$userinfo['user_ADDEDBY'],PDO::PARAM_STR),
				date("Y-m-d",strtotime($userinfo['user_DATEADMITTED'])),
				array(	$userinfo['course_ID'],PDO::PARAM_STR)
			);
			$user_id = '1';
            $action_event = "Add";
            $input = array(
            		'Student No.'=>$si_ID,
            		'First Name'=>$userinfo['user_FNAME'],
            		'Middle Name'=>$userinfo['user_MNAME'],
            		'Last Name'=>$userinfo['user_LNAME'],
            		'Month'=>$userinfo['user_MONTH'],
            		'Date'=>$userinfo['user_DATE'],
            		'Year'=>$userinfo['user_YEAR'],
            		'Gender'=>$userinfo['user_GENDER'],
            		'Age'=>$userinfo['user_age'],
            		'Height(cm)'=>$userinfo['user_Height'],
            		'Weight(kgs)'=>$userinfo['user_Weight'],
            		'Religion'=>$userinfo['user_Religion'],
            		'Civil Status'=>$userinfo['user_CivilStatus'],
            		'Speacial Skills'=>$userinfo['user_SpecialSkill'],
            		'Place of Birth'=>$userinfo['user_placeADDRESS']
            );
            $output = implode(', ', array_map(
			    function ($v, $k) {
			        if(is_array($v)){
			            return $k.'[]:'.implode('&'.$k.'[]:', $v);
			        }else{
			            return $k.':'.$v;
			        }
			    }, 
			    $input, 
			    array_keys($input)
			));
           	
           	if($userinfo['student_type_ID'] == '1'){

           		if($userinfo['1'] == '1'){
	           		$f138 = 'PASSED';
	           	}else{
	           		$f138 = 'NOT PASSED';
	           	}
	           	if($userinfo['2'] == '1'){
	           		$f137 = 'PASSED';
	           	}else{
	           		$f137 = 'NOT PASSED';
	           	}
	           	if($userinfo['3'] == '1'){
	           		$gmc = 'PASSED';
	           	}else{
	           		$gmc = 'NOT PASSED';
	           	}
	           	if($userinfo['4'] == '1'){
	           		$nso = 'PASSED';
	           	}else{
	           		$nso = 'NOT PASSED';
	           	}
	           	if($userinfo['5'] == '1'){
	           		$pic = 'PASSED';
	           	}else{
	           		$pic = 'NOT PASSED';
	           	}
           		$requirements = array(
					'F138'=>$f138,
					'F137'=>$f138,
					'Good Moral Certificate'=>$gmc,
					'NSO Birth Certificate'=>$nso,
					'1x1 Picture 2px'=>$pic
				);
				$output2 = implode(', ', array_map(
				    function ($v, $k) {
				        if(is_array($v)){
				            return $k.'[]:'.implode('&'.$k.'[]:', $v);
				        }else{
				            return $k.':'.$v;
				        }
				    }, 
				    $requirements, 
				    array_keys($requirements)
				));
	        }elseif($userinfo['student_type_ID'] == '2'){
	        	if($userinfo['3'] == '1'){
	           		$gmc = 'PASSED';
	           	}else{
	           		$gmc = 'NOT PASSED';
	           	}
	           	if($userinfo['4'] == '1'){
	           		$nso = 'PASSED';
	           	}else{
	           		$nso = 'NOT PASSED';
	           	}
	           	if($userinfo['5'] == '1'){
	           		$pic = 'PASSED';
	           	}else{
	           		$pic = 'NOT PASSED';
	           	}
        		if($userinfo['6'] == '1'){
	           		$honorable = 'PASSED';
	           	}else{
	           		$honorable = 'NOT PASSED';
	           	}
	           	if($userinfo['7'] == '1'){
	           		$cog = 'PASSED';
	           	}else{
	           		$cog = 'NOT PASSED';
	           	}
	           	if($userinfo['8'] == '1'){
	           		$tor = 'PASSED';
	           	}else{
	           		$tor = 'NOT PASSED';
	           	}
	        	$requirements = array(
					'Good Moral Certificate'=>$gmc,
					'NSO Birth Certificate'=>$nso,
					'1x1 Picture 2px'=>$pic,
					'Honorable Dismissal'=>$honorable,
					'Copy of Grades'=>$cog,
					'Transcript of Records'=>$tor
				);
				$output2 = implode(', ', array_map(
				    function ($v, $k) {
				        if(is_array($v)){
				            return $k.'[]:'.implode('&'.$k.'[]:', $v);
				        }else{
				            return $k.':'.$v;
				        }
				    }, 
				    $requirements, 
				    array_keys($requirements)
				));
	        }else{
	        	if($userinfo['4'] == '1'){
	           		$nso = 'PASSED';
	           	}else{
	           		$nso = 'NOT PASSED';
	           	}
	           	if($userinfo['5'] == '1'){
	           		$pic = 'PASSED';
	           	}else{
	           		$pic = 'NOT PASSED';
	           	}
	           	if($userinfo['9'] == '1'){
	           		$als_res = 'PASSED';
	           	}else{
	           		$als_res = 'NOT PASSED';
	           	}
	           	if($userinfo['10'] == '1'){
	           		$alscc = 'PASSED';
	           	}else{
	           		$alscc = 'NOT PASSED';
	           	}
	        	$requirements = array(
					'NSO Birth Certificate'=>$nso,
					'1x1 Picture 2px'=>$pic,
					'ALS Result'=>$als_res,
					'ALS Completion Certificate'=>$alscc
				);
				$output2 = implode(', ', array_map(
				    function ($v, $k) {
				        if(is_array($v)){
				            return $k.'[]:'.implode('&'.$k.'[]:', $v);
				        }else{
				            return $k.':'.$v;
				        }
				    }, 
				    $requirements, 
				    array_keys($requirements)
				));
	        }
			$input3 = array(
					'Guardian Name'=>$userinfo['user_GUARDIAN'],
					'Relationship'=>$userinfo['user_RELATIONSHIP'],
					'Occupation'=>$userinfo['user_OCCUPATION'],
					'Contact No.'=>$userinfo['user_GUARDIAN_CONTACT'],
					'Guardian Address'=>$userinfo['user_GUARDIAN_ADDRESS']
			);
			$output3 = implode(', ', array_map(
			    function ($v, $k) {
			        if(is_array($v)){
			            return $k.'[]:'.implode('&'.$k.'[]:', $v);
			        }else{
			            return $k.':'.$v;
			        }
			    }, 
			    $input3, 
			    array_keys($input3)
			));
			$input4 = array(
					'Elementary'=>$userinfo['user_ELEMENTARY'],
					'Date Graduated'=>$userinfo['user_ELEM_YR'],
					'High School'=>$userinfo['user_HIGHSCHOOL'],
					'Date Graduated'=>$userinfo['user_HS_YR'],
					'College Last Attended'=>$userinfo['user_COLLEGE_LAST_ATTENDED'],
					'Date'=>$userinfo['user_CLA_DATE'],

			);
			$output4 = implode(', ', array_map(
			    function ($v, $k) {
			        if(is_array($v)){
			            return $k.'[]:'.implode('&'.$k.'[]:', $v);
			        }else{
			            return $k.':'.$v;
			        }
			    }, 
			    $input4, 
			    array_keys($input4)
			));
			$event_desc = "Registrar / Find Student, DESCRIPTION: User Added New Record. Student Information: ".$output.', Present Address:'.$userinfo['user_placeADDRESS'].' '.$userinfo['user_STREET'].' '.$userinfo['user_BRGY'].' '.$userinfo['user_CITY'].' '.$userinfo['user_DISTRICT'].' '.$userinfo['user_PROVINCE'].' '.$userinfo['present_ContactNo'].', Permanent Address:'.$userinfo['permanent_STREET'].' '.$userinfo['permanent_BRGY'].' '.$userinfo['permanent_CITY'].' '.$userinfo['permanent_DISTRICT'].' '.$userinfo['permanent_PROVINCE'].' '.$userinfo['permanent_ContactNo'].' '.', Email Address:'.$userinfo['user_EMAIL'].' ,Program:'.$userinfo['course_ID'].', Curriculum:'.$userinfo['curricullum_ID'].', Student Type:'.$userinfo['student_type_ID'].' '.$output2.'.  Contact Person (Parent/Guardian) In Case of Emergency. '.$output3.'. Educational Attainment.'.$output4;
            $audit = Audit_final::audit_trail($user_id, $action_event,$event_desc);
			SQL::execute($sql, $param);
			

			//Get Last ID
			$id = SQL::last_ID();
			
			//Add curicullum
			CECONTROLLER::addfromadd($id,$userinfo['curricullum_ID']);
			
			//Aditional Info
			$sql = "
			INSERT INTO	student_additional_info
			SET 
                
				si_ID = ?,
                sai_name_of_guardian = ?,
                sai_relationship = ?,  
                sai_occupation = ?,
				sai_tel_cell_number = ?,
				sai_address = ?,
				sai_elementary_graduated = ?,
				sai_elementary_year_graduated = ?,
				sai_highschool_graduated = ?,
				sai_highschool_year_graduated = ?,
				sai_college_last_attended = ?,
				sai_cla_inclusive_date = ?,
				sai_degree_title = ?,
				sai_religion = ?,
				sai_civil = ?,
				sai_height = ?,
				sai_weight = ?,
				sai_special_skills = ?,
				sai_present_addr_st =?,
				sai_present_brgy = ?,
				sai_present_city = ?,
				sai_present_district = ?,
				sai_present_prov =?,
				sai_present_contact = ?
			";
			$param = array(
				$id,
                strtoupper($userinfo['user_GUARDIAN']),
				strtoupper($userinfo['user_RELATIONSHIP']),
                strtoupper($userinfo['user_OCCUPATION']),
                strtoupper($userinfo['user_GUARDIAN_CONTACT']),
                strtoupper($userinfo['user_GUARDIAN_ADDRESS']),
				$userinfo['user_ELEMENTARY'],
				$userinfo['user_ELEM_YR'],
				$userinfo['user_HIGHSCHOOL'],
				$userinfo['user_HS_YR'],
				$userinfo['user_COLLEGE_LAST_ATTENDED'],
				$userinfo['user_CLA_DATE'],
				$userinfo['user_DEGREE'],
				$userinfo['user_Religion'],
				$userinfo['user_CivilStatus'],
				$userinfo['user_Height'],
				$userinfo['user_Weight'],
				$userinfo['user_SpecialSkill'],
				$userinfo['permanent_STREET'],
				$userinfo['permanent_BRGY'],
				$userinfo['permanent_CITY'],
				$userinfo['permanent_DISTRICT'],
				$userinfo['permanent_PROVINCE'],
				$userinfo['permanent_ContactNo']

			);
			SQL::execute($sql, $param);
			
			
			//Requirements
			$requirements = USERS::getStudentTypeRequirements($userinfo['student_type_ID']);
			$datenow = date("Y-m-d H:i:s");
				foreach($requirements as $key => $value)
				{
				
					$req_ID= $value['req_ID'];
					$sql = "
					INSERT INTO	student_requirements_passed
					SET 
						si_ID = ?,
						req_ID = ?,
						status = ?,
						date = ?
					";
					
					$param = array(
						$id,
						$value['req_ID'],
						$userinfo[$value['req_ID']],
						$datenow
						);
					
					SQL::execute($sql, $param);		
				}
			
			//Display message
			$lname = $userinfo['user_LNAME'];
			$fname = $userinfo['user_FNAME'];
			
			msgbox("Student No: $si_ID ($lname , $fname)");
			
			
			
				#AUDIT TRAIL#
				$user = $_SESSION['USER']['account_ID'];
				$action = "Added Student #$si_ID (Reference=$id)";
				$module = "Student Information";
				AUDIT::ins($user,$module, $action);
				#AUDIT TRAIL#
			
			
			SESSION::StoreMsg("User Successfully Added!", "success");
			#href("users.php");	
	}
	
	
    public static function update_student($id,$userinfo){
			$sql = "
			UPDATE	student_information
			SET
                
				si_FNAME = ?,
                si_MNAME = ?,
                si_LNAME = ?,
                si_BIRTHYEAR = ?,
                si_BIRTHMONTH = ?,
                si_BIRTHDAY = ?,
                si_GENDER = ?,
                student_ID = ?,
                si_STREET = ?,
				si_BRGY = ?,
				si_CITY = ?,
				si_DISTRICT = ?,
				si_PROVINCE = ?,
				si_EMAIL = ?,
                -- si_CONTACT = ?, 
                student_type_ID = ?,
				admitted_date = ?,
              	student_admit_ID = ?,
              	course_ID = ?
				WHERE si_ID = ?
			";
			$data = array(
					'First Name'=>$userinfo['st_fname'],
					'Middle Name'=>$userinfo['st_mname'],
					'Last Name'=>$userinfo['st_lname'],
					'Birth Month'=>$userinfo['st_month'],
					'Birth Day'=>$userinfo['st_day'],
					'Birth Year'=>$userinfo['st_year'],
					'Gender'=>$userinfo['st_gender'],
					'Age'=>$userinfo['st_age'],
					'Height(cm)'=>$userinfo['st_height'],
					'Weight(kgs)'=>$userinfo['st_weight'],
					'Religion'=>$userinfo['st_religion'],
					'Civil Status'=>$userinfo['st_civil'],
					'Special Skills'=>$userinfo['st_sk'],
					'Place of Birth'=>$userinfo['st_birthplace'],
					'Present Street No. And Street Address'=>$userinfo['st_street'],
					'Present Barangay'=>$userinfo['st_brgy'],
					'Present City'=>$userinfo['st_city'],
					'Present District'=>$userinfo['st_district'],
					'Present Province'=>$userinfo['st_province'],
					'Present Contact No.'=>$userinfo['st_contact'],
					'Permanent Street No. And Street Address'=>$userinfo['st_street1'],
					'Permanent Barangay'=>$userinfo['st_brgy1'],
					'Permanent City'=>$userinfo['st_city1'],
					'Permanent District'=>$userinfo['st_district1'],
					'Permanent Province'=>$userinfo['st_province1'],
					'Permanent Contact No.'=>$userinfo['st_contact1'],
					'Email Address'=>$userinfo['st_email'],
					'Program'=>$userinfo['st_course_id'],
					'Curriculum'=>$userinfo['st_curricul_id'],
					'Student Type'=>$userinfo['st_type_id'],
					'Guardian Name'=>$userinfo['st_g_name'],
					'Relationship'=>$userinfo['st_g_rel'],
					'Occupation'=>$userinfo['st_g_occu'],
					'Contact No.'=>$userinfo['st_g_no'],
					'Guardian Address'=>$userinfo['st_g_address'],
					'Elementary'=>$userinfo['st_g_eg'],
					'Date Graduated'=>$userinfo['st_g_eyg'],
					'High School'=>$userinfo['st_g_hs'],
					'Date Graduated'=>$userinfo['st_g_hsyg'],
					'College Last Attended'=>$userinfo['st_cla'],
					'Date'=>$userinfo['st_cid'],
					'Student Status'=>$userinfo['st_s_id'],
					'Graduating'=>$userinfo['st_g'],
					'How did you know about us'=>$userinfo['st_s_a_id'],	
			);
			$data1 = array(
					'First Name'=>$userinfo['user_FNAME'],
					'Middle Name'=>$userinfo['user_MNAME'],
					'Last Name'=>$userinfo['user_LNAME'],
					'Birth Month'=>$userinfo['user_MONTH'],
					'Birth Day'=>$userinfo['user_DATE'],
					'Birth Year'=>$userinfo['user_YEAR'],
					'Gender'=>$userinfo['user_GENDER'],
					'Age'=>$userinfo['user_age'],
					'Height(cm)'=>$userinfo['user_Height'],
					'Weight(kgs)'=>$userinfo['user_Weight'],
					'Religion'=>$userinfo['user_Religion'],
					'Civil Status'=>$userinfo['user_CivilStatus'],
					'Special Skills'=>$userinfo['user_SpecialSkill'],
					'Place of Birth'=>$userinfo['user_placeADDRESS'],
					'Present Street No. And Street Address'=>$userinfo['permanent_STREET'],
					'Present Barangay'=>$userinfo['permanent_BRGY'],
					'Present City'=>$userinfo['permanent_CITY'],
					'Present District'=>$userinfo['permanent_DISTRICT'],
					'Present Province'=>$userinfo['permanent_PROVINCE'],
					'Present Contact No.'=>$userinfo['permanent_ContactNo'],
					'Permanent Street No. And Street Address'=>$userinfo['user_STREET'],
					'Permanent Barangay'=>$userinfo['user_BRGY'],
					'Permanent City'=>$userinfo['user_CITY'],
					'Permanent District'=>$userinfo['user_DISTRICT'],
					'Permanent Province'=>$userinfo['user_PROVINCE'],
					'Permanent Contact No.'=>$userinfo['present_ContactNo'],
					'Email Address'=>$userinfo['user_EMAIL'],
					'Program'=>$userinfo['course_ID'],
					'Curriculum'=>$userinfo['curricullum_ID'],
					'Student Type'=>$userinfo['student_type_ID'],
					'Guardian Name'=>$userinfo['user_GUARDIAN'],
					'Relationship'=>$userinfo['user_RELATIONSHIP'],
					'Occupation'=>$userinfo['user_OCCUPATION'],
					'Contact No.'=>$userinfo['user_GUARDIAN_CONTACT'],
					'Guardian Address'=>$userinfo['user_GUARDIAN_ADDRESS'],
					'Elementary'=>$userinfo['user_ELEMENTARY'],
					'Date Graduated'=>$userinfo['user_ELEM_YR'],
					'High School'=>$userinfo['user_HIGHSCHOOL'],
					'Date Graduated'=>$userinfo['user_HS_YR'],
					'College Last Attended'=>$userinfo['user_COLLEGE_LAST_ATTENDED'],
					'Date'=>$userinfo['user_CLA_DATE'],
					'Date'=>$userinfo['st_cid'],
					'Student Status'=>$userinfo['student_status'],
					'Graduating'=>$userinfo['student_graduating'],
					'How did you know about us'=>$userinfo['student_admit_ID'],
			);
			$before = array_diff($data, $data1);
			$after = array_diff($data1, $data);
			
			$b_fore = preg_replace('/[{-}"]/', '', json_encode($before));
			$a_fter = preg_replace('/[{-}"]/', '', json_encode($after));
			if($userinfo['student_type_ID'] == 1 ){
				$requirements = array(
					'F138'=>$userinfo['11'],
					'F137'=>$userinfo['21'],
					'Good Moral Certificate'=>$userinfo['31'],
					'NSO Birth Certificate'=>$userinfo['41'],
					'1x1 picture 2pcs'=>$userinfo['51']
				);
				$requirements2 = array(
					'F138'=>$userinfo['1'],
					'F137'=>$userinfo['2'],
					'Good Moral Certificate'=>$userinfo['3'],
					'NSO Birth Certificate'=>$userinfo['4'],
					'1x1 picture 2pcs'=>$userinfo['5']
				);
				$sample = array_diff_assoc($requirements, $requirements2);
				$sample2 = array_diff_assoc($requirements2, $requirements);
				$requim = preg_replace('/[{-}"]/', '', json_encode($sample));
				$requim2 = preg_replace('/[{-}"]/', '', json_encode($sample2));

				
			}elseif($userinfo['student_type_ID'] == 2){
				$requirements = array(
					'Good Moral Certificate'=>$userinfo['31'],
					'NSO Birth Certificate'=>$userinfo['41'],
					'1x1 picture 2pcs'=>$userinfo['51'],
					'Honorable Dismissal'=>$userinfo['61'],
					'Copy of Grades'=>$userinfo['71'],
					'Transcript of Records'=>$userinfo['81']
				);
				$requirements2 = array(
					'Good Moral Certificate'=>$userinfo['3'],
					'NSO Birth Certificate'=>$userinfo['4'],
					'1x1 picture 2pcs'=>$userinfo['5'],
					'Honorable Dismissal'=>$userinfo['6'],
					'Copy of Grades'=>$userinfo['7'],
					'Transcript of Records'=>$userinfo['8']
				);
				$sample = array_diff_assoc($requirements, $requirements2);
				$sample2 = array_diff_assoc($requirements2, $requirements);
				$requim = preg_replace('/[{-}"]/', '', json_encode($sample));
				$requim2 = preg_replace('/[{-}"]/', '', json_encode($sample2));
			}elseif($userinfo['student_type_ID'] == 3){
				$requirements = array(
					'NSO Birth Certificate'=>$userinfo['41'],
					'1x1 picture 2pcs'=>$userinfo['51'],
					'ALS Result'=>$userinfo['91'],
					'ALS Completion Certificate'=>$userinfo['101']
				);
				$requirements2 = array(
					'NSO Birth Certificate'=>$userinfo['4'],
					'1x1 picture 2pcs'=>$userinfo['5'],
					'ALS Result'=>$userinfo['9'],
					'ALS Completion Certificate'=>$userinfo['10']
				);
				$sample = array_diff_assoc($requirements, $requirements2);
				$sample2 = array_diff_assoc($requirements2, $requirements);
				$requim = preg_replace('/[{-}"]/', '', json_encode($sample));
				$requim2 = preg_replace('/[{-}"]/', '', json_encode($sample2));
			}
			$user_id = $userinfo['account_id'];
            $action_event = "Update";
           if($sample == null || $sample == ""){
           		
           		if($after == null || $after == ""){
           			$event_desc = "MODULE: Subject Schedule <br>DESCRIPTION: NO CHANGES";
           		}else{
           			$event_desc = "Module Registrar / Find Student. Description User Updated Information of student with a student number of ".$userinfo['user_STUDENT_ID'].'. BEFORE: '.$b_fore.'. AFTER: '.$a_fter;
           		}
           }else{
           		if($after == null || $after == ""){
           			$event_desc = "Module Registrar / Find Student. Description User Updated Information of student with a student number of ".$userinfo['user_STUDENT_ID'].'. BEFORE: '.$requim.'. AFTER: '.$requim2;
           		}else{
           			$event_desc = "Module Registrar / Find Student. Description User Updated Information of student with a student number of ".$userinfo['user_STUDENT_ID'].'. BEFORE: '.$b_fore.' '.$requim.'. AFTER: '.$a_fter.' '.$requim2;
           		}

           }
  	       

  	       	
  	       Audit_final::audit_trail($user_id, $action_event,$event_desc);
        
			$param = array(
				$userinfo['user_FNAME'],
				$userinfo['user_MNAME'],
				$userinfo['user_LNAME'],
                $userinfo['user_YEAR'],
                $userinfo['user_MONTH'],
                $userinfo['user_DATE'],
				$userinfo['user_GENDER'],
                $userinfo['user_STUDENT_ID'],
				array(	strtoupper($userinfo['user_STREET']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_BRGY']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_CITY']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_DISTRICT']),PDO::PARAM_STR),
				array(	strtoupper($userinfo['user_PROVINCE']),PDO::PARAM_STR),
				$userinfo['user_EMAIL'],
				// $userinfo['user_CONTACT'],
				$userinfo['student_type_ID'],
				date("Y-m-d", strtotime($userinfo['user_DATEADMITTED'])),
				$userinfo['student_admit_ID'],
				$userinfo['course_ID'],
                $id
			);
			SQL::execute($sql, $param);
			
			
		
			$sql = "
			UPDATE	student_additional_info
			SET 
                sai_name_of_guardian = ?,
                sai_relationship = ?,  
                sai_occupation = ?,
				sai_tel_cell_number = ?,
				-- sai_address = ?,
				sai_elementary_graduated = ?,
				sai_elementary_year_graduated = ?,
				sai_highschool_graduated = ?,
				sai_highschool_year_graduated = ?,
				sai_college_last_attended = ?,
				sai_cla_inclusive_date = ?,
				sai_degree_title = ?,
				sai_status = ?,
				sai_graduating = ?,
				sai_height = ?,
				sai_weight = ?
            WHERE
                si_ID = ?
            AND sai_ID = ?
			";
			$param = array(
                $userinfo['user_GUARDIAN'],
				$userinfo['user_RELATIONSHIP'],
                $userinfo['user_OCCUPATION'],
                $userinfo['user_GUARDIAN_CONTACT'],
                // $userinfo['user_ADDRESS'],
				$userinfo['user_ELEMENTARY'],
				$userinfo['user_ELEM_YR'],
				$userinfo['user_HIGHSCHOOL'],
				$userinfo['user_HS_YR'],
				$userinfo['user_COLLEGE_LAST_ATTENDED'],
				$userinfo['user_CLA_DATE'],
				$userinfo['user_DEGREE'],
				$userinfo['student_status'],
				$userinfo['student_graduating'],
				$userinfo['user_Height'],
				$userinfo['user_Weight'],
                $id,
                $userinfo['sai_ID']
			);
			SQL::execute($sql, $param);
			
			
			
			
			$requirements = USERS::getStudentTypeRequirements($userinfo['student_type_ID']);
			$datenow = date("Y-m-d H:i:s");
			
				foreach($requirements as $key => $value)
				{
				
					#$count = mysql_num_rows(mysql_query("SELECT * FROM student_requirements_passed where req_ID = '$value[req_ID]' and si_ID = '$id'"));
					$count = USERS::isExistReq($value['req_ID'],$id);
					
					
					#msgbox($count);
					if($count > 0)
					{
				
							$req_ID= $value['req_ID'];
							$sql = "
							UPDATE student_requirements_passed
							SET 
								
								status = ?,
								date = ?
							where si_ID = ? and
								req_ID = ?
							";
							
							$param = array(
								$userinfo[$value['req_ID']],
								$datenow,
								$id,
								$value['req_ID']
							);	
							SQL::execute($sql, $param);		
					}else{
							$req_ID= $value['req_ID'];
							$sql = "
							INSERT INTO	student_requirements_passed
							SET 
								si_ID = ?,
								req_ID = ?,
								status = ?,
								date = ?
							";
							
							$param = array(
								$id,
								$value['req_ID'],
								$userinfo[$value['req_ID']],
								$datenow
								);
							
							SQL::execute($sql, $param);	
					}
			}

			##CECONTROLLER::update2
			$enrolled = CECONTROLLER::getEnrolled($id); 
			if(!($enrolled)){
				#CECONTROLLER::update2($id);
            	CECONTROLLER::addfromadd($id,$userinfo['curricullum_ID']);
			}
            if($enrolled['curricullum_ID'] != $userinfo['curricullum_ID']){
            	CECONTROLLER::update2($id);
            	CECONTROLLER::addfromadd($id,$userinfo['curricullum_ID']);
            }

			SESSION::StoreMsg("User Successfully Updated!", "success");
			
				#AUDIT TRAIL#
				$user = $_SESSION['USER']['account_ID'];
				$action = "Updated Student #$si_ID (Reference=$id)";
				$module = "Student Information";
				AUDIT::ins($user,$module, $action);
				#AUDIT TRAIL#

					#href("users.php");

	}

	// hans [START]
	public static function update_student1($id,$userinfo){
	$address = $userinfo['si_STREET'].', '.$userinfo['si_BRGY'].', '.$userinfo['si_CITY'].', '.$userinfo['si_DISTRICT'].', '.$userinfo['si_PROVINCE'];
	$sql = "
	UPDATE 
		student_information
	SET 
		si_FNAME='".$userinfo['si_FNAME']."',
		si_MNAME='".$userinfo['si_MNAME']."',
		si_LNAME='".$userinfo['si_LNAME']."',
		si_BIRTHYEAR='".$userinfo['si_BIRTHYEAR']."',
		si_BIRTHMONTH='".$userinfo['si_BIRTHMONTH']."',
		si_BIRTHDAY='".$userinfo['si_BIRTHDAY']."',
		address='".$address."',
		si_BIRTHPLACE='".$userinfo['si_BIRTHPLACE']."',
		si_GENDER='".$userinfo['si_GENDER']."',
		si_STREET='".$userinfo['si_STREET']."',
		si_BRGY='".$userinfo['si_BRGY']."',
		si_CITY='".$userinfo['si_CITY']."',
		si_DISTRICT='".$userinfo['si_DISTRICT']."',
		si_PROVINCE='".$userinfo['si_PROVINCE']."',
		si_EMAIL='".$userinfo['si_EMAIL']."',
		si_CONTACT='".$userinfo['si_CONTACT']."',
		student_type_ID='".$userinfo['student_type_ID']."',
		course_ID='".$userinfo['course_ID']."'
		WHERE si_ID = $id ";
	SQL::execute1($sql);

	$sql = "
	UPDATE	
		student_additional_info
	SET 
        sai_name_of_guardian='".$userinfo['sai_name_of_guardian']."',
		sai_relationship='".$userinfo['sai_relationship']."',
		sai_occupation='".$userinfo['sai_occupation']."',
		sai_tel_cell_number='".$userinfo['sai_tel_cell_number']."',
		sai_address='".$userinfo['sai_address']."',
		sai_elementary_graduated='".$userinfo['sai_elementary_graduated']."',
		sai_elementary_year_graduated='".$userinfo['sai_elementary_year_graduated']."',
		sai_highschool_graduated='".$userinfo['sai_highschool_graduated']."',
		sai_highschool_year_graduated='".$userinfo['sai_highschool_year_graduated']."',
		sai_college_last_attended='".$userinfo['sai_college_last_attended']."',
		sai_cla_inclusive_date='".$userinfo['sai_cla_inclusive_date']."',
		sai_degree_title='".$userinfo['user_DEGREE']."',
		sai_status=1,
		sai_graduating='".$userinfo['sai_graduating']."',
		sai_religion='".$userinfo['sai_religion']."',
		sai_civil='".$userinfo['sai_civil']."',
		sai_age='".$userinfo['sai_age']."',
		sai_height='".$userinfo['sai_height']."',
		sai_weight='".$userinfo['sai_weight']."',
		sai_present_addr_st='".$userinfo['sai_present_addr_st']."',
		sai_present_brgy='".$userinfo['sai_present_brgy']."',
		sai_present_city='".$userinfo['sai_present_city']."',
		sai_present_district='".$userinfo['sai_present_district']."',
		sai_present_prov='".$userinfo['sai_present_prov']."',
		sai_present_contact='".$userinfo['sai_present_contact']."'
    WHERE
        si_ID = $id
    AND sai_ID = '".$userinfo['sai_ID']."'
	";
	SQL::execute1($sql);

	//deleted = sai_status='".$userinfo['sai_status']."',
	//deleted = sai_graduating='".$userinfo['sai_graduating']."',
	$requirements = USERS::getStudentTypeRequirements($userinfo['student_type_ID']);
			$datenow = date("Y-m-d H:i:s");
			
				foreach($requirements as $key => $value)
				{
				
					#$count = mysql_num_rows(mysql_query("SELECT * FROM student_requirements_passed where req_ID = '$value[req_ID]' and si_ID = '$id'"));
					$count = USERS::isExistReq($value['req_ID'],$id);
					
					
					#msgbox($count);
					if($count > 0)
					{
				
							$req_ID= $value['req_ID'];
							$sql = "
							UPDATE student_requirements_passed
							SET 
								
								status = ?,
								date = ?
							where si_ID = ? and
								req_ID = ?
							";
							
							$param = array(
								$userinfo[$value['req_ID']],
								$datenow,
								$id,
								$value['req_ID']
							);	
							SQL::execute($sql, $param);		
					}else{
							$req_ID= $value['req_ID'];
							$sql = "
							INSERT INTO	student_requirements_passed
							SET 
								si_ID = ?,
								req_ID = ?,
								status = ?,
								date = ?
							";
							
							$param = array(
								$id,
								$value['req_ID'],
								$userinfo[$value['req_ID']],
								$datenow
								);
							
							SQL::execute($sql, $param);	
					}
			}
			
			// SESSION::StoreMsg("User Successfully Updated!", "success");
			##CECONTROLLER::update2
			$enrolled = CECONTROLLER::getEnrolled($id); 
			if(!($enrolled)){
				#CECONTROLLER::update2($id);
            	CECONTROLLER::addfromadd($id,$userinfo['curricullum_ID']);
			}
            if($enrolled['curricullum_ID'] != $userinfo['curricullum_ID']){
            	CECONTROLLER::update2($id);
            	CECONTROLLER::addfromadd($id,$userinfo['curricullum_ID']);
            }

			SESSION::StoreMsg("User Successfully Updated!", "success");
			
				#AUDIT TRAIL#
				$user = $_SESSION['USER']['account_ID'];
				$action = "Updated Student #$si_ID (Reference=$id)";
				$module = "Student Information";
				AUDIT::ins($user,$module, $action);
				#AUDIT TRAIL#

					#href("users.php");
	}
	// hans [END]

	// hans [START]
	public static function update_student_request($id,$userinfo){
		$address = $userinfo['si_STREET'].', '.$userinfo['si_BRGY'].', '.$userinfo['si_CITY'].', '.$userinfo['si_DISTRICT'].', '.$userinfo['si_PROVINCE'];
		$sql = "
		UPDATE 
			request_approval
		SET 
			si_FNAME='".$userinfo['si_FNAME']."',
			si_MNAME='".$userinfo['si_MNAME']."',
			si_LNAME='".$userinfo['si_LNAME']."',
			si_BIRTHYEAR='".$userinfo['si_BIRTHYEAR']."',
			si_BIRTHMONTH='".$userinfo['si_BIRTHMONTH']."',
			si_BIRTHDAY='".$userinfo['si_BIRTHDAY']."',
			address='".$address."',
			si_BIRTHPLACE='".$userinfo['si_BIRTHPLACE']."',
			si_GENDER='".$userinfo['si_GENDER']."',
			si_STREET='".$userinfo['si_STREET']."',
			si_BRGY='".$userinfo['si_BRGY']."',
			si_CITY='".$userinfo['si_CITY']."',
			si_DISTRICT='".$userinfo['si_DISTRICT']."',
			si_PROVINCE='".$userinfo['si_PROVINCE']."',
			si_EMAIL='".$userinfo['si_EMAIL']."',
			si_CONTACT='".$userinfo['si_CONTACT']."',
			student_type_ID='".$userinfo['student_type_ID']."',
			course_ID='".$userinfo['course_ID']."'
			WHERE si_ID = $id ";
		SQL::execute1($sql);

	
		$sql = "
		UPDATE	
			request_approval_add_info
		SET 
			sai_name_of_guardian='".$userinfo['sai_name_of_guardian']."',
			sai_relationship='".$userinfo['sai_relationship']."',
			sai_occupation='".$userinfo['sai_occupation']."',
			sai_tel_cell_number='".$userinfo['sai_tel_cell_number']."',
			sai_address='".$userinfo['sai_address']."',
			sai_elementary_graduated='".$userinfo['sai_elementary_graduated']."',
			sai_elementary_year_graduated='".$userinfo['sai_elementary_year_graduated']."',
			sai_highschool_graduated='".$userinfo['sai_highschool_graduated']."',
			sai_highschool_year_graduated='".$userinfo['sai_highschool_year_graduated']."',
			sai_college_last_attended='".$userinfo['sai_college_last_attended']."',
			sai_cla_inclusive_date='".$userinfo['sai_cla_inclusive_date']."',
			sai_degree_title='".$userinfo['course_NAME']."',
			sai_status=1,
			sai_graduating='".$userinfo['sai_graduating']."',
			sai_religion='".$userinfo['sai_religion']."',
			sai_civil='".$userinfo['sai_civil']."',
			sai_age='".$userinfo['sai_age']."',
			sai_height='".$userinfo['sai_height']."',
			sai_weight='".$userinfo['sai_weight']."',
			sai_present_addr_st='".$userinfo['sai_present_addr_st']."',
			sai_present_brgy='".$userinfo['sai_present_brgy']."',
			sai_present_city='".$userinfo['sai_present_city']."',
			sai_present_district='".$userinfo['sai_present_district']."',
			sai_present_prov='".$userinfo['sai_present_prov']."',
			sai_present_contact='".$userinfo['sai_present_contact']."'
		WHERE
			RAAI_ID = $id
		";
		SQL::execute1($sql);
	
		//deleted = sai_status='".$userinfo['sai_status']."',
		//deleted = sai_graduating='".$userinfo['sai_graduating']."',
		$requirements = USERS::getStudentTypeRequirements($userinfo['student_type_ID']);
				$datenow = date("Y-m-d H:i:s");
				
					foreach($requirements as $key => $value)
					{
					
						#$count = mysql_num_rows(mysql_query("SELECT * FROM student_requirements_passed where req_ID = '$value[req_ID]' and si_ID = '$id'"));
						$count = USERS::isExistReq($value['req_ID'],$id);
						
						
						#msgbox($count);
						if($count > 0)
						{

								$req_ID= $value['req_ID'];
								$sql = "
								UPDATE student_requirements_passed
								SET 
									
									status = ?,
									date = ?
								where si_ID = ? and
									req_ID = ?
								";
								
								$param = array(
									$userinfo[$value['req_ID']],
									$datenow,
									$id,
									$value['req_ID']
								);	
								SQL::execute($sql, $param);		
						}else{
								$req_ID= $value['req_ID'];
								$sql = "
								INSERT INTO	student_requirements_passed
								SET 
									si_ID = ?,
									req_ID = ?,
									status = ?,
									date = ?
								";
								
								$param = array(
									$id,
									$value['req_ID'],
									$userinfo[$value['req_ID']],
									$datenow
									);
									
								SQL::execute($sql, $param);	
						}
				}
				
				// SESSION::StoreMsg("User Successfully Updated!", "success");
				##CECONTROLLER::update2
				$enrolled = CECONTROLLER::getEnrolled($id); 
				if(!($enrolled)){
					#CECONTROLLER::update2($id);
					CECONTROLLER::addfromadd($id,$userinfo['curricullum_ID']);
				}
				if($enrolled['curricullum_ID'] != $userinfo['curricullum_ID']){
					CECONTROLLER::update2($id);
					CECONTROLLER::addfromadd($id,$userinfo['curricullum_ID']);
				}
	
				SESSION::StoreMsg("This update was shoot in the List of Request for the update of student info please wait for approval!", "success");
		
					#AUDIT TRAIL#
					$user = $_SESSION['USER']['account_ID'];
					$action = "Updated Student #$si_ID (Reference=$id)";
					$module = "Student Information";
					AUDIT::ins($user,$module, $action);
					#AUDIT TRAIL#
	
						#href("users.php");
		}
		// hans [END]

	public static function isExistReq($req,$si_ID){
	
			$sql = "
			SELECT *
			FROM student_requirements_passed where req_ID = ? and si_ID = ?

			";		
			
			$param = array ($req , $si_ID);
			return SQL::find_scalar($sql,$param);
			
	}
	
	
	
	
	public static function getSingle($order=array(),$id) 
	{
		
			$sql = "
			SELECT *
			FROM student_information
			WHERE si_ID = ?
			";
			
	
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}	
	public static function getSingle_new($order=array(),$id) 
	{
		
			$sql = "
			SELECT *
			FROM request_approval
			WHERE si_ID = ?
			";
			
	
		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function getSingleSAI($order=array(),$id)
	{
		
			$sql = "
			SELECT *
			FROM student_additional_info
			WHERE si_ID = ?
			";
			

		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function getSingleSAI_new($order=array(),$id)
	{
		
			$sql = "
			SELECT *
			FROM request_approval_add_info
			WHERE si_ID = ?
			";
			

		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    public static function getSingleSRP($order=array(),$id)
	{
		
			$sql = "
			SELECT *
			FROM student_requirements_passed
			WHERE req_ID = ?
			";
			

		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
    #MOJA END

    #KYL
    public static function getAllStudentInformation($order=array()){
		$sql = "
		SELECT *
		FROM student_information
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
	// hansel
    public static function getAllStudentInformation_new($order=array()){
		$sql = "
		SELECT *
		FROM request_approval
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}
	// hansel
	
    public static function viewStudents($order=array()){
		$sql = "
		SELECT *
		FROM student_information
		";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}

	public static function view1Student($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_all($sql);
	}
	
	public static function viewSingleStudent($id){
		$sql = "
		SELECT *
		FROM student_information
		WHERE si_ID = $id
		";
		return SQL::find_id($sql);
	}	
	
	
	#kmsj
	
	public static function getAllHighSchool(){
		$sql = "
		SELECT DISTINCT(sai_highschool_graduated)
		FROM student_additional_info
		";
	
		return SQL::find_all($sql);
	}
	public static function getAllElementary(){
		$sql = "
		SELECT DISTINCT(sai_elementary_graduated)
		FROM student_additional_info
		";
	
		return SQL::find_all($sql);
	}	
	
	public static function getAllCollege(){
		$sql = "
		SELECT DISTINCT(sai_college_last_attended)
		FROM student_additional_info
		";
	
		return SQL::find_all($sql);
	}	
	
	public static function getAllStudentType(){
		$sql = "
		SELECT * 
		FROM student_type
		";
	
		return SQL::find_all($sql);
	}	
	
	public static function getAllstudentStatus(){
		$sql = "
		SELECT * 
		FROM student_status ORDER BY 1
		";

		return SQL::find_all($sql);
	}	
	
	
	public static function checkReqByUser($req_ID,$si_ID){
		$sql = "
		SELECT * 
		FROM student_requirements_passed
		WHERE req_ID = '$req_ID' and si_ID = '$si_ID'
		";
	
		return SQL::find_id($sql);
	}	
	
	public static function getStudentTypeRequirements($student_type_ID){
		$sql = "
		SELECT * FROM student_type_requirements,student_requirements
		WHERE student_type_requirements.student_req_ID = student_requirements.req_ID and
		student_type_requirements.student_type_ID = '$student_type_ID'
		";
	
		return SQL::find_all($sql);
	}	
	
	public static function getLastNumber($prefix){
		$sql = "
		SELECT RIGHT(MAX(student_ID), 3) si_ID
		  FROM student_information
		 WHERE student_ID LIKE '$prefix%'
		";
		return SQL::find_id($sql);
	}
		
	
	
	public static function getAdmittedList()
		{
			$sql = "
			SELECT *
			FROM student_admit_list
			";
			
			return SQL::find_all($sql);
		}
	
	public static function getAdmittedbyID($ID)
		{
			$sql = "
			SELECT *
			FROM student_admit_list where
			student_admit_ID = '$ID'
			";
			
			return SQL::find_id($sql);
		}	
	
	
	
}


class DEPT
{
		public static function getAlldept()
		{
			$sql = "
			SELECT *
			FROM department_list
			";
			
			return SQL::find_all($sql);
		}
		
		public static function getdeptbyID($dept_ID)
		{
			$sql = "
			SELECT *
			FROM department_list
			where department_ID = '$dept_ID'
			";
			
			return SQL::find_id($sql);
		}

}
?>


<?php

class CLEARANCE{
	public static function getAllClearanceByStudID($id)
	{
		$sql = "
		SELECT cl.*,sm.sem_NAME, dp.department_NAME
		FROM clearance cl 
		LEFT JOIN semester sm on cl.sem_ID = sm.sem_ID
		LEFT JOIN department_list dp on cl.department_ID = dp.department_ID
		WHERE si_ID = '$id' ORDER BY cl.sem_ID DESC
		";
		
		return SQL::find_all($sql);
	}
	
}
?>

<?php
class SESSION{
	
	//Fetch the current user's information
	public static function getUser($key = null){
		if($key){
			return isset($_SESSION['USER'][$key]) ? $_SESSION['USER'][$key] : false;
		} else {
			return isset($_SESSION['USER']) ? $_SESSION['USER'] : false;
		}
	}
	
	//Checks if there is an active user
	//Basically called in login form to give the user the chance to login first
	public static function isLoggedIn(){
		return isset($_SESSION['USER']);
	}

	//Forces redirection if not logged in
	public static function CheckLogin(){
		if(!self::isLoggedIn()){
			self::StoreMsg("Please Login First!", "error");
			$_SESSION['previous_page'] = $_SERVER['REQUEST_URI'];
			href("login.php");
		}
	}

	//Set the active user
	public static function login($User){
		$_SESSION['USER'] = $User;
	}

	//Logout in the sesssion
	public static function logout(){
		session_destroy();
	}

	//Stores a session flash message
	public static function StoreMsg($msg, $type = 'info'){
		$_SESSION['MSG']['txt'] = $msg;
		$_SESSION['MSG']['type'] = $type;
	}

	//Displays the current session flash message and then unset it
	public static function DisplayMsg(){
		if(isset($_SESSION['MSG'])){				
			switch($_SESSION['MSG']['type']){
				case 'changes' : 
					$type = "alert-info"; 
					$title = "Changelogs!";
					break;
				case 'success' : 
					$type = "alert-success"; 
					$title = "Success!";
					break;
				case 'error' : 
					$type = "alert-danger"; 
					$title = "Error!";
					break;
				case 'warning' : 
					$type = "alert-warning";
					$title = "Warning!";
					break;
				case 'info' : 
					$type = "alert-info";
					$title = "Heads Up!";
					break;
				default: 
					$type = "alert-success";
					$title = "Success!";
			}
			
			?>

			<div class="alert alert-dismissable <?php echo $type; ?>">
				<button type="button" class="close" data-dismiss="alert">×</button>
				<h4> <?php echo $title; ?> </h4>
				<?php echo $_SESSION['MSG']['txt']; ?>
			</div>

			<?php
			unset($_SESSION['MSG']);
			return true;
		} else {
			return false;
		}
	}


	//Displays the current session flash message and then unset it
	public static function ShowMsg(){
		if(isset($_SESSION['MSG'])){				
			switch($_SESSION['MSG']['type']){
				case 'changes' : 
					$type = "alert-info"; 
					$title = "Changelogs!";
					break;
				case 'success' : 
					$type = "alert-success"; 
					$title = "Success!";
					break;
				case 'error' : 
					$type = "alert-danger"; 
					$title = "Error!";
					break;
				case 'warning' : 
					$type = "alert-warning";
					$title = "Warning!";
					break;
				case 'info' : 
					$type = "alert-info";
					$title = "Heads Up!";
					break;
				default: 
					$type = "alert-success";
					$title = "Success!";
			}
			
			?>

			<div class="alert <?php echo $type; ?>">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong><?php echo $title; ?></strong> - <?php echo $_SESSION['MSG']['txt']; ?>.
			</div>

			<?php
			unset($_SESSION['MSG']);
			return true;
		} else {
			return false;
		}
	}
	
	
	public static function DisplayCustomMsg($type,$msg,$button_close=NULL){
		if(isset($type)){				
			switch($type){
				case 'changes' : 
					$type = "alert-info"; 
					$title = "Changelogs!";
					break;
				case 'success' : 
					$type = "alert-success"; 
					$title = "Success!";
					break;
				case 'error' : 
					$type = "alert-danger"; 
					$title = "Error!";
					break;
				case 'warning' : 
					$type = "alert-warning";
					$title = "Warning!";
					break;
				case 'info' : 
					$type = "alert-info";
					$title = "Heads Up!";
					break;
				default: 
					$type = "alert-success";
					$title = "Success!";
			}
			
			?>
			<div class="alert alert-dismissable <?php echo $type; ?>">
				<?php if($button_close){ ?><button type="button" class="close" data-dismiss="alert">x</button><?php } ?>
				<h4> <?php echo $title; ?> </h4>
				<?php echo $msg; ?>
			</div>
			<?php
		} 
	}
	
	
	
}


function required()
{
?>
 <div class="col-sm-1" align="left" style="padding-left:0px; color:red;">*
                            </div>
<?php

}


function required_label()
{
?>
<div class="form-group">
							<div class="col-md-3" align="right"><b>Note:</b></div>
							<div class="col-sm-6" align="LEFT" style="color:red">
								* Required Fields
							</div>
						</div>
<?php
}


?>
<?php
$db = new MYSQLi_Database();
class SQL{
	
	public static function last_ID(){
		global $db;
		return $db->last_ID();
	}
	
	public static function execute($sql, $param = null){
		global $db;
		return $db->query($sql, $param);
	}
	
	public static function execute1($sql){
		global $db;
		return $db->query($sql);
	}

	public static function find_by_sql($sql, $param = null){
		global $db;
		return $db->fetch($sql, $param);
	}
	
	public static function find_all($sql, $param = null){
		return self::find_by_sql($sql, $param);
	}
	
	public static function find_id($sql, $param = null){
		$result_array = self::find_all($sql, $param);
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	public static function find_scalar($sql, $param = null){
		$result_array = self::find_id($sql, $param);
		return !empty($result_array) ? current($result_array) : false;
	}
	
}
?>

<?php
class MYSQLi_Database{
	private $con;
	private $host = DB_HOST;
	private $username = DB_USER;
	private $password = DB_PASS;
	private $db = DB_NAME;
	
	function __construct(){
		$this->open_con();
	}

	public function open_con(){
		$this->con = new PDO("mysql:host={$this->host};dbname={$this->db}", $this->username, $this->password)
				or die("Failed to connect to Database: (" . $this->con->errorCode() . ") " . print_r($this->con->errorInfo()));
		//echo 'Successfully connected.......';
	}
		
	public function query($sql, $param = null){
		$stmt = $this->con->prepare($sql);
		if($param){
			$stmt = $this->bind($stmt, $param);
		}
		if(!$stmt->execute())
			$this->display_error($stmt->errorInfo());
		return $stmt->rowCount();
	}

	public function fetch($sql, $param = null){
		$stmt = $this->con->prepare($sql);
		if($param){
			$stmt = $this->bind ($stmt, $param);
		}
		if(!$stmt->execute())
			$this->display_error($stmt->errorInfo());
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
	}

	public function display_error($error){
		SESSION::StoreMsg("
			Error Code : <b>{$error[1]}</b>
			<br/>
			Error Message : {$error[2]}",
			"error"
		);
		HTML::redirect();
	}

	public function last_ID(){
		return $this->con->lastInsertId();
	}
		
	public function fetch_assoc($result_set){
		return $result_set->fetch_assoc();
	}

	private function bind($stmt, $param){
		foreach($param as $key => $value){
			if(is_array($value)){
				$val = trim($value[0]) == "" ? null : $value[0];
				$stmt->bindValue($key+1, $val, $value[1]) or die("BIND ERROR : " . print_r($stmt->errorInfo()));
			} else {
				$val = trim($value) == "" ? null : $value;
				$stmt->bindValue($key+1, $val) or die("BIND ERROR : " . print_r($stmt->errorInfo()));
			}
		}
		return $stmt;
    }
	
}
?>

<?php

class Audit_final{
	public function audit_trail($account_id,$action_event,$event_desc){
		$current_date =  date('Y-m-d H:i:s');
		$CLIENT_IP = isset($_SERVER['HTTP_CLIENT_IP'])?$_SERVER['HTTP_CLIENT_IP']:isset($_SERVER['HTTP_X_FORWARDED_FOR'])?$_SERVER['HTTP_X_FORWARDED_FOR']:$_SERVER['REMOTE_ADDR'];
		$sql = "INSERT INTO audit_trail VALUES(NULL, ?, ?, ?,?,?)";

		$param = array($account_id,$CLIENT_IP,$action_event,$event_desc,$current_date);
    	SQL::execute($sql,$param);

	}
	
}
?>
