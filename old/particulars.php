<?php 
//INITIALIZE INCLUDES
    include('init.php');
#SESSION::CheckLogin();

    //QUERIES
    if(Request::post()){
        switch(Request::get("action")) {
            case "add": 
            PARTICULARS::addroom(Request::post());
            #var_dump(Request::post());
            
            break;
            case "edit": PARTICULARS::update_room($_GET['id'],Request::post()); break;
            #case "update_blog":  BLOG::update(Request::post("blog_info")); break;
            
            #case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
            
            default: Fail::not_found();
        }
    }
    
    if(Request::get()){
        switch(Request::get("action")) {
            case "delete": PARTICULARS::delete($_GET['id']); break;
        
        }
    }       
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>SCHOOL MANAGEMENT SYSTEM</title>
    </head>
    <body>
        <!-- top nav -->
        <?php include(LAYOUTS . 'top_nav.php'); ?>
        <!-- end nav -->
        <div class="container">
            <!-- banner -->
            <?php include(LAYOUTS . "banner.php"); ?>
            <!-- end banner -->
            <!-- alert messages -->
            <?php SESSION::DisplayMsg(); ?>
            <!-- end of alert messages -->
            <!-- start row container -->
            <!-- 
            <div class="row">
                <div class="col-lg-12">
                    <ul class="breadcrumb">
                        <li class="active">Home</li>
                    </ul>
            </div>  
            </div>
            -->
            <!-- end row container -->
            <!-- row container -->
            <div class="row">
                <!-- left nav -->   
            <?php
                if (!SESSION::isLoggedIn()) {
                    ?>
                <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> particular Categories </h3>
                    </div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                        <li >
                            <a style="cursor:pointer" id="1-link">
                                <span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=1','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CCS (2)
                                </span>
                            </a>
                        </li>
                        <li >
                            <a style="cursor:pointer" id="2-link">
                                <span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=2','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CBA</span>
                            </a>
                        </li>
                        <script>

                                  $( "#1-link" ).click(function() {
                             $( "#1-content" ).slideToggle( "fast" );
                         });
                                  $( "#2-link" ).click(function() {
                             $( "#2-content" ).slideToggle( "fast" );
                         });

                        </script>
                        <!--
                            <li class="active"><a href="#">Computer Studies (#)</a></li>
                            <li class=""><a href="#">Business particular</a></li>
                            <li class="dropdown"><a class="dropdown-toggle" id="link" data-toggle="dropdown" href="#">Cat3 <span class="caret"></span></a>
                                <ul class="nav nav-pills nav-stacked nav-sub"  >
                                    <li><a href="#">Cat3 Sub 1 <span class="caret"></span></a>

                                    <ul class="nav nav-pills nav-stacked  nav-sub"  style="">

                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                    </ul>

                                    </li>
                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                </ul>
                            </li>

                           --> 
                        </ul>
                    </div>
                </div>
                </div>
                <?php   }else{?>
                <div class="col-md-3">
    
                <?php include(PAGES."navigation.php");?>
                </div>
                <?php } ?>
                <!-- end left nav -->
                
                <!-- BODY -->
                
                
                <div class="col-md-9">
                <?php if (SESSION::isLoggedIn()) { ?>
                    <?php
                    switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add particular </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> particular Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for='si_FNAME' class='col-md-6'>particular Name</label>
                                    <input name='room_NAME' pattern="([a-zA-Z0-9]| |/|\|@|#|$|%|&)+" placeholder="Particular Name" class='form-control' type='text' required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit' onclick="return confirm('Do you really want to add this? This is irreversible');"><i class='fa fa-edit'></i> Add particular</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":
                        $siiid =$_GET['id'];
                        $getsingle = PARTICULARS::getSingle(array("particular_ID"=>"DESC"),$siiid  );
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Update particular </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> particular Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for='si_FNAME' class='col-md-6'>Particular ID</label>
                                    <input name='room_ID' placeholder="Room Name" class='form-control' type='text' required value="<?= $getsingle['particular_ID'];?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for='si_FNAME' class='col-md-6'>Particular Name</label>
                                    <input name='room_NAME' autofocus placeholder="Room Name" class='form-control' type='text' required value="<?= $getsingle['particular_NAME'];?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Update particular</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
                        include "searching.php";                        
                        ?>
                            
                    <?php break; 
                        case "view":?>
                        <?php
                                if(isset($_GET['id']))
                                {
                                $rID = $_GET['id'];
                                    //msgbox($studID);
                                    $view1room = PARTICULARS::view1room($rID);
                                    if(count($view1room)>=1){
                                        foreach($view1room as $key => $value){
                                            $room_ID = $value['particular_ID'];
                                            $room_NAME = $value['particular_NAME'];
                                            
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading"  style="margin-top: 5px; margin-bottom: 5px">
                                <div class="col-md-3">
                                <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                    <i class="fa fa-user"></i>      
                                </h3>
                                </div>
                                <div class="col-md-53">
                                <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                </h3>
                                </div>
                            </div>
                            
                            <div class="panel-body">
                                <table border='0' class="table table-responsive text-md">
                                    <tr>
                                        <td colspan='4'><strong>View Particulars</strong>
                                    <tr>
                                        <td>&nbsp;
                                        <td align='center'><?php echo $room_ID;?>
                                    <tr>
                                        <td>&nbsp;
                                        <td align='center'><strong><i>Room ID</i></strong>
<tr>
                                        <td>&nbsp;
                                        <td align='center'><?php echo $room_Name;?>
                                    <tr>
                                        <td>&nbsp;
                                        <td align='center'><strong><i>Room Name</i></strong>

                        <?php } ?>
                                </table>
                                <!--
                                <tr>
                                        <td colspan='4' align='right'>
                                -->
                                    
                            </div>
                            
                        </div>
                            <div align='right'>
                                <input type='button' class='btn btn-warning' value='Edit'>
                                <!--<input type='button' onClick="location.href='../../isjb/student_information.php?action=view'" class='btn btn-success' value='Back'>-->
                            </div>
                    <?php
                                    }else{ echo 'Error: 404 Not found.'; }
                                }else{
                    ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <table>
                                <tr>
                                <td width="50%">
                                    <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                        <i class="fa fa-folder-o"></i> View Particulars </h3>
                                </td>
                                <!-- <script src="jquery.js" type="text/javascript"></script> -->
                                    <script> 
                                    $(document).ready(function(){
                                      $("#flip").click(function(){
                                        $("#panel").slideToggle("fast");
                                      });
                                    });
                                    </script>
                                <td >
                                    <!--
                                    <div class="input-group col-md-12">
                                      <?php #include('searching3.php');?>
                                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
                                    -->
                                </td>
                                <script>
                                    function term(value){
                                           
                                            $.post("search3.php",{search_term:value}, function(data){
                                                $("#display_result").html(data).hide().fadeIn();   
                                                $("#display_hide").hide();
                                                if(value==""){
                                                 $("#display_hide").show();
                                                }
                                            });
                                            
                                        }
                                    </script>
                                <td width="5%">
                                <div class="col-md-1">   
                                <button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms P6C4_CREATEPARTICULARS' type='submit' onclick="javascript:window.open('particulars.php?action=add','_self');">Create Particulars</button>
                                </div>
                                </td>
                                </tr>
                                </table>
                            </div>
                            
                            <div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
                                <table class="table table-hover table-responsive table-striped text-md">
                                    <th style="width:20%;">Ref #</th>
                                    <th style="width:30%;">Particular Name</th>
                                    <th style="">Operation</th>
                                    
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    $viewStudents = PARTICULARS:: getallrooms(array("particular_ID"=>"ASC"));
                                     if(count($viewStudents)>=10) 
                                     {
                                         $pagination2 = new Pagination($viewStudents, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($viewStudents, 10, NULL);
                                     }

                                     $viewStudents = $pagination2->get_array();

                                     if($viewStudents) {
                                        foreach($viewStudents as $key => $value){
                                    ?>
                                        <tr>
                                            <td><?php echo $value['particular_ID']; ?>
                                            <td><?php echo $value['particular_NAME']; ?>
                                            <td><a href="particulars.php?action=edit&id=<?php echo $value['particular_ID']; ?>" class='btn btn-warning P6C4_EDIT btn-xs'>Edit</a>
                                                <!--<a href="/isjb/particulars.php?action=delete&id=<?php echo $value['particular_ID']; ?>" class='btn btn-danger btn-xs'>Delete</a> -->
                                            </td>
                                        </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
                                </table> 
                                        <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                                        </center>
                                 </div>
                            </div>
                        </div>
                            
                    </div>
                </div>
                  <?php } break; ?>
                  
                    <?php 
                        case "delete":?>
                        
                <?php break;
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                    
                    
                 </div>

                    
                </div>
                
                <!-- end right nav -->
                
            </div>
            <!-- end row container -->
            
            

            <!-- footer-->      
            <?php include (LAYOUTS . "footer.php"); ?>
            <!-- end footer -->

        </div>
        <!-- end container -->

    </body>
</html>
