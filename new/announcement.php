<?php 
	//INITIALIZE INCLUDES
	include('init.php');
	
	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
			
			ANNOUNCEMENT::insert(Request::post()); 
			#var_dump(Request::post());
			
			break;
			case "edit": ANNOUNCEMENT::announce_update(Request::post()); break;
			#case "update_blog":  BLOG::update(Request::post("blog_info")); break;
			
			#case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ANNOUNCEMENT::delete($_GET['id']); break;
		
		}
	}		
	
?>
<html xmlns="http://www.w3.org/1999/xhtml">
	
    
  
	
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
      <title>Announcements | <?php echo $site_options['site_NAME']; ?></title>
        
        
 
	
	</head>
	
	<body>
		
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">

			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end alert messages -->
			
			<!-- end row container -->
			<div class="row">
				<!-- right content -->
				<div class="col-lg-12">

					<!-- breadcrumbs -->
					<ul class="breadcrumb">
						<li class="active"><a href="index.php">Home</a></li>
                        	<li class="active"><a href="announcement.php">Announcement</a></li>
					</ul>
					<!-- end bread --> 
                    <a name="send"></a>
					<a name="view"></a>
				</div>
				<!-- right content -->
			</div>
			<!-- end row container -->
			
			<!-- row container -->
			<div class="row">

				<!-- left nav -->
				<div class="col-md-3">
							<!-- nav-->
                                                        <div class="panel panel-default">
                                        <div class="panel-heading"> <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"> Announcement </h3> </div>
                                        <div class="panel-body">
                                            <ul class="nav nav-pills nav-stacked">
                                                <li class="<?php if($_GET['action'] == 'view_all'){ ?>active<?php } ?>"><a href="?action=view_all&view=all">View </a></li>
                                                <li class="<?php if($_GET['action'] == 'add'){ ?>active<?php } ?>"><a href="?action=add">Add</a></li>
                                              
                                                
                                            </ul>
                                        </div>
                                    </div>
				</div>
				<!-- end left nav -->
				
				<!-- BODY -->
				<div class="col-md-9">
          
                
      <?php
					switch($_GET['action']){
					
					case "edit":
					
					$announcement = ANNOUNCEMENT::getSingle(array("announcement_ID"=>"DESC"), $_GET['id']);
					
					
					 ?>
                     <div class="panel panel-default">
                                        <div class="panel-heading"> <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">Edit Announcement </h3></div>
                                        <div class="panel-body">
                    
                   
                    
					<form class="form-horizontal" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST">
				
							<input type="HIDDEN" name="announcement_ID" value="<?php echo $announcement['announcement_ID']; ?>"/>
						
												
						<div class="form-group">
						 <label for="message[message_TITLE]" class="col-md-2 control-label" align="LEFT"> Title: </label>
                         <div class="col-sm-6" align="LEFT">
							  <input type="text"  name="announcement_TITLE" <?php if(isset($_GET['id'])) { echo "AUTOFOCUS"; } ?> required class="form-control" value="<?=  $announcement['announcement_TITLE'];?>"></input>
							</div>
                            </div>
                            
                         <div class="form-group">
                          <label for="message[message_CONTENT]" class="col-md-2 control-label" align="LEFT"> Message: </label>
							<div class="col-sm-6" align="LEFT">
							  <textarea name="announcement_CONTENT" <?php if(isset($_GET['id'])) { echo "AUTOFOCUS"; } ?> required cols="45" rows="5" class="form-control"><?=  $announcement['announcement_CONTENT'];?></textarea>
							</div>
						</div>
						
                        
                        <div class="form-group">
                        	   <label for="message[message_START]" class="col-md-2 control-label" align="LEFT"> Start Date:  </label>
                                <div class="col-sm-6" align="LEFT">
							
							    <input type="text" min="<?php echo date("m/d/Y H:i"); ?>" pattern="\d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\" value="<?=  $announcement['announcement_START'];?>"  name="announcement_START" required class="form-control datetimepicker" ></input>
								</div>
                        </div>
                        
                        <div class="form-group">
                        	   <label for="message[message_START]" class="col-md-2 control-label" align="LEFT">End Date? <input type="checkbox" onClick="disableElement('end')" checked=checked>: </label>
                                <div class="col-sm-6" align="LEFT">
							   <input type="text" pattern="\d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\" id="end"  name="announcement_END" <?php if($announcement['announcement_END'] != "2099-12-30 00:00:00") { ?>value="<?php echo  $announcement['announcement_END']; ?>"<?php } ?>  class="form-control datetimepicker" ></input>
								</div>*Uncheck or leave it blank for unexpiring announcement.
                        </div>
                        
                        
                        
						<div class="form-group">
							<div class="col-md-2"></div>
							<div class="col-sm-6" align="CENTER">
								<?php echo FORM::button("Save", array("type"=>"submit", "class"=>"btn btn-success btn-block")) ?>
							</div>
						</div>
						
					</form>
                    </div></div>
                    
                    
                    
					
					<?php 
					break;
					
					case "add": ?>

					<!-- Add PAGE -->
					
                     <div class="panel panel-default">
                                        <div class="panel-heading"> <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">Add Announcement </h3></div>
                                        <div class="panel-body">
                    
                   
                    
						<form class="form-horizontal" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="POST">
				
						<input type="HIDDEN" name="user_ID" value="<?php echo $user['user_ID']; ?>"/>
  						
						
						
												
						<div class="form-group">
						 <label for="message[message_TITLE]" class="col-md-2 control-label" align="LEFT"> Title: </label>
                         <div class="col-sm-6" align="LEFT">
							  <input type="text" autofocus name="announcement_TITLE" <?php if(isset($_GET['id'])) { echo "AUTOFOCUS"; } ?> required class="form-control"></input>
							</div>
                            </div>
                            
                         <div class="form-group">
                          <label for="message[message_CONTENT]" class="col-md-2 control-label" align="LEFT"> Message: </label>
							<div class="col-sm-6" align="LEFT">
							  <textarea name="announcement_CONTENT" <?php if(isset($_GET['id'])) { echo "AUTOFOCUS"; } ?> required cols="45" rows="5" class="form-control"></textarea>
							</div>
						</div>
						
                        
                        <div class="form-group">
                        	   <label for="message[message_START]" class="col-md-2 control-label" align="LEFT"> Start Date:  </label>
                                <div class="col-sm-6" align="LEFT">
							
							    <input type="text" min="<?php echo date("m/d/Y H:i"); ?>" pattern="\d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\" value="<?php echo date("m/d/Y H:i"); ?>"  name="announcement_START" <?php if(isset($_GET['id'])) { echo "AUTOFOCUS"; } ?> required class="form-control datetimepicker" ></input>
								</div>
                        </div>
                        
                        <div class="form-group">
                        	   <label for="message[message_START]" class="col-md-2 control-label" align="LEFT">End Date? <input type="checkbox" onClick="disableElement('end')">: </label>
                                <div class="col-sm-6" align="LEFT">
							   <input type="text" pattern="\d{2}/\d{2}/\d{4}\s+\d{2}:\d{2}\" id="end"  name="announcement_END" <?php if(isset($_GET['id'])) { echo "AUTOFOCUS"; } ?> required class="form-control datetimepicker" disabled></input>
								</div>*Uncheck or leave it blank for unexpiring announcement.
                        </div>
                        
                        
                        
						<div class="form-group">
							<div class="col-md-2"></div>
							<div class="col-sm-6" align="CENTER">
								<?php echo FORM::button("Post", array("type"=>"submit", "class"=>"btn btn-success btn-block")) ?>
							</div>
						</div>
						
					</form>
                    </div></div>
					<!-- END OF SEND PAGE -->


			      <?php break; case "view_all": ?>

				  <!-- VIEW ALL PAGE -->
                  
                   <div class="panel panel-default">
                                        <div class="panel-heading"> <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">View Announcement </h3></div>
                                        <div class="panel-body">
                  
                  
                  <?php
				  	if(isset($_GET['p']))
					{
					$p = $_GET['p'];
					
					}else{
					$p = 1;
					}
				  
				  ?>
                  
                  
                  <ul class="nav nav-tabs" style="margin-bottom: 15px;">
                <li class="<?php if(isset($_GET['view']) && ($_GET['view'] == 'all')) { echo "active"; } ?>" ><a href="#all" data-toggle="tab">All</a></li>
                <li class="<?php if(isset($_GET['view']) && ($_GET['view'] == 'active')) { echo "active"; } ?>"><a href="#Active" data-toggle="tab">Active</a></li>
                <li class="<?php if(isset($_GET['view']) && ($_GET['view'] == 'scheduled')) { echo "active"; } ?>"><a href="#scheduled" data-toggle="tab">Scheduled</a></li>
                 <li class="<?php if(isset($_GET['view']) && ($_GET['view'] == 'expired')) { echo "active"; } ?>"><a href="#expired" data-toggle="tab">Archived</a></li>
                
              </ul>
                  
                  
         <div id="myTabContent" class="tab-content">         
                  
              <!-- all --><div class="tab-pane fade <?php if(isset($_GET['view']) && ($_GET['view'] == 'all')) { echo "active in"; } ?>" id="all">
            		
                    
                    
                 
                    
                     <table class="table table-responsive table-hover">
						
                        <?php
							$announcement = ANNOUNCEMENT::getALL( array("announcement_ID"=>"DESC"));
							if($_GET['view'] == 'all')
							{
							$pagination2 = new Pagination($announcement, 10, Request::get("p"));
							}else{
							$pagination2 = new Pagination($announcement, 10, NULL);
							}
							$announcement = $pagination2->get_array();
						?>
                        
                        <?php if($announcement){ ?>
                        
                        
                           <a class="btn-warning btn btn-sm" target="_blank"  href="announcement_print.php?module=site&view=view_all&p=<?= $p; ?>" class="link2">PRINT</a>
                        <thead>
							<tr align="CENTER">
							  <th align="CENTER" width="10%">User</th>
							  	<th align="CENTER" width="15%">Title</th>
                                <th align="CENTER" width="10%">Date Posted</th>
                                <th align="CENTER" width="10%">Start</th>
                                <th align="CENTER" width="10%">End</th>
                                <th align="CENTER" width="15%">Options</th>
							</tr>
                           </thead>
                        
						<tbody>
                        	
                            <?php foreach($announcement as $key => $val){  ?>
                            <tr align="CENTER" style="cursor:pointer">
								<td align="LEFT" valign="bottom">
                               
                                
                                <?php 
									$person = USERS::getID($val['user_ID']); 
								
									?>
                              
                                   <img src="<?php echo IMAGES; ?>f1.png" width="30" height="30" align="middle" title="Guest"/>								  									 <a href="profile.php?id=<?php echo $person['user_ID']; ?>"><?php echo $person['user_UNAME']; ?></a>
                
									
								</td>
								<td align="LEFT">
                                <?php echo $val['announcement_TITLE']; ?>
                                
                                  </td><td align="LEFT">
                                  <?php echo date("m/d/Y H:i A",strtotime($val['announcement_DATE'])); ?>
                                  </td>
                                <td align="LEFT">
                                 <?php echo date("m/d/Y H:i A",strtotime($val['announcement_START'])); ?>
                                </td>
								<td align="left">  <?php if( $val['announcement_END'] == '2099-12-30 00:00:00'){ echo "-"; }else{ echo date("m/d/Y H:i A",strtotime($val['announcement_END']));  }  ?>
                                </td>
                                <td align="LEFT">
                                <a class="btn-success btn btn-sm" href="?action=edit&id=<?php echo $val['announcement_ID']; ?>" class="link2">EDIT</a> 
                                	<a class="btn-warning btn btn-sm" onClick="return confirm('Are you sure?')" href="?action=delete&id=<?php echo $val['announcement_ID']; ?>" class="link2">DELETE</a>
                             <!--  <a class="btn btn-warning btn-sm" href="#update" class="link2">Delete</a>  -->
                              </td> 
								
						  </tr>
                            <?php } ?>
                            <tr>
                            	<td colspan="6">
                                <center>
                                	<?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view_all&view=all') ?>
                                </center>    
                                </td>
                             </tr>   
                             
						</tbody>
                        <?php }else{ ?>
                        
                        <tbody>
                             <tr align="CENTER">
								<td align="CENTER" colspan="3">No Announcement</td>
							
							</tr>
						</tbody>
                        <?php } ?>
				  </table>

                    
                    
             <!-- all-end -->     </div>  
                    
             <!-- active -->     <div class="tab-pane fade <?php if(isset($_GET['view']) && ($_GET['view'] == 'active')) { echo "active in"; } ?>" id="Active">
             
             
<table class="table table-responsive table-hover">
						
                        <?php
							$announcement = ANNOUNCEMENT::getActive( array("announcement_ID"=>"DESC"));
							if($_GET['view'] == 'active')
							{
							$pagination2 = new Pagination($announcement, 10, Request::get("p"));
							}else{
							$pagination2 = new Pagination($announcement, 10, NULL);
							}
							$announcement = $pagination2->get_array();
						?>
                        
                        <?php if($announcement){ ?>
                         <a class="btn-warning btn btn-sm" target="_blank"  href="announcement_print.php?module=site&view=active&p=<?= $p; ?>" class="link2">PRINT</a>
                        <thead>
							<tr align="CENTER">
							  <th align="CENTER" width="10%">User</th>
							  	<th align="CENTER" width="15%">Title</th>
                                <th align="CENTER" width="10%">Date Posted</th>
                                <th align="CENTER" width="10%">Start</th>
                                  <th align="CENTER" width="10%">End</th>
                                <th align="CENTER" width="15%">Options</th>
							</tr>
                           
                            
						</thead>
                        
						<tbody>
                        	
                            <?php foreach($announcement as $key => $val){  ?>
                            <tr align="CENTER" style="cursor:pointer">
								<td align="LEFT" valign="bottom">
                               
                                
                                <?php 
									$person = USERS::getID($val['user_ID']); 
								
									?>
                              
                                   <img src="<?php echo IMAGES; ?>f1.png" width="30" height="30" align="middle" title="Guest"/>								  									 <a href="profile.php?id=<?php echo $person['user_ID']; ?>"><?php echo $person['user_UNAME']; ?></a>
                
									
								</td>
								<td align="LEFT">
                                <?php echo $val['announcement_TITLE']; ?>
                                
                                  
                                  </td><td align="LEFT">
                                  <?php echo date("m/d/Y H:i A",strtotime($val['announcement_DATE'])); ?>
                                  
                                  </td>
                                <td align="LEFT">
                                 <?php echo date("m/d/Y H:i A",strtotime($val['announcement_START'])); ?>
                                 
                                 
                                 
                                </td>
								<td align="left">  <?php if( $val['announcement_END'] == '2099-12-30 00:00:00'){ echo "-"; }else{ echo date("m/d/Y H:i A",strtotime($val['announcement_END']));  }  ?>
                                </td>
                                <td align="LEFT">
                                <a class="btn-success btn btn-sm" href="?action=edit&id=<?php echo $val['announcement_ID']; ?>" class="link2">EDIT</a> 
                                	<a class="btn-warning btn btn-sm" onClick="return confirm('Are you sure?')" href="?action=delete&id=<?php echo $val['announcement_ID']; ?>" class="link2">DELETE</a>
                             <!--  <a class="btn btn-warning btn-sm" href="#update" class="link2">Delete</a>  -->
                              
                              </td> 
								
						  </tr>
                            <?php } ?>
                             <tr>
                            	<td colspan="6">
                                	<center><?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view_all&view=active') ?></center>
                                </td>
                             </tr>   
                             
						</tbody>
                        <?php }else{ ?>
                        
                        <tbody>
                             <tr align="CENTER">
								<td align="CENTER" colspan="3">No Announcement</td>
							
							</tr>
						</tbody>
                        <?php } ?>
				  </table>
                       
                     
              <!-- end active --></div>
                    
                    
                    
                     <div class="tab-pane fade <?php if(isset($_GET['view']) && ($_GET['view'] == 'scheduled')) { echo "active in"; } ?>" id="scheduled">
                    <!-- Scheduled-->
                    <table class="table table-responsive table-hover">
						
                        <?php
							$announcement = ANNOUNCEMENT::getScheduled( array("announcement_ID"=>"DESC"));
							if($_GET['view'] == 'scheduled')
							{
							$pagination2 = new Pagination($announcement, 10, Request::get("p"));
							}else{
							$pagination2 = new Pagination($announcement, 10, NULL);
							}
							$announcement = $pagination2->get_array();
						?>
                        
                        <?php if($announcement){ ?>
                          <a class="btn-warning btn btn-sm" target="_blank"  href="announcement_print.php?module=site&view=scheduled&p=<?= $p; ?>" class="link2">PRINT</a>
                        <thead>
							<tr align="CENTER">
							  <th align="CENTER" width="10%">User</th>
							  	<th align="CENTER" width="15%">Title</th>
                                <th align="CENTER" width="10%">Date Posted</th>
                                <th align="CENTER" width="10%">Start</th>
                                  <th align="CENTER" width="10%">End</th>
                                <th align="CENTER" width="15%">Options</th>
							</tr>
                           
                            
						</thead>
                        
						<tbody>
                        	
                            <?php foreach($announcement as $key => $val){  ?>
                            <tr align="CENTER" style="cursor:pointer">
								<td align="LEFT" valign="bottom">
                               
                                
                                <?php 
									$person = USERS::getID($val['user_ID']); 
								
									?>
                              
                                   <img src="<?php echo IMAGES; ?>f1.png" width="30" height="30" align="middle" title="Guest"/>								  									 <a href="profile.php?id=<?php echo $person['user_ID']; ?>"><?php echo $person['user_UNAME']; ?></a>
                
									
								</td>
								<td align="LEFT">
                                <?php echo $val['announcement_TITLE']; ?>
                                
                                  
                                  </td><td align="LEFT">
                                  <?php echo date("m/d/Y H:i A",strtotime($val['announcement_DATE'])); ?>
                                  
                                  </td>
                                <td align="LEFT">
                                 <?php echo date("m/d/Y H:i A",strtotime($val['announcement_START'])); ?>
                                 
                                 
                                 
                                </td>
								<td align="left">  <?php if( $val['announcement_END'] == '2099-12-30 00:00:00'){ echo "-"; }else{ echo date("m/d/Y H:i A",strtotime($val['announcement_END']));  }  ?>
                                </td>
                                <td align="LEFT">
                                <a class="btn-success btn btn-sm" href="?action=edit&id=<?php echo $val['announcement_ID']; ?>" class="link2">EDIT</a> 
                                	<a class="btn-warning btn btn-sm" onClick="return confirm('Are you sure?')" href="?action=delete&id=<?php echo $val['announcement_ID']; ?>" class="link2">DELETE</a>
                             <!--  <a class="btn btn-warning btn-sm" href="#update" class="link2">Delete</a>  -->
                              
                              </td> 
								
						  </tr>
                            <?php } ?>
                            <tr>
                            	<td colspan="6">
                                	 <center>
                                	<?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view_all&view=scheduled') ?>
                                </center>
                                </td>
                             </tr>   
						</tbody>
                        <?php }else{ ?>
                        
                        <tbody>
                             <tr align="CENTER">
								<td align="CENTER" colspan="3">No Announcement</td>
							
							</tr>
						</tbody>
                        <?php } ?>
				  </table>
                    
                    <!-- scheduled -->
                     </div>
                     
                     
                      <div class="tab-pane fade <?php if(isset($_GET['view']) && ($_GET['view'] == 'expired')) { echo "active in"; } ?>" id="expired">
                     <!-- Expired -->
                      <table class="table table-responsive table-hover">
						
                        <?php
							$announcement = ANNOUNCEMENT::getExpired( array("announcement_ID"=>"DESC"));
							if($_GET['view'] == 'expired')
							{
							$pagination2 = new Pagination($announcement, 10, Request::get("p"));
							}else{
							$pagination2 = new Pagination($announcement, 10, NULL);
							}
							$announcement = $pagination2->get_array();
						?>
                        
                        <?php if($announcement){ ?>
                          <a class="btn-warning btn btn-sm" target="_blank"  href="announcement_print.php?module=site&view=expired&p=<?= $p; ?>" class="link2">PRINT</a>
                        <thead>
							<tr align="CENTER">
							  <th align="CENTER" width="10%">User</th>
							  	<th align="CENTER" width="15%">Title</th>
                                <th align="CENTER" width="10%">Date Posted</th>
                                <th align="CENTER" width="10%">Start</th>
                                  <th align="CENTER" width="10%">End</th>
                                <th align="CENTER" width="15%">Options</th>
							</tr>
                           
                            
						</thead>
                        
						<tbody>
                        	
                            <?php foreach($announcement as $key => $val){  ?>
                            <tr align="CENTER" style="cursor:pointer">
								<td align="LEFT" valign="bottom">
                               
                                
                                <?php 
									$person = USERS::getID($val['user_ID']); 
								
									?>
                              
                                   <img src="<?php echo IMAGES; ?>f1.png" width="30" height="30" align="middle" title="Guest"/>								  									 <a href="profile.php?id=<?php echo $person['user_ID']; ?>"><?php echo $person['user_UNAME']; ?></a>
                
									
								</td>
								<td align="LEFT">
                                <?php echo $val['announcement_TITLE']; ?>
                                
                                  
                                  </td><td align="LEFT">
                                  <?php echo date("m/d/Y H:i A",strtotime($val['announcement_DATE'])); ?>
                                  
                                  </td>
                                <td align="LEFT">
                                 <?php echo date("m/d/Y H:i A",strtotime($val['announcement_START'])); ?>
                                 
                                 
                                 
                                </td>
								<td align="left">  <?php if( $val['announcement_END'] == '2099-12-30 00:00:00'){ echo "-"; }else{ echo date("m/d/Y H:i A",strtotime($val['announcement_END']));  }  ?>
                                </td>
                                <td align="LEFT">
                                <a class="btn-success btn btn-sm" href="?action=view_announcement&id=<?php echo $val['announcement_ID']; ?>" class="link2">EDIT</a> 
                                	<a class="btn-warning btn btn-sm" onClick="return confirm('Are you sure?')" href="?action=delete&id=<?php echo $val['announcement_ID']; ?>" class="link2">DELETE</a>
                             <!--  <a class="btn btn-warning btn-sm" href="#update" class="link2">Delete</a>  -->
                              
                              </td> 
								
						  </tr>
                            <?php } ?>
                            <tr>
                            	<td colspan="6">
                                	 <center>
                                	<?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view_all&view=expired') ?>
                                </center>
                                </td>
                             </tr>   
						</tbody>
                        <?php }else{ ?>
                        
                        <tbody>
                             <tr align="CENTER">
								<td align="CENTER" colspan="3">No Announcement</td>
							
							</tr>
						</tbody>
                        <?php } ?>
				  </table>
                     <!-- Expired -->
                     </div>
                    
                  
         </div>           
					</div></div>

				  <!-- END OF VIEW ALL PAGE -->

				<?php break; default: HTML::redirect("announcement.php?action=view_all"); } ?>
					
				</div>
				<!-- END BODY -->
				
			</div>
			<!-- end row container -->

			<div class="spacer-mid"></div>

			<!-- footer-->
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->
		
		</div>
		<!-- end container -->
	</body>
	
</html>

<?php

//RECURSIVE FUNCTION TO POPULATE LIST OF CATEGORIES

//RECURSIVE FUNCTION TO POPULATE LIST OF CATEGORIES


//RECURSIVE FUNCTION TO POPULATE COMBO BOX WITH SUB CATEGORIES

?>