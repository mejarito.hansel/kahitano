<?php 
//INITIALIZE INCLUDES
    include('init.php');
#SESSION::CheckLogin();

    //QUERIES
    if(Request::post()){
        switch(Request::get("action")) {
            case "add": 
            USERS::addstudent(Request::post());
            #var_dump(Request::post());
            
            break;
            case "edit": USERS::update_student($_GET['id'],Request::post()); break;
            #case "update_blog":  BLOG::update(Request::post("blog_info")); break;
            
            #case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
            
            default: Fail::not_found();
        }
    }
    
    if(Request::get()){
        switch(Request::get("action")) {
            case "delete": ANNOUNCEMENT::delete($_GET['id']); break;
        
        }
    }       
    
?>
<!--
CONTROLLER!!!
public static function getReport1($order=array()){
		$sql= "SELECT * FROM student_information as X 
                 LEFT JOIN 
                 (student_additional_info as A,
                 curricullum_enrolled as B,
                 curricullum_list as C,
                 course_list as D)
                 ON (A.si_ID=X.si_ID 
                 AND B.si_ID=X.si_ID
                 AND B.curricullum_ID = C.curricullum_ID
                 AND C.course_ID = D.course_ID)";
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		return SQL::find_all($sql);
	}    
-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
      <title>HOME | SJB CAINTA SIMS</title>
    </head>

    <body>

        <!-- top nav -->
        <?php include(LAYOUTS . 'top_nav.php'); ?>
        <!-- end nav -->

        <div class="container">
            
            <!-- banner -->
            <?php include(LAYOUTS . "banner.php"); ?>
            <!-- end banner -->
            
          
            
            
            <!-- alert messages -->
            <?php SESSION::DisplayMsg(); ?>
            <!-- end of alert messages -->
            
            <!-- start row container -->
            <!-- 
            <div class="row">
                
                <div class="col-lg-12">

                
                    <ul class="breadcrumb">
                        <li class="active">Home</li>
                    </ul>
                

                </div>
                
            </div>
            -->
            <!-- end row container -->
            
   
             
            <!-- row container -->
            <div class="row">

                <!-- left nav -->
                
            <?php
                if (!SESSION::isLoggedIn()) {
                    ?>
                <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading"> <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> Course Categories </h3> </div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                                    <li ><a style="cursor:pointer" id="1-link"><span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=1','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CCS            (2)             </span>
                                      </a>


                             </li>
                                        <li ><a style="cursor:pointer" id="2-link"><span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=2','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CBA                         </span>
                                      </a>


                             </li>





                        <script>

                                  $( "#1-link" ).click(function() {
                             $( "#1-content" ).slideToggle( "fast" );
                         });
                                  $( "#2-link" ).click(function() {
                             $( "#2-content" ).slideToggle( "fast" );
                         });

                        </script>

                        <!--
                            <li class="active"><a href="#">Computer Studies (#)</a></li>
                            <li class=""><a href="#">Business Course</a></li>
                            <li class="dropdown"><a class="dropdown-toggle" id="link" data-toggle="dropdown" href="#">Cat3 <span class="caret"></span></a>
                                <ul class="nav nav-pills nav-stacked nav-sub"  >
                                    <li><a href="#">Cat3 Sub 1 <span class="caret"></span></a>

                                    <ul class="nav nav-pills nav-stacked  nav-sub"  style="">

                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                    </ul>

                                    </li>
                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                </ul>
                            </li>

                           --> 
                        </ul>
                    </div>
                </div>
                </div>
                <?php   }else{?>
                <div class="col-md-3">
    
                <?php include(PAGES."navigation.php");?>
                </div>
                <?php } ?>
                <!-- end left nav -->
                
                <!-- BODY -->
                
                
                <div class="col-md-9">
                    <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> View Cadet Information </h3>
                            </div>
                            
                            <div class="panel-body">
                                <table class="table table-hover table-responsive table-striped text-md">
                                    <th style="width:20%;">Last Name</th>
                                    <th style="width:20%;">First Name</th>
                                    <th style="width:20%;">Middle Name</th>
                                    <th style="width:20%;">HIGHSCHOOL</th>  
                                    <th style="width:20%;">Action</th>  
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    $viewStudents = USERS:: getReport1(array("ce_ID"=>"ASC"));
                                     if(count($viewStudents)>=10) 
                                     {
                                         $pagination2 = new Pagination($viewStudents, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($viewStudents, 10, NULL);
                                     }

                                     $viewStudents = $pagination2->get_array();

                                     if($viewStudents) {

                                        foreach($viewStudents as $key => $value){
                                    ?>
                                        <tr>
                                            <td>
                                                <?php echo $value['si_LNAME']; ?>
                                            <td>
                                                <?php echo $value['si_FNAME']; ?>
                                            <td>
                                                <?php echo $value['si_MNAME']; ?>
                                            <td>
                                                <?php echo $value['sai_highschool_graduated']; ?>
                                            <td>
                                                <?php echo $value['course_NAME']." ".$value['curricullum_NAME']; ?>
                                            </td>
                                        </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
                                </table> 

                            </div>
                        </div>
                            <center>
                                <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                            </center>
                    </div>
                </div>
                </div>
            <!-- end row container -->
            
            

            <!-- footer-->      
            
            <?php 
            #echo MESSAGES::check_message("gago");
            ?>
            <?php include (LAYOUTS . "footer.php"); ?>
            <!-- end footer -->

        </div>
        <!-- end container -->

    </body>
</html>
