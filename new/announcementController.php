<?php
class ANNOUNCEMENT{



public static function getID($user_ID,$order=array())
	{
		
			$sql = "
			SELECT *
			FROM user_blog
			WHERE user_ID = ?
			";
			$param = array(
				array($user_ID, PDO::PARAM_INT)
			);
		
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql, $param);
		
	}


public static function getblogID($blog_ID,$order=array())
	{
		
			$sql = "
			SELECT *
			FROM user_blog
			WHERE blog_ID = ?
			";
			$param = array(
				array($blog_ID, PDO::PARAM_INT)
			);
		
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql, $param);
		
	}	
	
	
	
	


	
	
	
	
	public static function update($blog_INFO)
	{
	$sql = "
		update user_blog
		set	blog_NAME = ?,
			blog_CONTENT = ?

		WHERE blog_ID = ?	
		";
		$param = array(

			array($blog_INFO['blog_NAME'], PDO::PARAM_STR),
			array($blog_INFO['blog_CONTENT'], PDO::PARAM_STR),
			array($blog_INFO['blog_ID'], PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);

			SESSION::StoreMsg("Blog Successfully Updated!", "success");
			HTML::redirect("profile.php?action=view_blog&id=$blog_INFO[user_ID]&blog_ID=$blog_INFO[blog_ID]");
	}
	
	
	
	
	
	
		public static function addviews($blog_id)
	{
	$sql = "
		UPDATE user_blog set blog_VIEWS = blog_VIEWS + 1 WHERE blog_ID = ?
		";
		$param = array(
			array($blog_id, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
	}
	
	
	
	
	
	
	#Announcements 
	
	
	public static function getActive($order=array())
	{
		
			$sql = "
			SELECT *
			FROM announcement_site
			WHERE announcement_START <= ?
			AND announcement_END >= ?
			";
		
		$date_today = date("Y-m-d H:i:s");
		$param = array($date_today,$date_today);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql,$param);
		
	}
	
	
	
	public static function insert($announcement_INFO)
	{
	$sql = "
		INSERT INTO announcement_site
		set user_ID = ?,
			announcement_TITLE = ?,
			announcement_CONTENT = ?,
			announcement_DATE = ?,
			announcement_START = ?,
			announcement_END = ?
		";
		
	
		
		if(!isset($announcement_INFO['announcement_END']))
		{
		$announcement_INFO['announcement_END'] = "2099-12-30 00:00:00";
		}else{
		
		$start = strtotime($announcement_INFO['announcement_START']);
		$end = strtotime($announcement_INFO['announcement_END']);
		
		if($start >= $end)
		{
		SESSION::StoreMsg("Invalid start and end datetime range!!", "error");
			HTML::redirect("announcement.php?action=add");
		
		}
		
		
		
		
		$announcement_INFO['announcement_END'] = date("Y-m-d H:i:s",strtotime($announcement_INFO['announcement_END']));
		}
		
		
		var_dump($announcement_INFO);
		
#		echo $announcement_INFO['announcement_TITLE'];
		$param = array(
			array($announcement_INFO['user_ID'], PDO::PARAM_INT),
			array($announcement_INFO['announcement_TITLE'], PDO::PARAM_STR),
			array($announcement_INFO['announcement_CONTENT'], PDO::PARAM_STR),
			array(date("Y-m-d H:i:s"), PDO::PARAM_STR),
			array(date("Y-m-d H:i:s",strtotime($announcement_INFO['announcement_START'])), PDO::PARAM_STR),
			array($announcement_INFO['announcement_END'], PDO::PARAM_STR)
			
		);
		
		SQL::execute($sql, $param);
		$id = SQL::last_id();
			SESSION::StoreMsg("Announcement Successfully Posted!", "success");
			HTML::redirect("announcement.php?action=view_all&view=all");
	}
	
	
	
	
	
	public static function getALL($order=array())
	{
		
			$sql = "
			SELECT *
			FROM announcement_site
			
			";
			
		
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql);
		
	}
	
	
	public static function getExpired($order=array())
	{
		
			$sql = "
			SELECT *
			FROM announcement_site
			WHERE announcement_END <= ?
			";
			
		$date_today = date("Y-m-d H:i:s");
		$param = array($date_today);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql,$param);
		
	}
	
	
	public static function getScheduled($order=array())
	{
		
			$sql = "
			SELECT *
			FROM announcement_site
			WHERE announcement_START >= ?
			";
			
		$date_today = date("Y-m-d H:i:s");
		$param = array($date_today);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_all($sql,$param);
		
	}
	
	
	public static function delete($announcement_ID)
	{
	$sql = "
		DELETE FROM announcement_site where announcement_ID = ?
		";
		$param = array(
			array($announcement_ID, PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);
			SESSION::StoreMsg("Announcement Successfully Deleted!", "success");
			HTML::redirect("announcement.php?action=view_all&view=all");
	}
	
	public static function getSingle($order=array(),$id)
	{
		
			$sql = "
			SELECT *
			FROM announcement_site
			WHERE announcement_ID = ?
			";
			

		$param = array($id);
		
		foreach($order as $key => $value){
			if($key ==  0) {
				$sql .= "ORDER BY {$key} {$value}";
			} else {
				$sql .= ", {$key} {$value}";
			}
		}
		
		return SQL::find_id($sql,$param);
		
	}
	
	
	
	public static function announce_update($announcement_INFO)
	{
	$sql = "
		UPDATE announcement_site
		set 
			announcement_TITLE = ?,
			announcement_CONTENT = ?,
			announcement_START = ?,
			announcement_END = ?
		WHERE
			announcement_ID = ?	
		";
		#var_dump($announcement_INFO);
	
		
		if(!isset($announcement_INFO['announcement_END']) or ($announcement_INFO['announcement_END'] == ''))
		{
		$announcement_INFO['announcement_END'] = "2099-12-30 00:00:00";
		}else{
		
		$start = strtotime($announcement_INFO['announcement_START']);
		$end = strtotime($announcement_INFO['announcement_END']);
		
		if($start >= $end)
		{
		SESSION::StoreMsg("Invalid start and end datetime range!!", "error");
		HTML::redirect("announcement.php?action=edit&id=$announcement_INFO[announcement_ID]");
		die();
		#	HTML::redirect();
		
		}
		
		
		
		
		$announcement_INFO['announcement_END'] = date("Y-m-d H:i:s",strtotime($announcement_INFO['announcement_END']));
		}
		
		
		
		
#		echo $announcement_INFO['announcement_TITLE'];
		$param = array(
			
			array($announcement_INFO['announcement_TITLE'], PDO::PARAM_STR),
			array($announcement_INFO['announcement_CONTENT'], PDO::PARAM_STR),
			array(date("Y-m-d H:i:s",strtotime($announcement_INFO['announcement_START'])), PDO::PARAM_STR),
			array($announcement_INFO['announcement_END'], PDO::PARAM_STR),
			$announcement_INFO['announcement_ID']
			
		);
		
		SQL::execute($sql, $param);
		$id = SQL::last_id();
			SESSION::StoreMsg("Announcement Successfully Updated!", "success");
			HTML::redirect("announcement.php?action=view_all&view=all");
	}
	
	
	
	
	
	
}
?>