<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
            COURSE::addcourse(Request::post());
			break;
			case "edit": COURSE::update_course(Request::post()); break;
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			#case "delete": COURSE::delete($_GET['id']); break;
		
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>HOME | SJB CAINTA SIMS</title>
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end of alert messages -->
			<!-- start row container -->
			<!-- 
            <div class="row">
				<div class="col-lg-12">
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
            </div>	
			</div>
            -->
			<!-- end row container -->
            <!-- row container -->
			<div class="row">
				<!-- left nav -->   
            <?php
				if (!SESSION::isLoggedIn()) {
					?>
			
                <?php   }else{?>
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
                <?php } ?>
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Course </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Course Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <b>Course Name</b>
                                    <input autofocus name='course_NAME' pattern="([a-zA-Z0-9]| |/|\|@|#|$|%|&)+" placeholder="Course Name" class='form-control' type='text' required/>
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="col-sm-6">
                                    <b>Course Initial</b>
                                    <input autofocus name='course_INIT'  placeholder="Course Initial" class='form-control' type='text' required/>
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Add Course</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":
                        $siiid =$_GET['id'];
                        $getsingle = COURSE::getSingle(array("course_ID"=>"DESC"),$siiid  );
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Update Course </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Course Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <b>Course ID</b>
                                    <input disabled="disabled" placeholder="Room Name" class='form-control' type='text' required value="<?= $getsingle['course_ID'];?>"/>
                                    <input type="hidden" name="course_ID" value="<?= $getsingle['course_ID'];?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <b>Course Name</b>
                                    <input name='course_NAME' pattern="([a-zA-Z0-9]| |/|\|@|#|$|%|&)+" placeholder="Course Name" class='form-control' type='text' required value="<?= $getsingle['course_NAME'];?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <b>Course Initial</b>
                                    <input name='course_INIT' pattern="([a-zA-Z0-9]| |/|\|@|#|$|%|&)+" placeholder="Course Initial" class='form-control' type='text' required value="<?= $getsingle['course_INIT'];?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Update Course</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
						include "searching.php";						
						?>
							
                    <?php break; 
                        case "view": ?>
                    
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table>
                                <tr>
                                <td width="50%">
                                    <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                        <i class="fa fa-folder-o"></i> View Course </h3>
                                </td>
                                
                                <td >
                                    <div class="input-group col-md-12">
                                    	 <form id="form1" runat="server">
                                        		<input id="searchItem" class="searchItem form-control" name="q" type="text" onkeyup="term(q.value)" autofocus="autofocus" onchange="term(q.value)"/>
                                        </form>
                                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
                                </td>
                                	 <script>
                                    			$(document).ready(function() {
												$('#searchItem').keyup(function(){
												var s_Item = $('#searchItem').val();
													
													if(s_Item.length >= 3)
													{
														 $.ajax({
															type: "POST",
															url: "search.php?action=course.php",
															data: 'search_term=' + s_Item,
															success: function(msg){
																/* $('#resultip').html(msg); */
																	$("#display_result").show();
																	$("#display_result").html(msg);
																	
																	$("#display_hide").hide();
																
															}
											
														}); // Ajax Call
													
														//alert(s_Item);
														}else{
															$("#display_hide").show();
															$("#display_result").hide();   
														}
													});
												});			
                                    			</script>
                                <td width="5%">
                                <div class="col-md-1">   
                                <button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('course.php?action=add','_self');"><i class='fa fa-plus'></i></button>
                                </div>
                                </td>
                                </tr>
                                </table>
                            </div>
							
							<div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
								<table class="table table-hover table-responsive table-striped text-md">
									<th style="width:10%">ID</th>
									<th style="">Course Name</th>
                                    <th style="">Course Initial</th>
									<th style="">Operation</th>
                                    
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    $viewStudents = COURSE:: getallrooms(array("course_ID"=>"ASC"));
                                     if(count($viewStudents)>=10) 
                                     {
                                         $pagination2 = new Pagination($viewStudents, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($viewStudents, 10, NULL);
                                     }

                                     $viewStudents = $pagination2->get_array();

                                     if($viewStudents) {
                                        foreach($viewStudents as $key => $value){
                                    ?>
                                        <tr>
                                            <td height="21"><?php echo $value['course_ID']; ?>
                                          <td><?php echo $value['course_NAME']; ?>
                                          <td><?php echo $value['course_INIT']; ?></td> 
                                            <td><a href="course.php?action=edit&id=<?php echo $value['course_ID']; ?>" class='btn btn-warning btn-xs'>Edit</a>
                                                <!-- <a href="course.php?action=delete&id=<?php echo $value['course_ID']; ?>" class='btn btn-danger btn-xs'>Delete</a> -->
                                            </td>
                                        </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
                                        <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                                        </center>
							     </div>
                            </div>
						</div>
                            
					</div>
                </div>
                  <?php break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                	
				    
                 </div>

				    
				</div>
                
				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
            

			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
</html>
