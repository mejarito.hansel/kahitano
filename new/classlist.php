<?php
	include('init.php');

SESSION::CheckLogin();

$id = $_GET['id'];

$subj = SUBJSCHED::getID($id);
#print_r($subj);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Class List</title>
<style type="text/css">
<!--
.style3 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; }
.style5 {font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
-->
</style>
</head>

<body>
<div align="center">
  <p><span class="style5">
    <?php $ss = SUBJSCHED::getSched($subj['subject_sched_ID']); 
	
	 $s = SUBJECTS::getID($ss['subject_ID']);
							 
							# print_r($s);
                             echo $s['subject_CODE'];
	?>
    - </span> <span class="style5"><?php echo strtoupper($s['subject_DESCRIPTION']); ?></span><strong> (<span class="style3"><?php echo $s['LEC_UNIT']."/".$s['LAB_UNIT']; ?></span>)</strong><br />
    <span class="style3"><?php 
                         $sem = SEMSECTION::getID($subj['semester_section_ID']);
						 
						# print_r($sem);
						 
                      	$course = COURSE::getbyID($sem['course_ID']);
						 echo $course['course_INIT'];  ?>
-
<?php  echo $sem['semester_section_NAME']; ?>
  </span><br />
  <span class="style3">
  <?php  $room = ROOM::getID($subj['room_ID']); echo $room['room_NAME']; ?>
  </span>- <span class="style3"><?php echo substr($subj['day'],0,3); ?></span>- (<span class="style3"><?php echo $subj['start']."-".$subj['end']; ?></span>)<span class="style3">
  <br />
  <?php
                    $ins = INSTRUCTORS::getSingle1($subj['instructor_ID']);
					
					echo  strtoupper($ins['instructor_NAME']);
					?>
  </span><br />
  </p>
</div>
<p align="center"><span class="style3">Total Enrolled Student: <u><?= SUBJSCHED::checkRemaining($subj['subject_sched_ID']); ?></u>
</span></p>
<p>
  <?php
 $stud =   SUBJSCHED::getBySubjectSchedID(clean($_GET['id']),$ins['instructor_ID']);
 #var_dump($stud);
?>
</p>
<table width="629" border="1" align="center" cellpadding="3" cellspacing="0" class="style3">
  <tr>
    <td width="35"><strong>No.</strong></td>
    <td width="305"><strong>Name</strong></td>
    <td width="103"><strong>Student No.</strong></td>
    <td width="152"><div align="center"><strong>Course</strong></div></td>
  </tr>
  
  <?php 
  $x = 1;
  foreach($stud as $key => $val){ ?>
  <tr>
    <td><?= $x++; ?></td>
    <td><?php echo $val['si_LNAME'].", ".$val['si_FNAME']." ".$val['si_MNAME']; ?></td>
    <td><?php echo $val['student_ID']; ?></td>
    <td><div align="center"><?php echo USERS::getCourse($val['si_ID']); ?></div></td>
  </tr>
  <?php } ?>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
