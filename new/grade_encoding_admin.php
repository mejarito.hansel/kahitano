<?php
	include('init.php');

if (!SESSION::isLoggedIn()) {
	HTML::redirect();
}


?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
      <title>Curriculum Checklist Admin | <?php echo $site_options['site_NAME']; ?></title>
	</head>

	<body>

		<!-- Navbar -->	
		<?php include(LAYOUTS . "top_nav.php"); ?>
		<!--END Navbar -->

		<div class="container">

		<?php
		include(LAYOUTS . "banner.php");
		?>

			<!-- start body -->

			<!-- start row -->
			<div class="row">
				<div class="col-lg-12">
					<!-- breadcrumbs -->
					<ul class="breadcrumb">
						<li class=""><a href="<?php echo HOME; ?>index.php">Home</a></li>
						<li class="active">Grade Inquiry</li>
					</ul>
					<!-- end breadcrumbs -->  
				</div>
			</div>
			<!-- end row -->

			<!-- start row -->
			<div class="row">
				
                <div class="col-md-3">
                	<?php include(PAGES."navigation.php");?>
                </div>

				<div class="col-md-9">
     			  <h3>Curriculum Checklist</h3>
                
                
                <?php 
				switch($_GET['action'])
				{
				case "remove":
				$id = clean($_GET['id']);
				$si_ID = clean($_GET['si_ID']);
				SUBJENROL::hideGrade($id);
				HTML::redirect("grade_inquiry.php?action=view&id=$si_ID");
				break;
				
				
				case "view":
				$id = clean($_GET['id']);
				$info =  USERS::viewSingleStudent($_GET['id']);
				#var_dump($info);

				$mygrade = SUBJENROL::gradebyid($id);
				#var_dump($mygrade);
				?>
                <?php $i = CECONTROLLER::getAllinfo($info['si_ID']); 
				#print_r($i);
				?>
                
                
                <table class="table table-hover table-responsive table-striped text-md">
				<tr>
               	  <td>Name:</td>
                	<td colspan="4"><?php echo $info['si_LNAME'].", ".$info['si_FNAME']." ".$info['si_MNAME'];  ?>                  </td>
                </tr>
                <tr>
                
                </tr>
                <tr>
                	<td>Course: 
                  </td>
                  <td colspan="4"><?= $i['course_NAME']; ?>
             	</tr>
                <tr>
                	<td>Curriculum:
                  </td>
                  <td colspan="4"><?= $i['curricullum_NAME']; ?>
             	</tr>
                </table>
				
				<!-- START -->
				
				
				 <table border='0' class="table table-responsive text-md">
                                    
                                    <?php
                                    $getAllSemester = CURRICULLUM:: getAllSemester(array("yr_sem_ID"=>"ASC"));
                                    if($getAllSemester) {
                                    $counTemp = 1;
									
									
									
                                        foreach($getAllSemester as $key => $value){




                                            $curr_subj = CURRICULLUM::getSubjectbycurr($value['yr_sem_ID'],$i['curricullum_ID']);
                   # print_r($curr_subj);


                       # print_r($curr_subj);
                                            /*
                                            foreach($curr_subj as $key => $val_a){
                                            $subject_ID_a = $val_a['subject_ID'];
                                            $SubjectGET_a = CURRICULLUM:: getSubject($subject_ID_a);
                                            */
                                            //print_r($SubjectGET_a);

                                            //if($SubjectGET_a['subject_ID'] != $val_a['subject_ID'])

                                                //if((sizeof($curr_subj))%2 OR (sizeof($curr_subj))==0)
                                            //(sizeof($curr_subj))

                                            /*
                                               if($counTemp==2)
                                                {
                                                    $counTemp = 1;
                                               ?>
                                                        <td>
                                                <?php
                                                }else{
                                                ?>
                                                    <tr>
                                                        <td>
                                               <?php 
                                                    $counTemp ++;
                                                }
                                            */
                                            ?>

                                            <?php
                                            $totalCount = sizeof($curr_subj);
                                            //if(($totalCount %2 != 2) OR ($temppppp > 0)){
                                            if(($totalCount %2)){
                                                if($counTemp == 2)
                                                {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                    <?php
                                                    $counTemp = 1;
                                                }else{
                                                ?>
                                                    
                                                        <td>
                                                <?php
                                                }
                                                    $counTemp ++;
                                            }
                                    ?>

                                    <!--
                                    <table id="sem<?= $value['yr_sem_ID']; ?>" border="1" class="" style="border-top: solid 4px black; border-bottom: solid 2px black;">
                                    -->
                                    
                                    <table id="sem<?= $value['yr_sem_ID']; ?>" border="1" class="table table-responsive text-md" style="border-top: solid 4px black; border-bottom: solid 2px black;">
                                    
                                                            <th style="border-bottom: solid 2px black;"><?= $value['yr_sem_NAME']; ?>
                                                                <tr>
                                                                    <td>
                                                                        <table border='0' class="table table-responsive text-md">
                                                                            <!-- <th width="10%">S-ID -->
                                                                            <th width="10%">S-Code
                                                                            <th width="30%">Subject Description
                                                                            <th width="10%"  style="text-align:center">Units
                                                                            <th width="20%" style="text-align:center">PreRequisite
                                                                            <th width="" style="text-align:center">Option
                                                                     
                                                                               
                                                            <?php
                                                                foreach($curr_subj as $key => $val)
                                                                {
                                                            ?>
                                                                <tr>
                                                                    <!-- <td colspan='6'> -->
                                                                        <!--<table border='1'>-->
                                                                        <?php 
                                                                        /*
                                                                            foreach($curr_subj as $key => $val_a){
                                                                            //if(($subject_ID_a)=($val_a['subject_ID'])){
                                                                            $subject_ID_a = $val_a['subject_ID'];
                                                                            $SubjectjectGET_a = CURRICULLUM:: getSubject($subject_ID_a);
                                                                            */
                                                                            $subject_ID = $val['subject_ID'];
                                                                            $SubjectGET = CURRICULLUM:: getSubject($subject_ID);
                                                                            //print_r($SubjectGET);
                                                                            foreach($SubjectGET as $subj => $val1)
                                                                            {
                                                                            ?>
                                                                                <tr>
                                                                                   <!-- <td><?= $val1['subject_ID'];?> -->
                                                                                    <td><?= $val1['subject_CODE'];?>
                                                                                    <td><?= $val1['subject_DESCRIPTION'];?>
                                                                                    <td align="center"><?= ($val1['LEC_UNIT']+$val1['LAB_UNIT']);?>
                                                                                    <td align="center"><?php 
																									$preq = SUBJECTS::getID($val['prerequisite_subject_ID']);
																										echo $preq['subject_CODE'];
																										
																										if(sizeof($preq) == 1){ echo "None"; }
																								 ?>
                                                                                  <td align="center">
                                                                                  <?php
                                                                                  $grade = CECONTROLLER::getEnrolledGrade($info['si_ID'],$val1['subject_ID']);
																				  
																				 #print_r($grade);
																				  
																				  if(!is_null($grade['grade']) && $grade['is_check'] == 0)
																				  {
																				  echo "NOT YET ENCODED";
																				  }else{
                                                                                  echo $grade['grade'];
																				  }
																				  
																				  
																				  if(is_null($grade['grade']) and !is_null($grade['subject_enrolled_ID']))
																				  {
																				  echo "NOT YET ENCODED";
																				  }else if(is_null($grade['grade']) and is_null($grade['subject_enrolled_ID']))
																				  {
																				 	$grade = CECONTROLLER::getEncodedGrade($info['si_ID'],$val1['subject_ID']);
																					
																					#print_r($grade);
																					# echo $grade['grade'];
																					 
                                                                                     
                                                                                     if(!is_null($grade['grade'])){
																					 ?>
																					 
																					 <form>
                                                                                      <select name="grade<?= $val1['subject_ID']; ?>" class="form-control">
                                                                                        <option value="NG">NO GRADE</option>
                                                                                        <option value="1.00" <?php if($grade['grade'] == "1.00"){ echo " SELECTED=SELECTED "; } ?>>1.00</option>
                                                                                        <option value="1.25" <?php if($grade['grade'] == "1.25"){ echo " SELECTED=SELECTED "; } ?>>1.25</option>
                                                                                        <option value="1.50" <?php if($grade['grade'] == "1.50"){ echo " SELECTED=SELECTED "; } ?>>1.50</option>
                                                                                        <option value="1.75" <?php if($grade['grade'] == "1.75"){ echo " SELECTED=SELECTED "; } ?>>1.75</option>
                                                                                        <option value="2.00" <?php if($grade['grade'] == "2.00"){ echo " SELECTED=SELECTED "; } ?>>2.00</option>
                                                                                        <option value="2.25" <?php if($grade['grade'] == "2.25"){ echo " SELECTED=SELECTED "; } ?>>2.25</option>
                                                                                        <option value="2.50" <?php if($grade['grade'] == "2.50"){ echo " SELECTED=SELECTED "; } ?>>2.50</option>
                                                                                        <option value="2.75" <?php if($grade['grade'] == "2.75"){ echo " SELECTED=SELECTED "; } ?>>2.75</option>
                                                                                        <option value="3.00" <?php if($grade['grade'] == "3.00"){ echo " SELECTED=SELECTED "; } ?>>3.00</option>
                                                                                        <option value="5.00" <?php if($grade['grade'] == "5.00"){ echo " SELECTED=SELECTED "; } ?>>5.00</option>
                                                                                        <option value="INC" <?php if($grade['grade'] == "INC"){ echo " SELECTED=SELECTED "; } ?>>INCOMPLETE</option>
                                                                                        <option value="DRP" <?php if($grade['grade'] == "DRP"){ echo " SELECTED=SELECTED "; } ?>>DROPPED</option>
                                                                                        <option value="C" <?php if($grade['grade'] == "C"){ echo " SELECTED=SELECTED "; } ?>>CREDIT</option>
                                                                                        
                                                                                    </select>   
                                                                                    
                                                                                    </form>
																					 <?php } ?>
																					 
																					 <?php
																				  }
																				 
																				  
																				  
																				  
																				  
																				  if( is_null($grade['grade']) and is_null($grade['subject_enrolled_ID']))
																				  {
																				  
																				  ?>
																				  <form>
                                                                                  <select name="grade<?= $val1['subject_ID']; ?>" class="form-control">
                                                                                    <option value="NG">NO GRADE</option>
                                                                                    <option value="1.00" <?php if($val['grade'] == "1.00"){ echo " SELECTED=SELECTED "; } ?>>1.00</option>
                                                                                    <option value="1.25" <?php if($val['grade'] == "1.25"){ echo " SELECTED=SELECTED "; } ?>>1.25</option>
                                                                                    <option value="1.50" <?php if($val['grade'] == "1.50"){ echo " SELECTED=SELECTED "; } ?>>1.50</option>
                                                                                    <option value="1.75" <?php if($val['grade'] == "1.75"){ echo " SELECTED=SELECTED "; } ?>>1.75</option>
                                                                                    <option value="2.00" <?php if($val['grade'] == "2.00"){ echo " SELECTED=SELECTED "; } ?>>2.00</option>
                                                                                    <option value="2.25" <?php if($val['grade'] == "2.25"){ echo " SELECTED=SELECTED "; } ?>>2.25</option>
                                                                                    <option value="2.50" <?php if($val['grade'] == "2.50"){ echo " SELECTED=SELECTED "; } ?>>2.50</option>
                                                                                    <option value="2.75" <?php if($val['grade'] == "2.75"){ echo " SELECTED=SELECTED "; } ?>>2.75</option>
                                                                                    <option value="3.00" <?php if($val['grade'] == "3.00"){ echo " SELECTED=SELECTED "; } ?>>3.00</option>
                                                                                    <option value="5.00" <?php if($val['grade'] == "5.00"){ echo " SELECTED=SELECTED "; } ?>>5.00</option>
                                                                                    <option value="INC" <?php if($val['grade'] == "INC"){ echo " SELECTED=SELECTED "; } ?>>INCOMPLETE</option>
                                                                                    <option value="DRP" <?php if($val['grade'] == "DRP"){ echo " SELECTED=SELECTED "; } ?>>DROPPED</option>
                                                                                     <option value="C" <?php if($grade['grade'] == "C"){ echo " SELECTED=SELECTED "; } ?>>CREDIT</option>
                                                                                </select>   
                                                                                
                                                                                </form>
                                                                                
                                                                               
                                                                                  <?php
																				  
																				  }
																				  
                                                                                  ?>
                                                                                  
                                                                                 <span id="result<?php echo $val['subject_ID']; ?>"></span>
                                                                            <?php
                                                                            }   
																			
                                                                        ?>
                                                                       			
                                                                        <!--</table>-->
                                                                        <?// = $val['subject_ID']; ?>
                                                                    </td>
                                                            <?php
                                                            }

                                                                if(sizeof($curr_subj)==0)  
																	{
																		
																		if(isset($_GET['hidden']) && $_GET['hidden'] == 'yes')
																		{
																		
																		}else{
																		?>
																		
																			<script type="text/javascript">
                                                                                $("#sem<?php echo $value['yr_sem_ID']; ?>").toggle();
                                                                            </script>
																		<?php
																		}
																		
																		
																	} 
																	
																	
																	
																	
																	
																	
															?>		
																	
															<script>
								$(document).ready(function(){
								
								
									<?php  foreach($curr_subj as $key => $val){ ?>
										  $('[name="grade<?php echo $val['subject_ID']; ?>"]').each(function() {
												$(this).change(function() {
													var mydata = $('[name="grade<?php echo $val['subject_ID']; ?>"]').val();//$(this).val();
													var inputdata = <?php echo $val['subject_ID']; ?>;
													var studentID = <?php echo $info['si_ID']; ?>;
												//alert(mydata)
												//alert(inputdata)
												//alert(studentID)
											
													$.ajax({   
													   type: 'GET',   
													   url: 'update_ajax.php',   
													   data: {action:'grade_encoding_admin.php',subject_ID:inputdata, grade:mydata, student_ID:studentID},
															success: function(data){
															
															  $('#result<?php echo $val['subject_ID']; ?>').html(data);
															}													
														});
												
												
													});
													
										  	});
									<?php } ?>		
								});
								</script>		
																	
														<?php			
																
                                                            //loop of subjects
                                                                        /*
                                                                ?>
                                                                <script type="text/javascript">
                                                                $(document).ready(function(){
                                                               
                                                                <?php
                                                                $getAllSemester = CURRICULLUM:: getAllSemester(array("yr_sem_ID"=>"ASC"));
                                                                if($getAllSemester)
                                                                {
                                                                    foreach($getAllSemester as $key => $value)
                                                                    {
                                                                        $curr_subj = CURRICULLUM::getSubjectbycurr($value['yr_sem_ID'],$cur_ID);
                                                                    foreach($curr_subj as $key => $val)
                                                                    {
                                                                          
                                                                    }
                                                                }
                                                                ?>
                                                                });
                                                                </script>
                                                                <?php
                                                                
                                                            } //last
                                                            */
															
                                                            ?>
                                                            	  
                                                            		 
                                                                        </table>
                                    </table>                                    
                        <?php    
                    } //foreach getallsemester
                } //if getallsemester

                    ?>			<a target="_blank" href="print_curriculum.php?id=<?= $_GET['id']; ?>" class="btn btn-success btn-small">Print Curriculum Checklist</a>
                                </table>
				
				
				
				
				
				
				
				<!-- END -->
				<?php
				break;
				?>
                 
                <?php case "search": ?>
                	
                      
                      
                      
                      
                      <div class="panel-heading">
									<table class=""  style="width:100%;">
                                		<tr>
                                			<td width="20%">
												<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> Search</h3>
											</td>

										 	<td width="80%">
                                    			<div class="input-group col-md-12">
                                   				  <script src="jquery.js" type="text/javascript"></script>
												  <script> 
                                                    $(document).ready(function(){
                                                      $("#flip").click(function(){
                                                        $("#panel").slideToggle("fast");
														$('#display_hide').trigger('click');
                                                      });
                                                    });
                                                    </script>
                                                    <form id="form1" runat="server">
                                                    	<input id="searchItem" class="searchItem form-control" name="q" type="text" onkeyup="term(q.value)" autofocus="autofocus" onchange="term(q.value)"/>
                                                    </form>
                                      					<span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    			</div>
                                			</td>
                                            
                           		
                                			
                                             <script>
                                    				$(document).ready(function() {
												$('#searchItem').keyup(function(){
												var s_Item = $('#searchItem').val();
													
													if(s_Item.length >= 3)
													{
														 $.ajax({
															type: "POST",
															url: "search.php?action=grade_encoding_admin.php",
															data: 'search_term=' + s_Item,
																success: function(msg){
																	/* $('#resultip').html(msg); */
																		$("#display_result").show();
																		$("#display_result").html(msg);
																		
																		
																		$("#display_hide").hide();
																		$("#pagination").hide();
																	
																}
											
															}); // Ajax Call
													
															//alert(s_Item);
														}else{
															$("#display_result").hide();   
															
														}
													});
												});			
                                    			</script>
                                            
                                			
                                		</tr>
                                	</table>
				  </div>

								<div class="panel-body">
									<div id="display_result"></div>
                                	
								</div>
							
                      
                     <?php break; 
					  default: HTML::redirect("grade_encoding_admin.php?action=search");  ?>
                    <?php } ?>  
                      
                      
                      
			  </div>
				
			
			<!-- end row -->

			<!-- end body -->

			<div class="spacer-short"></div>

			  </div>
			<!-- footer-->
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- container end -->
</div>
	</body>
</html>