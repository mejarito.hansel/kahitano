<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
            INSTRUCTORS:: addInstructor(Request::post());
			#var_dump(Request::post());
			
			break;
			case "edit": INSTRUCTORS::update_Instructor($_GET['id'],Request::post()); break;
			#case "update_blog":  BLOG::update(Request::post("blog_info")); break;
			
			#case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": INSTRUCTORS::delete($_GET['id']); break;
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
      <title>HOME | SJB CAINTA SIMS</title>
	</head>

	<body>

		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->

		<div class="container">
			
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			
          
            
            
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end of alert messages -->
			
			<!-- start row container -->
			<!-- 
            <div class="row">
				
				<div class="col-lg-12">

				
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
				

				</div>
				
			</div>
            -->
			<!-- end row container -->
			
   
             
            <!-- row container -->
			<div class="row">

				<!-- left nav -->
                
            <?php
				if (!SESSION::isLoggedIn()) {
					?>
				
                <?php   }else{?>
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
                <?php } ?>
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                      		<div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Instructor </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                        	<fieldset>
                            <legend><i class="fa fa-user"></i> Instructor Information</legend>
                            <!-- Name -->
                           
                           <div class="form-group">
                                <div class="col-sm-6">
                                   <b>User Name</b>
                                    <input autofocus name='instructor_UNAME' placeholder="User Name" class='form-control' type='text' required/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-6">
                                   <b>Password</b>
                                    <input autofocus name='instructor_UPASS' placeholder="Password" class='form-control' type='text' required/>
                                </div>
                            </div>
                           
                            <div class="form-group">
                                <div class="col-sm-6">
                                   <b>First Name</b>
                                    <input  name='instructor_FNAME' placeholder="First Name" class='form-control' type='text' required/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-6">
                                   <b>Middle Name</b>
                                    <input  name='instructor_MNAME' placeholder="Middle Name" class='form-control' type='text' required/>
                                </div>
                            </div>
                            
                             <div class="form-group">
                                <div class="col-sm-6">
                                   <b>Last Name</b>
                                    <input  name='instructor_LNAME' placeholder="Last Name" class='form-control' type='text' required/>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Add Instructor</button>
                                </div>
                            </div>
                        </fieldset>
                    	</form>
                    </div>
                	</div>
                    <?php break; 
                        case "edit":
                        $instructor_ID  =$_GET['id'];
                        $getsingle = INSTRUCTORS::getSingle(array("instructor_ID"=>"DESC"),$instructor_ID  );
                        
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Update Instructor </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                        	<table border='0' class="table table-responsive text-md">
                        			<tr>
                        				<td>&nbsp;
                        					
                        				</td>

                        				<td>
                        					<label for='subject_ID' class='col-md-3'>ID</label>
                        				</td>

                        				<td>
                        					<input name='id' class='form-control' type='hidden' value="<?= $getsingle['instructor_ID'];?>"/>
                        					<input name='instructor_ID' class='form-control' disabled type='text' value="<?= $getsingle['instructor_ID'];?>"/>
                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;
                        					
                        				</td>

                        				<td>
                        					<label for='instructor_NAME' class='col-md-3'>NAME</label>
                        				</td>

                        				<td>
                        					<input name='instructor_NAME' class='form-control' type='text' value="<?= $getsingle['instructor_NAME'];?>"/>
                            			</td>
                            		</tr>

                            		<tr>
                            			<td colspan='3' align='right'>
                                    		<button  class='btn btn-success btn-ms' type='submit'><i class='fa fa-edit'></i> Update Instructor</button>
                            			</td>
                            		</tr>
                            </table>
                    	</form>
                    </div>
                	</div>
                </div>
                </div>
            
                    <?php break; 
                        case "view":
					?>
						<?php
						if(!isset($_GET['id']))
						{
						?>
					
						<div class="panel panel-default">
							<div class="panel-heading">
								<table>
									<tr>
											<td width="50%">
												<h3 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px"><i class="fa fa-folder-o"></i> Instructor List </h3>
											</td>
											<td>
												<!--<input type="text" placeholder="Search" class="form-control">-->
												 <div class="input-group col-md-12">
                                                    <form id="form1" runat="server">
                                                         <input id="searchItem" class="searchItem form-control"  name="q" type="text" onkeyup="term(q.value)" autofocus="autofocus" onchange="term(q.value)"/>
                                                    </form>
                                                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                </div>
                                                <script>
                                    			$(document).ready(function() {
												$('#searchItem').keyup(function(){
												var s_Item = $('#searchItem').val();
													
													if(s_Item.length >= 3)
													{
														 $.ajax({
															type: "POST",
															url: "search.php?action=instructors.php",
															data: 'search_term=' + s_Item,
															success: function(msg){
																/* $('#resultip').html(msg); */
																	$("#display_result").show();
																	$("#display_result").html(msg);
																	$("#display_hide").hide();
																
															}
											
														}); // Ajax Call
													
														//alert(s_Item);
														}else{
															$("#display_hide").show();
															$("#display_result").hide();   
														}
													});
												});			
                                    			</script>
											</td>

											
                                            <td width="5%">
                                                <div class="col-md-1">   
                                                	<button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('instructors.php?action=add','_self');"><i class='fa fa-plus'></i></button>
                                                </div>
                                            </td>
									</tr>
								</table>
							</div>

							<div id="display_result"></div>

							<div class="panel-body" id="display_hide">
								<table class="table table-hover table-responsive table-striped text-md">
									<!--
									<tr>
										<td colspan='4'>
										<td align='right' style="padding-right: 15px;">
											<a href="/isjb/instructors.php?action=add" class='btn btn-success btn-ms'>
												<i class="fa fa-plus"></i>
											</a>
										</td>
									-->
									</tr>

									<th style="width:15%;">ID</th>
									<th style="width:40%;">NAME</th>
									<th style="width:50%;">OPTIONS</th>	
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    $viewIntructors = INSTRUCTORS:: getAllInstructors(array("instructor_ID"=>"ASC"));
                                     if(count($viewIntructors)>=10) 
                                     {
                                         $pagination2 = new Pagination($viewIntructors, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($viewIntructors, 10, NULL);
                                     }

                                     $viewIntructors = $pagination2->get_array();

                                     if($viewIntructors) {

                                        foreach($viewIntructors as $key => $value){
                                    ?>
                                        <tr>
                                            <td>
                                                <?php echo $value['instructor_ID']; ?>
                                            <td>
                                                <?php echo $value['instructor_NAME']; ?>
                                            <td>
                                                <a href="instructors.php?action=edit&id=<?php echo $value['instructor_ID']; ?>" class='btn btn-warning btn-xs'>
                                                    Edit
                                                </a>
                                                &nbsp;&nbsp;&nbsp;
                                                <!--
                                                <a href="instructors.php?action=delete&id=<?php echo $value['instructor_ID']; ?>" onClick="return confirm('You\'re about to delete the subject.')"; class='btn btn-warning btn-xs'>
													Delete
												</a>
                                                -->
                                            </td>
                                        </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
								<center>
                                <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                            </center>
								</div>
							</div>
						</div>
					</div>
                </div>
                  <?php } break; ?>
				  
				  
         <div id="myTabContent" class="tab-content">         
                  
              <!-- all --><div class="tab-pane fade " id="all">

                    <?php 
                        case "delete":?>
                        view
                <?php break;
                    }}else{?>
                    
                <?php } ?>
                	 </div>
				</div>
                
				<!-- end right nav -->
			</div>
			<!-- end row container -->
            
            
			<!-- footer-->      
            
            <?php 
			#echo MESSAGES::check_message("gago");
			?>
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
	
</html>
