<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::post("action")) {
			case "add": 
			
			break;
			case "change_lock":
				SUBJSCHED::change_lock(Request::post());
			break;
			
			case "change_check":
				SUBJSCHED::change_check(Request::post());
			break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ROOM::delete($_GET['id']); break;

		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>GRADE CHECKING | CSJP-II-AS</title>
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end of alert messages -->
			<!-- start row container -->
			<!-- 
            <div class="row">
				<div class="col-lg-12">
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
            </div>	
			</div>
            -->
			<!-- end row container -->
            <!-- row container -->
			<div class="row">
				<!-- left nav -->   
          
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
               
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Semester Section</h3> 
                    </div>
                    <div class="panel-body">
                    
                    <?php
					$sem = SEM::getallsems(array("sem_ID"=>"DESC"));
					#print_r($sem);
					
					$course = COURSE::getallcourse(array("course_NAME"=>"ASC"));
					?>
                    <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Semester Section Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                              <div class="col-sm-6">
                                  <label for='si_FNAME' class=''>Semester: </label>
                                  <select class='form-control' name="sem_ID">
                                  	<?php foreach($sem as $key => $val){ ?>
                                    <option value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
                                    <?php } ?>
                                  </select>
                              </div>
                            </div>
                            
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Course:</label>
                                    <select class='form-control' name="course_ID">
										<?php foreach($course as $key => $val){ ?>
                                        <option value="<?php echo $val['course_ID']; ?>"><?php echo $val['course_NAME']; ?></option>
                                        <?php } ?>    
                                    </select>
                               </div>
                             </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Section Name:</label>
                                    <input name='semester_section_NAME' placeholder="Section Name" class='form-control' type='text' required="required"/>
                               </div>
                            </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''></label>
                               </div>
                            </div>                            
                            
                            
                             <div class="form-group"></div>                            
                            
                            
                            
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Add Semester Section</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":

          			$geThis = SEMSECTION::geThis($_GET['id']);
					#print_r($geThis);
					?>
                    <?php
					$sem = SEM::getallsems(array("sem_ID"=>"DESC"));
					#print_r($sem);
					
					$course = COURSE::getallcourse(array("course_NAME"=>"ASC"));
					?>
                                        
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Update Semester Section</h3> 
                    </div>
                    <div class="panel-body">
                       <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Semester Section Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                              <div class="col-sm-6">
                                  <label for='si_FNAME' class=''>Semester: </label>
                                  <select class='form-control' name="sem_ID">
                                  	<?php foreach($sem as $key => $val){ ?>
                                    <option value="<?php echo $val['sem_ID']; ?>"
                                    
                                    <?php
									  if($val['sem_ID'] == $geThis['sem_ID']){
										echo "selected=selected";
									  }
									?>
                                    
                                    ><?php echo $val['sem_NAME']; ?></option>
                                    <?php } ?>
                                  </select>
                              </div>
                            </div>
                            
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Course:</label>
                                    <select class='form-control' name="course_ID">
										<?php foreach($course as $key => $val){ ?>
                                        <option value="<?php echo $val['course_ID']; ?>" 
                                        
                                          <?php
											  if($geThis['course_ID'] == $val['course_ID']){
												echo "selected=selected";
											  }
											?>
                                        
                                        ><?php echo $val['course_NAME']; ?></option>
                                        <?php } ?>    
                                    </select>
                               </div>
                             </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Section Name: </label>
                                    <input name='semester_section_NAME' value="<?php echo $geThis['semester_section_NAME']; ?>" placeholder="Section Name" class='form-control' type='text' required="required"/>
                               </div>
                            </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''></label>
                               </div>
                            </div>                            
                            
                            
                             <div class="form-group"></div>                            
                            
                            
                            
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Update Semester Section</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
						include "searching.php";						
						?>
							
                    <?php break; 
                        case "view":?>
                       
                       
                       
                       <ul class="breadcrumb">
                                        <li class="active"><a href="grade_checking.php?action=view">Grade Checking</a></li>
                                        
                                        <?php if(isset($_GET['sem_ID']))
										{	$sem = SEM::getSingleSem($_GET['sem_ID']) ?>
                                        
                                        <li class="active"><a href="?action=view&sem_ID=<?= $_GET['sem_ID'] ?>"><?php echo $sem['sem_NAME']; ?></a></li>
                                    	<?php } ?>
                                        
                                        
                                         <?php if(isset($_GET['fac_id']))
										{	$f = INSTRUCTORS::getSingle1($_GET['fac_id']) ?>
                                        
                                        <li class="active"><a href="?action=view&sem_ID=<?= $_GET['sem_ID'] ?>&fac_id=<?= $_GET['fac_id']; ?>"><?php  echo $f['instructor_NAME']; ?></a></li>
                                    	<?php } ?>
                                        
                                        <?php if(isset($_GET['subject_sched_id'])){ 
										$su = SUBJSCHED::getSched($_GET['subject_sched_id']) ;
										#print_r($su);
										?>
                                        <li class="active"><a href="?action=view&sem_ID=<?= $_GET['sem_ID'] ?>&fac_id=<?= $_GET['fac_id']; ?>&subject_sched_id=<?= $_GET['subject_sched_id']; ?>">
                                        
                                         <?php $c=COURSE::getbyID($su['course_ID']); 
												echo $c['course_INIT']."-";
                                              echo $su['semester_section_NAME']; ?> -
                                           <?php $s = SUBJECTS::getID($su['subject_ID']);
												echo $s['subject_CODE'];
												 ?> 
                                        
                                        </a></li>
                                    	
                                        <?php } ?>
                                        
                                        
                                       
                                        
                                        </ul>
                       
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table width="100%">
                                <tr>
                                    <td colspan="6">
                                    
                                    
                                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                            <i class="fa fa-folder-o"></i> Grade Checking</h3>                                                                      
                                      <!--
                                        <div class="col-md-1">   
                                            <button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('?action=add','_self');"><i class='fa fa-plus'></i></button>
                                        </div>
                                    -->                                        </td>
                                  </tr>
                                <tr>
                                  <td width="10%">Semester:</td>
                                  <td width="28%" >
                                  <?php
								  $sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
								  ?>
                                  <form action="<?= $_SERVER['REQUEST_URI']; ?>" method="GET">
                                  <input type="hidden" value="view" name="action" />
                                  
                                  <select class='form-control' name="sem_ID" onchange="this.form.submit()">
								  <option value="">Please Choose</option>
                                		<?php foreach($sem as $key => $val){ ?>
                                        <option <?php if(isset($_GET['sem_ID'])){ if($_GET['sem_ID'] == $val['sem_ID']){ echo " SELECTED=SELECTED "; } } ?> value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
                                        <?php } ?>
                                  </select>
                                  </form></td>
                                  <td width="3%" >&nbsp;</td>
                                  
                                  <?php if(isset($_GET['sem_ID'])){ ?>
                                  <?php $faculty = SEM::allfacultypersem($_GET['sem_ID']);       
								  #print_r($faculty);
								  ?>
                                  <td width="5%">&nbsp;</td>
                                  <td width="31%">&nbsp;</td>
                                  
                                  <?php } ?>
                                  <td width="23%">&nbsp;</td>
                                </tr>
                                <tr>
                                  <td>Faculty:</td>
                                  <td><form action="<?= $_SERVER['REQUEST_URI']; ?>" method="get">
                                      <input type="hidden" value="view" name="action" />
                                      <input type="hidden" value="<?= $_GET['sem_ID']; ?>" name="sem_ID" />
                                      <select class='form-control' name="fac_id" onchange="this.form.submit()">
                                        <option  disabled value="">Please Choose</option>
                                        <?php foreach($faculty as $key => $val){ ?>
                                        <option <?php if(isset($_GET['fac_id'])){ if($_GET['fac_id'] == $val['instructor_ID']){ echo " SELECTED=SELECTED "; } } ?> value="<?php echo $val['instructor_ID']; ?>">
                                          <?php
									  $f = INSTRUCTORS::getSingle1($val['instructor_ID']); 
											echo $f['instructor_NAME']; 
									   ?>
                                        </option>
                                        <?php } ?>
                                      </select>
                                  </form></td>
                                  <td >&nbsp;</td>
                                  
                                  <?php if(isset($_GET['fac_id'])){ ?>
                                  <td>Subject:</td>
                                  <td><form action="<?= $_SERVER['REQUEST_URI']; ?>" method="get">
                                      <input type="hidden" value="view" name="action" />
                                      <input type="hidden" value="<?= $_GET['sem_ID']; ?>" name="sem_ID" />
                                      <input type="hidden" value="<?= $_GET['fac_id']; ?>" name="fac_id" />
                                      <select required class='form-control' name="subject_sched_id" onchange="this.form.submit()">
                                        <option disabled value="">Please Choose</option>
                                        
                                        
                                        <?php
										$getmysubj = SUBJSCHED::getByfaculty($_GET['sem_ID'], $_GET['fac_id']);
										?>
                                        
                                        <?php foreach($getmysubj as $key => $val){ ?>
                                        <option value="<?= $val['subject_sched_ID']; ?>" <?php if(isset($_GET['subject_sched_id'])){ if($_GET['subject_sched_id'] == $val['subject_sched_ID']){ echo " SELECTED=SELECTED "; } } ?> >
                                        <?php $s = SUBJECTS::getID($val['subject_ID']);	echo $s['subject_CODE'] ." - "; $c=COURSE::getbyID($val['course_ID']); echo $c['course_INIT']."-"; echo $val['semester_section_NAME']; ?>
                                        </option>
                                        <?php } ?>
                                      </select>
                                  </form></td>
                                  <?php } ?>
                                  <td>&nbsp;</td>
                                </tr>
                                
                                </table>
                          </div>
							
							<div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
                                
                              
                                
                                
                                
                                
                                
                                
                                <?php 
								if(isset($_GET['subject_sched_id'])){
								
								$ss = SUBJSCHED::getSched(clean($_GET['subject_sched_id']));
								
								?>
                                    <center>
                                        <?php $s = SUBJECTS::getID($ss['subject_ID']);
                                            echo $s['subject_CODE']."-".$s['subject_DESCRIPTION'];
                                             ?> <br />
                                             
                                              <?php $c=COURSE::getbyID($ss['course_ID']); 
                                            echo $c['course_INIT']."-".$ss['semester_section_NAME'];
                                            ?>
                                            <br />
                                            <?php echo $ss['day']; ?> 
                                            <br />
                                            <?php echo $ss['start']; ?> - <?php echo $ss['end']; ?> 
        <br />
        <div style="width:370px">
        	<span style="float:left">
        						<form method="post">Check Status: 	
                                <input type="hidden" value="change_check" name="action" />
                                <input type="hidden" value="<?= $_GET['subject_sched_id']; ?>" name="subject_sched_id" />		
                                  <input type="hidden" value="<?= $_SERVER['REQUEST_URI']; ?>" name="url" />
									<select name="is_check" onchange="this.form.submit()">
                                        <option value="0" <?php if($ss['is_check'] == 0){ echo " SELECTED=SELECTED "; } ?>>Unchecked</option>
                                        <option value="1" <?php if($ss['is_check'] == 1){ echo " SELECTED=SELECTED "; } ?>>Checked</option>
                                     </select>
                                </form>      
      							</span>
        <span style="float:right">
        						<form method="post" > Lock Status:	
                                <input type="hidden" value="change_lock" name="action" />
                                 <input type="hidden" value="<?= $_GET['subject_sched_id']; ?>" name="subject_sched_id" />
                                 <input type="hidden" value="<?= $_SERVER['REQUEST_URI']; ?>" name="url" />			 
                                    <select name="is_lock" onchange="this.form.submit()">
                                       <option value="0" <?php if($ss['is_lock'] == 0){ echo " SELECTED=SELECTED "; } ?>>Unlocked</option>
                                       <option value="1"  <?php if($ss['is_lock'] == 1){ echo " SELECTED=SELECTED "; } ?>>Locked</option>
                                        
                                    </select>
                                 </form>     
       </span>                          
       </div>                          
                                    </center>
								<form action="" method="get">
                                
                                <table class="table table-hover table-responsive table-striped text-md">
									<tr><th style="width:10%;">#</th>
									<th style="width:30%;">Student Name</th>
									<th style="width:20%;text-align:center" >Grade</th>
                                    <th style="widows:20%; text-align:center">Remarks</th>
                                    <th style="width:20%;text-align:center">Status</th>
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    #$viewStudents = ROOM:: getallrooms(array("room_ID"=>"ASC"));
	                                 $stud =   SUBJSCHED::getBySubjectSchedID(clean($_GET['subject_sched_id']),$_GET['fac_id']);
									 #var_dump($stud);
									$x=1;
                                    ?>
                                       
                                     <?php foreach($stud as $key => $val) { ?>  
                                        <tr>
                                            <td><?= $x++; ?>
                                            <td><?php $u = USERS::getSingle(array("si_ID"=>"ASC"),$val['si_ID']);
											echo $u['si_LNAME'].", ".$u['si_FNAME']." ".$u['si_MNAME'];
											
											 ?>
                                            <td><select name="grade<?php echo $val['subject_enrolled_ID']; ?>" class="form-control">
                                            	<option value="NG">NO GRADE</option>
                                            	<option value="1.00" <?php if($val['grade'] == "1.00"){ echo " SELECTED=SELECTED "; } ?>>1.00</option>
                                                <option value="1.25" <?php if($val['grade'] == "1.25"){ echo " SELECTED=SELECTED "; } ?>>1.25</option>
                                                <option value="1.50" <?php if($val['grade'] == "1.50"){ echo " SELECTED=SELECTED "; } ?>>1.50</option>
                                                <option value="1.75" <?php if($val['grade'] == "1.75"){ echo " SELECTED=SELECTED "; } ?>>1.75</option>
                                                <option value="2.00" <?php if($val['grade'] == "2.00"){ echo " SELECTED=SELECTED "; } ?>>2.00</option>
                                                <option value="2.25" <?php if($val['grade'] == "2.25"){ echo " SELECTED=SELECTED "; } ?>>2.25</option>
                                                <option value="2.50" <?php if($val['grade'] == "2.50"){ echo " SELECTED=SELECTED "; } ?>>2.50</option>
                                                <option value="2.75" <?php if($val['grade'] == "2.75"){ echo " SELECTED=SELECTED "; } ?>>2.75</option>
                                                <option value="3.00" <?php if($val['grade'] == "3.00"){ echo " SELECTED=SELECTED "; } ?>>3.00</option>
                                                <option value="5.00" <?php if($val['grade'] == "5.00"){ echo " SELECTED=SELECTED "; } ?>>5.00</option>
                                                <option value="INC" <?php if($val['grade'] == "INC"){ echo " SELECTED=SELECTED "; } ?>>INCOMPLETE</option>
                                                <option value="DRP" <?php if($val['grade'] == "DRP"){ echo " SELECTED=SELECTED "; } ?>>DROPPED</option>
                                                
                                            </select>   
                                            <input type="hidden" name="subject_enrolled_ID<?php echo $val['subject_enrolled_ID']; ?>" value="<?php echo $val['subject_enrolled_ID']; ?>" />                                         
                                           
                                           <td align="center">
                                           
                                            <?php 
											 	switch($val['grade'])
												{
													case "1.00":
													case "1.25":
													case "1.50":
													case "1.75":
													case "2.00":
													case "2.25":
													case "2.50":
													case "2.75":
													case "3.00": echo "PASSED";
													break;
													case "5.00": echo "FAILED";
													break;
													case "INC": echo "INCOMPLETE";
													break;
													case "DRP": echo "DROPPED";
													break;
													default: echo "NO GRADE";
												}
											 ?>
                                           
                                           
                                           </td>
                                           
                                            <td><div id="result<?php echo $val['subject_enrolled_ID']; ?>" align="center">
                                            <?php 
											 	switch($val['grade'])
												{
													case "1.00":
													case "1.25":
													case "1.50":
													case "1.75":
													case "2.00":
													case "2.25":
													case "2.50":
													case "2.75":
													case "3.00": 
													case "5.00": 
													case "INC": 
													case "DRP":
													case "NG":
														echo "Grade Encoded";		
														break;											
													default: echo "<span style='color:red'> No Grade Encoded</span>";
												}
											 ?>
                                            </div></td>
                                        </tr>
                                        
                                        <?php
                                      }
                                       
                                    ?>
								</table>
                                
                                </form> 
								
                                Total Records: <?php echo count($stud); ?>
                                
                                
                                <script>
								$(document).ready(function(){
								
								
									<?php foreach($stud as $key => $val){ ?>
										  $('[name="grade<?php echo $val['subject_enrolled_ID']; ?>"]').each(function() {
												$(this).change(function() {
													var mydata = $('[name="grade<?php echo $val['subject_enrolled_ID']; ?>"]').val();//$(this).val();
													var inputdata = $('[name="subject_enrolled_ID<?php echo $val['subject_enrolled_ID']; ?>"]').val();
											//	alert(mydata)
											//	alert(inputdata)
													$.ajax({   
													   type: 'GET',   
													   url: 'update_ajax.php',   
													   data: {action: 'grade_encoding.php',subject_enrolled_ID:inputdata, grade:mydata},
													    success: function(data){
      													  $('#result<?php echo $val['subject_enrolled_ID']; ?>').html(data);
   														 }													
													});
												});
										  	});
									<?php } ?>		
								});
								</script>
                                
                                
								
								<?php
								}else if(isset($_GET['fac_id']))
								{
								?>
                                
                               
								
                                <table class="table table-hover table-responsive table-striped text-md">
									
                                    <th style="width:5%;">#</th>
                                    <th style="width:15%;">Section</th>
                                    <th style="width:40%;">Subject</th>
									<th style="width:10%%;">Locked</th>
									<th style="width:10%;">Checked</th>
                                    <th style="">Operation</th>
                                    
                                    <?php
									
									$getmysubj = SUBJSCHED::getByfaculty($_GET['sem_ID'], $_GET['fac_id']);
									#print_r($getmysubj);
									$totalcount = count($getmysubj);
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                   // $viewSems = SEMSECTION::getAll(array("semester_section_ID" => "DESC"));
									
									
                                     if(count($getmysubj)>=50) 
                                     {
                                         $pagination2 = new Pagination($getmysubj, 50, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($getmysubj, 50, NULL);
                                     }

                                     $getmysubj = $pagination2->get_array();
									 $x = 1;

                                     if($getmysubj) {
                                        foreach($getmysubj as $key => $value){
                                    ?>
                                        <tr>
                                            <td>
											<?php 
												if(isset($_GET['p']))
												{
												$p = $_GET['p'];
												
												$num = 	$x+($p*10)-10;
												echo $num;
												$x++;
												
												}else{
													echo $x++;
												}
											 ?>
                                            <td><?php $c=COURSE::getbyID($value['course_ID']); 
												echo $c['course_INIT']."-";
                                              echo $value['semester_section_NAME']; ?>
                                            
                                          
                                            <td><?php $s = SUBJECTS::getID($value['subject_ID']);
												echo $s['subject_CODE']."/".$s['subject_DESCRIPTION'];
												 ?> 
												 </td>
                                            <td ><?php switch($value['is_lock'])
											{
												case 0: echo "Unlocked"; break;
												case 1: echo "Locked"; break;	
											} ?></td>
                                            <td ><?php switch($value['is_check'])
											{
												case 0: echo "Unchecked"; break;
											 	case 1: echo "Checked"; break;
											}
											?>
                                            <td>
											
													<a href="?action=view&sem_ID=<?= $_GET['sem_ID']; ?>&fac_id=<?= $_GET['fac_id']; ?>&subject_sched_id=<?= $value['subject_sched_ID']; ?>" class='btn btn-warning btn-xs'>View</a>
													<!-- <a href="subject_sched.php?action=edit&id=<?php echo $value['subject_sched_ID']; ?>" class='btn btn-warning btn-xs' target="_blank">Edit</a>
													<a onclick="return popitup('gs.php?id=<?php echo $value['subject_sched_ID']; ?>')" href="#" class='btn btn-success btn-xs'>GS</a> -->
                                            </td>
                                  </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
                                Total Records: <?=  $totalcount; ?>
                                        <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view&sem_ID='.$_GET['sem_ID'].'&fac_id='.$_GET['fac_id'].'') ?>
                                        </center>
                                        
                                  <?php }else{ ?>  
                                  
                                        
                                        
                                        
                                        
                                  <table class="table table-hover table-responsive table-striped text-md">
									
                                    <th style="width:5%;">#</th>
                                    <th style="width:25%;">Faculty</th>
									<th style="width:20%;"><div align="center">No. of Subjects</div></th>
									<th style="width:20%;"><div align="center">Checked Subjects</div></th>
                                    <th style=""><div align="center">Operation</div></th>
                                     <?php     
                                  	$faculty = SEM::allfacultypersem($_GET['sem_ID']);      
									#print_r($faculty);
                                    ?>    
                                    
                                    
                                <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
									 
									 
									 
                                 
									
									
                                     if(count($faculty)>20) 
                                     {
                                         $pagination2 = new Pagination($faculty, 20, Request::get("p"));
						
                                     }else{
                                         $pagination2 = new Pagination($faculty, 20, NULL);
							
                                     }
			
                                     $faculty = $pagination2->get_array();
									 $x = 1;
									

                                     if($faculty) {
                                        foreach($faculty as $key => $value){
                                    ?>
                                        <tr>
                                            <td><?php echo $x++; ?>.
                                            <td><?php
											$f = INSTRUCTORS::getSingle1($value['instructor_ID']); 
											echo $f['instructor_NAME']; 
											?>
                                            <td><div align="center"><?php echo SEM::numofsubjpersemperfac($_GET['sem_ID'],$value['instructor_ID']); ?>
                                            </div>
                                            <td><div align="center"><?php echo SEM::numofchksubjpersemperfac($_GET['sem_ID'],$value['instructor_ID']); ?>
                                            </div>
                                            <td><div align="center"><a href="?action=view&sem_ID=<?= $_GET['sem_ID']; ?>&fac_id=<?= $value['instructor_ID']; ?>" class='btn btn-warning btn-xs'>View Subjects</a></div></td>
                                  </tr>
                                    
                                        <?php
                                         } 
                                        }else{
                                    ?>
                                        <tr>
                                          <td colspan="5" align="center">No Faculty</td>
                                        </tr>
                                        <?php } ?>
								</table> 
          <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view&sem_ID='.$_GET['sem_ID']) ?>
                                        </center>
                                
                                        
                                  <?php } ?>      
                                        
                                        
						      </div>
                            </div>
						</div>
                            
					</div>
                </div>
                  <?php  break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
					default: href("?action=view");
				
				
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                	
				    
                 </div>

				    
				</div>
                
				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
            

			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
</html>
