<?php
class NAV{

	
	
 	public static function SIS(){
		?><a href="#" data-toggle="modal" data-target="#modal_student_information">
			<div class="tile col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-file-text "></span></tile>
				<br>Cadet Information
			</div>
		</a>
		<!-- Modal SIS -->
		<div id="modal_student_information" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-file-text"></i> Cadet Information</h4>
			  </div>
			  <div class="modal-body">
					<a href="<?php echo HOME; ?>old/student_information.php?action=add"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-plus-square pull-left"></i> Add Cadet</button></a>
					<a href="<?php echo HOME; ?>old/student_information.php?action=view"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-file-text pull-left"></i> View Cadet</button></a>
					<a href="<?php echo HOME; ?>old/curricullum_enrolled.php?action=add"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-outdent pull-left"></i> Cadet Course Selection</button></a>
					<a href="<?php echo HOME; ?>old/curricullum_enrolled.php?action=view"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-list-ul pull-left"></i> Curriculum Enrolled</button></a>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>	
		<!-- Modal SIS -->
		
		<?php
	}	
	
	
	
 	public static function ACCTG(){
		?><a href="#" data-toggle="modal" data-target="#modal_accounting">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-bank "></span></tile>
				<br>Accounting
			</div> 
		</a>
		<!-- Modal Accounting -->
		<div id="modal_accounting" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-bank"></i> Accounting</h4>
			  </div>
			  <div class="modal-body"><a href="<?php echo HOME; ?>modules/accounting.php?action=view"><button  class="btn btn-primary btn-lg btn-block "><i class="fa fa-search pull-left"></i> Transact</button></a>
					
					<a href="<?php echo HOME; ?>modules/promi_list.php?action=reports"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-file-text-o pull-left"></i> Promissory List <br><b style="font-size:12px;">(<?php
									$od_promi = ACCT::getExpiredPromiPerSem();
											$c = count($od_promi);
											echo $c;
											?> Needs action)</b></button></a>
						
					<a href="<?php echo HOME; ?>modules/particulars.php?action=view"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-gears pull-left" ></i> Particulars Management</button></a>
					
					<a href="<?php echo HOME; ?>modules/accounting_expense.php?action=view"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-shopping-cart pull-left"></i> Expense Management</button></a>
					
					<a href="<?php echo HOME; ?>modules/accounting_deposits.php?action=view"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-money pull-left"></i> Deposit Management</button></a>
					
					<a href="<?php echo HOME; ?>modules/acct_transaction_logs.php?action=view"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-file-text-o pull-left"></i> Transaction Logs</button></a>
					
					<a href="<?php echo HOME; ?>modules/acct_transaction_logs.php?action=reports">
						<button  class="btn btn-primary btn-lg btn-block">
							<i class="fa fa-file-text-o pull-left"></i> Transaction Reports
						</button>
					</a>
					
					
					
			  </div>

			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>
		
		
		<?php
	}
 	public static function SUBJ_MAN(){
		?><a href="<?php echo HOME; ?>modules/subjects.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Subject Management
			</div> 
		</a><?php
	}
 	public static function ROOM_MAN(){
		?><a href="<?php echo HOME; ?>modules/room.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Room Management
			</div> 
		</a><?php
	}
 	public static function ASSESSMENTLIST(){
		?><a href="<?php echo HOME; ?>old/report_assesment_list.php?action=choose">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-list"></span></tile>
				<br>Assessment List
			</div> 
		</a><?php
	}
 	public static function SEM_MAN(){
		?><a href="<?php echo HOME; ?>modules/semester.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Semester Management
			</div> 
		</a><?php
	}
 	public static function SEC_MAN(){
		?><a href="<?php echo HOME; ?>old/semester_section.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Section Management
			</div> 
		</a><?php
	}
 	public static function INS_MAN(){
		?><a href="<?php echo HOME; ?>old/instructors.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Instructor Management
			</div> 
		</a><?php
	}
 	public static function COURSE_MAN(){
		?><a href="<?php echo HOME; ?>old/course.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-gear"></span></tile>
				<br>Course Management
			</div> 
		</a><?php
	}
 	public static function CURR_MAN(){
		?><a href="#" data-toggle="modal" data-target="#modal_curriculum">
			<div class="tile col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-gear "></span></tile>
				<br>Curriculum Management
			</div>
		</a>
		<!-- Modal Curriculum Management-->
		<div id="modal_curriculum" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-gear"></i> Curriculum Management</h4>
			  </div>
			  <div class="modal-body">
					<a href="<?php echo HOME; ?>old/curricullum.php?action=view"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-list pull-left"></i> Curriculum List</button></a>
					
					<a href="<?php echo HOME; ?>old/curricullum_subjects.php?action=view"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-book pull-left"></i> Curriculum Subjects</button></a>
					
					<a href="<?php echo HOME; ?>old/curricullum_enrolled.php?action=view"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-book pull-left"></i> Enrolled Curriculum</button></a>
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>	
		
		<?php
	}
 	public static function GRADE_MAN(){
		?><a href="#" data-toggle="modal" data-target="#modal_grade">
			<div class="tile modal_grade  col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-tasks "></span></tile>
				<br>Grade Management
			</div>
        </a>
		<!-- Modal Grade Management -->
		<div id="modal_grade" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> Grade Management</h4>
			  </div>
			  <div class="modal-body">
					<a href="<?php echo HOME; ?>old/grade_encoding_admin.php?action=search"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-sort-numeric-asc pull-left"></i> Curriculum Checklist</button></a>
					
					<a href="<?php echo HOME; ?>old/grade_inquiry.php?action=search"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-print pull-left"></i> Print Grades</button></a>
					
					<a href="<?php echo HOME; ?>old/grade_checking.php?action=view&sem_ID="><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-check pull-left"></i> Grade Verification</button></a>
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>	
		<!-- Modal Grade Management -->
		<?php
	}
 	public static function ENROLMENT(){
		?><a href="#" data-toggle="modal" data-target="#modal_enrolment">
			<div class="tile modal_grade  col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-archive "></span></tile>
				<br>Enrolment
			</div>
        </a>
		<!-- Modal Enrolment Management -->
		<div id="modal_enrolment" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-archive"></i> Enrolment Management</h4>
			  </div>
			  <div class="modal-body">
					<a href="<?php echo HOME; ?>old/sectioning.php?action=search"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-outdent pull-left"></i> Sectioning</button></a>
					
					<a href="<?php echo HOME; ?>old/enrolment_list.php?action=choose"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-list pull-left"></i> Enrolment List</button></a>
					
					<a href="<?php echo HOME; ?>old/report_semester_per_subject.php?action=choose"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-print pull-left"></i> Print Per Subject Reports</button></a>
				
					<a href="<?php echo HOME; ?>old/fin_enrolment2.php?action=search"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-print pull-left"></i> View Cadet Schedule <br></button></a>
				
					<!-- <a href="<?php echo HOME; ?>old/fin_enrolment.php?action=search"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-print pull-left"></i> View Cadet Schedule <br>14-15 2nd Tri (Old)</button></a> -->
					
					<a href="<?php echo HOME; ?>old/print_deficiencies.php?action=search"><button class="btn btn-primary btn-lg btn-block"><i class="fa fa-print pull-left"></i> Print Deficiencies Reports</button></a>
				
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>

		  </div>
		</div>	
		
		<?php
	}
 	public static function CHANGE_PASS(){
		$force_login = false;
		#print_r($user);
		/*
		if($user['account_CHANGEPASS'] == 1){
			$force_login = true;
		}else{
			$force_login = false;
		}
		*/
		
		if($force_login){
			?>
			<script type="text/javascript">
				$(window).load(function(){
					$('#modal_password').modal({backdrop: 'static', keyboard: false});
				});
			</script>
			<?php
		}
		
		
		?><a href="#" data-toggle="modal" data-target="#modal_password" <?php if($force_login){ echo 'data-backdrop="static" data-keyboard="false"'; } ?>  >
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-unlock-alt "></span></tile>
				<br>Update Password
			</div>
		</a>
		<!-- Modal Change Password -->
		<div id="modal_password" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<?php if(!$force_login){ ?><button type="button" class="close" data-dismiss="modal">&times;</button><?php } ?>
				<h4 class="modal-title"><i class="fa fa-key"></i> Change Password</h4>
			  </div>
				<div class="modal-body">
					<form action="javascript:void(0);">
						 <div id="error"></div>
						 
						 <?php if($force_login) { SESSION::DisplayCustomMsg('warning','Required to change password!'); } ?> 
						 
						 <div class="form-group">
							<b>Enter Your Current Password</b>
							<input type="password" id="cpassword" name="cpassword" class="form-control" autofocus="autofocus" required="required">
						 </div>
						 <div class="form-group">
							<b>Enter Your New Password</b>
							<input type="password" id="npassword" name="npassword" class="form-control" autofocus="autofocus" required="required">
						 </div>
						 <div class="form-group">
							<b>Retype Your New Password</b>
							<input type="password" id="vpassword" name="vpassword" class="form-control" autofocus="autofocus" required="required">
						 </div>
						 <div class="form-group">
							<button  class="btn btn-primary btn-lg btn-block" id="pass_button_save"><i class="fa fa-file-text-o pull-left"></i> Save</button>
							<script>
														$(document).ready(function() {
														$('#pass_button_save').click(function(){
														var cpassword = $('#cpassword').val();
														var npassword = $('#npassword').val();
														var vpassword = $('#vpassword').val();
															if( (cpassword.length > 0) && (npassword.length > 0) && (vpassword.length > 0))
															{
																$.ajax({
																	type: "POST",
																	url: "<?php echo HOME; ?>old/search.php?action=cp",
																	data: 'cpassword=' + cpassword +'&npassword='+ npassword +'&vpassword=' +vpassword,
																	beforeSend : function() {
																			$('#modal_loading').modal('show');
																    }, 
																	success: function(msg){
																		//alert(msg);
																			
																			$('#modal_loading').modal('hide');
																			
																			switch(msg)
																			{
																				
																				case 'E_NR': 
																				$("#error").html("<div class=\"alert alert-dismissable alert-danger\"><h4> Error! </h4>New Password and Re-type Password didn't matched	</div>");
																				
																				break;
																				case 'I_OP':
																				
																				$("#error").html("<div class=\"alert alert-dismissable alert-danger\"><h4> Error! </h4>Incorrect Old Password</div>");
																				
																				break;
																				
																				case 'S':
																				$("#error").html("<div class=\"alert alert-dismissable alert-success\"><h4> Success! </h4>Password successfully changed!</div>");
																				
																				
																				setTimeout(function(){
																					   $('#modal_password').modal('hide');
																				   }, 1500);
																				
																				
																				break;
																				
																			}
																				
																			$('#cpassword').val("");
																			$('#npassword').val("");
																			$('#vpassword').val("");
																			$('#cpassword').focus();
																			
																	}
																}); // Ajax Call
															}
														})
														});			
														</script>
						 </div>
					</form>	 
				</div>
				

			</div>

		  </div>
		</div>


		<!-- Modal  Loading -->
		<div id="modal_loading" class="modal" role="dialog" data-keyboard="false" data-backdrop="static">
		  <div class="modal-dialog modal-sm">
			<div class="modal-content">
				<div class="modal-body">
					 <center><img src="<?= IMG; ?>/ring.gif"></center>
				</div>
			</div>
			</div>
		</div>	
		
		
		<?php
	}
 	public static function LOGOUT(){
		?><a href="<?php echo HOME; ?>old/logout.php">
			<div class="tile tile3 col-xs-6 col-sm-4" 
			onclick="window.location.assign('index.php');">
				<tile><span class="tile-icon fa fa-sign-out "></span></tile>
				<br>Logout
			</div>
		</a><?php
	}
	
	
	/* ACCTG TILES */
	public static function ACCTG_TRANSACT(){
		?><a href="<?php echo HOME; ?>modules/accounting.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-search "></span></tile>
				<br>Transact
			</div>
		</a><?php
	}
	
	public static function ACCTG_PROMISSORY(){
		?><a href="<?php echo HOME; ?>modules/promi_list.php?action=reports">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-file-text-o "></span></tile>
				<br>Promissory List
			</div>
		</a><?php
	}
	public static function ACCTG_PARTICULARS(){
		?><a href="<?php echo HOME; ?>old/particulars.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-gear"></span></tile>
				<br>Particulars Management
			</div>
		</a><?php
	}
	public static function ACCTG_EXPENSE(){
		?><a href="<?php echo HOME; ?>old/accounting_expense.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-shopping-cart"></span></tile>
				<br>Expense Management
			</div>
		</a><?php
	}
	public static function ACCTG_DEPOSIT(){
		?><a href="<?php echo HOME; ?>old/accounting_deposits.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-money"></span></tile>
				<br>Deposit Management
			</div>
		</a><?php
	}
	public static function ACCTG_TRANSACTION_LOGS(){
		?><a href="<?php echo HOME; ?>old/acct_transaction_logs.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-file-text-o"></span></tile>
				<br>Transaction Logs
			</div>
		</a><?php
	}
	public static function ACCTG_TRANSACTION_REPORTS(){
		?><a href="<?php echo HOME; ?>old/acct_transaction_logs.php?action=reports">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-file-text-o"></span></tile>
				<br>Transaction Reports
			</div>
		</a><?php
	}
	
	public static function ENROLMENT_REPORT(){
		?><a href="#" data-toggle="modal" data-target="#enrolment_report"> 
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-group"></span></tile>
				<br>Enrolment Report
			</div> 
		</a>
		
		<div id="enrolment_report" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> Enrolment Report</h4>
			  </div>
			  
				<form  action="<?php echo HOME; ?>modules/enrolment_report.php?action=view" method="GET"> 
					<input type="hidden" name="action" value="view">
				  <div class="modal-body">
				  <?php
				  $sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
				  #print_r($sem);
				  ?>
				  
					<div>
						Semester:
						<select name="sem_ID" class="form-control">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
				  </div>
				</form>
			  
			</div>
		  </div>
		</div>	
		<!-- Modal Enrolment Report Management -->
		<?php
	}
	
	
	public static function ENROLMENT_COMPARATIVE_REPORT(){
		?><a href="#" data-toggle="modal" data-target="#ENROLMENT_COMPARATIVE_REPORT"> 
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-exchange"></span></tile>
				<br>Comparative Report
			</div> 
		</a>
		
		<div id="ENROLMENT_COMPARATIVE_REPORT" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-exchange"></i> Comparative Report</h4>
			  </div>
			  
				<form  action="<?php echo HOME; ?>modules/enrolment_comparative.php?action=view" method="GET"> 
					<input type="hidden" name="action" value="view">
				  <div class="modal-body">
				  <?php
				  $sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
				  #print_r($sem);
				  ?>
				  
					<div>
						From Semester:
						<select name="sem_ID_from" class="form-control">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
					<br>
					<div>
						To Semester:
						<select name="sem_ID_to" class="form-control">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
				  </div>
				</form>
			  
			</div>
		  </div>
		</div>	
		<!-- Modal Enrolment Report Management -->
		<?php
	}
	
	public static function ACCTG_REPORT(){
		?><a href="#" data-toggle="modal" data-target="#modal_accounting_report"> 
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-file-o"></span></tile>
				<br>Accounting Report
			</div> 
		</a>
		
		
		<!-- Modal Accounting -->
		<div id="modal_accounting_report" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			<form  action="<?php echo HOME; ?>modules/accounting_report.php" method="GET"> 
			 <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-bank"></i> Accounting Report</h4>
			  </div>
			  
					<div class="modal-body">
					
					<!--
					<a href="<?php echo HOME; ?>modules/accounting_report.php?action=collection"><button  class="btn btn-primary btn-lg btn-block "><i class="fa fa-search pull-left"></i> Collection Report</button></a>
					
					<a href="<?php echo HOME; ?>modules/accounting_report.php?action=expense"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-shopping-cart pull-left"></i> Expense Report</button></a>
					
					<a href="<?php echo HOME; ?>modules/accounting_report.php?action=deposit"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-money pull-left"></i> Deposit Report</button></a>
					-->
					<div>
						Report:
						<select id="report" name="action" class="form-control">
						<option>Please Select</option>
						<option value="collection">Collection Report</option>
						<option value="expense">Expense Report</option>
						<option value="deposit">Deposit Report</option>
						</select>
					</div>	
					<br>
					<div id="semester" style="display:none">
						Semester:
						<select id="sem_ID" name="sem_ID" class="form-control">
						<?php
						$sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>	
			  </div>
			   <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
			   </div>
			</form> 
			</div>

		  </div>
		</div>
		<script>
		 $("#report").change(function () {
			var select = this.value;
			switch(select)
			{
				case 'collection': 
					$("#semester").show(); 
					$("#sem_ID").prop( "disabled", false );
					break;
				case 'expense': $("#semester").hide(); 
					$("#sem_ID").prop( "disabled", true );
					break;
				case 'deposit': $("#semester").hide(); 
					$("#sem_ID").prop( "disabled", true );
					break;
				default:  $("#semester").hide(); 
						$("#sem_ID").prop( "disabled", true );
				break;
			}
		});
		</script>
		<?php
	}

	public static function MY_SUBJECTS(){
		?>
		<a href="#" data-toggle="modal" data-target="#subject_load">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-group"></span></tile>
				<br>My Subject Load
			</div> 
		</a>
		
		<!-- Modal Grade Management -->
		<div id="subject_load" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> My Subject Load</h4>
			  </div>
			  <div class="modal-body">
					<a href="#" data-toggle="modal" data-dismiss="modal" data-target="#subject_load_sem"><button  class="btn btn-primary btn-lg btn-block"><i class="fa fa-sort-numeric-asc pull-left"></i> Encode Grades</button></a>
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>	
		<!-- Modal Grade Management -->
		<?php
	}	
	
	public static function OLD_PORTAL(){
		?><a href="<?php echo HOME; ?>old/index.php?action=view">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa  fa-home"></span></tile>
				<br>Please Access Old Portal
			</div> 
		</a>	<?php
	}	
	
	
	public static function ENCODE_GRADES(){
		?>
		<a href="#" data-toggle="modal" data-target="#subject_load">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-group"></span></tile>
				<br>Encode Grades
			</div> 
		</a>
		
		<!-- Modal Grade Management -->
		<div id="subject_load" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> Encode Grades</h4>
			  </div>
			  
				<form action="<?php echo HOME; ?>modules/faculty_grade_encoding.php" method="GET"> 
					<input type="hidden" name="action" value="view">
				  <div class="modal-body">
				  <?php
				  $sem = SEM::getallsemsencoding(array("sem_ID"=>"ASC"));
				  #print_r($sem);
				  ?>
				  
					<div>
						Semester:
						
						<select name="sem_ID">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
				  </div>
				</form>
			  
			</div>
		  </div>
		</div>	
		<!-- Modal Grade Management -->
		<?php
	}
	
	public static function STUDENT_LIST(){
		?>
		<a href="#" data-toggle="modal" data-target="#student_list">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-group"></span></tile>
				<br>Student List
			</div> 
		</a>
		
		<!-- Modal Grade Management -->
		<div id="student_list" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> Student List</h4>
			  </div>
			  
				<form action="<?php echo HOME; ?>modules/faculty_view_students.php" method="GET"> 
					<input type="hidden" name="action" value="view">
				  <div class="modal-body">
				  <?php
				  $sem = SEM::getallsemsloading(array("sem_ID"=>"DESC"));
				  #print_r($sem);
				  ?>
				  
					<div>
						Semester:
						
						<select name="sem_ID">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
				  </div>
				</form>
			  
			</div>
		  </div>
		</div>	
		<!-- Modal Grade Management -->
		<?php
	}	
	
	
	
	public static function ACCTG_AUDIT(){
		?>
		<a href="#" data-toggle="modal" data-target="#student_list">
			<div class="tile col-xs-6 col-sm-4" >
				<tile><span class="tile-icon fa fa-group"></span></tile>
				<br>Accounting Audit
			</div> 
		</a>
		
		<!-- Modal Grade Management -->
		<div id="student_list" class="modal fade" role="dialog">
		  <div class="modal-dialog modal-sm">
			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><i class="fa fa-tasks"></i> Accounting Audit</h4>
			  </div>
			  
				<form action="<?php echo HOME; ?>modules/accounting_audit.php" method="GET"> 
					<input type="hidden" name="action" value="view">
				  <div class="modal-body">
				  <?php
				  $sem = SEM::getallsems(array("sem_ID"=>"DESC"));
				  #print_r($sem);
				  ?>
				  
					<div>
						Semester:
						
						<select name="sem_ID">
						<?php
							foreach($sem as $key => $val)
							{
							?>
								<option value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							
							<?php
							}
						?>
						</select>
					</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-default">Proceed</button>
				  </div>
				</form>
			  
			</div>
		  </div>
		</div>	
		<!-- Modal Grade Management -->
		<?php
	}	
	
	public static function AUDIT_REPORT(){
		?><a href="<?php echo HOME; ?>modules/audit_report.php?action=view">
			<div class="tile tile3 col-xs-6 col-sm-4">
				<tile><span class="tile-icon fa fa-file-text-o"></span></tile>
				<br>Audit Reports
			</div>
		</a>
		<?php
	}
}
?>