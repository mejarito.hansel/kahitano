<?php

class Arr {

	public static function get($array, $key) {
		if(!$array){
			return NULL;
		} elseif(!isset($array[$key])){
			return NULL;
		} else {
			return $array[$key];
		}
	}

	public static function in_arrayi($needle, $haystack) {
        return in_array(strtolower($needle), array_map('strtolower', $haystack));
    }

	public static function to_select($array, $k, $v){
		$result = array();
		foreach($array as $value){
			$result[$value[$k]] = $value[$v]; 
		}
		return $result;
	}

	public static function trim($array) {
		foreach($array as $key => $value) {
			$array[$key] = trim($value);
		}
		return $array;
	}

}
	
?>
<?php

class Excel {

	public static function getContent($file) {
		$excel = new Spreadsheet_Excel_Reader();
		if(is_numeric($file)) {
			$file = FILE::getID($file);
			$excel->read(FILE::getPath($file['file_LOCATION']));
		} else {
			$excel->read($file);
		}
		return $excel;
	}

}
	
?>
<?php

!defined('EX_NON_ARRAY') ? define('EX_NON_ARRAY', 101) : NULL;

?>
<?php

class Fail {

	public static function not_found () {
		SESSION::StoreMsg("Page not found!", "error");
		HTML::redirect();
	}

}

?>
<?php

class FORM{
	
	public static function open ($action = null, $attributes=array()){
		if(is_null($action)){
			$action = $_SERVER['REQUEST_URI'];
		} else {
			$action = HOME.$action;
		}
		return "<form action='{$action}' enctype='multipart/form-data' method='POST' ".HTML::attribute($attributes).">";
	}

	public static function close () {
		return "</form>";
	}

	public static function msgbox($msg){
		echo "<script> alert('{$msg}');	</script>";
	}

	public static function input($name, $value, $attributes=array()) {
		if(!Arr::get($attributes, "type")) {
			$attributes['type'] = "text";
		}
		$tag = "<input name='{$name}' value='{$value}' ".HTML::attribute($attributes)." />";
		return $tag;
	}

	public static function textarea($name, $value, $attributes=array()) {
		$tag = "<textarea name='{$name}' ".HTML::attribute($attributes).">{$value}</textarea>";
		return $tag;
	}

	public static function file ($name, $attributes=array()) {
		$tag = "<input type='file' name='{$name}' ".HTML::attribute($attributes)." />";
		return $tag;
	}

	public static function label($name, $value, $attributes=array()) {
		$tag = "<label for='{$name}'".HTML::attribute($attributes).">{$value}</label>";
		return $tag;
	}

	public static function hidden ($name, $value, $attributes=array()) {
		$tag = "<input type='hidden' name='{$name}' value='{$value}' ".HTML::attribute($attributes)."/>";
		return $tag;
	}

	public static function password($name, $value, $attributes=array()) {
		$tag = "<input type='password' name='{$name}' value='{$value}' ".HTML::attribute($attributes)." />";
		return $tag;
	}

	public static function button($label, $attributes=array()) {
		$tag = "<button ".HTML::attribute($attributes).">{$label}</button>";
		return $tag;
	}

	public static function checkbox($name, $value, $checked=false, $attributes=array()) {
		$tag = "\n<input type='checkbox' name='{$name}' value='{$value}' ".($checked ? "CHECKED" : null)." ".HTML::attribute($attributes)." />";
		return $tag;
	}

	public static function radio($name, $value, $checked = false, $attributes=array()) {
		$tag = "\n<input type='radio' name='{$name}' value='{$value}' ".($checked ? "CHECKED" : null)." ".HTML::attribute($attributes)." />";
		return $tag;
	}

	public static function select($name, $list=array(), $selected=null, $attributes=array()){
		$tag = "<select name='{$name}'".HTML::attribute($attributes).">";
		foreach($list as $key => $value){
			if(is_array($value)){
				$tag .= "\n\t<optgroup label='{$key}'>";
				foreach ($value as $k => $v) {
					$tag .= "\n\t<option value=\"{$k}\" ".($selected==$k||($selected==intval($k) && is_numeric($k)) ? "SELECTED": null).">{$v}</option>";
				}
				$tag .= "\n</optgroup>";
			} else {
				$tag .= "\n\t<option value='{$key}' ".($selected==$key||($selected==intval($key) && is_numeric($key)) ? "SELECTED": null).">{$value}</option>";
			}
		}
		$tag .= "\n</select>";
		return $tag;
	}

	public static function submit($name, $value, $attributes=array()) {
		$tag = "<input type='submit' name='{$name}' value='{$value}' ".HTML::attribute($attributes)." />";
		return $tag;
	}
	
}
?>
<?php

class HTML {

	public static function img ($uri, $attributes = array(), $external=false) {
		if($external){
			$tag = "<img src='".$uri."'".self::attribute($attributes)." />";
		} else {
			$tag = "<img src='".HOME."assets/img/".$uri."'".self::attribute($attributes)." />";
		}
		return $tag;
	}

	public static function a ($uri, $label=null, $attributes = array(), $external = FALSE) {
		if(!$label){
			$label = $uri;
		}
		$uri = self::href($uri, $external);
		$tag = "<a href='{$uri}'".self::attribute($attributes).">".$label."</a>";
		return $tag;
	}

	public static function attribute ($attributes) {
		$result = "";
		if (is_array($attributes) || is_object($attributes)){
			foreach ($attributes as $key => $attribute) {
				if(is_numeric($key)){
					$result .= " {$attribute}='{$attribute}'";
				} else {
					$temp = str_replace("'", "\"", $attribute);
					$result .= " {$key}='{$temp}'";
				}
			}
		}
		return $result;
	}

	public static function iframe($src, $attributes = array(), $external = false) {
		if(!$external)
			$src = HOME.$src;
		$tag = "<iframe src='{$src}'".self::attribute($attributes).">No support for iFrame</iframe>";
		return $tag;
	}

	public static function redirect($uri="", $external = false){
		if(!$external)
			die(header("location:".HOME.$uri));
		else
			die(header("location:".$uri));
	}

	public static function href ($uri="", $external=FALSE) {
		if($external){
			return $uri;	
		}
		return HOME.$uri;
	}

	public static function render($uri) {
		if(!file_exists(ROOT.$uri)){
			Fail::not_found();
		} else {
			require(ROOT.$uri);
		}
	}

	public static function close_window(){
		echo "<script> window.close(); </script>";
	}

}

?>
<?php

class Pagination {

	public $pages, $count, $limit, $current_page, $array, $offset;

	public function __construct($array, $limit, $page = 1) {
		if(!is_array($array))
			throw new Exception("Pagination error: non-array given.", EX_NON_ARRAY);
		$this->array = $array;
		$this->count = count($this->array);
		$this->current_page = $page ? $page : 1;
		$this->limit = $limit;
		$this->pages = ceil($this->count / $limit);
		$this->offset = ($this->current_page-1) * $this->limit;
	}

	public function get_array() {
		return array_slice($this->array, $this->offset, $this->limit);
	}

	public function create_link($url = NULL, $attributes = array()) {
		echo "<ul class='pagination'>";

			if($url == NULL) 
			{
		
			$u1 = explode("?p=",$_SERVER['REQUEST_URI']);
			$u = $u1[0];
			#print_r($u);
			}else{
			$u = $url;
			}
			
			
			
			
			
			if(count($_GET) >= 1)
			{
				$p = "&p";				
			}else{
				$p = "?p";
			}

	
			//FIRST PAGE
			$attributes = "";
			echo "<li class='".($this->has_prev()?NULL:"disabled")."' title='First Page'>";
			echo HTML::a($u."$p=1", "&laquo;", $attributes, TRUE);
			echo "</li>";

			//PREVIOUS PAGE
			$attributes = "";
			echo "<li class='".($this->has_prev()?NULL:"disabled")."' title='Previous Page'>";
			
				if($this->has_prev()==FALSE)
				{
					$attributes = array("onclick"=>"return false;");
				}
			
			echo HTML::a($u."$p=".($this->current_page-1), "&lsaquo;", $attributes, TRUE);
			echo "</li>";

			// PAGES
			$attributes = "";
			$cur_page =$this->current_page;
			
				echo "<li class='".("active")."'>";
				echo HTML::a($u."$p=".$cur_page, $cur_page." of ".$this->pages, $attributes, TRUE);
				echo "</li>";
			
			
			//previous_page
			/*
			for($i=($cur_page-5);$i<=$cur_page; $i++)
			{	
				if($i > 0){
				echo "<li class='".($i==$this->current_page?"active":NULL)."'>";
				echo HTML::a($u."$p=".$i, $i, $attributes, TRUE);
				echo "</li>";
				}
			}
			*/
			//next_page
			#for($i=($cur_page+1);$i<=($cur_page+4); $i++)
			#{	
				#if($i > 0 and $i <= $this->pages){
				#echo "<li class='".($i==$this->current_page?"active":NULL)."'>";
				#echo HTML::a($u."$p=".$i, $i, $attributes, TRUE);
				#echo "</li>";
				#}
			#}
			
			
			/* ORIGINAL
			for($i=1; $i<=$this->pages; $i++){
			echo "<li class='".($i==$this->current_page?"active":NULL)."'>";
			echo HTML::a($u."$p=".$i, $i, $attributes, TRUE);
			echo "</li>";
			}
			*/
			
			//display last number
			/*
			if($this->current_page < $this->pages)
			{
				echo "<li><a>...</a></li>";
				echo "<li class='".($i==$this->current_page?"active":NULL)."'>";
				echo HTML::a($u."$p=".$this->pages,$this->pages, $attributes, TRUE);
				echo "</li>";
			}
				*/
			
			#print_r($this->pages);	
			
			

			//NEXT PAGE
			$attributes = "";
			if($this->has_next()==FALSE)
				{
					$attributes = array("onclick"=>"return false;");
				}
			echo "<li class='".($this->has_next()?NULL:"disabled")."' title='Next Page'>";
			echo HTML::a($u."$p=".($this->current_page+1), "&rsaquo;", $attributes, TRUE);
			echo "</li>";

			
			
			
			
			
			//LAST PAGE
			echo "<li class='".($this->has_next()?NULL:"disabled")."' title='Last Page'>";
			echo HTML::a($u."$p=".($this->pages), "&raquo;", $attributes, TRUE);
			echo "</li>";

		echo "</ul>";
	}

	public function has_next() {
		return $this->current_page < $this->pages;
	}

	public function has_prev() {
		return $this->current_page > 1;
	}

}






?>
<?php

class Request {

	public static function get_or_fail($key) {
		if(!isset($_GET[$key])) {
			Fail::not_found();
			return false;
		} else {
			return $_GET[$key];
		}
	}

	public static function post_or_fail($key) {
		if(!isset($_POST[$key])) {
			Fail::not_found();
			return false;
		} else {
			return $_POST[$key];
		}
	}

	public static function get ($key = NULL) {
		if(is_null($key)) {
			return $_GET;
		} elseif(!isset($_GET[$key])) {
			return null;
		} else {
			return $_GET[$key];
		}
	}

	public static function post ($key = NULL) {
		if(is_null($key)) {
			return $_POST;
		} elseif(!isset($_POST[$key])) {
			return null;
		} else {
			return $_POST[$key];
		}
	}

	public static function file ($key = NULL) {
		if(!FILE::checkUpload($key)) {
			return null;
		} else {
			return $_FILE[$key];
		}
	}

}

?>
<?php

class Time {

	public static function countHours($time) {
		return date('H', strtotime($time));
	}

	public static function countMinutes($time) {
		return date('i', strtotime($time));
	}

	public static function countSeconds($time) {
		return date('s', strtotime($time));
	}


	public static function to_time ($time) {
		return date(' h:i:s A', strtotime($time));
	}

	public static function to_date ($time) {
		return date('M d, Y', strtotime($time));
	}

	public static function to_datetime ($time) {
		return date('M d, Y h:i A', strtotime($time));
	}

	public static function now() {
		return date("Y-m-d H:i:s");
	}

	public static function today() {
		return date("Y-m-d");
	}

	public static function format($time, $format) {
		return date($format, strtotime($time));
	}

}

?>