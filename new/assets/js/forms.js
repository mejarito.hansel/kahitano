//Preview File
$(function(){
	$('.datetime-picker').datetimepicker({
		format:'Y-m-d H:i:00'
	});
	$('.time-picker').datetimepicker({
		datepicker:false,
		format:'H:i:s',
		step: 5
	});
	$('.date-picker').datetimepicker({
		timepicker:false,
		format:'Y-m-d',
		formatDate:'Y/m/d'
	});
});

$(document).ready(function(){
	$(".dialog-panel-close").click(function(){
		$(".dialog").fadeOut();
		$(".dialog-shadow").fadeOut();
	});

	//File Upload
	$("input.upload").change(function(){
		var filename = $(this).val();
		if(filename.length > 0) {
			filename = filename.split("\\").pop();
			$(".file-upload-text").html(filename);
		} else {
			$(".file-upload-text").html("Drag a file or click here");
		}
	}); 

});

function preview(url) {
	$(".dialog").fadeIn();
	$(".dialog-shadow").fadeIn();
	ajax(".dialog-panel .dialog-panel-content", url, null, "get", true);
	return false;
}
