<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
			
				#var_dump(Request::post());		
	           SEMSECTION::addsemsec(Request::post());
	
			
			break;
			case "edit": 
				#print_r(Request::post());
				SEMSECTION::updatesemsec($_GET['id'],Request::post());
						#case "update_blog":  BLOG::update(Request::post("blog_info")); break;
			
			#case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
			break;
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ROOM::delete($_GET['id']); break;
		
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>SEMESTER SECTION MANAGEMENT | CSJP-II-AS</title>
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end of alert messages -->
			<!-- start row container -->
			<!-- 
            <div class="row">
				<div class="col-lg-12">
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
            </div>	
			</div>
            -->
			<!-- end row container -->
            <!-- row container -->
			<div class="row">
				<!-- left nav -->   
          
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
               
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Semester Section</h3> 
                    </div>
                    <div class="panel-body">
                    
                    <?php
					$sem = SEM::getallsems(array("sem_ID"=>"DESC"));
					#print_r($sem);
					
					$course = COURSE::getallcourse(array("course_NAME"=>"ASC"));
					?>
                    
                    
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Semester Section Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                              <div class="col-sm-6">
                                  <label for='si_FNAME' class=''>Semester: </label>
                                  <select class='form-control' name="sem_ID">
                                  	<?php foreach($sem as $key => $val){ ?>
                                    <option value="<?php echo $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
                                    <?php } ?>
                                  </select>
                              </div>
                            </div>
                            
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Course:</label>
                                    <select class='form-control' name="course_ID">
										<?php foreach($course as $key => $val){ ?>
                                        <option value="<?php echo $val['course_ID']; ?>"><?php echo $val['course_NAME']; ?></option>
                                        <?php } ?>    
                                    </select>
                               </div>
                             </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Section Name:</label>
                                    <input name='semester_section_NAME' placeholder="Section Name" class='form-control' type='text' required="required"/>
                               </div>
                            </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''></label>
                               </div>
                            </div>                            
                            
                            
                             <div class="form-group"></div>                            
                            
                            
                            
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Add Semester Section</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":

          			$geThis = SEMSECTION::geThis($_GET['id']);
					#print_r($geThis);
					?>
                    <?php
					$sem = SEM::getallsems(array("sem_ID"=>"DESC"));
					#print_r($sem);
					
					$course = COURSE::getallcourse(array("course_NAME"=>"ASC"));
					?>
                                        
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Update Semester Section</h3> 
                    </div>
                    <div class="panel-body">
                       <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Semester Section Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                              <div class="col-sm-6">
                                  <label for='si_FNAME' class=''>Semester: </label>
                                  <select class='form-control' name="sem_ID">
                                  	<?php foreach($sem as $key => $val){ ?>
                                    <option value="<?php echo $val['sem_ID']; ?>"
                                    
                                    <?php
									  if($val['sem_ID'] == $geThis['sem_ID']){
										echo "selected=selected";
									  }
									?>
                                    
                                    ><?php echo $val['sem_NAME']; ?></option>
                                    <?php } ?>
                                  </select>
                              </div>
                            </div>
                            
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Course:</label>
                                    <select class='form-control' name="course_ID">
										<?php foreach($course as $key => $val){ ?>
                                        <option value="<?php echo $val['course_ID']; ?>" 
                                        
                                          <?php
											  if($geThis['course_ID'] == $val['course_ID']){
												echo "selected=selected";
											  }
											?>
                                        
                                        ><?php echo $val['course_NAME']; ?></option>
                                        <?php } ?>    
                                    </select>
                               </div>
                             </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''>Section Name: </label>
                                    <input name='semester_section_NAME' value="<?php echo $geThis['semester_section_NAME']; ?>" placeholder="Section Name" class='form-control' type='text' required="required"/>
                               </div>
                            </div>
                            
                             <div class="form-group">
                               <div class="col-sm-6">
                                    <label for='si_FNAME' class=''></label>
                               </div>
                            </div>                            
                            
                            
                             <div class="form-group"></div>                            
                            
                            
                            
                            
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Update Semester Section</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
						include "searching.php";						
						?>
							
                    <?php break; 
                        case "view":?>
                        <?php
								if(isset($_GET['id']))
								{
								$rID = $_GET['id'];
                                    //msgbox($studID);
                                    $view1room = ROOM::view1room($rID);
                                    if(count($view1room)>=1){
										foreach($view1room as $key => $value){
											$room_ID = $value['room_ID'];
											$room_NAME = $value['room_NAME'];
											
						?>
						<div class="panel panel-default">
							<div class="panel-heading"  style="margin-top: 5px; margin-bottom: 5px">
                                <div class="col-md-3">
								<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
									<i class="fa fa-user"></i>		
								</h3>
                                </div>
                                <div class="col-md-53">
								<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
								</h3>
                                </div>
                            </div>
							
							<div class="panel-body">
								<table border='0' class="table table-responsive text-md">
									<tr>
										<td colspan='4'><strong>View Rooms</strong>
									<tr>
										<td>&nbsp;
										<td align='center'><?php echo $room_ID;?>
									<tr>
										<td>&nbsp;
										<td align='center'><strong><i>Room ID</i></strong>
<tr>
										<td>&nbsp;
										<td align='center'><?php echo $room_Name;?>
									<tr>
										<td>&nbsp;
										<td align='center'><strong><i>Room Name</i></strong>

						<?php } ?>
								</table>
								<!--
								<tr>
										<td colspan='4' align='right'>
								-->
									
							</div>
							
						</div>
							<div align='right'>
								<input type='button' class='btn btn-warning' value='Edit'>
								<!--<input type='button' onClick="location.href='../../isjb/student_information.php?action=view'" class='btn btn-success' value='Back'>-->
							</div>
					<?php
									}else{ echo 'Error: 404 Not found.'; }
									
									
									
                                }else{
					?>
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table>
                                <tr>
                                <td width="50%">
                                    <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                        <i class="fa fa-folder-o"></i> View Semester Section</h3>
                                </td>
                                <script src="jquery.js" type="text/javascript"></script>
                                    <script> 
                                    $(document).ready(function(){
                                      $("#flip").click(function(){
                                        $("#panel").slideToggle("fast");
                                      });
                                    });
                                    </script>
                                <td >
                                    
                                </td>
                                <script>
                                    function term(value){
                                           
                                            $.post("search2.php",{search_term:value}, function(data){
                                                $("#display_result").html(data).hide().fadeIn();   
                                                $("#display_hide").hide();
                                                if(value==""){
                                                 $("#display_hide").show();
                                                }
                                            });
                                            
                                        }
                                    </script>
                                <td width="5%">
                                <div class="col-md-1">   
                                <button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('?action=add','_self');"><i class='fa fa-plus'></i></button>
                                </div>
                                </td>
                                </tr>
                                </table>
                            </div>
							
							<div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
								<table class="table table-hover table-responsive table-striped text-md">
									<th style="width:10%;">ID #</th>
                                    <th style="width:25%;">Semester</th>
                                    <th style="width:20%;">Course</th>
									<th style="width:20%;">Semester Section</th>
									<th style="">Operation</th>
                                    
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
									 
									 
									 
                                    $viewSems = SEMSECTION::getAll(array("semester_section_ID" => "DESC"));
									
									
                                     if(count($viewSems)>=10) 
                                     {
                                         $pagination2 = new Pagination($viewSems, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($viewSems, 10, NULL);
                                     }

                                     $viewSems = $pagination2->get_array();

                                     if($viewSems) {
                                        foreach($viewSems as $key => $value){
                                    ?>
                                        <tr>
                                            <td><?php echo $value['semester_section_ID']; ?>
                                            
                                            <td>
                                            <?php 
											$s = SEM::getSingleSem($value['sem_ID']);
											echo $s['sem_NAME']; ?>
                                          </td>
                                            <td>
                                            <?php 
											$c = COURSE::getbyID($value['course_ID']);
											echo $c['course_INIT'];
                                            ?>
                                          </td>
                                            <td><?php echo $value['semester_section_NAME']; ?>
                                            <td><a href="?action=edit&id=<?php echo $value['semester_section_ID']; ?>" class='btn btn-warning btn-xs'>Edit</a></td>
                                  </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
                                        <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                                        </center>
							     </div>
                            </div>
						</div>
                            
					</div>
                </div>
                  <?php } break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                	
				    
                 </div>

				    
				</div>
                
				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
            

			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
</html>
