<?php 
//INITIALIZE INCLUDES
    include('init.php');
#SESSION::CheckLogin();

    //QUERIES
    if(Request::post()){
        switch(Request::get("action")) {
            case "add": 
			
			#var_dump(Request::post());
			
            USERS::addstudent(Request::post());
            
			
			#var_dump(Request::post());
            
            break;
            case "edit": USERS::update_student($_GET['id'],Request::post()); break;
            #case "update_blog":  BLOG::update(Request::post("blog_info")); break;
            
            #case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
            
            default: Fail::not_found();
        }
    }
    
    if(Request::get()){
        switch(Request::get("action")) {
            case "delete": ANNOUNCEMENT::delete($_GET['id']); break;
        
        }
    }       
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
      <title>HOME | SJB CAINTA SIMS</title>
    </head>

    <body>

        <!-- top nav -->
        <?php include(LAYOUTS . 'top_nav.php'); ?>
        <!-- end nav -->

        <div class="container">
            
            <!-- banner -->
            <?php include(LAYOUTS . "banner.php"); ?>
            <!-- end banner -->
            
          
            
            
            <!-- alert messages -->
            <?php SESSION::DisplayMsg(); ?>
            <!-- end of alert messages -->
            
            <!-- start row container -->
            <!-- 
            <div class="row">
                
                <div class="col-lg-12">

                
                    <ul class="breadcrumb">
                        <li class="active">Home</li>
                    </ul>
                

                </div>
                
            </div>
            -->
            <!-- end row container -->
            
   
             
            <!-- row container -->
            <div class="row">
			<!-- left nav -->

      	
                <div class="col-md-3">
					<?php include(PAGES."navigation.php");?>
                </div>
				
				<!-- end left nav -->
                
                <!-- BODY -->
                
                
                <div class="col-md-9">
                <?php if (SESSION::isLoggedIn()) { ?>
                    <?php
                    switch($_GET['action']){
                         case "view":
                    ?>
                       
                    
                    
							<div class="panel panel-default">
								<div class="panel-heading">
									<table>
                                		<tr>
                                			<td width="50%">
												<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> View Transaction Logs</h3>											</td>

										 	<script>
                                    				function term(value){
                                            			$.post("search.php",{search_term:value}, function(data){
                                                		$("#display_result").html(data).hide().fadeIn();   
                                                		$("#display_hide").hide();
                                                			if(value==""){
                                                 				$("#display_hide").show();
                                                			}
                                            			});
                                        			}
                                    			</script>
                               			</tr>
                                	</table>
							  </div>

								<div class="panel-body">
									<div id="display_result"></div>
                                	<div id="display_hide">
									<table class="table table-hover table-responsive table-striped text-md">
										 <th  style="width:12%;">Student ID</th>
                                        <th  style="width:25%;">Name</th>
                                        <th style="width:5%;">Course</th>
                                        
                                        <th style="width:8%;">OR #</th>
                                        <th style="width:10%;">Amount</th>
                                        <th style="width:15%;">Date</th>
                                        <th style="width:;">Note</th>
                                        <?php
										 //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
										$viewStudents = ACCT:: getallpayments(array("payment_ID"=>"DESC"));
										 if(count($viewStudents)>=20) 
										 {
											 $pagination2 = new Pagination($viewStudents, 20, Request::get("p"));
										 }else{
											 $pagination2 = new Pagination($viewStudents, 20, NULL);
										 }

										 $viewStudents = $pagination2->get_array();

										 if($viewStudents) {

											foreach($viewStudents as $key => $value){
											
											
											$user = USERS::getSingle(array("si_ID"=>"ASC"),$value['si_ID']);
										?>
											<tr>
												<td>
													<?php echo strtoupper($user['student_ID']); ?>
												<td>
													<a href="accounting.php?action=view&id=<?= $value['si_ID']; ?>" title="View Account"><?php echo $user['si_LNAME']; ?>, <?php echo $user['si_FNAME']; ?></a>
												<td><?php echo USERS::getCourse($value['si_ID']); ?>
												
											  <td><?php echo $value['payment_OR']; ?></td>
                                              <td>
                                              <?php 
                                              echo "P "; printf("%.2f",$value['payment_AMOUNT']);
											  ?>
											  </td>
                                              <td><?php echo date("M-j-Y", strtotime($value['payment_DATE'])); ?></td>
                                             
                                             
											  
                                              <td><?php echo $value['payment_NOTE']; ?></td>
                                              											</tr>
											<?php
											 } 
											}
										?>
									</table> 

								</div>
							</div>
								<center>
									<?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
								</center>
							</div>
                        </div>
                    </div>
                  <?php  break;
						
						
						case "reports":
						?>
 
							<div class="panel panel-default">
								<div class="panel-heading">
									
                                    <form action="" method="GET">
                                    <table border="0" width="100%">
                                		<tr>
                                			<td width="" style="width:40%">
												<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> View Reports</h3>											</td>

                                                <script src="//cdn.jsdelivr.net/webshim/1.14.5/polyfiller.js"></script>
												<script>
                                                webshims.setOptions('forms-ext', {types: 'date'});
                                                webshims.polyfill('forms forms-ext');
                                                </script>
                                              
                                                
                                                
                                              <td>Start: <input id="start" type="date" value="<?php if(isset($_GET['start'])){ echo $_GET['start']; } ?>" name="start" /></td>  
                                              <td>End: <input id="end" type="date" value="<?php if(isset($_GET['end'])){ echo $_GET['end']; } ?>" name="end" /></td>
                                              <td><input type="submit" value="Go" /></td>
                                             <input type="hidden" name="action" value="reports" />   
                                             
                                             
                                             
                                             <script>
											 
											 $(document).ready(function() {                                       
												$("#start").on("input", function() {
												  $("#end").val($(this).val());
												  alert($(this).val());
												})
											 }); ​
                                             </script>
                                             
                                             
                               			</tr>
                                	</table>
                                    
                                    </form>
							  </div>

								<div class="panel-body">
									<div id="display_result"></div>
                                	<div id="display_hide">
									<table class="table table-hover table-responsive table-striped text-md">
										 <tr>
                                         <th  style="width:3%;">#</th>
                                         <th  style="width:12%;">Student ID</th>
                                           
                                           <th  style="width:25%;">Name</th>
                                        <th style="width:5%;">Course</th>
										<th style="width:8%;">Particulars</th>
                                        <th style="width:8%;">OR #</th>
                                        <th style="width:15%;">Amount</th>
                                        <th style="width:15%;">Date</th>
                                        <th style="width:;">Note</th>
                                        
                               
										<?php
										if(isset($_GET['start']) && isset($_GET['end']))
										{
										$start = date("Y-m-d H:i:s",strtotime($_GET['start']));
										#$end = date("Y-m-d H:i:s",strtotime($_GET['end']));
										$end = $_GET['end'];
										$viewStudents = ACCT:: getallpaymentsbydate(array("payment_ID"=>"DESC"),$start,$end);
										
										
										 
										}else{
										 $viewStudents = NULL;
										}
										
										 if($viewStudents) {
											$x=1;
											$total_amnt = 0;
											
											foreach($viewStudents as $key => $value){
											
											
											$user = USERS::getSingle(array("si_ID"=>"ASC"),$value['si_ID']);
												?>
											<tr>
                                            	<td><?= $x++; ?></td>
												<td>
													<?php echo strtoupper($user['student_ID']); ?>
												                                               
												<td>
													<a href="accounting.php?action=view&id=<?= $value['si_ID']; ?>" title="View Account"><?php echo $user['si_LNAME']; ?>, <?php echo $user['si_FNAME']; ?></a>
												
												<td><?php echo USERS::getCourse($value['si_ID']); ?>
												<td>  <?php
												$p = PARTICULARS::getSingleParticular($value['particular_ID']);
												
												echo $p['particular_NAME'];
												
												?>
											  <td><?php echo $value['payment_OR']; ?></td>
                                              <td>
                                              <?php 
                                              echo "P ";  echo number_format($value['payment_AMOUNT'],2, '.', ',');
											  
											  $total_amnt += $value['payment_AMOUNT'];
											  
											  ?>											  </td>
                                              <!--<td><?php echo date("M-j-Y", strtotime($value['payment_DATE'])); ?></td>-->
											  <td><?php echo date("m/d/Y", strtotime($value['payment_DATE'])); ?></td>
											  
											  
                                              <td><?php echo $value['payment_NOTE']; ?></td>
                                              											</tr>
											<?php
											 } 
										
										?>
                                        <tr style="border-top-color:#000000; border-top-style:solid; border-top-width:medium">
                                        	<td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td> <b>Total</b></td>
                                            <td>P 
                                            <?php echo number_format($total_amnt,2, '.', ',');
                                            ?></td>
                                            <td></td>
                                            <td></td>
                                         </tr> 
                                         <?php } ?> 
									</table> 

								</div>
							</div>
								<center>
									
								</center>
							</div>
                        </div>
                    </div>

					<?php
					
					break;
					default: href("?action=view");
					
					
					}}
				  ?>
         
            
            <!-- end row container -->
            
            

            <!-- footer-->      
        
            <?php include (LAYOUTS . "footer.php"); ?>
            <!-- end footer -->
		</div>
        <!-- end container -->

    </body>
</html>
