<?php
include('init.php');
$siiid = clean($_GET['id']);
$getsingle = USERS::getSingle(array("si_ID"=>"DESC"),$siiid  );
$getsingle1 = USERS::getSingleSAI(array("si_ID"=>"DESC"), $siiid );

#print_r($getsingle);
#print_r($getsingle1);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/bootstrap.min.css">
  	<link rel="stylesheet" href="pages\htmlfolder\assets/css/jquery.dataTables.min.css">
  	<!-- <link href="ams\old\pages\htmlfolder\assets/css/style.css" rel="stylesheet"> -->
  	<link href="pages\htmlfolder\assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  	<link href="pages\htmlfolder\assets/css/style.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  	<title>AMS</title>

</head>
<body onload="window.print();">
	<div class="container text-capitalize" style="margin-bottom: 15px; margin-top: 15px">
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<img src="pages\htmlfolder\assets/img/logo.png">
			</div>
		</div>
		<div class="row ">
			<div class="col-lg-12 col-md-12 ">
				<span class="pull-right">MMM-FM-08-02 Rev. 00</span>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<h4 class="user_info">CADET INFORMATION SHEET</h4>
			</div>
		</div>
		<div class="row ">
			<div class="col-lg-12 col-md-12">
				<div class="user_name font-weight-bold">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4">
							<span class=""><?= $getsingle['si_LNAME']; ?></span>
						</div>	
						<div class="col-lg-4 col-md-4 col-sm-4">
							<span class=""><?= $getsingle['si_FNAME']; ?></span> 
						</div>	
						<div class="col-lg-4 col-md-4 col-sm-4">
							<span class=""><?= $getsingle['si_MNAME']; ?></span>
						</div>
					</div>
				</div>
				<div class="user_fullname font-italic">
					<div class="row">
						<div class="col-lg-4 col-md-4 col-sm-4">
							<p class="">Last Name</p>
						</div>	
						<div class="col-lg-4 col-md-4 col-sm-4">
							<p class="">First Name</p> 
						</div>	
						<div class="col-lg-4 col-md-4 col-sm-4">
							<p class="">Middle Name</p>
						</div>	
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12 ">
				<div class="topBorder">
					<span>EDUCATIONAL ATTAINMENT</span>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-2">
						<span class="">Course:</span> 
					</div>
					<div class="col-lg-10 col-md-10 col-sm-10">
						<div class="bottomBorder">
							<span class=""><?php #USERS::getCourse($siiid); ?>&nbsp;</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-2">
						<span class="">Name of School:</span> 
					</div>
					<div class="col-lg-10 col-md-10 col-sm-10">
						<div class="bottomBorder">
							<span class="">none</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12 col-md-12">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-2">
						<span class="">School Address:</span> 
					</div>
					<div class="col-lg-10 col-md-10 col-sm-10">
						<div class="bottomBorder">
							<span class="">none</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row">
			<div class="col-lg-12 col-md-12 ">
				<div class="topBorder">
					<span>PERSONAL INFORMATION</span>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Present Address:</span> 	
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10">
								<div class="bottomBorder">
									<span class=""><?= $getsingle['si_STREET']; ?>, <?= $getsingle['si_BRGY']; ?>, <?= $getsingle['si_CITY']; ?>, <?= $getsingle['si_DISTRICT']; ?>, <?= $getsingle['si_PROVINCE']; ?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<!-- <div class="col-lg-7">
								<div class="bottomBorder">
									<span class="">none</span>
								</div>
							</div> -->
							<div class="col-lg-2">
								<p>Contact No.:</p> 	
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10">
								<div class="bottomBorder">
									<span class=""><?= $getsingle['si_CONTACT']; ?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Permanent Address:</span> 	
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10">
								<div class="bottomBorder">
									<span class=""><?= $getsingle1['sai_present_addr_st']; ?>, <?= $getsingle1['sai_present_brgy']; ?>, <?= $getsingle1['sai_present_city']; ?>, <?= $getsingle1['sai_present_district']; ?>,<?= $getsingle1['sai_present_prov']; ?> </span>
								</div>
							</div>
						</div>
						<div class="row">
							<!-- <div class="col-lg-7">
								<div class="bottomBorder">
									<span class="">none</span>
								</div>
							</div> -->
							<div class="col-lg-2">
								<p>Contact No.:</p> 	
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10">
								<div class="bottomBorder">
									<span class=""><?= $getsingle1['sai_present_contact']; ?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Date of Birth:</span> 	
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="bottomBorder">
									<span class=""><?= $getsingle['si_BIRTHDATE']; ?></span>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Place of Birth:</span> 	
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="bottomBorder">
									<span class=""><?= $getsingle['si_BIRTHPLACE']; ?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Age:</span> 	
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="bottomBorder">
									<span class="">
										
										<?php
										 $date = new DateTime($getsingle['si_BIRTHDATE']);
										 $now = new DateTime();
										 $interval = $now->diff($date);
										 echo $interval->y;
										?>

									</span>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Height(cm):</span> 	
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="bottomBorder">
									<span class=""><?= $getsingle1['sai_height']; ?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Gender:</span> 	
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="bottomBorder">
									<span class=""><?= $getsingle['si_GENDER']; ?></span>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Weight(kgs):</span> 	
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="bottomBorder">
									<span class=""><?= $getsingle1['sai_weight']; ?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Religion:</span> 	
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="bottomBorder">
									<span class=""><?= $getsingle1['sai_religion']; ?></span>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Civil Status:</span> 	
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="bottomBorder">
									<span class=""><?= $getsingle1['sai_civil']; ?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Special Skills:</span> 	
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10">
								<div class="bottomBorder">
									<span class=""><?= $getsingle1['sai_special_skills']; ?></span>
								</div>
							</div>
							<!-- <div class="col-lg-12 col-md-12">
								<div class="bottomBorder">
									<span class="">none</span>
								</div>
							</div> -->
						</div>

					</div>
				</div>
			</div>
		</div><br>
		<div class="row">
			<div class="col-lg-12 col-md-12 ">
				<div class="topBorder">
					<span>CONTACT PERSON (Parent/Guardian) In Case of Emergency:</span>
				</div><br>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Complete Name:</span> 	
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10">
								<div class="bottomBorder">
									<span class=""><?= $getsingle1['sai_name_of_guardian']; ?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Relationship:</span> 	
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="bottomBorder">
									<span class=""><?= $getsingle1['sai_relationship']; ?></span>
								</div>
							</div>
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Contact No.:</span> 	
							</div>
							<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="bottomBorder">
									<span class=""><?= $getsingle1['sai_tel_cell_number']; ?></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2">
								<span>Address:</span> 	
							</div>
							<div class="col-lg-10 col-md-10 col-sm-10">
								<div class="bottomBorder">
									<span class=""><?= $getsingle1['sai_address']; ?></span>
								</div>
							</div>
							<div class="col-lg-12 col-md-12">
								<div class="bottomBorder">
									<span class="">&nbsp;</span>
								</div>
							</div>
						</div><br>
						<div class="topBorder">
						</div><br>
					</div>
				</div>
			</div>
		</div>
	</div>

    <script src="pages\htmlfolder\assets/js/jquery.min.js"></script>
    <script src="pages\htmlfolder\assets/js/jquery.dataTables.min.js"></script>
  	<script src="pages\htmlfolder\assets/js/bootstrap.min.js"></script>

    

</body>
</html>
