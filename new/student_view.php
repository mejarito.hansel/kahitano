<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="pages\htmlfolder\assets/css/jquery.dataTables.min.css">
    <!-- <link href="ams\old\pages\htmlfolder\assets/css/style.css" rel="stylesheet"> -->
    <link href="pages\htmlfolder\assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="pages\htmlfolder\assets/css/style.css" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>AMS</title>

</head>
<body>
<div class="content-wrapper">
  <div class="container" style="margin-bottom: 15px; margin-top: 15px">
      <div class="card">
        <div class="card-header">
          <h4 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
          <i class="fa fa-folder-o"></i> Student
          </h4>
        </div>
        <div class="card-body">
      <legend><i class="fa fa-user"></i> Find Cadet</legend><hr>
      <div class="row ">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-body">
              <table id="cadetSched" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr style="color: grey">
                        <th>Cadet No.</th>
                        <th>Surname</th>
                        <th>Given Name</th>
                        <th>Middle Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                  <tr>
                    <td>MT2017-001</td>
                    <td>MAPA</td>
                    <td>MARCELO VICENTE</td>
                    <td>MAGADIA</td>
                    <td><a href="">View</a></td>
                  </tr>
                  <tr>
                    <td>lorem ipsum</td>
                    <td>lorem ipsum</td>
                    <td>lorem ipsum</td>
                    <td>lorem ipsum</td>
                    <td><a href="">View</a></td>
                  </tr>
                  <tr>
                    <td>MT2017-001</td>
                    <td>MAPA</td>
                    <td>MARCELO VICENTE</td>
                    <td>MAGADIA</td>
                    <td><a href="">View</a></td>
                  </tr>
                </tbody>
              </table>   
            </div>
          </div>
        </div>
      </div>

        </div>


        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>

  </div>
</div>

    <script src="pages\htmlfolder\assets/js/jquery.min.js"></script>
    <script src="pages\htmlfolder\assets/js/jquery.dataTables.min.js"></script>
    <script src="pages\htmlfolder\assets/js/bootstrap.min.js"></script>

    <script type="text/javascript">
      $(document).ready(function() {
        $('#cadetSched').DataTable();
      } );
    </script>
    

</body>
</html>
