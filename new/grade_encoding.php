<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
            ROOM::addroom(Request::post());
			#var_dump(Request::post());
			
			break;
			case "edit": ROOM::update_room($_GET['id'],Request::post()); break;
			#case "update_blog":  BLOG::update(Request::post("blog_info")); break;
			
			#case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ROOM::delete($_GET['id']); break;
		
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>HOME | SJB CAINTA SIMS</title>
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end of alert messages -->
			<!-- start row container -->
			<!-- 
            <div class="row">
				<div class="col-lg-12">
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
            </div>	
			</div>
            -->
			<!-- end row container -->
            <!-- row container -->
			<div class="row">
				<!-- left nav -->   
            <?php
				if (!SESSION::isLoggedIn()) {
					?>
				<div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> Course Categories </h3>
                    </div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                        <li >
                            <a style="cursor:pointer" id="1-link">
                                <span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=1','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CCS (2)
                                </span>
                            </a>
                        </li>
                        <li >
                            <a style="cursor:pointer" id="2-link">
                                <span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=2','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CBA</span>
                            </a>
                        </li>
                        <script>

                                  $( "#1-link" ).click(function() {
                             $( "#1-content" ).slideToggle( "fast" );
                         });
                                  $( "#2-link" ).click(function() {
                             $( "#2-content" ).slideToggle( "fast" );
                         });

                        </script>
                        <!--
                            <li class="active"><a href="#">Computer Studies (#)</a></li>
                            <li class=""><a href="#">Business Course</a></li>
                            <li class="dropdown"><a class="dropdown-toggle" id="link" data-toggle="dropdown" href="#">Cat3 <span class="caret"></span></a>
                                <ul class="nav nav-pills nav-stacked nav-sub"  >
                                    <li><a href="#">Cat3 Sub 1 <span class="caret"></span></a>

                                    <ul class="nav nav-pills nav-stacked  nav-sub"  style="">

                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                    </ul>

                                    </li>
                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                </ul>
                            </li>

                           --> 
                        </ul>
                    </div>
                </div>
				</div>
                <?php   }else{?>
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
                <?php } ?>
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Room </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Room Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for='si_FNAME' class='col-md-6'>Room Name</label>
                                    <input name='room_NAME' pattern="([a-zA-Z0-9]| |/|\|@|#|$|%|&)+" placeholder="Room Name" class='form-control' type='text' required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Add Room</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":
                        $siiid =$_GET['id'];
                        $getsingle = ROOM::getSingle(array("room_ID"=>"DESC"),$siiid  );
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Update Room </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <legend><i class="fa fa-user"></i> Room Information</legend>
                            <!-- Name -->
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for='si_FNAME' class='col-md-6'>Room ID</label>
                                    <input name='room_ID' placeholder="Room Name" class='form-control' type='text' required value="<?= $getsingle['room_ID'];?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for='si_FNAME' class='col-md-6'>Room Name</label>
                                    <input name='room_NAME' pattern="([a-zA-Z0-9]| |/|\|@|#|$|%|&)+" placeholder="Room Name" class='form-control' type='text' required value="<?= $getsingle['room_NAME'];?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-5">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i> Update Room</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
						include "searching.php";						
						?>
							
                    <?php break; 
                        case "view":
						
						
						$ss = SUBJSCHED::getSched(clean($_GET['subject_sched_ID']));
						
						#<strong></strong>print_r($ss);
						?>
                        
						<div class="panel panel-default">
                        
                        
                        
                        
							<div class="panel-heading">
                            
                                <table>
                                <tr>
                                <td width="50%">
                                    <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                        <i class="fa fa-folder-o"></i> Curriculum Checklist  </h3>
                                </td>
                                <script src="jquery.js" type="text/javascript"></script>
                                    <script> 
                                    $(document).ready(function(){
                                      $("#flip").click(function(){
                                        $("#panel").slideToggle("fast");
                                      });
                                    });
                                    </script>
                                <td >
                                  
                                 
                                </td>
                               
                                <td width="5%">
                                
                                </td>
                                </tr>
                                </table>
                            </div>
							
							<div class="panel-body" >
                            <center>
                            
                            
                            
                          <?php $s = SUBJECTS::getID($ss['subject_ID']);
									echo $s['subject_CODE']."-".$s['subject_DESCRIPTION'];
									 ?> <br />
                                     
                                      <?php $c=COURSE::getbyID($ss['course_ID']); 
									echo $c['course_INIT']."-".$ss['semester_section_NAME'];
									?>
                                    <br />
									<?php echo $ss['day']; ?> 
                                    <br />
                                    <?php echo $ss['start']; ?> - <?php echo $ss['end']; ?> 
 
 
<br />
        	 Lock Status:	
                                 <?php
                                 switch($ss['is_lock'])
								{
								  case 1 : echo "LOCKED"; break;
								  case 0 : echo "UNLOCKED";
								}
								?> 
                                -
                                
        						Check Status: 
								<?php
								switch($ss['is_check'])
								{
								  case 1 : echo "CHECKED"; break;
								  case 0 : echo "UNCHECKED";
								}
								?> 

        						
                                 
                             
       
 
                            </center>
                            <br />
                            <br />
       
                            
                            
                            
                            
                            
                                <div id="display_hide">
								<form action="" method="get">
                                
                                <table class="table table-hover table-responsive table-striped text-md">
									<tr><th style="width:10%;">#</th>
									<th style="width:30%;">Student Name</th>
									<th style="width:20%;text-align:center" >Grade</th>
                                    <th style="widows:20%; text-align:center">Remarks</th>
                                    <th style="width:20%;text-align:center">Status</th>
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    #$viewStudents = ROOM:: getallrooms(array("room_ID"=>"ASC"));
	                                 $stud =   SUBJSCHED::getBySubjectSchedID(clean($_GET['subject_sched_ID']),$instructor_ID);
									 #var_dump($stud);
									$x=1;
                                    ?>
                                       
                                     <?php foreach($stud as $key => $val) { ?>  
                                        <tr>
                                            <td><?= $x++; ?>
                                            <td><?php $u = USERS::getSingle(array("si_ID"=>"ASC"),$val['si_ID']);
													echo $u['si_LNAME'].", ".$u['si_FNAME']." ".$u['si_MNAME'];
												?>
                                            <td>
                                            
                                            <select <?php if($ss['is_lock'] == 1){ echo "DISABLED=DISABLED"; } ?>    name="grade<?php echo $val['subject_enrolled_ID']; ?>" class="form-control">
                                            	<option value="NG">NO GRADE</option>
                                            	<option value="1.00" <?php if($val['grade'] == "1.00"){ echo " SELECTED=SELECTED "; } ?>>1.00</option>
                                                <option value="1.25" <?php if($val['grade'] == "1.25"){ echo " SELECTED=SELECTED "; } ?>>1.25</option>
                                                <option value="1.50" <?php if($val['grade'] == "1.50"){ echo " SELECTED=SELECTED "; } ?>>1.50</option>
                                                <option value="1.75" <?php if($val['grade'] == "1.75"){ echo " SELECTED=SELECTED "; } ?>>1.75</option>
                                                <option value="2.00" <?php if($val['grade'] == "2.00"){ echo " SELECTED=SELECTED "; } ?>>2.00</option>
                                                <option value="2.25" <?php if($val['grade'] == "2.25"){ echo " SELECTED=SELECTED "; } ?>>2.25</option>
                                                <option value="2.50" <?php if($val['grade'] == "2.50"){ echo " SELECTED=SELECTED "; } ?>>2.50</option>
                                                <option value="2.75" <?php if($val['grade'] == "2.75"){ echo " SELECTED=SELECTED "; } ?>>2.75</option>
                                                <option value="3.00" <?php if($val['grade'] == "3.00"){ echo " SELECTED=SELECTED "; } ?>>3.00</option>
                                                <option value="5.00" <?php if($val['grade'] == "5.00"){ echo " SELECTED=SELECTED "; } ?>>5.00</option>
                                                <option value="INC" <?php if($val['grade'] == "INC"){ echo " SELECTED=SELECTED "; } ?>>INCOMPLETE</option>
                                                <option value="DRP" <?php if($val['grade'] == "DRP"){ echo " SELECTED=SELECTED "; } ?>>DROPPED</option>
                                                
                                            </select>   
                                            <input type="hidden" name="subject_enrolled_ID<?php echo $val['subject_enrolled_ID']; ?>" value="<?php echo $val['subject_enrolled_ID']; ?>" />                                         
                                           
                                           <td align="center">
                                           
                                            <?php 
											 	switch($val['grade'])
												{
													case "1.00":
													case "1.25":
													case "1.50":
													case "1.75":
													case "2.00":
													case "2.25":
													case "2.50":
													case "2.75":
													case "3.00": echo "PASSED";
													break;
													case "5.00": echo "FAILED";
													break;
													case "INC": echo "INCOMPLETE";
													break;
													case "DRP": echo "DROPPED";
													break;
													default: echo "NO GRADE";
												}
											 ?>
                                           
                                           
                                           </td>
                                           
                                            <td><div id="result<?php echo $val['subject_enrolled_ID']; ?>" align="center">
                                            <?php 
											 	switch($val['grade'])
												{
													case "1.00":
													case "1.25":
													case "1.50":
													case "1.75":
													case "2.00":
													case "2.25":
													case "2.50":
													case "2.75":
													case "3.00": 
													case "5.00": 
													case "INC": 
													case "DRP":
													case "NG":
														echo "Grade Encoded";		
														break;											
													default: echo "<span style='color:red'> No Grade Encoded</span>";
												}
											 ?>
                                            </div></td>
                                        </tr>
                                        
                                        <?php
                                      }
                                       
                                    ?>
								</table>
                                
                                </form> 
                                
                                <script>
								$(document).ready(function(){
								
								
									<?php foreach($stud as $key => $val){ ?>
										  $('[name="grade<?php echo $val['subject_enrolled_ID']; ?>"]').each(function() {
												$(this).change(function() {
													var mydata = $('[name="grade<?php echo $val['subject_enrolled_ID']; ?>"]').val();//$(this).val();
													var inputdata = $('[name="subject_enrolled_ID<?php echo $val['subject_enrolled_ID']; ?>"]').val();
											//	alert(mydata)
											//	alert(inputdata)
													$.ajax({   
													   type: 'GET',   
													   url: 'update_ajax.php',   
													   data: {action: 'grade_encoding.php',subject_enrolled_ID:inputdata, grade:mydata},
													    success: function(data){
      													  $('#result<?php echo $val['subject_enrolled_ID']; ?>').html(data);
    }													
													});
												});
										  	});
									<?php } ?>		
								});
								</script>
                                        <center>
                                          
                                        </center>
						      </div>
                            </div>
						</div>
                            
					</div>
                </div>
                  <?php break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                	
				    
                 </div>

				    
				</div>
                
				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
            

			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
</html>
