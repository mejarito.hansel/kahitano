<?php
	include('init.php');
//SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
           # ROOM::addroom(Request::post());
			
			#var_dump(Request::post());
			SUBJSCHED::addsubjsched(Request::post());
			
			break;
			case "edit": 
            
            SUBJSCHED::updateSubjSched($_GET['id'],Request::post()); 
            #msgbox(1);
            break;
			#case "update_blog":  BLOG::update(Request::post("blog_info")); break;
			#case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ROOM::delete($_GET['id']); break;
		
		}
	}	


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <?php include(LAYOUTS . "styles.php"); ?>
        <?php include(LAYOUTS . "scripts.php"); ?>
      <title>Student Information | ACADEMIC MANAGEMENT SYSTEM</title>
      <link rel="shortcut icon" href="<?= HOME; ?>assets/images/site_logo.png">
          <script type="text/javascript">
            $(document).ready(function() {
              $('#example').DataTable({
                "scrollX": true,
                "order": [[ 0, "desc" ]]
              });
            } );
          </script>
    </head>

    <body>
  <?php


switch($_GET['action']){

case "add":	?>


<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
          <i class="fa fa-folder-o"></i>  Student Subject Schedule </h3> 
        </div>
        <div class="panel-body">
          <form action="<?php echo $_SERVER['REQUEST_URI']?>" method="post">
            <legend><i class="fa fa-user"></i>  Semester Section</legend>
              <div class="row">
                <div class="col-lg-6">
                  <label class="">Section</label>
                    <?php
                    #$sem = SEMSECTION::getAll(array("sem_ID"=>"DESC");
                    $sem = SEMSECTION::getAll3(array("sem_ID"=>"DESC","course_ID"=>"ASC","semester_section_NAME"=>"ASC"));
                    #print_r($sem);
                    ?>
                    <select name="semester_section_ID" id="semester_section_ID" class="form-control">
                        <?php foreach($sem as $key => $val){ ?>
                      <option value="<?php echo $val['semester_section_ID']; ?>"    <?php if(isset($_POST['semester_section_ID'])){ if($_POST['semester_section_ID'] == $val['semester_section_ID']){ echo "SELECTED=SELECTED"; } } ?>>
                        <?php $sem = SEMSECTION::getID($val['semester_section_ID']); ?>
                        <?php $d = SEMSECTION::getSEM($sem['sem_ID']);
                        echo $d['sem_NAME']; ?>
                      -
                        <?php $c = COURSE::getbyID($val['course_ID']);
                        echo $c['course_INIT']; ?>-<?php echo $val['semester_section_NAME']; ?>
                        </option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-lg-6">
                  <label for="Subject" class="">Subject</label>
                    <?php
                       $viewSubjects = SUBJECTS:: getAllSubjects(array("subject_CODE"=>"ASC"));
                    ?>
                    <select name="subject_ID" id="subject_ID" class="form-control">
                        <?php foreach($viewSubjects as $key => $val){ ?>
                        <option  value="<?php echo $val['subject_ID']; ?>"><?php echo $val['subject_CODE']; ?> - <?php echo $val['subject_DESCRIPTION']; ?> - <?php echo $val['LEC_UNIT']."/".$val['LAB_UNIT']; ?></option>
                        <?php } ?>    
                    </select>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-lg-3">
                  <label for="Day" class="">Day</label>
                  <select name="day" id="day" class="form-control">
                    <option value="TBD">TBD</option>
                    <option value="Monday">MON</option>
                    <option value="Tuesday">TUE</option>
                    <option value="Wednesday">WED</option>
                    <option value="Thursday">THU</option>
                    <option value="Friday">FRI</option>
                    <option value="Saturday">SAT</option>
                    <?php foreach($viewSubjects as $key => $val){ ?>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Room" class="">Room</label>
                    <?php
                     $viewStudents = ROOM:: getallrooms(array("room_NAME"=>"DESC"));
                    ?>
                    <select name="room_ID" id="room_ID" class="form-control">
                      <?php foreach($viewStudents as $key => $val){ ?>
                      <option value="<?php echo $val['room_ID']; ?>"><?php echo $val['room_NAME']; ?></option>
                      <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3">
                  <label for="Start" class="">Start</label>
                  <select name="start" id="start" class="form-control">
                    <option value="TBD">TBD</option>  
                    <option value="7:00 AM">7:00 AM</option>
                    <option value="7:30 AM">7:30 AM</option>
                    <option value="8:00 AM">8:00 AM</option>
                    <option value="8:30 AM">8:30 AM</option>
                    <option value="9:00 AM">9:00 AM</option>
                    <option value="9:30 AM">9:30 AM</option>
                    <option value="10:00 AM">10:00 AM</option>
                    <option value="10:30 AM">10:30 AM</option>
                    <option value="11:00 AM">11:00 AM</option>
                    <option value="11:30 AM">11:30 AM</option>
                    <option value="12:00 PM">12:00 PM</option>
                    <option value="12:30 PM">12:30 PM</option>
                    <option value="1:00 PM">1:00 PM</option>
                    <option value="1:30 PM">1:30 PM</option>
                    <option value="2:00 PM">2:00 PM</option>
                    <option value="2:30 PM">2:30 PM</option>
                    <option value="3:00 PM">3:00 PM</option>
                    <option value="3:30 PM">3:30 PM</option>
                    <option value="4:00 PM">4:00 PM</option>
                    <option value="4:30 PM">4:30 PM</option>
                    <option value="5:00 PM">5:00 PM</option>
                    <option value="5:30 PM">5:30 PM</option>
                    <option value="6:00 PM">6:00 PM</option>
                    <option value="6:30 PM">6:30 PM</option>
                    <option value="7:00 PM">7:00 PM</option>
                    <option value="7:30 PM">7:30 PM</option>
                    <option value="8:00 PM">8:00 PM</option>
                    <option value="8:30 PM">8:30 PM</option>
                    <option value="9:00 PM">9:00 PM</option>
                    <option value="9:30 PM">9:30 PM</option>
                    <option value="10:00 PM">10:00 PM</option>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Installment" class="">End</label>
                  <select name="end" id="end" class="form-control">
                    <option value="TBD">TBD</option>  
                    <option value="7:00 AM">7:00 AM</option>
                    <option value="7:30 AM">7:30 AM</option>
                    <option value="8:00 AM">8:00 AM</option>
                    <option value="8:30 AM">8:30 AM</option>
                    <option value="9:00 AM">9:00 AM</option>
                    <option value="9:30 AM">9:30 AM</option>
                    <option value="10:00 AM">10:00 AM</option>
                    <option value="10:30 AM">10:30 AM</option>
                    <option value="11:00 AM">11:00 AM</option>
                    <option value="11:30 AM">11:30 AM</option>
                    <option value="12:00 PM">12:00 PM</option>
                    <option value="12:30 PM">12:30 PM</option>
                    <option value="1:00 PM">1:00 PM</option>
                    <option value="1:30 PM">1:30 PM</option>
                    <option value="2:00 PM">2:00 PM</option>
                    <option value="2:30 PM">2:30 PM</option>
                    <option value="3:00 PM">3:00 PM</option>
                    <option value="3:30 PM">3:30 PM</option>
                    <option value="4:00 PM">4:00 PM</option>
                    <option value="4:30 PM">4:30 PM</option>
                    <option value="5:00 PM">5:00 PM</option>
                    <option value="5:30 PM">5:30 PM</option>
                    <option value="6:00 PM">6:00 PM</option>
                    <option value="6:30 PM">6:30 PM</option>
                    <option value="7:00 PM">7:00 PM</option>
                    <option value="7:30 PM">7:30 PM</option>
                    <option value="8:00 PM">8:00 PM</option>
                    <option value="8:30 PM">8:30 PM</option>
                    <option value="9:00 PM">9:00 PM</option>
                    <option value="9:30 PM">9:30 PM</option>
                    <option value="10:00 PM">10:00 PM</option>
                    <option value="10:30 PM">10:30 PM</option>
                    <option value="11:00 PM">11:00 PM</option>
                  </select>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-lg-3">
                  <label for="Day2" class="">Day2</label>
                  <select name="day2" id="day2" class="form-control">
                    <option value="">None</option>
                    <option value="TBD">TBD</option>
                    <option value="Monday">MON</option>
                    <option value="Tuesday">TUE</option>
                    <option value="Wednesday">WED</option>
                    <option value="Thursday">THU</option>
                    <option value="Friday">FRI</option>
                    <option value="Saturday">SAT</option>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Room2" class="">Room2</label>
                    <?php
                       $viewStudents = ROOM:: getallrooms(array("room_NAME"=>"ASC"));
                    ?>
                    <select name="room_ID2" id="room_ID2" class="form-control">
                      <option value="">None</option>
                      <?php foreach($viewStudents as $key => $val){ ?>
                      <option value="<?php echo $val['room_ID']; ?>"><?php echo $val['room_NAME']; ?></option>
                      <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3">
                  <label for="Start2" class="">Start2</label>
                  <select name="start2" id="start2" class="form-control">
                    <option value="">None</option>
                    <option value="TBD">TBD</option>  
                    <option value="7:00 AM">7:00 AM</option>
                    <option value="7:30 AM">7:30 AM</option>
                    <option value="8:00 AM">8:00 AM</option>
                    <option value="8:30 AM">8:30 AM</option>
                    <option value="9:00 AM">9:00 AM</option>
                    <option value="9:30 AM">9:30 AM</option>
                    <option value="10:00 AM">10:00 AM</option>
                    <option value="10:30 AM">10:30 AM</option>
                    <option value="11:00 AM">11:00 AM</option>
                    <option value="11:30 AM">11:30 AM</option>
                    <option value="12:00 PM">12:00 PM</option>
                    <option value="12:30 PM">12:30 PM</option>
                    <option value="1:00 PM">1:00 PM</option>
                    <option value="1:30 PM">1:30 PM</option>
                    <option value="2:00 PM">2:00 PM</option>
                    <option value="2:30 PM">2:30 PM</option>
                    <option value="3:00 PM">3:00 PM</option>
                    <option value="3:30 PM">3:30 PM</option>
                    <option value="4:00 PM">4:00 PM</option>
                    <option value="4:30 PM">4:30 PM</option>
                    <option value="5:00 PM">5:00 PM</option>
                    <option value="5:30 PM">5:30 PM</option>
                    <option value="6:00 PM">6:00 PM</option>
                    <option value="6:30 PM">6:30 PM</option>
                    <option value="7:00 PM">7:00 PM</option>
                    <option value="7:30 PM">7:30 PM</option>
                    <option value="8:00 PM">8:00 PM</option>
                    <option value="8:30 PM">8:30 PM</option>
                    <option value="9:00 PM">9:00 PM</option>
                    <option value="9:30 PM">9:30 PM</option>
                    <option value="10:00 PM">10:00 PM</option>
                    <option value="10:30 PM">10:30 PM</option>
                    <option value="11:00 PM">11:00 PM</option>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="End2" class="">End2</label>
                  <select name="end2" id="end2" class="form-control">
                    <option value="">None</option>
                    <option value="TBD">TBD</option>  
                    <option value="7:00 AM">7:00 AM</option>
                    <option value="7:30 AM">7:30 AM</option>
                    <option value="8:00 AM">8:00 AM</option>
                    <option value="8:30 AM">8:30 AM</option>
                    <option value="9:00 AM">9:00 AM</option>
                    <option value="9:30 AM">9:30 AM</option>
                    <option value="10:00 AM">10:00 AM</option>
                    <option value="10:30 AM">10:30 AM</option>
                    <option value="11:00 AM">11:00 AM</option>
                    <option value="11:30 AM">11:30 AM</option>
                    <option value="12:00 PM">12:00 PM</option>
                    <option value="12:30 PM">12:30 PM</option>
                    <option value="1:00 PM">1:00 PM</option>
                    <option value="1:30 PM">1:30 PM</option>
                    <option value="2:00 PM">2:00 PM</option>
                    <option value="2:30 PM">2:30 PM</option>
                    <option value="3:00 PM">3:00 PM</option>
                    <option value="3:30 PM">3:30 PM</option>
                    <option value="4:00 PM">4:00 PM</option>
                    <option value="4:30 PM">4:30 PM</option>
                    <option value="5:00 PM">5:00 PM</option>
                    <option value="5:30 PM">5:30 PM</option>
                    <option value="6:00 PM">6:00 PM</option>
                    <option value="6:30 PM">6:30 PM</option>
                    <option value="7:00 PM">7:00 PM</option>
                    <option value="7:30 PM">7:30 PM</option>
                    <option value="8:00 PM">8:00 PM</option>
                    <option value="8:30 PM">8:30 PM</option>
                    <option value="9:00 PM">9:00 PM</option>
                    <option value="9:30 PM">9:30 PM</option>
                    <option value="10:00 PM">10:00 PM</option>
                    <option value="10:30 PM">10:30 PM</option>
                    <option value="11:00 PM">11:00 PM</option>
                  </select>
                </div>
              </div>
              <br>  
              <div class="row">
                <div class="col-lg-3">
                  <label for="Instructor" class="">Instructor</label>
                    <?php
                      $viewIntructors = INSTRUCTORS:: getAllInstructors(array("instructor_ID"=>"ASC"));
                    ?>
                    <select name="instructor_ID" id="instructor_ID" class="form-control">
                      <?php foreach($viewIntructors as $key => $val){ ?>
                      <option <?php if($val['instructor_NAME'] == 'TBD'){ echo "SELECTED=SELECTED"; } ?> value="<?php echo $val['instructor_ID']; ?>"><?php echo $val['instructor_NAME']; ?></option>
                      <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3">
                  <label for="max_student" class="">Max Student</label>
                  <input class="form-control" name="max_student" type="number" id="max_student" value="30" />
                </div>
              </div> 
              <div class="row ">
                <div class="col-lg-12 ">
                  <div class="row">
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-3">
                      <button id="button" class="btn btn-success btn-block" type="submit"><i class="fa fa-edit"></i> Add
                      </button>
                    </div>
                    <div class="col-lg-1">
                    </div>
                  </div>
                </div>
              </div>
          </form>
          <hr>
          <?php
          case "view": ?>


          <?php
          $record = SUBJSCHED::getAllperSEM2(array("subject_sched_ID" => "DESC"));
          #print_r($record);

          ?>
          <div class="row ">
            <div class="col-lg-12">
              <legend><i class="fa fa-user"></i> View</legend>
              <div class="card">
                <div class="card-body">
                  <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                        <tr style="color: grey">
                            <th width="30">Ref</th>
                            <th width="300">Section</th>
                            <th width="">Subject</th>
                            <th width="">Day</th>
                            <th width="">Room</th>
                            <th width="">Start</th>
                            <th width="">End</th>
                            <th width="">Instructor</th>
                            <th width="">Max</th>
                            <th width="">Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
  
                        $x= 1;
                        foreach($record as $key => $val){ 
                        if($x <= 50){
                        ?>
                      <tr>
                        <td><?php echo $val['subject_sched_ID']; ?></td>
                        <td><?php $sem = SEMSECTION::getID($val['semester_section_ID']); ?>
                            <?php $d = SEMSECTION::getSEM($sem['sem_ID']);
                              echo $d['sem_NAME']; ?>
                              -
                            <?php $c = COURSE::getbyID($sem['course_ID']);
                              echo $c['course_INIT']; ?>
                              -
                            <?php echo $sem['semester_section_NAME']; ?></td>
                        <td><?php $c = SUBJECTS::getID( $val['subject_ID']);
                            echo "(".$c['subject_CODE'].")".$c['subject_DESCRIPTION']; ?> 
                            &nbsp;</td>
                        <td><?php echo $val['day']; ?> <?php if($val['day2'] != NULL){ ?>/<br /><?php echo $val['day2']; ?><?php } ?></td>
                        <td><?php
                            $r = ROOM::getID($val['room_ID']);
                            echo $r['room_NAME'];
                             ?>
                               <?php if($val['room_ID2'] != NULL){ ?>/<br />
                               <?php
                                   $r = ROOM::getID($val['room_ID2']);
                                  echo $r['room_NAME'];
                             } ?></td>
                        <td><?php echo $val['start']; ?> <?php if($val['start2'] != NULL){ ?>/<br /><?php echo $val['start2']; ?><?php } ?></td>
                        <td><?php echo $val['end']; ?> <?php if($val['end2'] != NULL){ ?>/<br /><?php echo $val['end2']; ?><?php } ?></td>
                        <td><?php $i = INSTRUCTORS::getSingle(array(),$val['instructor_ID']);
                            echo $i['instructor_NAME']; ?></td>
                        <td><?php echo $val['max_student']; ?></td>
                        <td><a href="subject_sched.php?action=edit&id=<?php echo $val['subject_sched_ID']; ?>">Edit</a></td>
                      </tr>
                        <?php  }
                        $x++;
                        }
                        
                        ?>
                    </tbody>
                  </table>   
                </div>
                 
              </div>
              
            </div>
          </div>
        </div>  
      </div>
    </div>
  </div>
</div>


<?php break; 

case "edit": ?>
    <?php 
    $ssid1 = $_GET['id'];
    #msgbox($ssid1);
    $ssid = SUBJSCHED::getSubjectSchedID($ssid1);
    #print_r($ssid);
    ?>

<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <br>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
          <i class="fa fa-folder-o"></i>  Edit Subject Schedule </h3> 
        </div>
        <div class="panel-body">
          <form action="<?php echo $_SERVER['REQUEST_URI']?>" method="post">
              <div class="row">
                <div class="col-lg-6">
                  <label class="">Section</label>
                    <?php
                      #$sem = SEMSECTION::getAll(array("sem_ID"=>"DESC");
                      $sem = SEMSECTION::getAll(array("semester_section_ID"=>"DESC"));
                      #print_r($sem);
                    ?>
                    <select name="semester_section_ID" id="semester_section_ID" class="form-control">
                        <?php foreach($sem as $key => $val){ ?>
                      <option value="<?php echo $val['semester_section_ID']; ?>"
                                    <?php
                                        if(($val['semester_section_ID']) == ($ssid['semester_section_ID']))
                                        { 
                                            echo "selected=selected"; 
                                        }
                                    ?>
                                    >
                          <?php $sem = SEMSECTION::getID($val['semester_section_ID']); ?>
                          <?php $d = SEMSECTION::getSEM($sem['sem_ID']);
                          echo $d['sem_NAME']; ?>
                          -
                          <?php $c = COURSE::getbyID($val['course_ID']);
                          echo $c['course_INIT']; ?>-<?php echo $val['semester_section_NAME']; ?></option>
                            <?php } ?>
                    </select>
                </div>
                <div class="col-lg-6">
                  <label for="Subject" class="">Subject</label>
                    <?php
                       $viewSubjects = SUBJECTS:: getAllSubjects(array("subject_CODE"=>"ASC"));
                    ?>
                      
                      <select name="subject_ID" id="subject_ID" class="form-control">
                          <?php foreach($viewSubjects as $key => $val){ ?>
                          <option value="<?php echo $val['subject_ID']; ?>"
                                   <?php
                                      if(($val['subject_ID']) == ($ssid['subject_ID']))
                                      { 
                                          echo "selected=selected"; 
                                      }
                                  ?>
                                  
                          ><?php echo $val['subject_CODE']; ?> - <?php echo $val['subject_DESCRIPTION']; ?> - <?php echo $val['LEC_UNIT']."/".$val['LAB_UNIT']; ?></option>
                          <?php } ?>    
                      </select>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-lg-3">
                  <label for="Day" class="">Day</label>
                  <select name="day" id="day" class="form-control">
                    <option value="Monday"    
                            <?php
                                  if("Monday" == $ssid['day'])
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                              >MON</option>
                    <option value="Tuesday" <?php
                                  if("Tuesday"== ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> >TUE</option>
                    <option value="Wednesday"<?php
                                  if("Wednesday" == ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>WED</option>
                    <option value="Thursday"<?php
                                  if("Thursday" == ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>THU</option>
                    <option value="Friday"<?php
                                  if("Friday" == ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>FRI</option>
                    <option value="Saturday"<?php
                                  if("Saturday" == ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>SAT</option>
                        <option value="TBD"<?php
                                  if("TBD" == ($ssid['day']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>TBD</option>         
                    <?php foreach($viewSubjects as $key => $val){ ?>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Room" class="">Room</label>
                    <?php
                       $viewStudents = ROOM:: getallrooms(array("room_ID"=>"ASC"));
                    ?>
                    <select name="room_ID" id="room_ID" class="form-control">
                      <?php foreach($viewStudents as $key => $val){ ?>
                      <option value="<?php echo $val['room_ID']; ?>" 
                              <?php
                                  if($val['room_ID']  == ($ssid['room_ID']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> ><?php echo $val['room_NAME']; ?></option>
                      <?php } ?>
                    </select>
                </div>
                <div class="col-lg-3">
                  <label for="Start" class="">Start</label>
                  <select name="start" id="start" class="form-control">
                    <option  <?php
                                  if("TBD"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> value="TBD">TBD</option> 
                    <option value="7:00 AM"
                            <?php
                                  if("7:00 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 AM</option>
                    <option value="7:30 AM"
                            <?php
                                  if("7:30 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:30 AM</option>
                    <option value="8:00 AM"
                            <?php
                                  if("8:00 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:00 AM</option>
                    <option value="8:30 AM"
                            <?php
                                  if("8:30 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:30 AM</option>
                    <option value="9:00 AM" 
                            <?php
                                  if("9:00 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 AM</option>
                    <option value="9:30 AM"
                            <?php
                                  if("9:30 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:30 AM</option>
                    <option value="10:00 AM"
                            <?php
                                  if("10:00 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:00 AM</option>
                    <option value="10:30 AM" 
                            <?php
                                  if("10:30 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 AM</option>
                    <option value="11:00 AM" <?php
                                  if("11:00 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 AM</option>
                    <option value="11:30 AM" <?php
                                  if("11:30 AM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:30 AM</option>
                    <option value="12:00 PM"<?php
                                  if("12:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:00 PM</option>
                    <option value="12:30 PM"
                            <?php
                                  if("12:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:30 PM</option>
                    <option value="1:00 PM" <?php
                                  if("1:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:00 PM</option>
                    <option value="1:30 PM" <?php
                                  if("1:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:30 PM</option>
                    <option value="2:00 PM" <?php
                                  if("2:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:00 PM</option>
                    <option value="2:30 PM" <?php
                                  if("2:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:30 PM</option>
                    <option value="3:00 PM" <?php
                                  if("3:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:00 PM</option>
                    <option value="3:30 PM" <?php
                                  if("3:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:30 PM</option>
                    <option value="4:00 PM" <?php
                                  if("4:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:00 PM</option>
                    <option value="4:30 PM" <?php
                                  if("4:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:30 PM</option>
                    <option value="5:00 PM" <?php
                                  if("5:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:00 PM</option>
                    <option value="5:30 PM" <?php
                                  if("5:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:30 PM</option>
                    <option value="6:00 PM"
                            <?php
                                  if("6:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:00 PM</option>
                    <option value="6:30 PM" <?php
                                  if("6:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:30 PM</option>
                    <option value="7:00 PM" <?php
                                  if("7:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 PM</option>
                    <option value="7:30 PM" <?php
                                  if("7:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:30 PM</option> 

                    <option value="8:00 PM" <?php
                                  if("8:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:00 PM</option> 
                              
                              
                     <option value="8:30 PM" <?php
                                  if("8:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:30 PM</option> 

                    <option value="9:00 PM" <?php
                                  if("9:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 PM</option>          

                    <option value="9:30 PM" <?php
                                  if("9:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:30 PM</option> 

                    <option value="10:00 PM" <?php
                                  if("10:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:00 PM</option>  
                      
                      
                    <option value="10:30 PM" <?php
                                  if("10:30 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 PM</option>  
                    <option value="11:00 PM" <?php
                                  if("11:00 PM"  == ($ssid['start']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 PM</option> 
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Installment" class="">End</label>
                  <select name="end" id="start" class="form-control">
                    <option  <?php
                                    if("TBD"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?> value="TBD">TBD</option> 
                      <option value="7:00 AM"
                              <?php
                                    if("7:00 AM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>7:00 AM</option>
                      <option value="7:30 AM"
                              <?php
                                    if("7:30 AM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>
                              >7:30 AM</option>
                      <option value="8:00 AM"
                              <?php
                                    if("8:00 AM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>
                              >8:00 AM</option>
                      <option value="8:30 AM"
                              <?php
                                    if("8:30 AM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>
                              >8:30 AM</option>
                      <option value="9:00 AM" 
                              <?php
                                    if("9:00 AM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>9:00 AM</option>
                      <option value="9:30 AM"
                              <?php
                                    if("9:30 AM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>
                              >9:30 AM</option>
                      <option value="10:00 AM"
                              <?php
                                    if("10:00 AM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>
                              >10:00 AM</option>
                      <option value="10:30 AM" 
                              <?php
                                    if("10:30 AM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>10:30 AM</option>
                      <option value="11:00 AM" <?php
                                    if("11:00 AM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>11:00 AM</option>
                      <option value="11:30 AM" <?php
                                    if("11:30 AM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>11:30 AM</option>
                      <option value="12:00 PM"<?php
                                    if("12:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>12:00 PM</option>
                      <option value="12:30 PM" 
                              <?php
                                    if("12:30 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>12:30 PM</option>
                      <option value="1:00 PM" <?php
                                    if("1:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>1:00 PM</option>
                      <option value="1:30 PM" <?php
                                    if("1:30 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>1:30 PM</option>
                      <option value="2:00 PM" <?php
                                    if("2:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>2:00 PM</option>
                      <option value="2:30 PM" <?php
                                    if("2:30 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>2:30 PM</option>
                      <option value="3:00 PM" <?php
                                    if("3:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>3:00 PM</option>
                      <option value="3:30 PM" <?php
                                    if("3:30 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>3:30 PM</option>
                      <option value="4:00 PM" <?php
                                    if("4:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>4:00 PM</option>
                      <option value="4:30 PM" <?php
                                    if("4:30 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>4:30 PM</option>
                      <option value="5:00 PM" <?php
                                    if("5:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>5:00 PM</option>
                      <option value="5:30 PM" <?php
                                    if("5:30 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>5:30 PM</option>
                      <option value="6:00 PM"
                              <?php
                                    if("6:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>6:00 PM</option>
                      <option value="6:30 PM" <?php
                                    if("6:30 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>6:30 PM</option>
                      <option value="7:00 PM" <?php
                                    if("7:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>7:00 PM</option>
                    <option value="7:30 PM" <?php
                                    if("7:30 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>7:30 PM</option> 
                     
                    <option value="8:00 PM" <?php
                                    if("8:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>8:00 PM</option> 
                                
                                
                   <option value="8:30 PM" <?php
                                    if("8:30 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>8:30 PM</option> 
                     
                    <option value="9:00 PM" <?php
                                    if("9:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>9:00 PM</option>          
                      
                    <option value="9:30 PM" <?php
                                    if("9:30 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>9:30 PM</option> 
                     
                    <option value="10:00 PM" <?php
                                    if("10:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>10:00 PM</option>  
                        
                    <option value="10:30 PM" <?php
                                    if("10:30 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>10:30 PM</option>  
                    <option value="11:00 PM" <?php
                                    if("11:00 PM"  == ($ssid['end']))
                                    { 
                                        echo "selected=selected"; 
                                    }
                                ?>>11:00 PM</option>  
                  </select>
                </div>
              </div>
              <br>
              <div class="row">
                <div class="col-lg-3">
                  <label for="Day2" class="">Day2</label>
                  <select name="day2" id="day" class="form-control">
                    <option value="">None</option>
                    <option value="Monday"    
                            <?php
                                  if("Monday" == $ssid['day2'])
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                              >MON</option>
                    <option value="Tuesday" <?php
                                  if("Tuesday"== ($ssid['day2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> >TUE</option>
                    <option value="Wednesday"<?php
                                  if("Wednesday" == ($ssid['day2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>WED</option>
                    <option value="Thursday"<?php
                                  if("Thursday" == ($ssid['day2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>THU</option>
                    <option value="Friday"<?php
                                  if("Friday" == ($ssid['day2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>FRI</option>
                    <option value="Saturday"<?php
                                  if("Saturday" == ($ssid['day2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>SAT</option>
                    <option value="TBD"<?php
                              if("TBD" == ($ssid['day2']))
                              { 
                                  echo "selected=selected"; 
                              }
                              ?>>TBD</option>         
                      <?php foreach($viewSubjects as $key => $val){ ?>
                      <?php } ?>
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="Room2" class="">Room2</label>
                    <?php
                       $viewStudents = ROOM:: getallrooms(array("room_ID"=>"ASC"));
                    ?>
                        <select name="room_ID2" id="room_ID2" class="form-control">
                          <option value="">None</option>
                          <?php foreach($viewStudents as $key => $val){ ?>
                          <option value="<?php echo $val['room_ID']; ?>" 
                                  <?php
                                      if($val['room_ID']  == ($ssid['room_ID2']))
                                      { 
                                          echo "selected=selected"; 
                                      }
                                  ?> ><?php echo $val['room_NAME']; ?></option>
                          <?php } ?>
                        </select>
                </div>
                <div class="col-lg-3">
                  <label for="Start2" class="">Start2</label>
                  <select name="start2" id="start2" class="form-control">
                    <option value="">None</option>
                    <option  <?php
                                  if("TBD"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> value="TBD">TBD</option> 
                    <option value="7:00 AM"
                            <?php
                                  if("7:00 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 AM</option>
                    <option value="7:30 AM"
                            <?php
                                  if("7:30 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:30 AM</option>
                    <option value="8:00 AM"
                            <?php
                                  if("8:00 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:00 AM</option>
                    <option value="8:30 AM"
                            <?php
                                  if("8:30 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:30 AM</option>
                    <option value="9:00 AM" 
                            <?php
                                  if("9:00 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 AM</option>
                    <option value="9:30 AM"
                            <?php
                                  if("9:30 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:30 AM</option>
                    <option value="10:00 AM"
                            <?php
                                  if("10:00 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:00 AM</option>
                    <option value="10:30 AM" 
                            <?php
                                  if("10:30 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 AM</option>
                    <option value="11:00 AM" <?php
                                  if("11:00 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 AM</option>
                    <option value="11:30 AM" <?php
                                  if("11:30 AM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:30 AM</option>
                    <option value="12:00 PM"<?php
                                  if("12:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:00 PM</option>
                    <option value="12:30 PM"
                            <?php
                                  if("12:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:30 PM</option>
                    <option value="1:00 PM" <?php
                                  if("1:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:00 PM</option>
                    <option value="1:30 PM" <?php
                                  if("1:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:30 PM</option>
                    <option value="2:00 PM" <?php
                                  if("2:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:00 PM</option>
                    <option value="2:30 PM" <?php
                                  if("2:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:30 PM</option>
                    <option value="3:00 PM" <?php
                                  if("3:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:00 PM</option>
                    <option value="3:30 PM" <?php
                                  if("3:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:30 PM</option>
                    <option value="4:00 PM" <?php
                                  if("4:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:00 PM</option>
                    <option value="4:30 PM" <?php
                                  if("4:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:30 PM</option>
                    <option value="5:00 PM" <?php
                                  if("5:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:00 PM</option>
                    <option value="5:30 PM" <?php
                                  if("5:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:30 PM</option>
                    <option value="6:00 PM"
                            <?php
                                  if("6:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:00 PM</option>
                    <option value="6:30 PM" <?php
                                  if("6:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:30 PM</option>
                    <option value="7:00 PM" <?php
                                  if("7:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 PM</option>
                  <option value="7:30 PM" <?php
                                  if("7:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:30 PM</option> 
                   
                    <option value="8:00 PM" <?php
                                  if("8:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:00 PM</option> 
                              
                              
                     <option value="8:30 PM" <?php
                                  if("8:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:30 PM</option> 
                   
                    <option value="9:00 PM" <?php
                                  if("9:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 PM</option>          
                    
                      <option value="9:30 PM" <?php
                                  if("9:30 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:30 PM</option> 
                   
                    <option value="10:00 PM" <?php
                                  if("10:00 PM"  == ($ssid['start2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:00 PM</option>  
                  </select>
                </div>
                <div class="col-lg-3">
                  <label for="End2" class="">End2</label>
                  <select name="end2" id="end2" class="form-control">
                    <option value="">None</option>
                    <option  <?php
                                  if("TBD"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?> value="TBD">TBD</option> 
                    <option value="7:00 AM"
                            <?php
                                  if("7:00 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 AM</option>
                    <option value="7:30 AM"
                            <?php
                                  if("7:30 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >7:30 AM</option>
                    <option value="8:00 AM"
                            <?php
                                  if("8:00 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:00 AM</option>
                    <option value="8:30 AM"
                            <?php
                                  if("8:30 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >8:30 AM</option>
                    <option value="9:00 AM" 
                            <?php
                                  if("9:00 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 AM</option>
                    <option value="9:30 AM"
                            <?php
                                  if("9:30 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >9:30 AM</option>
                    <option value="10:00 AM"
                            <?php
                                  if("10:00 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>
                            >10:00 AM</option>
                    <option value="10:30 AM" 
                            <?php
                                  if("10:30 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 AM</option>
                    <option value="11:00 AM" <?php
                                  if("11:00 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 AM</option>
                    <option value="11:30 AM" <?php
                                  if("11:30 AM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:30 AM</option>
                    <option value="12:00 PM"<?php
                                  if("12:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:00 PM</option>
                    <option value="12:30 PM"
                            <?php
                                  if("12:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>12:30 PM</option>
                    <option value="1:00 PM" <?php
                                  if("1:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:00 PM</option>
                    <option value="1:30 PM" <?php
                                  if("1:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>1:30 PM</option>
                    <option value="2:00 PM" <?php
                                  if("2:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:00 PM</option>
                    <option value="2:30 PM" <?php
                                  if("2:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>2:30 PM</option>
                    <option value="3:00 PM" <?php
                                  if("3:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:00 PM</option>
                    <option value="3:30 PM" <?php
                                  if("3:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>3:30 PM</option>
                    <option value="4:00 PM" <?php
                                  if("4:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:00 PM</option>
                    <option value="4:30 PM" <?php
                                  if("4:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>4:30 PM</option>
                    <option value="5:00 PM" <?php
                                  if("5:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:00 PM</option>
                    <option value="5:30 PM" <?php
                                  if("5:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>5:30 PM</option>
                    <option value="6:00 PM"
                            <?php
                                  if("6:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:00 PM</option>
                    <option value="6:30 PM" <?php
                                  if("6:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>6:30 PM</option>
                    <option value="7:00 PM" <?php
                                  if("7:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:00 PM</option>
                  <option value="7:30 PM" <?php
                                  if("7:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>7:30 PM</option> 
                   
                    <option value="8:00 PM" <?php
                                  if("8:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:00 PM</option> 
                              
                              
                     <option value="8:30 PM" <?php
                                  if("8:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>8:30 PM</option> 
                   
                    <option value="9:00 PM" <?php
                                  if("9:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:00 PM</option>          
                    
                      <option value="9:30 PM" <?php
                                  if("9:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>9:30 PM</option> 
                   
                    <option value="10:00 PM" <?php
                                  if("10:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:00 PM</option> 
                      <option value="10:30 PM" <?php
                                  if("10:30 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>10:30 PM</option>  
                   
                    <option value="11:00 PM" <?php
                                  if("11:00 PM"  == ($ssid['end2']))
                                  { 
                                      echo "selected=selected"; 
                                  }
                              ?>>11:00 PM</option>  
                  </select>
                </div>
              </div>
              <br>  
              <div class="row">
                <div class="col-lg-3">
                  <label for="Instructor" class="">Instructor</label>
                    <?php
                      $viewIntructors = INSTRUCTORS:: getAllInstructors(array("instructor_ID"=>"ASC"));
                    ?>
                        <select name="instructor_ID" id="instructor_ID" class="form-control">
                          <?php foreach($viewIntructors as $key => $val){ ?>
                          <option value="<?php echo $val['instructor_ID']; ?>" <?php
                                      if( $val['instructor_ID'] == ($ssid['instructor_ID']))
                                      { 
                                          echo "selected=selected"; 
                                      }
                                  ?>
                                  ><?php echo $val['instructor_NAME']; ?></option>
                          <?php } ?>
                        </select>
                </div>
                <div class="col-lg-3">
                  <label for="max_student" class="">Max Student</label>
                 <!--  <input class="form-control" name="max_student" type="number" id="max_student" value="30" /> -->
                  <input class="form-control" name="max_student" type="number" id="max_student" value="<?= $ssid['max_student'];?>" />
                  <input type="hidden" name =subject_sched_ID value="<?= $ssid['subject_sched_ID'];?>">
                </div>
              </div> 
              <div class="row ">
                <div class="col-lg-12 ">
                  <div class="row">
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-3">
                    </div>
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-3">
                      <button id="button" class="btn btn-success btn-block" type="submit" onclick="return confirm('Do You Want Save ?')"><i class="fa fa-edit"></i> Save
                      </button>
                    </div>
                    <div class="col-lg-1">
                    </div>
                  </div>
                </div>
              </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
    
<?php break; 
case "delete": ?>



<?php break;


default: header("location:?action=view");
}
?>

</body>
</html>
