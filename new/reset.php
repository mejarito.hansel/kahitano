<?php
	include('init.php');

if (SESSION::isLoggedIn()) {
	HTML::redirect();
}


?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
      <title>RESET | <?php echo $site_options['site_NAME']; ?></title>
	</head>

	<body>

		<!-- Navbar -->	
		<?php include(LAYOUTS . "top_nav.php"); ?>
		<!--END Navbar -->

		<div class="container">

		<?php
		include(LAYOUTS . "banner.php");
		?>

			<!-- start body -->

			<!-- start row -->
			<div class="row">
				<div class="col-lg-12">
					<!-- breadcrumbs -->
					<ul class="breadcrumb">
						<li class=""><a href="<?php echo HOME; ?>index.php">Home</a></li>
						<li class="active">Reset</li>
					</ul>
					<!-- end breadcrumbs -->  
				</div>
			</div>
			<!-- end row -->

			<!-- start row -->
			<div class="row">
				<div class="col-md-2"></div>

				<div class="col-md-8">


					<div class="well">
                    
						For your forgotten username or password.<br /> Please do contact Kevin M. San Jose (IT Manager)
                    
						
					</div>
					
				</div>
				
			</div>
			<!-- end row -->

			<!-- end body -->

			<div class="spacer-short"></div>

			<!-- footer-->
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- container end -->

	</body>
</html>