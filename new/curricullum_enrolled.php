<?php 
//INITIALIZE INCLUDES
	include('init.php');
SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
            CECONTROLLER::add(Request::post());
		#	var_dump(Request::post());
			
			break;
			case "edit": CECONTROLLER::update($_GET['id'],Request::post()); break;
			#case "update_blog":  BLOG::update(Request::post("blog_info")); break;
			
			#case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
            case "enroll": CECONTROLLER::enroll($_GET['id']); break;
		    case "drop": CECONTROLLER::drop($_GET['id']); break;
			case "delete": CECONTROLLER::delete($_GET['id']); break;
		    
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>HOME | SJB CAINTA SIMS</title>
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end of alert messages -->
			<!-- start row container -->
			<!-- 
            <div class="row">
				<div class="col-lg-12">
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
            </div>	
			</div>
            -->
			<!-- end row container -->
            <!-- row container -->
			<div class="row">
				<!-- left nav -->   
            <?php
				if (!SESSION::isLoggedIn()) {
					?>
				<div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> Course Categories </h3>
                    </div>
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked">
                        <li >
                            <a style="cursor:pointer" id="1-link">
                                <span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=1','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CCS (2)
                                </span>
                            </a>
                        </li>
                        <li >
                            <a style="cursor:pointer" id="2-link">
                                <span class="span_link" onclick="javascript:window.open('categories.php?action=view&id=2','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><i class="fa fa-folder-open"></i> CBA</span>
                            </a>
                        </li>
                        <script>

                                  $( "#1-link" ).click(function() {
                             $( "#1-content" ).slideToggle( "fast" );
                         });
                                  $( "#2-link" ).click(function() {
                             $( "#2-content" ).slideToggle( "fast" );
                         });

                        </script>
                        <!--
                            <li class="active"><a href="#">Computer Studies (#)</a></li>
                            <li class=""><a href="#">Business Course</a></li>
                            <li class="dropdown"><a class="dropdown-toggle" id="link" data-toggle="dropdown" href="#">Cat3 <span class="caret"></span></a>
                                <ul class="nav nav-pills nav-stacked nav-sub"  >
                                    <li><a href="#">Cat3 Sub 1 <span class="caret"></span></a>

                                    <ul class="nav nav-pills nav-stacked  nav-sub"  style="">

                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                    </ul>

                                    </li>
                                    <li><a href="#">Cat3 Sub 2</a></li>
                                    <li><a href="#">Cat3 Sub 3</a></li>
                                    <li><a href="#">Cat3 Sub 4</a></li>
                                </ul>
                            </li>

                           --> 
                        </ul>
                    </div>
                </div>
				</div>
                <?php   }else{?>
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
                <?php } ?>
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Curricullum Enrolled </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <!-- Name -->
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Name</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name=si_ID class="form-control">
                                        <?php 
										
										$x = 0;
										$getallsubject= CECONTROLLER::getallstudent();   
                                            foreach($getallsubject as $key => $value){
											
											
											
										if(CECONTROLLER::isEnrolled($value['si_ID']) == 0){	
										$x++;
                                        ?>
                                        <option value=<?= $value['si_ID']?>>
                                            <?= $value['si_LNAME'].", ".$value['si_FNAME']." ".$value['si_MNAME']?>
                                        </option>
                                        
                                        <?php } ?>
                                        
                                        
                                        <?php }  ?>
                                        
                                        
                                    </select>
                                    
                                    <?php 	#print_r($value); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Course/Curricullum</label>
                                </div>
                                <div class="col-sm-8">
                                    <select name=curricullum_ID class="form-control">
                                        <?php $getallCurricullum= CECONTROLLER::getallCurricullum(array("curricullum_ID"=>"ASC"));   
                                            foreach($getallCurricullum as $key => $value){
                                                $cid = $value['course_ID'];
                                                $getsingleCourse = CECONTROLLER::getsingleCourse(array("course_ID"=>"ASC"),$cid);
                                                foreach($getsingleCourse as $key2 => $val){
                                        ?>
                                        <option value=<?= $value['curricullum_ID']?>>
                                            <?= $val['course_NAME']." - ".$value['curricullum_NAME']; ?>
                                        </option>
                                        <?php } } ?>
                                    </select>
                                    
                                  
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button  class='btn btn-success btn-block' type='submit' <?php if($x==0){ echo "disabled"; } ?>><i class='fa fa-edit'></i> Set Curricullum</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":
                        $siiid =$_GET['id'];
                        $getsingle = CECONTROLLER::getSingle(array("ce_ID"=>"ASC"),$siiid  );
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Curricullum Enrolled </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <!-- Name -->
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Curricullum</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=curricullum_ID class="form-control" readonly>
                                        <?php $getallCurricullum= CECONTROLLER::getallCurricullum(array("curricullum_ID"=>"ASC"));   
                                            foreach($getallCurricullum as $key => $value){
                                                $cid = $value['course_ID'];
                                                $getsingleCourse = CECONTROLLER::getsingleCourse(array("course_ID"=>"ASC"),$cid);
                                                foreach($getsingleCourse as $key2 => $val){
                                        ?>
                                        <option value=<?= $value['curricullum_ID']?>
                                        <?php        
                                $getsingleCurricullum= CECONTROLLER::getallCurricullum(array("curricullum_ID"=>"ASC"),$getsingle['curricullum_ID']);
                                            foreach($getallCurricullum as $keyx => $valuex){    
                                            if($value['curricullum_ID']==$valuex['curricullum_ID']){
                                            echo"selected";
                                            }
                                        }    
                                        ?>   
                                        >
                                            <?= $value['curricullum_NAME']." - (".$val['course_NAME'].")"?>
                                        </option>
                                        <?php } } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Name</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=si_ID class="form-control" readonly>
                                        <?php $getallsubject= CECONTROLLER::getallstudent(array("si_ID"=>"ASC"));   
                                            foreach($getallsubject as $key => $value){
                                        ?>
                                        <option value=<?= $value['si_ID']?>
                                        <?php        
                                $getsinglestudent= CECONTROLLER::getsinglestudent(array("si_ID"=>"ASC"),$getsingle['si_ID']);
                                            foreach($getsinglestudent as $keyx => $valuex){    
                                            if($value['si_ID']==$valuex['si_ID']){
                                            echo"selected";
                                            }
                                        }          
                                        ?>        
                                        >
                                            <?= $value['si_LNAME'].", ".$value['si_FNAME']." ".$value['si_MNAME']?>
                                        </option>
                                        <?php }  ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class='col-md-6'>Status</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name=status class="form-control">
                                        <option value="ENROLLED" <?php if($getsingle['status']=="ENROLL"){ echo "selected"; } ?>>ENROLL</option>
                                        <option value="DROP" <?php if($getsingle['status']=="DROP"){ echo "selected"; } ?>>DROP</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <button  class='btn btn-success btn-block' type='submit'><i class='fa fa-edit'></i>Update Curricullum Enrolled</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
						include "searching.php";						
						?>
							
                    <?php break; 
                        
                        case "view":?>
                        <?php
								if(isset($_GET['id']))
								{
                                }else{
					?>
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table>
                                <tr>
                                <td width="50%">
                                    <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                    <i class="fa fa-folder-o"></i> View Curricullum Enrolled </h3>
                                </td>
                                <script src="jquery.js" type="text/javascript"></script>
                                    <script> 
                                    $(document).ready(function(){
                                      $("#flip").click(function(){
                                        $("#panel").slideToggle("fast");
                                      });
                                    });
                                    </script>
                                <td >
                                    <div class="input-group col-md-12">
                                      <form id="form1" runat="server">
                                       		 <input id="searchItem" class="searchItem form-control" name="q" type="text" onkeyup="term(q.value)" autofocus="autofocus" onchange="term(q.value)"/>
                                        </form>
                                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
                                </td>
                                <script>
                                    			$(document).ready(function() {
												$('#searchItem').keyup(function(){
												var s_Item = $('#searchItem').val();
													
													if(s_Item.length >= 3)
													{
														 $.ajax({
															type: "POST",
															url: "search.php?action=ce.php",
															data: 'search_term=' + s_Item,
															success: function(msg){
																/* $('#resultip').html(msg); */
																	$("#display_result").show();
																	$("#display_result").html(msg);
																	
																	$("#display_hide").hide();
																
															}
											
														}); // Ajax Call
													
														//alert(s_Item);
														}else{
															$("#display_hide").show();
															$("#display_result").hide();   
														}
													});
												});			
                                    			</script>
                                <td width="5%">
                                <div class="col-md-1">   
                                <button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('curricullum_enrolled.php?action=add','_self');"><i class='fa fa-plus'></i></button>
                                </div>
                                </td>
                                </tr>
                                </table>
                            </div>
							<div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
								<table class="table table-hover table-responsive table-striped text-md">
									<th style="width:15%;">Curricullum Name</th>
									<th style="width:15%;">Student Name</th>
									<th style="width:15%;">Status</th>
									<th style="width:15%;">Operation</th>
                                    
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    $viewStudents = CECONTROLLER:: getallce(array("curricullum_ID"=>"ASC"));
                                     if(count($viewStudents)>=10) 
                                     {
                                         $pagination2 = new Pagination($viewStudents, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($viewStudents, 10, NULL);
                                     }

                                     $viewStudents = $pagination2->get_array();
                                     if($viewStudents) {
                                        foreach($viewStudents as $key => $value){
                                    ?>
                                        <tr>
                        <td><?php $getsingleCurricullum =CECONTROLLER::getsingleCurricullum(array("curricullum_ID"=>"ASC"),$value['curricullum_ID']);
                            foreach($getsingleCurricullum as $key1 => $value1){
                                $getsingleCourse = CECONTROLLER::getsingleCourse(array("course_ID"=>"ASC"),$value1['course_ID']);
                                foreach($getsingleCourse as $keys => $val){
                                    echo "(".$val['course_NAME'].") - ".$value1['curricullum_NAME']." Curricullum";
                                }
                            }
                            ?>
                        <td><?php $getsinglesubjects = CECONTROLLER::getsinglestudent(array("si_ID"=>"ASC"),$value['si_ID']);
                            foreach($getsinglesubjects as $key2 => $value2){
                            echo $value2['si_LNAME'].", ".$value2['si_FNAME']." ".$value2['si_MNAME'];
                            }
                            ?>
                        <td><?php $getsingle = CECONTROLLER::getsingle(array("curricullum_ID"=>"ASC"),$value['ce_ID']);
                            if($getsingle['status']=='ENROLLED'){
                            echo "ENROLLED";
                            }else{
                            echo "NOT ENROLLED";
                            }
                            ?>
                        
                        <td><?php $getsingle = CECONTROLLER::getsingle(array("curricullum_ID"=>"ASC"),$value['ce_ID']);
                                                if($getsingle['status']=='ENROLLED'){
                            ?><a href="curricullum_enrolled.php?action=drop&id=<?= $value['ce_ID']; ?>" class='btn btn-danger btn-xs'>Drop</a>
                                                <?php
                                                }else{
                            ?><a href="curricullum_enrolled.php?action=enroll&id=<?= $value['ce_ID']; ?>" class='btn btn-success btn-xs'>Enroll</a>
                            <?php                    }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
                                        <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                                        </center>
							     </div>
                            </div>
						</div>
                            
					</div>
                </div>
                  <?php } break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                	
				    
                 </div>

				    
				</div>
                
				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
            

			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
</html>
