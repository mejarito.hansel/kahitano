<?php
include('init.php');
				$id = clean($_GET['id']);
				$accinfo =  SUBJENROL::getenrollmentofstudentpersem($_GET['id'],$_GET['sem_ID']);
				$info =  USERS::viewSingleStudent($accinfo['si_ID']);
				#var_dump($info);

				$mygrade = SUBJENROL::gradebyid($id);
				#var_dump($mygrade);
				
				$po_info = SUBJSCHED::getPaymentOptionByID($accinfo['po_ID']);
				
				?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cadet Schedule Card</title>
<style>
	
body{
	width: 8.5in;
	height: 5.5in;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
    }

.style1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style3 {	font-size: 18px;
	font-weight: bold;
}
.style4 {	font-size: 14px;
	font-weight: bold;
}
.style5 {font-size: 10px}
.style6 {
	font-family: Georgia, "Times New Roman", Times, serif;
	font-weight: bold;
	font-size: 24px;
}
.style7 {font-style: italic}
</style>
</head>

<body onload="window.print();">
<table width="100%" border="0" align="right" cellpadding="3" cellspacing="0" style="border-bottom-color:#000000; border-bottom-style:dashed; border-bottom-width:thin;">
 
  <tr>
    <td width="15%" align="right" valign="top">&nbsp;</td>
    <td width="60%" align="right" valign="top"><table width="100%" border="0" align="center" cellpadding="1" cellspacing="1" >
      <tr>
        <td width="10%"  ><div align="center"><img src="<?php echo HOME; ?>assets/images/site_logo.png" alt="" width="75" height="75" /></div></td>
        <td width="90%"  ><div align="center"><span class="style1"><span class="style3">MOL Magsaysay  </span><br />
              <span class="style4">Maritime Academy</span><br />
                  <em><strong>&nbsp;</strong></em><br />
          <span class="style5">Tel: (046) 454-2187 | Website: www.mmma.com.ph</span></span></div></td>
      </tr>
    </table></td>
    <td width="25%" align="right" valign="middle"><div align="center" class="style1">
      <p><span class="style7"><strong>Date Enrolled:</strong><br />
          <?= date("F j, Y",strtotime($accinfo['date_enrolled'])); ?>
      </span></p>
      </div></td>
  </tr>
  <tr>
    <td colspan="3" valign="top"><table width="95%" border="0" align="center" cellpadding="0" cellspacing="1" >
      <tr>
        <td width="12" class="style1"><strong>Cadet No</strong>.: <?php echo strtoupper($info['student_ID']); ?></td>
        <td width="45%" class="style1"><strong>School Year/Semester:</strong>
              <?php
					$sem = SEM::getSingleSem(clean($_GET['sem_ID']));

					echo $sem_NAME = $sem['sem_NAME'];
					 $sem_ID = $_GET['sem_ID'];
					?></td>
      </tr>
      <tr>
        <td class="style1"><strong>Name:</strong> <?php echo strtoupper($info['si_LNAME']).", ".strtoupper($info['si_FNAME'])." ".strtoupper($info['si_MNAME']);  ?> </td>
        <td class="style1"><strong>Course:</strong> <?php echo USERS::getFullCourse($info['si_ID']) ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="3" align="center" valign="top" class="style6">Registration Card</td>
  </tr>
  <tr>
    <td colspan="3"><table width="95%" align="center" cellpadding="0" cellspacing="1" class="" >
      <tr>
        <th width="9%" class="style1"><div align="left">Code</div></th>
        <th width="30%" class="style1"><div align="left">Description</div></th>
        <th width="9%" class="style1"><div align="center">Units</div></th>
        <th width="18%" class="style1" ><div align="center">Section</div></th>
        <th width="9%" class="style1" ><div align="center">Room</div></th>
        <th width="8%" class="style1"><div align="center">Day</div></th>
        <th width="17%" class="style1" style=""><div align="left">Time</div></th>
        </tr>
      <?php
			   	$mysubjects = SUBJENROL::mysubjects($accinfo['si_ID'],$sem_ID);
				#var_dump($mysubjects);
			   
			   ?>
      <?php foreach($mysubjects as $key => $val) { ?>
      <?php if($val['status'] == 1){ ?>
      <tr>
        <td class="style1" valign="top"><?php 
                   			 $ss = SUBJSCHED::getSched($val['subject_sched_ID']);
							 $s = SUBJECTS::getID($ss['subject_ID']);
                             echo $s['subject_CODE'];
                   		 ?>        </td>
        <td valign="top" class="style1"><?= $s['subject_DESCRIPTION']; ?></td>
        <td valign="top" class="style1"><div align="center">
          <?= $s['LEC_UNIT']+$s['LAB_UNIT']; ?>.00
        </div></td>
        <td valign="top" class="style1"><div align="center">
              <?php $sem = SUBJSCHED::getBySubjectSchedID2($val['subject_sched_ID']); 
							$course = COURSE::getbyID($sem['course_ID']);
							 echo $course['course_INIT'];  ?>-<?php  echo $sem['semester_section_NAME']; ?>
        </div></td>
        <?php
                            #var_dump($sem);
							?>
        <td valign="top" class="style1"><div align="center">
          <?php  $room = ROOM::getID($ss['room_ID']); echo $room['room_NAME']; ?><?php if($ss['room_ID2'] != NULL){ ?>/<br />
                                                                                                 <?php
                                                                                                     $r = ROOM::getID($ss['room_ID2']);
                                                                                                    echo $r['room_NAME'];
																									}
																									?>
        </div></td>
        <td valign="top" class="style1"><div align="center"><?php echo substr($ss['day'],0,3); ?><?php if($ss['day2'] != NULL){ ?>/<br /><?php echo substr($ss['day2'],0,3); ?><?php } ?></div></td>
        <td valign="top" class="style1"><?php echo $ss['start']."-".$ss['end']; ?><?php if($ss['start2'] != NULL){ ?>/<br />
          <?php echo $ss['start2']."-".$ss['end2']; ?><?php } ?></td>
        </tr>
      <?php } ?>
      <?php } ?>
    </table></td>
  </tr>
  <tr>
  	<td colspan="3"><table width="80%" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="50%" valign="top"><?php
				 
	
				 #var_dump($po_info);
				  
				    $po_lab_rate = $po_info['po_LAB'];
					$po_lec_rate = $po_info['po_LEC'];	
					$po_misc = $po_info['po_MISC'];
					
					$accinfo['subject_sched_ID'] = explode(",",$accinfo['subject_sched_ID']);
					$length = sizeof($accinfo['subject_sched_ID']);
					$total_lec = 0;
					$total_lab = 0;
					$acc_ID = $accinfo['acc_ID'];
					
					for($i=0;$i<$length;$i++)
					{
						$subject_sched_ID = $accinfo['subject_sched_ID'][$i];
						$si_ID = $id;
						
							$subject_sched = SUBJSCHED::getID($subject_sched_ID);
							$subject_info = SUBJECTS::getID($subject_sched['subject_ID']);	
							
							$total_lec += $subject_info['LEC_UNIT'];
							$total_lab += $subject_info['LAB_UNIT'];
						
					}
					
				
					
					
					$assess = explode(",",$accinfo['af_ID']);
					 $y = count($assess);
					 $afee = 0;
					 for($x=0;$x<$y;$x++)
					 {
						$af = SUBJSCHED::getFAbyID($assess[$x]);
						$afee += $af['af_PRICE'];
					 }	
				
				$grad_fee = 0;	
				
					
				  if($accinfo['gf_ID'] != '')
				  {
					$gf = SUBJSCHED::getGFbyID($accinfo['gf_ID']);
					$grad_fee = $gf['gf_AMOUNT'];
				  }		
				  
				  
				   $thesis_fee = 0;		
					  if($accinfo['tf_ID'] != '')
					  {
						$gf = SUBJSCHED::getTFbyID($accinfo['tf_ID']);
						$thesis_fee = $gf['tf_AMOUNT'];
					  }		
					
				        $lab_fee = $total_lab*$po_lab_rate;
						$lec_fee = $total_lec*$po_lec_rate;
						
 
  	$install_fee = $po_info['po_installment_fee'];
 
		 $discount_tf = ($lab_fee+$lec_fee)*$po_info['po_discount_tf'];
 
 		$discount_mf = ($po_misc)*$po_info['po_discount_misc'];
  
 		$additional_tf = ($lab_fee+lec_fee)*$po_info['po_additional_tf'];
  
 	     $additional_mf = ($po_misc)*$po_info['po_additional_misc'];
 		 $total_tf = $thesis_fee + $grad_fee + $afee + $po_misc + $lab_fee + $lec_fee - ($discount_tf + $discount_mf) + ($additional_tf + $additional_mf) + $install_fee;
  
				 ?>
            <table width="80%" align="center" cellpadding="1" cellspacing="0" class="style1">
              <tr>
                <td width="">Payment Type:</td>
                <td align="right"><?= $po_info['po_NAME']; ?></td>
              </tr>
              <tr>
                <td>Tuition Fee:</td>
                <td align="right"><?= money(($lab_fee+$lec_fee)+additional_tf);  ?></td>
              </tr>
              <?php if($install_fee > 0) { ?>
              <tr>
                <td>Installment Fee:</td>
                <td align="right"><?= money($install_fee); ?></td>
              </tr>
              <?php } ?>
              <tr>
                <td>Miscellaneous Fee:</td>
                <td align="right"><?= money($po_misc+additional_mf); ?></td>
              </tr>
              <?php if(($discount_mf+$discount_tf) > 0) { ?>
              <tr>
                <td>Discount:</td>
                <td align="right"><?= money($discount_mf+$discount_tf); ?></td>
              </tr>
              <?php } ?>
              <?php if($grad_fee > 0) { ?>
              <tr>
                <td>Graduation Fee:</td>
                <td align="right"><?= money($grad_fee); ?></td>
              </tr>
              <?php } ?>
              <?php if($thesis_fee > 0) { ?>
              <tr>
                <td>Thesis Fee:</td>
                <td align="right"><?= money($thesis_fee); ?></td>
              </tr>
              <?php } ?>
              <?php if($afee > 0) { ?>
              <tr>
                <td>Assessment Fee:</td>
                <td align="right"><?= money($afee); ?></td>
              </tr>
              <?php } ?>
              <tr> </tr>
              <tr>
                <td>Total Tuition Fee:</td>
                <td align="right"><?= money($total_tf);
					
					
					$down = 0; 
					
					$bal =$total_tf-$down;
					
					?>
                    <?php
				if($po_info['po_DATE1'] == NULL)
				{
				$slice = 1;
				}else if($po_info['po_DATE2'] == NULL){
				$slice = 2;
				}else if($po_info['po_DATE3'] == NULL){
				$slice = 3;
				}else if($po_info['po_DATE4'] == NULL){
				$slice = 4;
				}else if($po_info['po_DATE5'] == NULL){
				$slice = 5;
				}
				
				$slice -= 1;
				#msgbox($slice);
				
				
				
					
					
					
				?></td>
              </tr>
            </table></td>
          <td valign="top"><table width="90%" align="center" cellpadding="1" cellspacing="0" class="style1">
            <?php if($accinfo['or_ID'] != ''){ ?>
            <?php $d = ACCT::checkORData($accinfo['or_ID'],$id,$sem_ID); 


				 ?>
            <tr>
              <td>Downpayment:</td>
             
              <td colspan="2" width="32%"  align="right"><div align="left">
                 (OR#<?= $accinfo['or_ID'] ?>) <?php   $down = $d['payment_AMOUNT'];
		  $bal =$total_tf-$down;
		
						echo number_format($down,2, '.', ','); ?>
              </div></td>
            </tr>
            <?php } ?>
            <tr>
              <td valign="top">Balance:
                <?php $bal =  ($total_tf-$down); ?></td>
              
              <td align="right"><div align="left"><strong><?php echo number_format($bal,2, '.', ','); ?></strong> </div></td>
            </tr>
            <?php
				if($slice == 1)
				{
				$p1 = $bal;
				}else if($slice == 2)
				{
				$p1 = floor($bal/2);
				$p2 = $bal-$p1;
				}else if($slice == 3)
				{
				
					$p1 = floor($bal/3);
					$p2 = floor(($bal-$p1) / 2);
					$p3 = $bal - ($p1+$p2);
				}else if($slice == 4)
				{
					$p1 = floor($bal/4);
					$p2 = floor(($bal-$p1) / 3);
					$p3 = $p2;
					$p4 = $bal-($p1+$p2+$p3);
				}	
				?>
            <?php if($slice >= 1){ ?>
            <tr>
              <td width="40%">First Payment:</td>
              <td><?php echo date("m/j/Y",strtotime($po_info['po_DATE1'])); ?></td>
              <td width="28%"><strong>- P<?php echo number_format($p1,2, '.', ','); ?></strong></td>
            </tr>
            <?php } ?>
            <?php if($slice >=2 ){ ?>
            <tr>
              <td width="40%">Second Payment:</td>
              <td><?php echo date("m/j/Y",strtotime($po_info['po_DATE2'])); ?></td>
              <td><strong>- P<?php echo number_format($p2,2, '.', ','); ?></strong></td>
            </tr>
            <?php } ?>
            <?php if($slice >=3 ){ ?>
            <tr>
              <td width="40%">Third Payment:</td>
              <td><?php echo date("m/j/Y",strtotime($po_info['po_DATE3'])); ?></td>
              <td><strong>- P<?php echo number_format($p3,2, '.', ','); ?></strong></td>
            </tr>
            <?php } ?>
            <?php if($slice >=4 ){ ?>
            <tr>
              <td width="40%">Fourth Payment:</td>
              <td><?php echo date("m/j/Y",strtotime($po_info['po_DATE4'])); ?></td>
              <td><strong>- P<?php echo number_format($p4,2, '.', ','); ?></strong></td>
            </tr>
            <?php } ?>
            <tr>
        	<td colspan="3"><i>Note: Downpayment is non-refundable</i><br>
            </td>
        </tr>    
          </table></td>
        </tr>
        
      </table>    </td>
  </tr>  
  
  <tr>
    <td colspan="3">
    
    
    <div>
        <div align="right" style="float:left; width:300px">
          <p align="center"><em class="style1">
          Released By:
            <b><?= $user['account_FNAME']." ".$user['account_LNAME']; ?>
            </b>
              <br />
                  Admin Staff
          </em></p>
        </div>
        <div style="float:right" align="right">
        	<div align="center" >
          		<div align="right"><strong><em><br />Student's Copy</em></strong></div>
            </div>    
        </div>
  </div>      
        </td>
  </tr>
  <tr>
  	<!-- <td colspan="3" align="center" class="style1">*** Start of Class on June 15, 2015 ***</td> -->
  </tr>
</table>

<br />
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
