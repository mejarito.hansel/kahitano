<?php 
//INITIALIZE INCLUDES
	include('init.php');
SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
				#var_dump(Request::post());
           		ACCT::addDeposit(Request::post(),$user['account_ID']);
			break;
			case "edit": 
				#var_dump(Request::post());
				ACCT::updateDeposit(Request::post(),$user['account_ID']);
			
			 break;
			#case "update_blog":  BLOG::update(Request::post("blog_info")); break;
			
			#case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
            #case "enroll": CECONTROLLER::enroll($_GET['id']); break;
		    #case "drop": CECONTROLLER::drop($_GET['id']); break;
			case "delete": ACCT::hideDeposit($_GET['id'],$user['account_ID']); break;
		    
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
        <script> 
        $(document).ready(function(){
          $("#flip").click(function(){
            $("#panel").slideToggle("fast");
          });
        });
            
        </script>
      <title>ACCOUNTING DEPOSITS | CSJPII</title>
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
			<!-- end of alert messages -->
			<!-- start row container -->
			<!-- 
            <div class="row">
				<div class="col-lg-12">
					<ul class="breadcrumb">
						<li class="active">Home</li>
					</ul>
            </div>	
			</div>
            -->
			<!-- end row container -->
            <!-- row container -->
			<div class="row">
				<!-- left nav -->   
            <?php
				if (!SESSION::isLoggedIn()) {
					?>
					
                <?php   }else{?>
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
                <?php } ?>
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Deposit</h3> 
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <!-- Name -->
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>DATE</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ad_DATE" value="<?= date("m/d/Y"); ?>"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" class='form-control datepicker' required  >
                                </div>
                            </div>
                           
                            
                            
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='ad_AMOUNT' class=''>AMOUNT</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ad_AMOUNT" autocomplete="off"   class='form-control' required  >
                                </div>
                            </div>
							
							<div class="form-group">
                                <div class="col-sm-4">
                                    <label for='ad_file' class=''>ATTACHED DOCUMENT</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" name="ad_FILE" class='form-control' required  >
                                </div>
                            </div>
							
                            <div class="row form-group">
                                <div class="col-sm-4">
                                    <label for="si_FNAME" class="">NOTE</label> 
                                </div>             
                                <div class="col-sm-8">
                                    <textarea name="ad_NOTE" class="form-control"></textarea>
                                </div> 
                            </div>
                           
                            
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button  class='btn btn-success btn-block' type='submit' ><i class='fa fa-edit'></i> Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":
                        $siiid =$_GET['id'];
                        $getsingle = ACCT::getSingleDeposit($siiid);
						#print_r($getsingle);
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Edit Accounting Deposit</h3> 
                    </div>
                    <div class="panel-body">
                         <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <!-- Name -->
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>DATE</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ad_DATE" value="<?= date("m/d/Y",strtotime($getsingle['ad_DATE'])); ?>"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" class='form-control datepicker' required  >
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>AMOUNT</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ad_AMOUNT" value="<?= $getsingle['ad_AMOUNT']; ?>" autocomplete="off"   class='form-control' required  >
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-4">
                                    <label for="si_FNAME" class="">NOTE</label> 
                                </div>             
                                <div class="col-sm-8">
                                    <textarea name="NOTE" class="form-control"><?= $getsingle['ad_NOTE']; ?></textarea>
                                </div> 
                            </div>
                           <div class="form-group">
                                <div class="col-md-12">
                                
                                <input type="hidden" value="<?= $getsingle['ad_ID']; ?>" name="ad_ID" />
                                
                                    <button  class='btn btn-success btn-block' type='submit' ><i class='fa fa-edit'></i> Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
						include "searching.php";						
						?>
							
                    <?php break; 
                        
                        case "view":?>
                        <?php
								if(isset($_GET['id']))
								{
                                }else{
					?>
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table>
                                <tr>
                                <td width="50%">
                                    <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                    <i class="fa fa-folder-o"></i> View Accounting Deposits</h3> 
                                    <sub><a id="daily" style="cursor:pointer;">[Daily Report]</a></sub> <sub><a style="cursor:pointer;" id="monthly">[Monthly Report]</a></sub> <!--<sub>[Custom Report]</sub>-->
                                    
                                    
                                    
                                    
                                    
                                    <div id="dialog" style="display:none;">
                                        <form id="" action="deposit_report.php?action=daily" method="POST" target="_blank">
                                            <input type="hidden" autofocus />
                                            <b> Date:</b>   <input type="text" value="<?= date("m/d/Y"); ?>" class="datepicker"  name="date" />
                                            <br /><br />
                                            <input type="hidden" name="show" id="tags" /> <input type="submit" value="Generate" />
                                        </form>
                                    </div>
                                    
                                    
                                    <div id="dialog1" style="display:none;">
                                        <form id="" action="deposit_report.php?action=monthly" method="POST" target="_blank">
                                            <input type="hidden" autofocus />
                                            <b> Month:</b>   <input type="text" value="<?= date("F Y"); ?>" class="monthPicker"  name="date" />
                                              <br /><br />
                                              
                                            
                                          
                                      <br />
                                             
                                            <input type="hidden" name="show" id="tags2" /> <input type="submit" value="Generate" />
                                        </form>
                                    </div>
                                    
                                    
                                    
                                    
                                    <script type="text/javascript">
										$(document).ready(function () {
										
										
										
										
										$('#selecctall').click(function(event) {  //on click 
											if(this.checked) { // check select status
												$('.cb').each(function() { //loop through each checkbox
													this.checked = true;  //select all checkboxes with class "checkbox1"               
												});
												Populate()
												
											}else{
												$('.cb').each(function() { //loop through each checkbox
													this.checked = false; //deselect all checkboxes with class "checkbox1"                       
												});         
												Populate()
											}
										});
										
										
											
										$('#selecctall2').click(function(event) {  //on click 
											if(this.checked) { // check select status
												$('.cb2').each(function() { //loop through each checkbox
													this.checked = true;  //select all checkboxes with class "checkbox1"               
												});
												Populate2()
												
											}else{
												$('.cb2').each(function() { //loop through each checkbox
													this.checked = false; //deselect all checkboxes with class "checkbox1"                       
												});         
												Populate2()
											}
										});
										
										
										
										
										
											$("#dialog").dialog({ autoOpen: false });
											
											
											
											$("#dialog1").dialog({ autoOpen: false });
									 
											$("#daily").click(
												function () {
													$("#dialog").dialog({ title: 'Daily Report' });
													$("#dialog").dialog('open');
													$("#dialog1").dialog('close');
													return false;
												}
											);
											
											$("#monthly").click(
												function () {
													$("#dialog1").dialog({ title: 'Monthly Report' });
													$("#dialog1").dialog('open');
													$("#dialog").dialog('close');
													return false;
												}
											);
												
										
										
										
										/* MONTH PICKER */
										$(".monthPicker").datepicker({
												dateFormat: 'MM yy',
												changeMonth: true,
												changeYear: true,
												showButtonPanel: true,
										
												onClose: function(dateText, inst) {
													var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
													var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
													$(this).val($.datepicker.formatDate('MM yy', new Date(year, month, 1)));
												}
											});
										
											$(".monthPicker").focus(function () {
												$(".ui-datepicker-calendar").hide();
												$("#ui-datepicker-div").position({
													my: "center top",
													at: "center bottom",
													of: $(this)
												});
											});
										
										/* MONTH PICKER */
										
										
										
										
										
											
										});
										
										
										
										
										
										
										
										
										function Populate(){
											vals = $('input[class="cb"]:checked').map(function() {
												return this.value;
											}).get().join(',');
											console.log(vals);
											$('#tags').val(vals);
										}
										
										$('input[class="cb"]').on('change', function() {
											Populate()
										}).change();
										
										
										
										function Populate2(){
											vals = $('input[class="cb2"]:checked').map(function() {
												return this.value;
											}).get().join(',');
											console.log(vals);
											$('#tags2').val(vals);
										}
										
										$('input[class="cb2"]').on('change', function() {
											Populate2()
										}).change();
										
										
										
									</script>
									
                                    
                                    
                                </td>
                               
                                <td >
                                    <div class="input-group col-md-12">
                                      <!--
                                      	<form id="form1" runat="server">
                                        		<input id="searchItem" class="searchItem form-control"  name="q" type="text" onkeyup="term(q.value)" autofocus="autofocus" onchange="term(q.value)"/>
                                        </form>
                                      
                                        <script>
                                    		$(document).ready(function() {
											
												
												
													
												$('#searchItem').keyup(function(){
												var s_Item = $('#searchItem').val();
													
													if(s_Item.length >= 3)
													{
														alert(1);
														 $.ajax({
															type: "POST",
															url: "acct_exp.php",
															data: 'search_term=' + s_Item,
																success: function(msg){
																	/* $('#resultip').html(msg); */
																		$("#display_result").show();
																		$("#display_result").html(msg);
																		
																		
																		$("#display_hide").hide();
																		$("#pagination").hide();
																	
																}
											
															}); // Ajax Call
													
															//alert(s_Item);
														}else{
															$("#display_result").hide();   
															
														}
													});
												});			
                                    			</script>
                                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
									-->
                                </td>
                                
                                <td width="5%">
                                <div class="col-md-1">   
                                <button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('?action=add','_self');"><i class='fa fa-plus'></i></button>
                                </div>
                                </td>
                                </tr>
                                </table>
                            </div>
							<div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
								<table class="table table-hover table-responsive table-striped text-md">
									  <th style="width:15%;">Date</th>
									    <th style="width:15%;">Amount</th>
									    <th style="width:15%;">Note</th>
									    <th style="width:15%;">Attached Document</th>
									    <th style="width:15%;">Operation</th>
                                    
                                    <?php
                                   
                                    $getDep = ACCT:: getAllDepositActive(array("ad_DATE"=>"DESC"));
									
									
									
                                     if(count($getDep)>=10) 
                                     {
                                         $pagination2 = new Pagination($getDep, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($getDep, 10, NULL);
                                     }

                                     $getExpense = $pagination2->get_array();
                                     if($getExpense) {
                                        foreach($getExpense as $key => $value){
											
											
									?>
									
											
                                    
                                        <tr>
                                          <td>
                                          <?= date("m/d/Y",strtotime($value['ad_DATE'])); ?>
                                          <td>
                                          <?= money($value['ad_AMOUNT']); ?>
                                          <td>
                                         <?= $value['ad_NOTE']; ?>
                                          
										  <td align="center">
										  <a href="images/deposit/<?= $value['ad_FILE']; ?>" target="_blank">Click Here</a>
										  
										  <td>
                                         <a href="?action=edit&id=<?= $value['ad_ID']; ?>">Edit</a> | <a onclick="return confirm('Are you sure to remove this invalid entry?')" href="?action=delete&id=<?= $value['ad_ID']; ?>">Invalid Entry</a>
                                          </td>
                                        </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
                                        <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                                        </center>
							     </div>
                            </div>
						</div>
                            
					</div>
                </div>
                  <?php } break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
                	
			
			<!-- end row container -->
            
            

			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
</html>
