<?php 
//INITIALIZE INCLUDES
    include('init.php');
SESSION::CheckLogin();

  
   
    
    if(Request::get()){
        switch(Request::get("action")) {
            #case "delete": ANNOUNCEMENT::delete($_GET['id']); break;
        
        }
    }       
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      
      <title>DEPOSIT REPORT | CSJPII</title>
      <style>
	  body
	  {
	  font-family:Verdana, Arial, Helvetica, sans-serif;
	  font-size:12px;
	  }
	  </style>
    </head>

    <body>

     
        <div class="container">
            
         
            <!-- row container -->
            <div class="row">

                <!-- left nav -->
         
                
                <!-- BODY -->
                
                
                <div class="col-md-9">
                    <div class="panel panel-default">
                    
                    
                    <?php
                 switch($_GET['action']){
                        case "daily":?>   
                    
                    
                    		<?php
								
							$active = ACCT::getAllActiveDepositByDate(Request::post());
							#var_dump($active);
							?>
                    
                            <div class="panel-heading">
                                <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> DAILY DEPOSIT REPORT </h3>
                                <b>Date: <?= date("F j, Y", strtotime($_POST['date'])); ?></b><br />&nbsp;
                            </div>
                            
                            <div class="panel-body">
                                <table style="font-family:verdana;  font-size:12px" width="500" border="2" cellspacing="0" cellpadding="5" class="">
                              <tr style="font-weight:bold;">
                                    <td width="10">#</th>
                                   
                                    <td style="width:200;" align="center">Amount</th>
                                    <td style="" align="center">Note</th>  
                                <?php $i=1;
										$amt = 0;
								
								  foreach($active as $key => $val){ ?>    
                                <tr>
                                	<td><?= $i++; ?>
                                	                            
                                	<td  align="right">
									
									<?= 
									money($val['ad_AMOUNT']);
										#sprintf("%.2f",$val['ae_AMOUNT']);
									
									$amt += $val['ad_AMOUNT'];
									
									 ?>                                    
                                	<td align="right"><?= $val['ad_NOTE']; ?>                                                                                                        
                                <?php } ?>    
                                <tr>
                                	<td>
                                    
                                    <td align="right"><b>
									
								
                                    
                                    <?php echo money($amt);
									?>		
                                    
                                    </b>
                                    <td>
                                </table> 
							<br /><b>Prepared By:</b><br /><br />
                            <?= $user['account_FNAME']; ?>  <?= $user['account_LNAME']; ?>
                            <br />
                             <?= $user['access_NAME']; ?>
                            <br /> <br />
                            <i>Printed: <?= date("F j, Y"); ?></i>                            </div>	
                        </div>
						
                        </div>
                       <?php
					   break;
					   case 'monthly': 
					   
				
					   
							# print_r(Request::post());
							   $date = $_POST['date'];
							  # echo $date;
							   #echo date("m/1/Y",strtotime($date));
							   
							   $by = $_POST['by'];
							   
							   $number = cal_days_in_month(CAL_GREGORIAN, date("m",strtotime($date)), date("Y",strtotime($date))); // 31
							?>
							
							
                            
                            
                            
                            
                                <?php 
										$month = date("Y-m",strtotime($date));
										
									
							
        
			  $start = 0;
			  $sum = 0;
			  ?>              
                     
                     
                     
                                                       
                    
<!-- START -->              
                            <div class="panel-heading">
                                <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-folder-o"></i> MONTHLY DEPOSIT REPORT </h3>
                                <b>Date: <?= date("F j-$number, Y", strtotime($_POST['date'])); ?></b>
                                
                                
              				</div>
                            
							<table width="" border="1" cellspacing="0" cellpadding="1" style=" font-family: verdana;font-size:12px; ">
								<tr>
									<td width="20"  align="center">
                                    	<b><?= date("M",strtotime($date)); ?><br /><?= date("Y",strtotime($date)); ?></b>                                    </td>
									
                                    
                                 
                               		<td width="200"  align="center"><b>Amount</b>
                                  
                                         <!-- TOTAL -->
								</tr>
                                
                                
                              <!--DATA -->  

                             <?php for($x=1;$x<=$number;$x++) { ?>
                             <tr>
                             	<td align="center"><b><?= $x; ?></b>
                                <td align="right"><?php 
								
								$day = ACCT::getSUMActiveDepositByDate(array('date'=>date("Y-m-d",strtotime($month."-$x"))));   
								if(is_null($day['SUM']))
								{
								}else{
								echo money($day['SUM']);
								$sum += $day['SUM'];
								}
								?>
                                
                             <?php
                              }
                              ?>  
                              <tr>
                              	<td colspan="2">
								<span style="float:left">
                                
                                 <b>Total Deposit:</b>
                                
                                </span>
                                <span style="float:right">
                                	<b><?= money($sum); ?></b>
                                </span>
                               </table>                
			 
            
             
        
					
			 
              
              			   
    <?php    
					   break;
					   
					}
					   ?> 
                        
                       
                    
                </div>
                </div>
            <!-- end row container -->
            
            

            <!-- footer-->      
            
          

        </div>
        <!-- end container -->

    </body>
</html>
