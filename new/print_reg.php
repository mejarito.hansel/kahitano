<?php
include('init.php');
				$id = clean($_GET['id']);
				$accinfo =  SUBJENROL::getaccrecord($_GET['id']);
				$info =  USERS::viewSingleStudent($accinfo['si_ID']);
				#var_dump($info);

				$mygrade = SUBJENROL::gradebyid($id);
				#var_dump($mygrade);
				?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cadet Schedule Card</title>
<style>
	
body{
	width: 8.5in;
	height: 5.5in;
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
    }

.style1 {	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style3 {	font-size: 18px;
	font-weight: bold;
}
.style4 {	font-size: 14px;
	font-weight: bold;
}
.style5 {font-size: 10px}
.style6 {
	font-family: Georgia, "Times New Roman", Times, serif;
	font-weight: bold;
	font-size: 24px;
}
</style>
</head>

<body>
<table width="100%" border="0" align="right" cellpadding="3" cellspacing="0" style="border-bottom-color:#000000; border-bottom-style:dashed; border-bottom-width:thin;">
  <tr>
    <td colspan="2" align="right" valign="top"><table width="50%" border="0" align="center" cellpadding="1" cellspacing="1" >
      <tr>
        <td width="25%"  ><div align="center"><!-- <img src="assets/img/new_csjp.gif" alt="" width="75" height="75" /> --></div></td>
        <td width="75%"  ><div align="center"><span class="style1"><span class="style3">College of Saint John Paul II </span><br />
                    <span class="style4">Arts and Sciences</span><br />
<em><strong>(Formerly SJB IAS Cainta)</strong></em><br />
                    <span class="style5">Tel: (02)655-2171 | Website: www.csjpii.edu.ph</span></span></div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2" valign="top"><table width="95%" border="0" align="center" cellpadding="0" cellspacing="1" >
      <tr>
        <td width="12" class="style1"><strong>Student No</strong>.: <?php echo strtoupper($info['student_ID']); ?></td>
        <td width="45%" class="style1"><strong>School Year/Semester:</strong>
              <?php
					$sem = SEM::getSingleSem(clean($_GET['sem_ID']));

					echo $sem_NAME = $sem['sem_NAME'];
					 $sem_ID = $_GET['sem_ID'];
					?></td>
      </tr>
      <tr>
        <td class="style1"><strong>Name:</strong> <?php echo strtoupper($info['si_LNAME']).", ".strtoupper($info['si_FNAME'])." ".strtoupper($info['si_MNAME']);  ?> </td>
        <td class="style1"><strong>Course:</strong> <?php echo USERS::getFullCourse($info['si_ID']) ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="top" class="style6">Registration Card</td>
  </tr>
  <tr>
    <td colspan="2"><table width="95%" align="center" cellpadding="0" cellspacing="1" class="" >
      <tr>
        <th width="9%" class="style1"><div align="left">Code</div></th>
        <th width="30%" class="style1"><div align="left">Description</div></th>
        <th width="9%" class="style1"><div align="center">Units</div></th>
        <th width="19%" class="style1" ><div align="center">Section</div></th>
        <th width="9%" class="style1" ><div align="center">Room</div></th>
        <th width="8%" class="style1"><div align="center">Day</div></th>
        <th width="17%" class="style1" style=""><div align="left">Time</div></th>
        </tr>
      <?php
			   	$mysubjects = SUBJENROL::mysubjects($accinfo['si_ID'],$sem_ID);
				#var_dump($mysubjects);
			   
			   ?>
      <?php foreach($mysubjects as $key => $val) { ?>
      <?php if($val['status'] == 1){ ?>
      <tr>
        <td class="style1" valign="top"><?php 
                   			 $ss = SUBJSCHED::getSched($val['subject_sched_ID']);
							 $s = SUBJECTS::getID($ss['subject_ID']);
                             echo $s['subject_CODE'];
                   		 ?>        </td>
        <td class="style1"><?= $s['subject_DESCRIPTION']; ?></td>
        <td class="style1"><div align="center">
          <?= $s['LEC_UNIT']+$s['LAB_UNIT']; ?>.00
        </div></td>
        <td class="style1"><div align="center">
              <?php $sem = SUBJSCHED::getBySubjectSchedID2($val['subject_sched_ID']); 
							$course = COURSE::getbyID($sem['course_ID']);
							 echo $course['course_INIT'];  ?>-<?php  echo $sem['semester_section_NAME']; ?>
        </div></td>
        <?php
                            #var_dump($sem);
							?>
        <td class="style1"><div align="center">
          <?php  $room = ROOM::getID($ss['room_ID']); echo $room['room_NAME']; ?>
        </div></td>
        <td class="style1"><div align="center"><?php echo substr($ss['day'],0,3); ?></div></td>
        <td class="style1"><?php echo $ss['start']."-".$ss['end']; ?></td>
        </tr>
      <?php } ?>
      <?php } ?>
    </table></td>
  </tr>
  <tr>
    <td><table align="right" cellpadding="1" cellspacing="0" class="style1">
      <tr>
        <td width="">Payment Type:</td>
        <td align="right"><?php echo $accinfo['payment_option']; ?></td>
      </tr>
      <tr>
        <td>Tuition Fee:</td>
        <td align="right"><?php 
					$amount = $accinfo['lec_amount'] + $accinfo['lab_amount'];
					echo number_format($amount, 2, '.', ',') ?>        </td>
      </tr>
     
      <?php if($accinfo['payment_option'] == 'INSTALLMENT'){ ?>
      <tr>
        <td>Installment Fee:</td>
        <td align="right"> 500.00</td>
      </tr>
	 <?php }else if ($accinfo['payment_option'] == 'CASH'){ ?>   
     <tr>
        <td>Discount (10%):</td>
        <td align="right"> <?php echo number_format(($amount+$accinfo['misc'])*.10,2, '.', ','); ?></td>
        </tr>
     <?php }else if ($accinfo['payment_option'] == 'S30'){ ?>   
     <tr>
        <td>Discount (Scholarship 30%):</td>
        <td align="right"> <?php echo number_format(($amount)*.30,2, '.', ','); ?></td>
        </tr>
     <?php }else if ($accinfo['payment_option'] == 'S50'){ ?>   
     <tr>
        <td>Discount (Scholarship 50%):</td>
        <td align="right"> <?php echo number_format(($amount)*.50,2, '.', ','); ?></td>
        </tr>
     <?php }else if ($accinfo['payment_option'] == 'S70'){ ?>   
     <tr>
        <td>Discount (Scholarship 70%):</td>
        <td align="right"> <?php echo number_format(($amount)*.70,2, '.', ','); ?></td>
        </tr>
     <?php } else if ($accinfo['payment_option'] == 'S100'){ ?>   
     <tr>
        <td>Discount (Scholarship 100%):</td>
        <td align="right"> <?php echo number_format(($amount),2, '.', ','); ?></td>
    	</tr>
     <?php } ?> 
       <tr>
        <td>Miscellaneous Fee:</td>
        <td align="right"><?php echo number_format($accinfo['misc']	, 2, '.', ',') ?> </td>
      </tr>
      
      <?php if($accinfo['thesis_fee'] > 0){ ?>
                 <tr>
                 	<td>Thesis Fee:</td>
                    <td align="right"> <?php echo $thesis_fee = number_format($accinfo['thesis_fee']	, 2, '.', ',') ?>
                   </td>    
               </tr>   
               <?php }else{
			   	$thesis_fee = 0;
			   } 
			   ?>    
      
      
      
      
      
      <tr>
        
      </tr>
      <tr>
        <td>Total Tuition Fee:</td>
        <td align="right">
		<b>
		<?php 
		$total = 0;
		$total += $thesis_fee;
		?>
		
         <?php if($accinfo['payment_option'] == 'INSTALLMENT'){ ?>   
					 <?php  $total +=($amount+$accinfo['misc'])+500.00;
                            echo number_format($total,2, '.', ','); ?>
    	             <?php }else if ($accinfo['payment_option'] == 'CASH'){ ?>   
                     <?php $total += ($amount+$accinfo['misc'])*.90;
					 	echo number_format($total,2, '.', ','); ?>
                     <?php }else if ($accinfo['payment_option'] == 'S30'){ ?>   
                     <?php $total += (($amount)*.70)+$accinfo['misc'];
					 	echo number_format($total,2, '.', ','); ?>
                     <?php }else if ($accinfo['payment_option'] == 'S50'){ ?>   
                     <?php $total += (($amount)*.50)+$accinfo['misc'];
					 	echo number_format($total,2, '.', ','); ?>
                     <?php }else if ($accinfo['payment_option'] == 'S70'){ ?>   
                     <?php $total += (($amount)*.30)+$accinfo['misc'];
					 	echo number_format($total,2, '.', ','); ?>
                     <?php }else if ($accinfo['payment_option'] == 'S100'){ ?>   
                   	 <?php $total += ($accinfo['misc']);
					 	echo number_format($total,2, '.', ','); ?>
                     <?php } ?>
				 
				 <?php
				 $down = 0;
				 
				 
				 
				 ?>
			 
             
             </b>
             
             
             
           </td>
      </tr>
    </table></td>
    <td width="58%" valign="top"><table width="80%" align="center" cellpadding="1" cellspacing="0" class="style1">
     
     
     
        <?php if($accinfo['down_OR'] != ''){ ?>        
      <?php $d = ACCT::checkORData($accinfo['down_OR']); 


				 ?>
      <tr>
        <td>Downpayment:</td>
        <td width="32%" align="right">
        <div align="left">
          <?php   $down = $d['payment_AMOUNT'];
						echo number_format($down,2, '.', ','); ?>
          </div>
  			</td>
      </tr>
      
      <?php } ?>
      
      <tr>
        <td valign="top">Balance:
          <?php $bal =  ($total-$down); ?></td>
        <td align="right"><div align="left"><strong><?php echo number_format($bal,2, '.', ','); ?></strong> </div></td>
      </tr>
      <?php if( ($bal != 0) and ($accinfo['payment_option'] == 'INSTALLMENT')  ){ ?>
      <tr>
        <?php
					$p1 = floor($bal/3);
					$p2 = floor(($bal-$p1) / 2);
					$p3 = $bal - ($p1+$p2);
				?>
        <td width="40%">First Payment:</td>
        <td><?php echo date("m/j/Y",strtotime($accinfo['payment1'])); ?></td>
        <td width="28%"><?php echo number_format($p1,2, '.', ','); ?></td>
      </tr>
      <tr>
        <td width="40%">Second Payment:</td>
        <td><?php echo date("m/j/Y",strtotime($accinfo['payment2'])); ?></td>
        <td><?php echo number_format($p2,2, '.', ','); ?></td>
      </tr>
      <tr>
        <td width="40%">Third Payment:</td>
        <td><?php echo date("m/j/Y",strtotime($accinfo['payment3'])); ?></td>
        <td><?php echo number_format($p3,2, '.', ','); ?></td>
      </tr>
      <?php } ?>
      
    </table></td>
  </tr>
  <tr>
    <td><div align="right">
      <p align="center"><em class="style1">Released By:
        <b><?= $user['account_FNAME']." ".$user['account_LNAME']; ?></b>
              <br />
              Admin Staff
      </em></p>
    </div>
        <div align="right"></div></td>
    <td><div align="center" style="width:90%">
      <div align="right"><strong><em>Student's Copy</em></strong></div>
    </div></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
</table>
<table width="100%" border="0" align="right" cellpadding="3" cellspacing="0" style="border-bottom-color:#000000; border-bottom-style:dashed; border-bottom-width:thin;">
  <tr>
    <td colspan="2" align="right" valign="top"><table width="50%" border="0" align="center" cellpadding="1" cellspacing="1" >
      <tr>
        <td width="25%"  ><div align="center"><!-- <img src="assets/img/new_csjp.gif" alt="" width="75" height="75" /> --></div></td>
        <td width="75%"  ><div align="center"><span class="style1"><span class="style3">College of Saint John Paul II </span><br />
                  <span class="style4">Arts and Sciences</span><br />
                  <em><strong>(Formerly SJB IAS Cainta)</strong></em><br />
                  <span class="style5">Tel: (02)655-2171 | Website: www.csjpii.edu.ph</span></span></div></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2" valign="top"><table width="95%" border="0" align="center" cellpadding="0" cellspacing="1" >
      <tr>
        <td width="12" class="style1"><strong>Student No</strong>.: <?php echo strtoupper($info['student_ID']); ?></td>
        <td width="45%" class="style1"><strong>School Year/Semester:</strong>
               <?php
					$sem = SEM::getSingleSem(clean($_GET['sem_ID']));

					echo $sem_NAME = $sem['sem_NAME'];
					 $sem_ID = $_GET['sem_ID'];
					?></td>
      </tr>
      <tr>
        <td class="style1"><strong>Name:</strong> <?php echo strtoupper($info['si_LNAME']).", ".strtoupper($info['si_FNAME'])." ".strtoupper($info['si_MNAME']);  ?> </td>
        <td class="style1"><strong>Course:</strong> <?php echo USERS::getFullCourse($info['si_ID']) ?></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2" align="center" valign="top" class="style6">Registration Card</td>
  </tr>
  <tr>
    <td colspan="2"><table width="95%" align="center" cellpadding="0" cellspacing="1" class="" >
      <tr>
        <th width="9%" class="style1"><div align="left">Code</div></th>
        <th width="34%" class="style1"><div align="left">Description</div></th>
        <th width="9%" class="style1"><div align="center">Units</div></th>
        <th width="14%" class="style1" ><div align="center">Section</div></th>
        <th width="9%" class="style1" ><div align="center">Room</div></th>
        <th width="8%" class="style1"><div align="center">Day</div></th>
        <th width="17%" class="style1" style=""><div align="left">Time</div></th>
      </tr>
      <?php
			   	$mysubjects = SUBJENROL::mysubjects($accinfo['si_ID'],$sem_ID);
				#var_dump($mysubjects);
			   
			   ?>
      <?php foreach($mysubjects as $key => $val) { ?>
      <?php if($val['status'] == 1){ ?>
      <tr>
        <td class="style1" valign="top"><?php 
                   			 $ss = SUBJSCHED::getSched($val['subject_sched_ID']);
							 $s = SUBJECTS::getID($ss['subject_ID']);
                             echo $s['subject_CODE'];
                   		 ?>
        </td>
        <td class="style1"><?= $s['subject_DESCRIPTION']; ?></td>
        <td class="style1"><div align="center">
          <?= $s['LEC_UNIT']+$s['LAB_UNIT']; ?>
          .00 </div></td>
        <td class="style1"><div align="center">
          <?php $sem = SUBJSCHED::getBySubjectSchedID2($val['subject_sched_ID']); 
							$course = COURSE::getbyID($sem['course_ID']);
							 echo $course['course_INIT'];  ?>
          -
          <?php  echo $sem['semester_section_NAME']; ?>
        </div></td>
        <?php
                            #var_dump($sem);
							?>
        <td class="style1"><div align="center">
          <?php  $room = ROOM::getID($ss['room_ID']); echo $room['room_NAME']; ?>
        </div></td>
        <td class="style1"><div align="center"><?php echo substr($ss['day'],0,3); ?></div></td>
        <td class="style1"><?php echo $ss['start']."-".$ss['end']; ?></td>
      </tr>
      <?php } ?>
      <?php } ?>
    </table></td>
  </tr>
  <tr>
    <td><table align="right" cellpadding="1" cellspacing="0" class="style1">
      <tr>
        <td width="">Payment Type:</td>
        <td align="right"><?php echo $accinfo['payment_option']; ?></td>
      </tr>
      <tr>
        <td>Tuition Fee:</td>
        <td align="right"><?php 
					$amount = $accinfo['lec_amount'] + $accinfo['lab_amount'];
					echo number_format($amount, 2, '.', ',') ?>
        </td>
      </tr>
      <?php if($accinfo['payment_option'] == 'INSTALLMENT'){ ?>
      <tr>
        <td>Installment Fee:</td>
        <td align="right"> 500.00</td>
      </tr>
      <?php }else if ($accinfo['payment_option'] == 'CASH'){ ?>
      <tr>
        <td>Discount (10%):</td>
        <td align="right"><?php echo number_format(($amount+$accinfo['misc'])*.10,2, '.', ','); ?></td>
      </tr>
      <?php }else if ($accinfo['payment_option'] == 'S30'){ ?>
      <tr>
        <td>Discount (Scholarship 30%):</td>
        <td align="right"><?php echo number_format(($amount)*.30,2, '.', ','); ?></td>
      </tr>
      <?php }else if ($accinfo['payment_option'] == 'S50'){ ?>
      <tr>
        <td>Discount (Scholarship 50%):</td>
        <td align="right"><?php echo number_format(($amount)*.50,2, '.', ','); ?></td>
      </tr>
      <?php }else if ($accinfo['payment_option'] == 'S70'){ ?>
      <tr>
        <td>Discount (Scholarship 70%):</td>
        <td align="right"><?php echo number_format(($amount)*.70,2, '.', ','); ?></td>
      </tr>
      <?php } else if ($accinfo['payment_option'] == 'S100'){ ?>
      <tr>
        <td>Discount (Scholarship 100%):</td>
        <td align="right"><?php echo number_format(($amount),2, '.', ','); ?></td>
      </tr>
      <?php } ?>
      <tr>
        <td>Miscellaneous Fee:</td>
        <td align="right"><?php echo number_format($accinfo['misc']	, 2, '.', ',') ?> </td>
      </tr>
      <?php if($accinfo['thesis_fee'] > 0){ ?>
      <tr>
        <td>Thesis Fee:</td>
        <td align="right"><?php echo $thesis_fee = number_format($accinfo['thesis_fee']	, 2, '.', ',') ?> </td>
      </tr>
      <?php }else{
			   	$thesis_fee = 0;
			   } 
			   ?>
      <tr> </tr>
      <tr>
        <td>Total Tuition Fee:</td>
        <td align="right"><b>
          <?php 
		$total = 0;
		$total += $thesis_fee;
		?>
          <?php if($accinfo['payment_option'] == 'INSTALLMENT'){ ?>
          <?php  $total +=($amount+$accinfo['misc'])+500.00;
                            echo number_format($total,2, '.', ','); ?>
          <?php }else if ($accinfo['payment_option'] == 'CASH'){ ?>
          <?php $total += ($amount+$accinfo['misc'])*.90;
					 	echo number_format($total,2, '.', ','); ?>
          <?php }else if ($accinfo['payment_option'] == 'S30'){ ?>
          <?php $total += (($amount)*.70)+$accinfo['misc'];
					 	echo number_format($total,2, '.', ','); ?>
          <?php }else if ($accinfo['payment_option'] == 'S50'){ ?>
          <?php $total += (($amount)*.50)+$accinfo['misc'];
					 	echo number_format($total,2, '.', ','); ?>
          <?php }else if ($accinfo['payment_option'] == 'S70'){ ?>
          <?php $total += (($amount)*.30)+$accinfo['misc'];
					 	echo number_format($total,2, '.', ','); ?>
          <?php }else if ($accinfo['payment_option'] == 'S100'){ ?>
          <?php $total += ($accinfo['misc']);
					 	echo number_format($total,2, '.', ','); ?>
          <?php } ?>
          <?php
				 $down = 0;
				 
				 
				 
				 ?>
        </b> </td>
      </tr>
    </table></td>
    <td width="58%" valign="top"><table width="80%" align="center" cellpadding="1" cellspacing="0" class="style1">
      <?php if($accinfo['down_OR'] != ''){ ?>
      <?php $d = ACCT::checkORData($accinfo['down_OR']); 


				 ?>
      <tr>
        <td>Downpayment:</td>
        <td width="32%" align="right"><div align="left">
          <?php   $down = $d['payment_AMOUNT'];
						echo number_format($down,2, '.', ','); ?>
        </div></td>
      </tr>
      <?php } ?>
      <tr>
        <td valign="top">Balance:
          <?php $bal =  ($total-$down); ?></td>
        <td align="right"><div align="left"><strong><?php echo number_format($bal,2, '.', ','); ?></strong> </div></td>
      </tr>
      <?php if( ($bal != 0) and ($accinfo['payment_option'] == 'INSTALLMENT')  ){ ?>
      <tr>
        <?php
					$p1 = floor($bal/3);
					$p2 = floor(($bal-$p1) / 2);
					$p3 = $bal - ($p1+$p2);
				?>
        <td width="40%">First Payment:</td>
        <td><?php echo date("m/j/Y",strtotime($accinfo['payment1'])); ?></td>
        <td width="28%"><?php echo number_format($p1,2, '.', ','); ?></td>
      </tr>
      <tr>
        <td width="40%">Second Payment:</td>
        <td><?php echo date("m/j/Y",strtotime($accinfo['payment2'])); ?></td>
        <td><?php echo number_format($p2,2, '.', ','); ?></td>
      </tr>
      <tr>
        <td width="40%">Third Payment:</td>
        <td><?php echo date("m/j/Y",strtotime($accinfo['payment3'])); ?></td>
        <td><?php echo number_format($p3,2, '.', ','); ?></td>
      </tr>
      <?php } ?>
    </table></td>
  </tr>
  <tr>
    <td><div align="right">
      <p align="center"><em class="style1">Released By: <b>
        <?= $user['account_FNAME']." ".$user['account_LNAME']; ?>
        </b> <br />
        Admin Staff </em></p>
    </div>
        <div align="right"></div></td>
    <td><div align="center" style="width:90%">
      <div align="right"><strong><em>Registrar's Copy</em></strong></div>
    </div></td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
