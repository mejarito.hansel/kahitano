<?php 
//INITIALIZE INCLUDES
	include('init.php');
#SESSION::CheckLogin();

	//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
            ROOM::addroom(Request::post());
			#var_dump(Request::post());
			
			break;
			case "edit": ROOM::update_room($_GET['id'],Request::post()); break;
			case "change_password":USERS::changePassword($_SESSION['USER']['account_ID'],Request::post());
			break;
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": ROOM::delete($_GET['id']); break;
		
		}
	}		
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<?php include(LAYOUTS . "styles.php"); ?>
		<?php include(LAYOUTS . "scripts.php"); ?>
       
      <title>PROFILE | CSJP-II-AS</title>
	</head>
	<body>
		<!-- top nav -->
		<?php include(LAYOUTS . 'top_nav.php'); ?>
		<!-- end nav -->
		<div class="container">
			<!-- banner -->
			<?php include(LAYOUTS . "banner.php"); ?>
			<!-- end banner -->
			<!-- alert messages -->
			<?php SESSION::DisplayMsg(); ?>
	
			<div class="row">
				<!-- left nav -->   
         
                <div class="col-md-3">
	
                <?php include(PAGES."navigation.php");?>
                </div>
               
				<!-- end left nav -->
				
				<!-- BODY -->
				
                
                <div class="col-md-9">
				
                    <?php
					switch($_GET['action']){
                      
					    case "change_password":
             
                    ?>
					 
						     <div class="panel panel-default">
                        
                   					 <div class="panel-heading">
                        						<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                          						  <i class="fa fa-folder-o"></i> Change Password
                                                </h3>
                                     </div>
                                   <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                     
                   					 <div class="panel-body">
                                        <div class="form-group">
                                            <?= FORM::label("pass_old", "Old Password", array("class"=>"control-label col-sm-4")) ?>
                                            <div class="col-md-6"><?= FORM::password("cpassword",'', array("class"=>"form-control","autofocus"=>"autofocus","required")) ?></div>
                                        </div>
            
                                                    
                                        <div class="form-group">
                                            <?= FORM::label("new", "New Password", array("class"=>"control-label col-sm-4")) ?>
                                            <div class="col-md-6"><?= FORM::password("npassword", '', array("required", "class"=>"form-control")) ?></div>
                                        </div>                                        
                                        
                                        <div class="form-group">
                                            <?= FORM::label("pass_new2", "Retype Password", array("class"=>"control-label col-sm-4")) ?>
                                            <div class="col-md-6"><?= FORM::password("vpassword", '', array("required", "class"=>"form-control")) ?></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-4"></div>
                                            <div class="col-md-6">
                                                <?= FORM::submit(null, Arr::get($user, "change_pass") ? "Change Password": "Change Password", array("class"=>"btn btn-success btn-block")) ?>
                                            </div>
                                        </div>

										<div class="form-group">	
													

		                             </div>
                                     
                                     </form>
							</div>			  
					  
					 </div>
                     
					  <?php break;
					    case "add":?>
                    
                    <?php break; 
                        case "edit":
             
                    ?>
                    
                    <?php break;  
                        case "search":
									
						?>
							
                    <?php break; 
                        case "view":?>
                      
                  <?php  break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
				default: HTML::redirect("profile.php?action=view");
                    }
					
			 ?>
                	
				    
                 </div>

				    
				
                
				<!-- end right nav -->
				
			</div>
			<!-- end row container -->
            
            

			<!-- footer-->      
			<?php include (LAYOUTS . "footer.php"); ?>
			<!-- end footer -->

		</div>
		<!-- end container -->

	</body>
</html>
