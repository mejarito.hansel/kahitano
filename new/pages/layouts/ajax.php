<script>
	function ajax(div, url, params, type, loader){
		type = (typeof type === "undefined") ? "get" : type;
		loader = (typeof loader === "undefined") ? false : true;
		if(loader){
			var loader = "<div class='loader-image'><?= HTML::img('loader-image.gif', array('alt'=>'Loading...')) ?></div>";
			$(div).html(loader);
		}
		$.ajax({
			url: url,
			data: params,
			type: type,
			success: function(result){
				$(div).html(result);
			},
			error: function(e) {
				$(div).clear();
				alert(e.messae);
			}
		});
	}
</script>