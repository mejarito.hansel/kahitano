<?php 
	global $user;
	$teachers = GROUPS::getGroupRoles(Request::get_or_fail("id"), "Teacher");
	$students = GROUPS::getGroupRoles(Request::get_or_fail("id"), "Student");
	$x = 0;
?>

<div class="panel panel-default">
	<div class="panel-heading">
		<div class="text-lg">
		<h4>	<i class='fa fa-users'></i> Sponsored Links</h4>
		</div>
        
	</div>
	<div class="panel-body"><center>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- sponsor_200_200 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:200px;height:200px"
     data-ad-client="ca-pub-5672460765730918"
     data-ad-slot="2498102817"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script></center>
	</div>
</div>	





<!-- ADMIN PANEL -->
<div class="panel panel-default">
	<div class="panel-heading">
		<div class="text-lg">
		<h4>	<i class='fa fa-users'></i> Members</h4>
		</div>
        
	</div>
    
    
 
    
	<div class="overflow " style="min-height:75px;max-height:300px">
		<table class="table table-hover text-md"">
			<tbody>
				<?php 
				

				foreach($teachers as $teacher): ?>
				<tr>
					<td>
						<?= Arr::get($teacher, "file_ID")
							? HTML::img(FILE::getURI($teacher['file_ID']), array("class"=>"picture-xs"), true)
							: HTML::img("f1.png", array("class"=>"picture-xs"))
						?>&nbsp;
						<?= HTML::a("profile.php?action=view&id={$teacher['user_ID']}", USERS::getName($teacher)) ?>
						<div class="pull-right"><?= HTML::a("messages.php?action=send&id={$teacher['user_ID']}", "<i class='fa fa-envelope-o action-icon'></i>", array("class"=>"pop-icon", "title"=>"Message ".USERS::getName($teacher))) ?></div>
					</td>
				</tr>
				<?php 
				$x++;
				endforeach; ?>
				<?php foreach($students as $student): ?>
				<tr>
					<td>
						<?= Arr::get($student, "PIC")
							? HTML::img(FILE::getURI($student['PIC']), array("class"=>"picture-xs"), true)
							: HTML::img("f1.png", array("class"=>"picture-xs"))
						?>&nbsp;
						<?= HTML::a("profile.php?action=view&id={$student['user_ID']}", USERS::getName($student)) ?>
						<div class="pull-right"><?= HTML::a("messages.php?action=send&id={$student['user_ID']}", "<i class='fa fa-envelope-o action-icon'></i>", array("class"=>"pop-icon", "title"=>"Send a Message to ".USERS::getName($student))) ?></div>
					</td>
				</tr>

				<?php 
                $x++;
				endforeach; ?>
                
                
                <?php if($x == 0)
				{
				?>
                <br><center>No Members Yet</center><br>
                <?php
				
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<?php include('pages/announcement/group_announcement.php'); ?>
<?php include('pages/event/group_calendar.php'); ?>

<!-- ADMIN PANEL -->