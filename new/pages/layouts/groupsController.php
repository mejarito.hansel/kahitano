<?php

class GROUPS {


	public static function getFormat()
	{
		$sql = "
		SELECT 
			`course_format_ID`,
			`course_format_NAME`
		FROM course_format
		";
		return SQL::find_all($sql);
	
	}
	
	public static function getAllgroup()
	{
		$sql = "
		SELECT GI.*
		FROM groups_info AS GI
		INNER JOIN course_info AS CI
		ON GI.course_ID = CI.course_ID
		ORDER BY CI.course_NAME ASC
		";
		return SQL::find_all($sql);
	
	
	}
	
	public static function getTopic($group_ID){
		$sql = "
		SELECT 
			*
		FROM topic_info
		WHERE group_ID = ?
		";
		$param = array($group_ID);
		return SQL::find_all($sql, $param);
	}
	
	
	public static function getGroups($course_ID){
		$sql = "
		SELECT 
			*
		FROM groups_info
		WHERE course_ID = ?
		";
		$param = array(
			array($course_ID, PDO::PARAM_INT)
		);
		return SQL::find_all($sql, $param);
	}
	
	public static function getID($group_ID){
		$sql = "
		SELECT 
			*
		FROM groups_info
		WHERE group_ID = ?
		";
		$param = array(
			array($group_ID, PDO::PARAM_INT)
		);
		return SQL::find_id($sql, $param);
	}
	
	public static function getGroupRoles($group_ID, $role){
		if(is_numeric($role)){
			$sql = "
			SELECT CA.course_access_ID, UI.*
			FROM users_info AS U
			INNER JOIN users AS U
			ON U.user_ID = UI.user_ID
			INNER JOIN course_access AS CA
			ON U.user_ID = CA.user_ID
			INNER JOIN groups_role AS GR
			ON GR.course_access_ID = CA.course_access_ID
			WHERE GR.group_ID = ?
			AND CA.course_role_ID = ?
			ORDER BY UI.user_LNAME ASC, UI.user_FNAME ASC
			";
			$param = array(
				array($group_ID, PDO::PARAM_INT),
				array($role, PDO::PARAM_INT)
			);
		} else {
			$sql = "
			SELECT CA.course_access_ID, UI.*,U.file_ID
			FROM users_info AS UI
			INNER JOIN users AS U
			ON U.user_ID = UI.user_ID
			INNER JOIN course_access AS CA
			ON U.user_ID = CA.user_ID
			INNER JOIN course_role AS CR
			ON CR.course_role_ID = CA.course_role_ID
			INNER JOIN groups_role AS GR
			ON GR.course_access_ID = CA.course_access_ID
			WHERE GR.group_ID = ?
			AND CR.course_role_NAME = ?
			ORDER BY UI.user_LNAME ASC, UI.user_FNAME ASC
			";
			$param = array(
				array($group_ID, PDO::PARAM_INT),
				array($role, PDO::PARAM_STR)
			);
		}
		return SQL::find_all($sql, $param);
	}
	
	public static function getPotentialUsers($group_ID, $role, $course_ID){
		if(is_numeric($role)){
		$sql = "
			SELECT CA.course_access_ID, UI.*, CR.course_role_NAME
			FROM users_info AS UI
			INNER JOIN users AS U
			ON U.user_ID = UI.user_ID
			INNER JOIN course_access AS CA
			ON CA.user_ID = U.user_ID
			INNER JOIN course_role AS CR
			ON CR.course_role_ID = CA.course_role_ID
			LEFT JOIN groups_role AS GR
			ON GR.course_access_ID = CA.course_access_ID
			WHERE
			(
				GR.group_role_ID IS NULL
				OR (
					GR.group_role_ID IS NOT NULL
					AND CR.course_role_NAME <> 'Student'
				)
			)
			AND CR.course_role_ID = ?
			ORDER BY UI.user_LNAME ASC, UI.user_FNAME ASC
			";
			$param = array(
				array($group_ID, PDO::PARAM_INT),
				array($role, PDO::PARAM_INT)
			);
		} else {
			$sql = "
			SELECT CA.course_access_ID, UI.*, CR.course_role_NAME
			FROM users_info AS UI
			INNER JOIN users AS U
			ON U.user_ID = UI.user_ID
			INNER JOIN course_access AS CA
			ON CA.user_ID = U.user_ID
			INNER JOIN course_role AS CR
			ON CR.course_role_ID = CA.course_role_ID
			LEFT JOIN (
				groups_role AS GR
				INNER JOIN groups_info AS GI
				ON GI.`group_ID` = GR.`group_ID`
			)
			ON GR.course_access_ID = CA.course_access_ID
			WHERE
			(
				(
					CR.course_role_NAME <> 'Student'
					AND U.user_ID NOT IN(
						SELECT C.user_ID
						FROM groups_role AS G
						INNER JOIN course_access AS C
						ON C.course_access_ID = G.course_access_ID
						AND G.group_ID = ?
					)
				)
				OR GR.group_ID IS NULL
			)
			AND CR.course_role_NAME = ?
			AND CA.course_ID = ?
			GROUP BY CA.course_access_ID
			ORDER BY UI.user_LNAME ASC, UI.user_FNAME ASC
			";
			$param = array(
				array($group_ID, PDO::PARAM_INT),
				array($role, PDO::PARAM_STR),
				array($course_ID, PDO::PARAM_INT)
			);
		}
		return SQL::find_all($sql, $param);
	}
	
	public static function isUserExists($course_access_ID, $group_ID){
		$sql = "
		SELECT TRUE
		FROM groups_role
		WHERE group_ID = ?
		AND course_access_ID = ?
		";
		$param = array(
			array($group_ID, PDO::PARAM_INT),
			array($course_access_ID, PDO::PARAM_INT)
		);
		return SQL::find_scalar($sql, $param);
	}
	
	public static function addRole($group_ID, $group_role){
		if(isset($group_role['course_access_ID_P'])){
			foreach($group_role['course_access_ID_P'] as $course_access_ID_P) {
				if(!self::isUserExists($course_access_ID_P, $group_ID)){
					$sql = "
					INSERT INTO groups_role
					SET
						group_ID = ?,
						course_access_ID = ?
					";
					$param = array(
						$group_ID,
						$course_access_ID_P
					);
					SQL::execute($sql, $param);
				} else {
					SESSION::StoreMsg("User already has a role in this group!");
				}
			}
		}
	}
	
	public static function removeRole($group_ID, $group_role){
		if(isset($group_role['course_access_ID'])){
			foreach($group_role['course_access_ID'] as $course_access_ID){
				$sql = "
				DELETE FROM groups_role
				WHERE group_ID = ?
				AND course_access_ID = ?
				";
				$param = array(
					$group_ID,
					$course_access_ID
				);
				SQL::execute($sql, $param);
			}
		}
	}
	
	public static function insert($group_info){
		$sql = "
		INSERT INTO groups_info
		SET
			group_NAME = ?,
			group_DESCRIPTION = ?,
			course_id = ?,
			course_length_ID = ?,
			course_format_ID = ?
		";
		$param = array(
			array($group_info['group_NAME'], PDO::PARAM_STR),
			array($group_info['group_DESCRIPTION'], PDO::PARAM_STR),
			array($group_info['course_ID'], PDO::PARAM_INT),
			array($group_info['course_length_ID'], PDO::PARAM_INT),
			array($group_info['course_format_ID'], PDO::PARAM_INT)
		);
		SQL::execute($sql, $param);

		// $id = SQL::last_ID();
		// for($x=1;$x<=$no;$x++)
		// {
		
		// 	$topic['topic_NAME'] = "TOPIC ".$x;
		// 	$topic['topic_DESCRIPTION'] = "NULL";
		// 	$topic['group_ID'] = $id;
		
		// 	TOPICS::insert($topic);
		// }			
		SESSION::StoreMsg("Group Successfully Added!", "success");
		HTML::redirect("courses.php?action=view&id=" . $group_info['course_ID']);
	}

	public static function update ($id, $data) {
		$sql = "
		UPDATE groups_info
		SET
			course_format_ID = ?,
			group_NAME = ?,
			group_DESCRIPTION = ?,
			course_ID = ?,
			course_length_ID = ?
		WHERE group_ID = ?
		";
		$param = array(
			$data['course_format_ID'],
			$data['group_NAME'],
			$data['group_DESCRIPTION'],
			$data['course_ID'],
			$data['course_length_ID'],
			$id
		);
		SQL::execute($sql, $param);
		SESSION::StoreMsg("Group successfully updated!", "success");
		HTML::redirect("groups.php?action=view&id=$id");
	}
	
	public static function getStudentCount($group_ID){
		$sql = "
		SELECT COUNT(*)
		FROM groups_role AS GR
		INNER JOIN course_access AS CA
		ON CA.course_access_ID = GR.course_access_ID
		INNER JOIN course_role AS CR
		ON CR.course_role_ID = CA.course_role_ID
		WHERE CR.course_role_NAME = 'Student'
		AND GR.group_ID = ?
		";
		$param = array(
			array($group_ID, PDO::PARAM_INT)
		);
		return SQL::find_scalar($sql, $param);
	}

	public static function is_teacher($user_id, $group_id){
		$sql = "
		SELECT 1
		FROM groups_role AS GR
		INNER JOIN course_access AS CA
		ON CA.course_access_ID = GR.course_access_ID
		INNER JOIN course_role AS CR
		ON CR.course_role_ID = CA.course_role_ID
		WHERE GR.group_ID = ?
		AND CA.user_ID = ?
		AND CR.course_role_NAME = 'Teacher'
		";
		$param = array($group_id, $user_id);
		return SQL::find_scalar($sql, $param);
	}
	
	

	public static function is_student($user_id, $group_id)
	{
		$sql = "
		SELECT 1
		FROM groups_role AS GR
		INNER JOIN course_access AS CA
		ON CA.course_access_ID = GR.course_access_ID
		INNER JOIN course_role AS CR
		ON CR.course_role_ID = CA.course_role_ID
		WHERE GR.group_ID = ?
		AND CA.user_ID = ?
		AND CR.course_role_NAME = 'Student'
		";
		$param = array($group_id, $user_id);
		return SQL::find_scalar($sql, $param);
	}

}


	



?>
