<?php global $user ?>
<div class="panel panel-default">
	<div class="panel-heading"><div class="text-lg"><i class='fa fa-list'></i> Navigation </div></div>
	<div style="padding:5px;">
		<ul class="nav nav-pills nav-stacked">
			<li class="<?php if(checkfile("index")){ ?>active<?php } ?>"><a href="<?php echo HOME; ?>"><i class="fa fa-home"></i> Home</a></li>
			<li class=""><a href="<?= HTML::href("profile.php?action=view&id={$user['user_ID']}") ?>"><i class="fa fa-user"></i> Profile</a></li>
			<!-- <li class="disabled"><a href="http://bootswatch.com/cerulean/#">Disabled</a></li> -->
			<li class="dropdown <?php if(checkfile("courses")){ ?>active<?php } ?>">
				<a class="dropdown-toggle" id="myCourse-link" style="cursor:pointer" >
					<i class="fa fa-book"></i> My Courses <span class="caret"></span>
				</a>
				<ul class="nav nav-pills nav-stacked nav-sub" id="myCourse-content" style="display:none" >
				
                <?php foreach($myCourse as $key => $val) {  ?>
                
                <?php
							$groups = navCat::course_groups($user['user_ID'],$val['course_ID']);
							$groups_size = sizeof($groups);
							
						?>
                	<li >
                    	<a id="<?php echo $val['course_ID']; ?>-link" style="cursor:pointer" title="<?php echo $val['course_NAME']; ?>">
                        	<span class="span_link"  style="cursor:pointer" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'"  onclick="javascript:window.open('courses.php?action=view&id=<?php echo $val['course_ID']; ?>','_self');">
								<i class="fa fa-book"></i>
								<?php echo $val['course_CODE']; ?>
							</span> 
                            <?php if($groups_size > 0) { ?>
                            <span class="caret"></span>
                            <?php } ?>
                           
                        </a>
                        
                        <ul class="nav nav-pills nav-stacked nav-sub" id="<?php echo $val['course_ID']; ?>-content" style="display:none" >
                        
                        
						<?php foreach($groups as $key2 => $val2){ ?>                        
                        	<li>
                            	<a id="group-<?php echo $val2['group_ID']; ?>-link">
                                	<span class="span_link" onclick="javascript:window.open('groups.php?action=view&id=<?php echo $val['course_ID']; ?>&group_ID=<?php echo $val2['group_ID']; ?>','_self');" style="cursor:pointer" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'">
										<i class="fa fa-users"></i>
										<?php echo $val2['group_NAME']; ?> <!--<span class="caret"></span>-->
                                	</span>
                                </a>
                           
							<!--
                				<ul class="nav nav-pills nav-stacked nav-sub" id="group-<?php echo $val2['group_ID']; ?>-content" style="display:none" >
                                <?php
								$topics = navCat::getTopics($val2['group_ID']);
								$topics_size = sizeof($topics);
								?>
                                
                            		<li><a>Participants <span class="caret"></span></a></li>
                            
                            		<?php if($topics_size > 0) { ?>
                                    <?php foreach($topics as $key3 => $val3){ ?>
									<li><a><?php echo $val3['topic_NAME']; ?></a></li>
									
                                    <?php } ?>
                                    <?php } ?>
                            
                            	</ul>
                               -->
                            </li>
						<?php } ?>
                        </ul>
                        
                        
                        <?php foreach($groups as $key => $val){ ?>          
                        <script>
						 $( "#group-<?php echo $val['group_ID']; ?>-link" ).click(function() {
							 $( "#group-<?php echo $val['group_ID']; ?>-content" ).slideToggle( "fast" );
						 });
						</script>
                        <?php } ?>
                        
                        
                    </li>
				<?php } ?>
				</ul>
			</li>
			
			<li class="<?php if(checkfile("clubs")){ ?>active<?php } ?>">
				<a href='<?php echo HOME; ?>clubs.php?action=view_all'><i class="fa fa-users"> </i> My Clubs</a>
			</li>
			<li><?= HTML::a("my_storage.php", "<i class='fa fa-hdd-o'></i> Personal Storage") ?></li>
		</ul>
	</div>
</div>


<script>
 $( "#myCourse-link" ).click(function() {
	$( "#myCourse-content" ).slideToggle( "fast" );
});



$( document ).ready(function() {
<?php if(checkfile("courses")){ ?>
    $( "#myCourse-content" ).slideToggle( "fast" );
	
<?php } ?>

<?php if(isset($_GET['id'])) { ?>
	$( "#<?php echo $_GET['id']; ?>-content" ).slideToggle( "fast" );
	
<?php } ?>
	
});


<?php foreach($myCourse as $key => $val) {  ?>
 $( "#<?php echo $val['course_ID']; ?>-link" ).click(function() {
	$( "#<?php echo $val['course_ID']; ?>-content" ).slideToggle( "fast" );
});

<?php } ?> 	  
</script>


<?php
if($user['access_ID'] == 1 or $user['access_ID'] == 2){ 
?>
<!-- ADMIN PANEL -->
<div class="panel panel-default">
	<div class="panel-heading"><div class="text-lg"><i class='fa fa-gear'></i> Administration</div></div>
	<div style="padding:5px;">
		<ul class="nav nav-pills nav-stacked">

			<!-- <li class="disabled"><a href="http://bootswatch.com/cerulean/#">Disabled</a></li> -->

			<li> <?php echo HTML::a("courses.php?action=view_all", "<i class='fa fa-book'></i> Courses") ?></li>
			<li class="<?php if(checkfile("categories")){ ?>active<?php } ?>"><?php echo HTML::a("categories.php?action=view_all", "<i class='fa fa-users'></i> Course Categories") ?></li>
			<li><?php echo HTML::a("groups.php?action=view_all", "<i class='fa fa-users'></i> Course Groups") ?></li>
			<li class="dropdown">
				<a class="dropdown-toggle" id="link" data-toggle="dropdown" href="#">
					<i class='fa fa-users'></i> Users <span class="caret"></span>
				</a>
				<ul class="dropdown-menu" >
					<li><?php echo HTML::a("users.php?action=add", "Add User") ?></li>
					<li><?php echo HTML::a("users.php", "Manage Users") ?></li>
				</ul>
			</li>
            
            
			
			<li class=""><a href="#"><i class="fa fa-shield"></i> Permissions</a></li>
			<li class=""><a href="#"><i class="fa fa-bar-chart-o"></i> Grades</a></li>
		</ul>
	</div>
</div>
<!-- ADMIN PANEL -->
<?php } ?>