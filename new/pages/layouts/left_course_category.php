<?php

$categoryList = navCat::getAlltop();
$caegoryList_size = sizeof($categoryList);

?>

<div class="panel panel-default">
	<div class="panel-heading"> <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"> Course Categories </h3> </div>
	<div class="panel-body">
		<ul class="nav nav-pills nav-stacked">
        
        
        <?php if($caegoryList_size > 0) { ?>
        
        <?php foreach($categoryList as $key => $val){         	
				GetCat($val['category_ID'],$val['category_NAME']);
		} ?>
        
        <?php }else{ ?>
        	<li>No Category Yet</li>
        <?php } ?>
        
        
        <?php
			function GetCat($cat_id,$cat_name)
			{
			
			?>
            <li ><a style="cursor:pointer" id="c<?php echo $cat_id; ?>-link"><span class="span_link" onclick="javascript:window.open('category/category.php?id=<?php echo $cat_id; ?>','_self');" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" ><?php echo $cat_name ?>
			<?php 
			
			
			  //GetCourseCount
				  $courseCount = navCat::getCourseCount($cat_id); 
				  $courseCount_size = sizeof($courseCount);
			
				//display course count
					if($courseCount_size > 0) {
					echo "(".$courseCount_size.")"; 
					} 
			
				//getSubCat count
            	$getSub = navCat::getSubCat($cat_id);
				$getSub_size = sizeof($getSub);
			?>
             </span>
            <?php
			
				//display dropdown
				if($getSub_size > 0){
				echo ' <span class="caret"></span>';
				}
			
			?>
          </a>
            
            	<?php
				if($getSub_size > 0){
				?>
                <ul class="nav nav-pills nav-stacked nav-sub" style="display:none;" id="c<?php echo $cat_id; ?>-content">
                <?php
					
					foreach($getSub as $key => $val)
					{
					GetCat($val['category_ID'],$val['category_NAME']);
					}
					?>
				</ul>
                <?php	
				
				}
				
				?>
            	
             </li>
            <?php	
				
			}
		?>
        
        <script>
		<?php $allCat = navCat::getAll(); ?>
		
		<?php foreach($allCat as $key => $val) { ?>
		  $( "#c<?php echo $val['category_ID']; ?>-link" ).click(function() {
			 $( "#c<?php echo $val['category_ID']; ?>-content" ).slideToggle( "fast" );
	  	 });
		<?php } ?> 	  
		</script>
        
        <!--
			<li class="active"><a href="#">Computer Studies (#)</a></li>
			<li class=""><a href="#">Business Course</a></li>
			<li class="dropdown"><a class="dropdown-toggle" id="link" data-toggle="dropdown" href="#">Cat3 <span class="caret"></span></a>
				<ul class="nav nav-pills nav-stacked nav-sub"  >
					<li><a href="#">Cat3 Sub 1 <span class="caret"></span></a>
                    
                    <ul class="nav nav-pills nav-stacked  nav-sub"  style="">
					
					<li><a href="#">Cat3 Sub 2</a></li>
					<li><a href="#">Cat3 Sub 3</a></li>
					<li><a href="#">Cat3 Sub 4</a></li>
					</ul>
                    
                    </li>
					<li><a href="#">Cat3 Sub 2</a></li>
					<li><a href="#">Cat3 Sub 3</a></li>
					<li><a href="#">Cat3 Sub 4</a></li>
				</ul>
			</li>
            
           --> 
		</ul>
	</div>
</div>

