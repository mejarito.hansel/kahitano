<!-- Navbar -->	
<div class="navbar navbar-default navbar-fixed-top">
	<div class="container">
        <div class="navbar-header">
			<a href="index.php" class="navbar-brand">IDLE</a>
			<button class="navbar-toggle collapsed" type="button" data-toggle="collapse" data-target="#navbar-main">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main" style="height: 1px;">
			<ul class="nav navbar-nav">
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="themes">My Courses <span class="caret"></span></a>
					<ul class="dropdown-menu" >
						<li><a tabindex="-1" href="#">1</a></li>
						<li class="divider"></li>
						<li><a tabindex="-1" href="#">2</a></li>
					</ul>
				</li>


				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="Administration">Administration <span class="caret"></span></a>
					<ul class="dropdown-menu" >
						<li><a tabindex="-1" href="#">1</a></li>
						<li class="divider"></li>
						<li><a tabindex="-1" href="#">2</a></li>
					</ul>
				</li>

				<li><a href="#">Courses</a></li>
			</ul>

			<ul class="nav navbar-nav navbar-right">
				<li><a href="login.php" target="">Login</a></li>

				<li class="dropdown">

					<a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <span class="caret"></span></a>

					<ul class="dropdown-menu">
						<li><a href="#">My Profile</a></li>
						<li><a href="#">Messages</a></li>
						<li><a href="#">Blogs</a></li>
						<li><a href="#">Logout</a></li>
					</ul>
				</li>
			</ul>
        </div>
	</div>
</div>

<!-- Navbar -->