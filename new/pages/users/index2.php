<script src="jquery.js" type="text/javascript"></script>
<script> 
$(document).ready(function(){
  $("#flip").click(function(){
    $("#panel").slideToggle("fast");
  });
});
</script>

<table class="table1">
	<tr>
		<td>
		<td>
		<td align='right'>
			Search: <input class="searchItem" id="q" name="q" type="text" onkeyup="term(q.value,oBY.value,aDES.value)" autofocus="autofocus"/>
	<tr>
		<td>
		<td>
		<td align='right'>
			<div id="flip"><button class="aSearchButton">Search tool</button></div>
			<div id="panel">
				Sort by:
				<select name='oBY' id='oBY' onChange="term(q.value,this.value,aDES.value)">
					<option value='si_FNAME'>Name</option>
					<option value='si_MNAME'>Middle Name</option>
					<option value='si_LNAME'>Last Name</option>
				</select>
				<select name='aDES' id='aDES' onChange="term(q.value,oBY.value,aDES.value)">
					<option value='ASC'>Ascending</option>
					<option value='DESC'>Descending</option>
				</select>
			</div>
	<tr>
		<td colspan='3'>
			<div id="display_result"></div>
</table>

<?php
?>

<script>
	function term(value,oBY, aDES){
		$.post("search.php",{search_term:value, oBY:oBY, aDES:aDES}, function(data){
			$("#display_result").html(data).hide().fadeIn();
		});
	}
</script>