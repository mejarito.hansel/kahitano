<div class="panel panel-default">  
    
	<div class="panel-heading"> <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class="fa fa-bullhorn"></i> Announcements</h3> 
	</div>
	<div class="panel-body" style=" min-height:50px; max-height:200px; overflow-y: auto; ">

                <div  id="A1" class="list-group-item" style="margin-bottom:6px; cursor:pointer" title="Click to show content" >               
                     <a href="announcement_popup.php?action=view&id=1" style=""  target="POPUPW"  onclick="POPUPW = window.open('about:blank','POPUPW', 'scrollbars=1,width=400,height=400');"><h3 style="margin-top:0px; margin-bottom:0px;">Welcome!</h3> </a>       
                   <i style="font-size:12px">03/05/14 03:32PM<br /> by  <a href="profile.php?id=1"> admin</a></i>
                           
                </div>
        </div>
         </div>

 <script type="text/javascript">
			  var clndr = {};
			$( function() {
			
					
			  // PARDON ME while I do a little magic to keep these events relevant for the rest of time...
			  var currentMonth = moment().format('YYYY-MM');
			  var nextMonth    = moment().add('month', 1).format('YYYY-MM');
			 
			  var current	= moment().format('YYYY-MM-DD');
			 
			 
			  var events = [
					
				
				{ date: '2014-04-03', title: 'Graduation!', content: "Graduation day @ PICC Plenary Hall 6AM" }, 						
				
				
			  ];
			
			
			clndr = $('#full-clndr').clndr({
				template: $('#full-clndr-template').html(),
				events: events
			  });
			  $('#mini-clndr').clndr({
				template: $('#mini-clndr-template').html(),
				events: events,
				clickEvents: {
				  click: function(target) {
					if(target.events.length) {
					  var daysContainer = $('#mini-clndr').find('.days-container');
					  daysContainer.toggleClass('show-events', true);
					  $('#mini-clndr').find('.x-button').click( function() {
						daysContainer.toggleClass('show-events', false);
					  });
					}
				  }
				},
				adjacentDaysChangeMonth: true
			  });
			
			});
  
  </script>
  