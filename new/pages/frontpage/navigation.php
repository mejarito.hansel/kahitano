<?php
if (!SESSION::isLoggedIn()) {
?>

   <div class="panel panel-default">
            <div class="panel-heading"> 
            	<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px;   font-size: 24px;">
                		<i class="fa fa-list"></i>  Navigation 
                </h3> 
             </div>
       		 <div class="panel-body">
             	<ul class="nav nav-pills nav-stacked">
             		<li class="<?php if(checkfile("index")){ ?>active<?php } ?>"><a href="index.php"><i class="fa fa-home" ></i> Home</a></li>
                   <!--<li class="<?php if(checkfile("grade_inquiry")){ ?>active<?php } ?>"><a href="grade_inquiry.php"><i class="fa fa-search" ></i> Grade Inquiry</a></li>-->
                	<li class="<?php if(checkfile("contact")){ ?>active<?php } ?>"><a href="contact.php"><i class="fa fa-phone-square" ></i> Contact Us</a></li>
                </ul>
             </div>
 	</div>

                    
<?php   }else{?>                   
                            <!-- start -->
        <div class="panel panel-default">
            <div class="panel-heading"> <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px;   font-size: 24px;"><i class="fa fa-list"></i>  Navigation </h3> </div>
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked">
                  
                    
                    <li class="<?php if(checkfile("index")){ ?>active<?php } ?>"><a href="index.php"><i class="fa fa-home" ></i> Home</a></li>
          <?php
                    $acid = $_SESSION['USER']['access_ID'];
                    if($acid=='1'||$acid=='3'){
                    ?>
                    <!-- <li class="disabled"><a href="http://bootswatch.com/cerulean/#">Disabled</a></li> -->
        
                    <li class="dropdown <?php if(checkfile("student_information")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="admission_Link" style="cursor: pointer;  ">
                           <i class="fa fa-book"></i> Admission <span class="caret" style="border-top- "></span>                        </a>
                        <ul class="nav nav-pills nav-stacked nav-sub" id="admission_Content" style="display:none">     
                          <li class=""><a href="student_information.php?action=add"><i class="fa fa-plus-square"></i> Add Student</a></li> 
                           <li class=""><a href="student_information.php?action=view"><i class="fa fa-file-text"></i> View Cadet Information</a></li> 
                           <li class=""><a href="curricullum_enrolled.php?action=add"><i class="fa fa-outdent"></i> Student Course Selection</a></li> 
                           <li class=""><a href="curricullum_enrolled.php?action=view"><i class="fa fa-list-ul"></i> Curricullum Enrolled</a></li> 
                          
                          </ul>
					</li>
                    
                    <li class="dropdown <?php if(checkfile("accounting") or checkfile("particulars")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="accounting_Link" style="cursor: pointer;  ">
                          <i class="fa <?php if(checkfile("accounting") or checkfile("particulars") or checkfile("acc_transaction") or checkfile("accounting_expense")){ ?>fa-folder-open-o<?php }else{ ?>fa-folder-o<?php } ?>"></i> Accounting <span class="caret" style="border-top- "></span>                        </a>
                        <ul class="nav nav-pills nav-stacked nav-sub" id="acc_Content" style=" <?php if(checkfile("accounting") or checkfile("particulars") or checkfile("accounting_expense") or checkfile("acc_transaction.php")){ ?>display:block<?php }else{ ?>display:none<?php } ?>">   
                        
                           <li class=""><a href="accounting.php?action=view"><i class="fa fa-search" ></i> Transact</a></li> 
                           <li class=""><a href="promi_list.php"><i class="fa fa-file-text-o" ></i> Promissory List<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         <b style="font-size:12px;">(<?php
                            $od_promi = ACCT::getExpiredPromiPerSem();
									$c = count($od_promi);
									echo $c;
									?> Needs action)</b>
                           </a></li>            
                           <li class=""><a href="particulars.php?action=view"><i class="fa fa-gears" ></i> Particulars Management</a></li>            
                           <li class=""><a href="accounting_expense.php?action=view"><i class="fa fa-shopping-cart" ></i> Expense Management</a></li>
                           <li class=""><a href="accounting_deposits.php?action=view"><i class="fa fa-money" ></i> Deposit Management</a></li>
                           <li class=""><a href="acct_transaction_logs.php?action=view"><i class="fa fa-file-text-o" ></i> Transaction Logs</a></li>
                           <li class=""><a href="acct_transaction_logs.php?action=reports"><i class="fa fa-file-text-o" ></i> Transaction Reports</a></li>
                             
                      </ul>
					</li>
					
					<li class="dropdown <?php if(checkfile("subjects")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="subject_Link" style="cursor: pointer;" href="subjects.php?action=view">
                           <i class="fa fa-book"></i> Subject Management                        </a>					</li>
                    <li class="dropdown <?php if(checkfile("room")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="room_Link" style="cursor: pointer;" href="room.php?action=view">
                           <i class="fa fa-book"></i> Room Management                        </a>                    </li>
                    <li class=""><a href="report_assesment_list.php?action=choose"><i class="fa  fa-list" ></i> Assessment List</a></li>
                                   
					
                    <li class="dropdown <?php if(checkfile("semester")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="" style="cursor: pointer;" href="semester.php?action=view">
                           <i class="fa fa-book"></i> Semester                        </a>                    </li>
                    <li class="dropdown <?php if(checkfile("semester_section")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="" style="cursor: pointer;" href="semester_section.php?action=view">
                           <i class="fa fa-book"></i> Semester Section                        </a>                    </li>
                    
                    
                    <li class="dropdown <?php if(checkfile("instructors")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="subject_Link" style="cursor: pointer;" href="instructors.php?action=view">
                           <i class="fa fa-book"></i> Instructors                        </a>                    </li>
                    <li class="dropdown <?php if(checkfile("course")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="subject_Link" style="cursor: pointer;" href="course.php?action=view">
                           <i class="fa fa-book"></i> Courses                        </a>                    </li>
                    
					<li class="dropdown <?php if(checkfile("curricullum") or checkfile("curricullum_subjects") or checkfile("curricullum_enrolled")){ ?>active<?php } ?>"> <a class="dropdown-toggle" id="currmgt_Link" style="cursor: pointer;  "> <i class="fa fa-gear"></i> Curriculum Management <span class="caret" style="border-top- "></span> </a>
                      <ul class="nav nav-pills nav-stacked nav-sub" id="currmgt_Content" style=" <?php if(checkfile("curricullum") or checkfile("curricullum_subjects") or checkfile("curricullum_enrolled")){ ?> <?php }else{ ?>display:none<?php } ?>">
                        <li class="dropdown"> <a class="dropdown-toggle" style="cursor: pointer;" href="curricullum.php?action=view"> <i class="fa fa-book"></i> Curriculum </a> </li>
                        <li class="dropdown"> <a class="dropdown-toggle" id="subject_Link2" style="cursor: pointer;" href="curricullum_subjects.php?action=view"> <i class="fa fa-book"></i> Curriculum Subjects </a> </li>
                        <li class="dropdown "> <a class="dropdown-toggle " id="subject_Link2" style="cursor: pointer;" href="curricullum_enrolled.php?action=view"> <i class="fa fa-book"></i> Curriculum Enrolled</a></li>
                      </ul>
                    </li>   
					
                    <li class="<?php if(checkfile("grade_inquiry")){ ?>active<?php } ?>"> <a class="dropdown-toggle" id="grade_Link" style="cursor: pointer;  "> <i class="fa fa-sort-numeric-asc"></i> Grade Management <span class="caret" style="border-top- "></span> </a>
                      <ul class="nav nav-pills nav-stacked nav-sub" id="grade_Content" style=" <?php if(checkfile("grade_inquiry") or checkfile("grade_encoding_admin")){ ?> <?php }else{ ?>display:none<?php } ?>">
                        <li class=""><a href="grade_encoding_admin.php?action=search"><i class="fa fa-sort-numeric-asc" ></i> Curriculum Checklist</a></li>
                        <li class=""><a href="grade_inquiry.php?action=search"><i class="fa fa-print" ></i> Print Grades</a></li>
                        <li class=""><a href="grade_checking.php?action=view&sem_ID="><i class="fa fa-check" ></i> Grade Verification</a></li>
                      </ul>
                    </li>
                    <li class="<?php if(checkfile("fin_enrolment") or checkfile("sectioning") or checkfile("enrolment_list") or checkfile("fin_enrolment2") or checkfile("fin_enrolment")){ ?>active<?php } ?>"> <a class="dropdown-toggle" id="enrolment_Link" style="cursor: pointer;  "> <i class="fa fa-archive"></i> Enrolment <span class="caret" style="border-top- "></span> </a>
                      <ul class="nav nav-pills nav-stacked nav-sub" id="enrolment_Content" style=" <?php if(checkfile("fin_enrolment") or checkfile("sectioning") or checkfile("enrolment_list") or checkfile("fin_enrolment2") or checkfile("fin_enrolment")){ ?> <?php }else{ ?>display:none<?php } ?>">
                        <li class=""><a href="sectioning.php?action=search"><i class="fa fa-outdent" ></i> Sectioning</a></li>
                        <li class=""><a href="enrolment_list.php?action=choose"><i class="fa  fa-list" ></i> Enrolment List</a></li>
                        <li class=""><a href="report_semester_per_subject.php?action=choose"><i class="fa  fa-print" ></i> Print Per Subject Reports</a></li>
                        <li class=""><a href="fin_enrolment2.php?action=search"><i class="fa fa-print" ></i> View Cadet Schedule</a></li>
                        <!-- <li class=""><a href="old/fin_enrolment.php?action=search"><i class="fa fa-print" ></i> View Cadet Schedule <br />
                          14-15 2nd Tri (Old)</a></li> -->
                          <li class=""><a target="_blank" href="print_deficiencies.php?action=search"><i class="fa fa-print" ></i> Print Deficiencies Reports</a></li>
                        
                      </ul>
                  </li>
                    <?php 
                    }else if($acid=='7'){
					?>
                    
                    
                    <li class="dropdown <?php if(checkfile("student_information")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="admission_Link" style="cursor: pointer;  ">
                           <i class="fa fa-book"></i> Admission <span class="caret" style="border-top- "></span>                        </a>
                        <ul class="nav nav-pills nav-stacked nav-sub" id="admission_Content" style=" <?php if(checkfile("student_information")){ ?> <?php }else{ ?>display:none<?php } ?>">            
                            <li class=""><a href="student_information.php?action=add"><i class="fa fa-plus-square"></i> Add Student</a></li>       
                            <li>
                                <a  style="cursor: pointer;">
                                    <span class="span_link" style="cursor:pointer" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" onclick="javascript:window.open('student_information.php?action=view','_self');">
									<i class="fa fa-book"></i> View Cadet Information  </span>                                </a>							</li>
                            <li>
                                <a  style="cursor: pointer;">
                                    <span class="span_link" style="cursor:pointer" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" onclick="javascript:window.open('curricullum_enrolled.php?action=add','_self');">
									<i class="fa fa-book"></i> Student Course Selection </span>                                </a>							</li>
                        </ul>
						
                        <!-- 	<li class=""><a href="grade_encoding_admin.php"><i class="fa fa-sort-numeric-asc" ></i> Curriculum Checklist</a></li> -->
					</li>
                    
                     
                    
                    
                    <?php }else if($acid=='2'){
					?>
					
					 
        
                
        
                    <li class="dropdown <?php if(checkfile("student_information")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="admission_Link" style="cursor: pointer;  ">
                           <i class="fa fa-group"></i> Admission <span class="caret" style="border-top- "></span>                        </a>
                        <ul class="nav nav-pills nav-stacked nav-sub" id="admission_Content" style=" <?php if(checkfile("student_information")){ ?> <?php }else{ ?>display:none<?php } ?>">            
                           <li class=""><a href="student_information.php?action=add"><i class="fa fa-plus-square"></i> Add Student</a></li> 
                           <li class=""><a href="student_information.php?action=view"><i class="fa fa-file-text"></i> View Cadet Information</a></li> 
                           <li class=""><a href="curricullum_enrolled.php?action=add"><i class="fa fa-outdent"></i> Student Course Selection</a></li> 
                           <li class=""><a href="curricullum_enrolled.php?action=view"><i class="fa fa-list-ul"></i> Curricullum Enrolled</a></li> 
                         </ul>
					</li>
					
					<li class="dropdown <?php if(checkfile("grade_inquiry") or checkfile("grade_encoding_admin")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="grade_Link" style="cursor: pointer;  ">
                           <i class="fa fa-sort-numeric-asc"></i> Grade Management <span class="caret" style="border-top- "></span>                        </a>
                        <ul class="nav nav-pills nav-stacked nav-sub" id="grade_Content" style=" <?php if(checkfile("grade_inquiry") or checkfile("grade_encoding_admin")){ ?> <?php }else{ ?>display:none<?php } ?>">            
                       		
                            <li class=""><a href="grade_encoding_admin.php?action=search"><i class="fa fa-sort-numeric-asc" ></i> Curriculum Checklist</a></li>
                        	<li class=""><a href="grade_inquiry.php?action=search"><i class="fa fa-print" ></i> Print Grades</a></li>
                        
                            <li class=""><a href="grade_checking.php?action=view&sem_ID="><i class="fa fa-check" ></i> Grade Verification</a></li>
                        </ul>
                  </li>    
                    <li class="dropdown <?php if(checkfile("sectioning") or checkfile("fin_enrolment")  or checkfile("fin_enrolment2") or checkfile("enrolment_list")  or checkfile("change_enrolment")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="enrolment_Link" style="cursor: pointer;  ">
                           <i class="fa fa-archive"></i> Enrolment <span class="caret" style="border-top- "></span>                        </a>
                        <ul class="nav nav-pills nav-stacked nav-sub" id="enrolment_Content" style=" <?php if(checkfile("sectioning") or checkfile("fin_enrolment")  or checkfile("fin_enrolment2") or checkfile("enrolment_list")  or checkfile("change_enrolment")){ ?> <?php }else{ ?>display:none<?php } ?>">            
                        
                        	 <li class=""><a href="sectioning.php?action=search"><i class="fa fa-outdent" ></i> Sectioning</a></li>
                            <li class=""><a href="enrolment_list.php?action=choose"><i class="fa  fa-list" ></i> Enrolment List</a></li>
                             <li class=""><a href="report_assesment_list.php?action=choose"><i class="fa  fa-list" ></i> Assessment List</a></li>
                            <li class=""><a href="report_semester_per_subject.php?action=choose"><i class="fa  fa-print" ></i> Print Per Subject Reports</a></li><li class=""><a href="fin_enrolment2.php?action=search"><i class="fa fa-print" ></i> View Cadet Schedule</a></li>
                            <!-- <li class=""><a href="fin_enrolment.php?action=search"><i class="fa fa-print" ></i> View Cadet Schedule <br>14-15 2nd Tri (Old)</a></li> -->
                        	<li class=""><a target="_blank" href="print_deficiencies.php?action=search"><i class="fa fa-print" ></i> Print Deficiencies</a></li>
                        
                        </ul>
                    </li>    
                    
                    
                       <li class="dropdown <?php if(checkfile("curricullum") or checkfile("curricullum_subjects") or checkfile("curricullum_enrolled")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="currmgt_Link" style="cursor: pointer;  ">
                           <i class="fa fa-gear"></i> Curriculum Management <span class="caret" style="border-top- "></span>                        </a>
                        <ul class="nav nav-pills nav-stacked nav-sub" id="currmgt_Content" style=" <?php if(checkfile("curricullum") or checkfile("curricullum_subjects") or checkfile("curricullum_enrolled")){ ?> <?php }else{ ?>display:none<?php } ?>">            
                                   
                                <li class="dropdown">
                                    <a class="dropdown-toggle" style="cursor: pointer;" href="curricullum.php?action=view">
                                       <i class="fa fa-book"></i> Curriculum                                    </a>                                </li>   
                                <li class="dropdown">
                                    <a class="dropdown-toggle" id="subject_Link" style="cursor: pointer;" href="curricullum_subjects.php?action=view">
                                       <i class="fa fa-book"></i> Curriculum Subjects                                    </a>                                </li>
                                <li class="dropdown ">
                                    <a class="dropdown-toggle " id="subject_Link" style="cursor: pointer;" href="curricullum_enrolled.php?action=view">
                                       <i class="fa fa-book"></i> Curriculum Enrolled                                    </a>                                </li>
                    	</ul>
                    </li>    
				   	<?php }else if($acid=='4'){
                    ?>
                    <li class="<?php if(checkfile("accounting")){ ?>active<?php } ?>"><a href="accounting.php?action=view"><i class="fa fa-search" ></i> Transact</a></li> 
                           <li class="<?php if(checkfile("promi_list")){ ?>active<?php } ?>"><a href="promi_list.php"><i class="fa fa-file-text-o" ></i> Promissory List<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         <b style="font-size:12px;">(<?php
                            $od_promi = ACCT::getExpiredPromiPerSem();
									$c = count($od_promi);
									echo $c;
									?> Needs action)</b>
                           </a></li>            
                           <li class="<?php if(checkfile("particulars")){ ?>active<?php } ?>"><a href="particulars.php?action=view"><i class="fa fa-gears" ></i> Particulars Management</a></li>            
                           <li class="<?php if(checkfile("accounting_expense")){ ?>active<?php } ?>"><a href="accounting_expense.php?action=view"><i class="fa fa-shopping-cart" ></i> Expense Management</a></li>
                           <li class="<?php if(checkfile("accounting_deposits")){ ?>active<?php } ?>"><a href="accounting_deposits.php?action=view"><i class="fa fa-money" ></i> Deposit Management</a></li>
                           <li class="<?php if(checkfile("acct_transaction_logs") && $_GET['action']=='view'){ ?>active<?php } ?>"><a href="acct_transaction_logs.php?action=view"><i class="fa fa-file-text-o" ></i> Transaction Logs</a></li>
                           <li class="<?php if(checkfile("acct_transaction_logs") && $_GET['action']=='reports'){ ?>active<?php } ?>"><a href="acct_transaction_logs.php?action=reports"><i class="fa fa-file-text-o" ></i> Transaction Reports</a></li>
                    			
                            
                            
                          <!--
                             <li>
          						  <a class="dropdown-toggle" id="transaction-log" style="cursor: pointer;" onclick="javascript:window.open('#','_self');">
                                   <i class="fa fa-shopping-cart"></i> Expenses
                                </a>
                            </li>
                             <li>
          						  <a class="dropdown-toggle" id="transaction-log" style="cursor: pointer;" onclick="javascript:window.open('#','_self');">
                                   <i class="fa fa-money"></i> Drawings
                                </a>
                            </li>
                             <li>
          						  <a class="dropdown-toggle" id="transaction-log" style="cursor: pointer;" onclick="javascript:window.open('#','_self');">
                                   <i class="fa fa-file-text-o"></i> Transaction Logs
                                </a>
                            </li>
                            -->
                        </ul>
					</li>
                    <?php
                    }if($acid=='5'){
                    ?>
                    
                    <?php
					$sem = SEM::getallsemsencoding(array("sem_ID"=>"ASC"));
					$instructor_ID = ui::getInstructorID($user['account_ID']);  
					$instructor_ID = $instructor_ID['instructor_ID'];
			
					 ?>
					
                    
                    
                    <?php foreach($sem as $key => $val){ ?>
                    
                    <li class="dropdown <?php if(checkfile("student_information")){ ?>active<?php } ?>">
                        <a class="dropdown-toggle" id="admission_Link" style="cursor: pointer;  ">
                           <b>&middot;</b>  <?php echo $val['sem_NAME']; ?> <span class="caret" style="border-top- "></span>                        </a>
                        
                        
                        <?php
						$getmysubj = SUBJSCHED::getByfaculty($val['sem_ID'], $instructor_ID);
						#print_r($getmysubj);
						?>
                        
                        
                       
                        <ul class="nav nav-pills nav-stacked nav-sub" id="admission_Content" style="/*display:none*/">            
                         <?php foreach($getmysubj as $key1 => $val1){ ?>
                            <li class="<?php if(isset($_GET['subject_sched_ID'])){ if($_GET['subject_sched_ID'] == $val1['subject_sched_ID']){ echo " active "; } } ?>">
                                <a  style="cursor: pointer;" title="UNLOCKED/PENDING">
                                    <span class="span_link" style="cursor:pointer" onmouseover="this.style.textDecoration='underline'" onmouseout="this.style.textDecoration='none'" onclick="javascript:window.open('grade_encoding.php?action=view&subject_sched_ID=<?php echo $val1['subject_sched_ID']; ?>','_self');">
									<b>&middot;</b> <?php $s = SUBJECTS::getID($val1['subject_ID']);
									echo $s['subject_CODE'];
									 ?>/<?php $c=COURSE::getbyID($val1['course_ID']); 
									echo $c['course_INIT']; 
									 
									 ?>-<?php echo $val1['semester_section_NAME']; ?>
                                      <?php
                                 switch($val1['is_lock'])
								{
								  case 1 : ?>
                               			 <img src="<?= IMAGES ?>/icon/lock.png" height="16" width="16" />
                                  <?php ; break;
								  case 0 : ?>
                                        <img src="<?= IMAGES ?>/icon/unlock.png" height="16" width="16" />
                                  <?php
								}
								?> 
                               
								<?php
								switch($val1['is_check'])
								{
								  case 1 :?>
                               			 <img src="<?= IMAGES ?>/icon/Y.gif" height="16" width="16" />
                                  <?php ; break;
								  case 0 : ?>
                                        <img src="<?= IMAGES ?>/icon/X.png" height="16" width="16" />
								<?php }
								?> 
                                </a>							</li>
                             <?php } ?>
                        </ul>
					</li>
                    
                    <?php } ?>
                    
                    
                    
                    
                    <?php } ?>	
                </ul>
          </div>
</div>
		
        <script>
		$( "#admission_Link" ).click(function() {
            $( "#admission_Content" ).slideToggle( "fast" );
        });
		
	
		
		
		
		
		$( "#subject_Link" ).click(function() {
				$( "#subject_Content" ).slideToggle( "fast" );
		});
		
		$( "#currmgt_Link" ).click(function() {
				$( "#currmgt_Content" ).slideToggle( "fast" );
		});
		
		
		$( "#enrolment_Link" ).click(function() {
				$( "#enrolment_Content" ).slideToggle( "fast" );
		});
		
		
		$( "#grade_Link" ).click(function() {
				$( "#grade_Content" ).slideToggle( "fast" );
		});
		
		
        $( "#room_Link" ).click(function() {
				$( "#room_Content" ).slideToggle( "fast" );
		});
        $( "#subject_Link" ).click(function() {
				$( "#subject_Content" ).slideToggle( "fast" );
		});
        $( "#accounting_Link" ).click(function() {
				$( "#acc_Content" ).slideToggle( "fast" );
		});
        $( "#Course-link" ).click(function() {
            $( "#Course-content" ).slideToggle( "fast" );
        });
		$( document ).ready(function() {
        });
        </script>
        
        <!-- End -->


<?php   } ?>		