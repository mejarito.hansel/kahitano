

<div class="panel panel-default">  
    
	<div class="panel-heading"> <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px"><i class='fa fa-calendar'></i> Calendar</h3> 
	</div>
	<div class="panel-body" style="padding:0">



			<div id="mini-clndr">
            
          <script id="mini-clndr-template" type="text/template">

            <div class="controls" style="background-color:#2a99d8">
              <div class="clndr-previous-button">&lsaquo;</div><div class="month"><%= month %> <%= year %></div><div class="clndr-next-button">&rsaquo;</div>
            </div>

            <div class="days-container">
              <div class="days">
                <div class="headers" style="background-color:#bc3d11">
                  <% _.each(daysOfTheWeek, function(day) { %><div class="day-header"><%= day %></div><% }); %>
                </div>
                <% _.each(days, function(day) { %><div class="<%= day.classes %>" id="<%= day.id %>"><%= day.day %></div><% }); %>
              </div>
              <div class="events">
                <div class="headers">
                  <div class="x-button">< Back</div>
                  <div class="event-header">EVENTS</div>
                </div>
                <div class="events-list">
                  <% _.each(eventsThisMonth, function(event) { %>
                    <div class="event">
                      <a href="<%= event.url %>"><%= moment(event.date).format('MMMM D') %>: <br><b><%= event.title %></b><br><%= event.content %></a>
                    </div>
                  <% }); %>
                </div>
              </div>
            </div>

          </script>
        </div>
<script>
$( document ).ready(function() {
$('#mini-clndr .clndr').css("border-bottom","7px solid #2a99d8");

$('#mini-clndr .clndr .days-container .events .event-header').css("background-color","#bc3d11");

});
</script>

<style>
#mini-clndr .clndr .controls .clndr-previous-button:hover,#mini-clndr .clndr .controls .clndr-next-button:hover{
background-color:#bc3d11;
}
#mini-clndr .clndr .days-container .events .x-button {
color:#ffffff;
}

#mini-clndr .clndr .days-container .days .day.event, #mini-clndr .clndr .days-container .days .empty.event {
background-color:#bc3d11;
color:#ffffff;
}
#mini-clndr .clndr .days-container .days .day.event:hover, #mini-clndr .clndr .days-container .days .empty.event:hover {
background-color:#2a99d8;
}

#mini-clndr .clndr .days-container .events .event-header {
background-color:#bc3d11;

}


</style>
                

	</div>
</div>  