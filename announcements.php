<?php
include("header.php");
?>
<div class="container">
	<div class="row">
    	<div class="col-lg-12" style="background-image: url('assets/images/banner copy2.png');
		height:210px;">
        </div>
    </div>
	<br>
    <div class="row">
        <h4 style='color:#2c3e50;'>
            <center>
                <span  style='color:#2c3e50; margin-right:5px;' class="fa fa-comments">  
                </span>
                <b>Announcements</b>
            </center>
        </h4>
        <!--Contents style="min-width: 550px;"-->
        <div class="col-xs-12">
            <div class="post">
                <h2 class="post-title">AMS Mobile 6.9 Alpha</h2>
                <span class="post-date" style="color:gray;">19 Aug 2015</span><br><br>
                <p style="font-size:18px;">Today is a special day for Mobi. Not only is it our fourth birthday, but after a year of development, we’re finally shipping the first alpha release of AMS Mobile 6.9. Hell yeah!</p>
                <p>AMS Mobile 6.9 has been a massive undertaking that touches nearly every line of code. We’re stoked to share it with you and hear your feedback. We’ve got a lot of news to share with you, so let’s jump right into it.</p>
            </div>
        </div>
    </div>

<?php
include("footer.php");
?>
</div>