<?php
include("header.php");
?>
<div class="container">
	<div class="row">
    	<div class="col-lg-12" style="background-image: url('assets/images/banner copy2.png');
		height:210px;">
        </div>
    </div>
	<br>
    <div class="row">
        <h4 style='color:#2c3e50;'>
            <center>
                <span  style='color:#2c3e50; margin-right:5px;' class="fa fa-tasks">  
                </span>
                <b>Grades</b>
            </center>
        </h4>
        <!--Contents style="min-width: 550px;"-->
        <div class="col-xs-12">
        <table class="table table-hover table-responsive table-striped text-md">
                <tr>
                    <td>
                        <b><i class="fa fa-book"></i>&nbsp;&nbsp;Course:
                    </td>
                    <td>
                        DK
                    </td>
                </tr>
                <tr>
                    <td>
                        <b><i class="fa fa-calendar"></i>&nbsp;&nbsp;School Year/Semester:
                    </td>
                    <td>
                        <select style="border:1px solid rgba(100,100,100,0);">
                            <option>2015-16 1st Trimester</option>
                            <option>2015-16 2nd Trimester</option>
                            <option>2015-16 3rd Trimester</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <hr/>
        <center><h4>2015-16 1st Trimester</h4></center>
        <div class="col-xs-12" style="overflow-y: auto;">
            <table class="table table-hover table-responsive table-striped text-md">
                <tbody>
                    <tr>                
                                        <th style="width:20%">Subject</th>
                                        <th style="width:20%">Units</th>
                                        <th style="width:20%">Grade</th>
                                        <th style="width:20%">Remarks</th>
                                        
                                       <th style="">Action</th>                   </tr>
                   
                    <tr>
                          <td>
                            
                            IT 215                          </td>
                            <td>3.00</td>
                            <td>
                            5.00                            
                            
                            </td>
                            
                          <td>FAILED                                             </td>
                            <td><a href="?action=remove&amp;id=9134&amp;si_ID=1" onclick="return confirm('Are you sure this is an invalid entry?');">[invalid entry]</a></td>
                        </tr>
                    <tr>
                          <td>
                            
                            IT 213                          </td>
                            <td>3.00</td>
                            <td>
                                                        
                            
                            </td>
                            
                          <td>NO GRADE                                             </td>
                            <td><a href="?action=remove&amp;id=9135&amp;si_ID=1" onclick="return confirm('Are you sure this is an invalid entry?');">[invalid entry]</a></td>
                        </tr>
                    <tr>
                          <td>
                            
                            IT 222                          </td>
                            <td>3.00</td>
                            <td>
                            DRP                            
                            
                            </td>
                            
                          <td>DROPPED                                             </td>
                            <td><a href="?action=remove&amp;id=9136&amp;si_ID=1" onclick="return confirm('Are you sure this is an invalid entry?');">[invalid entry]</a></td>
                        </tr>
                    <tr>
                          <td>
                            
                            NSTP 2                          </td>
                            <td>3.00</td>
                            <td>
                            INC                            
                            
                            </td>
                            
                          <td>INCOMPLETE                                             </td>
                            <td><a href="?action=remove&amp;id=9137&amp;si_ID=1" onclick="return confirm('Are you sure this is an invalid entry?');">[invalid entry]</a></td>
                        </tr> 
                    <tr>
                          <td>
                            
                            CS 313                          </td>
                            <td>3.00</td>
                            <td>
                            5.00                            
                            
                            </td>
                            
                          <td>FAILED                                             </td>
                            <td><a href="?action=remove&amp;id=9138&amp;si_ID=1" onclick="return confirm('Are you sure this is an invalid entry?');">[invalid entry]</a></td>
                        </tr>
                    <tr>
                          <td>
                            
                            IT 221                          </td>
                            <td>3.00</td>
                            <td>
                            DRP                            
                            
                            </td>
                            
                          <td>DROPPED                                             </td>
                            <td><a href="?action=remove&amp;id=9139&amp;si_ID=1" onclick="return confirm('Are you sure this is an invalid entry?');">[invalid entry]</a></td>
                        </tr>
                    <tr>
                          <td>
                            
                            ENG 2                          </td>
                            <td>3.00</td>
                            <td>
                            DRP                            
                            
                            </td>
                            
                          <td>DROPPED                                             </td>
                            <td><a href="?action=remove&amp;id=9140&amp;si_ID=1" onclick="return confirm('Are you sure this is an invalid entry?');">[invalid entry]</a></td>
                        </tr>
                    <tr>
                          <td>
                            
                            PE 1                          </td>
                            <td>2.00</td>
                            <td>
                            DRP                            
                            
                            </td>
                            
                          <td>DROPPED                                             </td>
                            <td><a href="?action=remove&amp;id=9141&amp;si_ID=1" onclick="return confirm('Are you sure this is an invalid entry?');">[invalid entry]</a></td>
                        </tr>
                </tbody>
            </table>
        </div>
    </div>

<?php
include("footer.php");
?>
</div>