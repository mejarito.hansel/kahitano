<?php
include("../old/init.php");
include(LAYOUTS_N . "header.php"); 

if (!SESSION::isLoggedIn()) {
	HTML::redirect();
}
?>
<div class="container-fluid">
	<?php #include(LAYOUTS_N . "banner.php");  ?>
	
    </div>
	<div class="container">
			<br><br><br>
			<div class="row col-md-8 col-md-offset-2">
				<center>
						<img name="logo" src="<?= IMG ?>f1.png" border="0" id="logo" alt="" width="100px" height="100px"
						style="box-shadow: 5px 5px  #888888;">
					<br><br><b><?=  strtoupper($user['account_LNAME']).", ".strtoupper($user['account_FNAME'])." ".strtoupper($user['account_MNAME']); ?></b><br>(<?= $user['access_NAME']; ?>)
				</center>
			</div>
	
		<div class="row col-md-8 col-md-offset-2">
				<h4 style='color:#2c3e50;'><center>
				<!--<span  style='color:#2c3e50;' class="fa fa-group">  
				</span>-->
					<b>My Subjects</b>
				</center></h4>
				<hr>
		</div>
		
		<div class="row col-md-10 col-md-offset-1">
		<?php
           switch($_GET['action']){
               case "view":
			   $sem_ID = clean($_GET['sem_ID']);
			   $sem = SEM::getSingleSem($sem_ID);
			   $isSemLoading = SEM::isSemLoading($sem_ID);
			   #print_r($isActiveEncoding);
				if (!$isSemLoading == 1) {
					HTML::redirect();
				}
			   ?>
			  
								<center><h4>Semester: <?php $sem = SEM::getSingleSem($sem_ID); echo $sem['sem_NAME']; ?></h4></center>
								<table class="table table-hover table-responsive table-striped text-md">
									
                                    <!-- <th style="width:2%;">#</th> -->
                                    <th style="width:13%;text-align:center;">Section</th>
                                    <th class="hidden-xs" style="width:18%;text-align:center;">Code / Description</th>
                                    
									<th class="visible-xs" style="width:25%;text-align:center;">Code</th>
									<th class="visible-xs" style="width:25%;text-align:center;">Schedule</th>
									
									
									
                                    <th class="hidden-xs"  style="width:5%;text-align:center;">Day</th>
                                    <th class="hidden-xs"  style="width:20%;text-align:center;">Time</th>
                                    <th class="hidden-xs"  style="width:7%;text-align:center;">Room</th>
									<th class="hidden-xs" style="width:5%;text-align:center;">Population</th>
                                    <th style="width:10%;text-align:center" >Operation</th>
										
                                    <?php
									
									$getmysubj = SUBJSCHED::getByfaculty(clean($_GET['sem_ID']), $instructor_ID);
									#print_r($getmysubj);
									$totalcount = count($getmysubj);
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                   // $viewSems = SEMSECTION::getAll(array("semester_section_ID" => "DESC"));
									
									
                                     if(count($getmysubj)>=50) 
                                     {
                                         $pagination2 = new Pagination($getmysubj, 50, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($getmysubj, 50, NULL);
                                     }

                                     $getmysubj = $pagination2->get_array();
									 $x = 1;

                                     if($getmysubj) {
                                        foreach($getmysubj as $key => $value){
											#var_dump($value);
                                    ?>
                                        <tr>
                                           <!-- <td style="vertical-align:middle;">
											<?php 
												if(isset($_GET['p']))
												{
												$p = $_GET['p'];
												
												$num = 	$x+($p*10)-10;
												echo $num;
												$x++;
												
												}else{
													echo $x++;
												}
											 ?>
											 -->
											 <td align="center"><?php $c=COURSE::getbyID($value['course_ID']); 
												echo $c['course_INIT']; ?><br>
                                            <?php 
                                              echo $value['semester_section_NAME']; ?>
                                            
                                          <td  class="hidden-xs"  align="center"> <?php $s = SUBJECTS::getID($value['subject_ID']);
												echo $s['subject_CODE']." <br> ".$s['subject_DESCRIPTION'];
												 ?> 
												 </td>
												 
												 
											<td class="visible-xs"  align="center"><?= $s['subject_CODE'] ?>  </td>
											
											
												 
												 
                                            <td class="hidden-xs"  align="center"><?= substr($value['day'],0,3); ?><br><?= substr($value['day2'],0,3) ?>  </td>
                                            <td class="hidden-xs"  align="center"><?= $value['start']; ?>-<?= $value['end']; ?><br>
											<?php if(isset($value['start2'])){  echo $value['start2']; ?>-<?= $value['end2']; } ?></td>
                                            <td class="hidden-xs"  align="center">
												<?php $room1 = ROOM::getID($value['room_ID']); ?>
												<?php echo $room1['room_NAME']; ?>
												<?php if(isset($value['room_ID2'])){ ?>
												<br>
												<?php $room2 = ROOM::getID($value['room_ID2']);
												echo $room2['room_NAME']; ?>
												<?php } ?>
											
											</td>
											
											
											<td class="visible-xs"  align="center">
														<?= substr($value['day'],0,1); ?>/<?php 
														
														$start = explode(':',$value['start']);
														echo $start[0]; 
														$start_2 = explode(' ',$start[1]);
														if($start_2[0] != '00')
														{
																echo ":".$start_2[0];
														}
														echo $start_2[1];
														
														?>
														
														
														
														-<?= $value['end']; ?> 
														
														
												<br><?= substr($value['day2'],0,1) ?>  </td>
											
											
                                            <td class="hidden-xs" align="center"> <?= SUBJSCHED::checkRemaining($value['subject_sched_ID']) ?></td>
											<td align="center">
											
													<a href="?action=encoding&subject_sched_ID=<?= $value['subject_sched_ID']; ?>&sem_ID=<?= $sem_ID ?>" class='btn btn-xs btn-info btn-block'>View Student</a>
													
													<!-- <a onclick="return popitup('../old/gs.php?id=<?php echo $value['subject_sched_ID']; ?>')" href="#" class='btn btn-xs btn-info btn-block'>Grading Sheet</a> -->
													
													
													<!-- <a onclick="return popitup('gs.php?id=<?php echo $value['subject_sched_ID']; ?>')" href="#" class='btn btn-success btn-xs'>GS</a> -->
                                            </td>
                                  </tr>
                                        <?php
                                         } 
                                        }
										
										#print_r($sem);
                                    ?>
								</table> 
                                Total Records: <?=  $totalcount; ?>
                                       
									<!--
									   <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view&sem_ID='.$sem[0]['sem_ID'].'&fac_id='.$instructor_ID.'') ?>
                                        </center>
									-->
		 
		 
					
					</div>
			   <script>
			   function popitup(url) {
					newwindow=window.open(url,'name','height=200,width=150');
					if (window.focus) {newwindow.focus()}
					return false;
				}
			   
			   </script>
			   <?php
			   break;
				case 'encoding':
						$subject_sched_ID = clean($_GET['subject_sched_ID']);
						$ss = SUBJSCHED::getSched($subject_sched_ID);
						$sem_ID = clean($_GET['sem_ID']);
						#<strong></strong>print_r($ss);
						?>
                        
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table>
                                <tr>
									<td width="50%">
										<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
											<i class="fa fa-folder-o"></i> View Student List  </h3>
									</td>
									<td width="5%">
									</td>
                                </tr>
                                </table>
                            </div>
							<div class="panel-body" >
                            <center>
							<h4><a href="faculty_view_students.php?action=view&sem_ID=<?=$sem_ID;?>">Semester: <?php $sem = SEM::getSingleSem($sem_ID); echo $sem['sem_NAME']; ?></a></h4>
									<b><?php $s = SUBJECTS::getID($ss['subject_ID']);
									echo $s['subject_CODE']."-".$s['subject_DESCRIPTION'];
									 ?></b> <br />
                                      <?php $c=COURSE::getbyID($ss['course_ID']); 
									echo $c['course_INIT']."-".$ss['semester_section_NAME'];
									?>
                                    <br />
									<?php echo $ss['day']; ?> 
                                    <br />
                                    <?php echo $ss['start']; ?> - <?php echo $ss['end']; ?> 
								<br />
								Lock Status:	
                                 <?php
                                 switch($ss['is_lock'])
								{
								  case 1 : echo "LOCKED"; break;
								  case 0 : echo "UNLOCKED";
								}
								?> 
                                -
        						Check Status: 
								<?php
								switch($ss['is_check'])
								{
								  case 1 : echo "CHECKED"; break;
								  case 0 : echo "UNCHECKED";
								}
								?> 
                            </center>
                            <br />
                            <br />
							
							
							
								
								<div id="display_hide">
								<form action="" method="get">
                                
                                <table class="table table-hover table-responsive table-striped text-md">
									<tr>
									<th style="width:5%;">#</th>
									<th style="width:15%;" >Student No.</th>
									<th style="width:30%;">Student Name</th>
									<th style="width:5%;">Course</th>
									
                                    <th style="widows:25%; text-align:center">Date Enrolled</th>
                                    <th style="width:20%;text-align:center">Contact No.</th>
                                    <?php
                                    //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    #$viewStudents = ROOM:: getallrooms(array("room_ID"=>"ASC"));
	                                 $stud =   SUBJSCHED::getBySubjectSchedID(clean($_GET['subject_sched_ID']),$instructor_ID);
									 #var_dump($stud);
									$x=1;
                                    ?>
                                       
                                     <?php foreach($stud as $key => $val) { ?>  
                                        <tr>
                                            <td><?= $x++; ?>
                                           <td><?php echo $val['student_ID']; ?>
										   <td><?php $u = USERS::getSingle(array("si_ID"=>"ASC"),$val['si_ID']);
													echo strtoupper($u['si_LNAME'].", ".$u['si_FNAME']);
												?>
                                            <td><?php echo USERS::getCourse($val['si_ID']); ?>
                                            <td align="center"><?php $c1 = SUBJENROL::checkdateenrolled($sem['sem_ID'],$val['si_ID']);
												echo $c1['date_enrolled']; ?>
											<td align="center"><?php echo $val['si_CONTACT']; ?></td>
																					
                                           
                                        
                                        </tr>
                                        
                                        <?php
                                      }
                                       
                                    ?>
								</table>
                                
								<center>
									<a href="faculty_view_students.php?action=view&sem_ID=<?= $sem_ID; ?>" class="btn btn-info" role="button">Go Back</a>
								</center>
								
                                </form> 
                               
						      </div>
                            </div>
						</div>
                            
					
                  <?php 
                    
				
				break;
				
				default: HTML::redirect();
		   }
        ?>
		</div>
	</div>	
			
<?php include(LAYOUTS_N . "footer.php"); ?>
