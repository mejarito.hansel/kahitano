<?php
include("../old/init.php");
include(LAYOUTS_N . "header.php"); 

if (!SESSION::isLoggedIn()) {
	HTML::redirect();
}
//QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
				#var_dump(Request::post());
           		ACCT::addExpense(Request::post(),$user['account_ID']);
			break;
			case "edit": 
				#var_dump(Request::post());
				ACCT::updateExpense(Request::post(),$user['account_ID']);
			
			 break;
			#case "update_blog":  BLOG::update(Request::post("blog_info")); break;
			
			#case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
            #case "enroll": CECONTROLLER::enroll($_GET['id']); break;
		    #case "drop": CECONTROLLER::drop($_GET['id']); break;
			case "delete": ACCT::hideExpense($_GET['id'],$user['account_ID']); break;
		    
		}
	}		
?>
<div class="container-fluid">
	<?php #include(LAYOUTS_N . "banner.php");  ?>
	
    </div>
	<div class="container">
			<br><br><br>
			<div class="row col-md-8 col-md-offset-2">
				<center>
						<img name="logo" src="<?= IMG ?>f1.png" border="0" id="logo" alt="" width="100px" height="100px"
						style="box-shadow: 5px 5px  #888888;">
					<br><br><b><?=  strtoupper($user	['account_LNAME']).", ".strtoupper($user['account_FNAME'])." ".strtoupper($user['account_MNAME']); ?></b><br>(<?= $user['access_NAME']; ?>)
				</center>
			</div>
	
		<div class="row col-md-8 col-md-offset-2">
				<h4 style='color:#2c3e50;'><center>
				<!--<span  style='color:#2c3e50;' class="fa fa-group">  
				</span>-->
					<b>Accounting Expense</b>
				</center></h4>
				<hr>
		</div>
		<div class="row col-md-8 col-md-offset-2">
		<?php if (SESSION::isLoggedIn()) { ?>
                    <?php
					switch($_GET['action']){
                        case "add":?>
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Expense</h3> 
                    </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <!-- Name -->
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>DATE</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ae_DATE" value="<?= date("m/d/Y"); ?>"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" class='form-control datepicker' required  >
                                </div>
                            </div>
                            
                            
                             <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>DEPARTMENT</label>
                                </div>
                                <div class="col-sm-8">
                                    <select class="form-control" name="department_ID" required>
                                    <option value="">Please Select</option>
                                    <?php $dept = DEPT::getAlldept(); ?>
                               
                                   	<?php foreach($dept as $key => $val) { ?>  
	                                  <option value="<?= $val['department_ID']; ?>"><?= $val['department_NAME']; ?></option>
                                 	<?php } ?>
                                    </select>
                                    
                                   
                                  
                                </div>
                            </div>
                            
                            
                            
                            
                            
                            
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>PARTICULARS</label>
                                </div>
                                <div class="col-sm-8">
                                    <input list="ae_PARTICULARS" name="ae_PARTICULARS"  autofocus class='form-control' required>
                                  
                                  <datalist id="ae_PARTICULARS">
                                  <?php $ae_PARTICULARS = ACCT::getAllAEParticulars(); ?>
                                  <?php #print_r($ae_PARTICULARS); ?>
                                   <?php foreach($ae_PARTICULARS as $key => $val) { ?>  
	                                  <option value="<?= $val['ae_PARTICULARS']; ?>">
                                 	<?php } ?>
                                </datalist>
                                  
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>AMOUNT</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ae_AMOUNT" autocomplete="off"   class='form-control' required  >
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-4">
                                    <label for="si_FNAME" class="">NOTE</label> 
                                </div>             
                                <div class="col-sm-8">
                                    <textarea name="NOTE" class="form-control"></textarea>
                                </div> 
                            </div>
                             <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>REQUEST BY</label> <i>(Optional)</i>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ae_REQUEST" list="ae_REQUEST"  class='form-control'   >
                                    
                                     <datalist id="ae_REQUEST">
									  <?php $ae_REQUEST = ACCT::getAllRequest(); ?>
										  <?php #print_r($ae_PARTICULARS); ?>
                                           <?php foreach($ae_REQUEST as $key => $val) { ?>  
                                              	<option value="<?= $val['ae_REQUEST']; ?>">
                                            <?php } ?>
                              		  </datalist>
                                    
                                    
                                    
                                </div>
                            </div>
                            
                             <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>RECEIVED BY</label> <i>(Optional)</i>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ae_RECEIVED" list="ae_RECEIVED"   class='form-control'   >
                                    
                                    
                                     <datalist id="ae_RECEIVED">
									  <?php $ae_RECEIVED = ACCT::getAllReceived(); ?>
										  <?php #print_r($ae_PARTICULARS); ?>
                                           <?php foreach($ae_RECEIVED as $key => $val) { ?>  
                                              	<option value="<?= $val['ae_RECEIVED']; ?>">
                                            <?php } ?>
                              		  </datalist>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button  class='btn btn-success btn-block' type='submit' ><i class='fa fa-edit'></i> Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break; 
                        case "edit":
                        $siiid =$_GET['id'];
                        $getsingle = ACCT::getSingleExpense($siiid);
						#print_r($getsingle);
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Edit Accounting Expense</h3> 
                    </div>
                    <div class="panel-body">
                         <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                            <fieldset>
                            <!-- Name -->
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>DATE</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ae_DATE" value="<?= date("m/d/Y",strtotime($getsingle['ae_DATE'])); ?>"  pattern="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d" class='form-control datepicker' required  >
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>DEPARTMENT</label>
                                </div>
                                <div class="col-sm-8">
                                    <select class="form-control" name="department_ID" required>
                                    <option value="">Please Select</option>
                                    <?php $dept = DEPT::getAlldept(); ?>
                               
                                   	<?php foreach($dept as $key => $val) { ?>  
	                                  <option <?php if($getsingle['department_ID'] == $val['department_ID']){ echo "SELECTED=SELECTED"; } ?> value="<?= $val['department_ID']; ?>"><?= $val['department_NAME']; ?></option>
                                 	<?php } ?>
                                    </select>
                                    
                                   
                                  
                                </div>
                            </div>
                            
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>PARTICULARS</label>
                                </div>
                                <div class="col-sm-8">
                                    <input list="ae_PARTICULARS" name="ae_PARTICULARS" value="<?= $getsingle['ae_PARTICULARS']; ?>"  autofocus class='form-control' required>
                                  
                                  <datalist id="ae_PARTICULARS">
                                  <?php $ae_PARTICULARS = ACCT::getAllAEParticulars(); ?>
                                  <?php #print_r($ae_PARTICULARS); ?>
                                   <?php foreach($ae_PARTICULARS as $key => $val) { ?>  
	                                  <option value="<?= $val['ae_PARTICULARS']; ?>">
                                 	<?php } ?>
                                </datalist>
                                  
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>AMOUNT</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ae_AMOUNT" value="<?= $getsingle['ae_AMOUNT']; ?>" autocomplete="off"   class='form-control' required  >
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-4">
                                    <label for="si_FNAME" class="">NOTE</label> 
                                </div>             
                                <div class="col-sm-8">
                                    <textarea name="NOTE" class="form-control"><?= $getsingle['NOTE']; ?></textarea>
                                </div> 
                            </div>
                             <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>REQUEST BY</label> <i>(Optional)</i>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ae_REQUEST" list="ae_REQUEST" value="<?= $getsingle['ae_REQUEST']; ?>"  class='form-control'   >
                                    
                                     <datalist id="ae_REQUEST">
									  <?php $ae_REQUEST = ACCT::getAllRequest(); ?>
										  <?php #print_r($ae_PARTICULARS); ?>
                                           <?php foreach($ae_REQUEST as $key => $val) { ?>  
                                              	<option value="<?= $val['ae_REQUEST']; ?>">
                                            <?php } ?>
                              		  </datalist>
                                    
                                    
                                    
                                </div>
                            </div>
                            
                             <div class="form-group">
                                <div class="col-sm-4">
                                    <label for='si_FNAME' class=''>RECEIVED BY</label> <i>(Optional)</i>
                                </div>
                                <div class="col-sm-8">
                                    <input type=text name="ae_RECEIVED" list="ae_RECEIVED" value="<?= $getsingle['ae_RECEIVED']; ?>"   class='form-control'   >
                                    
                                    
                                     <datalist id="ae_RECEIVED">
									  <?php $ae_RECEIVED = ACCT::getAllReceived(); ?>
										  <?php #print_r($ae_PARTICULARS); ?>
                                           <?php foreach($ae_RECEIVED as $key => $val) { ?>  
                                              	<option value="<?= $val['ae_RECEIVED']; ?>">
                                            <?php } ?>
                              		  </datalist>
                                    
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-12">
                                
                                <input type="hidden" value="<?= $getsingle['ae_ID']; ?>" name="ae_ID" />
                                
                                    <button  class='btn btn-success btn-block' type='submit' ><i class='fa fa-edit'></i> Submit</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                    </div>
                </div>
                    <?php break;  
                        case "search":
						include "searching.php";						
						?>
							
                    <?php break; 
                        
                        case "view":?>
                        <?php
								if(isset($_GET['id']))
								{
                                }else{
					?>
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table>
                                <tr>
                                <td width="50%">
                                    <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                                    <i class="fa fa-folder-o"></i> View Accounting Expenses</h3> 
                                    <sub><a id="daily" style="cursor:pointer;">[Daily Report]</a></sub> <sub><a style="cursor:pointer;" id="monthly">[Monthly Report]</a></sub> <!--<sub>[Custom Report]</sub>-->
                                    
                                    
                                    
                                    
                                    
                                    <div id="dialog" style="display:none;">
                                        <form id="" action="expense_report.php?action=daily" method="POST" target="_blank">
                                            <input type="hidden" autofocus />
                                            <b> Date:</b>   <input type="text" value="<?= date("m/d/Y"); ?>" class="datepicker"  name="date" />
                                              <br /><br />
                                              <b>Show Particulars:  </b>
											<br />
                                            <input type="checkbox" checked="checked"  id="selecctall" /> Select All<br />
                                            <div style="overflow-y: scroll; min-height:50px;max-height:200px;">
                                            <?php $ae_PARTICULARS = ACCT::getAllAEParticulars(); ?>
												  <?php #print_r($ae_PARTICULARS); ?>
                                                   <?php foreach($ae_PARTICULARS as $key => $val) { ?>  
                                                      <input checked="checked" type="checkbox" class="cb" value="<?= $val['ae_PARTICULARS']; ?>"> <?= $val['ae_PARTICULARS']; ?> <br />
                                                    <?php } ?>
                                          	</div>  <br />  
                                            <input type="hidden" name="show" id="tags" /> <input type="submit" value="Generate" />
                                        </form>
                                    </div>
                                    
                                    
                                    <div id="dialog1" style="display:none;">
                                        <form id="" action="expense_report.php?action=monthly" method="POST" target="_blank">
                                            <input type="hidden" autofocus />
                                            <b> Month:</b>   <input type="text" value="<?= date("F Y"); ?>" class="monthPicker"  name="date" />
                                              <br /><br />
                                              
                                             <b>Display:</b><br />
                                      				<input type="radio" name="by" value="custom" checked="checked" /> Specified Particulars Below<br />
                                                    <input type="radio" name="by" value="only" /> Existing Particulars Only
											<br />
                                            
                                            
                                            <br /><b>Show:</b><br />
                                            <input type="checkbox" checked="checked"  id="selecctall2" /> Select All<br />
                                            <div style="overflow-y: scroll; min-height:50px;max-height:200px;">
                                            <?php $ae_PARTICULARS = ACCT::getAllAEParticulars(); ?>
												  <?php #print_r($ae_PARTICULARS); ?>
                                                   <?php foreach($ae_PARTICULARS as $key => $val) { ?>  
                                                      <input checked="checked" type="checkbox" class="cb2" value="<?= $val['ae_PARTICULARS']; ?>"> <?= $val['ae_PARTICULARS']; ?> <br />
                                                    <?php } ?>
                                          	</div> 
                                      <br />
                                             
                                            <input type="hidden" name="show" id="tags2" /> <input type="submit" value="Generate" />
                                        </form>
                                    </div>
                                    
                                    
                                    
                                    
                                    <script type="text/javascript">
										$(document).ready(function () {
										
										
										$('#selecctall').click(function(event) {  //on click 
											if(this.checked) { // check select status
												$('.cb').each(function() { //loop through each checkbox
													this.checked = true;  //select all checkboxes with class "checkbox1"               
												});
												Populate()
												
											}else{
												$('.cb').each(function() { //loop through each checkbox
													this.checked = false; //deselect all checkboxes with class "checkbox1"                       
												});         
												Populate()
											}
										});
										
										
											
										$('#selecctall2').click(function(event) {  //on click 
											if(this.checked) { // check select status
												$('.cb2').each(function() { //loop through each checkbox
													this.checked = true;  //select all checkboxes with class "checkbox1"               
												});
												Populate2()
												
											}else{
												$('.cb2').each(function() { //loop through each checkbox
													this.checked = false; //deselect all checkboxes with class "checkbox1"                       
												});         
												Populate2()
											}
										});
										
										
										
										
										
											$("#dialog").dialog({ autoOpen: false });
											
											
											$("#dialog1").dialog({ autoOpen: false });
									 
											$("#daily").click(
												function () {
													$("#dialog").dialog({ title: 'Daily Report' });
													$("#dialog").dialog('open');
													return false;
												}
											);
											
											$("#monthly").click(
												function () {
													$("#dialog1").dialog({ title: 'Monthly Report' });
													$("#dialog1").dialog('open');
													return false;
												}
											);
												
										
										
										
										/* MONTH PICKER */
										$(".monthPicker").datepicker({
												dateFormat: 'MM yy',
												changeMonth: true,
												changeYear: true,
												showButtonPanel: true,
										
												onClose: function(dateText, inst) {
													var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
													var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
													$(this).val($.datepicker.formatDate('MM yy', new Date(year, month, 1)));
												}
											});
										
											$(".monthPicker").focus(function () {
												$(".ui-datepicker-calendar").hide();
												$("#ui-datepicker-div").position({
													my: "center top",
													at: "center bottom",
													of: $(this)
												});
											});
										
										/* MONTH PICKER */
										
										
										
										
										
											
										});
										
										
										
										
										
										
										
										
										function Populate(){
											vals = $('input[class="cb"]:checked').map(function() {
												return this.value;
											}).get().join(',');
											console.log(vals);
											$('#tags').val(vals);
										}
										
										$('input[class="cb"]').on('change', function() {
											Populate()
										}).change();
										
										
										
										function Populate2(){
											vals = $('input[class="cb2"]:checked').map(function() {
												return this.value;
											}).get().join(',');
											console.log(vals);
											$('#tags2').val(vals);
										}
										
										$('input[class="cb2"]').on('change', function() {
											Populate2()
										}).change();
										
										
										
									</script>
									
                                    
                                    
                                </td>
                               
                                <td >
                                    <div class="input-group col-md-12">
                                      
                                      	<form id="form1" runat="server">
                                        		<input id="searchItem" autocomplete="off" class="searchItem form-control"  name="q" type="text" onkeyup="term(q.value)" autofocus="autofocus" onchange="term(q.value)"/>
                                        </form>
                                      
                                        <script>
                                    				$(document).ready(function() {
													
													
													
													
												$('#searchItem').keyup(function(){
												var s_Item = $('#searchItem').val();
													
													if(s_Item.length >= 3)
													{
														 $.ajax({
															type: "POST",
															url: "search.php?action=accounting_expense.php",
															data: 'search_term=' + s_Item,
																success: function(msg){
																	/* $('#resultip').html(msg); */
																		$("#display_result").show();
																		$("#display_result").html(msg);
																		
																		$("#display_hide").hide();
																		$("#pagination").hide();
																	
																}
											
															}); // Ajax Call
													
															//alert(s_Item);
														}else{
															$("#display_result").hide();   
															$("#display_hide").show();
															
														}
													});
												});			
                                    			</script>
                                      <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                    </div>
                                </td>
                                
                                <td width="5%">
                                <div class="col-md-1">   
                                <button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('?action=add','_self');"><i class='fa fa-plus'></i></button>
                                </div>
                                </td>
                                </tr>
                                </table>
                            </div>
							<div class="panel-body" >
                                <div id="display_result"></div>
                                <div id="display_hide">
								<table class="table table-hover table-responsive table-striped text-md">
									  <tr><th style="width:10%;">Date</th>
                                        <th style="width:15%;">Department</th>
									    <th style="width:15%;">Particulars</th>
									    <th style="width:15%;">Amount</th>
									    <th style="width:15%">Note</th>
									    <th style="width:15%;">Operation</th>
                                    
                                    <?php
                                   
                                    $getExpense = ACCT:: getAllExpenseActive(array("ae_DATE"=>"DESC"));
									
									
									
                                     if(count($getExpense)>=10) 
                                     {
                                         $pagination2 = new Pagination($getExpense, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($getExpense, 10, NULL);
                                     }

                                     $getExpense = $pagination2->get_array();
                                     if($getExpense) {
                                        foreach($getExpense as $key => $value){
                                    ?>
                                        <tr>
                                          <td>
                                          <?= date("m/d/Y",strtotime($value['ae_DATE'])); ?>
                                          <td>
                                          <?php
										  $dept = DEPT::getdeptbyID($value['department_ID']); 
										  echo $dept['department_NAME'];
										  
										  ?>
                                          <td>
                                          <?= $value['ae_PARTICULARS']; ?>
                                          <td>
                                          <?= money($value['ae_AMOUNT']);  ?>
                                          <td>   
                                              <?= $value['NOTE'];  ?>                              
                                          <td>
                                         <a href="?action=edit&id=<?= $value['ae_ID']; ?>">Edit</a> | <a onclick="return confirm('Are you sure to remove this invalid entry?')" href="?action=delete&id=<?= $value['ae_ID']; ?>">Invalid Entry</a>                                          </td>
                                        </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
          <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                                        </center>
							     </div>
                            </div>
						</div>
                            
					</div>
                </div>
                  <?php } break; ?>
				  
                    <?php 
                        case "delete":?>
                        
                <?php break;
                    }}else{?>
                        echo 'Error: 404 Not found.';
                <?php } ?>
		</div>
	
	
       
	
			
<?php include(LAYOUTS_N . "footer.php"); ?>
