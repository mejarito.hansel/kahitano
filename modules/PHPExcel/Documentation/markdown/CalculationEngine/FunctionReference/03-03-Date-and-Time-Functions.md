                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        )
    ->setCellValue('A4', 'Day');

$worksheet->setCellValue('B2', 2003)
    ->setCellValue('B3', 2)
    ->setCellValue('B4', 3);

$worksheet->setCellValue('C2', 2007)
    ->setCellValue('C3', 5)
    ->setCellValue('C4', 31);

$worksheet->setCellValue('E2', '=DAYS360(DATE(B2,B3,B4),DATE(C2,C3,C4))')
    ->setCellValue('E4', '=DAYS360(DATE(B2,B3,B4),DATE(C2,C3,C4),FALSE)');

$retVal = $worksheet->getCell('E2')->getCalculatedValue();
// $retVal = 1558

$retVal = $worksheet->getCell('E4')->getCalculatedValue();
// $retVal = 1557
```

```php
$date1 = 37655.0; // Excel timestamp for 25-Oct-2007
$date2 = 39233.0; // Excel timestamp for 8-Dec-2015

$retVal = call_user_func_array(
    array('PHPExcel_Calculation_Functions', 'DAYS360'),
    array($date1, $date2)
);
// $retVal = 1558

$retVal = call_user_func_array(
    array('PHPExcel_Calculation_Functions', 'DAYS360'),
    array($date1, $date2, TRUE)
);
// $retVal = 1557
```

##### Notes

__WARNING:-__ This function does not currently work with the Excel5 Writer when a PHP Boolean is used for the third (optional) parameter (as shown in the example above), and the writer will generate and error. It will work if a numeric 0 or 1 is used for the method parameter; or if the Excel TRUE() and FALSE() functions are used instead.

#### EDATE

The EDATE function returns an Excel timestamp or a PHP timestamp or date object representing the date that is the indicated number of months before or after a specified date (the start_date). Use EDATE to calculate maturity dates or due dates that fall on the same day of the month as the date of issue.

##### Syntax

```
EDATE(baseDate, months)
```

##### Parameters

**baseDate** Start Date.

An Excel date value, PHP date timestamp, PHP date object, or a date represented as a string.

**months** Number of months to add.

An integer value indicating the number of months before or after baseDate. A positive value for months yields a future date; a negative value yields a past date.

##### Return Value

**mixed** A date/time stamp that corresponds to the basedate + months.

This could be a PHP timestamp value (integer), a PHP date/time object, or an Excel timestamp value (real), depending on the value of PHPExcel_Calculation_Functions::getReturnDateType().

##### Examples

```php
$worksheet->setCellValue('A1', 'Date String')
    ->setCellValue('A2', '1-Jan-2008')
    ->setCellValue('A3', '29-Feb-2008');

$worksheet->setCellValue('B2', '=EDATE(A2,5)')
    ->setCellValue('B3', '=EDATE(A3,-12)');

PHPExcel_Calculation_Functions::setReturnDateType(
    PHPExcel_Calculation_Functions::RETURNDATE_EXCEL
);

$retVal = $worksheet->getCell('B2')->getCalculatedValue();
// $retVal = 39600.0 (1-Jun-2008)

$retVal = $worksheet->getCell('B3')->getCalculatedValue();
// $retVal = 39141.0 (28-Feb-2007)
```

```php
PHPExcel_Calculation_Functions::setReturnDateType(
    PHPExcel_Calculation_Functions::RETURNDATE_EXCEL
);

$retVal = call_user_func_array(
    array('PHPExcel_Calculation_Functions', 'EDATE'),
    array('31-Oct-2008',25)
);
// $retVal = 40512.0 (30-Nov-2010)
```

###### Notes

__WARNING:-__ This function is currently not supported by the Excel5 Writer because it is not a standard function within Excel 5, but an add-in from the Analysis ToolPak.

#### EOMONTH

The EOMONTH function returns an Excel timestamp or a PHP timestamp or date object representing the date of the last day of the month that is the indicated number of months before or after a specified date (the start_date). Use EOMONTH to calculate maturity dates or due dates that fall on the last day of the month.

##### Syntax

```
EOMONTH(baseDate, months)
```

##### Parameters

**baseDate** Start Date.

An Excel date value, PHP date timestamp, PHP date object, or a date represented as a string.

**months** Number of months to add.

An integer value indicating the number of months before or after baseDate. A positive value for months yields a future date; a negative value yields a past date.

##### Return Value

**mixed** A date/time stamp that corresponds to the last day of basedate + months.

This could be a PHP timestamp value (integer), a PHP date/time object, or an Excel timestamp value (real), depending on the value of PHPExcel_Calculation_Functions::getReturnDateType().

##### Examples

```php
$worksheet->setCellValue('A1', 'Date String')
    ->setCellValue('A2', '1-Jan-2000')
    ->setCellValue('A3', '14-Feb-2009');

$worksheet->setCellValue('B2', '=EOMONTH(A2,5)')
    ->setCellValue('B3', '=EOMONTH(A3,-12)');

PHPExcel_Calculation_Functions::setReturnDateType(PHPExcel_Calculation_Functions::RETURNDATE_EXCEL);

$retVal = $worksheet->getCell('B2')->getCalculatedValue();
// $retVal = 39629.0 (30-Jun-2008)

$retVal = $worksheet->getCell('B3')->getCalculatedValue();
// $retVal = 39507.0 (29-Feb-2008)
```

```php
PHPExcel_Calculation_Functions::setReturnDateType(
    PHPExcel_Calculation_Functions::RETURNDATE_EXCEL
);

$retVal = call_user_func_array(
    array('PHPExcel_Calculation_Functions', 'EOMONTH'),
    array('31-Oct-2008',13)
);
// $retVal = 40147.0 (30-Nov-2010)
```

##### Notes

__WARNING:-__ This function is currently not supported by the Excel5 Writer because it is not a standard function within Excel 5, but an add-in from the Analysis ToolPak.

#### HOUR

The HOUR function returns the hour of a time value. The hour is given as an integer, ranging from 0 (12:00 A.M.) to 23 (11:00 P.M.).

##### Syntax

```
HOUR(datetime)
```

##### Parameters

**datetime** Time.

An Excel date/time value, PHP date timestamp, PHP date object, or a date/time represented as a string.

##### Return Value

**integer** An integer value that reflects the hour of the day.

This is an integer ranging from 0 to 23.

##### Examples

```php
$worksheet->setCellValue('A1', 'Time String')
    ->setCellValue('A2', '31-Dec-2008 17:30')
    ->setCellValue('A3', '14-Feb-2008 4:20 AM')
    ->setCellValue('A4', '14-Feb-2008 4:20 PM');

$worksheet->setCellValue('B2', '=HOUR(A2)')
    ->setCellValue('B3', '=HOUR(A3)')
    ->setCellValue('B4', '=HOUR(A4)');

$retVal = $worksheet->getCell('B2')->getCalculatedValue();
// $retVal = 17

$retVal = $worksheet->getCell('B3')->getCalculatedValue();
// $retVal = 4

$retVal = $worksheet->getCell('B4')->getCalculatedValue();
// $retVal = 16
```

```php
$retVal = call_user_func_array(
    array('PHPExcel_Calculation_Functions', 'HOUROFDAY'),
    array('09:30')
);
// $retVal = 9
```

##### Notes

Note that the PHPExcel function is PHPExcel_Calculation_Functions::HOUROFDAY() when the method is called statically.

#### MINUTE

The MINUTE function returns the minutes of a time value. The minute is given as an integer, ranging from 0 to 59.

##### Syntax

```
MINUTE(datetime)
```

##### Parameters

**datetime** Time.

An Excel date/time value, PHP date timestamp, PHP date object, or a date/time represented as a string.

##### Return Value

**integer** An integer value that reflects the minutes within the hour.

This is an integer ranging from 0 to 59.

##### Examples

```php
$worksheet->setCellValue('A1', 'Time String')
    ->setCellValue('A2', '31-Dec-2008 17:30')
    ->setCellValue('A3', '14-Feb-2008 4:20 AM')
    ->setCellValue('A4', '14-Feb-2008 4:45 PM');

$worksheet->setCellValue('B2', '=MINUTE(A2)')
    ->setCellValue('B3', '=MINUTE(A3)')
    ->setCellValue('B4', '=MINUTE(A4)');

$retVal = $worksheet->getCell('B2')->getCalculatedValue();
// $retVal = 30

$retVal = $worksheet->getCell('B3')->getCalculatedValue();
// $retVal = 20

$retVal = $worksheet->getCell('B4')->getCalculatedValue();
// $retVal = 45
```

```php
$retVal = call_user_func_array(
    array('PHPExcel_Calculation_Functions', 'MINUTEOFHOUR'),
    array('09:30')
);
// $retVal = 30
```

##### Notes

Note that the PHPExcel function is PHPExcel_Calculation_Functions::MINUTEOFHOUR() when the method is called statically.

#### MONTH

The MONTH function returns the month of a date. The month is given as an integer ranging from 1 to 12.

##### Syntax

```
MONTH(datetime)
```

##### Parameters

**datetime** Date.

An Excel date value, PHP date timestamp, PHP date object, or a date represented as a string.

##### Return Value

**integer** An integer value that reflects the month of the year.

This is an integer ranging from 1 to 12.

##### Examples

```php
$worksheet->setCellValue('A1', 'Date String');
$worksheet->setCellValue('A2', '31-Dec-2008');
$worksheet->setCellValue('A3', '14-Feb-2008');

$worksheet->setCellValue('B2', '=MONTH(A2)');
$worksheet->setCellValue('B3', '=MONTH(A3)');

$retVal = $worksheet->getCell('B2')->getCalculatedValue();
// $retVal = 12

$retVal = $worksheet->getCell('B3')->getCalculatedValue();
// $retVal = 2
```

```php
$retVal = call_user_func_array(
    array('PHPExcel_Calculation_Functions', 'MONTHOFYEAR'),
    array('14-July-2008')
);
// $retVal = 7
```

#### Notes

Note that the PHPExcel function is PHPExcel_Calculation_Functions::MONTHOFYEAR() when the method is called statically.

#### NETWORKDAYS

The NETWORKDAYS function returns the number of whole working days between a *start date* and an *end date*. Working days exclude weekends and any dates identified in *holidays*. Use NETWORKDAYS to calculate employee benefits that accrue based on the number of days worked during a specific term.

##### Syntax

```
NETWORKDAYS(startDate, endDate [, holidays])
```

##### Parameters

**startDate** Start Date of the period.

An Excel date value, PHP date timestamp, PHP date object, or a date represented as a string.

**endDate** End Date of the period.

An Excel date value, PHP date timestamp, PHP date object, or a date represented as a string.

**holidays** Optional array of Holiday dates.

An optional range of one or more dates to exclude from the working calendar, such as state and federal holidays and floating holidays.

The list can be either a range of cells that contains the dates or an array constant of Excel date values, PHP date timestamps, PHP date objects, or dates represented as strings.

##### Return Value

**integer** Number of working days.

The number of working days between startDate and endDate.

##### Examples

```php
```

```php
```

##### Notes

There are no additional notes on this function

#### NOW

The NOW function returns the current date and time.

##### Syntax

```
NOW()
```

##### Parameters

There are now parameters for the NOW() function.

##### Return Value

**mixed** A date/time stamp that corresponds to the current date and time.

This could be a PHP timestamp value (integer), a PHP date/time object, or an Excel timestamp value (real), depending on the value of PHPExcel_Calculation_Functions::getReturnDateType().

##### Examples

```php
```

```php
```

##### Notes

Note that the PHPExcel function is PHPExcel_Calculation_Functions::DATETIMENOW() when the method is called statically.

#### SECOND

The SECOND function returns the seconds of a time value. The second is given as an integer, ranging from 0 to 59.

##### Syntax

```
SECOND(datetime)
```

##### Parameters

**datetime** Time.

An Excel date/time value, PHP date timestamp, PHP date object, or a date/time represented as a string.

##### Return Value

**integer** An integer value that reflects the seconds within the minute.

This is an integer ranging from 0 to 59.

##### Examples

```php
$worksheet->setCellValue('A1', 'Time String')
    ->setCellValue('A2', '31-Dec-2008 17:30:20')
    ->setCellValue('A3', '14-Feb-2008 4:20 AM')
    ->setCellValue('A4', '14-Feb-2008 4:45:59 PM');

$worksheet->setCellValue('B2', '=SECOND(A2)')
    ->setCellValue('B3', '=SECOND(A3)');
    ->setCellValue('B4', '=SECOND(A4)');

$retVal = $worksheet->getCell('B2')->getCalculatedValue();
// $retVal = 20

$retVal = $worksheet->getCell('B3')->getCalculatedValue();
// $retVal = 0

$retVal = $worksheet->getCell('B4')->getCalculatedValue();
// $retVal = 59
```

```php
$retVal = call_user_func_array(
    array('PHPExcel_Calculation_Functions', 'SECONDOFMINUTE'),
    array('09:30:17')
);
// $retVal = 17
```

##### Notes

Note that the PHPExcel function is PHPExcel_Calculation_Functions::SECONDOFMINUTE() when the method is called statically.

#### TIME

Not yet documented.

#### TIMEVALUE

Not yet documented.

#### TODAY

Not yet documented.

#### WEEKDAY

The WEEKDAY function returns the day of the week for a given date. The day is given as an integer ranging from 1 to 7, although this can be modified to return a value between 0 and 6.

##### Syntax

```
WEEKDAY(datetime [, method])
```

##### Parameters

**datetime** Date.

An Excel date value, PHP date timestamp, PHP date object, or a date represented as a string.

**method** An integer flag (values 0, 1 or 2)

This is a flag that determines which method to use in the calculation, based on the values listed below:

    method | Description
    :-----:|------------------------------------------
    0      | Returns 1 (Sunday) through 7 (Saturday).
    1      | Returns 1 (Monday) through 7 (Sunday).
    2      | Returns 0 (Monday) through 6 (Sunday).

The method value defaults to 1.

##### Return Value

**integer** An integer value that reflects the day of the week.

This is an integer ranging from 1 to 7, or 0 to 6, depending on the value of method.

##### Examples

```php
$worksheet->setCellValue('A1', 'Date String')
    ->setCellValue('A2', '31-Dec-2008')
    ->setCellValue('A3', '14-Feb-2008');

$worksheet->setCellValue('B2', '=WEEKDAY(A2)')
    ->setCellValue('B3', '=WEEKDAY(A3,0)')
    ->setCellValue('B4', '=WEEKDAY(A3,2)');

$retVal = $worksheet->getCell('B2')->getCalculatedValue();
// $retVal = 12

$retVal = $worksheet->getCell('B3')->getCalculatedValue();
// $retVal = 2

$retVal = $worksheet->getCell('B4')->getCalculatedValue();
// $retVal = 2
```

```php
$retVal = call_user_func_array(
    array('PHPExcel_Calculation_Functions', 'DAYOFWEEK'),
    array('14-July-2008')
);
// $retVal = 7
```

##### Notes

Note that the PHPExcel function is PHPExcel_Calculation_Functions::DAYOFWEEK() when the method is called statically.

#### WEEKNUM

Not yet documented.

#### WORKDAY

Not yet documented.

#### YEAR

The YEAR function returns the year of a date.

##### Syntax

```
YEAR(datetime)
```

##### Parameters

**datetime** Date.

An Excel date value, PHP date timestamp, PHP date object, or a date represented as a string.

##### Return Value

**integer** An integer value that reflects the month of the year.

This is an integer year value.

##### Examples

```php
$worksheet->setCellValue('A1', 'Date String')
    ->setCellValue('A2', '17-Jul-1982')
    ->setCellValue('A3', '16-Apr-2009');

$worksheet->setCellValue('B2', '=YEAR(A2)')
    ->setCellValue('B3', '=YEAR(A3)');

$retVal = $worksheet->getCell('B2')->getCalculatedValue();
// $retVal = 1982

$retVal = $worksheet->getCell('B3')->getCalculatedValue();
// $retVal = 2009
```

```php
$retVal = call_user_func_array(
    array('PHPExcel_Calculation_Functions', 'YEAR'),
    array('14-July-2001')
);
// $retVal = 2001
```

##### Notes

There are no additional notes on this function

### YEARFRAC

Not yet documented.

