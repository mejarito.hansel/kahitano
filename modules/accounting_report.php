<?php
include("../old/init.php");
include(LAYOUTS_N . "header.php"); 
?>

<div class="container-fluid">
	<?php #include(LAYOUTS_N . "banner.php");  ?>
</div>
	<div class="container">
		<br><br><br>
			<div class="row col-md-8 col-md-offset-2">
				<center>
						<img name="logo" src="<?= IMG ?>f1.png" border="0" id="logo" alt="" width="100px" height="100px"
						style="box-shadow: 5px 5px  #888888;">
					<br><br><b><?=  strtoupper($user['account_LNAME']).", ".strtoupper($user['account_FNAME'])." ".strtoupper($user['account_MNAME']); ?></b><br>(<?= $user['access_NAME']; ?>)
				</center>
			</div>
			<div class="row col-md-8 col-md-offset-2">
					<h4 style='color:#2c3e50;'><center>
					<?php
					$action = clean($_GET['action']);
					switch($action){
						
						case "collection":
					?>
					<span  style='color:#2c3e50;' class="fa fa-search"></span>
						<b>Collection Report</b>
						<?php
						break;
						case "expense":
						?>
						<span  style='color:#2c3e50;' class="fa fa-shopping-cart"></span>
						<b>Expense Report</b>
						<?php
						break;
						case "deposit":
						?>
						<span  style='color:#2c3e50;' class="fa fa-money"></span>
						<b>Deposit Report</b>
						<?php
						}
						?>
						</center>
					</h4>
					<hr>
			</div>
			<div class="row  col-xs-10 col-md-8 col-xs-offset-1 col-md-offset-2 ">
			<?php if($action == 'collection'){ ?>
			<form>
				<div>Semester: 
							<input type="hidden" name="action" value="<?= $action; ?>">
							<select name="sem_ID" class="form-control" onchange="this.form.submit()">
							<?php
								$selected_sem = clean($_GET['sem_ID']);
								$sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
								foreach($sem as $key => $val)
								{
								?>
									<option <?php if($val['sem_ID'] == $selected_sem){ echo "SELECTED=SELECTED"; } ?> value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
								
								<?php
								}
							?>
							</select>
				</div>
			</form><?php } ?>	<br>
			<?php
			
			switch($action){
				case "collection":
			?>	
					
					<div class="tab-pane fade in active">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#home">Tabular</a></li>
							<li ><a data-toggle="tab" href="#menu1">Graph</a></li>
						</ul>
						<div class="tab-content">
						  <div id="home" class="tab-pane fade in active">
							<table class="table table-hover table-responsive table-striped text-md">
								<tr>
									
									<th width="50%" style="text-align:center">Date</th>
									<th width="50%" style="text-align:center">Amount</th>
									<!--<th width="" style="text-align:center">Action</th>-->
								</tr>	
								<?php 
								$data = ACCT::getAllPaySem($selected_sem,array("payment_DATE"=>"DESC"));
								#var_dump($data);
								
								
								 if(count($data)>=10) 
                                         {
                                             $pagination2 = new Pagination($data, 10, Request::get("p"));
                                         }else{
                                             $pagination2 = new Pagination($data, 10, NULL);
                                         }

                                         $data = $pagination2->get_array();

                                         if($data) {

                                            foreach($data as $key => $value){
								?>
								<tr>
									<td  class="hidden-xs" style="text-align:center">
										<?php echo $date = date("F j, Y",strtotime($value['payment_DATE'])); ?>
									</td>
									<td  class="visible-xs" style="text-align:center">
										<?php echo $date = date("m/d/Y",strtotime($value['payment_DATE'])); 
										$date = date("Y-m-d",strtotime($value['payment_DATE']));
										?>
									</td>
									<td style="text-align:center">&#x20B1; <?= money($value['total']); ?></td>
									<!--<td style="text-align:center"> <button id="<?= $date; ?>" class="btn btn-primary btn-xs btn-details">View Details</button> </td>-->
								</tr>	
								<?php } ?>
								<?php } ?>
							</table>
								<center>
									<span id="pagination">
										<?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action='.$action."&sem_ID=".$selected_sem.'') ?>
                                    </span>
                                </center>
						  </div>
						  <div id="menu1" class="tab-pane fade in ">
							<div>
								<table align="center">
									<tr>
										<td width="20%"><h2><i class="fa fa-lg fa-exclamation-triangle"></i></h2></td>
										<td width="80%"><h2>Sorry, This feature <br>is not yet available!</h2></td>
									</tr>
								</table>
						
							</div>
						  </div>
						</div>
						
					</div>	
					
					
					<!-- Modal Accounting -->
					<div id="collection_view" class="modal fade" role="dialog">
					  <div class="modal-dialog modal-md">
						<!-- Modal content-->
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title"><i class="fa fa-bank"></i> Collection Details: <span id="date_detail" style="color:black"></span>   </h4>
						  </div>
						  <div class="modal-body">
								
						  </div>

						  <div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						  </div>
						</div>

					  </div>
					</div>
					
					<script>
					  $(document).ready(function(){
						  $('.btn-details').on('click', function(){
								//alert(this.id);
								var date_detail = this.id;
								$('#date_detail').text(date_detail);
								
								$('#collection_view').modal('show');
						   });
					  });
					</script>
				<?php
						break;
						case "expense":
						?>
						
					<div class="tab-pane fade in active">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#home">Tabular</a></li>
							<li ><a data-toggle="tab" href="#menu1">Graph</a></li>
						</ul>
						<div class="tab-content">
						  <div id="home" class="tab-pane fade in active">
							
							<?php 
								$data = ACCT::getAllExpensePerday(array("ae_DATE"=>"DESC"));
								#var_dump($data);
								
								
								 if(count($data)>=10) 
                                         {
                                             $pagination2 = new Pagination($data, 10, Request::get("p"));
                                         }else{
                                             $pagination2 = new Pagination($data, 10, NULL);
                                         }

                                         $data = $pagination2->get_array();

                                if($data) {
							?>		
							<table class="table table-hover table-responsive table-striped text-md">
								<tr>
									
									<th width="50%" style="text-align:center">Date</th>
									<th width="50%" style="text-align:center">Amount</th>
									<!--<th width="" style="text-align:center">Action</th>-->
								</tr>	
								
								<?php
                                foreach($data as $key => $value){
								?>
								<tr>
									<td  class="hidden-xs" style="text-align:center">
										<?php echo $date = date("F j, Y",strtotime($value['ae_DATE'])); ?>
									</td>
									<td  class="visible-xs" style="text-align:center">
										<?php echo $date = date("m/d/Y",strtotime($value['ae_DATE'])); 
										$date = date("Y-m-d",strtotime($value['payment_DATE']));
										?>
									</td>
									<td style="text-align:center">&#x20B1; <?= money($value['total']); ?></td>
									<!--<td style="text-align:center"> <button id="<?= $date; ?>" class="btn btn-primary btn-xs btn-details">View Details</button> </td>-->
								</tr>	
								<?php } ?>
								
							</table>
								<center>
									<span id="pagination">
										<?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action='.$action) ?>
                                    </span>
                                </center>
							<?php } ?>	
						  </div>
						  <div id="menu1" class="tab-pane fade in ">
							<div>
								<table align="center">
									<tr>
										<td width="20%"><h2><i class="fa fa-lg fa-exclamation-triangle"></i></h2></td>
										<td width="80%"><h2>Sorry, This feature <br>is not yet available!</h2></td>
									</tr>
								</table>
						
							</div>
						  </div>
						</div>
						
					</div>
						
						<?php
						break;
						case "deposit":
						?>
					
					<div class="tab-pane fade in active">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#home">Tabular</a></li>
							<li ><a data-toggle="tab" href="#menu1">Graph</a></li>
						</ul>
						<div class="tab-content">
						  <div id="home" class="tab-pane fade in active">
							<?php 
								$data = ACCT::getAllDepositPerday(array("ad_DATE"=>"DESC"));
								#var_dump($data);
								
								
								 if(count($data)>=10) 
                                         {
                                             $pagination2 = new Pagination($data, 10, Request::get("p"));
                                         }else{
                                             $pagination2 = new Pagination($data, 10, NULL);
                                         }

                                         $data = $pagination2->get_array();

                                         if($data) {
							?>				 
							<table class="table table-hover table-responsive table-striped text-md">
								<tr>
									
									<th width="50%" style="text-align:center">Date</th>
									<th width="50%" style="text-align:center">Amount</th>
									<!--<th width="" style="text-align:center">Action</th>-->
								</tr>	
								
								<?php
                                            foreach($data as $key => $value){
								?>
								<tr>
									<td  class="hidden-xs" style="text-align:center">
										<?php echo $date = date("F j, Y",strtotime($value['ad_DATE'])); ?>
									</td>
									<td  class="visible-xs" style="text-align:center">
										<?php echo $date = date("m/d/Y",strtotime($value['ad_DATE'])); 
										$date = date("Y-m-d",strtotime($value['ad_DATE']));
										?>
									</td>
									<td style="text-align:center">&#x20B1; <?= money($value['total']); ?></td>
									<!--<td style="text-align:center"> <button id="<?= $date; ?>" class="btn btn-primary btn-xs btn-details">View Details</button> </td>-->
								</tr>	
								<?php } ?>
								
							</table>
								<center>
									<span id="pagination">
										<?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action='.$action) ?>
                                    </span>
                                </center>
							<?php }else{ ?>

								<table align="center">
									<tr>
										<td width="30%"><h2><i class="fa fa-lg fa-exclamation-triangle"></i></h2></td>
										<td width="70%"><h3>No Data Available!</h2></td>
									</tr>
								</table>

							<?php } ?>
						  </div>
						  <div id="menu1" class="tab-pane fade in ">
							<div>
								<table align="center">
									<tr>
										<td width="20%"><h2><i class="fa fa-lg fa-exclamation-triangle"></i></h2></td>
										<td width="80%"><h3>Sorry, This feature <br>is not yet available!</h3></td>
									</tr>
								</table>
						
							</div>
						  </div>
						</div>
						
					</div>
					
			<?php } ?>	
			</div>
			<div class="clearfix"></div>
			<?php include(LAYOUTS_N . "footer.php"); ?>
		</div>
	</body>
	
	
</html>