<?php
include("../old/init.php");
include(LAYOUTS_N . "header.php"); 
?>

<div class="container-fluid">
	<?php #include(LAYOUTS_N . "banner.php");  ?>
</div>
	<div class="container">
		<br><br><br>
			<div class="row col-md-8 col-md-offset-2">
				<center>
						<img name="logo" src="<?= IMG ?>f1.png" border="0" id="logo" alt="" width="100px" height="100px"
						style="box-shadow: 5px 5px  #888888;">
					<br><br><b><?=  strtoupper($user['account_LNAME']).", ".strtoupper($user['account_FNAME'])." ".strtoupper($user['account_MNAME']); ?></b><br>(<?= $user['access_NAME']; ?>)
				</center>
			</div>
			<div class="row col-md-8 col-md-offset-2">
					<h4 style='color:#2c3e50;'><center>
					<?php
					$action = clean($_GET['action']);
					?>
					<span  style='color:#2c3e50;' class="fa fa-file-text-o"></span>
						<b>Audit Report</b>
						
						</center>
					</h4>
					<hr>
			</div>
			
			
			
			
			<div class="row  col-xs-10 col-md-8 col-xs-offset-1 col-md-offset-2 ">
			
			<!--
			<table class="table table-responsive">
				<tr>
					<td width="50%" align="center">User:
					<td width="50%"><select class="form-control input-sm">
						<option>Please select</option>
					</select></td>
				</tr>
				<tr>
					<td width="50%" align="center">Date: </td>
					<td width="50%" valign="bottom"><input type="date" class="form-control input-sm" id="datepicker"></td>
					
				</tr>	
				<tr>
					<td></td>
					<td><button class="btn btn-lg btn-primary">Filter</button></td>
				</tr>	
				
			</table>
			-->
			
			
					<div class="tab-pane fade in active">
						<div class="tab-content">
						  <div id="home" class="tab-pane fade in active">
							<table class="table table-hover table-responsive table-striped text-md">
								<tr>
									
									<th width="25%" style="text-align:center">Date/Time</th>
									<th width="25%" style="text-align:center">Module</th>
									<th width="25%" style="text-align:center">User</th>
									<th width="25%" style="text-align:center">Action</th>
									<!--<th width="" style="text-align:center">Action</th>-->
								</tr>	
								
								
								<form method="GET">
								<tr>
									
									<th width="25%" style="text-align:center">
										<input type="date" class="form-control" value="<?php if(isset($_GET['date'])){ echo $_GET['date']; } ?>" name="date">
									</th>
									<th width="25%" style="text-align:center">
										<!--
										<select name="module" class="form-control">
											<?php $modules = AUDIT::getAllModule(); ?>
											<?php foreach($modules as $key => $value){ ?>
											<option value="<?= $value['audit_MODULE']; ?>"><?= $value['audit_MODULE']; ?></option>
											<?php } ?>
											
										</select>
										-->
									</th>
									<th width="25%" style="text-align:center">
										<?php $u = USERS::getAll(array("account_LNAME"=>"ASC")); ?>
										<select name="user" class="form-control">
											<?php foreach($u as $key => $val){ ?>
											<option value="<?= $val['account_ID'] ?>"><?= $val['account_LNAME']; ?>,<?= $val['account_FNAME']; ?></option>
											<?php } ?>
										</select>
										
									</th>
									<th width="25%" style="text-align:center">
										<!-- <input name="action2" type="text" class="form-control pull-left" style="width:70%">
										-->
										<button type="submit" class="btn btn-primary pull-right">Go</button>
									</th>
									<!--<th width="" style="text-align:center">Action</th>-->
								</tr>	
								</form>
								
								<?php 
								
								
								if(isset($_GET['date']) or isset($_GET['module']) or isset($_GET['user']) or isset($_GET['action2']) ){
									$date = clean($_GET['date']);
									$module = clean($_GET['module']);
									$selected_user = clean($_GET['user']);
									$action2 = clean($_GET['action2']);
									
								$data = AUDIT::getAuditby($date,$module,$selected_user,$action2,$order=array());
								}else{
								$data = AUDIT::getAllAudit(array("audit_DATE"=>"DESC"));
								}
								#var_dump($data);
								
								
								 if(count($data)>=20) 
                                         {
                                             $pagination2 = new Pagination($data, 20, Request::get("p"));
                                         }else{
                                             $pagination2 = new Pagination($data, 20, NULL);
                                         }

                                         $data = $pagination2->get_array();

                                         if($data) {

                                            foreach($data as $key => $value){
								?>
								<tr>
									<td  class="hidden-xs" style="text-align:center">
										<?php echo $date = date("F j, Y h:iA",strtotime($value['audit_DATE'])); ?>
									</td>
									<td  class="visible-xs" style="text-align:center">
										<?php echo $date = date("m/d/Y",strtotime($value['audit_DATE'])); 
										$date = date("Y-m-d",strtotime($value['audit_DATE']));
										?>
									</td>
									<td style="text-align:center"><?= $value['audit_MODULE']; ?></td>
									<td style="text-align:center"><?php $user = USERS::getID($value['account_ID']);
													echo $user['account_FNAME']." ".$user['account_LNAME'];
													?></td>
									<td style="text-align:center"><?= $value['audit_NOTE']; ?></td>
								</tr>	
								<?php } ?>
								<?php } ?>
							</table>
								<center>
									<span id="pagination">
										<?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action='.$action) ?>
                                    </span>
                                </center>
						  </div>
						</div>
					</div>	
					
					
				
			</div>
			<div class="clearfix"></div>
			<?php include(LAYOUTS_N . "footer.php"); ?>
		</div>
	</body>
	
	
</html>