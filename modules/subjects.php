<?php
include("../old/init.php");
include(LAYOUTS_N . "header.php"); 

if (!SESSION::isLoggedIn()) {
	HTML::redirect();
}

    //QUERIES
	if(Request::post()){
		switch(Request::get("action")) {
			case "add": 
            SUBJECTS::addSubject(Request::post());
			#var_dump(Request::post());
			
			break;
			case "edit": SUBJECTS::update_Subject($_GET['id'],Request::post()); break;
			#case "update_blog":  BLOG::update(Request::post("blog_info")); break;
			
			#case "edit": USERS::update(Request::get_or_fail("id"), Request::post()); break;
			
			default: Fail::not_found();
		}
	}
	
	if(Request::get()){
		switch(Request::get("action")) {
			case "delete": SUBJECTS::delete($_GET['id']); break;
		}
	}	
?>
<div class="container-fluid">
	<?php #include(LAYOUTS_N . "banner.php");  ?>
	
    </div>
	<div class="container">
			<br><br><br>
			<div class="row col-md-8 col-md-offset-2">
				<center>
						<img name="logo" src="<?= IMG ?>f1.png" border="0" id="logo" alt="" width="100px" height="100px"
						style="box-shadow: 5px 5px  #888888;">
					<br><br><b><?=  strtoupper($user	['account_LNAME']).", ".strtoupper($user['account_FNAME'])." ".strtoupper($user['account_MNAME']); ?></b><br>(<?= $user['access_NAME']; ?>)
				</center>
			</div>
	
		<div class="row col-md-8 col-md-offset-2">
				<h4 style='color:#2c3e50;'><center>
				<!--<span  style='color:#2c3e50;' class="fa fa-group">  
				</span>-->
					<b>Subject Management</b>
				</center></h4>
				<hr>
		</div>
		<div class="row col-md-8 col-md-offset-2">
		<?php SESSION::DisplayMsg(); ?>

			 <?php
			switch($_GET['action']){
                        case "add":?>
                      		<div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Add Subject </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                        	<table border='0' class="table table-responsive text-md" style="width:80%">
                        			<!--
                        			<tr>
                        				<td>
                        					&nbsp;
                        				</td>

                        				<td>
                        					<label for='subject_ID' class='col-md-3'>ID</label>
                        				</td>

                        				<td>
                        					<input name='id' class='form-control' type='text'/>
                            			</td>
                            		</tr>
                            		-->

                            		<tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='subject_CODE' class='col-md-3'>CODE</label>                        				</td>

                        				<td>
                        					<input name='subject_CODE' autofocus required class='form-control' type='text'/>                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='subject_DESCRIPTION' class='col-md-3'>DESCRIPTION</label>                        				</td>

                        				<td>
                        					<input name='subject_DESCRIPTION'  required class='form-control' type='text'/>                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='UNIT' class='col-md-5'>LEC UNIT</label>                        				</td>

                        				<td>
                        					<input name='LEC_UNIT' value="0.0"  required class='form-control' type='text'/>                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='UNIT' class='col-md-5'>LAB UNIT</label>                        				</td>

                        				<td>
                        					<input name='LAB_UNIT' value="0.0"  required class='form-control' type='text'/>                            			</td>
                            		</tr>
                            		<tr>
                            		  <td colspan='2'>                                    
                            		  
                       		          <td align='right'><button  class='btn btn-success btn-ms' type='submit'><i class='fa fa-edit'></i> Add Subject</button></td>
                       		  </tr>
                            </table>
                   	  </form>
                    </div>
                	</div>
                    <?php break; 
                        case "edit":
                        $subject_ID  =$_GET['id'];
                        $getsingle = SUBJECTS::getSingle(array("subject_ID"=>"DESC"),$subject_ID  );
                        
                    ?>
                    <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
                            <i class="fa fa-folder-o"></i> Update Subject </h3> </div>
                    <div class="panel-body">
                        <form action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype='multipart/form-data' method='POST'  class='form-horizontal well'>
                        	<table border='0' class="table table-responsive text-md" style="width:75%">
                        			<tr>
                        				<td>&nbsp;
                        					
                        				</td>

                        				<td>
                        					<label for='subject_ID' class='col-md-3'>ID</label>
                        				</td>

                        				<td>
                        					<input name='id' class='form-control' type='hidden' value="<?= $getsingle['subject_ID'];?>"/>
                        					<input name='subject_ID' class='form-control' type='text' value="<?= $getsingle['subject_ID'];?>"/>
                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;
                        					
                        				</td>

                        				<td>
                        					<label for='subject_CODE' class='col-md-3'>CODE</label>
                        				</td>

                        				<td>
                        					<input name='subject_CODE' class='form-control' type='text' value="<?= $getsingle['subject_CODE'];?>"/>
                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;
                        					
                        				</td>

                        				<td>
                        					<label for='subject_DESCRIPTION' class='col-md-3'>DESCRIPTION</label>
                        				</td>

                        				<td>
                        					<input name='subject_DESCRIPTION' class='form-control' type='text' value="<?= $getsingle['subject_DESCRIPTION'];?>"/>
                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='UNIT' class='col-md-7'>LEC UNIT</label>                        				</td>

                        				<td>
                        					<input name='LEC_UNIT' required class='form-control' type='text' value="<?= $getsingle['LEC_UNIT'];?>"/>                            			</td>
                            		</tr>

                            		<tr>
                        				<td>&nbsp;                        				</td>

                        				<td>
                        					<label for='UNIT' class='col-md-7'>LAB UNIT</label>                        				</td>

                        				<td>
                        					<input name='LAB_UNIT' value="<?= $getsingle['LAB_UNIT'];?>"  required class='form-control' type='text'/>                            			</td>
                            		</tr>

                            		<tr>
                            			<td colspan='3' align='right'>
                                    		<button  class='btn btn-success btn-ms' type='submit'><i class='fa fa-edit'></i> Update Subject</button>
                            			</td>
                            		</tr>
                            </table>
                    	</form>
                    </div>
                	</div>
                </div>
                </div>
            
                    <?php break; 
                        case "view":
					?>
						<?php
						if(!isset($_GET['id']))
						{
						?>
					
						<div class="panel panel-default">
							<div class="panel-heading">
								<table>
									<tr>
											<td width="50%">
												<h3 class="nav-pills" style="margin-top: 10px; margin-bottom: 10px"><i class="fa fa-folder-o"></i> Subject List </h3>
											</td>
											
											<td>
												<!--<input type="text" placeholder="Search" class="form-control">-->
												 <div class="input-group col-md-12">
                                                    <form id="form1" runat="server">
                                                         <input id="searchItem" class="searchItem form-control"  name="q" type="text" onkeyup="term(q.value)" autofocus="autofocus" onchange="term(q.value)"/>
                                                    </form>
                                                  <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                                </div>
                                                <script>
                                    			$(document).ready(function() {
												$('#searchItem').keyup(function(){
												var s_Item = $('#searchItem').val();
													
													if(s_Item.length >= 3)
													{
														 $.ajax({
															type: "POST",
															url: "search.php?action=subjects.php",
															data: 'search_term=' + s_Item,
															success: function(msg){
																/* $('#resultip').html(msg); */
																	$("#display_result").show();
																	$("#display_result").html(msg);
																	$("#display_hide").hide();
																
															}
											
														}); // Ajax Call
													
														//alert(s_Item);
														}else{
															$("#display_hide").show();
															$("#display_result").hide();   
														}
													});
												});			
                                    			</script>
											</td>

											
                                            <td width="5%">
                                                <div class="col-md-1">   
                                                	<button style="margin-top: 5px; margin-bottom: 5px" class='btn btn-success btn-ms' type='submit' onclick="javascript:window.open('subjects.php?action=add','_self');"><i class='fa fa-plus'></i></button>
                                                </div>
                                            </td>
                    
									</tr>
								</table>
							</div>
							
							

							<div id="display_result"></div>

							<div class="panel-body" id="display_hide">
								<table class="table table-hover table-responsive table-striped text-md">
									</tr>
									<th style="width:10%;">ID</th>
									<th style="width:15%;">CODE</th>
									<th style="">DESCRIPTION</th>
									<th align="center" style="width:10%;">LEC/LAB UNITS</th>
									<th style="width:10%;">OPTIONS</th>	
                                    <?php
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    $viewSubjects = SUBJECTS:: getAllSubjects(array("subject_ID"=>"ASC"));
                                     if(count($viewSubjects)>=10) 
                                     {
                                         $pagination2 = new Pagination($viewSubjects, 10, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($viewSubjects, 10, NULL);
                                     }

                                     $viewSubjects = $pagination2->get_array();

                                     if($viewSubjects) {

                                        foreach($viewSubjects as $key => $value){
                                    ?>
                                        <tr>
                                            <td>
                                                <?php echo $value['subject_ID']; ?>
                                            <td>
                                                <?php echo $value['subject_CODE']; ?>
                                            <td>
                                                <?php echo $value['subject_DESCRIPTION']; ?>
                                            <td align="center">
                                                <?php echo $value['LEC_UNIT']; ?> / <?php echo $value['LAB_UNIT']; ?>
                                            <td>
                                                <a href="subjects.php?action=edit&id=<?php echo $value['subject_ID']; ?>" class='btn btn-warning btn-xs'>
                                                    Edit
                                                </a>
                                               <!--
                                               <a href="subjects.php?action=delete&id=<?php echo $value['subject_ID']; ?>" onClick="return confirm('You\'re about to delete the subject.')"; class='btn btn-danger btn-xs'>
													Delete
												</a>
                                                -->
                                            </td>
                                        </tr>
                                        <?php
                                         } 
                                        }
                                    ?>
								</table> 
								<center>
                                <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view') ?>
                            </center>
								</div>
							</div>
						</div>
					</div>
                </div>
                  <?php } break; ?>
				  
				  
         <div id="myTabContent" class="tab-content">         
                  
              <!-- all --><div class="tab-pane fade " id="all">

                    <?php 
                        case "delete":?>
                        view
                <?php break;
                    }
		?>
		</div>
	
	
       
	
			
<?php include(LAYOUTS_N . "footer.php"); ?>
