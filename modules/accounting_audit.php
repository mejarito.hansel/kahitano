<?php
include("../old/init.php");
include(LAYOUTS_N . "header.php"); 
?>

<div class="container-fluid">
	<?php #include(LAYOUTS_N . "banner.php");  ?>
</div>
	<div class="container">
		<br><br><br>
			<div class="row col-md-8 col-md-offset-2">
				<center>
						<img name="logo" src="<?= IMG ?>f1.png" border="0" id="logo" alt="" width="100px" height="100px"
						style="box-shadow: 5px 5px  #888888;">
					<br><br><b><?=  strtoupper($user['account_LNAME']).", ".strtoupper($user['account_FNAME'])." ".strtoupper($user['account_MNAME']); ?></b><br>(<?= $user['access_NAME']; ?>)
				</center>
			</div>
			<div class="row col-md-8 col-md-offset-2">
					<h4 style='color:#2c3e50;'><center>
					<?php
					$action = clean($_GET['action']);
					
					?>
					<span  style='color:#2c3e50;' class="fa fa-search"></span>
							<b>Accounting Audit</b>
				</center>
					</h4>
					<hr>
			</div>
			<div class="row col-md-12 ">
		
			<form>
				<div>Semester: 
							<input type="hidden" name="action" value="<?= $action; ?>">
							<select name="sem_ID" class="form-control" onchange="this.form.submit()">
							<?php
								$selected_sem = clean($_GET['sem_ID']);
								$sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
								foreach($sem as $key => $val)
								{
								?>
									<option <?php if($val['sem_ID'] == $selected_sem){ echo "SELECTED=SELECTED"; } ?> value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
								
								<?php
								}
							?>
							</select>
				</div>
			</form>
			<?php
			switch($action){
				case "view":
			?>	
					<?php
					$assoc = COURSE::getallcourseAssociate(array("course_NAME"=>"ASC"));
					foreach($assoc as $keyA => $valA){
					?>
					<center><h4><?= $valA['course_NAME']; ?></h4></center>
					
							<table class="table table-hover table-responsive table-striped text-md">
								<tr>
									
									<th width="" style="text-align:center">#</th>
									<th width="" style="text-align:left">Name</th>
									<th width="" style="text-align:center">Student #</th>
									<th width="" style="text-align:center">Tuition Fee<br>(Payable)</th>
									<th width="" style="text-align:center">Tuition Fee<br>(Payment)</th>
									<th width="" style="text-align:center">Tuition Fee<br>(Balance)</th>
									<th width="" style="text-align:center">Other Fee<br>(Payable)</th>
									<th width="" style="text-align:center">Other Fee<br>(Payment)</th>
									<th width="" style="text-align:center">Other Fee<br>(Balance)</th>
									<th width="" style="text-align:center">Total<br>(Balance)</th>
									<!--<th width="" style="text-align:center">Action</th>-->
								</tr>	
								<?php 
								$data = SUBJENROL::getenrollmentpersempercourse(array("si_LNAME"=>"ASC"),$valA['course_ID'],$selected_sem);
							
				
                                         if($data) {
											 $x=1;
                                            foreach($data as $key => $value){
												
												$user = USERS::getSingle(array("si_LNAME"=>"ASC"),$value['si_ID']);
										?>
										<tr>
											<td  align="center"><?= $x++; ?></td>
											<td align="left"><?php echo $user['si_LNAME']; ?>, <?php echo $user['si_FNAME']; ?></td>
											<td align="center"><?php echo strtoupper($user['student_ID']); ?></td>
											
											<td align="center"><?php $tfPayable = (ACCT::getTFPayablesPerSem($user['si_ID'],$selected_sem));
																		echo money($tfPayable);
																?></td>
											<td align="center"><?php $tfPayment = (ACCT::getTFPaymentPerSem($user['si_ID'],$selected_sem)); 
																		echo money($tfPayment);
																?></td>
											<td align="center"><?php  $tf = ($tfPayable-$tfPayment);
																echo money($tf);
																?></td>
											<td align="center"><?php $ofPayable = (ACCT::getOFPayablesPerSem($user['si_ID'],$selected_sem));
																		echo money($ofPayable);
																?></td>
											<td align="center"><?php $ofPayment = (ACCT::getOFPaymentPerSem($user['si_ID'],$selected_sem));
																		echo money($ofPayment);
																?></td>
											
											<td align="center"><?php $of = $ofPayable-$ofPayment; 
																echo money($of);
																?></td>
											<td align="center"><?php $total = $tf+$of;
																echo money($total);
																?></td>
											
											
											
										</tr>	
										<?php } ?>
								<?php }else{ ?>
								<td colspan="10" align="center">No Available Data!</td>
								
								<?php } ?>
							</table>
							
							
				<?php } ?>	
				
					<?php
					$bachelor = COURSE::getallcourseBachelor(array("course_NAME"=>"ASC"));
					foreach($bachelor as $keyA => $valA){
					?>
					<center><h4><?= $valA['course_NAME']; ?></h4></center>
					
							<table class="table table-hover table-responsive table-striped text-md">
								<tr>
									
									<th width="" style="text-align:center">#</th>
									<th width="" style="text-align:left">Name</th>
									<th width="" style="text-align:center">Student #</th>
									<th width="" style="text-align:center">Tuition Fee<br>(Payable)</th>
									<th width="" style="text-align:center">Tuition Fee<br>(Payment)</th>
									<th width="" style="text-align:center">Tuition Fee<br>(Balance)</th>
									<th width="" style="text-align:center">Other Fee<br>(Payable)</th>
									<th width="" style="text-align:center">Other Fee<br>(Payment)</th>
									<th width="" style="text-align:center">Other Fee<br>(Balance)</th>
									<th width="" style="text-align:center">Total<br>(Balance)</th>
									<!--<th width="" style="text-align:center">Action</th>-->
								</tr>	
								<?php 
								$data = SUBJENROL::getenrollmentpersempercourse(array("si_LNAME"=>"ASC"),$valA['course_ID'],$selected_sem);
							
				
                                         if($data) {
											 $x=1;
                                            foreach($data as $key => $value){
												
												$user = USERS::getSingle(array("si_LNAME"=>"ASC"),$value['si_ID']);
										?>
										<tr>
											<td  align="center"><?= $x++; ?></td>
											<td align="left"><?php echo $user['si_LNAME']; ?>, <?php echo $user['si_FNAME']; ?></td>
											<td align="center"><?php echo strtoupper($user['student_ID']); ?></td>
											
											<td align="center"><?php $tfPayable = (ACCT::getTFPayablesPerSem($user['si_ID'],$selected_sem));
																		echo money($tfPayable);
																?></td>
											<td align="center"><?php $tfPayment = (ACCT::getTFPaymentPerSem($user['si_ID'],$selected_sem)); 
																		echo money($tfPayment);
																?></td>
											<td align="center"><?php  $tf = ($tfPayable-$tfPayment);
																echo money($tf);
																?></td>
											<td align="center"><?php $ofPayable = (ACCT::getOFPayablesPerSem($user['si_ID'],$selected_sem));
																		echo money($ofPayable);
																?></td>
											<td align="center"><?php $ofPayment = (ACCT::getOFPaymentPerSem($user['si_ID'],$selected_sem));
																		echo money($ofPayment);
																?></td>
											
											<td align="center"><?php $of = $ofPayable-$ofPayment; 
																echo money($of);
																?></td>
											<td align="center"><?php $total = $tf+$of;
																echo money($total);
																?></td>
											
											
											
										</tr>	
										<?php } ?>
								<?php }else{ ?>
								<td colspan="10" align="center">No Available Data!</td>
								
								<?php } ?>
							</table>
					<?php
					}
					?>
			<?php } ?>
			</div>
			<div class="clearfix"></div>
			<?php include(LAYOUTS_N . "footer.php"); ?>
		</div>
	</body>
	
	
</html>