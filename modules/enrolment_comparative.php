<?php
include("../old/init.php");
include(LAYOUTS_N . "header.php"); 
?>

<div class="container-fluid">
	<?php #include(LAYOUTS_N . "banner.php");  ?>
	
</div>
	<div class="container">
		<br><br><br>
			<div class="row col-md-8 col-md-offset-2">
				<center>
						<img name="logo" src="<?= IMG ?>f1.png" border="0" id="logo" alt="" width="100px" height="100px"
						style="box-shadow: 5px 5px  #888888;">
					<br><br><b><?=  strtoupper($user['account_LNAME']).", ".strtoupper($user['account_FNAME'])." ".strtoupper($user['account_MNAME']); ?></b><br>(<?= $user['access_NAME']; ?>)
				</center>
			</div>
	
		<div class="row col-md-8 col-md-offset-2">
				<h4 style='color:#2c3e50;'><center>
				<span  style='color:#2c3e50;' class="fa fa-exchange">  
				</span>
					<b>Enrolment Comparative Report</b>
				</center></h4>
				<hr>
		</div>
		
		
		
		<div class="row  col-xs-10 col-md-8 col-xs-offset-1 col-md-offset-2 ">
		
		<form>
			<div>From Semester: 
						<input type="hidden" name="action" value="view">
						<select name="sem_ID_from" class="form-control" >
			
						<?php
							$selected_sem_from = clean($_GET['sem_ID_from']);
							$sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
							
							foreach($sem as $key => $val)
							{
									$sel = NULL;	
									if($val['sem_ID'] == $selected_sem_from){
									$sel = "SELECTED=SELECTED"; 
									$selected_sem_from_name = $val['sem_NAME'];
									}
							?>
								<option <?= $sel; ?>  value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							<?php
							}
						?>
						</select>
						
			</div>
			<br>
			<div>To Semester: 
						<input type="hidden" name="action" value="view">
						<select name="sem_ID_to" class="form-control" >
			
						<?php
							$selected_sem_to = clean($_GET['sem_ID_to']);
							$sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
							foreach($sem as $key => $val)
							{
									$sel = NULL;	
									if($val['sem_ID'] == $selected_sem_to){
									$sel = "SELECTED=SELECTED"; 
									$selected_sem_to_name = $val['sem_NAME'];
									}
								
								
							?>
								<option <?= $sel; ?> value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
							<?php
							}
						?>
						</select>
						
			</div><br>
			<div>
				<button type="submit" class="button btn-primary pull-right">Compare</button>
			</div>
		</form>	
		<br>
		
			<ul class="nav nav-tabs">
			  <li class="active"><a data-toggle="tab" href="#menu1">Graph</a></li>
			  <li ><a data-toggle="tab" href="#home">Tabular</a></li>
			 
			</ul>
	
			<div class="tab-content">
			
			  <div id="home" class="tab-pane fade in">
				<h4>Associate Program:</h4>
				
				<table class="table table-hover table-responsive table-striped text-md">
					<tr>
						<th width="10%" class="hidden-xs" style="text-align:center">#</th>
						<th width="60%" class="hidden-xs">Course</th>
						<th width="15%" class="hidden-xs" style="text-align:center"><?= $selected_sem_from_name; ?></th>
						<th width="15%" class="hidden-xs" style="text-align:center"><?= $selected_sem_to_name ?></th>
						<th width="15%" class="hidden-xs" style="text-align:center">Figure Difference</th>
						<th width="15%" class="hidden-xs" style="text-align:center">Percentage Difference</th>
						
						<th width="10%" class="visible-xs" style="text-align:center">#</th>
						<th width="20%" class="visible-xs">Course</th>
						<th width="15%" class="visible-xs" style="text-align:center;">From</th>
						<th width="15%" class="visible-xs" style="text-align:center;">To</th>
						<th width="20%" class="visible-xs" style="text-align:center;">Fig.</th>
						<th width="20%" class="visible-xs" style="text-align:center;">Perc.</th>
					</tr>
					
					
					<?php
					$course = COURSE::getallcourseAssociate(array("course_NAME"=>"ASC"));
					$x = 1;
					$total = 0;
					$total1 = 0;
					$i = 0;
					$data = array();
					?>
					<?php foreach($course as $key => $val){ ?>
					<tr>
						<td align="center"><?= $x++; ?></td>
						<td class="hidden-xs"><?= $val['course_NAME']; ?></td>
						<td class="visible-xs"><?= $val['course_INIT']; ?></td>
						<td style="text-align:center">
							<?php
							$count = COURSE::enrolled_percourse($val['course_ID'],$selected_sem_from);
							echo $count;
							$total += $count;
							?>
						</td>
						<td style="text-align:center">
							<?php
							$count2 = COURSE::enrolled_percourse($val['course_ID'],$selected_sem_to);
							echo $count2;
							$total1 += $count2;
							?>
						</td>
						<?php
						$total_sem = COURSE::enrolled_persem($selected_sem);
						?>
						<td style="text-align:center">
							<?php
							if($count2 == 0 || $count == 0)
							{
								echo "No Data";
							}else{
									echo sprintf("%+d",$count2-$count)."";
							}
							 ?>
						</td>
						
						
						<td style="text-align:center">
						<?php 
							if($count2 == 0 || $count == 0)
							{
								echo "No Data";
							}else{
									echo sprintf("%+.2f",round(($count2/$count )*100-100,2))."%";
							}
							?></td>
						
					</tr>
					<?php 
					$data[$i]['COURSE'] =  $val['course_INIT'];
					$data[$i]['COUNT1'] =  $count;
					$data[$i]['COUNT2'] =  $count2;
					$i++;
					} ?>
					<tr>
						<td></td>
						<td align="right"><b>Total:</b></td>
						<td align="center"><?= $total; ?></td>
						<td align="center"><?= $total1; ?></td>
						<td align="center"><?= sprintf("%+d",$total1-$total); ?></td>
						<td align="center"><?= sprintf("%+.2f",round($total1/$total*100-100,2))."%"; ?></td>
					</tr>	
				</table>
				<h4>Bachelor Program:</h4>
				<table class="table table-hover table-responsive table-striped text-md">
					<tr>
						<th width="10%" class="hidden-xs" style="text-align:center">#</th>
						<th width="60%" class="hidden-xs">Course</th>
						<th width="15%" class="hidden-xs" style="text-align:center"><?= $selected_sem_from_name; ?></th>
						<th width="15%" class="hidden-xs" style="text-align:center"><?= $selected_sem_to_name ?></th>
						<th width="15%" class="hidden-xs" style="text-align:center">Figure Difference</th>
						<th width="15%" class="hidden-xs" style="text-align:center">Percentage Difference</th>
						
						<th width="10%" class="visible-xs" style="text-align:center">#</th>
						<th width="20%" class="visible-xs">Course</th>
						<th width="15%" class="visible-xs" style="text-align:center;">From</th>
						<th width="15%" class="visible-xs" style="text-align:center;">To</th>
						<th width="20%" class="visible-xs" style="text-align:center;">Fig.</th>
						<th width="20%" class="visible-xs" style="text-align:center;">Perc.</th>
					</tr>
					
					
					<?php
					$course2 = COURSE::getallcourseBachelor(array("course_NAME"=>"ASC"));
					$x = 1;
					$total = 0;
					$total2 = 0;
					$i = 0;
					$data2 = array();
					?>
					<?php foreach($course2 as $key => $val){ ?>
					<tr>
						<td align="center"><?= $x++; ?></td>
						<td class="hidden-xs"><?= $val['course_NAME']; ?></td>
						<td class="visible-xs"><?= $val['course_INIT']; ?></td>
						<td style="text-align:center">
							<?php
							$count = COURSE::enrolled_percourse($val['course_ID'],$selected_sem_from);
							echo $count;
							$total += $count;
							?>
						</td>
						<td style="text-align:center">
							<?php
							$count2 = COURSE::enrolled_percourse($val['course_ID'],$selected_sem_to);
							echo $count2;
							$total2 += $count2;
							?>
						</td>
						<?php
						$total_sem = COURSE::enrolled_persem($selected_sem);
						?>
						<td style="text-align:center">
							<?php
							if($count2 == 0 || $count == 0)
							{
								echo "No Data";
							}else{
									echo sprintf("%+d",$count2-$count)."";
							}
							 ?>
						</td>
						
						
						<td style="text-align:center">
						<?php 
							if($count2 == 0 || $count == 0)
							{
								echo "No Data";
							}else{
									echo sprintf("%+.2f",round(($count2/$count )*100-100,2))."%";
							}
							?></td>
						
					</tr>
					<?php 
					$data2[$i]['COURSE'] =  $val['course_INIT'];
					$data2[$i]['COUNT1'] =  $count;
					$data2[$i]['COUNT2'] =  $count2;
					$i++;
					} ?>
					<tr>
						<td></td>
						<td align="right"><b>Total:</b></td>
						<td align="center"><?= $total; ?></td>
						<td align="center"><?= $total2; ?></td>
						<td align="center">
						<?php if($total > 0 and $total2 > 0){
							echo sprintf("%+d", ($total2-$total) );
						}else{
							echo "No Data";
						}
						?>
							</td>
						<td align="center">
					
						<?php if($total > 0 and $total2 > 0){
						echo sprintf("%+.2f",round($total2/$total*100-100,2))."%"; 
						}else{
							echo "No Data";
						}
						?>
						</td>
					</tr>	
				</table>
			  </div>
			  <div id="menu1" class="tab-pane fade in active">
				<h4>Associate Program:</h4>
				<div id="placeholder" style="width:auto"></div>
				<h4>Bachelor Program:</h4>
				<?php if($total2 > 0 and $total > 0){ ?>
				<div id="placeholder2" style="width:auto"></div>
				<?php }else{  ?>
				No Data
				<?php } ?>
			  </div>
			
			
			</div>
		</div>
		
		
	
	
		
		
		<div class="clearfix"></div>
		
		<?php include(LAYOUTS_N . "footer.php"); ?>
		</div>
	</body>
	<script language="javascript" type="text/javascript" src="<?= HOME; ?>assets/js/raphael-min.js"></script>
	<script language="javascript" type="text/javascript" src="<?= HOME; ?>assets/js/morris.js"></script>
	<script>
     
	
			 graphStyle = Morris.Bar({
			  resize: true,
			  element: 'placeholder',
			  data: [
			  
			  <?php foreach($data as $key => $val){ ?>
				{x: '<?= $val['COURSE']; ?>', y: <?= $val['COUNT1']; ?>, z: <?= $val['COUNT2']; ?>},
			  <?php } ?>	
				
				
			  ],
			  xkey: 'x',
			  ykeys: ['y','z'],
			  labels: ['From', 'To'],
			  barColors: ['#13E2AF','#08D91D']
			})
			
			 graphStyle2 = Morris.Bar({
			  resize: true,
			  element: 'placeholder2',
			  data: [
			   <?php foreach($data2 as $key => $val){ ?>
				{x: '<?= $val['COURSE']; ?>', y: <?= $val['COUNT1']; ?>, z: <?= $val['COUNT2']; ?>},
			  <?php } ?>	
				
			  ],
			  xkey: 'x',
			  ykeys: ['y','z'],
			  labels: ['From', 'To'],
			  barColors: ['#13E2AF','#08D91D']
			})
			
			
			

    </script>
</html>