<?php
include("../old/init.php");
include(LAYOUTS_N . "header.php"); 
?>

<div class="container-fluid">
	<?php #include(LAYOUTS_N . "banner.php");  ?>
</div>
	<div class="container">
		<br><br><br>
			<div class="row col-md-8 col-md-offset-2">
				<center>
						<img name="logo" src="<?= IMG ?>f1.png" border="0" id="logo" alt="" width="100px" height="100px"
						style="box-shadow: 5px 5px  #888888;">
					<br><br><b><?=  strtoupper($user['account_LNAME']).", ".strtoupper($user['account_FNAME'])." ".strtoupper($user['account_MNAME']); ?></b><br>(<?= $user['access_NAME']; ?>)
				</center>
			</div>
			<div class="row col-md-8 col-md-offset-2">
					<h4 style='color:#2c3e50;'><center>
					<span  style='color:#2c3e50;' class="fa fa-tasks">  
					</span>
						<b>Enrolment Report</b>
					</center></h4>
					<hr>
			</div>
			<div class="row  col-xs-10 col-md-8 col-xs-offset-1 col-md-offset-2 ">
			<form>
				<div>Semester: 
							<input type="hidden" name="action" value="view">
							<select name="sem_ID" class="form-control" onchange="this.form.submit()">
							<?php
								$selected_sem = clean($_GET['sem_ID']);
								$sem = SEM::getallsems(array("sem_NAME"=>"DESC"));
								foreach($sem as $key => $val)
								{
								?>
									<option <?php if($val['sem_ID'] == $selected_sem){ echo "SELECTED=SELECTED"; } ?> value="<?= $val['sem_ID']; ?>"><?php echo $val['sem_NAME']; ?></option>
								
								<?php
								}
							?>
							</select>
				</div>
			</form>	
			<br>
				<ul class="nav nav-tabs">
				    <li class="active"><a data-toggle="tab" href="#menu1">Graph</a></li>
					<li ><a data-toggle="tab" href="#home">Tabular</a></li>
				
				</ul>
				<div class="tab-content">
				  <div id="home" class="tab-pane fade in">
				
				
					<h4>Associate Program:</h4>
					<table class="table table-hover table-responsive table-striped text-md">
						<tr>
							<th width="10%" class="hidden-xs" style="text-align:center">#</th>
							<th width="60%" class="hidden-xs">Course</th>
							<th width="15%" class="hidden-xs" style="text-align:center">Number </th>
							<th width="15%" class="hidden-xs" style="text-align:center">Percentage</th>
							
							<th width="20%" class="visible-xs" style="text-align:center">#</th>
							<th width="30%" class="visible-xs">Course</th>
							<th width="30%" class="visible-xs" style="text-align:center">No.</th>
							<th width="30%" class="visible-xs" style="text-align:center">%</th>
						</tr>
						<?php
						$course = COURSE::getallcourseAssociate(array("course_NAME"=>"ASC"));
						$x = 1; $total = 0; $i = 0; $data = array(); $total_per =0;
						?>
						<?php foreach($course as $key => $val){ ?>
						<tr>
							<td align="center"><?= $x++; ?></td>
							<td class="hidden-xs"><?= $val['course_NAME']; ?></td>
							<td class="visible-xs"><?= $val['course_INIT']; ?></td>
							<td style="text-align:center">
								<?php
								$count = COURSE::enrolled_percourse($val['course_ID'],$selected_sem);
								echo $count;
								$total += $count;
								?>
							</td>
							<?php
							$total_sem = COURSE::enrolled_persemAssociate($selected_sem);
							?>
							<td style="text-align:center">
							
							<?= $percent = round(($count/$total_sem)*100,2); ?>%</td>
							<?php $total_per+= $percent; ?>
						</tr>
						<?php 
						$data[$i]['COURSE'] =  $val['course_INIT'];
						$data[$i]['COUNT'] =  $count;
						$data[$i]['PERCENTAGE'] =  $percent;
						$i++;
						} ?>
						<tr>
							<td></td>
							<td align="right"><b>Total:</b></td>
							<td align="center"><b><?= $total; ?></b></td>
							<td align="center"><b><?= $total_per; ?>%</b></td>
						</tr>	
					</table>
					
					<h4>Bachelor Program:</h4>
					<table class="table table-hover table-responsive table-striped text-md">
						<tr>
							<th width="10%" class="hidden-xs" style="text-align:center">#</th>
							<th width="60%" class="hidden-xs">Course</th>
							<th width="15%" class="hidden-xs" style="text-align:center">Number </th>
							<th width="15%" class="hidden-xs" style="text-align:center">Percentage</th>
							
							<th width="20%" class="visible-xs" style="text-align:center">#</th>
							<th width="30%" class="visible-xs">Course</th>
							<th width="30%" class="visible-xs" style="text-align:center">No.</th>
							<th width="30%" class="visible-xs" style="text-align:center">%</th>
						</tr>
						<?php
						$course = COURSE::getallcourseBachelor(array("course_NAME"=>"ASC"));
						$x = 1; $total2 = 0; $i = 0; $data2 = array(); $total_per2=0;
						?>
						<?php foreach($course as $key => $val){ ?>
						<tr>
							<td align="center"><?= $x++; ?></td>
							<td class="hidden-xs"><?= $val['course_NAME']; ?></td>
							<td class="visible-xs"><?= $val['course_INIT']; ?></td>
							<td style="text-align:center">
								<?php
								$count = COURSE::enrolled_percourse($val['course_ID'],$selected_sem);
								echo $count;
								$total2 += $count;
								?>
							</td>
							<?php
							$total_sem = COURSE::enrolled_persemBachelor($selected_sem);
							?>
							<td style="text-align:center">
							<?= $percent = round(($count/$total_sem)*100,2); ?>%</td>
							<?php $total_per2 += $percent; ?>
						</tr>
						<?php 
						$data2[$i]['COURSE'] =  $val['course_INIT'];
						$data2[$i]['COUNT'] =  $count;
						$data2[$i]['PERCENTAGE'] =  $percent;
						$i++;
						} ?>
						<tr>
							<td></td>
							<td align="right"><b>Total:</b></td>
							<td align="center"><b><?= $total2; ?></b></td>
							<td align="center"><b><?= $total_per2; ?>%</b></td>
							
						</tr>	
					</table>
				  </div>
				  <div id="menu1" class="tab-pane fade in active">
					<h4>Associate Program:</h4>
					<div id="placeholder" style="width:100%;height:300px;"></div>
					<h4>Bachelor Program:</h4>
					<div id="placeholder2" style="width:100%;height:300px;"><?php
					if($total2 == 0){
							echo "No data Available";
					}
					?></div>
					<div id="graphlegend" style="width:100%;"></div>
				  </div>
				<?php
				$sort = array();
				foreach($data as $k=>$v) {
					$sort['COURSE'][$k] = $v['COURSE'];
					$sort['COUNT'][$k] = $v['COUNT'];
					$sort['PERCENTAGE'][$k] = $v['PERCENTAGE'];
				}
				# sort by event_type desc and then title asc
				array_multisort($sort['COUNT'], SORT_ASC, $sort['COURSE'], SORT_ASC,$data);
				#var_dump($data);
				
				$sort = array();
				foreach($data2 as $k=>$v) {
					$sort['COURSE'][$k] = $v['COURSE'];
					$sort['COUNT'][$k] = $v['COUNT'];
					$sort['PERCENTAGE'][$k] = $v['PERCENTAGE'];
				}
				# sort by event_type desc and then title asc
				array_multisort($sort['COUNT'], SORT_ASC, $sort['COURSE'], SORT_ASC,$data2);
				#var_dump($data);
			?>
				</div>
			</div>
			<div class="clearfix"></div>
			<?php include(LAYOUTS_N . "footer.php"); ?>
		</div>
	</body>
	<script language="javascript" type="text/javascript" src="<?= HOME; ?>assets/js/jquery.flot.js"></script>
	<script language="javascript" type="text/javascript" src="<?= HOME; ?>assets/js/jquery.flot.pie.js"></script>
	<script language="javascript" type="text/javascript" src="<?= HOME; ?>assets/js/jquery.flot.resize.min.js"></script>
	<script>
      $(document).ready(function(){
        /*
         * DONUT CHART
         * -----------
         */
        var donutData = [
		
		<?php foreach($data as $key => $val){

			 $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
			$color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
		?>
          {label: "<?= $val['COURSE']; ?>", data: <?= $val['PERCENTAGE']; ?>, color: "<?= $color; ?>"},
		<?php } ?>
        
		];
			 var donutData2 = [
		
		<?php foreach($data2 as $key => $val){

			 $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
			$color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
		?>
          {label: "<?= $val['COURSE']; ?>", data: <?= $val['PERCENTAGE']; ?>, color: "<?= $color; ?>"},
		<?php } ?>
        
		];
			
			$.plot('#placeholder', donutData, {
				series: {
					pie: {
						show: true,
						label: {
							radius: 1,
						formatter: function(label, series) {
									return '<div style="font-size:10px; text-align:center; padding:1px; color:white;">'+label+'<br>'+Math.round(series.percent)+'%</div>';
								},
								background: {
									opacity: .8,
									color: '#444'
								}
						}
					}
				},
				legend: {
					show: false
				}
			});
			
			$.plot('#placeholder2', donutData2, {
				series: {
					pie: {
						show: true,
						label: {
							radius: 1,
						formatter: function(label, series) {
									return '<div style="font-size:10px; text-align:center; padding:1px; color:white;">'+label+'<br>'+Math.round(series.percent)+'%</div>';
								},
								background: {
									opacity: .8,
									color: '#444'
								}
						}
					}
				},
				legend: {
					show: false
				}
			});
        /*
         * END DONUT CHART
         */
      });
	function labelFormatter(label, series) {
        return '<div style="font-size:12px; text-align:center; padding:2px; color: #fff; font-weight: 600;">'
                + label
                + "<br>"
                + Math.round(series.percent) + "%</div>";
				}
	function labelFormatter2(label, series) {
        return ''
                + label
                + ""
                + Math.round(series.percent) + "%";
				}
    </script>
</html>