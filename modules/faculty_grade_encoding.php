<?php
include("../old/init.php");
include(LAYOUTS_N . "header.php"); 

if (!SESSION::isLoggedIn()) {
	HTML::redirect();
}
?>
<div class="container-fluid">
	<?php #include(LAYOUTS_N . "banner.php");  ?>
	
    </div>
	<div class="container">
			<br><br><br>
			<div class="row col-md-8 col-md-offset-2">
				<center>
						<img name="logo" src="<?= IMG ?>f1.png" border="0" id="logo" alt="" width="100px" height="100px"
						style="box-shadow: 5px 5px  #888888;">
					<br><br><b><?=  strtoupper($user['account_LNAME']).", ".strtoupper($user['account_FNAME'])." ".strtoupper($user['account_MNAME']); ?></b><br>(<?= $user['access_NAME']; ?>)
				</center>
			</div>
	
		<div class="row col-md-8 col-md-offset-2">
				<h4 style='color:#2c3e50;'><center>
				<!--<span  style='color:#2c3e50;' class="fa fa-group">  
				</span>-->
					<b>My Subjects</b>
				</center></h4>
				<hr>
		</div>
		
		<div class="row col-md-8 col-md-offset-2">
		<?php
           switch($_GET['action']){
               case "view":
			   $sem_ID = clean($_GET['sem_ID']);
			   $sem = SEM::getSingleSem($sem_ID);
			   $isActiveEncoding = SEM::isSemEncoding($sem_ID);
			   #print_r($isActiveEncoding);
				if (!$isActiveEncoding == 1) {
					HTML::redirect();
				}
			   ?>
			   <div class="col-md-12">
								<center><h4>Semester: <?php $sem = SEM::getSingleSem($sem_ID); echo $sem['sem_NAME']; ?></h4></center>
								<table class="table table-hover table-responsive table-striped text-md">
									
                                    <th style="width:5%;">#</th>
                                    <th style="width:15%;">Section</th>
                                    <th style="width:40%;">Code / Description</th>
									<th style="width:10%%;">Status</th>
                                    <th style=""  >Operation</th>
										
                                    <?php
									
									$getmysubj = SUBJSCHED::getByfaculty(clean($_GET['sem_ID']), $instructor_ID);
									#print_r($getmysubj);
									$totalcount = count($getmysubj);
                                     //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                   // $viewSems = SEMSECTION::getAll(array("semester_section_ID" => "DESC"));
									
									
                                     if(count($getmysubj)>=50) 
                                     {
                                         $pagination2 = new Pagination($getmysubj, 50, Request::get("p"));
                                     }else{
                                         $pagination2 = new Pagination($getmysubj, 50, NULL);
                                     }

                                     $getmysubj = $pagination2->get_array();
									 $x = 1;

                                     if($getmysubj) {
                                        foreach($getmysubj as $key => $value){
                                    ?>
                                        <tr>
                                            <td>
											<?php 
												if(isset($_GET['p']))
												{
												$p = $_GET['p'];
												
												$num = 	$x+($p*10)-10;
												echo $num;
												$x++;
												
												}else{
													echo $x++;
												}
											 ?>
											 <td><?php $c=COURSE::getbyID($value['course_ID']); 
												echo $c['course_INIT']; ?>
                                            <?php 
                                              echo $value['semester_section_NAME']; ?>
                                            
                                          <td > <?php $s = SUBJECTS::getID($value['subject_ID']);
												echo $s['subject_CODE']." / ".$s['subject_DESCRIPTION'];
												 ?> 
												 </td>
                                            <td ><?php switch($value['is_lock'])
											{
												case 0: echo "Unlocked"; break;
												case 1: echo "Locked"; break;	
											} ?> / <?php switch($value['is_check'])
											{
												case 0: echo "Unchecked"; break;
											 	case 1: echo "Checked"; break;
											}
											?>
                                            <td align="center">
											
													<a href="?action=encoding&subject_sched_ID=<?= $value['subject_sched_ID']; ?>&sem_ID=<?= $sem_ID ?>" class='btn btn-warning btn-xs'>Encode Grades</a>
													
													<!-- <a onclick="return popitup('gs.php?id=<?php echo $value['subject_sched_ID']; ?>')" href="#" class='btn btn-success btn-xs'>GS</a> -->
                                            </td>
                                  </tr>
                                        <?php
                                         } 
                                        }
										
										#print_r($sem);
                                    ?>
								</table> 
                                Total Records: <?=  $totalcount; ?>
                                       
									<!--
									   <center>
                                            <?= $pagination2->create_link($_SERVER['PHP_SELF'].'?action=view&sem_ID='.$sem[0]['sem_ID'].'&fac_id='.$instructor_ID.'') ?>
                                        </center>
									-->
		 
		 
					
					</div>
			   
			   <?php
			   break;
				case 'parse_grade':
				
					$subject_sched_ID = clean($_GET['subject_sched_ID']);
					$date = date("Y_m_d_h_i_s");
					
					$target_dir = "../files/grades/";
					$filename = $user['account_ID']. "_".$date."_". basename($_FILES["fileToUpload"]["name"]);
					$target_file = $target_dir . $filename ;
					
					$uploadOk = 1;
					
					// Check if file already exists
					if (file_exists($target_file)) {
						echo "Sorry, file already exists.";
						$uploadOk = 0;
					}
					// Check file size
					if ($_FILES["fileToUpload"]["size"] > 500000) {
						#echo "Sorry, your file is too large.";
						#$uploadOk = 0;
					}
					// Check if $uploadOk is set to 0 by an error
					if ($uploadOk == 0) {
						echo "Sorry, your file was not uploaded.";
					// if everything is ok, try to upload file
					} else {
						if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
							
							SUBJSCHED::updateFileSubjectSched($filename,$subject_sched_ID);
							
							
							#echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.<br>";
							
							require_once 'PHPExcel/Classes/PHPExcel.php';
												
							
							$objReader = PHPExcel_IOFactory::createReaderForFile($target_file);
							$sheetname = array('STUDINFO','GRADE'); 
							$objReader->setLoadSheetsOnly($sheetname); 
							$objPHPExcel = $objReader->load($target_file);
							
							if($objPHPExcel)
							{
								
								$objPHPExcel->setActiveSheetIndexByName('STUDINFO');
								
								//GET MAX # OF STUDENTS
								$E62  = $objPHPExcel->getActiveSheet()->getCell('E62')->getCalculatedValue();
								
								$student_info = array();
								$D = 10;  //Starting Row
									for($i=0 ;$i<$E62; $i++)
									{
										$student_ID = $objPHPExcel->getActiveSheet()->getCell('D'.$D)->getCalculatedValue();
										if($student_ID == NULL)
										{
											$student_info[$i]['student_ID'] = 'NO-STUDENT-ID';
										}else{
											$student_info[$i]['student_ID'] = $student_ID;
										}
										
										$student_info[$i]['number'] = $i;
										$student_info[$i]['student_NAME'] = $objPHPExcel->getActiveSheet()->getCell('C'.$D)->getCalculatedValue();
										$D++;
									}
								#var_dump($student_info);
								
								$objPHPExcel->setActiveSheetIndexByName('GRADE');
								$O14  = $objPHPExcel->getActiveSheet()->getCell('O14')->getValue();
								
								
								if($O14 == 'EQUIV')
								{
								//ASSOCIATE	
								
								$D = 15; //Starting Row
									for($i=0 ;$i<$E62; $i++)
									{
										$student_info[$i]['EQUIV'] = $objPHPExcel->getActiveSheet()->getCell('O'.$D)->getOldCalculatedValue();
										$student_info[$i]['REMARKS'] = $objPHPExcel->getActiveSheet()->getCell('P'.$D)->getOldCalculatedValue();
										$D++;
									}
									
									#var_dump($student_info);
									
									
									$stud =   SUBJSCHED::getBySubjectSchedID(clean($_GET['subject_sched_ID']),$instructor_ID);
									
									
									$subject_sched_ID = clean($_GET['subject_sched_ID']);
						$ss = SUBJSCHED::getSched($subject_sched_ID);
						$sem_ID = clean($_GET['sem_ID']);
						?>
						<center>
						<h4><a href="faculty_grade_encoding.php?action=view&sem_ID=<?=$sem_ID;?>">Semester: <?php $sem = SEM::getSingleSem($sem_ID); echo $sem['sem_NAME']; ?></a></h4>
									<b><?php $s = SUBJECTS::getID($ss['subject_ID']);
									echo $s['subject_CODE']."-".$s['subject_DESCRIPTION'];
									 ?></b> <br />
                                      <?php $c=COURSE::getbyID($ss['course_ID']); 
									echo $c['course_INIT']."-".$ss['semester_section_NAME'];
									?>
                                    
								<br />
								<br>
                            </center>
									
									
									<div id="error1" style="display:none"><?php session::DisplayCustomMsg('warning','Some record(s) are not encoded due to inconsistency on student information sheet. Please encode them manually.'); ?></div>
									<div id="success" style="display:none"><?php session::DisplayCustomMsg('success','Success uploading grading sheet. Please review below:'); ?></div>
									
									<h3 class="text-success"><i class="fa  fa-list-alt"></i> Summary:</h3>
									<table class="table table-hover table-responsive table-striped text-md">
										<tr>
											<th width="5%">#</td>
											<th width="20%">Student #</td>
											<th width="30%">Student Name</td>
											<th width="15%">Equivalent</td>
											<th width="15%">Remarks</td>
											<th width="15%">Status</td>
										</tr>	
											
										<?php
										$found_student = array();
										
										
										$x=1;
										foreach($stud as $key => $val){
											$status = 0;
										?>
										<tr>
											<td><?=  $x++; ?></td>
											<td><?= $val['student_ID']; ?>
											<td><?= $val['si_LNAME']; ?>, <?= $val['si_FNAME']; ?></td>
											<?php 
													foreach($student_info as $key2 => $val2)
													{
														if($val['student_ID'] == $val2['student_ID'])
														{
															$equiv =  $val2['EQUIV'];
															$remarks = $val2['REMARKS'];
															$status =  "UPDATED";
															
															unset($student_info[$key2]);
															$found_student[] = $val2;
														}
													}
													
													switch($remarks)
													{
														case 'NFE':
														case 'INC':	SUBJSCHED::uploadGrade($val['subject_enrolled_ID'],'INC'); break;
														case 'DROP': SUBJSCHED::uploadGrade($val['subject_enrolled_ID'],'DRP'); break;
														case 'PASSED': 
														case 'FAILED': SUBJSCHED::uploadGrade($val['subject_enrolled_ID'],$equiv);	break;
														default: $status = 'NO GRADE';
													}
												?>	
											<td><?= $equiv; ?></td>
											<td><?= $remarks; ?></td>
											<td><?= $status; ?> </td>
												
										</tr>	
										<?php
										}
										?>
									</table>
									
									<div id="notfound">
									<?php if(sizeof($student_info) > 0){ ?>
									<h3 class="text-danger"><i class="fa  fa-times"></i> Not Found In Student List:</h3>
									<div>Please encode this manually:</div>
									<table class="table table-hover table-responsive table-striped text-md">
										<tr>
											<th width="5%">#</td>
											<th width="20%">Student #</td>
											<th width="30%">Student Name</td>
											<th width="15%">Equivalent</td>
											<th width="15%">Remarks</td>
											<th width="15%">Status</td>
										</tr>	
										<?php 
										$x=1;
										foreach($student_info as $key => $val){ ?>
										<tr>
											<td><?= $x++; ?></td>
											<td><?= $val['student_ID']; ?></td>
											<td><?= $val['student_NAME']; ?></td>
											<td><?= $val['EQUIV']; ?></td>
											<td><?= $val['REMARKS']; ?></td>
											<td>NOT FOUND</td>
										</tr>
										<?php } ?>
									</table>
									<?php } ?>
									</div>
									
									
									<script>
										$(document).ready(function() {
											<?php if(sizeof($student_info) > 0){ ?>
											$("#error1").show();
											$("#error1").append($("#notfound").html());
											$("#notfound").hide();
											<?php }else{ ?>
											$("#success").show();
											<?php } ?>
										});

									</script>
									
									<?php
									#var_dump($student_info);
									#var_dump($found_student);
									
									
								
								}else{
								//BACHELOR	
									
								}
								
								?>
								<center>
									<a href="faculty_grade_encoding.php?action=encoding&subject_sched_ID=<?= $subject_sched_ID; ?>&sem_ID=<?= $sem_ID; ?>" class="btn btn-info" role="button">Go Back</a>
								</center>	
								
								<?php
								
								
							}
							
							
							
							
						} else {
							echo "Sorry, there was an error uploading your file. Please try again <a href=$_SERVER[HTTP_REFERER]>Click here</a>.";
						}
					}
					?>
				
				
				<?php
			   break;
				case 'encoding':
						$subject_sched_ID = clean($_GET['subject_sched_ID']);
						$ss = SUBJSCHED::getSched($subject_sched_ID);
						$sem_ID = clean($_GET['sem_ID']);
						#<strong></strong>print_r($ss);
						?>
                        
						<div class="panel panel-default">
							<div class="panel-heading">
                                <table>
                                <tr>
									<td width="50%">
										<h3 class="nav-pills" style="margin-top: 5px; margin-bottom: 5px">
											<i class="fa fa-folder-o"></i> Grade Encoding  </h3>
									</td>
									<td width="5%">
									</td>
                                </tr>
                                </table>
                            </div>
							<div class="panel-body" >
                            <center>
							<h4><a href="faculty_grade_encoding.php?action=view&sem_ID=<?=$sem_ID;?>">Semester: <?php $sem = SEM::getSingleSem($sem_ID); echo $sem['sem_NAME']; ?></a></h4>
									<b><?php $s = SUBJECTS::getID($ss['subject_ID']);
									echo $s['subject_CODE']."-".$s['subject_DESCRIPTION'];
									 ?></b> <br />
                                      <?php $c=COURSE::getbyID($ss['course_ID']); 
									echo $c['course_INIT']."-".$ss['semester_section_NAME'];
									?>
                                    <br />
									<?php echo $ss['day']; ?> 
                                    <br />
                                    <?php echo $ss['start']; ?> - <?php echo $ss['end']; ?> 
								<br />
								Lock Status:	
                                 <?php
                                 switch($ss['is_lock'])
								{
								  case 1 : echo "LOCKED"; break;
								  case 0 : echo "UNLOCKED";
								}
								?> 
                                -
        						Check Status: 
								<?php
								switch($ss['is_check'])
								{
								  case 1 : echo "CHECKED"; break;
								  case 0 : echo "UNCHECKED";
								}
								?> 
                            </center>
                            <br />
                            <br />
							
							
							<?php if($ss['is_lock'] == 0){ ?>
							<form action="?action=parse_grade&subject_sched_ID=<?= $subject_sched_ID; ?>&sem_ID=<?= $sem_ID; ?>" method="post" enctype="multipart/form-data" style="display:none">
							<div class="col-md-10 col-md-offset-1 col-xs-offset-0">
								<h4><i class="fa fa-paperclip fa-big"></i> Upload Grading Sheet</h4>
								<table class="table table-hover table-responsive text-md">
									<tr>
										<td> Attach File:</td>
										<td><input type="file" name="fileToUpload" required="required"></td>
										<td align="right"><button type="submit" class="btn btn-default">Upload</button></td>
									</tr>
									<tr>
										<td colspan="3"><b>Tip</b>: Final Grade will be automatically updated from the uploaded grading sheet. <i class="fa fa-smile-o"></i></td>
								</table>
							</div>
                            </form>    
							<?php } ?>
								
								<div id="display_hide">
								<form action="" method="get">
                                
                                <table class="table table-hover table-responsive table-striped text-md">
									<tr>
									<th style="width:5%;">#</th>
									<th style="width:30%;">Student Name</th>
									<th style="width:25%;text-align:center" >Grade</th>
                                    <th style="widows:20%; text-align:center">Remarks</th>
                                    <th style="width:20%;text-align:center">Status</th>
                                    <?php
                                    //$viewStudents = USERS::viewStudents(array("si_ID"=>"ASC"));
                                    #$viewStudents = ROOM:: getallrooms(array("room_ID"=>"ASC"));
	                                 $stud =   SUBJSCHED::getBySubjectSchedID(clean($_GET['subject_sched_ID']),$instructor_ID);
									 #var_dump($stud);
									$x=1;
                                    ?>
                                       
                                     <?php foreach($stud as $key => $val) { ?>  
                                        <tr>
                                            <td><?= $x++; ?>
                                            <td><?php $u = USERS::getSingle(array("si_ID"=>"ASC"),$val['si_ID']);
													echo strtoupper($u['si_LNAME'].", ".$u['si_FNAME']);
												?>
                                            <td>
                                            <select <?php if($ss['is_lock'] == 1){ echo "DISABLED=DISABLED"; } ?>    name="grade<?php echo $val['subject_enrolled_ID']; ?>" class="form-control">
                                            	<option value="NG">NO GRADE</option>
                                            	<option value="1.00" <?php if($val['grade'] == "1.00"){ echo " SELECTED=SELECTED "; } ?>>1.00</option>
                                                <option value="1.25" <?php if($val['grade'] == "1.25"){ echo " SELECTED=SELECTED "; } ?>>1.25</option>
                                                <option value="1.50" <?php if($val['grade'] == "1.50"){ echo " SELECTED=SELECTED "; } ?>>1.50</option>
                                                <option value="1.75" <?php if($val['grade'] == "1.75"){ echo " SELECTED=SELECTED "; } ?>>1.75</option>
                                                <option value="2.00" <?php if($val['grade'] == "2.00"){ echo " SELECTED=SELECTED "; } ?>>2.00</option>
                                                <option value="2.25" <?php if($val['grade'] == "2.25"){ echo " SELECTED=SELECTED "; } ?>>2.25</option>
                                                <option value="2.50" <?php if($val['grade'] == "2.50"){ echo " SELECTED=SELECTED "; } ?>>2.50</option>
                                                <option value="2.75" <?php if($val['grade'] == "2.75"){ echo " SELECTED=SELECTED "; } ?>>2.75</option>
                                                <option value="3.00" <?php if($val['grade'] == "3.00"){ echo " SELECTED=SELECTED "; } ?>>3.00</option>
                                                <option value="5.00" <?php if($val['grade'] == "5.00"){ echo " SELECTED=SELECTED "; } ?>>5.00</option>
                                                <option value="UW" <?php if($val['grade'] == "UW"){ echo " SELECTED=SELECTED "; } ?>>UNAUTHORIZED WITHDRAWAL</option>
                                                <option value="AW" <?php if($val['grade'] == "AW"){ echo " SELECTED=SELECTED "; } ?>>AUTHORIZED WITHDRAWAL</option>
                                                <option value="INC" <?php if($val['grade'] == "INC"){ echo " SELECTED=SELECTED "; } ?>>INCOMPLETE</option>
                                                <!-- <option value="DRP" <?php if($val['grade'] == "DRP"){ echo " SELECTED=SELECTED "; } ?>>DROPPED</option> -->
                                                
                                            </select>   
                                            <input type="hidden" name="subject_enrolled_ID<?php echo $val['subject_enrolled_ID']; ?>" value="<?php echo $val['subject_enrolled_ID']; ?>" />                                         
                                           
                                           <td align="center" id="remarks<?php echo $val['subject_enrolled_ID']; ?>">
                                           
                                            <?php 
											 	switch($val['grade'])
												{
													case "1.00":
													case "1.25":
													case "1.50":
													case "1.75":
													case "2.00":
													case "2.25":
													case "2.50":
													case "2.75":
													case "3.00": echo "PASSED";
													break;
													case "5.00": echo "FAILED";
													break;
													case "UW": echo "UNAUTHORIZED WITHDRAWAL";
							                        break;
							                        case "AW": echo "AUTHORIZED WITHDRAWAL";
							                        break;
													case "INC": echo "INCOMPLETE";
													break;
													case "DRP": echo "DROPPED";
													break;
													default: echo "NO GRADE";
												}
											 ?>
                                           
                                           
                                           </td>
                                           
                                            <td><div id="result<?php echo $val['subject_enrolled_ID']; ?>" align="center">
                                            <?php 
											 	switch($val['grade'])
												{
													case "1.00":
													case "1.25":
													case "1.50":
													case "1.75":
													case "2.00":
													case "2.25":
													case "2.50":
													case "2.75":
													case "3.00": 
													case "5.00": 
													case "INC": 
													case "AW": 
													case "UW": 
													case "DRP":
													case "NG":
														echo "Grade Encoded";		
														break;											
													default: echo "<span style='color:red'> No Grade Encoded</span>";
												}
											 ?>
                                            </div></td>
                                        </tr>
                                        
                                        <?php
                                      }
                                       
                                    ?>
								</table>
                                
								<center>
									<a href="faculty_grade_encoding.php?action=view&sem_ID=<?= $sem_ID; ?>" class="btn btn-info" role="button">Go Back</a>
								</center>
								
                                </form> 
                                <?php if($ss['is_lock'] == 0){?>
									<script>
									$(document).ready(function(){
										<?php foreach($stud as $key => $val){ ?>
											  $('[name="grade<?php echo $val['subject_enrolled_ID']; ?>"]').each(function() {
													$(this).change(function() {
														var mydata = $('[name="grade<?php echo $val['subject_enrolled_ID']; ?>"]').val();//$(this).val();
														var inputdata = $('[name="subject_enrolled_ID<?php echo $val['subject_enrolled_ID']; ?>"]').val();
												//	alert(mydata)
												//	alert(inputdata)
														$.ajax({   
														   type: 'GET',   
														   url: '<?= HOME ?>/old/update_ajax.php',   
														   data: {action: 'grade_encoding.php',subject_enrolled_ID:inputdata, grade:mydata},
															success: function(data)
															{
															  $('#result<?php echo $val['subject_enrolled_ID']; ?>').html(data);
															  var remarks = "";
															  switch(mydata)
																{
																	case "1.00":
																	case "1.25":
																	case "1.50":
																	case "1.75":
																	case "2.00":
																	case "2.25":
																	case "2.50":
																	case "2.75":
																	case "3.00":  remarks = "PASSED";
																	break;
																	case "5.00": remarks = "FAILED";
																	break;
																	case "INC": remarks = "INCOMPLETE";
																	break;
																	case "DRP": remarks = "DROPPED";
																	break;
																	default: remarks = "NO GRADE";
																}
															 
															  $('#remarks<?php echo $val['subject_enrolled_ID']; ?>').html(remarks);
															}													
														});
													});
												});
										<?php } ?>		
									});
									</script>
								<?php } ?>	
						      </div>
                            </div>
						</div>
                            
					
                  <?php 
                    
				
				break;
				
				default: HTML::redirect();
		   }
        ?>
		</div>
	</div>	
			
<?php include(LAYOUTS_N . "footer.php"); ?>
