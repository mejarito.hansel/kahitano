<?php
include("old/init.php");
if (SESSION::isLoggedIn()) {
	HTML::redirect('home.php');
}

if (isset($_POST['username']) and isset($_POST['password'])) {
	$user = USERS::getID($_POST['username'], $_POST['password']);
	// var_dump($user);
	// var_dump($user['account_ID']);
	if ($user['account_ACTIVE'] == 1 && $user['access_status'] == 1){
		SESSION::login($user);
		msgbox("Login Success");

		USERS::setActiveInactive(2,$user['account_ID']);

		AUDIT::insert_new($user['account_ID'],"Login", "MODULE: Login, DESCRIPTION: User clicks login button with valid credentials");
	   
		$_SESSION['ins'] = ui::getInstructorID($user['account_ID']);

		if(isset($_SESSION['previous_page']))
		{
			header("location:$_SESSION[previous_page]");
		}else{
			if($_SESSION['USER']['access_ID'] == 5 or $_SESSION['USER']['access_ID'] == 8){
			// HTML::redirect('home.php');
				HTML::redirect('old/');	
			}else{
				HTML::redirect('old/');
			}
		}
	} elseif ($user['account_ACTIVE'] == 1 && $user['access_status'] == 2) {
		SESSION::StoreMsg("Your account is still login", "warning");

	} elseif ($user['account_ACTIVE'] == 2) {
		// AUDIT::insert_new("0","Login", "MODULE: Login, DESCRIPTION: User clicks login button with invalid credentials. User inputs[USERNAME:'".$_POST['username']."'' | PASSWORD: '".$_POST['password']."']");
		SESSION::StoreMsg("Account is not active", "error");
	} else {
		// AUDIT::insert_new("0","Login", "MODULE: Login, DESCRIPTION: User clicks login button with invalid credentials. User inputs[USERNAME:'".$_POST['username']."'' | PASSWORD: '".$_POST['password']."']");
		
		
		SESSION::StoreMsg("You have entered an invalid Username and / or Password.", "error");
	}
} else {
	$_SESSION['errors'] = NULL;
}

include(LAYOUTS_N . "header.php"); 
?>
<div class="container-fluid">
	<div class="mmma-login-container" style="width:50rem; float:none; margin:0 auto; margin-top:5rem;">
		<h3 class="text-center" style="color: #ffffff;">School Management System </h3><br>
		<div class="panel panel-default" style="border-radius:0px;">
			<div class="panel-header" style="padding-top:2rem;">
				<img src="<?= IMG ?>logo.png" class="img center-block">
			</div>
			<div class="panel-body" style="padding:5rem; padding-top:20px; padding-bottom:20px;">
				<?php 
					SESSION::ShowMsg(); 
				?>
				<form  method="POST">
				<div class="input-group">
					<span class="input-group-addon " id="basic-inputUsername"><i class="fa fa-user fa-lg span-icon"></i></span>
					<input type="text" class="form-control input-lg" data-stoppropagation="true" id="inputUsername" title="Username is required" placeholder="Username" autofocus="" required="required" autocomplete="off" name="username">
				</div>
				<br>
				<div class="input-group">
					<span class="input-group-addon " id="basic-inputPassword"><i class="fa fa-key fa-lg span-icon"></i></span>
					<input type="password" class="form-control input-lg" data-stoppropagation="true" id="inputPassword" title="Password is required" placeholder="Password" required="required" autocomplete="off" name="password">
				</div>
				<br>
				<button type="submit" class="btn btn-primary btn-md btn-block center-block" style="float:right; border:none; border-radius:0rem !important;">Login</button>
				</form>
			</div>
			<div class="panel-footer text-center">
				For technical support, please message <a href="mailto:support@massiveits.com">here</a>
			</div>
		</div>
	</div>	
</div>
<?php
include(LAYOUTS_N . "footer.php"); 
?>
</html>
